---
title: "Prepare and deliver the source code"
linkTitle: "Prepare and deliver the source code"
type: "docs"
weight: 7
---

Discovery is a process that is actioned during the delivery process.
CAST will attempt to automatically identify "projects" within your
application using a set of predefined rules. See:

- [Mainframe Discoverer](../extensions/com.castsoftware.dmtmainframediscoverer)

You should read the relevant documentation for each discoverer (provided
in the link above) to understand how the source code will be handled.

## Source code delivery

AIP Console expects either a ZIP/archive file or source code
located in a folder configured in AIP Console. You should include in
the ZIP/source code folder all Mainframe source code, for example:

-   Cobol: \*.cob, \*.cbl, \*.pco, \*.sqb, \*.cpy, \*.cop, \*.cpb
-   JCL: \*.jcl, \*.prc, \*.mbr, \*.inc
-   IMS: \*.dbd, \*.psb, \*.tra (≥ 8.3.19), .mfs (≥ 8.3.25)
-   CICS: \*.csd, \*.cics, \*.bms

CAST highly recommends placing the files in a folder dedicated to
Mainframe. If you are using a ZIP/archive file, zip the folders in the
"temp" folder - but do not zip the "temp" folder itself, nor create any
intermediary folders:

``` java
D:\temp
    |-----Mainframe
    |-----OtherTechno1
    |-----OtherTechno2
```

## Handling PDS dump files

A PDS is a type of "library" containing elements known as "members"
exported from a z/OS system (eg. Cobol programs, copybooks, JCL
etc.) Each member in the PDS is preceded by a banner containing the
member's name (among other information) and is concatenated with other
elements in text format. Since AIP Console 1.19, it is possible to
deliver source code via a PDS dump. As with the legacy CAST Delivery
Manager, AIP Console only supports one type of member and one banner
prefix per PDS dump file. If there are several types of members they
must be delivered through multiple dump files and if several banner
prefixes are used for the same type of members, then the source code
delivery must also be done through multiple dump files.

If you want to deliver PDS dump files (containing the Mainframe
source code) in the ZIP or the source code folder location, you will
need to configure AIP Console to recognise them. This can be done using
the [Administration Center - Settings - PDS
Dump](https://doc.castsoftware.com/display/AIPCONSOLE/Administration+Center+-+Settings+-+PDS+Dump) option
available in [Administration Center - Global
Configurations](https://doc.castsoftware.com/display/AIPCONSOLE/Administration+Center+-+Global+Configurations):

![](pds.jpg)

Out of the box, Console will have several PDS library extensions
predefined. This will ensure that Console is able to recognise PDS dump
files provided in the source code configured as follows - you can leave
these predefined PDS types as is, or you can delete/edit as necessary:

![](predefined.jpg)

All entries will be configured with a banner prefix "VMEMBER NAME",
left margin =1 and Line maxLength = 80:

-   COBX
    -   .cob
-   CPYX
    -   .cpy
-   JCLX
    -   .jcl
-   PSBX
    -   .psb
-   DBDX
    -   .dbd
-   BMSX
    -   .bms

Each PDS dump file that is recognised by AIP Console will be extracted:
one file (that the CAST Mainframe Analyzer can analyze) will be created
per element in the PDS dump file and these files are then analyzed when
an analysis is run.