---
title: "Analysis results"
linkTitle: "Analysis results"
type: "docs"
weight: 10
---

## What results can you expect?

### Objects

#### Cobol

| Icon | Description |
|:----:|-------------|
| ![](../images/664436777.png) | Cobol Class                                     |
| ![](../images/664436784.png) | Cobol Conditional Test                          |
| ![](../images/664436785.png) | Cobol Constant Data                             |
| ![](../images/664436775.png) | Cobol CopyBook                                  |
| ![](../images/664436789.png) | Cobol Data                                      |
| ![](../images/664436790.png) | Cobol Data Link                                 |
| ![](../images/664436789.png) | Cobol Declarative Block                         |
| ![](../images/664436789.png) | Cobol Declarative Paragraph                     |
| ![](../images/664436786.png) | Cobol Declarative Section, Cobol Section        |
| ![](../images/664436778.png) | Cobol Directory/Root Directory                  |
| ![](../images/664436788.png) | Cobol Division                                  |
| ![](../images/664436779.png) | Cobol Entry Point                               |
| ![](../images/664436791.png) | Cobol File Link (Sequential, Partitioned, VSAM) |
| ![](../images/664436793.png) | Cobol Index                                     |
| ![](../images/664436780.png) | Cobol Interface                                 |
| ![](../images/664436785.png) | Cobol Literal                                   |
| ![](../images/664436787.png) | Cobol Paragraph                                 |
| ![](../images/664436844.png) | Cobol Nested Program                            |
| ![](../images/664436776.png) | Cobol Program                                   |
| ![](../images/664436785.png) | Cobol Structure Data                            |
| ![](../images/664436781.png) | Cobol Transaction                               |
| ![](../images/664436792.png) | Cobol SQL Query                                 |
| ![](../images/664436845.png) | Publisher IBM MQ                                |
| ![](../images/664436846.png) | Subscriber IBM MQ                               |
| ![](../images/CAST_CallToJavaProgram32.png) | Cobol Call To Java Method        |
| ![](../images/664436845.png) | Cobol Data Queue Publisher                      |
| ![](../images/664436846.png) | Cobol Data Queue Receiver                       |

#### JCL

| Icon | Description |
|:----:|-------------|
| ![](../images/664436798.png) | JCL Data Set (Sequential, Partitioned, VSAM) |
| ![](../images/664436802.png) | JCL Directory/Root Directory                 |
| ![](../images/664436935.png) | JCL External Procedure                       |
| ![](../images/664436934.png) | JCL External Program                         |
| ![](../images/664436799.png) | JCL Included File / Included Not Found       |
| ![](../images/664436800.png) | JCL Index                                    |
| ![](../images/664436797.png) | JCL In-stream Procedure                      |
| ![](../images/664436794.png) | JCL to Java Program                          |
| ![](../images/664436846.png) | JCL (Catalogued) Job                         |
| ![](../images/664436936.png) | Unknown JCL Procedure                        |
| ![](../images/664436848.png) | JCL (Catalogued) Job Prototype               |
| ![](../images/664436795.png) | JCL (Catalogued) Procedure                   |
| ![](../images/664436847.png) | JCL Project                                  |
| ![](../images/664436796.png) | JCL Step                                     |
| ![](../images/664436803.png) | JCL SQL Query                                |

#### IMS

| Icon | Description |
|:----:|-------------|
| ![](../images/664436809.png) | IMS Alternate Program Control Block |
| ![](../images/664436811.png) | IMS Database Program Control Block  |
| ![](../images/664436807.png) | IMS DB Definition                   |
| ![](../images/664436808.png) | IMS DB Field                        |
| ![](../images/664436802.png) | IMS Directory/Root Directory        |
| ![](../images/664436815.png) | IMS File / Transaction File         |
| ![](../images/664436812.png) | IMS GSAM Program Control Block      |
| ![](../images/664436817.png) | IMS Message Format Service          |
| ![](../images/664436818.png) | IMS Message Input Descriptor        |
| ![](../images/664436819.png) | IMS Message Output Descriptor       |
| ![](../images/664436850.png) | IMS Program Control Block           |
| ![](../images/664436810.png) | IMS Program Specification Block     |
| ![](../images/664436813.png) | IMS Project                         |
| ![](../images/664436814.png) | IMS Segment / DB Segment            |
| ![](../images/664436816.png) | IMS Transaction                     |
| ![](../images/664436803.png) | IMS SQL Query                       |

#### CICS

| Icon | Description |
|:----:|-------------|
| ![](../images/664436842.png) | CICS DataSet                 |
| ![](../images/664436781.png) | CICS DB2 Transaction         |
| ![](../images/664436940.png) | CICS Definition File         |
| ![](../images/664436943.png) | CICS External File           |
| ![](../images/664436802.png) | CICS Folder                  |
| ![](../images/664436941.png) | CICS Group                   |
| ![](../images/664436839.png) | CICS Map                     |
| ![](../images/664436840.png) | CICS MapSet                  |
| ![](../images/664436939.png) | CICS Map Definition          |
| ![](../images/664436937.png) | CICS Project                 |
| ![](../images/664436942.png) | CICS Temporary Storage       |
| ![](../images/664436781.png) | CICS Transaction             |
| ![](../images/664436841.png) | CICS Transient Data          |
| ![](../images/664436836.png) | CICS SOAP Operation          |
| ![](../images/664436836.png) | CICS SOAP Operation Call     |
### Links

#### Cobol

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td colspan="2" rowspan="2" class="confluenceTd"><strong>Link
Type</strong></td>
<td colspan="2" class="confluenceTd"><strong>Linked
Objects</strong></td>
<td rowspan="2" class="confluenceTd"><strong>Code Example</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Calling</strong></td>
<td class="confluenceTd"><strong>Called</strong></td>
</tr>
<tr class="odd">
<td rowspan="7" class="confluenceTd"><strong>CALL</strong></td>
<td class="confluenceTd"><strong>PROG*</strong></td>
<td rowspan="2" class="confluenceTd">Program, section or copybook</td>
<td rowspan="2" class="confluenceTd">Program or entrypoint</td>
<td class="confluenceTd">CALL "CC2DISPLAY"</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>TRANSAC*</strong></td>
<td class="confluenceTd">EXEC CICS XCTL<br />
PROGRAM(TEST)<br />
END EXEC</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>PERFORM</strong></td>
<td class="confluenceTd">Program, section or sub-object</td>
<td class="confluenceTd">Section or paragraph</td>
<td class="confluenceTd">MODULE-ENTREE SECTION.<br />
*<br />
* LA LIGNE CI DESSOUS DOIT ETRE DELETEE APRES LES TESTS<br />
*<br />
MOVE '* EXECUTION STARTEE NORMALEMENT *' TO MESS.<br />
DISPLAY MESS.<br />
MOVE SPACE TO MESS.<br />
*<br />
<strong>PERFORM LECTURE-PARAM</strong>.<br />
*<br />
FIN-MODULE-ENTREE. EXIT.<br />
EJECT<br />
*<br />
COPY SYBPINIT.</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd"><strong>GOTO</strong></td>
<td class="confluenceTd">Program</td>
<td class="confluenceTd">First executed section or paragraph</td>
<td rowspan="3" class="confluenceTd"><br />
LECTURE-PARAM SECTION.<br />
<br />
*<br />
READ MEF001.<br />
IF FILE-STATUS NOT = '00'<br />
DISPLAY 'CODE RETOUR' FILE-STATUS<br />
DISPLAY 'CARTE PARAMETRE INEXISTANTE : ' CODE-ABEND<br />
<strong>GO TO SORTIE-ERREUR</strong><br />
END-IF.<br />
*</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Section</td>
<td class="confluenceTd">First executed paragraph in section</td>
</tr>
<tr class="even">
<td class="confluenceTd">Paragraph</td>
<td class="confluenceTd">Next executed paragraph in same section</td>
</tr>
<tr class="odd">
<td class="confluenceTd">-</td>
<td class="confluenceTd">Section/Paragraph</td>
<td class="confluenceTd">Section/Paragraph</td>
<td class="confluenceTd">When Sections or Paragraphs are sequentially
executed (one after the other) a Call link will be generated. For
example, if a program is made of Section A then Section B then Paragraph
C then Section D, the links will be resolved as A -&gt; B -&gt; C -&gt;
D.</td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>INCLUDE</strong></td>
<td class="confluenceTd">Program or Copybook</td>
<td class="confluenceTd">Copybook</td>
<td class="confluenceTd">* -ACDA * MNPW * * * * * * * * * * * * * * * *
* * * * * * * * *<br />
* FD DU FICHIER INF103 PAR LE CHEMIN INF103<br />
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *<br />
*<br />
BLOCK CONTAINS 0.<br />
*<br />
<strong>COPY INF103 REPLACING INF103 BY ING103.</strong></td>
</tr>
<tr class="odd">
<td colspan="2" rowspan="2"
class="confluenceTd"><strong>USE</strong></td>
<td colspan="2" class="confluenceTd">Calls to matched character
strings</td>
<td class="confluenceTd">Display "a <strong>string</strong>"</td>
</tr>
<tr class="even">
<td class="confluenceTd"><ul>
<li>Cobol Program</li>
<li>Cobol Section</li>
<li>Cobol Paragraph</li>
</ul></td>
<td class="confluenceTd">Program Specification Block</td>
<td class="confluenceTd">EXEC DLI SCH PSB (<strong>MYPSB</strong>)
END-EXEC</td>
</tr>
<tr class="odd">
<td rowspan="4" class="confluenceTd"><strong>ACCESS</strong></td>
<td class="confluenceTd"><strong>OPEN</strong></td>
<td rowspan="2" class="confluenceTd"><ul>
<li>Cobol Program or sub-object</li>
</ul></td>
<td rowspan="2" class="confluenceTd"><ul>
<li>Cobol File Link</li>
<li>Cobol Data Link</li>
</ul></td>
<td rowspan="4"
class="confluenceTd">*========================================================
* OUVERTURE DU FICHIER IAHCDECE
*========================================================
OPEN-IAHCDECE.<br />
<strong>OPEN INPUT IAHCDECE</strong>.
*======================================================== * LECTURES DU
FICHIER IAHCDECE
*======================================================== LECT-IAHCDECE.
<strong><br />
READ IAHCDECE</strong>.
*======================================================== * FERMETURE DU
FICHIER IAHCDECE
*========================================================
CLOSE-IAHCDECE.<br />
<strong>CLOSE IAHCDECE</strong>.<br />
 &#10;<p>*======================================================== *
LECTURE-SEGMENT-CISRAPMP
*========================================================<br />
EXEC DLI<br />
GU<br />
USING<br />
PCB (3)<br />
SEGMENT (<strong>CISRAPMP</strong>)<br />
WHERE (CIOEAX = IN-PREM-H)<br />
INTO (WS-DUMMY-AREA)<br />
END-EXEC.<br />
 </p>
<p>*========================================================<br />
COMMANDE-CALL-LEVEL<br />
*========================================================<br />
CALL 'CBLTDLI' USING GU<br />
<strong>CICSDLI-PCB</strong><br />
CICSDLI-SEGM<br />
CICSDLI-QSSA</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>CLOSE</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>READ</strong></td>
<td rowspan="2" class="confluenceTd"><ul>
<li>Cobol Program or sub-object</li>
<li>Cobol Section</li>
<li>Cobol Paragraph</li>
</ul></td>
<td rowspan="2" class="confluenceTd"><ul>
<li>Cobol File Link</li>
<li>Cobol Data Link</li>
<li>Constant Data</li>
<li>Structured Data</li>
<li>Data</li>
<li>Conditional Test</li>
<li>Program Communication Block<br />
(IMS PCB)</li>
<li>IMS Segment</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>WRITE</strong></td>
</tr>
</tbody>
</table>

For Embedded SQL links, the following are valid for all servers.

| Link | Sub-link | Description | Example |
|---|---|---|---|
| USE | SELECT | This type is reserved for server side object referencing | `EXEC SQL... SELECT` |
| USE | UPDATE | As above. | `EXEC SQL... UPDATE` |
| USE | INSERT | As above. | `EXEC SQL... INSERT` |
| USE | DELETE | As above. | `EXEC SQL... DELETE` |
| CALL | - | - | `EXEC SQL... CAL`    |

For link types `CALL PROG` and `CALL TRANSAC`, two limitations exist when the call is in "string" form:

- If the string is constant and declared in the "data-division" section, the entry point will be resolved in the normal way.
- If the string is dynamic, the program may be found by the Dynamic Link Manager.

In addition, the following Embedded SQL links are valid for DB2 only:

| Link | Sub-link | Description | Example |
|---|---|---|---|
| DEPEND ON | - | This type is reserved for server side object referencing on structured or distinct UDTs. | - |
| DDL | CREATE | This type is reserved for server side object referencing on Tables | - |
| DDL | DROP | - | - |

#### JCL

| Links | Sub-link | Calling object | Called object |
|---|---|---|---|
| ACCESS | WRITE, READ, EXECUTE | JCL Step  | JCL Data Set |
| PROTOTYPE | - | Cobol File Link    | JCL Data Set      |
|  |  | Cobol Data Link | JCL Data Set  |
|  |  | JCL Data Set | Cobol JCL Program |
| CALL | - | JCL Step | Cobol JCL Program |
|  |  | JCL Job | JCL Step |
|  |  | JCL Procedure | JCL Step |
|  |  | JCL Step | JCL Procedure |
| USE | - | JCL Step | IMS DBD |
| MONITOR | AFTER | JCL Step | JCL Step |

#### IMS

| Links | Sub-link | Calling object | Called object |
|---|---|---|---|
| ACCESS | WRITE | IMS PC Block | IMS Segment |
| USE | - | IMS PC Block | IMS DBD or IMS GSAM File |

#### CICS

| Links | Sub-link | Calling object | Called object |
|---|---|---|---|
| CALL | TRANSAC | CICS Transid | Client/Cobol Program |

For Transactional Code, the following are valid:

| Link | Sub-link | Calling object  | Called objects | When does this link occur? |
|---|---|---|---|---|
| CALL | TRANSAC | Client/Cobol Program or its Sub object | Client/Cobol Program | `EXEC CICS XCTL`<br>`PROGRAM(TEST)`<br>`END EXEC`<br><br>or<br><br>`EXEC CICS LINK`<br>`PROGRAM(TEST)`<br>`END EXEC` |
| CALL | TRANSAC | Client/Cobol Program or its Sub object | CICS Transaction | `EXEC CICS START`<br>`TRANSID(TRANSID)`<br>`END EXEC`<br><br>or<br><br>`EXEC CICS RETURN`<br>`TRANSID (W-XFR-CTRS)`<br>`COMMAREA (W-CIC-CMA-LDON)`<br>`LENGTH (W-CIC-CMA-QLENDON)`<br>`END-EXEC` |
| MONITOR | - | Client/Cobol Program or its Sub object | CICS Map | `EXEC CICS`<br>`SEND MAP (W-CIC-CMAPCUR)`<br>`MAPSET (W-CIC-CMPSCUR)`<br>`FROM (MGAB10O)`<br>`ERASE`<br>`FREEKB`<br>`FRSET`<br>`CURSOR`<br>`END-EXEC`<br><br>or<br><br>`EXEC CICS`<br>`RECEIVE MAP (W-CIC-CMAPCUR)`<br>`MAPSET (W-CIC-CMPSCUR)`<br>`FROM (MGAB10O)`<br>`END-EXEC` |
| ACCESS | OPEN, CLOSE, READ, WRITE | Client/Cobol Program or its Sub object | CICS Dataset | `ENDBR, DELETE,`<br>`LINK, READ, READNEXT,`<br>`READPREV, REWRITE,`<br>`STARTBR, XCTL,`<br>`WRITE `<br><br>e.g.:<br><br>`EXEC CICS`<br>`READ DATASET ('C1MASTR')`<br>`INTO (BLKBLK(BLKBLK-OCUR))`<br>`RIDFLD (CICS-RRN) RRN`<br>`RESP (WS-RESP) END-EXEC` |
| ACCESS | READ, WRITE | Client/Cobol Program or its Sub object | CICS Transient Data | `DELETEQ, READQ, WRITEQ`<br><br>e.g.:<br><br>`EXEC CICS DELETEQ TD QUEUE (W-CIC-TSQ-LNOM)` |

## Miscellaneous Cobol information

Rules for resolving Paragraph names in a Section

The Mainframe Analyzer (Cobol) uses the following rules when resolving
Paragraph names defined in Sections:

1.  If the referenced Paragraph is located in the current Section,
    Cobol Analyzer will link the calling Paragraph to the called
    Paragraph
2.  If the referenced Paragraph is not located in the current
    section, Cobol Analyzer will issue a syntax error
3.  If the referenced Paragraph has a unique declaration outside the
    Section, Cobol Analyzer will create a link to this Paragraph.
4.  The following syntax is also resolved: "Paragraph Name IN/OF Section
    Name". A link will be created to the correct Paragraph (which
    will be outside the current Section).

Access type links

Access type links are created when your Cobol program calls an
external file (Data Set).

1.  The instructions that manage classic files ('File Link' type) are:

    Instructions for opening a file: OPEN Reading data: READ Writing
    data: WRITE, REWRITE Closing the file: CLOSE

2.  Sorting operations on data set files (Data Link type) are carried
    out via the instructions MERGE and/or SORT. These instructions
    should generate CALL + PERFORM type links on paragraphs or sections.
    OPEN and CLOSE instructions are also used.

3.  Access (Read/Write) links to "Cobol Constants" and "Cobol Data":
    -   Variables used in the flow control: IF, PERFORM, EVALUATE,
        SEARCH.
    -   Inclusion of the instructions: MOVE, INITIALIZE, SET, CALL,
        OPEN, CLOSE, READ, WRITE, REWRITE, MERGE, SORT.
    -   The following instructions are not yet included: MULTIPLY,
        SUBTRACT, ADD, DIVIDE, EXHIBIT.

4.  CICS files read via CICS instructions in an EXEC CICS ... END-EXEC
    -   Opening of file - Instructions: STARTBR -- Acesse(Open) on the
        file
    -   Data reading - Instructions: DELETE, WRITE, REWRITE --
        Access(Write)
    -   Closing of file - Instruction: ENDBR -- Access(Close)

5.  Access to the segment in the IMS databases:
    -   Links to the segment (Access + Write/Read)
    -   Links to the Program Specification Block

*File Selection*

Logical Files are declared in the "File Section" part of the
program with the FD or SD tag.

-   FD: declaration of  'file link' type
-   SD: declaration of 'data link' type

Example declaration of a logical file called MAIL-XREF (type FILE-LINK)
in the Cobol program:

``` text
004600 FILE SECTION.                          00015900
004700                                          00016000
009000 FD MAIL-XREF                             00020700
009100 LABEL RECORDS ARE STANDARD               00020800
009200 BLOCK CONTAINS 0 RECORDS.                00020900
009300 01 MAIL-XREF-REC.                        00021000
009400 03 MAIL-XREF-KEY.                        00021100
009500 07 MAIL-XREF-ADDRESS.                    00021200
009600 11 MAIL-XREF-ZIP-PRE PIC X(05).          00021300
009700 11 MAIL-XREF-ZIP-SUF PIC X(04).          00021400
009800 07 MAIL-XREF-STATE PIC X(03).            00021500
009900 07 MAIL-XREF-CITY PIC X(25).             00021600
010000 07 MAIL-XREF-POSTAL-CODE PIC X(02).      00021700
010100 03 MAIL-XREF-DATA PIC X(324).            00021800
```

Replacement used in COPY REPLACING directives

-   Replacements can be applied to words or parts of words
-   Patterns used to replace parts of words must be delimited by the
    following characters:  <u>:</u>, <u>(</u>, <u>)</u>, <u>\\</u> or
    <u>"</u>
-   Patterns that are not delimited by the above characters are
    considered as being used to replace entire words
-   LEADING and TRAILING clauses mean that the replacement will be
    applied on parts of words and as such, patterns must respect rule
    two (first character and last character will be removed from the
    pattern).

## Miscellaneous JCL Information

JCL is a command language used to execute programs on large
systems - primarily Cobol oriented. CAST's Mainframe Analyzer (JCL) is
targeted at IBM's JCL language for 3090 systems used in conjunction
with Cobol projects.

## Specific information about rule results

### Avoid using ALTER (5062)

The rule [Avoid using ALTER (5062)](https://technologies.castsoftware.com/rules?s=5062|qualityrules|5062) is designed to trigger a violation when an `ALTER` statement is used in a `COBOL program`. However, when a `COBOL copybook` is referenced in a `COBOL program` and the copybook itself contains an `ALTER` statement, CAST will create a violation on the `COBOL program`, even though the `COBOL program` itself does not contain the `ALTER` statement.

This behaviour may create violations where you were not expecting them.

## Display in CAST Enlighten

In CAST Enlighten, a full Mainframe Analysis (i.e. including COBOL, JCL,
CICS and IMS) may be represented as follows:

![](363855939.gif)

COBOL Objects branch - if the data has NOT been saved in the
Analysis Service (Data Structures \> Save Data Only option in the
[Mainframe Technology options](../analysis-config/) is
not activated). Note that the Cobol Copybooks and its sub-heading
Data in the Source file folders heading will only be visible if
the Save data found in copy books option in the [Mainframe Technology options](../analysis-config/) is activated:

![](363855940.gif)

COBOL Objects branch - if the data has been saved in the Analysis
Service (Data Structures \> Save Data Only option in the [Mainframe Technology options](../analysis-config/) is activated).
Note that the Cobol Copybooks and its sub-heading Data in the
Source file folders heading will only be visible if the Save data
found in copy books option in the [Mainframe Technology options](../analysis-config/) is activated:

![](363855941.gif)

JCL Objects branch

![](363855942.gif)

CICS Objects branch

![](363855943.gif)

IMS Objects branch

![](363855944.gif)

## Changes introduced in Mainframe Analyzer 1.1

### New object "Cobol SQL Query"

For each embedded SQL statement found in Cobol (except INCLUDE
statements which are already supported by creating a Copy Book object),
an object called Cobol SQL Query is created. For example, the
following code will cause this object type to be created:

``` java
EXEC SQL
    SELECT ABCDED
    FROM TABLE
END-EXEC
```

### ReferLink between paragraphs/sections

A ReferLink will now be added between sections/paragraphs containing the
CICS command HANDLE ABEND LABEL and the sections/paragraphs specified in
the LABEL clause. For example:

![](cobol1.png)

In the same way, a ReferLink is added for the CICS commands HANDLE
AID and HANDLE CONDITION:

![](cobol2.png)

#### Supported conditions

Below is a list of the conditions supported by the Mainframe Analyzer ≥
1.1:

| CICS command | Condition |
|---|---|
| HANDLE ABEND | LABEL |
| HANDLE AID | ANYKEY<br>CLEAR<br>CLRPARTN<br>ENTER<br>LIGHTPEN<br>OPERID<br>PA1, PA2, PA3<br>PF1 - PF24<br>TRIGGER  |
| HANDLE CONDITION | ALLOCERR<br>CBIDERR<br>CHANNELERR<br>DISABLED<br>DSIDERR<br>DSSTAT<br>DUPKEY<br>DUPREC<br>END<br>ENDDATA<br>ENDFILE<br>ENDINPT<br>ENQBUSY<br>ENVDEFERR<br>EOC<br>EODS<br>EOF<br>ERROR<br>EXPIRED<br>FUNCERR<br>IGREQCD<br>IGREQID<br>ILLOGIC<br>INBFMH<br>INVERRTERM<br>INVEXITREQ<br>INVLDC<br>INVMPSZ <br>INVPARTN<br>INVPARTNSET<br>INVREQ<br>IOERR<br>ISCINVREQ   <br>ITEMERR<br>JIDERR<br>LENGERR<br>LOADING<br>LOCKED<br>MAPFAIL<br>NETNAMEIDERR<br>NODEIDERR<br>NOJBUFSP<br>NONVAL<br>NOPASSBKRD<br>NOPASSBKWR<br>NOSPACE <br>NOSPOOL<br>NOSTART<br>NOSTG<br>NOTALLOC<br>NOTAUTH    <br>NOTFND<br>NOTOPEN     <br>OPENERR<br>OVERFLOW<br>PARTNERIDERR<br>PARTNFAIL<br>PGMIDERR<br>QBUSY    <br>QIDERR<br>QZERO<br>RDATT<br>RECORDBUSY<br>RESUNAVAIL<br>RETPAGE<br>ROLLEDBACK   <br>RTEFAIL<br>RTESOME<br>SELNERR<br>SESSBUSY<br>SESSIONERR<br>SIGNAL<br>SPOLBUSY<br>SPOLERR<br>STRELERR<br>SUPPRESSED<br>SYSBUSY<br>SYSIDERR<br>TERMERR<br>TASKIDERR<br>TERMIDERR <br>TRANSIDERR<br>TSIOERR<br>UNEXPIN<br>USERIDERR<br>WRBRK |

## Changes introduced in Mainframe Analyzer 1.2

### Support for CICS Webservices

#### Introduction

Web services make the interactions between programs over a network.
There are two types of CICS Webservice:

-   Provider: CICS acting as server to provide information
-   Requester: CICS acting as client to request information

For example:

![](649330731.png)

The items highlighted with a green oval represent the new objects and
the new links to existing Mainframe type objects. Where:

-   Cob program ECHOCLNT: client program to invoke the cob program
    which handles the CICS Webservice  
-   Cob program ECHOPROG: the cob program which handles the CICS
    Webservice as a Provider
-   Cob program ECHOWEBS: the cob program which handles the CICS
    Webservice as a Requester
-   CICS Webservice and Call to CICS Webservice: new objects
    which represents CICS webservice support 
-   JCL Step LS2WS: JCL step which run a Webservice utility batch to
    create a web service provider in CICS
-   JCL Step WS2LS: JCL step which run a Webservice utility batch to
    create a web service requester in CICS


#### CICS SOAP Operation/CICS SOAP Operation Call

We use the SOAP protocol for these objects.

Linking by **port_type.operation_name**

Each wsdl is a webservice resource file corresponding to a CICS SOAP Operation or CICS SOAP Operation Call which must consist the URI, PortType and Operation:
``` java
<portType name="ECHOPROGPort">
      <operation name="ECHOPROGOperation">
         <input message="tns:ECHOPROGOperationRequest" name="ECHOPROGOperationRequest"/>
         <output message="tns:ECHOPROGOperationResponse" name="ECHOPROGOperationResponse"/>
      </operation>
</portType>
<service name="ECHOPROGService">
      <port binding="tns:ECHOPROGHTTPSoapBinding" name="ECHOPROGPort">
         <!--This soap:address indicates the location of the Web service over HTTP.
              Please replace "my-server" with the TCPIP host name of your CICS region.
              Please replace "my-port" with the port number of your CICS TCPIPSERVICE.-->
         <soap:address location="http://my-server:my-port/ca1p/echoProgProviderBatch"/>
         <!--This soap:address indicates the location of the Web service over HTTPS.-->
         <!--<soap:address location="https://my-server:my-port/ca1p/echoProgProviderBatch"/>-->
         <!--This soap:address indicates the location of the Web service over WebSphere MQSeries.
              Please replace "my-queue" with the appropriate queue name.-->
         <!--<soap:address location="jms:/queue?destination=my-queue&connectionFactory=()&targetService=/ca1p/echoProgProviderBatch&initialContextFactory=com.ibm.mq.jms.Nojndi" />-->
      </port>
 </service>
```

#### Configuration

In order for CAST to be able to fully support CICS Webservices, the
extension \*.wsdl must be manually added to the CICS File
Extensions option in the configuration of Mainframe extension in CAST
Console:

![](649330730.jpg)

Note: this file extension .wsdl will be added to the CICS File
Extensions option by default in a future release of AIP Core.

#### Example Provider Webservice

SAMPLE1.jcl job has the LS2WS step which run the DFHLS2WS
webservice utility and exposed ECHOPROG cob program as a Webservice
Provider:

``` java
//LS2WS   EXEC DFHLS2WS,                                                        
//    JAVADIR='java142s/J1.4',                                                  
//    USSDIR='cics650',                                                         
//    PATHPREF='',                                                              
//    TMPDIR='/tmp',                                                            
//    TMPFILE='ls2ws'                                                           
//INPUT.SYSUT1 DD *                                                             
LOGFILE=<Install_Directory>/ca1p/wsbind/provider/*                              
echoProgProviderBatch.log                                                       
PDSLIB=//<INSTALL_HLQ>.CA1P.COBCOPY                                             
REQMEM=ECHOCOMM                                                                 
RESPMEM=ECHOCOMM                                                                
LANG=COBOL                                                                      
PGMNAME=ECHOPROG                                                                
URI=ca1p/echoProgProviderBatch                                                  
PGMINT=COMMAREA                                                                 
WSBIND=<Install_Directory>/ca1p/wsbind/provider/*                               
echoProgProviderBatch.wsbind                                                    
WSDL=<Install_Directory>/ca1p/wsdl/echoProgProviderBatch.wsdl                   
MAPPING-LEVEL=1.2    
```

The parameter PGMNAME specifies the name of the cob program that will be exposed as a web service => ECHOPROG.cob

The parameter WSDL specifies the location of the web service description file => echoProgProviderBatch.wsdl 

#### Example Requester Webservice

SAMPLE2.jcl job has the WS2LS step which run the DFHWS2LS
webservice utility and exposed ECHOWEBS cob program as a Webservice
Requester:

``` java
//WS2LS   EXEC DFHWS2LS,                                                        
//    JAVADIR='java142s/J1.4',                                                  
//    USSDIR='cics650',                                                         
//    PATHPREF='',                                                              
//    TMPDIR='/tmp',                                                            
//    TMPFILE='WS2LS'                                                           
//INPUT.SYSUT1 DD *                                                             
LOGFILE=<Install_Directory>/ca1p/wsbind/requester/*                             
echoProgRequesterBatch.log                                                      
PDSLIB=//<INSTALL_HLQ>.CA1P.COBCOPY                                             
REQMEM=ECHOPI                                                                   
RESPMEM=ECHOPO                                                                  
LANG=COBOL                                                                      
WSBIND=<Install_Directory>/ca1p/wsbind/requester/*                              
echoProgRequesterBatch.wsbind                                                   
WSDL=<Install_Directory>/ca1p/wsdl/echoProgRequesterBatch.wsdl                  
MAPPING-LEVEL=1.2  
```

ECHOWEBS.cob must consist a CICS invoke webservice command linking to the same webservice resource echoProgRequesterBatch (echoProgRequesterBatch.wsdl):

``` java
MOVE 'echoProgRequesterBatch'                                        
             TO WS-WEBSERVICE-NAME 
EXEC CICS INVOKE WEBSERVICE(WS-WEBSERVICE-NAME)                      
             CHANNEL(WS-CHANNEL-NAME)                                           
             OPERATION(WS-OPERATION-NAME)                                       
             RESP(COMMAND-RESP) RESP2(COMMAND-RESP2)                            
END-EXEC                                                 
```



### Links to RPG objects

Cobol File Link objects will link to DDS Display File /DDS Printer
File/ DDS Physical File / DDS Logical File of RPG which has the same
object name:

![](662143556.png)

### Links to Cobol Program objects

#### Support EXEC CICS LOAD PROGRAM

A useLink link will added from Cobol Section objects to Cobol Program
objects when the EXEC CICS LOAD PROGRAM statement is used:

``` java
EXEC CICS LOAD PROGRAM ('ME00000B')
```

![](656802088.png)

#### Support EXEC CICS HANDLE ABEND PROGRAM

A referLink will be added from Cobol Section objects to Cobol Program
objects when the EXEC CICS HAND ABEND PROGRAM statement is used:

``` java
EXEC CICS HANDLE ABEND PROGRAM ('ME00000A')
```

![](656802087.png)

#### Support INDEX BY

A new object type Cobol Index is created with a RelyON Link to cobol
data using INDEX BY statement, other links are created to the new Cobol
Index Object when it is referenced:

``` java
15 T-BUCH-VAR          OCCURS 1000  ASCENDING KEY
                                  T-BUCH-ST-2-20
                       INDEXED BY I-BUCH.

...

D0200.
           SET I-BUCH TO 1.
      
D0202.
           MOVE T-BUCH-VAR (I-BUCH) TO V-BUCH.
```

#### Support PROCESSING PROCEDURE

Link between paragraphs is created when they are referenced in
PROCESSING PROCEDURE statement:

``` java
 1300-XML-PARSE.                                          
             XML PARSE WS-INT-XML-RESPONSE                                      
                       ( 1 : WS-INT-XML-RESPONSE-LEN )                          
                 PROCESSING PROCEDURE                                           
                   1350-XMLEVENT-HANDLER THRU 1350-EXIT 
             END-XML                                            
```

![](662143555.png)

## Changes introduced in Mainframe Analyzer 1.3

### New object "IMS SQL Query"

For each embedded IMS SQL statement found in Cobol, an object called
IMS SQL Query is created. For example, the following code will cause
this object type to be created:

```
ENTERING-THE-PROGRAM.                                            
EXEC SQLIMS                                                  
FETCH cursor-name INTO :HOSPITAL-RESULT-ROW               
END-EXEC
```    

![](ims_sql_query.jpg)

## Changes introduced in Mainframe Analyzer 1.4

### Add links from  "IMS SQL Query" to "IMS Segment" 

![](link_ims_query.png)

## Changes introduced in Mainframe Analyzer 1.5.0-beta1

### Support CICS Mirror Transactions
A callTransacLink is created when a Cobol program is associated with a Transaction in CSD file.
For example, the following code indicates that Cobol program Y774UPRI is associated with TRANSID(Y774).

``` java
DEFINE PROGRAM(Y774UPRI) GROUP(N476P1)                                         
DESCRIPTION(CSCF - UPS PREMIER INITIATION MODULE)                              
       LANGUAGE(COBOL) RELOAD(NO) RESIDENT(NO) USAGE(NORMAL)                   
       USELPACOPY(NO) STATUS(ENABLED) CEDF(YES) DATALOCATION(ANY)              
       EXECKEY(USER) CONCURRENCY(THREADSAFE) API(CICSAPI) DYNAMIC(NO)          
       REMOTESYSTEM(CS28) REMOTENAME(Y774UPRI) TRANSID(Y774) 
```
![](cics_mirror.png)

## Changes introduced in Mainframe Analyzer 1.5.0-beta5

### Support Cobol Call To Java Method in **IBM i (AS/400)**
New support of COBOL program calls Java methods in **IBM i (AS/400)** platform using invocation API functions Java Native Interface (JNI).

An example Cobol program calls to Java Methods: [Example of COBOL Calling Java](https://www.ibm.com/support/pages/example-cobol-calling-java-0) 

First of all, Java VM is created and initialized, the API references transfer to **JDKINIT** and **JNI** members. 

JNI interface function **FIND-CLASS**:


``` java
CALL FIND-CLASS USING BY VALUE ENV-PTR
                               CLASS-NAME-PTR
                RETURNING INTO MY-CLASS-REF.
```
This function returns a class reference **MY-CLASS-REF** for **CLASS-NAME-PTR** (HelloWorldCbl java class in this example). The value of **CLASSPATH** (JDKINIT member) which is the actual directory containing the class file must be resolved.

**MY-CLASS-REF** can be used to invoke the methods of this class.

JNI interface functions supported to invoke Java methods:

- **GET-METHOD-ID**

- **GET-STATIC-METHOD-ID**

- **NEW-OBJECT**


``` java
CALL GET-METHOD-ID USING BY VALUE ENV-PTR
                         MY-CLASS-REF
                         METHOD-NAME-PTR
                         SIGNATURE-NAME-PTR
                   RETURNING INTO METHOD-ID.
``` 
The Java method **METHOD-NAME-PTR** in the Java class **MY-CLASS-REF** is called. 
An object Cobol Call To Java Method is created and it will link to the corresponding Java Method.

![](image.png)
 

#### Configuration for **IBM i (AS/400)**
Because some predefined names provided by IBM have more than 8 characters such as **CLASSPATH** data, **JDK11INIT** copybook .  The option **IBM z/OS** (using only 8 characters) must be deactivated.
![](image-1.png)

## Changes introduced in Mainframe Analyzer 1.5.2-funcrel

### Support Cobol Call To Java Method in **IBM z/OS**
Similar support with [**IBM i (AS/400)**](#support-cobol-call-to-java-method-in-ibm-i-as400), we add the support Cobol Call To Java Method in **IBM z/OS** platform.

A COBOL program example can be found at: https://www.ibm.com/docs/en/cobol-zos/6.4?topic=methods-example-invoking-java-from-batch-cobol-program

The Java class path is defined in REPOSITORY paragraph:
``` java
Repository.
           Class ZUtil         is "com.ibm.jzos.ZUtil"                          
           Class HelloWorld    is "com.ibm.jzos.sample.HelloWorld"              
```

The INVOKE statement is used to invoke Java method:
``` java
Invoke HelloWorld "main"                                             
               using by value args            
```

Note that **the option IBM z/OS must be activated** (this option is activated by default).

## Changes introduced in Mainframe Analyzer 1.6.0-beta1

### Support Data Queue in **IBM i (AS/400)**
We support Send Data Queue (**QSNDDTAQ**) API and Receive Data Queue (**QRCVDTAQ**) API.

A **Cobol Data Queue Publisher** object or a **Cobol Data Queue Receiver** object is created corresponding to the API call.

Send Data queue **QSNDDTAQ**:
```
            CALL 'QSNDDTAQ' USING   W01-QNAME
                                    W01-LIB
                                    W01-FLDLEN
                                    W01-LINK-MODULE
                                    W01-KEYLEN
                                    W01-DTAQ-KEY.
```
Receive Data queue **QRCVDTAQ**:
```
            CALL 'QRCVDTAQ' USING   W01-QNAME
                                    W01-LIB
                                    W01-FLDLEN
                                    W01-LINK-MODULE
                                    W01-KEYLEN
                                    W01-DTAQ-KEY.
```
![](image-2.png)
The link between **Cobol Data Queue Publisher** object and **Cobol Data Queue Receiver** is created by Universal Linker.