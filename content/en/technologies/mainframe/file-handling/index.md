---
title: "Support for COBOL File Handling Statements"
linkTitle: "Support for COBOL File Handling Statements"
type: "docs"
weight: 6
---

## What are FILES?

-   A FILE is a logical representation of the data that is stored in
    memory location.
-   FILEs are used to store the data permanently in a structured format.
-   A FILE contains RECORDs that logically divide the FILE data.
-   Each RECORD contains the FIELDs that are typically data items or
    identifiers in the RECORD definition.
-   FILEs are usually stored on DISKS/TAPES in a Mainframe environment.
-   Maximum 255 files used in a COBOL program.

## What are File Handling Statements?

File Handling Statements are verbs used in COBOL programs which handle
FILES, for example:

-   OPEN
-   CLOSE
-   WRITE
-   REWRITE
-   READ
-   DELETE
-   START (note that this statement is not currently supported)

## How does the Mainframe Analyzer support File Handling Statements?

The Mainframe Analyzer represents FILEs as COBOL File Link objects.
When the analyzer finds a file handling statement, an accessReadLink
or accessWriteLink is created to the COBOL File Link object. The
way in which the links are created by the Mainframe Analyzer depends on
the options chosen in AIP Console:

![](attachments/572063904/572063905.jpg)

|                                                |                                                                                                                                                                             |
|------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Do not save data and Save data only            | A link is created from the COBOL paragraph to the FILE description only, and no links with COBOL data are created.                                              |
| Save data and links to others data             | For READ/WRITE/REWRITE statements, links between COBOL data / COBOL data record and the COBOL File Link object are created.                                     |
| Save data and links to sections and paragraphs | For READ/WRITE/REWRITE statements, links between COBOL paragraph / COBOL sections and COBOL File Link / COBOL data / COBOL data record objects are created. |
| Save data and links to sections/paragraphs/other data | All links are created. |

More detail is provided below:

### OPEN-CLOSE

Example code:

``` java
000010 DATA DIVISION.
000010    FILE SECTION.
000010    FD STUDENT.
000010    01 STUDENT-FILE.
000010       05 STUDENT-ID PIC 9(5).
000010       05 NAME PIC A(25).
000010 
000010    WORKING-STORAGE SECTION.
000010    01 WS-STUDENT.
000010       05 WS-STUDENT-ID PIC 9(5).
000010       05 WS-NAME PIC A(25).
000010    01 WS-EOF PIC A(1). 
000010 
000010 PROCEDURE DIVISION.
000010    OPEN INPUT STUDENT.
000010       PERFORM UNTIL WS-EOF='Y'
000010          READ STUDENT
000010             NOT AT END DISPLAY STUDENT-FILE
000010          END-READ
000010       END-PERFORM.
000010    CLOSE STUDENT.
000010 STOP RUN.
```

The Mainframe Analyzer will create the accessOpenLink
and accessCloseLink links from the COBOL paragraph to the
COBOL File Link object by searching the OPEN/CLOSE statement and
trying to resolve the file name. These links are supported for all
options. Here we open and close the file description name
STUDENT for the READ statement:

![](image2022-2-2_15-47-58.png)

### WRITE-REWRITE

The WRITE and REWRITE statements are similar in that they both write to
the file. When we write data into a file, the data are read from
the data record associated to the file and pushed into the file
stream.

#### WRITE/REWRITE record-data (Implicit)

This is a data record write to the COBOL File Link object:

``` java
000010 DATA DIVISION.
000010    FILE SECTION.
000010    FD STUDENT2.
000010    01 STUDENT2-FILE.
000010       05 STUDENT2-ID PIC 9(5).
000010       05 NAME PIC A(25).
000010 
000010    WORKING-STORAGE SECTION.
000010    01 WS-STUDENT2.
000010       05 WS-STUDENT2-ID PIC 9(5).
000010       05 WS-NAME2 PIC A(25).
000010    01 WS-EOF PIC A(1). 
000010 
000010 PROCEDURE DIVISION.
000010    OPEN EXTEND STUDENT2.
000010       MOVE 1000 TO STUDENT2-ID.
000010       MOVE 'Tim' TO NAME.
000010       MOVE '10' TO CLASS.
000010       WRITE STUDENT2-FILE
000010       END-WRITE. 
000010    CLOSE STUDENT2.
000010 STOP RUN.
```

Result for each option in AIP Console:

##### Save data and links to other data

-   No link from the paragraph to the file.
-   accessWriteLink from record-data STUDENT2-FILE to COBOL File
    Link object STUDENT-2 will be created. In a real-world project, this
    code should include some MOVE statements from the data to record
    data before actioning an implicit WRITE/REWRITE.

![](image2022-2-3_21-54-12.png)

##### Save data and links to to sections and paragraphs

-   accessWriteLink from paragraph to file STUDENT2 will be
    created.
-   accessReadLink from paragraph to file STUDENT2-FILE will be
    created.

![](image2022-2-3_21-55-12.png)
##### Save data and links to sections/paragraphs/other data
- all links aboved will be created.

##### Other AIP Console options

With all other AIP Console options only accessWriteLink links will
be created from the paragraph to the COBOL File Link.

#### WRITE/REWRITE RECORD-DATA FROM WS-DATA

The result of a WRITE/REWRITE statement execution with the FROM
WS-DATA phrase is equivalent to the execution of the following
statements in the order specified:

``` java
MOVE WS-DATA TO RECORD-DATA. WRITE RECORD-DATA.
```

Example: 

``` java
000010 DATA DIVISION.
000010    FILE SECTION.
000010    FD STUDENT3.
000010    01 STUDENT3-FILE.
000010       05 STUDENT3-ID PIC 9(5).
000010       05 NAME PIC A(25).
000010 
000010    WORKING-STORAGE SECTION.
000010    01 WS-STUDENT3.
000010       05 WS-STUDENT3-ID PIC 9(5).
000010       05 WS-NAME3 PIC A(25).
000010    01 WS-EOF PIC A(1). 
000010 
000010 PROCEDURE DIVISION.
000010    OPEN EXTEND STUDENT3.
000010       MOVE 1000 TO WS-STUDENT3-ID.
000010       MOVE 'Tim' TO WS-NAME3.
000010       WRITE STUDENT3-FILE FROM WS-STUDENT3
000010       END-WRITE. 
000010    CLOSE STUDENT3.
000010 STOP RUN.
```

Result for each option in AIP Console:

##### Save data and links to others data

-   No link from paragraph to file with this option.
-   accessWriteLink from ws-data WS-STUDENT3 to
    record-data STUDENT3-FILE will be created. 
-   accessWriteLink from record-data STUDENT3-FILE to COBOL
    File Link object STUDENT-3 will be created. 

![](image2022-2-3_21-56-16.png)

##### Save data and links to to sections and paragraphs

-   accessWriteLink from paragraph to file STUDENT3 will be created.
-   accessWriteLink and accessReadLink from paragraph to
    file STUDENT3-FILE will be created.
-   accessReadLink from paragraph to file WS-STUDENT-3 will be
    created.

![](image2022-2-3_19-14-47.png)
##### Save data and links to sections/paragraphs/other data
- all links aboved will be created.

##### Other AIP Console options

With all other AIP Console options, only an accessWriteLink link is
created from the paragraph to the COBOL File Link object.

### READ

When we read data from a file, the data is pulled from the file
stream and written into the data record associated to the file.

#### READ file-name (Implicit)

This is a COBOL File Link object write to the data record:

``` java
000010 DATA DIVISION.
000010    FILE SECTION.
000010    FD STUDENT4.
000010    01 STUDENT4-FILE.
000010       05 STUDENT4-ID PIC 9(5).
000010       05 NAME PIC A(25).
000010 
000010    WORKING-STORAGE SECTION.
000010    01 WS-STUDENT4.
000010       05 WS-STUDENT4-ID PIC 9(5).
000010       05 WS-NAME4 PIC A(25).
000010    01 WS-EOF PIC A(1). 
000010 
000010 PROCEDURE DIVISION.
000010    OPEN INPUT STUDENT4.
000010       PERFORM UNTIL WS-EOF='Y'
000010          READ STUDENT4
000010             NOT AT END DISPLAY STUDENT4-ID
000010          END-READ
000010       END-PERFORM.
000010    CLOSE STUDENT4.
000010 STOP RUN.
```

Result for each option in AIP Console:

##### Save data and links to others data

-   No link from paragraph to file with this option.
-   accessWriteLink link from COBOL File Link object STUDENT4 to
    record-data STUDENT4-FILE will be created .In a real-world
    project, this code should include some MOVE statements from the
    record data to ws-data after actioning an implicit READ.

![](image2022-2-3_21-59-24.png)

##### Save data and links to to sections and paragraphs

-   accessReadLink link from paragraph to file STUDENT4 will be
    created.
-   accessWriteLink link from paragraph to file STUDENT4-FILE will
    be created.



![](image2022-2-3_22-0-53.png)
##### Save data and links to sections/paragraphs/other data
- all links aboved will be created.

##### Other AIP Console options

With all other AIP Console options, only an accessWriteLink from
the paragraph to the COBOL File Link object.

#### READ file-name INTO WS-DATA

This is a COBOL File Link write to the data record, then
record-data write to ws-data:

``` java
000010 DATA DIVISION.
000010    FILE SECTION.
000010    FD STUDENT.
000010    01 STUDENT-FILE.
000010       05 STUDENT-ID PIC 9(5).
000010       05 NAME PIC A(25).
000010 
000010    WORKING-STORAGE SECTION.
000010    01 WS-STUDENT.
000010       05 WS-STUDENT-ID PIC 9(5).
000010       05 WS-NAME PIC A(25).
000010    01 WS-EOF PIC A(1). 
000010 
000010 PROCEDURE DIVISION.
000010    OPEN INPUT STUDENT.
000010       PERFORM UNTIL WS-EOF='Y'
000010          READ STUDENT INTO WS-STUDENT
000010             AT END MOVE 'Y' TO WS-EOF
000010             NOT AT END DISPLAY WS-STUDENT-ID
000010          END-READ
000010       END-PERFORM.
000010    CLOSE STUDENT.
000010 STOP RUN.
```

Result for each option in AIP Console:

##### Save data and links to others data

-   No link from paragraph to file with this option.
-   accessWriteLink link from COBOL File Link STUDENT to
    record-data STUDENT-FILE will be created.
-   accessWriteLink link from record-data STUDENT-FILE to
    ws-data WS-STUDENT will be created. 

![Alt text](image2022-2-3_19-42-46.png)

##### Save data and links to to sections and paragraphs

-   accessReadLink from paragraph to file STUDENT will be created.
-   accessWriteLink/accessReadLink from paragraph to
    file STUDENT-FILE will be created.
-   accessWriteLink from paragraph to file WS-STUDENT will be
    created.

![](image2022-2-3_22-17-57.png)
##### Save data and links to sections/paragraphs/other data
- all links aboved will be created.

##### Other AIP Console options

With all other AIP Console options, only an accessWriteLink from
the paragraph to the COBOL File Link object is created.

### DELETE

The DELETE statement is used to delete a file. The current behavior is
to create an accessWriteLink link from the paragraph to the
COBOL File Link objects for all options.

``` java
000010 DATA DIVISION.
000010    FILE SECTION.
000010    FD STUDENT6.
000010    01 STUDENT6-FILE.
000010       05 STUDENT6-ID PIC 9(5).
000010       05 NAME PIC A(25).
000010 
000010    WORKING-STORAGE SECTION.
000010    01 WS-STUDENT6.
000010       05 WS-STUDENT6-ID PIC 9(5).
000010       05 WS-NAME6 PIC A(25).
000010    01 WS-EOF PIC A(1). 
000010 
000010 PROCEDURE DIVISION.
000010    OPEN EXTEND STUDENT6.
000010    DELETE STUDENT6.
000010    CLOSE STUDENT6.
000010 STOP RUN.
```

![](image2022-2-3_22-20-54.png)

### START

``` java
START FILE-NAME.
```

This file handling statement is not supported.

## Limitations

### Bookmarks

Currently the Mainframe Analyzer is designed such that the bookmark in
the Cobol File Link object for the accessWriteLink to the Cobol Data
object is not able to point to the exact line in which the data is being
written to the Cobol Data object. Instead the bookmark is placed at the
start of the Cobol File Link object.
