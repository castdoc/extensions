---
title: "Log messages"
linkTitle: "Log messages"
type: "docs"
weight: 9
---

## Mainframe.01

|                 |                                                                                                                                                                                                   |
|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.01                                                                                                                                                                                      |
| Message     | A potential recursive copybook '\<COPYBOOK_NAME\>' inclusion has been found.                                                                                                                      |
| Severity    | Warning                                                                                                                                                                                           |
| Explanation | The COBOL analyzer detected a potential recursive copybook inclusion (COPY directive) in the source code. Meaning that a copybook A refers to a copybook B that refers to copybook A.             |
| User Action | Check if the copybooks involved in the recursive reference are the expected source files. If not, then replace them by the proper files or remove the file that creates the recursive reference.  |

## Mainframe.02

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Identifier</strong></td>
<td class="confluenceTd">Mainframe.02</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Message</strong></td>
<td class="confluenceTd">Cannot resolve Copybook .</td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Severity</strong></td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Explanation</strong></td>
<td class="confluenceTd">A copybook is referenced (COPY directive) in
the source code but cannot be found by the analyzer. This can occurs
when source files have not been delivered or when the source files are
not accessible by the analyzer. </td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>User Action</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Select the object &lt;COPYBOOK_NAME&gt; in the list of objects in
the Unknown\Copybooks folder of the technical browser in CAST Enlighten
to see where it is referenced. If this is an application specific
copybook, then it is recommended to ask the application team for it.
This could affect reference resolution and any diagnostic rules you
might want around data components.</p>
<p>You will find the complete list of missing source elements at the end
of the analysis log.</p>
</div></td>
</tr>
</tbody>
</table>

## Mainframe.03

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Identifier</strong></td>
<td class="confluenceTd">Mainframe.03</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Message</strong></td>
<td class="confluenceTd">Ambiguous resolution of &lt;OBJECT_TYPE&gt;
'&lt;OBJECT_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Severity</strong></td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Explanation</strong></td>
<td class="confluenceTd"><p>The analyzer found a source file or an
object with a name containing characters like '[', ']', '(', or ')'.
These characters are removed leading to duplicate names. This message
can be also generated when the analysis option "Platform: IBM z/OS" is
set to true and the source code comes from another platform that allows
using long names. In that case, names are truncated to 8 characters
leading to duplicated names.<br />
Having duplicate names prevents to resolve references.</p></td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>User Action</strong></td>
<td class="confluenceTd">Check the source files and the analysis
options. </td>
</tr>
</tbody>
</table>

## Mainframe.04

|                 |                                                                                                                                                                                                                                                 |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.04                                                                                                                                                                                                                                    |
| Message     | The picture \<PICTURE\> may not be analysed properly.                                                                                                                                                                                           |
| Severity    | Warning                                                                                                                                                                                                                                         |
| Explanation | The COBOL analyzer is not able to parse a variable picture correctly. This can affect quality rules related to data handling.                                                                                                                   |
| User Action | Check the source code containing the variable declaration. If the picture is correct, then please contact the [CAST support](https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support).  |

## Mainframe.05

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Identifier</strong></td>
<td class="confluenceTd">Mainframe.05</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Message</strong></td>
<td class="confluenceTd">Cannot resolve included file
'&lt;INCLUDE_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Severity</strong></td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Explanation</strong></td>
<td class="confluenceTd">A copybook is referenced (INCLUDE directive) in
the source code but cannot be found by the analyzer. This can occurs
when source files have not been delivered or when the source files are
not accessible by the analyzer. </td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>User Action</strong></td>
<td class="confluenceTd"><p>Select the object in the list of objects in
the Unknown\Copybooks folder of the technical browser in CAST Enlighten
to see where it is referenced. If this is an application specific
copybook, then it is recommended to ask the application team for it.
This could affect reference resolution and any quality rules you might
want around data components.</p>
<p>You will find the complete list of missing source elements at the end
of the analysis log.</p></td>
</tr>
</tbody>
</table>

## Mainframe.06

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Identifier</strong></td>
<td class="confluenceTd">Mainframe.06</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Message</strong></td>
<td class="confluenceTd">Unclosed string is found.</td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Severity</strong></td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Explanation</strong></td>
<td class="confluenceTd">An alphanumeric literal has been found in the
source code but the COBOL analyzer is not able to the end of the
string. </td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>User Action</strong></td>
<td class="confluenceTd"><p>Open the source file. Check if it contains
COBOL code.<br />
If yes, then verify if the Starting Column analysis parameter is set
properly. It is possible to have copybooks that do not have the same
Starting column than calling programs. In this case, adapt the copybook
to the expected format.<br />
If no, then remove file from the analyzed folder and exclude it from
source selection. Generally, this means that source code from other
technology than Cobol has been delivered in the folder. If you select
source code via directories, then the analyzer is going to take into
account all files belonging to this directory.</p></td>
</tr>
</tbody>
</table>

## Mainframe.07

|                 |                                                                                                    |
|-----------------|----------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.07                                                                                       |
| Message     | The file '\<FILE_NAME\>' contains invalid '0' characters replaced by '#'.                          |
| Severity    | Information                                                                                        |
| Explanation | A binary character has been find in the source code and replaced by a text character.              |
| User Action | No action to perform. The binary character has been replaced automatically by an ASCII character.  |

## Mainframe.08

|                 |                                                                                                                                                                |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.08                                                                                                                                                   |
| Message     | The advanced user configuration file is not found at specified location. As a consequence, JCL files could not be analyzed. Please check the analysis options: |
| Severity    | Error                                                                                                                                                          |
| Explanation | The "AdvancedJCL.xml" file has not been found in the folder specified in the analysis settings. This prevents the analyzer to parse JCL source files.          |
| User Action | Review the Mainframe technology "Platform settings" to specify the correct location of the AdvancedJCL.xml file and restart the analysis.                      |

## Mainframe.09

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Identifier</strong></td>
<td class="confluenceTd">Mainframe.09</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Message</strong></td>
<td class="confluenceTd">Cannot resolve &lt;OBJECT_TYPE&gt;
'&lt;OBJECT_NAME&gt;'. </td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Severity</strong></td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Explanation</strong></td>
<td class="confluenceTd">The analyzer encountered a reference to a
source code element (for example: a COBOL program, a JCL procedure, a
CICS map, a CICS Transient Data item etc.…) but cannot find this item in
the analysis folder. </td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>User Action</strong></td>
<td class="confluenceTd"><p>Select the object &lt;OBJECT_NAME&gt; in the
list of objects in the Unknown\&lt;OBJECT_TYPE&gt; folder of the
technical browser in CAST Enlighten to see where it is referenced. If
this is an application specific element, then it is recommended to ask
the application team for it. It can affect any quality rules.</p>
<p>If the object is a program, it can be called through its PROGRAM-ID
name (this is the most frequent case) which is different from its source
file name. In this case you must search for it by using a GREP tool.</p>
<p>If the message is about a reference to a DD name, then check if there
is a JCL step calling the program and defining a DD card with the same
DD name. If there is no such JCL element and if there is a supported
batch part, then ask the application team for it. Note that the
resolution of this type of links is not supported in case the program
for which the message has been emitted is a subprogram and you can
ignore that message. If the program is a main batch program and there is
a JCL step that calls it, then please contact the CAST Support.</p>
<p>You will find the complete list of missing source elements at the end
of the analysis log.</p></td>
</tr>
</tbody>
</table>

## Mainframe.10

|                 |                                                                                                                                                                                                                                                                                     |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.10                                                                                                                                                                                                                                                                        |
| Message     | Potential comment is found.                                                                                                                                                                                                                                                         |
| Severity    | Warning                                                                                                                                                                                                                                                                             |
| Explanation | The analyzer detected a potential comment line in the COBOL source code. This can be the consequence of an improper indicator area column and the '\*' character has not been found at the expected location.                                                                       |
| User Action | Check the source code and the "Column of the Indicator area" analysis option. You can also check if the corresponding line of code comes from a copybook. In that case, change the indentation of the copybook lines to match the indicator area specified in the analysis option.  |

## Mainframe.11

|                 |                                                                                                                                                                                                             |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.11                                                                                                                                                                                                |
| Message     | Reference not found for DSN clause in DD card.                                                                                                                                                              |
| Severity    | Warning                                                                                                                                                                                                     |
| Explanation | The JCL analyzer found a DD card referring to another DD card (its name contains a prefix corresponding to another step in the same job or in an external procedure) but is not able to find that DD card.  |
| User Action | Check if the JCL code containing the referred DD card has been delivered and is part of the analysis scope. It could be the case for JCL external procedures.                                               |

## Mainframe.12

|                 |                                                                                                                                                                                         |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.12                                                                                                                                                                            |
| Message     | Analyzing \<FILE_TYPE\> \<OBJECT_NAME\> (\<FILE_NO\>/\<FILE_NB\>.                                                                                                                       |
| Severity    | Information                                                                                                                                                                             |
| Explanation | The Mainframe Analyzer started to analyze the file. It also displays the file number \<FILE_NO\> compare to the total number of files \<FILE_NB\> of type \<FILE_TYPE\> to be analyzed. |
| User Action | Nothing to do.                                                                                                                                                                          |

## Mainframe.13

|                 |                                                                                         |
|-----------------|-----------------------------------------------------------------------------------------|
| Identifier  | Mainframe.13                                                                            |
| Message     | Invalid option value : \<ANALYSIS_OPTION\>.                                             |
| Severity    | Error                                                                                   |
| Explanation | An analysis option has not been set correctly. This prevents to perform the analysis.   |
| User Action | Review the analysis settings and restart the analysis.                                  |

## Mainframe.14

|                 |                                                                                                                                                                                                                      |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.14                                                                                                                                                                                                         |
| Message     | Potential mismatch between the program and the PSB '\<PSB_NAME\>'. The PCB number \<NUMBER\> has not been found.                                                                                                     |
| Severity    | Warning                                                                                                                                                                                                              |
| Explanation | The number of PCBs in PSB and in COBOL program are different. This can lead to wrong links between COBOL code and IMS PCB.                                                                                           |
| User Action | Check the PCB list in the PSB and the parameters specified in the PROCEDURE division and the ENTRY DLITCBL or in the LINKAGE SECTION in the COBOL program. Both must have the same number of PCBs in the same order. |

## Mainframe.15

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Identifier</strong></td>
<td class="confluenceTd">Mainframe.15</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Message</strong></td>
<td class="confluenceTd">Cannot analyse &lt;LANGUAGE_ELEMENT&gt; for the
moment.</td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Severity</strong></td>
<td class="confluenceTd">Information</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Explanation</strong></td>
<td class="confluenceTd">The COBOL analyzer does not support Object
Oriented version of the COBOL programming language and no objects will
be created from source code. </td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>User Action</strong></td>
<td class="confluenceTd"><p>Remove corresponding source files from the
source files folder.<br />
Note that the list of supported COBOL version is available in the
documentation.</p></td>
</tr>
</tbody>
</table>

## Mainframe.16

|                 |                                                                                                                                                                                                   |
|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | Mainframe.16                                                                                                                                                                                      |
| Message     | A potential recursive INCLUDE '\<INCLUDE_NAME\>' inclusion has been found.                                                                                                                        |
| Severity    | Warning                                                                                                                                                                                           |
| Explanation | The COBOL analyzer detected a potential recursive copybook inclusion (INCLUDE directive) in the source code. Meaning that a copybook A refers to a copybook B that refers to copybook A.          |
| User Action | Check if the copybooks involved in the recursive reference are the expected source files. If not, then replace them by the proper files or remove the file that creates the recursive reference.  |

## Mainframe.17

|                 |                                                                                                                                            |
|-------|-----------------------------------------------------------------|
| Identifier  | Mainframe.17                                                                                                                               |
| Message     | Ambiguous resolution for copybook '\<INCLUDE_NAME\>' in the folders.                                                                       |
| Severity    | Warning                                                                                                                                    |
| Explanation | The COBOL analyzer detected more than two copybooks with the same name when expanding. The first copybook will be selected.                |
| User Action | Check why there are multiple copybooks with the same name in the project and verify if the selected copybook is correct in the Cobol file. |
