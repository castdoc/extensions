---
title: "Technical notes"
linkTitle: "Technical notes"
type: "docs"
weight: 4
---

>Note: The embedded Mainframe Analyzer available in AIP Core is
deprecated (no updates/bug fixes will be applied) in AIP Core ≥
8.3.26. Please ensure that are using the [Mainframe
Analyzer](Mainframe_Analyzer) extension for the best results.

## Support for JCL Datasets

The Mainframe Analyzer is able to detect the
following specific types of JCL Dataset, which will be visible in
CAST Enlighten, CAST Imaging, Architecture Checker and CAST Transaction Configuration:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Dataset type</th>
<th class="confluenceTh">Code example</th>
</tr>
&#10;<tr class="odd">
<th class="confluenceTh"><p>GDG datasets</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This type of file is identified in JCL DD cards. Its name ends with
"(n)" where n is a negative, zero, or positive number.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>//GDGFIL DD DSN=MYTELCO.COBDYNSQ.GDG(+1)</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh"><p>PDS datasets</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This type of file is identified in JCL DD cards. Its name ends with
the reference to a member enclosed in parentheses.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>//SYSTSIN  DD  DSN=MY.FILE.PDS(MBR)</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh"><p>DBD datasets</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This type of file can be identified in DBD files, PSB files, and in
JCL files:</p>
<ul>
<li>In a DBD file, the DATASET macro identifies the DD name associated
to the data set (DD1 attribute).</li>
<li>The PSB file identifies which DBD is referenced.</li>
<li>In the JCL file, the STEP that calls the PSB defines a DD card with
the name mentioned in the DBD file.</li>
</ul>
<p><strong>DBD</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>DBD NAME=DBDCOMP1,ACCESS=HISAM
DATASET DD1=CASTCMP1,DEVICE=3380</code></pre>
</div>
</div>
<p><strong>PSB</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>PCB TYPE=DB,NAME=DBDCOMP1,KEYLEN=3,PROCOPT=A,PCBNAME=DEPTPCB</code></pre>
</div>
</div>
<p><strong>JCL</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>//CASTCMP1 DD DSN=MYTELCO.IMSHISAM.DATA</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh"><p>GSAM datasets</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>GSAM datasets can be identified in JCL DD card and in PSB files.In
PSB files, PCBs associated to a GSAM file are typed GSAM and the name of
the DD card that references the dataset is in the NAME attribute. This
name must be matched to the DD card with the same name in the STEP that
calls the PSB.</p>
<p><strong>PSB</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>PCB TYPE=GSAM,NAME=GSASEQ,PROCOPT=A,PCBNAME=GSAPCB</code></pre>
</div>
</div>
<p><strong>JCL</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>//GSASEQ DD DSN=MYTELCO.IMSGSAM.DATA</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh"><p>VSAM datasets</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>A VSAM file can be identified in COBOL programs. In ENVIRONMENT
DIVISION / INPUT-OUTPUT SECTION / FILE-CONTROL, logical files are
declared with the INDEXED organization. The JCL Dataset object
associated to a Cobol File Link object that has the indexed organization
is sub-typed as "VSAM". The dataset is defined with IDCAMS utility and
VSAM command in SYSIN DD Card are also VSAM datasets (see <strong><a
href="#support-for-vsam-files">Support for VSAM files</a></strong>
for more information).</p>
<p><strong>COBOL</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>SELECT EMPDATA ASSIGN TO VSAMF
ORGANIZATION IS INDEXED
ACCESS IS SEQUENTIAL
RECORD KEY IS EMPDATA-EMPNO.</code></pre>
</div>
</div>
<p><strong>JCL</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>//VSAMF DD DSN=MYTELCO.EMPDATA.VSAM</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh"><p>Temporary datasets</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>A temporary data set is a data set that is created and deleted in the
same job, and is identified by coding one of the following:</p>
<p><strong>For a temporary data set</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>DSNAME=&amp;&amp;dsname</code></pre>
</div>
</div>
<p><strong>For a member of a temporary PDS or PDSE</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>DSNAME=&amp;&amp;dsname(member)</code></pre>
</div>
</div>
<p><strong>For a temporary data set to be named by the
system</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>No DSNAME parameter</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Support for IMS/DC

See [Mainframe - IMS DC support](../ims-dc/).

## Support for IBM MQSeries

The Mainframe Analyzer supports the publisher/subscriber mode and
point-to-point mode for IBM MQSeries. Publisher/Subscriber
objects will be generated and Call links between Cobol objects and IBM
MQ objects and between IBM MQ objects and Cobol objects will be
generated by the [Web Services Linker extension](../../multi/com.castsoftware.wbslinker/) - you must ensure that v. ≥ 1.6.8
of this extension is installed, otherwise no links will be generated.

### Main supported patterns

#### Point-to-point: MOVE MQOT-Q TO MQOD-OBJECTTYPE

In point-to-point, message producers are called senders and message
consumers are called receivers. Senders produce messages to a queue,
and receivers asynchronously consume messages from that queue. For
point-to-point to work, connected applications need to know the name of
the queue through which they interact. A queue could have a single
receiver or be scaled to many, in which case each message is only
consumed by one receiver, distributing the workload across them
all. This style of messaging is built around the concept of message
queues, and around the processing of messages by a single consumer.

#### Publisher/Subscriber: MOVE MQOT-TOPIC-Q TO MQOD-OBJECTTYPE

The producer of a message is known as a publisher and message
consumers are known as subscribers. The publishing and subscribing
applications within the infrastructure agree on the name of a topic. For
a consumer to signal that they want to receive messages on that topic,
they create a subscription to it. Any message published to a topic will
be delivered to all the subscriptions, so every subscriber gets a copy.
There could be many publishers and many subscribers to each topic, MQ
handles the delivery of messages between all of them. For example:

![](346751141.jpg)

### Support for links between different technologies

The [Web Services Linker extension](../../multi/com.castsoftware.wbslinker/) will take
all IBM MQ publisher and IBM MQ subscriber (of all technologies). Then
it will check if they have the same:

-   queue manager connection.
-   queue name
-   at least one topic from the topic list. 

For example:

-   Publisher TOTO publishes the topics (A, B, C)
-   Subscriber TOTO subscribes to the topics (A, D, E) with the same
    queue manager so in this case a link between publisher and
    subscriber is created.

### API support

The Mainframe Analyzer supports MQI calls and dynamic calling of IBM MQ,
see: [https://www.ibm.com/docs/en/ibm-mq/8.0?topic=zos-dynamically-calling-mq-stub](https://www.ibm.com/docs/en/ibm-mq/8.0?topic=zos-dynamically-calling-mq-stub).

-   Batch environment: 
    -   CONNECT: CSQBCONN, CSQBCONNX
    -   OPEN: CSQBOPEN
    -   GET: CSQBGET, CSQBSUB
    -   PUT: CSQBPUT, CSQBPUT1
-   CICS environment: 
    -   CONNECT: CSQCCONN, CSQCCONNX
    -   OPEN: CSQCOPEN
    -   GET: CSQCGET, CSQCSUB
    -   PUT: CSQCPUT, CSQCPUT1
-   IMS environment: 
    -   CONNECT: MQCONN, MQCONNX
    -   OPEN: MQOPEN
    -   GET: MQGET, MQSUB
    -   PUT: MQPUT, MQPUT1

### Automatic support of CMQODV copy book IBM MQ

IBM provides some default .cpy files for object queue name variables,
topic name variables or queue manager name variables and sometimes these
copybooks are not included in source code deliveries. Note that the
file CMQODV is automatically supported.

### Example basic Publisher/Subscriber

Publish a topic:

``` java
01 OBJECT-DESCRIPTOR.
    COPY CMQODV.
MOVE MQOT-TOPIC TO MQOD-OBJECTTYPE.
MOVE MQOD-VERSION-4 TO MQOD-VERSION.
MOVE ‘Fruit/Vegetal’ TO TARGET-TOPIC.
SET MQOD-OBJECTSTRING-VSPTR TO ADDRESS OF TARGET-TOPIC.
MOVE 18 TO MQOD-OBJECTSTRING-VSLENGTH.
...
DD MQOO-OUTPUT
        MQOO-FAIL-IF-QUIESCING GIVING OPTIONS.

CALL 'MQOPEN' USING HCONN,
              OBJECT-DESCRIPTOR,
              OPTIONS,
              Q-HANDLE,
              MQCC, 
              REASON.

CALL 'MQPUT' USING HCONN, 
            Q-HANDLE, 
            MESSAGE-DESCRIPTOR, 
            PMOPTIONS, 
            BUFFER-LENGTH, 
            BUFFER, 
            MQCC, 
            REASON.
```

Subscribe to a topic:

``` java
01 SUB-DESCRIPTOR.
    COPY CMQSDV.
ADD MQSO-CREATE MQSO-MANAGED MQSO-FAIL-IF-QUIESCING
                                    GIVING MQSD-OPTIONS.
MOVE ‘Vegetal/Meal’ TO TARGET-TOPIC.
SET MQSD-OBJECTSTRING-VSPTR TO ADDRESS OF TARGET-TOPIC.
MOVE 18 TO MQSD-OBJECTSTRING-VSLENGTH.
...
CALL 'MQSUB' USING HCONN,
            SUB-DESCRIPTOR,
            Q-HANDLE,
            SUB-HANDLE,
            MQCC,
            REASON.

CALL 'MQGET' USING HCONN,
            Q-HANDLE,
            MESSAGE-DESCRIPTOR,
            GMOPTIONS,
            BUFFER-LENGTH,
            BUFFER,
            DATA-LENGTH,
            MQCC,
            REASON.
```

Result in CAST Enlighten:

![](614826004.png)

Some main properties are available:

![](614826003.png)

### Request/Reply method

The Request/Reply is described by IBM
here: https://www.ibm.com/docs/en/was/8.5.5?topic=server-request-reply-messaging-using-websphere-mq. Just
as in the send-and-forget pattern sample, this request/reply sample
contains two programs. The first one sends a request message to a queue
(the REQUEST queue) and waits for a response in another queue (the REPLY
queue). The second program acts as the replier and it starts reading
messages from a queue (the REQUEST queue). Whenever a message is put
onto that queue, it sends a generic response to the REPLY queue.

Example:

Definition of REQUEST/REPLY Queue name 

``` js
049833 01 WS-MQ-FIELDS.
049834 03 WS-PGM1 PIC X(8) VALUE SPACES.
049835 03 WS-MQ-HCONN PIC S9(9) BINARY.
049836 03 WS-OPEN-OPTIONS PIC S9(9) BINARY.
049837 03 WS-MQ-HOBJ PIC S9(9) BINARY.
049838 03 WS-MQ-RC PIC S9(9) BINARY.
049839 03 WS-MQ-RC-DISPLAY PIC 9(18).
049840 03 WS-MQ-REASON PIC S9(9) BINARY.
049850 03 WS-MQ-REASON-DISPLAY PIC 9(18).
049900 03 WS-QUEUE-MANAGER PIC X(48) VALUE 'MGT0'.
049900 03 WS-QUEUE-MANAGER PIC X(48) VALUE 'MGP0'.
050000 03 WS-REQUEST-QUEUE-NAME PIC X(48) VALUE
050100 'TE.CDS.REQUEST'.
050200 03 WS-REPLY-QUEUE-NAME PIC X(48) VALUE
050300 'TE.CDS.REPLY'.
```

The Cobol program (publisher) OPENS the REQUEST queue and PUT the
message:

``` js
236500 3330-MQ-PUT-PROCESS.
236600*
236700* THIS PARA PERFORMS THE REQUEST QUEUE CONNECTION AND ALSO THE
236800* DATA FROM THE TECMQSTU COPYBOOK TO THE CDS.
236900*
237000
237100     PERFORM 3331-OPEN-REQUEST-QUEUE
237200        THRU 3331-EXIT
237300
237400     PERFORM 3332-PUT-MESSAGE
237500        THRU 3332-EXIT
237600
237700     PERFORM 9100-CLOSE-REQUEST-QUEUE
237800        THRU 9100-EXIT.
237900
238000 3330-EXIT.
238100     EXIT.
238200
238300 3331-OPEN-REQUEST-QUEUE.
...
241000     MOVE  WS-REQUEST-QUEUE-NAME      TO TEMQPUT-REPLYTO-QUEUE
...
241900     MOVE TEMQPUT-REPLYTO-QUEUE       TO MQOD-OBJECTNAME OF
242000                                         MQM-OBJECT-DESCRIPTOR
...
241000     MOVE  WS-REQUEST-QUEUE-NAME      TO TEMQPUT-REPLYTO-QUEUE
...
241900     MOVE TEMQPUT-REPLYTO-QUEUE       TO MQOD-OBJECTNAME OF
242000                                         MQM-OBJECT-DESCRIPTOR
...
246200 3332-PUT-MESSAGE.
246300*
246400* THIS PARA PUTS THE MESSAGE INTO THE REQUEST QUEUE.
246500*
...
247900     MOVE TEMQPUT-REPLYTO-QUEUE       TO MQMD-REPLYTOQ OF
248000                                         MQM-MESSAGE-DESCRIPTOR
248100     MOVE TEMQPUT-REPLYTO-QMGR        TO MQMD-REPLYTOQMGR OF
248200                                         MQM-MESSAGE-DESCRIPTOR
...
249900     CALL 'MQPUT'        USING TEMQPUT-HCONN
250000                               TEMQPUT-HOBJ
250100                               MQMD OF MQM-MESSAGE-DESCRIPTOR
250200                               MQPMO OF PM-OPTIONS-O
250300                               TEMQPUT-PUT-DATA-SIZE
250400                               TEMQPUT-PUT-DATA
250500                               WS-MQ-RC
250600                               WS-MQ-REASON
```

The Cobol Program (subscriber) tries to get the reponse from the
request (PUT). Then the response message could be resent to the
publisher by REPLY Queue:

GET behavior from REQUEST Queue

``` js
252700 3340-MQ-GET-PROCESS.
252800*
252900* THIS PARA PERFORMS THE REPLY QUEUE CONNECTION AND ALSO GETS THE
253000* REPLY MESSAGE FROM THE CDS IN TECMQSTU COPYBOOK.
253100*
253200
253300     PERFORM 3341-OPEN-REPLY-QUEUE
253400        THRU 3341-EXIT
253500
253520     DISPLAY '=> MQGET CORREL ID => ' WS-MSGID
253600     PERFORM 3342-GET-MESSAGE
253700        THRU 3342-EXIT UNTIL WS-END-OF-PROCESSING
253800
253900     PERFORM 9200-CLOSE-REPLY-QUEUE
254000        THRU 9200-EXIT.
254100
254200 3340-EXIT.
254300     EXIT.
254400
254500 3341-OPEN-REPLY-QUEUE.
...
257200     MOVE  WS-REPLY-QUEUE-NAME        TO MQOD-OBJECTNAME OF
257300                                         MQM-OBJECT-DESCRIPTOR
```

#### Supported behavior

The Mainframe Analyzer supports the main REQUEST queue, however
publisher and subscriber items defined by the REPLY queue will not be
created by the analyzer.

#### Limitations

-   When complex situations arise (i.e. due to the use of if_else,
    evaluate, when etc.), results may be ambiguous and
    potentially erroneous.
-   If the queue names do not detect the request and reply
    queues, the analyzer cannot correctly detect the REQUEST
    Publisher/Subscriber queue name. Note that the analyzer attempts
    to detect items using REQUEST/REPLY keywords and if none are
    found, all publisher/subscribers are created (no REPLY
    publishers/subscribers are removed).

### Unknown publisher/Unknown subscriber

When the analyzer cannot find the correct resolution between objects,
the unknown publisher and unknown subscriber objects will be
created, including the ID of the item. Note however that no links are
created between unknown publisher and unknown subscriber objects.

![](614826005.png)

### Objects

MQ Publisher and MQ Subscriber objects are created by the Mainframe
Analyzer:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="346751144.png" class="image-center"
draggable="false" data-image-src="346751144.png"
data-unresolved-comment-count="0" data-linked-resource-id="346751144"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_COBOL_MQ_Subscriber.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="268371456"
data-linked-resource-container-version="17" width="32" /></p>
</div></td>
<td class="confluenceTd">Subscriber IBM MQ</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><br />
</p>
</div>
<img src="346751143.png" class="image-center"
draggable="false" data-image-src="346751143.png"
data-unresolved-comment-count="0" data-linked-resource-id="346751143"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_COBOL_MQ_Publisher.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="268371456"
data-linked-resource-container-version="17" width="32" /></td>
<td class="confluenceTd">Publisher IBM MQ</td>
</tr>
</tbody>
</table>

### Links

Call links will be generated by the [Web Services Linker extension](../../multi/com.castsoftware.wbslinker/) - you must ensure that v. ≥ 1.6.8 of
this extension is installed, otherwise no links will be generated:

-   between Cobol objects and IBM MQ objects
-   between IBM MQ objects and Cobol objects

## Support for VSAM files

-   The analyzer will detect VSAM files in COBOL programs, just like
    other files, however, no specific object type for VSAM will be
    resolved in the results. Instead, VSAM files are resolved as follows
    (see [Mainframe - Analysis results](../results/)):  
    -   COBOL: File Link objects 
    -   JCL: Dataset objects
-   Some structural rules have been adapted to take into account this
    VSAM files, for example:
    -   [Avoid using SORT -
        5058](https://technologies.castsoftware.com/rules?s=5058%7Cqualityrules%7C5058)

    -   [Avoid using MERGE -
        5060](https://technologies.castsoftware.com/rules?s=5060%7Cqualityrules%7C5060)

    -   [File descriptor block must be defined with 0 record -
        7366](https://technologies.castsoftware.com/rules?s=7366%7Cqualityrules%7C7366)
-   The analyzer does not analyze the VSAM system tables that contain
    technical information about file system management.
-   Support for commands in "SYSIN" clauses, for
    example:
    -   ALLOCATE
    -   ALTER
    -   DEFINE
    -   DELETE
    -   EXAMINE
    -   LISTALC
    -   LISTCAT
    -   LISTDS
    -   PRINT
    -   REPRO
    -   VERIFY
-   Support for If IDCAMS utility and VSAM data-set types (for Cobol and
    JCL) when they call indexed, relative and sequential organisation:
    -   Entry-sequenced data set (ESDS)

    -   Key-sequenced data set (KSDS)

    -   Relative-record data set (RRDS)

    -   Linear data set (LDS) (since 1.0.14-funcrel)
-   Support since Mainframe Analyzer 1.0.14 funcrel:
    -   Support to abbreviations command VSAM e.g:
        -   DEFINE is DEF
        -   CLUSTER is CL
        -   EXPORT is EXP, ...
    -   By default, the VSAM data-set types is KSDS
    -   Support property for KEYS in KSDS VSAM

## File Management

-   The analyzer handles programs in PC file type format. However, in
    Mainframe, the structure of file/folder trees is not managed in the
    same way.
-   The notion "Library" is not taken into account.
-   When program names are identical, these programs may be ignored or
    may be incorrectly resolved.

## Resolution

### Inference Engine

The Inference Engine is implemented for the Mainframe Analyzer, but does
have some limitations:

-   Renaming (66) is not supported.
-   Indexation of arrays, i.e.: the instructions MOVE with
    (position:length) is not supported
-   The length of a variable when given by a constant (declared as 78 -
    Microfocus) is not supported.
-   The Inference Engine will remain local to each program.

### Creation of unknown objects

Given the following code:

``` java
IF condition A
<do this>
END-IF

IF condition B
<do that>
END-IF
```

The Inference Engine will consider that \<do this\> and \<do that\> can
be done in the same control flow path although this is not actually
possible. This leads to the situation where the analyzer will generate
incorrect subprogram names and therefore an unexpected unresolved object
will also be generated, that do not exist anywhere in the Application
source code.

### EXHIBIT Instruction

No links are created to variables handled by the instructions EXHIBIT.

## Embedded SQL for Oracle Users

Calls to tables, views, synonyms, procedures, functions, sub-functions
and package sub-procedures are all resolved. Calls to any other SQL
object are not.

## Analysis of Copybooks / Option "Save Copybook Structure"

-   A Copybook is analyzed implicitly when analyzing the Cobol program
    including it. You just have to make sure that the copybook is
    contained in one of the Working Folders defined in the Analysis Unit
    / Application. However, if you want to save the data structure
    located under a copybook, you need to activate the option "Save data
    found in copy books". Please be aware that in this case the copybook
    will be analyzed without performing any text replacements.

-   Copybooks containing characters that are not authorized in Cobol
    identifiers can cause issues if they have been selected in the Cobol
    page and if the "Save Copybook Structure" option has been selected.
    This limitation occurs when a copybook is included via the COPY
    REPLACING statement and if it contains replacement patterns such as
    '&-' (including the quote characters). In this specific case, the
    Cobol analyzer cannot extract the Cobol identifier correctly and so
    it cannot create the corresponding data structure object. However,
    the Cobol analyzer expands the Cobol programs correctly: the
    replacement patterns are replaced and the expanded code is correct.

## Cobol literals

If a literal is modified between two analyses and if the "Save Data
Structures" option is selected then the old version of the literal is
not actually removed from the knowledge base.

## DSN copied from earlier DD card

The JCL analyzer can not create a link from a step to a data set if the
DD card copies a DSN clause from an earlier DD card. For instance, when
the following DD cards are analyzed they will not generate a link from
the step to the dataset:

``` text
//STEP1 EXEC PGM=MYPROG1
//MYDD1 DD DSN=MY.DATA.SET
//MYDD2 DD DSN=*.MYDD01
//STEP2 EXEC PGM=MYPROG2
//MYDD3 DD DSN=*.STEP1.MYDD1
```

## JCL symbols

CAST AIP provides partial support for the resolution of symbols in JCL.
The following is supported:

-   JCL symbols in procedure EXEC cards - links between procedure steps
    and programs/procedures where a JCL symbol is used
-   JCL symbols in procedure DD cards
-   JCL Symbols present in job DD cards 
-   JCL Symbols present in job EXEC cards 
-   Where JCL contains PROCS using parameters from a different JCL
-   Where JCL calls PROC using a parameter
-   JCL Symbols present in external PROCS

In other words:

-   Text assigned to symbols will be extracted (e.g: "// SET
    MYVAR=VALUE")
-   When symbols are replaced with their value (e.g.: "//STEP1 EXEC
    PGM=&MYPGM")
-   Propagation via PROC calls
    -   Calling card example: "//STEP1 EXEC MYPROC, PROG=MYPROG"
    -   Default value assignment example: "//STEP1 PROC, PROG=MYPROG"

## Links between JCL and JEE technologies

The Mainframe Analyzer extension supports links
from JCL Steps to Java Methods through the creation of a
JCL_To_JavaProgram object. The JCL_To_JavaProgram object is created
when the following utilities are detected in the code:

-   JVMLDM
-   JZOSVM
-   BPXBATCH
-   BPXBATSL

Example code with the JVMLDM utility:

``` text
//LMGHWRLD JOB (MY,ACCOUNTING,INFO),'HELLO WORLD', 
//   CLASS=A,MSGCLASS=H,REGION=0M,NOTIFY=&SYSUID     
//STEP1    EXEC PGM=JVMLDM60,                        
//   PARM='com.foo.bar.HelloWorld'      
//STEPLIB  DD DSN=<MY.LIBRARY.PATH>,DISP=SHR
//SYSPRINT DD SYSOUT=*                               
//SYSOUT   DD SYSOUT=*                               
//STDOUT   DD SYSOUT=*                               
//STDERR   DD SYSOUT=*                               
//STDENV   DD *                                      
. /etc/profile                                       
. ~/.profile                                         
export CLASSPATH=~/HelloWorld                        
for i in ~/HelloWorld/*.jar; do                      
    export CLASSPATH=$i:$CLASSPATH                   
    done                                             
//
```

Result:

![](614826009.png)

  

## Batch utilities using external parameter files

The JCL analyzer can parse, via the AdvancedJCL.xml file (see [Mainframe - Analysis configuration](../analysis-config/#parameterization-for-jcl--advancedjclxml) for more information), the information sent to programs/utilities through a DD
 card. However, if this DD card refers to an external file via the
DSN parameter, then the analyzer cannot parse this information and
create the objects and links. For instance, the following JCL code is
correctly analyzed:

``` text
//JCLTEST JOB (8452200000),'COBTEST',CLASS=M,MSGCLASS=P,USER=JCH
//*
//PS10 EXEC PGM=IKJEFT01,
//STEPLIB DD DSN=ENV.APPL.LOADLIB,
// DISP=(SHR,KEEP,KEEP)
//SYSOUT DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
*//SYSTSIN DD 
DSN SYSTEM(DB2P)
RUN PROGRAM(COBTEST) PLAN(TESTPLAN) PARM('1')
END
//* 
```

And the following JCL code cannot currently be analyzed:

``` text
//JCLTEST JOB (8452200000),'COBTEST',CLASS=M,MSGCLASS=P,USER=JCH
//*
//PS10 EXEC PGM=IKJEFT01,
//STEPLIB DD DSN=ENV.APPL.LOADLIB,
// DISP=(SHR,KEEP,KEEP)
//SYSOUT DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//SYSTSIN DD DSN=ENV.APPL.PARMLIB(PARMTEST),
// DISP=(SHR,KEEP,KEEP)
//*
External parameter file PARMTEST:
DSN SYSTEM(DB2P)
RUN PROGRAM(COBTEST) PLAN(TESTPLAN) PARM('1')
END
```

## Dynamic Call to CBLTDLI/CEETDLI/AIBTDLI

The IMS CBLTDLI/CEETDLI/AIBTDLI subprogram can be called by using a variable (dynamic
call). If the variable is initialized in the DATA DIVISION (VALUE clause
of data declaration) then the analyzer detects and manages the calls to
IMS correctly. Otherwise, if the variable is only assigned in the
PROCEDURE DIVISION (ie: by using a MOVE statement), then the analyzer
does not detect a call to IMS.


## Object sharing

It is possible to create links between objects belonging to different
jobs but it is not possible to share objects between these jobs. This
means that if a source file is analyzed several times in different
Analysis Units, then several instances of objects will be created in the
CAST Analysis Service.  
In addition, copybooks referenced by programs via Working Folders in
different analysis jobs will be duplicated in the CAST Analysis Service.

## Cobol code containing "EXIT SECTION" statements

When analyzing Cobol source code implemented in MICRO Focus COBOL /
AcuCOBOL, the "EXIT SECTION" statement will cause the Mainframe analyzer
to issue the warning "Cannot find section or paragraph". To avoid this
warning, replace the statement "EXIT SECTION" with its short form
"EXIT".

## COPY REPLACING

The Mainframe analyzer does not allow to expand several level of
copybooks (e.g: a copybooks references another copybook) if at level "n"
a REPLACING clause is used to change the name of a referenced copybook
in a COPY directive located at a level n+1. In this specific case, these
copybooks specified at level n+1 are not found.

For example, considering the 3 levels of inclusion:

-   level 1: COPY A REPLACING B BY C.
-   level 2 (copybook A): COPY B.
-   level 3 (copybook C)

The Mainframe Analyzer will search for copybook B (that could be not
found) and C is ignored when it will expand level 2.

## Support for REPLACE statement

The Replace statement (used to replace some keywords in COBOL
programs) is supported in the Mainframe Analyzer
extension ≥ 1.0.10-funcrel.

  

## Paragraphs containing termination statement

If a paragraph containing a termination statement (ex: GOBACK, STOP
RUN...) is called by a PERFORM statement, then the analyzer will not
draw a "Call" link between the paragraph containing the PERFORM
statement and the next paragraph..

Nevertheless, there is a limitation if the termination statement is
located in a conditional block (IF, EVALUATE). In this specific case,
the analyzer will consider that the termination statement is always
executed and the "Call" link between the paragraph containing the
PERFORM statement and the next paragraph will not be drawn.

Example 1:

  

``` text
PARA-1.
    PERFORM PARA-TERMINATION.
PARA-2.
...
PARA-TERMINATION.
   GOBACK.
```

No "Call" link will be drawn between PARA-1 and PARA-2.

Example 2:

``` text
PARA-1.
    PERFORM PARA-TERMINATION.
PARA-2.
...
PARA-TERMINATION.
  IF <condition>
    GOBACK.
```

No "Call" link will be drawn between PARA-1 and PARA-2 (limitation).

This can impact the following quality rules based on paragraph calls:

-   Avoid OPEN/CLOSE inside loops
-   Avoid cyclic calls with PERFORM statements
-   Avoid unreferenced Sections and Paragraphs
-   Avoid GOTO jumps out of PERFORM range

## Cobol Programs without a PROGRAM-ID or a PROCEDURE DIVISION

Cobol Programs (.cob files) that do not contain a PROGRAM-ID
paragraph nor a PROCEDURE DIVISION (for example when a COPY
statement is used to call a COPYBOOK which contains the PROGRAM-ID) will
be considered as copybooks.

## Cobol data links with file handling COBOL statement

See [Cobol data links with file handling cobol statement](../file-handling/).

## Sensitive data in Mainframe Analyzer

Supported in the Mainframe Analyzer extension ≥ 1.0.10-funcrel. The
[Mainframe Sensitive Data extension](https://extend.castsoftware.com/#/extension?id=com.castsoftware.mainframe.sensitivedata&version=latest) is required. Some dataset and IMS Segment will raise an indicator
property if these are a sensitive data.

## Unknown / unresolved objects

An unknown unresolved object generally means that there is a missing
component, directly or not. Usually unknown/unresolved objects are
listed in the analysis log with a "Mainframe.09" warning message:

``` java
Warning MODULMSG ; Job execution Mainframe.09: Cannot resolve Entry or program
```

It is important to distinguish objects which have associated source
files from the other objects which do not have a source file.

For instance, a program, a copybook, a screen map, a JCL job or a JCL
procedure all have an associated source file. If they are involved in a
[log message](../logs/) which indicates they
are unresolved, then you can immediately assume they are missing. On the
contrary, data structures, paragraphs, and DD Names do not have a source
file. Instead they are defined in a program or in a copybook.

So why is the analyzer not able to find a component? Because it has not
been delivered or because the analyzer does not have any information
about its location (source file directories or working folders). In the
first case you must ask the application team to deliver it. You will
find the complete list of missing components at the end of the analysis
log. In the second case you must modify the parameters of the analysis
unit.

You can also see missing components using the CAST Enlighten tool. The
unknown / unresolved objects are classified in "Unknown" folders and you
can easily find them with the Object Browser.

![](572063968.png)

There are technical/system/utility programs which are called by
application programs and JCL jobs. The Mainframe Analyzer can also
produce "Unresolved objects" messages about them. Their name usually
begins with the "DFH", "DSN", "IKJ", "DFS" or "IEB" prefixes. The "DFH"
programs are dedicated to CICS, the "DSN" and "IKJ" programs are usually
dedicated to DB2 and the "DFS" programs are dedicated to IMS. The JCL
job can also invoke utilities such as the IEB family, IEF family or
IDCAMS, FILE-AID… Do not ask the application team to deliver them
because they do not have the corresponding source files. If they are
used to run another program in a JCL job then you should change the
analysis parameters. Otherwise, you can ignore the messages.

Note that Unknown Cobol copybooks are classed as external objects
for transaction configuration purposes: external objects are not
transferred to the Dashboard schema unless the object is an associated
value of a violation, therefore AEP enhancement transaction details will
not show these objects if they are not associated to a violation
(because AEP enhancement values only reflect objects that exist in the
Dashboard schema).

## Cobol Data object Bookmarks

Currently the Mainframe Analyzer is designed such that bookmarks
between Cobol Data object do not point to the exact line in which the
data is being written to the Cobol Data object (for example MOVE
statements) Instead the bookmark is placed at the start of the Cobol
Data object.

