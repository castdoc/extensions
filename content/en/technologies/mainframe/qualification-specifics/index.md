---
title: "Qualification specifics"
linkTitle: "Qualification specifics"
type: "docs"
weight: 1
---

## Introduction

A Mainframe application is often based on several types of components
such as programs, JCL jobs, databases, files, transactions, maps etc.

The Mainframe Analyzer takes into account these components for the IBM
z/OS platform. It can also analyze COBOL programs based on the COBOL-85
standard for other platforms. However, in this case, only COBOL programs
are analyzed and other platform-specific components are not.

For analysis purposes, it is necessary to identify the characteristics
of the COBOL dialect used to develop the programs and if they access any
databases. There are several DBMS and the CAST Mainframe Analyzer covers
only some of them. Moreover, it is useful to find out if the application
has a batch processing part and/or an on-line part (based on
transactions) or not. Indeed, batch processing means that programs are
executed through jobs (implemented in JCL language), logical files are
assigned to physical files and IMS resources are attached to programs.
JCL source files can refer to procedures or include files. If they are
not taken into account, then the analysis will not be complete and
results will be partial. In addition, on-line processing means that
programs can access specific resources and can call each other through
specific commands. If this is also taken into account, it influences the
results for programs in terms of incoming or outgoing links.

On-line programs usually have more "coupling" and, as a result, may be
more sensible than batch programs in robustness point of view. Thus, it
is useful to be able to differentiate batch programs and on-line
programs in order to deliver more accurate information in the resulting
dashboard.

Generally speaking, dividing the application into fine grained modules
means that CAST AIP will provide more value.

The following sections discuss about the source code qualification
process, the various existing mainframe platforms, how the delivered
source code could be organized, and the description of components that
are supported by CAST AIP.

## Multiple mainframe platforms

It is important to understand that there is not only one platform but
several mainframe platforms. The best known is certainly the IBM z/OS
one.

Although each platform generally manages the same type of components
(programs, jobs, transactions, files, databases, etc.), these components
can not be taken into account by the same set of parsers. For instance,
jobs from IBM zSeries computer family are not implemented with the same
language than jobs defined on Unisys A Series computers or Tandem
computers. As such, it is important to know and understand which
platforms (or part of them) can be analyzed by the Mainframe Analyzer
and which ones cannot.

The following tables show the different types of COBOL dialects, job
control languages, transaction managers and DBMS that have already been
encountered and contain information about their support by CAST AIP and
the potential issues that may occur.

### COBOL

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><p>Platform / OS</p></th>
<th class="confluenceTh"><p>COBOL dialect</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td rowspan="2" class="confluenceTd">IBM z/OS</td>
<td class="confluenceTd"><p>Enterprise COBOL for z/OS - up to
v6</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><ul>
<li>No problem</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IBM Cobol for OS/390</p>
VS COBOL II</td>
<td class="confluenceTd">Medium</td>
<td class="confluenceTd"><ul>
<li>Source code implemented with these compilers can be analyzed with
the Mainframe Analyzer</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>IBM i</p></td>
<td class="confluenceTd"><p>COBOL/400</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><ul>
<li>Some constructs related to screens and transactions are not taken
into account but this does not cause problems for the Mainframe Analyzer
nor the Metric Assistant.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Unisys A Series</p></td>
<td class="confluenceTd"><p>Unisys Cobol</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Copybook inclusion mechanism is not supported.</li>
<li>Program calls are not correctly managed.</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Bull DPS7 (GCOS7)</p>
<p>Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>GCOS Cobol</p></td>
<td class="confluenceTd"><p>Low</p></td>
<td class="confluenceTd"><ul>
<li>DML is not supported.</li>
<li>Screens are not supported.</li>
<li>Transactions are not supported.</li>
<li>Some constructs must/should be removed from the source code because
they can cause parsing problems.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Tandem NonStop</p></td>
<td class="confluenceTd"><p>HP COBOL for TNS/E Programs</p></td>
<td class="confluenceTd"><p>Medium</p></td>
<td class="confluenceTd"><ul>
<li>Transactions and screens are not supported.</li>
<li>Some constructs must/should be removed from the source code because
they can cause parsing problems.</li>
<li>CAST rules specific to CICS/IMS (DL/I), file descriptors and
SORT/MERGE may produce false violations.</li>
</ul></td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><p>Unix, AIX, Windows</p></td>
<td class="confluenceTd"><p>Microfocus </p></td>
<td class="confluenceTd"><p>High <br />
<br />
</p></td>
<td rowspan="2" class="confluenceTd"><ul>
<li>Definition and statements associated to screens are not support but
this does not cause problems for the Mainframe Analyzer nor the Metric
Assistant. </li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">ACUCOBOL-GT</td>
<td class="confluenceTd">Medium</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Windows</p></td>
<td class="confluenceTd"><p>Cobol .NET</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Object COBOL is not supported.</li>
<li>It is possible to use a Universal Analyzer profile to extract some
information.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">OpenVMS</td>
<td class="confluenceTd">HP Cobol for OpenVMS</td>
<td class="confluenceTd">Medium</td>
<td class="confluenceTd"><ul>
<li>Copy directive is supported</li>
<li>COBOL 85 syntax is supported</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Stratus VOS</td>
<td class="confluenceTd">VOS Cobol</td>
<td class="confluenceTd">Medium</td>
<td class="confluenceTd"><ul>
<li>Some constructs must/should be removed from the source code because
they can cause parsing problems.</li>
</ul></td>
</tr>
</tbody>
</table>

Note that some AIP quality rules specifically target IBM style
COBOL, therefore you may find that results of these rules may not be
pertinent if you are using a different compiler such as Microfocus.
Rules which target IBM style COBOL usually quote this in the "Reference"
section of the rule documentation, e.g.:

![](597295124.jpg)

### JCL

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><p>Platform (OS)</p></th>
<th class="confluenceTh"><p>Control Language</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>JCL</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IBM i (AS/400)</p></td>
<td class="confluenceTd"><p>CL</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>CL/CLP is supported thanks to <a
href="https://extend.castsoftware.com/#/extension?id=com.castsoftware.rpg&amp;version=latest"
rel="nofollow">IBM RPG Analyzers</a></p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Unisys A Series</p></td>
<td class="confluenceTd"><p>Workflow</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>Workflows are implemented in Algol language
(yes, this language is still used) so it is difficult to analyze it with
a Universal Analyzer profile/extension.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Bull DPS7 (GCOS7)</p>
<p>Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>JCL</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>A Universal Analyzer profile/extension could
be developed to take GCOS8 job into account. This is more difficult for
GCSO7.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Tandem/HP</p></td>
<td class="confluenceTd"><p>JCL</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>Difficult to analyze with a Universal
Analyzer profile/extension.</p></td>
</tr>
</tbody>
</table>

### Transaction Managers

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><p>Transaction Manager</p></th>
<th class="confluenceTh"><p>Platform (OS)</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"><p>CICS</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS) <br />
IBM i (AS/400)</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IMS/DC</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><ul>

<li>Support for:
<ul>
<li><p><a
href="https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_applctn_macro.htm"
style="text-decoration: none;" rel="nofollow">APPLCTN macro</a></p></li>
<li><p><a
href="https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_transact_macro.htm"
style="text-decoration: none;" rel="nofollow">TRANSACT
macro</a></p></li>
<li>Links from Cobol to IMS Transactions</li>
</ul></li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>TDS</p>
<p><br />
</p>
<p>TP8</p></td>
<td class="confluenceTd"><p>Bull DPS7 (GCOS7)</p>
<p>Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Transactional calls between programs are performed via the variable
NEXT-TPR defined in the LINKAGE section. The Dynamic Link Manager can
draw links between programs from this variable.</li>
<li>Initialization file could be analyzed with a Universal Analyzer
profile.</li>
</ul></td>
</tr>
</tbody>
</table>

### DBMS

<table class="wrapped relative-table confluenceTable"
style="width: 100.0%;">
<colgroup>
<col style="width: 4%" />
<col style="width: 10%" />
<col style="width: 4%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh"><p>DBMS</p></th>
<th class="confluenceTh"><p>Platform (OS)</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"><p>Db2</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS) <br />
IBM i (AS/400)<br />
Unix <br />
Windows</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p>EXEC SQL … END-EXEC macros</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>ORACLE</p></td>
<td class="confluenceTd"><p>Unix</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p>EXEC SQL … END-EXEC macros for
Pro*Cobol.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>IMS/DB</p></td>
<td class="confluenceTd"><p>IBM zSeries (zOS)</p></td>
<td class="confluenceTd"><p>Medium</p></td>
<td class="confluenceTd"><ul>
<li>Access to PCBs are detected for main programs that are directly associated to a PSB and sub-programs that are called by these main programs where the PCBs are passed as arguments of the CALL statements in these main programs.</li>

<li>EXEC SQLIMS … END-EXEC macros.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IDMS</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Often used in conjunction with ADSO.</li>
<li>Although unsupported in CAST AIP "out of the box", there is scope to
establish links between the COBOL code and the IDMS through a variety of
advanced Content Enrichment tools available in CAST AIP such as
Reference Patterns, CAST Transaction Configuration Center etc.</li>
<li><p>The IDMS statements inserted in COBOL programs may raise warning
messages in the analysis log, however, these will not impact the
analysis results (i.e. metamodel generation or rule
evaluation).</p></li>
<li>A custom extension has been developed for this platform, however it
is a User
Community extension and is therefore not officially supported by
CAST.</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>DATACOM</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>It is often used in conjunction with the
IDEAL programming language.</p>
<p>A custom extension has been developed for IDEAL/Datacom, however it
is a User
Community extension and is therefore not officially supported by
CAST.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>ADABAS</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS) <br />
Unix <br />
Windows</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>A custom extension has been developed for
this platform, however it is a User
Community extension</a> and is therefore not officially supported by
CAST.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>IDS2</p></td>
<td class="confluenceTd"><p>Bull DPS7 (GCOS7) <br />
Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>The DML verbs inserted in COBOL programs can cause problems. A
Universal Analyzer profile/extension can be implemented to analyze DDL
and DMCL files.</li>
<li>Some DML verbs can be parsed by using the Reference Pattern. Please
note that dynamic programming is often used. As a consequence, simple
analysis techniques won't deliver interesting results.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>DMS2</p></td>
<td class="confluenceTd"><p>Unisys A Series</p></td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd"><ul>
<li>This DBMS is similar to IDS2 but for the UNISYS A Series platform.
See remarks for IDS2.</li>
<li>A custom extension has been developed for this platform, however it
is a User
Community extension</a> and is therefore not officially supported by
CAST.</li>
</ul></td>
</tr>
</tbody>
</table>

## Questions to ask...

The first thing to do with regard to the source code is to ask some
questions about it and about how the architecture within it has been
developed. These questions are going to help you qualify the source
code, and find out if applications contain components that can not be
analyzed and if it will be necessary to undertake any customization.

Possible answers to questions are listed below - these are only
indications and you will probably receive other answers:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Questions to ask</p></th>
<th class="confluenceTh"><p>Possible answers</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>What platform and/or operating system is in
use?</p></td>
<td class="confluenceTd"><p>Platforms and associated OS that are
generally encountered are:</p>
<ul>
<li>IBM z/OS</li>
<li>Tandem</li>
<li>etc.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Are COBOL programs compliant with COBOL-85?
If yes, then what is the compiler?</p></td>
<td class="confluenceTd"><p>Most common COBOL compilers are:</p>
<ul>
<li>Microfocus</li>
<li>IBM Enterprise COBOL</li>
<li>NonStop COBOL</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Is there a COBOL code generator?<br />
It is important to know this for quality analysis purposes - it is
better to analyze the native code and not the generated COBOL
code.</p></td>
<td class="confluenceTd"><p>There are many code generators. Some of them
are frequently encountered, such as:</p>
<ul>
<li>Telon</li>
<li>Pacbase</li>
<li>Adelia</li>
<li>VisualAge Generator</li>
<li>CA Gen, formerly known as IEF, Composer by IEF, Composer, COOL: Gen,
Advantage: Gen and AllFusion Gen.</li>
<li>Micro Focus APS Generator</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Are there programming languages other than
COBOL?</p></td>
<td class="confluenceTd"><p>There are many other programming languages
that you may encounter:</p>
<ul>
<li>PL/I</li>
<li>C</li>
<li>Assembler</li>
<li>Natural</li>
<li>EasyTrieve</li>
<li>SAS</li>
<li>REXX</li>
<li>ALGOL</li>
</ul>
<p>There is an Universal Analyzer for PL/I, as product.</p>
<p>Natural, EasyTrieve or SAS and more have <a
href="https://doc.castsoftware.com/display/EXTEND/Contributor+types"
style="text-decoration: none;text-align: left;" rel="nofollow">User
Community extension</a>s and are therefore not officially supported by
CAST.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Is there a transactional part? If yes, then
what is the transactional monitor?</p></td>
<td class="confluenceTd"><p>There are several Transaction Managers:</p>
<ul>
<li>CICS</li>
<li>IMS/DC</li>
<li>TP8</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>What is the DBMS accessed by programs? What
is the technology of the DBMS?</p></td>
<td class="confluenceTd"><p>There are several types of DBMS used on
mainframe platforms.</p>
<p>Relational:</p>
<ul>
<li>Db2</li>
<li>Oracle</li>
</ul>
<p>Hierarchical:</p>
<ul>
<li>IMS/DB</li>
</ul>
<p>Network:</p>
<ul>
<li>DMS2</li>
<li>IDMS</li>
<li>IDS2</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Are there technical layers for data access
or program execution?</p></td>
<td class="confluenceTd"><p>Technical layers can be developed to manage
access to persistent data or to manage calls between programs. They are
often materialized by specific subprograms that are called by
applicative programs.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Are there middleware like MOM or ETL
?</p></td>
<td class="confluenceTd"><p>For example:</p>
<ul>
<li>MQSeries</li>
<li>home-made tools</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>How is the name of executable programs built
(load module on z/OS platform)?<br />
<br />
If the name of executable can not be deduced from PROGRAM-ID or source
filename, then how can program calls be resolved?</p></td>
<td class="confluenceTd"><p>The executable program can be named by using
the PROGRAM-ID clause of COBOL programs, by using their source filename
or by using another name defined during the compilation step.</p>
<p>The two first cases are taken into account by the Mainframe Analyzer
but not the third case (and, in this specific case, call links can not
be created). It is therefore important to understand how executable
programs are named.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Are there naming conventions that allow
programs to be grouped in applications, chains or domains?</p></td>
<td class="confluenceTd"><p>For example, first two characters
corresponding to the application code, the two after corresponding to
the sub-application code…</p></td>
</tr>
</tbody>
</table>

Try to get answers before starting the implementation tasks. Indeed,
this information is important for understanding the components that
could be analyzed by using the Mainframe Analyzer, the settings that
should be configured, the potential customizations that may need to be
done and the known limitations.

## Collecting information about the source code

At the end of this step, you should know how:

-   To find the components required for the analysis
-   To check if they are correctly defined in order to be processed by
    the Mainframe Analyzer
-   To identify the characteristics of the COBOL program

You should also have information about:

-   The location of all required components
-   The different types of COBOL programs
-   The databases accessed by the programs
-   The list of potential missing components

### Source files

In Mainframe environments, source files do not have extensions. An
extension must be added during or after the file transfer from the host
machine to a Windows server otherwise CAST AIP cannot correctly deal
with the files. It is highly recommended to use dedicated extensions for
each type of source file, which will be recognized by CAST AIP during
source code packaging and analysis. The following table shows the
extensions you can use:

| Source file    | Extension |
|--------------------|---------------|
| COBOL program      | COB or CBL    |
| COBOL copybook     | CPY or COP    |
| JCL job            | JCL           |
| JCL procedure      | PRC           |
| IMS PSB            | PSB           |
| IMS DBD            | DBD           |
| IMS DC             | TRA           |
| CICS Mapset        | BMS           |
| CICS CSD flat file | CSD or CICS   |

In addition, it is very useful to isolate the source files in dedicated
folders as soon as possible. The following figure shows an example of a
source file tree:

![](268371448.png)  


*A source file tree example*

If this is done, the Delivery Manager Tool could be used more easily to
collect the whole source code or specific parts required for the
analysis.

#### Distributing the source code

The larger the application you need to analyze, the longer the analysis
can take to complete. Large applications and consequently long analysis
times also increase the risk of memory related errors occurring during
the analysis. If this is the case it may be necessary to distribute the
source code in several packages in the Delivery Manager Tool that will
lead to the creation of multiple Analysis Units in CAST Management
Studio, according to the recommendations listed hereafter:

1.  Place all JCL components for analysis in the same package where
    possible even if multiple packages contain the same JCL components
    (see 5 below)
2.  Always include copybooks in packages that also contain COBOL
    programs.
3.  COBOL programs can be distributed over multiple packages - CAST will
    then automatically associate linked objects. It is difficult to
    estimate the size of COBOL programs because the expanded version
    will be analyzed - as a result, you should try to distribute the
    COBOL programs evenly across the available packages.
4.  CICS files can be placed in their own package.
5.  For applications that contain COBOL programs, JCL components and IMS
    databases together, IMS files can be placed in one separate package,
    but it is preferable to place the COBOL programs and the all JCL
    components in the same package (see 1 and 2 above) - if you split
    the COBOL programs over multiple packages (see 3 above), you should
    place all the accompanying JCL files in all packages that contain
    COBOL programs.
6.  Identical objects taken into account in multiple packages associated
    to the same Analysis Service (formerly known as the Local/Knowledge
    Base) will be merged.

For example:

-   AU 1: all IMS files
-   AU 2: all JCL files and first part of COBOL program files and all
    copybook files
-   AU 3: all JCL files and second part of COBOL program files and all
    copybook files
-   AU 4: all CICS files

All packages must be associated to the same parent Application if you
want links between objects in the different associated Analysis Units to
be detected.

### Components that are taken into account

#### COBOL programs

The COBOL analysis forms the main part of the Mainframe Analyzer.
Consequently, it is important to find all COBOL components and to
qualify the COBOL source code properly.

The COBOL language analyzer is the most important part of the Mainframe
Analyzer. You need to find the following information in order to know
how many jobs to create and which parameters require activation:

-   COBOL type
-   Code format
-   Source files location

The COBOL Analyzer is based on the COBOL ANSI 85 standard and several
extensions (refer to the Cast AIP Release Notes to find out which
extensions are taken into account). You need to know this before running
the analysis. The customer can provide you with this information.  
Initially, COBOL is a strict code-formatted programming language. A
source code line is based on five areas:

| Area  | Column range | Description                                                                                                 |
|-----------|------------------|-----------------------------------------------------------------------------------------------------------------|
| Left      | 1 through 6      | Ignored                                                                                                         |
| Indicator | 7                | Used for comment line, continuation character, debug line…                                                      |
| A         | 8 through 12     | Program division and section names; first level data definition; procedure division section and paragraph names |
| B         | 13 through 72    | Program clauses; second level (or more) data definition; procedure division statements                          |
| Right     | 73 through 80    | Ignored                                                                                                         |

There are extensions available which make it possible to avoid this
strict column format. The Left area and the Right area disappear and the
developer can use more than 60 columns for his code (in area B). This is
the case, for instance, with Microfocus COBOL.

You need to search for the source files: where are the programs and
where are the copybooks? It is quite common for the files to be located
in several directories. For instance, you could have a directory for the
batch programs, another for the transactional programs, another for the
subprograms, another for the common copybooks, another for the
transactional copybooks…

It is important to look at these directories and to check their content
by answering the following questions:

-   Is it really and only COBOL code?
-   Are they programs or copybooks?
-   Which column number is used for the Indicator Area?
-   Do they have the Right Area?

Occasionally, component source files implemented in other programming
languages than COBOL are hidden among COBOL source files. When you try
to analyze them, the analyzer will produce syntax error messages or will
ignore them. If you find this type of source files then you can change
the file extension (for instance, "NOT-CBL"). As a result, they will not
be analyzed. You will probably find them after the first analysis.

The COBOL Analyzer needs to know the Indicator Area column number. You
can easily find this information by opening a program source file and
searching for the '\*' column in comment lines. If there is no comment
line then look for the "IDENTIFICATION DIVISION" statement. The
Indicator Area column number is the column number of this statement
minus 1.

![](268371449.png)

##### COBOL areas

It is possible that program source code does not have the Right Area.
You can verify this by opening a source file. If there are statements
after column number 72 then you will need to check the corresponding
option ("End Column No Comments") in the analyzer wizard ("Options"
page).

The following piece of code shows Microfocus COBOL code without the Left
area and Right area. The Indicator area starts at column number 1:

![](268371450.png)  


*The COBOL terminal format*

If programs have different Indicator Area column numbers or if some
programs use the Right Area and others do not, then you will have to
analyze the programs in different Analysis Units configured with
specific parameters and options.

##### Partitioned Data Set (PDS)

If the source code of your COBOL programs is stored in Partitioned Data
Sets (PDS), you need to extract all MEMBERs that correspond to the COBOL
programs you want to analyze and place them in a flat file. In order
that this flat file can be divided up (on the Windows machine) into as
many files as there are COBOL programs, it is important that each MEMBER
is preceded by a banner that contains its name (ex: "MEMBER NAME 
myprog", where myprog is the name of a member). The DMT tool provides
a specific extractor allowing to get component source code through PDS
dump files.

The following image shows an example of a JCL file that can extract
MEMBERS stored in a PDS and then save them in a flat file. This JCL must
be configured according to the norms used in the execution environment.

Note that you can download this file [here](268371440.zip) (note this is a ZIP file that contains the JCL file).

![](268371439.jpg)  


*JCL for extracting MEMBERs to a flat file*

##### COBOL Program vs COBOL Program TP

When a Mainframe application is synchronized in CAST Imaging, the
synchronization process will distinguish two different types of COBOL
Programs as shown below. These are both COBOL Programs but CAST Imaging
uses this classification to distinguish between batch and transaction
execution mode:

-   COBOL Program - executed in batch mode
-   COBOL Program TP - executed in transaction mode

![](575373476.png)

#### Batch processing

A COBOL-based application can have a batch processing part. This step
explains how to identify batch components and how to verify the
completeness of the source code. The Mainframe Analyzer takes into
account JCL for the IBM z/OS platform only.

Batch processing allows you to run large groups of tasks (which can
invoke programs) without any human interaction. A task is implemented by
a JCL running a sequence of programs. A JCL source file can include
procedure source files which contain common commands.

A JCL is a sequence of commands (named cards such as JOB, EXEC or DD)
and is composed of one or more steps which run a program and assign
resources such as data files, database environment… The first thing to
do is to look for the source files for JCL and for procedures. You can
use a GREP tool to find them. A JCL source file starts with the "JOB"
card and a procedure source file starts with the "PROC" card. The second
thing to do is to find out if the JCL includes procedures. Calls to
procedures are made through the "EXEC" cards. Use your GREP tool to find
these cards. There are two categories of "EXEC" cards:

-   EXEC \<procedure_name\>
-   EXEC PGM=\<program_name\>

The first allows the inclusion of procedures into a JCL and the second
allows a program to be called (which is not necessarily a COBOL
program).

![](268371451.png)  


*JCL source code example*

If you find the first category of "EXEC" cards then it means that there
are procedures used by the jobs. You must then make sure you have the
corresponding source files. If not, then you can ask the customer to
give you the missing procedures.

You will inevitably find the second category of "EXEC" cards because the
main goal of a JCL is to run programs (utility, technical and
application programs). If you build the list of called programs then you
will be able to check if it does not miss any. However, you will have to
distinguish the technical programs from the others. It is often easier
to run the analysis and to check any unresolved programs after the
analysis has completed.

#### On-line processing

A COBOL-based application can have a transactional part. This step
explains how to find out usages related to transactions and helps the
reader to identify the corresponding components. The Mainframe Analyzer
can analyze on-line programs developed to run on the CICS environment.
It works with several types of information:

-   Macros embedded in the program source code
-   Screen definition
-   CSD table

You can easily find out if the programs use CICS commands by scanning
the source files with a GREP tool to find embedded macros. These macros
look like the embedded SQL macros and start with the "EXEC CICS" string.

If there are CICS programs then there are generally also screen
definition files (except if the screens are managed by another software
layer). These files contain BMS code describing the screens (named
maps). You can find them by using your GREP tool and searching for the
"DFHMSD" string.

There is a similar concept in an IMS DC environment (supported in AIP
Core ≥ 8.3.19 or by the Mainframe Analyzer extension,
see Mainframe - IMS DC support): screens are described in MFS
files and the MFS language is a macro language like BMS. MFS
can be analyzed by the Mainframe analyzer since AIP Core ≥ 8.3.25 or by
the Mainframe Analyzer extension.

Note though that if the customer uses IMS DC, then it is not appropriate
to ask him for BMS files since they do not exist in this environment.

![](268371452.png)  


*BMS map source code example*

Finally, if there are CICS programs, then it is useful to also have the
CSD table. It allows the creation of CICS objects and more particularly
links from transactions to programs.  
The CICS Analyzer currently works with the definition of the CSD
resources and not with a dump of them. This should contain the
following statements (you can see an example in the Figure 8):

-   DEFINE TRANSACTION()
-   DEFINE PROGRAM()
-   DEFINE TDQUEUE()
-   DEFINE FILE()
-   DEFINE MAPSET()

This resource definition can be delivered via a copy (into an ASCII flat
file) of the script used to define the CICS environment or by using a
JCL in order to extract this information from CICS. The following JCL is
shown as an example. If you want to use it, then you have to configure
it according to the norms used in the execution environment and then you
must specify the lists or the groups of CICS objects to collect (please
note that you cannot specify a list and a group on the same
command). The application owners and/or CICS administrators are best
placed to help you retrieve the information you need.

You can download this JCL [here](268371442.zip) (note this is a ZIP file that contains the JCL file).

![](268371453.png)  
*Example of JCL code to extract the CSD*

The resulting flat file must look like this:

![](268371445.png)  
*CICS CSD flat file example*

#### Databases

The goal of this step is to explain to the reader how to find
information about accesses to databases made by COBOL programs and to
indicate which different files the Mainframe Analyzer needs.  
A COBOL program can access several types of database:

-   Relational (e.g: IBM Db2)
-   Network (e.g: CA IDMS)
-   Hierarchical (e.g: IBM IMS/DB)

The Mainframe Analyzer can detect the accesses made to a Db2 or an
Oracle relational database and to an IMS/DB hierarchical database.
However, these detections are not based on the same types of
information.

##### Db2 and Oracle

If the COBOL programs access a relational database like Db2 or Oracle
through embedded SQL (queries are delimited by the "EXEC SQL" and
"END-EXEC" strings), then it is necessary to analyze it in the same
application. The CAST Management Studio will automatically perform
database analysis before programs analysis. Links will be drawn when
analyzing programs. As such, it is important to check if the COBOL code
contains SQL queries before defining the Analysis Units.

If not, then the links will not be created in the Analysis Service.
However, it is possible to draw complementary links by using the
"Dependencies" tab of the Application editor available in CAST
Management Studio.

##### IMS/DB

IMS database structure is directly analyzed by the Mainframe Analyzer
through the database definition files (DBD). Without these files, you
will not be able to analyze these databases. The IMS/DB accesses are
made via embedded macros starting with "EXEC DLI" (in the CICS context
only) or more generally through a technical sub-program called
"CBLTDLI". In addition, main programs making access to IMS contain the
entry point "DLITCBL" at the beginning of the PROCEDURE division. So if
you want to know if a program is an IMS program, then you should find
out one of these syntaxes:

-   EXEC DLI … END-EXEC
-   CALL "CBLTDLI" USING …
-   ENTRY DLITCBL USING …

If you find one or other of these syntaxes in the COBOL source code then
you can be sure that programs make accesses to IMS.

The Mainframe Analyzer does not take into account the IMS/DC transaction
manager. Accesses to this transaction manager are also made via the
CBLTDLI technical sub-program and associated to PSB files. It is not
really possible to know if a program works with IMS/DC or with IMS/DB by
looking at the source code. It is necessary to look at the PSB
associated to the COBOL program. If this PSB contains a first PCB that
is not typed as a DB PCB, then you can be sure that the program makes
access to IMS/DC.

If the program works with IMS/DB, then you need to obtain the DBD and
the PSB files associated to the IMS databases accessed by programs. You
can search for the DBD using the "DBD\[ \]+NAME=" string.

![](268371446.png)  
*DBD source code example.*

You can search for the PSB by using the "PCB\[ \]+TYPE=" and "PSBGEN" or
"PSBNAME" strings.

![](268371447.png)  
*PSB source code example*

If you do not have the PSB and the associated DBD then it will be
impossible for the Mainframe Analyzer to recognize the database elements
accessed by the programs and it will not be able to create the
corresponding links. The analyzer will indicate these missing components
in the log file/window.

##### IMS/DC

The Mainframe Analyzer will analyze the IMS Transaction file to resolve
two macros:

-   [APPLCTN
    macro](https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_applctn_macro.htm)

-   [TRANSACT
    macro](https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_transact_macro.htm)

You can generate the IMS Transaction file by using the following
JCL. The generated stage 1 macro statements are written to the SYSOUT
data set. Messages that are issued by the DFSURDD0 utility are written
to the SYSPRINT data set. You can find out more
here: [https://www.ibm.com/support/knowledgecenter/en/SSEPH2_14.1.0/com.ibm.ims14.doc.sur/ims_rddsexamples.htm](https://www.ibm.com/support/knowledgecenter/en/SSEPH2_14.1.0/com.ibm.ims14.doc.sur/ims_rddsexamples.htm).

``` text
==============================================================================
//job name JOB  CLASS=J,MSGCLASS=A,MSGLEVEL=(1,1)   
//JOBLIB   DD   DSN=library data set name,DISP=SHR   
//S1       EXEC PGM=DFSURDD0,MEMLIMIT=12G            
//RDDSDSN  DD   DSN=RDDS data set name,DISP=SHR 
//SYSOUT   DD   DSN=output data set name,DISP=(,CATLG,DELETE),  
//              UNIT=SYSDA,VOL=SER=Volume name, 
//              SPACE=(CYL,(1,1),RLSE),      
//              DCB=(LRECL=80,RECFM=FB,BLKSIZE=800) 
//SYSPRINT DD   SYSOUT=*                     
//SYSIN    DD   * 
OUTPUT=MAC 
/* 
//
==============================================================================
```

The resulting file would resemble the following:

``` text
==============================================================================
         APPLCTN PSB=ANT001,PGMTYPE=TP,SCHDTYP=PARALLEL                 00001400
         TRANSACT CODE=ANTPSR,MSGTYPE=(MULTSEG,RESPONSE,4),            *00001500
               PARLIM=0,SEGNO=400,                                     *00001600
               MODE=SNGL,PROCLIM=(03,01)                                00001700
...
         APPLCTN PSB=ANT100,PGMTYPE=BATCH                               00001800
         TRANSACT CODE=ANT100,MSGTYPE=(MULTSEG,NONRESPONSE,6),         *00001900
               MODE=SNGL,PRTY=(0,0),PROCLIM=(03,01)                     00002000
==============================================================================
```

## Delivering the source code

COBOL based applications can be deployed on various platforms. As a
consequence, source code delivery can be performed in different ways. 

### Applications deployed on IBM zOS mainframe

#### COBOL

DMT can only run packaging on Cobol files upto 2 MB. When packaging
Cobol files larger than 2MB, DMT raises the warning - "WARNING
cast.dmt.discover.mainframe.largeFile File source/Filename.cob is more
than 2 MB size".

Program and copybook source files can either be managed in a
Partitioned Data Set (PDS) or in a Source Code Manager (SCM)
like Endevor. In both cases, resulting files are transferred by using
the FTP protocol and this operation can be automated through a dedicated
JCL job or by adding a last step in JCL job used to extract the source
code as described below.

It is possible to dump the content of a PDS in a flat file where each
member is preceded by a specific banner. This operation is performed by
executing a dedicated tool
([IEBPTPCH](https://www.ibm.com/docs/en/zos/2.4.0?topic=utilities-iebptpch-print-punch-program))
through a JCL job (you can download this
file [here](268371440.zip) (note this is a ZIP file that
contains the JCL file)). The resulting file can be then transferred onto
the analysis server ready for delivery via the Delivery Manager Tool.

If the source code is managed in a SCM, it is necessary to study the
interfaces provided by the tool to export source code and to select the
most appropriate one. It is generally possible to export components in
to individual files or to group them in a PDS. In the first case, the
resulting files can be transferred directly and in the second case, it
is necessary to apply the operation described above.

The process looks like this:

SCM --\[extraction\]--\> PDS --\[JCL\]--\> PDS dump --\[FTP\]--\> PDS
dump --\[DMT\]--\> AIP

#### JCL

Although it is possible to manage JCL jobs and procedure source files in
an SCM, this type of component is generally managed in a PDS.
The same extraction and file transfer procedures described for COBOL can
therefore be applied to JCL source files.

#### CICS

Two types of source files are supported by the Mainframe Analyzer:
CSD files and map source files (BMS files):

-   CSD files can be extracted from the CICS system by executing a
    dedicated JCL job (you can download this JCL [here](268371442.zip) (note this is a
    ZIP file that contains the JCL file)). The resulting file can be
    then transferred onto the analysis server by using the FTP protocol,
    ready for delivery via the Delivery Manager Tool.
-   BMS files must be transferred manually to the Windows file
    system ready for delivery via the Delivery Manager Tool.

#### IMS/DB

Two types of source files are supported by the Mainframe Analyzer:
DBD files and PSB files. These files must be transferred
manually to the Windows file system ready for delivery via the Delivery
Manager Tool.

#### IMS/DC

One type of source file is supported by the Mainframe Analyzer: TRA
files. These files must be transferred manually to the Windows file
system ready for delivery via the Delivery Manager Tool.

#### Db2

The structure of a Db2 subsystem can be exported to DDL and then
analyzed with the [SQL Analyzer](../../sql/extensions/com.castsoftware.sqlanalyzer).

#### Summary

The complete source code delivery process for all source code types can
be summarized by the graph shown below:

![](268371443.png)

### Applications deployed on other platforms

#### COBOL

Like for the IBM zOS platform, program and copybook source files can
either be managed in a Source Code Manager (SCM) or in file
system. In both cases, the resulting files are transferred by using
the FTP protocol and the operation can be automated with a scheduler or
a command language.

-   If the source code is managed in an SCM, it is necessary to study
    the interfaces provided by the tool to export source code and then
    to select the most appropriate one. 
-   If the source code is managed in the file system, then it may be
    appropriate to use a file compression utility to group the source
    files in a limited number of ZIP files for transfer.
