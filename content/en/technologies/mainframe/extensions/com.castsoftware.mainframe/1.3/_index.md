---
title: "Mainframe Analyzer - 1.3"
linkTitle: "1.3"
type: "docs"
no_list: true
---

***
 
## Extension ID

com.castsoftware.mainframe

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Mainframe (Cobol, CICS, JCL,
IMS...). If your application contains Mainframe source code and
you want to view these object types and their links with other objects,
then you should install this extension.

## Technical information

### Extension

When installed, this extension replaces the Mainframe Analyzer
embedded in CAST Imaging Core:

-   The Mainframe Analyzer embedded in Imaging Core will continue to exist
    and will be shipped "out of the box" with Imaging Core.
-   Critical bugs will continue to be fixed in the Mainframe Analyzer
    embedded in Imaging Core but no new features or functionality will be
    added.
-   The Mainframe Analyzer extension will have exactly the same features
    and functionality on release as the Mainframe Analyzer embedded in
    Imaging Core, therefore analysis results will be identical.
-   The Mainframe Analyzer is compatible with Imaging Core ≥ 8.3.26.
-   All future development of the Mainframe Analyzer (new features,
    functionality etc.) will be completed in the Mainframe Analyzer
    extension only. Critical bug fixes will be fixed in the Mainframe
    Analyzer extension (as well as the analyzer embedded in Imaging Core).
-   The behaviour is as follows:
    -   Nothing is automatic - for both AIP Console and "legacy" CAST
        AIP deployments, the Mainframe Analyzer extension must be
        manually downloaded and installed in order to use it (\* see
        note below)
    -   If the extension is installed, CAST AIP Console/CAST Management
        Studio will automatically detect that it exists and will use the
        extension rather than the analyzer embedded in Imaging Core.
    -   Once the extension has been installed and used to produce
        analysis results, it is not possible to reverse this choice by
        removing the extension and re-analyzing the source code again.

Starting CAST Console 1.26, if Mainframe source code is detected by
AIP Console during delivery, the Mainframe extension will
automatically be downloaded and installed replacing the Mainframe
Analyzer embedded in Imaging Core. If you do not want this to occur, you
must blacklist the Mainframe extension using
the aip-node-app.properties file on the AIP Node responsible for
analyzing your application. See [Blacklisting
extensions ](https://doc.castsoftware.com/display/AIPCONSOLE/Blacklisting+extensions)for
more information.

### Features / support added in the extension

#### IBM MQ Series

The analyzer supports IBM MQSeries (see [Mainframe - Technical
notes](Mainframe_-_Technical_notes)), but the following support has
been added in the extension:

-   MQSeries objects are now supported via Batch and CICS environments
    (ex: CSQBOPEN, CSQCOPEN).
-   MQSeries objects are now identified without the copybook CMQODV.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| ≥ v2/8.3.26 | Microsoft Windows | :white_check_mark: |

### Required third-party software

Please see: [Required third-party software](../../../requirements/).

## Dependencies with other extensions

None.

## Parametrization_Cobol.xml file

The extension is shipped with a parameterization XML file
called Parametrization_Cobol.xml file - see [Mainframe - Analysis
configuration](Mainframe_-_Analysis_configuration) for more
information about this. This file takes priority over the same file
delivered in Imaging Core (in `Configuration\Parametrization\Cobol`)
therefore if you are moving to using the Mainframe Analyzer, you must
ensure the following:

-   You must copy the file (or the rules in file) from the Imaging Core
    installation folder `Configuration\Parametrization\Cobol` into the
    deployed extension on your AIP
    Node(s): `%PROGRAMDATA%\CAST\CAST\Extensions\com.castsoftware.mainframe.-funcrel\configuration\parametrization\Cobol`

-   If you upgrade to a newer release of the extension, and if you have
    modified the Parametrization_Cobol.xml file to add custom rules,
    you must ensure that you copy the file (or the rules) into the new
    extension installation location, e.g.:

    -   Current extension installation
        location: `%PROGRAMDATA%\CAST\CAST\Extensions\com.castsoftware.mainframe.<version1>-funcrel\configuration\parametrization\Cobol`
    -   New extension installation
        location: `%PROGRAMDATA%\CAST\CAST\Extensions\com.castsoftware.mainframe.<version2>-funcrel\configuration\parametrization\Cobol`

## Application qualification information

Please see: [Mainframe - Application qualification specifics](../../../qualification-specifics/).

## Prepare and deliver the source code

Please see: [Mainframe - Analysis configuration and execution](../../../prepare/).

## Analysis configuration and execution

Please see: [Mainframe - Analysis configuration and execution](../../../prepare/) and all
child pages:

-   [Mainframe - Analysis configuration](../../../analysis-config)
-   [Mainframe - Analysis messages](../../../logs/)
-   [Mainframe - Analysis results](../../../results/)

## Structural Rules

Please see: [Mainframe - Structural rules](../../../rules/).
