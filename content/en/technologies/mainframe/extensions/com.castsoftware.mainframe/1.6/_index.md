---
title: "Mainframe Analyzer - 1.6"
linkTitle: "1.6"
type: "docs"
no_list: true
---

***
 
## Extension ID

com.castsoftware.mainframe

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Mainframe (Cobol, CICS, JCL,
IMS...). If your application contains Mainframe source code and
you want to view these object types and their links with other objects,
then you should install this extension.

## Technical information

### Extension

When installed, this extension replaces the Mainframe Analyzer
embedded in CAST Imaging Core:

-   The Mainframe Analyzer embedded in CAST Imaging Core will continue to exist
    and will be shipped "out of the box" with CAST Imaging Core.
-   Critical bugs will continue to be fixed in the Mainframe Analyzer
    embedded in CAST Imaging Core but no new features or functionality will be
    added.
-   The Mainframe Analyzer extension will have exactly the same features
    and functionality on release as the Mainframe Analyzer embedded in
    CAST Imaging Core, therefore analysis results will be identical.
-   The Mainframe Analyzer is compatible with CAST Imaging Core ≥ 8.3.26.
-   All future development of the Mainframe Analyzer (new features,
    functionality etc.) will be completed in the Mainframe Analyzer
    extension only. Critical bug fixes will be fixed in the Mainframe
    Analyzer extension (as well as the analyzer embedded in CAST Imaging Core).
-   The behaviour is as follows:
    -   Nothing is automatic - the Mainframe Analyzer extension must be
        manually downloaded and installed in order to use it (\* see
        note below)
    -   If the extension is installed, it  will automatically be detected and will be used rather than the analyzer embedded in CAST Imaging Core.
    -   Once the extension has been installed and used to produce
        analysis results, it is not possible to reverse this choice by
        removing the extension and re-analyzing the source code again.

Starting CAST Console 1.26, if Mainframe source code is detected during delivery, the Mainframe extension will
automatically be downloaded and installed replacing the Mainframe
Analyzer embedded in CAST Imaging Core.

### Features / support added in the extension

#### IBM MQ Series

The analyzer supports IBM MQSeries (see [Technical Notes](../../../notes)), but the following support has
been added in the extension:

-   MQSeries objects are now supported via Batch and CICS environments
    (ex: CSQBOPEN, CSQCOPEN).
-   MQSeries objects are now identified without the copybook CMQODV.

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| ≥ v2/8.3.26 | Microsoft Windows | :white_check_mark: |

### Required third-party software

Please see: [Required third-party software](../../../requirements/).

## Dependencies with other extensions

None.

## Parametrization_Cobol.xml file

The extension is shipped with a parameterization XML file
called `Parametrization_Cobol.xml` file - see [Analysis configuration](../../../analysis-config/) for more
information about this. This file takes priority over the same file
delivered in CAST Imaging Core (in `Configuration\Parametrization\Cobol`)
therefore if you are moving to using the Mainframe Analyzer, you must
ensure the following:

-   You must copy the file (or the rules in file) from the CAST Imaging Core
    installation folder `Configuration\Parametrization\Cobol` into the
    deployed extension on your Node(s): `%PROGRAMDATA%\CAST\CAST\Extensions\com.castsoftware.mainframe.-funcrel\configuration\parametrization\Cobol`

-   If you upgrade to a newer release of the extension, and if you have
    modified the `Parametrization_Cobol.xml` file to add custom rules,
    you must ensure that you copy the file (or the rules) into the new
    extension installation location, e.g.:

    -   Current extension installation
        location: `%PROGRAMDATA%\CAST\CAST\Extensions\com.castsoftware.mainframe.<version1>-funcrel\configuration\parametrization\Cobol`
    -   New extension installation
        location: `%PROGRAMDATA%\CAST\CAST\Extensions\com.castsoftware.mainframe.<version2>-funcrel\configuration\parametrization\Cobol`

## Application qualification information

Please see: [Mainframe - Application qualification specifics](../../../qualification-specifics/).

## Prepare and deliver the source code

Please see: [Mainframe - Analysis configuration and execution](../../../prepare/).

## Analysis configuration and execution

Please see: [Mainframe - Analysis configuration and execution](../../../prepare/) and all
child pages:

-   [Mainframe - Analysis configuration](../../../analysis-config)
-   [Mainframe - Analysis messages](../../../logs/)
-   [Mainframe - Analysis results](../../../results/)

## Structural Rules

Please see: [Mainframe - Structural rules](../../../rules/).
