---
title: "Mainframe Analyzer - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.mainframe

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Mainframe (Cobol, CICS, JCL, IMS...). If your application contains Mainframe source code and you want to view these object types and their links with other objects, then you should install this extension.

## Isofunctionality

This release of the extension is based on the previous 1.4.x releases. Please check the [release notes](rn/) for more information.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :x: |

## Dependencies with other extensions

Some extensions require the presence of other extensions in order to function correctly. The Mainframe Analyzer extension requires that the following other extensions are also installed:

- [com.castsoftware.wbslinker](../../../../multi/com.castsoftware.wbslinker/)

>Note that when using CAST Imaging Console to install the extension, any dependent extensions are automatically downloaded and installed for you. You do not need to do anything.

## Download and installation instructions

For Mainframe applications, the extension will be automatically installed by CAST Imaging Console:

![](../images/662143579.jpg)

## What results can you expect?

### Objects

#### Cobol

| Icon | Description |
|:----:|-------------|
| ![](../images/664436777.png) | Cobol Class                                     |
| ![](../images/664436784.png) | Cobol Conditional Test                          |
| ![](../images/664436785.png) | Cobol Constant Data                             |
| ![](../images/664436775.png) | Cobol CopyBook                                  |
| ![](../images/664436789.png) | Cobol Data                                      |
| ![](../images/664436790.png) | Cobol Data Link                                 |
| ![](../images/664436789.png) | Cobol Declarative Block                         |
| ![](../images/664436789.png) | Cobol Declarative Paragraph                     |
| ![](../images/664436786.png) | Cobol Declarative Section, Cobol Section        |
| ![](../images/664436778.png) | Cobol Directory/Root Directory                  |
| ![](../images/664436788.png) | Cobol Division                                  |
| ![](../images/664436779.png) | Cobol Entry Point                               |
| ![](../images/664436791.png) | Cobol File Link (Sequential, Partitioned, VSAM) |
| ![](../images/664436793.png) | Cobol Index                                     |
| ![](../images/664436780.png) | Cobol Interface                                 |
| ![](../images/664436785.png) | Cobol Literal                                   |
| ![](../images/664436787.png) | Cobol Paragraph                                 |
| ![](../images/664436844.png) | Cobol Nested Program                            |
| ![](../images/664436776.png) | Cobol Program                                   |
| ![](../images/664436785.png) | Cobol Structure Data                            |
| ![](../images/664436781.png) | Cobol Transaction                               |
| ![](../images/664436792.png) | Cobol SQL Query                                 |
| ![](../images/664436845.png) | Publisher IBM MQ                                |
| ![](../images/664436846.png) | Subscriber IBM MQ                               |

#### JCL

| Icon | Description |
|:----:|-------------|
| ![](../images/664436798.png) | JCL Data Set (Sequential, Partitioned, VSAM) |
| ![](../images/664436802.png) | JCL Directory/Root Directory                 |
| ![](../images/664436935.png) | JCL External Procedure                       |
| ![](../images/664436934.png) | JCL External Program                         |
| ![](../images/664436799.png) | JCL Included File / Included Not Found       |
| ![](../images/664436800.png) | JCL Index                                    |
| ![](../images/664436797.png) | JCL In-stream Procedure                      |
| ![](../images/664436794.png) | JCL to Java Program                          |
| ![](../images/664436846.png) | JCL (Catalogued) Job                         |
| ![](../images/664436936.png) | Unknown JCL Procedure                        |
| ![](../images/664436848.png) | JCL (Catalogued) Job Prototype               |
| ![](../images/664436795.png) | JCL (Catalogued) Procedure                   |
| ![](../images/664436847.png) | JCL Project                                  |
| ![](../images/664436796.png) | JCL Step                                     |
| ![](../images/664436803.png) | JCL SQL Query                                |

#### IMS

| Icon | Description |
|:----:|-------------|
| ![](../images/664436809.png) | IMS Alternate Program Control Block |
| ![](../images/664436811.png) | IMS Database Program Control Block  |
| ![](../images/664436807.png) | IMS DB Definition                   |
| ![](../images/664436808.png) | IMS DB Field                        |
| ![](../images/664436802.png) | IMS Directory/Root Directory        |
| ![](../images/664436815.png) | IMS File / Transaction File         |
| ![](../images/664436812.png) | IMS GSAM Program Control Block      |
| ![](../images/664436817.png) | IMS Message Format Service          |
| ![](../images/664436818.png) | IMS Message Input Descriptor        |
| ![](../images/664436819.png) | IMS Message Output Descriptor       |
| ![](../images/664436850.png) | IMS Program Control Block           |
| ![](../images/664436810.png) | IMS Program Specification Block     |
| ![](../images/664436813.png) | IMS Project                         |
| ![](../images/664436814.png) | IMS Segment / DB Segment            |
| ![](../images/664436816.png) | IMS Transaction                     |
| ![](../images/664436803.png) | IMS SQL Query                       |

#### CICS

| Icon | Description |
|:----:|-------------|
| ![](../images/664436842.png) | CICS DataSet                 |
| ![](../images/664436781.png) | CICS DB2 Transaction         |
| ![](../images/664436940.png) | CICS Definition File         |
| ![](../images/664436943.png) | CICS External File          |
| ![](../images/664436802.png) | CICS Folder                  |
| ![](../images/664436941.png) | CICS Group                   |
| ![](../images/664436839.png) | CICS Map                     |
| ![](../images/664436840.png) | CICS MapSet                  |
| ![](../images/664436939.png) | CICS Map Definition          |
| ![](../images/664436937.png) | CICS Project                 |
| ![](../images/664436942.png) | CICS Temporary Storage       |
| ![](../images/664436781.png) | CICS Transaction             |
| ![](../images/664436841.png) | CICS Transient Data          |
| ![](../images/664436836.png) | CICS SOAP Operation          |
| ![](../images/664436836.png) | CICS SOAP Operation Call     |

### Links

#### Cobol

<table>
<tbody>
<tr>
<th>Link Type</th>
<th>Calling Linked
Objects</th>
<th>Called Linked
Objects</th>
<th>Code Example</th>
</tr>
&#10;<tr class="odd">
<td><p><strong>CALL
PROG*</strong></p>
<p><br />
</p></td>
<td>Program, section or
copybook</td>
<td>Program or
entrypoint</td>
<td><p>CALL
"CC2DISPLAY"</p></td>
</tr>
<tr class="even">
<td><strong>CALL
TRANSAC*</strong></td>
<td>Program, section or
copybook</td>
<td>Program or
entrypoint</td>
<td>EXEC CICS XCTL<br />
PROGRAM(TEST)<br />
END EXEC</td>
</tr>
<tr class="odd">
<td><strong>CALL
PERFORM</strong></td>
<td>Program, section or
sub-object</td>
<td>Section or
paragraph</td>
<td>MODULE-ENTREE
SECTION.<br />
*<br />
* LA LIGNE CI DESSOUS DOIT ETRE DELETEE APRES LES TESTS<br />
*<br />
MOVE '*** EXECUTION STARTEE NORMALEMENT ***' TO MESS.<br />
DISPLAY MESS.<br />
MOVE SPACE TO MESS.<br />
*<br />
<strong>PERFORM LECTURE-PARAM</strong>.<br />
*<br />
FIN-MODULE-ENTREE. EXIT.<br />
EJECT<br />
*<br />
COPY SYBPINIT.</td>
</tr>
<tr class="even">
<td><strong>CALL
GOTO</strong></td>
<td>Program</td>
<td>First executed section
or paragraph</td>
<td>****************<br />
LECTURE-PARAM SECTION.<br />
****************<br />
*<br />
READ MEF001.<br />
IF FILE-STATUS NOT = '00'<br />
DISPLAY 'CODE RETOUR' FILE-STATUS<br />
DISPLAY 'CARTE PARAMETRE INEXISTANTE : ' CODE-ABEND<br />
<strong>GO TO SORTIE-ERREUR</strong><br />
END-IF.<br />
*</td>
</tr>
<tr class="odd">
<td><strong>CALL
GOTO</strong></td>
<td>Section</td>
<td>First executed paragraph
in section</td>
<td>Same example as
above.</td>
</tr>
<tr class="even">
<td><strong>CALL
GOTO</strong></td>
<td>Paragraph</td>
<td>Next executed paragraph
in same section</td>
<td>Same example as
above.</td>
</tr>
<tr class="odd">
<td><strong>CALL</strong></td>
<td>Section/Paragraph</td>
<td>Section/Paragraph</td>
<td>When Sections or
Paragraphs are sequentially executed (one after the other) a Call link
will be generated. For example, if a program is made of Section A then
Section B then Paragraph C then Section D, the links will be resolved as
A -&gt; B -&gt; C -&gt; D.</td>
</tr>
<tr class="even">
<td><strong>INCLUDE</strong></td>
<td>Program or Copybook</td>
<td>Copybook</td>
<td>* -ACDA * MNPW * * * *
* * * * * * * * * * * * * * * * * * * * *<br />
* FD DU FICHIER INF103 PAR LE CHEMIN INF103<br />
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *<br />
*<br />
BLOCK CONTAINS 0.<br />
*<br />
<strong>COPY INF103 REPLACING INF103 BY ING103.</strong></td>
</tr>
<tr class="odd">
<td><strong>USE</strong></td>
<td><p>Calls to matched
character strings</p></td>
<td><p>Calls to matched
character strings</p></td>
<td>Display "a
<strong>string</strong>"</td>
</tr>
<tr class="even">
<td><strong>USE</strong></td>
<td>Cobol Program, Cobol
Section, Cobol Paragraph</td>
<td>Program Specification
Block</td>
<td>EXEC DLI SCH PSB
(<strong>MYPSB</strong>) END-EXEC</td>
</tr>
<tr class="odd">
<td><strong>ACCESS
OPEN</strong></td>
<td><p>Cobol Program or
sub-object</p></td>
<td><p>Cobol File Link or
Cobol Data Link</p></td>
<td>*========================================================
* OUVERTURE DU FICHIER IAHCDECE
*========================================================
OPEN-IAHCDECE.<br />
<strong>OPEN INPUT IAHCDECE</strong>.
*======================================================== * LECTURES DU
FICHIER IAHCDECE
*======================================================== LECT-IAHCDECE.
<strong><br />
READ IAHCDECE</strong>.
*======================================================== * FERMETURE DU
FICHIER IAHCDECE
*========================================================
CLOSE-IAHCDECE.<br />
<strong>CLOSE IAHCDECE</strong>.<br />
 &#10;<p>*======================================================== *
LECTURE-SEGMENT-CISRAPMP
*========================================================<br />
EXEC DLI<br />
GU<br />
USING<br />
PCB (3)<br />
SEGMENT (<strong>CISRAPMP</strong>)<br />
WHERE (CIOEAX = IN-PREM-H)<br />
INTO (WS-DUMMY-AREA)<br />
END-EXEC.<br />
 </p>
<p>*========================================================<br />
COMMANDE-CALL-LEVEL<br />
*========================================================<br />
CALL 'CBLTDLI' USING GU<br />
<strong>CICSDLI-PCB</strong><br />
CICSDLI-SEGM<br />
CICSDLI-QSSA</p></td>
</tr>
<tr class="even">
<td><strong>ACCESS
CLOSE</strong></td>
<td><p>Cobol Program or
sub-object</p></td>
<td><p>Cobol File Link or
Cobol Data Link</p></td>
<td><p>Same example as
above.</p></td>
</tr>
<tr class="odd">
<td><strong>ACCESS
READ</strong></td>
<td><p>Cobol Program or
sub-object, Cobol Section, Cobol Paragraph</p></td>
<td><p>Cobol File Link,
Cobol Data Link, Constant Data, Structured Data, Data, Conditional Test,
Program Communication Block<br />
(IMS PCB), IMS Segment</p></td>
<td><p>Same example as
above.</p></td>
</tr>
<tr class="even">
<td><strong>ACCESS
WRITE</strong></td>
<td><p>Cobol Program or
sub-object, Cobol Section, Cobol Paragraph</p></td>
<td><p>Cobol File Link,
Cobol Data Link, Constant Data, Structured Data, Data, Conditional Test,
Program Communication Block<br />
(IMS PCB), IMS Segment</p></td>
<td><p>Same example as
above.</p></td>
</tr>
</tbody>
</table>

For **Embedded SQL links**, the following are valid for **all** servers:

| Link Type      | Description                                              | Code Example       |
|----------------|----------------------------------------------------------|--------------------|
| **USE SELECT** | This type is reserved for server side object referencing | EXEC SQL... SELECT |
| **USE UPDATE** | As above.                                                | EXEC SQL... UPDATE |
| **USE INSERT** | As above.                                                | EXEC SQL... INSERT |
| **USE DELETE** | As above.                                                | EXEC SQL... DELETE |
| **CALL**       | As above.                                                | EXEC SQL... CAL    |

For link types CALL PROG and CALL TRANSAC, two limitations exist when
the call is in "string" form:

-   If the string is constant and declared in the "data-division"
    section, the entry point will be resolved in the normal way.
-   If the string is dynamic, the program may be found by the **Dynamic
    Link Manager**.

In addition, the following **Embedded SQL links **are valid for **DB2
only**:

| Link Type      | Description                                                                              | Code Example |
|----------------|------------------------------------------------------------------------------------------|--------------|
| **DEPEND ON**  | This type is reserved for server side object referencing on structured or distinct UDTs. | \-           |
| **DDL CREATE** | This type is reserved for server side object referencing on Tables                       | \-           |
| **DDL DROP**   |                                                                                          |              |

#### JCL

| Link Type          | Calling Linked Objects | Called Linked Objects |
|--------------------|------------------------|-----------------------|
| **ACCESS WRITE**   | JCL Step               | JCL Data Set          |
| **ACCESS READ**    | JCL Step               | JCL Data Set          |
| **ACCESS EXECUTE** | JCL Step               | JCL Data Set          |
| **PROTOTYPE**      | Cobol File Link        | JCL Data Set          |
| **PROTOTYPE**      | Cobol Data Link        | JCL Data Set          |
| **PROTOTYPE**      | JCL Data Set           | Cobol JCL Program     |
| **CALL**           | JCL Step               | Cobol JCL Program     |
| **CALL**           | JCL Job                | JCL Step              |
| **CALL**           | JCL Procedure          | JCL Step              |
| **CALL**           | JCL Step               | JCL Procedure         |
| **USE**            | JCL Step               | IMS DBD               |
| **MONITOR AFTER**  | JCL Step               | JCL Step              |

#### IMS

| Link Type        | Calling Linked Objects | Called Linked Objects    |
|------------------|------------------------|--------------------------|
| **ACCESS WRITE** | IMS PC Block           | IMS Segment              |
| **USE**          | IMS PC Block           | IMS DBD or IMS GSAM File |

#### CICS

| Link Type        | Calling Linked Objects | Called Linked Objects |
|------------------|------------------------|-----------------------|
| **CALL TRANSAC** | CICS Transid           | Client/Cobol Program  |

For **Transactional Code**, the following are valid:

<table>
<tbody>
<tr>
<th>Link Type</th>
<th>Calling Linked Objects</th>
<th>Called Linked Objects</th>
<th>Code Example</th>
</tr>
&#10;<tr class="odd">
<td><strong>CALL TRANSAC</strong></td>
<td>Client/Cobol Program or its Sub object</td>
<td>Client/Cobol Program</td>
<td><p>EXEC CICS XCTL<br />
PROGRAM(TEST)<br />
END EXEC</p>
<p>or</p>
<p>EXEC CICS LINK<br />
PROGRAM(TEST)<br />
END EXEC</p></td>
</tr>
<tr class="even">
<td><strong>CALL TRANSAC</strong></td>
<td>Client/Cobol Program or its Sub object</td>
<td>CICS Transaction</td>
<td><p>EXEC CICS START<br />
TRANSID(TRANSID)<br />
END EXEC</p>
<p>or</p>
<p>EXEC CICS RETURN<br />
TRANSID (W-XFR-CTRS)<br />
COMMAREA (W-CIC-CMA-LDON)<br />
LENGTH (W-CIC-CMA-QLENDON)<br />
END-EXEC</p></td>
</tr>
<tr class="odd">
<td><strong>MONITOR</strong></td>
<td>Client/Cobol Program or its Sub object</td>
<td>CICS Map</td>
<td>EXEC CICS<br />
SEND MAP (W-CIC-CMAPCUR)<br />
MAPSET (W-CIC-CMPSCUR)<br />
FROM (MGAB10O)<br />
ERASE<br />
FREEKB<br />
FRSET<br />
CURSOR<br />
END-EXEC
<p>or</p>
<p>EXEC CICS<br />
RECEIVE MAP (W-CIC-CMAPCUR)<br />
MAPSET (W-CIC-CMPSCUR)<br />
FROM (MGAB10O)<br />
END-EXEC</p></td>
</tr>
<tr class="even">
<td><strong>ACCESS OPEN</strong>
<p><strong>ACCESS CLOSE</strong></p>
<p><strong>ACCESS READ</strong></p>
<p><strong>ACCESS WRITE</strong></p></td>
<td>Client/Cobol Program or its Sub object</td>
<td>CICS Dataset</td>
<td>ENDBR, DELETE,<br />
LINK, READ, READNEXT,<br />
READPREV, REWRITE,<br />
STARTBR, XCTL,<br />
WRITE
<p>ex:</p>
<p>EXEC CICS<br />
READ DATASET ('C1MASTR')<br />
INTO (BLKBLK(BLKBLK-OCUR))<br />
RIDFLD (CICS-RRN) RRN<br />
RESP (WS-RESP) END-EXEC.</p></td>
</tr>
<tr class="odd">
<td><strong>ACCESS READ</strong>
<p><strong>ACCESS WRITE</strong></p></td>
<td>Client/Cobol Program or its Sub object</td>
<td>CICS Transient Data</td>
<td>DELETEQ, READQ, WRITEQ
<p>ex:</p>
<p>EXEC CICS DELETEQ TD QUEUE (W-CIC-TSQ-LNOM)</p></td>
</tr>
</tbody>
</table>

### Structural rules

All Mainframe Analyzer related rules are provided in the default CAST
Assessment Model shipped with CAST Imaging Core:
[https://technologies.castsoftware.com/rules?sec=t\_-4&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_-4&ref=%7C%7C).

## Additional information

### Rules for resolving Paragraph names in a Section

The Mainframe Analyzer (Cobol) uses the following rules when resolving
**Paragraph names** defined in **Sections**:

1.  If the referenced Paragraph **is located** in the current Section,
    Cobol Analyzer will link the calling Paragraph to the called
    Paragraph
2.  If the referenced Paragraph **is not located** in the current
    section, Cobol Analyzer will issue a syntax error
3.  If the referenced Paragraph has a unique declaration **outside the
    Section**, Cobol Analyzer will create a link to this Paragraph.
4.  The following syntax is also resolved: "Paragraph Name IN/OF Section
    Name". A link will be created to the **correct Paragraph** (which
    will be outside the current Section).

### Access type links

Access type links are created when your Cobol program calls an
**external file** (Data Set).

1.  The instructions that manage classic files ('File Link' type) are:

    Instructions for opening a file: OPEN Reading data: READ Writing
    data: WRITE, REWRITE Closing the file: CLOSE

2.  Sorting operations on data set files (Data Link type) are carried
    out via the instructions MERGE and/or SORT. These instructions
    should generate CALL + PERFORM type links on paragraphs or sections.
    OPEN and CLOSE instructions are also used.

3.  Access (Read/Write) links to "Cobol Constants" and "Cobol Data":
    -   Variables used in the flow control: IF, PERFORM, EVALUATE,
        SEARCH.
    -   Inclusion of the instructions: MOVE, INITIALIZE, SET, CALL,
        OPEN, CLOSE, READ, WRITE, REWRITE, MERGE, SORT.
    -   The following instructions are not yet included: MULTIPLY,
        SUBTRACT, ADD, DIVIDE, EXHIBIT.

4.  CICS files read via CICS instructions in an EXEC CICS ... END-EXEC
    -   Opening of file - Instructions: STARTBR -- Acesse(Open) on the
        file
    -   Data reading - Instructions: DELETE, WRITE, REWRITE --
        Access(Write)
    -   Closing of file - Instruction: ENDBR -- Access(Close)

5.  Access to the segment in the IMS databases:
    -   Links to the segment (Access + Write/Read)
    -   Links to the Program Specification Block

**Logical Files** are declared in the **"File Section"** part of the
program with the FD or SD tag.

-   FD: declaration of  'file link' type
-   SD: declaration of 'data link' type

Example declaration of a logical file called MAIL-XREF (type FILE-LINK)
in the Cobol program:

``` text
004600 FILE SECTION.                          00015900
004700                                          00016000
009000 FD MAIL-XREF                             00020700
009100 LABEL RECORDS ARE STANDARD               00020800
009200 BLOCK CONTAINS 0 RECORDS.                00020900
009300 01 MAIL-XREF-REC.                        00021000
009400 03 MAIL-XREF-KEY.                        00021100
009500 07 MAIL-XREF-ADDRESS.                    00021200
009600 11 MAIL-XREF-ZIP-PRE PIC X(05).          00021300
009700 11 MAIL-XREF-ZIP-SUF PIC X(04).          00021400
009800 07 MAIL-XREF-STATE PIC X(03).            00021500
009900 07 MAIL-XREF-CITY PIC X(25).             00021600
010000 07 MAIL-XREF-POSTAL-CODE PIC X(02).      00021700
010100 03 MAIL-XREF-DATA PIC X(324).            00021800
```

### Replacement used in COPY REPLACING directives

-   Replacements can be applied to words or parts of words
-   Patterns used to replace parts of words must be delimited by the
    following characters:  <u>:</u>, <u>(</u>, <u>)</u>, <u>\\</u> or
    <u>"</u>
-   Patterns that are not delimited by the above characters are
    considered as being used to replace entire words
-   LEADING and TRAILING clauses mean that the replacement will be
    applied on parts of words and as such, patterns must respect rule
    two (first character and last character will be removed from the
    pattern).

### Embedded SQL support

For each embedded SQL statement found in Cobol (except INCLUDE
statements which are already supported via a CopyBook object), an object
called **Cobol SQL Query** is created. For example, the following code
will cause this object type to be created:

``` java
EXEC SQL
    SELECT ABCDED
    FROM TABLE
END-EXEC
```

### ReferLink between paragraphs/sections

A ReferLink is added between sections/paragraphs containing the CICS
command HANDLE ABEND LABEL and the sections/paragraphs specified in the
LABEL clause. For example:

![](../images/referlink1.png)

In the same way, a **ReferLink** is added for the CICS commands **HANDLE
AID** and **HANDLE CONDITION:**

![Alt text](../images/referlink2.png)

Below is a list of supported conditions:

| CICS command | Condition |
|--------------|-----------|
| HANDLE ABEND     | LABEL |
| HANDLE AID       | ANYKEY, CLEAR, CLRPARTN, ENTER, LIGHTPEN, OPERID PA1, PA2, PA3, PF1 - PF24, TRIGGER |
| HANDLE CONDITION | ALLOCERR, CBIDERR, CHANNELERR, DISABLED, DSIDERR, DSSTAT, DUPKEY, DUPREC, END, ENDDATA, ENDFILE, ENDINPT, ENQBUSY, ENVDEFERR, EOC,EODS, EOF, ERROR, EXPIRED, FUNCERR, IGREQCD, IGREQID, ILLOGIC, INBFMH, INVERRTERM, INVEXITREQ, INVLDC, INVMPSZ, INVPARTN, INVPARTNSET, INVREQ, IOERR, ISCINVREQ, ITEMERR, JIDERR, LENGERR, LOADING, LOCKED, MAPFAIL, NETNAMEIDERR, NODEIDERR, NOJBUFSP, NONVAL, NOPASSBKRD, NOPASSBKWR, NOSPACE, NOSPOOL, NOSTART, NOSTG, NOTALLOC, NOTAUTH, NOTFND, NOTOPEN, OPENERR, OVERFLOW, PARTNERIDERR, PARTNFAIL, PGMIDERR, QBUSY, QIDERR, QZERO, RDATT, RECORDBUSY, RESUNAVAIL, RETPAGE, ROLLEDBACK, RTEFAIL, RTESOME, SELNERR, SESSBUSY, SESSIONERR, SIGNAL, SPOLBUSY, SPOLERR, STRELERR, SUPPRESSED, SYSBUSY, SYSIDERR, TERMERR, TASKIDERR, TERMIDERR, TRANSIDERR, TSIOERR, UNEXPIN, USERIDERR, WRBRK |

### Support for CICS Webservices

Web services make the interactions between programs over a network.
There are two types of CICS Webservice:

-   **Provider**: CICS acting as server to provide information
-   **Requester**: CICS acting as client to request information

For example:

![](../images/664436902.png)

The items highlighted with a green oval represent the new objects and
the new links to existing Mainframe type objects. Where:

-   Cob program **ECHOCLNT:** client program to invoke the cob program
    which handles the CICS Webservice  
-   Cob program **ECHOPROG:** the cob program which handles the CICS
    Webservice as a **Provider**
-   Cob program **ECHOWEBS:** the cob program which handles the CICS
    Webservice as a **Requester**
-   **CICS Webservice** and **Call to CICS Webservice**: new objects
    which represents CICS webservice support 
-   JCL Step **LS2WS**: JCL step which run a Webservice utility batch to
    create a web service provider in CICS
-   JCL Step **WS2LS**: JCL step which run a Webservice utility batch to
    create a web service requester in CICS

#### Example Provider Webservice

**SAMPLE1.jcl** job has the **LS2WS** step which run the **DFHLS2WS**
webservice utility and exposed **ECHOPROG** cob program as a Webservice
**Provider**:

``` java
//LS2WS   EXEC DFHLS2WS,                                                        
//    JAVADIR='java142s/J1.4',                                                  
//    USSDIR='cics650',                                                         
//    PATHPREF='',                                                              
//    TMPDIR='/tmp',                                                            
//    TMPFILE='ls2ws'                                                           
//INPUT.SYSUT1 DD *                                                             
LOGFILE=<Install_Directory>/ca1p/wsbind/provider/*                              
echoProgProviderBatch.log                                                       
PDSLIB=//<INSTALL_HLQ>.CA1P.COBCOPY                                             
REQMEM=ECHOCOMM                                                                 
RESPMEM=ECHOCOMM                                                                
LANG=COBOL                                                                      
PGMNAME=ECHOPROG                                                                
URI=ca1p/echoProgProviderBatch                                                  
PGMINT=COMMAREA                                                                 
WSBIND=<Install_Directory>/ca1p/wsbind/provider/*                               
echoProgProviderBatch.wsbind                                                    
WSDL=<Install_Directory>/ca1p/wsdl/echoProgProviderBatch.wsdl                   
MAPPING-LEVEL=1.2    
```

The parameter **PGMNAME** specifies the name of the cob program that
will be exposed as a web service. The parameter **URI** specifies the
URI link for the web service. An object **CICS Webservice** with
the name as **URI** is created and linking with the **Cobol Program**
which is the handle of CICS Webservice as a **Provider**.

#### Example Requester Webservice

**SAMPLE2.jcl** job has the **WS2LS** step which run the **DFHWS2LS**
webservice utility and exposed **ECHOWEBS** cob program as a Webservice
**Requester**:

``` java
//WS2LS   EXEC DFHWS2LS,                                                        
//    JAVADIR='java142s/J1.4',                                                  
//    USSDIR='cics650',                                                         
//    PATHPREF='',                                                              
//    TMPDIR='/tmp',                                                            
//    TMPFILE='WS2LS'                                                           
//INPUT.SYSUT1 DD *                                                             
LOGFILE=<Install_Directory>/ca1p/wsbind/requester/*                             
echoProgRequesterBatch.log                                                      
PDSLIB=//<INSTALL_HLQ>.CA1P.COBCOPY                                             
REQMEM=ECHOPI                                                                   
RESPMEM=ECHOPO                                                                  
LANG=COBOL                                                                      
WSBIND=<Install_Directory>/ca1p/wsbind/requester/*                              
echoProgRequesterBatch.wsbind                                                   
WSDL=<Install_Directory>/ca1p/wsdl/echoProgRequesterBatch.wsdl                  
MAPPING-LEVEL=1.2  
```

**ECHOWEBS.cob** must consist of a CICS invoke webservice command
linking to the same webservice resource **echoProgRequesterBatch** in
the above code:

``` java
MOVE 'echoProgRequesterBatch'                                        
             TO WS-WEBSERVICE-NAME 
EXEC CICS INVOKE WEBSERVICE(WS-WEBSERVICE-NAME)                      
             CHANNEL(WS-CHANNEL-NAME)                                           
             OPERATION(WS-OPERATION-NAME)                                       
             RESP(COMMAND-RESP) RESP2(COMMAND-RESP2)                            
END-EXEC                                                 
```

**echoProgRequesterBatch.wsdl** is the webservice resource file which
must consist the **URI** for webservice:

``` java
<service name="ECHOPROGService">
      <port binding="tns:ECHOPROGHTTPSoapBinding" name="ECHOPROGPort">
         <!--This soap:address indicates the location of the Web service over HTTP.
              Please replace "my-server" with the TCPIP host name of your CICS region.
              Please replace "my-port" with the port number of your CICS TCPIPSERVICE.-->
         <soap:address location="http://my-server:my-port/ca1p/echoProgProviderBatch"/>
         <!--This soap:address indicates the location of the Web service over HTTPS.-->
         <!--<soap:address location="https://my-server:my-port/ca1p/echoProgProviderBatch"/>-->
         <!--This soap:address indicates the location of the Web service over WebSphere MQSeries.
              Please replace "my-queue" with the appropriate queue name.-->
         <!--<soap:address location="jms:/queue?destination=my-queue&connectionFactory=()&targetService=/ca1p/echoProgProviderBatch&initialContextFactory=com.ibm.mq.jms.Nojndi" />-->
      </port>
 </service>
```

An object **Call To CICS Webservice** with the name as **URI** is
created and linking with **cobol program/section** which invoke CICS
Webservice.

#### Linking CICS Webservice objects and Call To CICS Webservice objects

The **CICS Webservice** object and **Call To CICS Webservice** object
have the same **URI** for linking purposes.

#### Configuration

In order for CAST to be able to fully support CICS Webservices, the
extension **\*.wsdl **must be manually added to the **CICS File
Extensions** option in the configuration of Mainframe extension in CAST
Console:

![](../images/664436903.jpg)

Note: the file extension .wsdl will be added to the **CICS File
Extensions** option by default in a future release of CAST Core.

### Links to RPG objects

**Cobol File Link** objects will link to **DDS Display File /DDS Printer
File/ DDS Physical File / DDS Logical File** of RPG which has the same
object name:

![](../images/664436904.png)

### Links to Cobol Program objects

#### Support EXEC CICS LOAD PROGRAM

A useLink link will added from Cobol Section objects to Cobol Program
objects when the EXEC CICS LOAD PROGRAM statement is used:

``` java
EXEC CICS LOAD PROGRAM ('ME00000B')
```

![](../images/664436905.png)

#### Support EXEC CICS HANDLE ABEND PROGRAM

A referLink will be added from Cobol Section objects to Cobol Program
objects when the EXEC CICS HAND ABEND PROGRAM statement is used:

``` java
EXEC CICS HANDLE ABEND PROGRAM ('ME00000A')
```

![](../images/664436906.png)

#### Support INDEX BY

A new object type Cobol Index is created with a RelyON Link to cobol
data using INDEX BY statement, other links are created to the new Cobol
Index Object when it is referenced:

``` java
15 T-BUCH-VAR          OCCURS 1000  ASCENDING KEY
                                  T-BUCH-ST-2-20
                       INDEXED BY I-BUCH.

...

D0200.
           SET I-BUCH TO 1.
      
D0202.
           MOVE T-BUCH-VAR (I-BUCH) TO V-BUCH.
```

![Alt text](../images/664436907.png)

#### Support PROCESSING PROCEDURE

Link between paragraphs is created when they are referenced in
PROCESSING PROCEDURE statement:

``` java
 1300-XML-PARSE.                                          
             XML PARSE WS-INT-XML-RESPONSE                                      
                       ( 1 : WS-INT-XML-RESPONSE-LEN )                          
                 PROCESSING PROCEDURE                                           
                   1350-XMLEVENT-HANDLER THRU 1350-EXIT 
             END-XML                                            
```

![](../images/664436908.png)

### Embedded IMS SQL statements

For each embedded IMS SQL statement found in Cobol, an object
called **IMS SQL Query** is created. For example, the following code
will cause this object type to be created:

``` java
ENTERING-THE-PROGRAM.                                            
    EXEC SQLIMS SELECT CURRENT SERVER                            
        INTO :CURRENT-SERVER                                      
        FROM SYSIBM.SYSDUMMY1                                     
    END-EXEC    
```

![](../images/673415284.png)
