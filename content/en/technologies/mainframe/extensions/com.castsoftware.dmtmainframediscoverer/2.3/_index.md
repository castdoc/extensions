---
title: "Mainframe Discoverer - 2.3"
linkTitle: "2.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtmainframediscoverer

## What's new?

See [Release Notes](rn/) for more information.

## Extension description

This discoverer detects projects and creates the resulting Analysis Units required for analysis based on the following:

- If you deliver **Mainframe files** on disk (`.cob`, `.jcl` etc.), this extension will configure one project for the highest level folder (within the root folder) that contains files with at least one of the following file extensions:
    - Cobol: `.cob`, `.cbl`, `.pco`, `.sqb`
    - JCL: `.jcl`, `.prc`
    - IMS: `.dbd`, `.psb`, `.tra`, `.mfs`
    - CICS: `.csd`, `.cics`, `.bms`, `.wsdl`
- If you deliver **PDS dumps** on disk, this extension will configure one project regardless of the number of PDS dumps you package.

{{% alert color="info" %}}Any other file types (such as Cobol Copybooks .`cpy`) stored in the same folder as any files listed above will also be delivered and therefore included in the analysis scope.{{% /alert %}}

### Technical information

This discoverer is already embedded in CAST Imaging Core: the embedded version of the extension will not undergo any further updates and instead all functional changes/customer bug fixes will be actioned in this extension.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| CAST Imaging Core release | Supported |
|---|:-:|
| 8.4.x | :white_check_mark: |
| 8.3.x | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically installed: instead if it is required, the extension should be downloaded and installed "manually".
