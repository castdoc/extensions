---
title: "Mainframe Sensitive Data - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***
 
{{% alert color="info" %}}This document describes the extension that provides the ability to add data sensitivity markers based on key
words for objects produced by the [Mainframe Analyzer](../../com.castsoftware.mainframe). See also [Data Sensitivity](../../../../multi/data-sensitivity) for more information about other technologies that support Data Sensitivity checks.{{% /alert %}}

## Extension ID

com.castsoftware.mainframe.sensitivedata

## What's new?

See [Release Notes](rn/) for more information.

## Description

Some Mainframe objects define data and some of this data can be
sensitive, for example, information such as:

-   Salary
-   Bonus
-   First Name
-   Last Name
-   Contact details
-   etc.

This extension, when installed with the Mainframe Analyzer (≥
1.0.8), will search your Mainframe objects for specific key
words (that you define) and when a key word is found, a property will
be added to the object that marks it as sensitive.

Note that CAST Console ≥ 1.26 also provides the ability to check
data sensitive keywords for GDPR/PCI-DSS requirements.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

This extension will be automatically downloaded and installed when
Mainframe source code is delivered for analysis.

{{% alert color="info" %}}Note that if you are using the legacy workflow without Fast Scan (not required when using the Fast Scan workflow where this option is automatically enabled), you must also manually enable the Data Sensitivity option as part of the source code delivery process:
![](../images/console_option.jpg){{% /alert %}}

## Configuration instructions

### Define the .datasensitive file

After having downloaded and installed the extension and before running
a new analysis, you must first define the key words that will be
used to identify the data which you want to flag as sensitive. To do
this, you will need to create an empty text file with the extension
`.datasensitive`. You should then fill this file with your key word
definitions, using the format shown below:

- one key word per line
- three levels of sensitivity - these are case sensitive and must respect the format listed below otherwise they will be ignored:

``` java
keyword=Highly sensitive
keyword=Very sensitive
keyword=Sensitive
```

For example:

``` java
WORKINGDAYS=Sensitive
SALARY=Highly sensitive
BONUS=Highly Sensitive
FIRSTNAME=Sensitive
LASTNAME=Sensitive
PHONENO=Very sensitive
```

This extension targets data stored in the following object types:

- Cobol File Link
- JCL Dataset
- IMS Segment

The Cobol File Link object contains the data definition and the JCL
Dataset is the physical storage method. If a prototype link type is
identified between the Cobol File Link (caller) and the JCL Dataset
(callee), then both the JCL Dataset and the Cobol File Link will be
flagged as "sensitive" when a keyword is located. If this link type is
not found, then only the Cobol File Link will be flagged when a keyword
is located.

### Deliver the .datasensitive file

The `.datasensitive` file must be delivered with your Mainframe source
code. It must be stored in a folder called Database which is located
in the root folder of your delivery. If it is located anywhere else it
will be ignored. For example:

![](../images/521109528.png)

## What results can you expect?

When a `.datasensitive` file is delivered and a defined key
word matches an object name, the "sensitive" flag will be added as
an object property, and the sensitive data will be listed. This can be
seen using CAST Imaging:

### Cobol File Link

![](../images/521109527.jpg)

### JCL Dataset

![](../images/521109526.jpg)

### IMS Segment

![](../images/521109525.jpg)
