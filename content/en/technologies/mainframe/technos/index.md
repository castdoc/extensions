---
title: "Covered Technologies"
linkTitle: "Covered Technologies"
type: "docs"
weight: 2
---

## Global support

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Technology / framework supported</th>

<th class="confluenceTh">Comments</th>

</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>Cobol ANSI 85</p></td>

<td class="confluenceTd"><div class="content-wrapper">
<p>The Mainframe Analyzer is based on the Cobol 85 standard but it does
not take into account all specific Cobol extensions to this
standard.<br />
<br />
<strong>Supported Cobol Extensions</strong></p>
<ul>
<li>Terminal Format</li>
<li>Pointer Variables declaration</li>
<li>Level-78 variables (Micro Focus)</li>
<li>Command mode EXEC CICS</li>
<li>Command mode EXEC SQL</li>
<li>Command mode EXEC SQLIMS</li>
<li>Command mode EXEC DLI</li>
<li>Floating comments tag ("*&gt;")</li>
</ul>
<div>
<div>
Please ensure you check the list of Cobol dialects and their level of
support as listed
below.
</div>
</div>
<p><strong>Copybook pre-processing</strong></p>
<p>In addition to the standard COPY directive, the following specific
directives are supported:</p>
<ul>
<li>Panvalet (++INCLUDE)</li>
<li>Librarian (-INC)</li>
<li>OpenVMS (&lt;copyDir&gt;:&lt;copyName&gt;)</li>
</ul>
<p><br />
<strong>Replacement rules used in COPY REPLACING directives
are</strong>:</p>
<ol>
<li>Replacements can be applied to words or parts of word.</li>
<li>Patterns used to replace parts of words must be delimited by the
following characters: :, (, ), \ or "</li>
<li>Patterns that are not delimited by the above characters are
considered as being used to replace entire words.</li>
<li>LEADING and TRAILING clauses mean that the replacement will be
applied on parts of words and as such, patterns must respect the rule 2
(first character and last character will be removed from the
pattern).</li>
</ol>
</div></td>

</tr>
<tr class="even">
<td class="confluenceTd">JCL z/OS</td>

<td class="confluenceTd">- </td>

</tr>
<tr class="odd">
<td class="confluenceTd">IMS/DB</td>

<td class="confluenceTd"><div class="content-wrapper">
<p>The Mainframe Analyzer takes into account IMS/DB (DBD, PSB, CBLTDLI calls, CEETDLI calls, AIBTDLI
calls and EXEC DLI macros).</p>
<div>
<div>
<p>Non-relational mainframe DBMS other than IMS/DB are not supported
(ex: DATACOM, IDMS, IDS2, DMS2, ADABAS...). However, DB2 and Oracle
(ProCobol) are supported.</p>
</div>
</div>

</div></td>

</tr>
<tr class="even">
<td class="confluenceTd">IMS/DC</td>

<td class="confluenceTd"><div class="content-wrapper">
<p>IMS/DC is taken into account. Support for:</p>
<ul>
<li><p><a
href="https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_applctn_macro.htm"
style="text-decoration: none;" rel="nofollow">APPLCTN macro</a></p></li>
<li><p><a
href="https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_transact_macro.htm"
style="text-decoration: none;" rel="nofollow">TRANSACT
macro</a></p></li>
<li><p>Links from Cobol to IMS Transactions</p></li>
</ul>
<div>

</div>
</div></td>

</tr>
<tr class="odd">
<td class="confluenceTd">CICS</td>

<td class="confluenceTd">-</td>

</tr>
</tbody>
</table>

## Detailed support

### COBOL

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><p>Platform / OS</p></th>
<th class="confluenceTh"><p>COBOL dialect</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td rowspan="2" class="confluenceTd">IBM z/OS</td>
<td class="confluenceTd"><p>Enterprise COBOL for z/OS - up to
v6</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><ul>
<li>No problem</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IBM Cobol for OS/390</p>
VS COBOL II</td>
<td class="confluenceTd">Medium</td>
<td class="confluenceTd"><ul>
<li>Source code implemented with these compilers can be analyzed with
the Mainframe Analyzer</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>IBM i</p></td>
<td class="confluenceTd"><p>COBOL/400</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><ul>
<li>Some constructs related to screens and transactions are not taken
into account but this does not cause problems for the Mainframe Analyzer
nor the Metric Assistant.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Unisys A Series</p></td>
<td class="confluenceTd"><p>Unisys Cobol</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Copybook inclusion mechanism is not supported.</li>
<li>Program calls are not correctly managed.</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Bull DPS7 (GCOS7)</p>
<p>Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>GCOS Cobol</p></td>
<td class="confluenceTd"><p>Low</p></td>
<td class="confluenceTd"><ul>
<li>DML is not supported.</li>
<li>Screens are not supported.</li>
<li>Transactions are not supported.</li>
<li>Some constructs must/should be removed from the source code because
they can cause parsing problems.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Tandem NonStop</p></td>
<td class="confluenceTd"><p>HP COBOL for TNS/E Programs</p></td>
<td class="confluenceTd"><p>Medium</p></td>
<td class="confluenceTd"><ul>
<li>Transactions and screens are not supported.</li>
<li>Some constructs must/should be removed from the source code because
they can cause parsing problems.</li>
<li>CAST rules specific to CICS/IMS (DL/I), file descriptors and
SORT/MERGE may produce false violations.</li>
</ul></td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><p>Unix, AIX, Windows</p></td>
<td class="confluenceTd"><p>Microfocus </p></td>
<td class="confluenceTd"><p>High <br />
<br />
</p></td>
<td rowspan="2" class="confluenceTd"><ul>
<li>Definition and statements associated to screens are not support but
this does not cause problems for the Mainframe Analyzer nor the Metric
Assistant. </li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">ACUCOBOL-GT</td>
<td class="confluenceTd">Medium</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Windows</p></td>
<td class="confluenceTd"><p>Cobol .NET</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Object COBOL is not supported.</li>
<li>It is possible to use a Universal Analyzer profile to extract some
information.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">OpenVMS</td>
<td class="confluenceTd">HP Cobol for OpenVMS</td>
<td class="confluenceTd">Medium</td>
<td class="confluenceTd"><ul>
<li>Copy directive is supported</li>
<li>COBOL 85 syntax is supported</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Stratus VOS</td>
<td class="confluenceTd">VOS Cobol</td>
<td class="confluenceTd">Medium</td>
<td class="confluenceTd"><ul>
<li>Some constructs must/should be removed from the source code because
they can cause parsing problems.</li>
</ul></td>
</tr>
</tbody>
</table>

>Note that some AIP quality rules specifically target IBM style
COBOL, therefore you may find that results of these rules may not be
pertinent if you are using a different compiler such as Microfocus.
Rules which target IBM style COBOL usually quote this in the "Reference"
section of the rule documentation, e.g.:

![](597295124.jpg)

### JCL

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><p>Platform (OS)</p></th>
<th class="confluenceTh"><p>Control Language</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>JCL</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IBM i (AS/400)</p></td>
<td class="confluenceTd"><p>CL</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>CL/CLP is supported thanks to <a
href="https://extend.castsoftware.com/#/extension?id=com.castsoftware.rpg&amp;version=latest"
rel="nofollow">IBM RPG Analyzers</a></p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Unisys A Series</p></td>
<td class="confluenceTd"><p>Workflow</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>Workflows are implemented in Algol language
(yes, this language is still used) so it is difficult to analyze it with
a Universal Analyzer profile/extension.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Bull DPS7 (GCOS7)</p>
<p>Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>JCL</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>A Universal Analyzer profile/extension could
be developed to take GCOS8 job into account. This is more difficult for
GCSO7.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Tandem/HP</p></td>
<td class="confluenceTd"><p>JCL</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>Difficult to analyze with a Universal
Analyzer profile/extension.</p></td>
</tr>
</tbody>
</table>

### Transaction Managers

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><p>Transaction Manager</p></th>
<th class="confluenceTh"><p>Platform (OS)</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"><p>CICS</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS) <br />
IBM i (AS/400)</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IMS/DC</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><ul>

<li>Support for:
<ul>
<li><p><a
href="https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_applctn_macro.htm"
style="text-decoration: none;" rel="nofollow">APPLCTN macro</a></p></li>
<li><p><a
href="https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.sdg/ims_transact_macro.htm"
style="text-decoration: none;" rel="nofollow">TRANSACT
macro</a></p></li>
<li>Links from Cobol to IMS Transactions</li>
</ul></li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>TDS</p>
<p><br />
</p>
<p>TP8</p></td>
<td class="confluenceTd"><p>Bull DPS7 (GCOS7)</p>
<p>Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Transactional calls between programs are performed via the variable
NEXT-TPR defined in the LINKAGE section. The Dynamic Link Manager can
draw links between programs from this variable.</li>
<li>Initialization file could be analyzed with a Universal Analyzer
profile.</li>
</ul></td>
</tr>
</tbody>
</table>

### DBMS

<table class="wrapped relative-table confluenceTable"
style="width: 100.0%;">
<colgroup>
<col style="width: 4%" />
<col style="width: 10%" />
<col style="width: 4%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh"><p>DBMS</p></th>
<th class="confluenceTh"><p>Platform (OS)</p></th>
<th class="confluenceTh"><p>Support</p></th>
<th class="confluenceTh"><p>Comment</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"><p>Db2</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS) <br />
IBM i (AS/400)<br />
Unix <br />
Windows</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p>EXEC SQL … END-EXEC macros</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>ORACLE</p></td>
<td class="confluenceTd"><p>Unix</p></td>
<td class="confluenceTd"><p>High</p></td>
<td class="confluenceTd"><p>EXEC SQL … END-EXEC macros for
Pro*Cobol.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>IMS/DB</p></td>
<td class="confluenceTd"><p>IBM zSeries (zOS)</p></td>
<td class="confluenceTd"><p>Medium</p></td>
<td class="confluenceTd"><ul>
<li>Access to PCBs are detected for main programs that are directly associated to a PSB and sub-programs that are called by these main programs where the PCBs are passed as arguments of the CALL statements in these main programs.</li>

<li>EXEC SQLIMS … END-EXEC macros</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IDMS</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>Often used in conjunction with ADSO.</li>
<li>Although unsupported in CAST AIP "out of the box", there is scope to
establish links between the COBOL code and the IDMS through a variety of
advanced Content Enrichment tools available in CAST AIP such as
Reference Patterns, CAST Transaction Configuration Center etc.</li>
<li><p>The IDMS statements inserted in COBOL programs may raise warning
messages in the analysis log, however, these will not impact the
analysis results (i.e. metamodel generation or rule
evaluation).</p></li>
<li>A custom extension has been developed for this platform, however it
is a User
Community extension and is therefore not officially supported by
CAST.</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>DATACOM</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>It is often used in conjunction with the
IDEAL programming language.</p>
<p>A custom extension has been developed for IDEAL/Datacom, however it
is a User
Community extension and is therefore not officially supported by
CAST.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>ADABAS</p></td>
<td class="confluenceTd"><p>IBM zSeries (z/OS) <br />
Unix <br />
Windows</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><p>A custom extension has been developed for
this platform, however it is a User
Community extension and is therefore not officially supported by
CAST.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>IDS2</p></td>
<td class="confluenceTd"><p>Bull DPS7 (GCOS7) <br />
Bull DPS8 (GCOS8)</p></td>
<td class="confluenceTd"><p>No</p></td>
<td class="confluenceTd"><ul>
<li>The DML verbs inserted in COBOL programs can cause problems. A
Universal Analyzer profile/extension can be implemented to analyze DDL
and DMCL files.</li>
<li>Some DML verbs can be parsed by using the Reference Pattern. Please
note that dynamic programming is often used. As a consequence, simple
analysis techniques won't deliver interesting results.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>DMS2</p></td>
<td class="confluenceTd"><p>Unisys A Series</p></td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd"><ul>
<li>This DBMS is similar to IDS2 but for the UNISYS A Series platform.
See remarks for IDS2.</li>
<li>A custom extension has been developed for this platform, however it
is a User
Community extension and is therefore not officially supported by
CAST.</li>
</ul></td>
</tr>
</tbody>
</table>
