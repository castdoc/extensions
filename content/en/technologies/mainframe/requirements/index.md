---
title: "Requirements"
linkTitle: "Requirements"
type: "docs"
weight: 3
---

To successfully deliver and analyze Mainframe technologies, the
following third-party software is required:

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh">Install on workstation running the AIP Console
/ DMT (for delivery)</th>
<th class="confluenceTh"><p>Install on workstation running AIP Node /
CMS (for analysis)</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>Nothing required</strong></p>
<p>AIP Console/CAST Delivery Manager Tool only requires the location of
the Mainframe files for delivery.</p>
</div></td>
<td class="confluenceTd"><strong>Nothing required</strong></td>
</tr>
</tbody>
</table>