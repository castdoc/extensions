---
title: "TIBCO - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.tibco

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for TIBCO BusinessWorks and ActiveMatrix BusinessWorks applications.

## In what situation should you install this extension?

If your application contains TIBCO source code and you want to view these object types and their links with other objects, then you should install this extension.

## Supported Versions of TIBCO

This version of the extension provides partial support for:

| TIBCO | Version |  Supported |
|---|---|:-:|
| TIBCO BusinessWorks | 5.1 - 5.13 | :white_check_mark: |
| TIBCO ActiveMatrix  | 6.0 - 6.2  | :white_check_mark: |

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-------------------------------:|:------------------:|
|        :white_check_mark:       | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed when you deliver PL/1 code. You can manage the extension using
the Application - Extensions interface.

## Prepare and deliver the source code

Only files with following extensions will be analyzed:

| TIBCO BusinessWorks 5 | TIBCO ActiveMatrix BusinessWorks 6 |
|---|---|
|  `*.aeschema`<br><br>`*.sharedjdbc`<br><br>`*.javaschema`<br><br>`*.sharedhttp`<br><br>`*.sharedjmscon`<br><br>`*.process`<br><br>`*.sharedjmsapp` | `*.bwp`<br><br>`*.MF`<br><br>`*.substvar`<br><br>`*.httpclientresource`<br><br>`*.httpconnresource`<br><br>`*.jmsconnresource`<br><br>`*.jdbcresource`<br><br>`*.mf`<br><br>`*.xsd` |

### TIBCO BusinessWorks 5.x

TIBCO BusinessWorks 5.x applications contain two types of source code:

-   standard TIBCO source code
-   embedded Java source code (i.e. .process files)

As such, two analyzers will be used to handle the different source
code. When using the legacy CAST Management Studio/Delivery Manager Tool
(DMT), you should also deliver each type of code in a specific way as
shown below (this does not apply when using AIP Console):

| Source code type | Analyzer that will be used | Legacy DMT - option to use |
|------------------|----------------------------|----------------------------|
| TIBCO            | Universal Analyzer         | Files on your system option    |
| Java             | JEE Analyzer               | TIBCO Library extractor option |

In BusinessWorks 5.x the source code for the Java Code activity is
embedded in the ActiveMatrix and therefore requires specific extraction
and preprocessing, as such, .process and .aeschema files need to
be preprocessed. The LISA folder will be used to analyse the
preprocessed files.

### TIBCO ActiveMatrix BusinessWorks 6

TIBCO ActiveMatrix BusinessWorks 6 source files are parsed with the SDK
and do not need any preprocessing, therefore when using the legacy CAST
Management Studio/Delivery Manager Tool (DMT) there is no need to
specifically extract Java code. This is because in BusinessWorks 6.x,
Java code is now created externally in the "src" folder of
the AppModule and is then referred to by the Java Invoke Activity when
the MultiOutput check box is selected. In BusinessWorks 5.x the source
code for the Java Code activity is embedded in the ActiveMatrix and
therefore requires specific extraction and preprocessing.

## Analysis configuration and execution

There are no analysis/technology configuration options available for
Shell, however you should check that at least one TIBCO analysis unit
has been created as shown below.

AIP Console exposes the technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Universal Technology (3) in the
Config (1) \> Analysis (2) tab to display the available options for your TIBCO source code:

![](../images/530317371.jpg)

Then choose the relevant Analysis Unit (1) to view the
configuration:

![](../images/530317374.jpg)

![](../images/530317375.jpg)

## What results can you expect?

For any TIBCO Process, Start and End activity will have special icons
and will be used for Start and End points of the transaction.

### Links

For Processes including Groups such as as the following: 

![](../images/383891399.png)

The analysis result will look like this:

![](../images/383891400.png)

Access to database objects will be resolved with typed links:

![](../images/383891401.png)

For Processes with services which are implemented using onMessage:

![](../images/383891402.png)

The result will look like this:

![](../images/383891403.png)

### Objects

The following table lists the objects produced by the TIBCO extension:

| Icon | Object Type |  TIBCO ActiveMatrix BusinessWorks 6 | TIBCO BusinessWorks 5 |
|---|---|:---:|:---:|
| ![](../images/383891404.png) | TIBCO Activity | :white_check_mark: | :white_check_mark: |
| ![](../images/383891405.png) | TIBCO Process End | :white_check_mark: | :white_check_mark: |
| ![](../images/383891406.png) | Activity Input | :x: | :white_check_mark: |
| ![](../images/383891407.png) | Activity Output | :x: | :white_check_mark: |
| ![](../images/383891408.png) | Activity Starter | :white_check_mark: | :white_check_mark: |
| ![](../images/383891409.png) | AE Class | :x: | :white_check_mark: |
| ![](../images/383891410.png) | AE Field | :white_check_mark: | :white_check_mark: |
| ![](../images/383891411.png) | AE Schema | :white_check_mark: | :white_check_mark: |
| ![](../images/383891412.png) | Directory/Folder | :white_check_mark: | :white_check_mark: |
| ![](../images/383891413.png) | Group | :x: | :white_check_mark: |
| ![](../images/383891414.png) | Variable | :x: | :white_check_mark: |
| ![](../images/383891415.png) | Process | :white_check_mark: | :white_check_mark: |
| ![](../images/383891416.png) | Process Variable | :x: | :white_check_mark: |
| ![](../images/383891417.png) | Shared Connection | :white_check_mark: | :white_check_mark: |
| ![](../images/383891418.png) | Shared JMS Connection | :white_check_mark: | :white_check_mark: |
| ![](../images/383891419.png) | TIBCO Process Transition | :x: | :white_check_mark: |
| ![](../images/383891420.png) | Variable Group | :white_check_mark: | :white_check_mark: |
| ![](../images/383891421.png) | TIBCO Service | :white_check_mark: | :x: |
| ![](../images/383891422.png) | TIBCO Service Operation | :white_check_mark: | :x: |

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---------|------|
| 1.2.6 | [https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=\|\|1.2.6](https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=%7C%7C1.2.6) |
| 1.2.5 | [https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=\|\|1.2.5](https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=%7C%7C1.2.5) |
| 1.2.4 | [https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=\|\|1.2.4](https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=%7C%7C1.2.4) |
| 1.2.3 | [https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=\|\|1.2.3](https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=%7C%7C1.2.3) |
| 1.2.2 | [https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=\|\|1.2.2](https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=%7C%7C1.2.2) |
| 1.2.1 | [https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=\|\|1.2.1](https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=%7C%7C1.2.1) |
| 1.2.0 | [https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=\|\|1.2.0](https://technologies.castsoftware.com/rules?sec=srs_tibco&ref=%7C%7C1.2.0) |

You can also find a global list
here: [https://technologies.castsoftware.com/rules?sec=t_1018000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1018000&ref=%7C%7C).
