---
title: "SAP BusinessObjects - 1.0"
linkTitle: "Release Notes"
type: "docs"
---

## 1.0.6
### Updates
SAPBOA-58 - A change has been implemented in preparation for the future support of encrypted SSL connections to CAST Storage Service/PostgreSQL.

## 1.0.5
### Resolved issues
| Internal ID | Ticket ID |                                                       Description                                                       |                                                                                                                                     Impact?                                                                                                                                     |
|-------------|-----------|-------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  SAPBOA-51  |   20286   | Crash during an analysis with the error [Error in Analysis:The program ExternalLinkCommandLine has not ended correctly] | A bug in the analyzer (the ID for BO Condition objects was not being correctly populated) was causing the ID of the most recent object analyzed to be used when a BO Condition object was encountered. This caused the analysis to crash in error. This bug has now been fixed. |

## 1.0.4
### Updates

SAPBOA-34 - A change has been implemented to introduce a connectivity layer compatible with PostgreSQL 10 and 11.

### Resolved issues

| Internal ID | Call ID |                          Description                           |                                                                                                                                                                              Impact?                                                                                                                                                                               |
|-------------|---------|----------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  SAPBOA-45  |  16340  |      Missing links between BO Objects and Oracle Objects       | Direct links between BO Universes and database tables are no longer displayed Instead, links now go from BO Universes via the BO object (BO Dimension, BO Detail, BO Measure) to the database table Transactions will be impacted Currently no bookmark is available for the links between the BO objects and the database table (will be fixed in future release) |
|  SAPBOA-40  |  16069  | BO Wrong description - AFP stands for Automated, not Automatic |                                                                                                                                                                                 -                                                                                                                                                                                  |
|  SAPBOA-39  |  16078  |      Missing flow from BO Universe to the Oracle database      |                                                      BO Universes are no longer classed as Transaction entry points in CAST Transaction Configuration Center Instead, BO Classes without sub-classes are now Transaction entry points in CAST Transaction Configuration Center Transactions will be impacted                                                       |
