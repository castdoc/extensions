---
title: "SAP BusinessObjects - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.businessobject

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications implemented in SAP
BusinessObjects.

## When should you install this extension?

If your application contains SAP BusinessObjects source code and you
want to view corresponding object types and their links with other
objects, then you should install this extension.

## Information for users of the legacy BO Analyzer delivered with Core 8.3

When working with Core 8.3.x, the legacy BO Analyzer (provided out-of-the-box) is no longer available, therefore to analyze SAP BusinessObjects, you must use the SAP BusinessObjects Analyzer and the [CAST SAP BusinessObjects Extractor](https://doc.castsoftware.com/display/DOCCOM/CAST+SAP+BusinessObjects+Extractor).

### What is the difference between this Extension and the legacy BO Analyzer?

The "legacy" analyzer requires you to do the following:

-   Install SAP BusinessObjects (the BO Designer module) on the
    workstation on which the analysis is being run from.
-   Manually define a connection to the SAP BusinessObjects instance
    on which your Universe files are used - this instance must be
    available during the analysis process - this is known as an "online" analysis.

These requirements can sometimes present significant difficulties for
the process of on-boarding a SAP BusinessObjects application. As a direct result and to simplify the process, this extension is
introduced. The extension helps you to analyze SAP BusinessObjects
source code "offline", i.e. no connection to the SAP BusinessObjects
instance is required during the analysis - instead output from a
standalone CAST SAP BusinessObjects Extractor is delivered for analysis. In addition, you no longer need to have SAP BusinessObjects (the BO Designer module) installed on the node.

### Analysis result expectation

Both the legacy BO Analyzer and the SAP
BusinessObjects Analyzer extension produce the same results in
terms of objects/links and Quality Rule grades, if the two are
used to analyze the same application source code.

### Switching from legacy to new delivery/analysis method

If you have upgraded to a release of Core that supports the CAST
SAP BusinessObjects Analyzer extension, and have SAP BusinessObjects
analysis and snapshot results (in an upgraded Application) generated
using the legacy code delivery and analysis method, you can switch
delivery and analysis to the CAST SAP BusinessObjects Analyzer
extension and the standalone CAST SAP BusinessObjects Extractor.
Switching to the new method produces the same results as the legacy BO
Analyzer provided "out-of-the-box", therefore, there
is no impact to grades and measures.

To switch:

Use the standalone CAST SAP BusinessObjects Extractor to extract the
same Universes (\*.unv) and Documents (\*.rep - where applicable) that
you delivered using the legacy method. In the CAST Management Studio,
reject the delivery that corresponds to your legacy BusinessObjects
version (click to enlarge):

Manage the delivery to open the CAST Delivery Manager:

In the CAST Delivery Manager Tool, delete the package corresponding to
the legacy packaging method:

![](../images/248353283.jpg)

Add a new package and this time use the new delivery option "Use
existing CAST extractor output" to package the output from the
standalone CAST SAP BusinessObjects Extractor - see [Packaging and
delivery from scratch](#SAPBusinessObjectsAnalyzer1.0-scratch).
Deliver the package and take a snapshot as normal.

Note that when working with CAST AIP ≤ 8.2.x, once you have switched
from the "legacy" to the CAST SAP BusinessObjects Analyzer
extension, it is not possible to return to the legacy code delivery
and analysis method.

## Workflow with extension

The analysis workflow with the SAP BusinessObjects extension is as
follows: 

1.  The standalone CAST SAP BusinessObjects Extractor is run. This takes
    the raw Universe (and Documents) files in a local or network folder
    as input, it then uses the SAP BusinessObjects instance to interpret
    these raw Universe (and Documents) files and finally transforms them
    into a format that can be understood by the analyzer provided in the
    SAP BusinessObjects Analyzer extension.
2.  The output of the standalone CAST SAP BusinessObjects Extractor is
    delivered, either via AIP Console or via the CAST Delivery
    Manager Tool.
3.  Delivery is accepted either in AIP Console or in the CAST
    Management Studio and Analysis Units are created.
4.  Analysis is completed.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions and components

The SAP BusinessObjects Analyzer extension has one dependent
extension which provides the "discovery" element. This dependent
extension is automatically downloaded when you use CAST Console:

- com.castsoftware.dmtboextractiondiscoverer

The CAST SAP BusinessObjects Extractor is not delivered with the SAP
BusinessObjects Analyzer extension and you will need to obtain this
separately. See the
[documentation](https://doc.castsoftware.com/display/DOCCOM/CAST+SAP+BusinessObjects+Extractor)
for more information.

## Detailed technology support

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Language version</th>
<th class="confluenceTh">Supported</th>
<th class="confluenceTh">Supported by reference</th>
<th class="confluenceTh">Deprecated support</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>BusinessObjects XI</p></td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">BusinessObjects XI R2</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">BusinessObjects XI 3.0</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">BusinessObjects XI 3.1</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>SAP Business Intelligence BusinessObjects
4.0</p></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">SAP Business Intelligence BusinessObjects
4.1</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

## Prepare, deliver and analyze your source code

Once the extension is installed, no further configuration changes are
required before you can package your source code and run an analysis.
The process of packaging, delivering and analyzing your source code is
as described below.

Ensure the CAST SAP BusinessObjects Extractor has been run before you
deliver your source code.

### Source code discovery

The SAP BusinessObjects Analyzer extension does not include a
discovery mechanism within the extension itself. However, a
dependent extension containing the discovery mechanism is downloaded
and installed automatically for you. Discovery functions as follows: 

-   One \*.unv file fed into the standalone [CAST SAP
    BusinessObjects
    Extractor](https://doc.castsoftware.com/display/DOCCOM/CAST+SAP+BusinessObjects+Extractor) results
    in the output of one \*.bxml file.
-   One \*.bxml file fed into AIP Console or the CAST Delivery
    Manager Tool results in one project detected during source
    code delivery.
-   One project detected during source code delivery results in
    one Analysis Unit in the AIP Console or in the CAST Management
    Studio.

### Using CAST Console

-   Supported in AIP Console v. ≥ 1.11
-   See [Application
    onboarding](https://doc.castsoftware.com/display/ONBRD/Application+onboarding) for
    more information about delivery your source code and performing an
    analysis/snapshot.

#### Source code delivery

AIP Console expects either a ZIP/archive file or source code
located in a folder configured in AIP Console. You should include in
the ZIP/source code folder all SAP Business Objects source code that has
already been extracted by the [CAST SAP BusinessObjects
Extractor](https://doc.castsoftware.com/display/DOCCOM/CAST+SAP+BusinessObjects+Extractor):

-   \*.bxml

CAST highly recommends placing the files in a folder dedicated to SAP
BusinessObjects. If you are using a ZIP/archive file, zip the folders in
the "temp" folder - but do not zip the "temp" folder itself, nor create
any intermediary folders:

``` java
D:\temp
    |-----SAPBusinessObjects
    |-----OtherTechno1
    |-----OtherTechno2
```

#### Configuring the analysis in CAST Console

AIP Console exposes the Technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Business Objects Technology to display the available options:

![](../images/445972526.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same as at Application level) that allow you to
        make fine-grained configuration changes.

You should check that settings are as required and that at least one
Analysis Unit exists for the specific technology.

![](../images/445972525.jpg)

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Dependencies</th>
<td class="confluenceTd">Dependencies are configured
at <strong>Application level</strong> for<strong> each
technology</strong> and are configured between <strong>individual
Analysis Units/groups of Analysis Units </strong>(dependencies between
technologies are not available as is the case in the CAST Management
Studio). You can find out more detailed information about how to ensure
dependencies are set up correctly, in Validate dependency
configuration for CAST Console.</td>
</tr>
<tr class="even">
<th class="confluenceTh">Analysis Units</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>For SAP BusinessObjects, there are <strong>no Analysis Unit level
settings available</strong>. You can however:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<th class="confluenceTh"><div class="content-wrapper">
<img src="../images/445972524.jpg" draggable="false"
data-image-src="../images/445972524.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="445972524"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="analyze.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" height="45" />
</div></th>
<td class="confluenceTd"><p>Analyze a specific Analysis Unit (and all
Analysis Units within the Execution Unit), specifically for testing
purposes.</p></td>
</tr>
<tr class="even">
<th class="confluenceTh"><div class="content-wrapper">
<img src="../images/445972523.jpg" draggable="false"
data-image-src="../images/445972523.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="445972523"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="toggle.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" height="28" />
</div></th>
<td class="confluenceTd">Disable or enable the analysis of a specific
Analysis Unit, when the next Application level analysis/snapshot is run.
By default all Analysis Units are always set to
<strong>enable</strong>. When an Analysis Unit is set to disable, this
is usually a temporary action to prevent an Analysis Unit from being
analyzed.</td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
</tbody>
</table>

## What are the expected results?

After the analysis/snapshot generation is complete, you can view the
results in the normal manner:

### CAST Enlighten

![](../images/248353295.png)

### SAP BusinessObjects objects and links

The following section lists the objects and links between
objects that the BusinessObjects analyzer is capable of detecting and
storing in the CAST Analysis Service:

Note that the SAP BusinessObjects Analyzer extension uses the same
metamodel as the legacy BO Analyzer.

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Metamodel name</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353282.png" draggable="false"
data-image-src="../images/248353282.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353282"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_PROJECT32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Project</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353281.png" draggable="false"
data-image-src="../images/248353281.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353281"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_SUBSET32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Subset</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353279.png" draggable="false"
data-image-src="../images/248353279.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353279"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_CLASS32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353280.png" draggable="false"
data-image-src="../images/248353280.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353280"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_CONDITION32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Condition</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353277.png" draggable="false"
data-image-src="../images/248353277.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353277"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_LISTOFVALUE32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO List of Value</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353278.png" draggable="false"
data-image-src="../images/248353278.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353278"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_OBJECT_DET32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Detail</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353276.png" draggable="false"
data-image-src="../images/248353276.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353276"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_OBJECT_DIM32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Dimension</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353275.png" draggable="false"
data-image-src="../images/248353275.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353275"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_OBJECT_IND32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Measure</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/248353274.png" draggable="false"
data-image-src="../images/248353274.png"
data-unresolved-comment-count="0" data-linked-resource-id="248353274"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="BO_UNIVERSE32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="248353273"
data-linked-resource-container-version="15" /></p>
</div></td>
<td class="confluenceTd"><p>BO Universe</p></td>
</tr>
</tbody>
</table>

#### Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><div>
Link Type
</div></th>
<th class="confluenceTh"><div>
When is this type of link created?
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><strong>Refer (R)</strong></td>
<td class="confluenceTd">Between two linked Universes</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Use Select (Us)</strong></td>
<td class="confluenceTd">Between a Universe and a database
table/view</td>
</tr>
</tbody>
</table>

#### Structural rules

Rules that the SAP BusinessObjects Analyzer extension supports are
listed and documented
in [https://technologies.castsoftware.com/rules?sec=t\_-23&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_-23&ref=%7C%7C)
These are the same Quality Rules used with the legacy SAP BusinessObjects analyzer provided "out-of-the-box" in CAST AIP.
