---
title: "RPG language analysis results"
linkTitle: "RPG language"
type: "docs"
---

## SQL RPG fix format example

``` java
     PDelFactArc       b                   Export
      /Free
        W_Requete = 'Select * From Viewsq';
      /End-Free     
     C/Exec Sql
     C+ Prepare S1 From :W_Requete
     C/End-Exec
    PDelFactArc       e     
```

![](../../../images/rpg/rpg_exec_sql_fixform_example.jpg)

## SQL RPG free format example

``` java
         SqlStmt = 'Select * From Viewsq';

         Exec Sql
         Prepare p1 From :SqlStmt ;
```

![](../../../images/rpg/rpg_exec_sql_freeform_example.jpg)

![](../../../images/rpg/rpg_exec_sql_freeform_props_example.jpg)

## RPG IV Missing Physical/Logical File

In absence of Table/View with name matching:

![](../../../images/rpg/rpg_missing_disk_file_example.png)

In presence of Table/View with name matching:

![](../../../images/rpg/rpg_disk_file_example.png)


The RPG IV Missing Physical/Logical File "APAGLQUA" object is created at
analyzer level because com.castsoftware.rpg cannot know at that point
that a Table or View exists resulting from a
com.castsoftware.sqlanalyser analysis. At application level, a matching
protocol is used to link "RPG IV File Disk" to the "Table" and delete
the RPG IV Missing Physical/Logical File only if a match has been found.

## RPG III/IV Work Station 

``` java
"     Fgrpabol1  if   e           k disk                                         "
"     Fwdgrpabo  cf   e             workstn sfile(SFL:ligne_sfl)                 "
"     F                                     sfile(SFL01:Å“Å“rang1)               "
"      *====================================================================*    "
```
Similarly to CL language for CL Display File, the RPG III and RPG IV Work Station expected results are:

![](../../../images/rpg/rpg_workstation.png)

## DataQ RPG example

``` java
I              'INKOOP    '          C         DTAQ

IINKOOP      DS
I                                        1   3 CTRL
I                                        4   9 BLVNR
C           *ENTRY    PLIST
C                     PARM           PLVNR   6
C                     PARM           DTAQL  10
C                     MOVEL'BEL'     CTRL
C                     MOVELPLVNR     BLVNR
C                     CALL 'QSNDDTAQ'
C                     PARM DTAQ      PDTAQ  10
C                     PARM DTAQL     PDTAQL 10
C                     PARM 9         LEN     50
C                     PARM           INKOOP
C                     MOVE *ON       *INLR
C                     RETRN
```

![](../../../images/rpg/rpg_main_to_dataQ_publisher.jpg)

``` java
d DQLib           s             10    inz('*LIBL')
d DQName          s             10    inz('CONVMAIL')
d DQLen           s              5  0 inz(512)
*
c     snddtq        begsr
c                   call      'QSNDDTAQ'
c                   parm                    DQName
c                   parm                    DQLib
c                   parm                    DQLen
c                   parm                    DQData
c                   endsr
*
```

![](../../../images/rpg/rpg_sub_to_dataQ_publisher.jpg)

``` java
dcl-pr QSNDDTAQ extpgm ;
  *n char(10) const ;  //Data queue name
  *n char(10) const ;  //Library
  *n packed(5) ;       //Length of data
  *n char(100) ;       //Data
end-pr ;
  
for QData.Counter = 1 to 10 ;
    QData.Field1 = %char(QData.Counter) ;
    QData.Field2 = %char(QData.Counter * 10) ;
    QData.Field3 = %char(QData.Counter * 100) ;
 
    QSNDDTAQ('TESTDTAQ':'MYLIB':LengthOfData:QData) ;
endfor ;
```

![](../../../images/rpg/rpg_main_to_dataQ_publisher_bis.jpg)

``` java
dcl-pr QSNDDTAQ extpgm ;
  *n char(10) const ;  //Data queue name
  *n char(10) const ;  //Library
  *n packed(5) ;       //Length of data
  *n char(100) ;       //Data
end-pr ;

BegSr srDTAQ;
   CALLP(e) QSNDDTAQ('TESTDTAQ':'MYLIB':LengthOfData:QData);
EndSr;
```

![](../../../images/rpg/rpg_sub_to_dataQ_publisher_bis.jpg)

## IBM MQ RPG example

``` java
001 C                   EVAL      ODON = TestQueue                                            UTT07R


     P  GetEqMsg       B                   export                                             UTT07R
                                                                                              UTT07R
      Procedure interface                                                                  UTT07R
      -------------------                                                                  UTT07R
     D GetEqMsg        PI         12000A                                                      UTT07R
     D  Queue                        48A   value                                              UTT07R
     D  Remove                        1A   value options(*nopass)                             UTT07R
     D  WaitMillis                   10I 0 value options(*nopass)                             UTT07R
                                                                                              UTT07R
      Local variables                                                                      UTT07R
      ---------------                                                                      UTT07R
                                                                                              UTT07R
     *  The last-used queue, held in static storage for persisitence over calls              UTT07R
     D LastQueue       S                   like(Queue) static                                 UTT07R
                                                                                              UTT07R
     *  Local error code                                                                     UTT07R
     D GetError        S              4A                                                      UTT07R
                                                                                              UTT07R
     * 0Reply from user if there is an error                                                 UTT07R
     D Reply           S              1A                                                      UTT07R
                                                                                              UTT07R
     *  Alpha versions of reason code                                                        UTT07R
     D ReasonA         DS             9                                                       UTT07R
     D ReasonA4                1      4A                                                      UTT07R
                                                                                              UTT07R
     *  The data returned from MQSeries                                                      UTT07R
     D MQMessage       S          12000A                                                      UTT07R
                                                                                              UTT07R
     *  Has there been a handle error                                                        UTT07R
     D HandleErr       S              1A                                                      UTT07R
                                                                                              UTT07R
      Parameter lists                                                                      UTT07R
      ---------------                                                                      UTT07R
                                                                                              UTT07R
     * MQ get message from queue (receive message)                                           UTT07R
     C     PLMQGET       PLIST                                                                UTT07R
     C                   PARM                    CID                                          UTT07R
     C                   PARM                    HCONN                                        UTT07R
     C                   PARM                    HOBJ                                         UTT07R
     C                   PARM                    MQMD                                         UTT07R
     C                   PARM                    MQGMO                                        UTT07R
     C                   PARM                    BUFLEN                                       UTT07R
     C                   PARM                    MQMessage                                    UTT07R
     C                   PARM                    MESLEN            9 0                        UTT07R
     C                   PARM                    CCODE                                        UTT07R
     C                   PARM                    REASON                                       UTT07R
                                                                                              UTT07R
      Procedure processing                                                                 UTT07R
      --------------------                                                                 UTT07R
                                                                                              UTT07R
     *  Connect to the queue manager to ensure connection                                    UTT07R
     C                   Eval      ReasonA4 = ConnectMQM( QueueManager )                      UTT07R
                                                                                              UTT07R
     *  If the connection failed dump and exit                                               UTT07R
B001 C                   if        ReasonA4 <> *blanks                                        UTT07R
 001 C                   dump                                                                 UTT07R
 001 C                   return    GET_ERROR                                                  UTT07R
E001 C                   ENDif                                                  ReasonA4 <> *bUTT07R
                                                                                              UTT07R
     *  Open MQSeries queue                                                                  UTT07R
     C                   eval      ReasonA4 = OpenQueue( Queue )                              UTT07R
                                                                                              UTT07R
     *  If the called procedures reported an error, dump and exit                            UTT07R
B001 C                   if        ReasonA4 <> *blanks                                        UTT07R
 001 C                   dump                                                                 UTT07R
 001 C                   return    GET_ERROR                                                  UTT07R
E001 C                   ENDif                                                  ReasonA4 <> *bUTT07R
                                                                                              UTT07R
     *  Use the queue name to get the MQSeries object handle                                 UTT07R
     C                   eval      HOBJ = GetHandle( Queue )                                  UTT07R
                                                                                              UTT07R
     *  Set up the options for an MQGET call                                                 UTT07R
                                                                                              UTT07R
     *  Set to 'Get Message'                                                                 UTT07R
     C                   Z-ADD     MQGET         CID   
```

The value of the parameter ODON is evaluated. This parameter holds the
name of the queue. Here are three examples of how the name of the queue
can be declared.

``` java
    D TestQueue       S             10    INZ('TESTQUEUE    ')                              UTT07R                                                                                              
001 C                   EVAL      ODON = TestQueue                                            UTT07R
```

![](../../../images/rpg/ibmmq_receiver_to_proc.jpg)

``` java
001 C                   EVAL      TestQueue = 'TESTQUEUE'                                            UTT07R
001 C                   EVAL      ODON = TestQueue                                            UTT07R
```

![](../../../images/rpg/ibmmq_receiver_to_proc.jpg)

``` java
001 C                   EVAL      TestQueue = $TESTQUEUE                                            UTT07R
001 C                   EVAL      ODON = TestQueue                                            UTT07R
```

If there is no file in the application where $TESTQUEUE is declared, we
create an unknown queue object.

![](../../../images/rpg/ibmmq_unknown_receiver_to_proc.jpg)

## REST RPG examples

Support of *http_get, http_put, http_post, http_delete* APIs : example
for *http_get* API

``` java
/free
    url = 'http://example.com:8500/cust';
    rc = http_get(url: options);
    if (rc<>1 and rc<>500);
        http_crash();
    endif;
    if rc=500;
        xml-into errMsg %xml(stmf: 'path=error doc=file');
        dsply errMsg;
    else;
        xml-into custInfo %xml(stmf: 'path=result/cust doc=file');
        dsply custInfo.name;
        dsply custInfo.street;
        dsply ( custInfo.city + ' '
                + custInfo.state + ' '
                + custInfo.postal );
    endif;
    unlink(stmf);
    *inlr = *on;
/end-free
```

![](../../../images/rpg/rpg_get_service.jpg)

Support of *http_string* API

``` java
dcl-proc translate;
      ...
   url = 'https://gateway.platform.net/language-translator/api' 
       + '/v3/translate?version=2018-05-01';

   monitor;
      response = http_string('POST': url: request: 'application/json');
      http_error(*omit: httpstatus);
      httpcode = %char(httpstatus);
   on-error;
      httpcode = http_error();
   endmon;

     ...
end-proc;
```

![](../../../images/rpg/rpg_post_service.jpg)

Support of *http_url_get* and *http_url_post* APIs : example for
*http_url_get* API

``` java
 /free                                                                                                  
 
   url = 'http://rss.test.com/rss/test.rss';                                                    
   filename = 'http_file.xml';                                                                  
                                                                                                          
   rc = http_url_get( url : filename );                                                                 
   if (rc <> 1);                                                                                        
      PrintLine = http_error();                                                                         
      except;                                                                                           
      unlink(filename);                                                                                 
      return;                                                                                           
   endif;                                                                                               
                                                                                                                                                                                          
 /end-free     
```

![](../../../images/rpg/rpg_get_service_bis.jpg)

## REST services with Embedded SQL

``` java
dcl-proc translate;
  url = 'http://example.com:8500/cust';
   exec SQL
     select SYSTOOLS.HTTPPOSTCLOB(:url, :hdr, :request)
       into :response
       from SYSIBM.SYSDUMMY1;
end-proc;
```

![](../../../images/rpg/rpg_post_within_exec_sql.jpg)

``` java
dcl-proc translate;
  url = 'http://example.com:8500/cust';
   ...
   exec SQL
     values QSYS2.HTTP_GET(:url, :hdr, :request)
       into :response
       from SYSIBM.SYSDUMMY1;
end-proc;  
```

![](../../../images/rpg/rpg_get_within_exec_sql.jpg)

## IBM-supplied AXISC routines

Support of AXISC routine : *axiscTransportSetProperty* API with
AXISC_PROPERTY_HTTP_METHOD parameter

``` java
dcl-proc translate;
 
   dcl-s url      varchar(2000);
   dcl-s request  varchar(2000);
   dcl-s response varchar(5000);
   dcl-s rcvBuf   char(5000);
   dcl-s rc       int(10);
   dcl-s propName char(200);
   dcl-s propVal  char(200);
   dcl-s transportHandle pointer;
 
   userid = 'apikey';
   password = '8OD0RY71zMwXjkNiBLzTN8i848R9wXMOmADjuvoKY2zw';
 
   url = 'https://gateway.platform.net/language-translator/api' 
       + '/v3/translate?version=2018-05-01';
 
   transportHandle = axiscTransportCreate(url: AXISC_PROTOCOL_HTTP11);
   if (transportHandle = *null);
     failWithError(transportHandle: 'axiscTransportCreate');
   endif;
 
   propName = 'POST' + x'00';
   axiscTransportSetProperty( transportHandle
                            : AXISC_PROPERTY_HTTP_METHOD
                            : %addr(propName));
 
end-Proc;
```

![](../../../images/rpg/axisc_post.jpg)