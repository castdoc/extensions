---
title: "RPG - 3.4"
linkTitle: "3.4"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.rpg

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for applications written using RPG languages and/or CL languages.

## Supported languages

-   RPG III (also known as GAP 3)
-   RPG IV (fixed-form and free format)
-   CL

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed when at least one RPG type file is delivered for analysis.

## Prepare and deliver the source code

When the extension is downloaded and installed, you can now package your source code and run an analysis. The process of preparing and delivering your source code is described below:

### Source code preparation

Before the RPG source code can be delivered and then analyzed, it needs to be collected from the proper iSeries libraries and transferred to the designated location. During this operation, each artifact (program, CL, DSPF) must be put into a single file only. This will result in a single artifact per file. Furthermore, the type of source code must be expressed using the extension to the file. The appropriate file extensions are listed in the following table. 

Source code is kept in a member of a file. The file is kept in a library. Each file can have many members. Each member is the source code for a program. The source code files can have any name but conventionally the names start with Q and end with SRC, for source.

| Source code type          | iSeries library | Required extension               | Used in MetaModel   |
|---------------------------|-----------------|----------------------------------|---------------------|
| RPG-III programs          | QRPGSRC         | \*.rpg, \*.rpg38                 | RPG300              |
| ILE RPG Programs          | QRPGLESRC       | \*.rpgle                         | RPG400              |
| ILE RPG Programs with SQL | QSQLRPGLESRC    | \*.sqlrpgle                      | RPG400              |
| Copy source members       | QCPYLESRC       | \*.cpyle                         | RPG400              |
| CL programs               | QCLSRC          | \*.cl, \*.clp, \*.cl38, \*.clp38 | CL400               |
| ILE CL Programs           | QCLLESRC        | \*.clle                          | CL400               |
| Display Files             | QDDSSRC         | \*.dspf, \*.dspf38               | DDS400              |
| Printer Files             | QDDSSRC         | \*.prtf, \*.prtf38               | DDS400              |
| Logical Files             | QDBSRC          | \*.lf, \*.lf38                   | SQL Analyzer/DDS400 |
| Physical Files            | QDBSRC          | \*.pf, \*.pf38                   | SQL Analyzer/DDS400 |

The RPG Analyzer is able to autodetect RPG III versus RPG IV, so there is no difference in using \*.rpg or \*.rpgle. What is very important is to distinguish between Display, Printer, Logical and Physical files using the specific file extensions listed in above table.

### What about IBM Db2 for i source code?

In previous releases of the RPG Analyzer extension, when IBM Db2 for i source code was delivered, a dedicated extraction process was launched within the RPG Analyzer to transform this source code into a format that could be analyzed. Starting in release ≥ 3.1.x, the RPG Analyzer is no longer responsible for analyzing source code from IBM DB2 for i. Instead, this task is undertaken by the
[com.castsoftware.sqlanalyzer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.sqlanalyzer&version=latest) (release ≥ 3.5.4 is required).

There is no change to source code delivery process when using AIP Console - the IBM DB2 for i source code should be delivered together
with the RPG source code and AIP Console will be able to detect this type of source code and create the necessary Analysis Units. For those using legacy CAST Management Studio/Delivery Manager Tool, the source code delivery process has changed and is explained below.

IBM Db2 for i is also known as DB400.

## Deliver the source code

### Using CAST Imaging Console

RPG is supported in CAST Console ≥ 1.22.

CAST Imaging Console expects either a ZIP/archive file or source code located in a folder configured in Console. You should include in
the ZIP/source code folder all RPG and IBM DB2 for i (if present) source code. CAST highly recommends placing the files in a folder dedicated to RPG. If you are using a ZIP/archive file, zip the folders in the "temp" folder - but do not zip the "temp" folder itself,
nor create any intermediary folders:

``` java
D:\temp
    |-----RPG
    |-----OtherTechno1
    |-----OtherTechno2
```

When the source code has been delivered, CAST Imaging Console will create an Analysis Unit for:

-   IBM Db2 for i (if present)
-   RPG

![](../images/mainpage/old_analysis_unit.png)

## Analysis configuration

Add a dependency between the RPG Analysis Unit as Source and SQL Universal Analysis Unit as the target:

![](../images/mainpage/old_dependencies.jpg)

Starting from CAST Imaging Console 1.26, this dependency will be automatically added and no manual intervention is required. You should
still check that the dependency is present, however.

### Configuring Technical Size measures for RPG in the CAST Health Dashboard

Technical Size measures can be displayed for RPG analysis results in the CAST Health Dashboard by manually editing the following file:

``` text
1.x WAR file: %CATALINA_HOME%\webapps\<dashboard>\portal\resources\app.json
2.x WAR file: %CATALINA_HOME%\webapps\<dashboard>\WEB-INF\classes\config\hd\app.json
2.x ZIP file: com.castsoftware.aip.dashboard.2.0.0\config\hd\app.json
```

Add the following entries into the existing section "TechnicalSizeMeasures":

``` java
"TechnicalSizeMeasures": {
...
      {
        "id": 1008000,
        "label": "Number of RPG400 Program(RPG400)"
      },
      {
        "id": 1008001,
        "label": "Number of RPG400 Subroutine(RPG400)"
      },
      {
        "id": 1008002,
        "label": "Number of RPG400 Procedure(RPG400)"
      },
      {
        "id": 1008003,
        "label": "Number of RPG IV Copy Member(RPG400)"
      },
      {
        "id": 1009000,
        "label": "Number of RPG300 Program(RPG300)"
      },
      {
        "id": 1009001,
        "label": "Number of RPG300 Subroutine(RPG300)"
      },
      {
        "id": 1009003,
        "label": "Number of RPG300 Copy Member(RPG300)"
      }
} 
```

Following any changes you make, save the app.json file and then restart your application server so that the changes are taken into account.

## What results can you expect?

### Transaction Sample

``` java
C     SCRLVL        CHAIN     PACCLVS
C                   IF        %FOUND
C                   DELETE    RACCLV
C                   ENDIF
```

![](../images/mainpage/transaction_example_1.png)

``` java
CALL       PGM(&EXTLIB/&EXTPGM)
```

![](../images/mainpage/transaction_example_2.png)

### CL language

See [CL Language](results/cl/) for more information.

### RPG Language

See [RPG Language](results/rpg/) for more information.

### Objects

#### CL400

|                           Icon                            | Object Name |
|:---------------------------------------------------------:|-------------|
|          ![](../images/icones/program_icon.png)           | CL Program                         |
|          ![](../images/icones/project_icon.png)           | CL Project                        |
|         ![](../images/icones/subroutine_icon.png)         | CL Subroutine                     |
|     ![](../images/icones/call_to_unknown_program.png)     | CL Call to Generic Program        |
|            ![](../images/icones/exec_sql.png)             | CL EXEC SQL                       |
|       ![](../images/icones/cl_call_to_sql_file.png)       | CL Call to Sql File               |
|      ![](../images/icones/data_queue_publisher.png)       | CL Data Queue Publisher           |
|       ![](../images/icones/data_queue_receiver.png)       | CL Data Queue Receiver            |
|  ![](../images/icones/unknown_data_queue_publisher.png)   | CL Data Unknown Queue Publisher   |
|   ![](../images/icones/unknown_data_queue_receiver.png)   | CL Data Unknown Queue Receiver    |
|       ![](../images/icones/cl_ibmmq_publisher.png)        | CL IBM MQ Queue Publisher         |
|        ![](../images/icones/cl_ibmmq_receiver.png)        | CL IBM MQ Queue Receiver          |
|   ![](../images/icones/cl_unknown_ibmmq_publisher.png)    | CL IBM MQ Unknown Queue Publisher |
|    ![](../images/icones/cl_unknown_ibmmq_receiver.png)    | CL IBM MQ Unknown Queue Receiver  | 
|            ![](../images/icones/disk_file.jpg)            | CL Disk File                      |
|            ![](../images/icones/disk_file.jpg)            | CL File Display                   |
|        ![](../images/icones/dds_physical_file.jpg)        | CL Missing File Disk              |
| ![](../images/icones/missing_display_printer_and_dds.jpg) | CL Missing Display File           |

#### DDS400

|                           Icon                            | Object Name             |
|:---------------------------------------------------------:|-------------------------|
|          ![](../images/icones/project_icon.png)           | DDS Project             |
|           ![](../images/icones/dds_section.png)           | DDS Section             |
| ![](../images/icones/missing_display_printer_and_dds.jpg) | DDS Printer File        |
| ![](../images/icones/missing_display_printer_and_dds.jpg) | DDS Display File        |
|       ![](../images/icones/dds_join_structure.png)        | DDS Join Structure      |
|        ![](../images/icones/dds_logical_file.png)         | DDS Logical File        |
|        ![](../images/icones/dds_physical_file.jpg)        | DDS Physical File       |
|          ![](../images/icones/dds_record_lf.png)          | DDS Record Structure LF |
|          ![](../images/icones/dds_record_pf.png)          | DDS Record Structure PF |

#### RPG 300

| Icon | Object Name |
|:----:|-------------|
| ![](../images/icones/project_icon.png)  | RPG III Project                        |
| ![](../images/icones/program_icon.png)  | RPG III Program                        |
|  ![](../images/icones/subroutine_icon.png)      | RPG III MainSubroutine                 |
|  ![](../images/icones/subroutine_icon.png)     | RPG III Subroutine                     |
| ![](../images/icones/disk_file.jpg)   | RPG III File Printer                   |
| ![](../images/icones/disk_file.jpg)  | RPG III File Disk                      |
| ![](../images/icones/disk_file.jpg)   | RPG III File Workstn                   |
| ![](../images/icones/disk_file.jpg)  | RPG III File Special                   |
| ![](../images/icones/disk_file.jpg)   | RPG III File Seq                       |
| ![](../images/icones/copy_member.jpg)  | RPG III Copy Member                    |
| ![](../images/icones/rpg_iii_sql_statement.jpg)  | RPG III SQL Statement/Structure        |
| ![](../images/icones/rpg_procedure_icon.jpg)    | RPG III Procedure                      |
| ![](../images/icones/call_to_unknown_program.png)  | RPG III Call to Generic Program        |
| ![](../images/icones/exec_sql.png)             | RPG III EXEC SQL                       |
| ![](../images/icones/copy_member.jpg)             | RPG III Missing Copy Member            |
| ![](../images/icones/dds_physical_file.jpg)  | RPG III Missing Physical/Logical File  |
| ![](../images/icones/missing_display_printer_and_dds.jpg)  | RPG III Missing Display File           |
| ![](../images/icones/missing_display_printer_and_dds.jpg)  | RPG III Missing Printer File           |
| ![](../images/icones/data_queue_publisher.png)  | RPG III Data Queue Publisher           |
| ![](../images/icones/data_queue_receiver.png)  | RPG III Data Queue Receiver            |
| ![](../images/icones/unknown_data_queue_publisher.png)  | RPG III Data Unknown Queue Publisher   |
| ![](../images/icones/unknown_data_queue_receiver.png)  | RPG III Data Unknown Queue Receiver    |
| ![](../images/icones/data_queue_publisher.png)  | RPG III IBM MQ Queue Publisher         |
| ![](../images/icones/data_queue_receiver.png)  | RPG III IBM MQ Queue Receiver          |
| ![](../images/icones/unknown_data_queue_publisher.png)  | RPG III IBM MQ Unknown Queue Publisher |
| ![](../images/icones/unknown_data_queue_receiver.png)  | RPG III IBM MQ Unknown Queue Receiver  |

#### RPG 400

|                           Icon                            | Object Name |
|:---------------------------------------------------------:|-------------|
|           ![](../images/icones/project_icon.png)      | RPG IV Project                        |
|           ![](../images/icones/program_icon.png)            | RPG IV Program                        |
|         ![](../images/icones/subroutine_icon.png)          | RPG IV MainSubroutine                 |
|          ![](../images/icones/subroutine_icon.png)             | RPG IV Subroutine                     |
|            ![](../images/icones/disk_file.jpg)            | RPG IV File Printer                   |
|            ![](../images/icones/disk_file.jpg)            | RPG IV File Disk                      |
|            ![](../images/icones/disk_file.jpg)            | RPG IV File Workstn                   |
|            ![](../images/icones/disk_file.jpg)            | RPG IV File Special                   |
|            ![](../images/icones/disk_file.jpg)            | RPG IV File Seq                       |
|       ![](../images/icones/rpg_procedure_icon.jpg)        | RPG IV Procedure                      |
|       ![](../images/icones/rpg_proc_proto_icon.jpg)       | RPG IV Procedure Prototype            |
|            ![](../images/icones/copy_member.jpg)            | RPG IV Copy Member                    |
|        ![](../images/icones/call_to_unknown_program.png)           | RPG IV Call to Generic Program        |
|        ![](../images/icones/exec_sql.png)              | RPG IV EXEC SQL                       |
|         ![](../images/icones/rpg_module_icon.jpg)         | RPG IV Module                         |
|           ![](../images/icones/copy_member.jpg)           | RPG IV Missing Copy Member            |
|    ![](../images/icones/dds_physical_file.jpg)     | RPG IV Missing Physical/Logical File  |
| ![](../images/icones/missing_display_printer_and_dds.jpg) | RPG IV Missing Display File           |
|            ![](../images/icones/missing_display_printer_and_dds.jpg)       | RPG IV Missing Printer File           |
|       ![](../images/icones/rpg_procedure_icon.jpg)        | RPG IV Missing Procedure              |
|      ![](../images/icones/data_queue_publisher.png)       | RPG IV Data Queue Publisher           |
|       ![](../images/icones/data_queue_receiver.png)       | RPG IV Data Queue Receiver            |
|  ![](../images/icones/unknown_data_queue_publisher.png)   | RPG IV Data Unknown Queue Publisher   |
|   ![](../images/icones/unknown_data_queue_receiver.png)   | RPG IV Data Unknown Queue Receiver    |
|      ![](../images/icones/data_queue_publisher.png)       | RPG IV IBM MQ Queue Publisher         |
|       ![](../images/icones/data_queue_receiver.png)       | RPG IV IBM MQ Queue Receiver          |
|  ![](../images/icones/unknown_data_queue_publisher.png)   | RPG IV IBM MQ Unknown Queue Publisher |
|   ![](../images/icones/unknown_data_queue_receiver.png)   | RPG IV IBM MQ Unknown Queue Receiver  |
|            ![](../images/icones/rpg_iv_get_service.png)                 | RPG IV Get Resource Service           |
|              ![](../images/icones/rpg_iv_post_service.png)                 | RPG IV Post Resource Service          |
|              ![](../images/icones/rpg_iv_put_service.png)           | RPG IV Put Resource Service           |
|              ![](../images/icones/rpg_iv_delete_service.png)             | RPG IV Delete Resource Service        |

### Analysis log messages

The following list of log messages may occur during an analysis and will help locate in which file the warning occured. In addition, just before the [Analyzer Statistical Report](#analyzer-statistical-report) is displayed in the log file, the extension will report the total number of occurences of these messages, for example: 

```
Info   : [com.castsoftware.rpg] RPG-001: Cannot resolve copy GFCOLEC/QRPGSRC/GF_OBTCN_L -- 1 occurrences
Info   : [com.castsoftware.rpg] RPG-001: Cannot resolve copy QRPGLESRC/HTTPAPI_H -- 5 occurrences
Info   : [com.castsoftware.rpg] RPG-002: Cannot resolve DDS file SGGQGC00 -- 5 occurrences
Info   : [com.castsoftware.rpg] RPG-002: Cannot resolve DDS file SGGQGD00 -- 5 occurrences
Info   : [com.castsoftware.rpg] RPG-002: Cannot resolve DDS file SGSPLZ00 -- 2 occurrences
Info   : [com.castsoftware.rpg] RPG-003: Cannot resolve program RTNFECHA -- 5 occurrences
Info   : [com.castsoftware.rpg] RPG-003: Cannot resolve program SGG4648C -- 1 occurrences
Info   : [com.castsoftware.rpg] RPG-003: Cannot resolve program SGS99500 -- 5 occurrences
Info   : [com.castsoftware.rpg] RPG-003: Cannot resolve program TBL50400 -- 4 occurrences
```

#### RPG-001

| Item | Description |
| --- | --- |
| **Identifier** | RPG-001 |
| **Message** | Cannot resolve copy \{RefName\} -- \{count\} occurrences |
| **Severity** | Info |
| **Explanation** | Analyzer could not find the source file included by a /copy statement. This can lead to various missing results. |
| **User Action** | Ensure that all required source code is packaged and delivered. The missing copy can also be in an external library in that case ensure to provide it. |

#### RPG-002

| Item | Description |
| --- | --- |
| **Identifier** | RPG-002 |
| **Message** | Cannot resolve DDS file \{Refname\} -- \{count\} occurrences |
| **Severity** | Info |
| **Explanation** | Analyzer could not find a DDS file (pf, lf, dspf, prtf, ...). The analysis results will be degraded. A fallback mechanism will link missing PF/LF references to available SQL Table & View Objects. |
| **User Action** | Ensure that all required source code is packaged and delivered. |

#### RPG-003

| Item | Description |
| --- | --- |
| **Identifier** | RPG-003 |
| **Message** | Cannot resolve program \{Refname\} -- \{count\} occurrences |
| **Severity** | Info |
| **Explanation** | Analyzer could not find a program. The analysis results will be incomplete. |
| **User Action** | Ensure that all required source code is packaged and delivered. The program can also be in a different technology (COBOL, etc...) in that case it will be linked in a later analysis step. The program may also be external to the application, that is outside of the application boundaries. In this case it should not be delivered (the RPG analyzer created a placeholder object of type CL/RPG III/RPG IV Call to Generic Program). |
 
#### RPG-004

| Item | Description |
| --- | --- |
| **Identifier** | RPG-004 |
| **Message** | Cannot resolve procedure \{Refname\} -- \{count\} occurrences |
| **Severity** | Info |
| **Explanation** | Analyzer could not find a procedure. |
| **User Action** | Ensure that all required source code is packaged and delivered. |

### Structural Rules

The following structural rules are provided:

|  Release | Link |
|----------|------|
| 3.4.8-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.8-funcrel) |
| 3.4.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.7-funcrel) |
| 3.4.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.6-funcrel) |
| 3.4.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.4-funcrel) |
| 3.4.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.4-funcrel) |
| 3.4.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.3-funcrel) |
| 3.4.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.2-funcrel) |
| 3.4.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.1-funcrel) |
| 3.4.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.0-funcrel) |
| 3.4.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.0-beta1) |
| 3.4.0-alpha3 | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.0-alpha3](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.0-alpha3) |
| 3.4.0-alpha2 | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.0-alpha2](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.0-alpha2) |
| 3.4.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=\|\|3.4.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_rpg&ref=%7C%7C3.4.0-alpha1) |

### Analyzer Statistical Report

At the end of an analysis, a statistical report is printed in the log file displaying the number of saved objects and links.
For the *com.castsoftware.rpg* extension, this report splits the results by object type. 

For links, the report uses the information stored in the analysis schema **(caller object type, link type, callee object type)** to provide a detailed view of links created by the extension.

An example statistical report is shown below:

``` text
Info   : [com.castsoftware.rpg] Stats Report:
Info   : [com.castsoftware.rpg] Number of Objects per type:
Info   : [com.castsoftware.rpg] 'RPG IV Call to Generic Program'       : 4 
Info   : [com.castsoftware.rpg] 'RPG IV File Disk'                     : 12 
Info   : [com.castsoftware.rpg] 'RPG IV MainSubroutine'                : 5 
Info   : [com.castsoftware.rpg] 'RPG IV Missing Copy Member'           : 2 
Info   : [com.castsoftware.rpg] 'RPG IV Missing Physical/Logical File' : 3 
Info   : [com.castsoftware.rpg] 'RPG IV Post Resource Service'         : 5 
Info   : [com.castsoftware.rpg] 'RPG IV Program'                       : 5 
Info   : [com.castsoftware.rpg] 'RPG IV Subroutine'                    : 15 
Info   : [com.castsoftware.rpg] Number of Links per link flags:
Info   : [com.castsoftware.rpg] 'RPG IV File Disk'       -- relyonLink    --> 'RPG IV Missing Physical/Logical File' : 12
Info   : [com.castsoftware.rpg] 'RPG IV MainSubroutine'  -- callLink      --> 'RPG IV Subroutine'                    : 11
Info   : [com.castsoftware.rpg] 'RPG IV Program'         -- callLink      --> 'RPG IV MainSubroutine'                : 5
Info   : [com.castsoftware.rpg] 'RPG IV Program'         -- includeLink   --> 'RPG IV Missing Copy Member'           : 6
Info   : [com.castsoftware.rpg] 'RPG IV Subroutine'      -- callLink      --> 'RPG IV Call to Generic Program'       : 15
Info   : [com.castsoftware.rpg] 'RPG IV Subroutine'      -- useSelectLink --> 'RPG IV File Disk'                     : 2
Info   : [com.castsoftware.rpg] 'RPG IV Subroutine'      -- callLink      --> 'RPG IV Post Resource Service'         : 5
Info   : [com.castsoftware.rpg] 'RPG IV Subroutine'      -- callLink      --> 'RPG IV Subroutine'                    : 8
```

## Known limitations

- SOAP services are not supported
- With regard to RPG language:
    - Dynamic calls are not supported
    - Dynamic SQL is partially supported since 3.3.0-alpha2 (`prepare` and `execute immediate` with variables are resolved when encountered in RPG IV free format; the support for RPG IV fixed format and CL is available since 3.3.0-alpha3)
    - If you code the `/FREE` or `/END-FREE` directive, it will be ignored by the compiler, but the syntax of the directive will be checked - see the [official documentation reference](https://www.ibm.com/docs/en/i/7.3?topic=directives-free-end-free#freefree). The com.castsoftware.rpg extension requires BOTH directives (i.e. a closing `/END-FREE` directive if there is an opening `/FREE` directive and vice-versa) in the source code: a missing opening or closing directive will produce traceback issues and missing objects. To fix the issue ensure you add the missing `/FREE` or `/END-FREE` appropriately within the file for it to be properly analyzed.
