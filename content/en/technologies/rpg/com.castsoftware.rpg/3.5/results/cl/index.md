---
title: "CL language analysis results"
linkTitle: "CL language"
type: "docs"
---

## DataQ CL example

``` java
/*/
/* QSNDDTAQ and QRCVDTAQ example for DataQ                           */
/*/
             PGM
             DCL VAR(&DQNAME) TYPE(*CHAR) LEN(10) VALUE('FILEINFO')
             DCL VAR(&DQLIB) TYPE(*CHAR) LEN(10) VALUE('QGPL')
             DCL VAR(&DQSNDLEN) TYPE(*DEC) LEN(5 0) VALUE(14)
             DCL VAR(&DQLEN) TYPE(*DEC) LEN(5 0)
             DCL VAR(&DQSNDDATA) TYPE(*CHAR) LEN(100)
             DCL VAR(&DQDATA) TYPE(*CHAR) LEN(100)
             DCL VAR(&DQWAIT) TYPE(*DEC) LEN(5 0) VALUE(0)
             CHGVAR VAR(&DQSNDDATA) VALUE('THIS IS A TEST')
             CALL QSNDDTAQ PARM(&DQNAME &DQLIB &DQSNDLEN &DQSNDDATA)
             CALL QRCVDTAQ PARM(&DQNAME &DQLIB &DQLEN &DQDATA &DQWAIT)
             SNDPGMMSG MSGID(CPF9898) MSGF(QCPFMSG) MSGDTA(&DQDATA)
             ENDPGM
```


![](../../../images/cl/CL_program_to_dataQ_publisher.jpg)

![](../../../images/cl/CL_dataQ_receiver_to_program.jpg)

## IBM MQ CL example

``` java
CALL PGM(QMQMSAMP/AMQ3GET4) PARM('Queue_Name','Queue_Manager_Name')
```

![](../../../images/cl/CL_ibmmq_receiver_to_program.jpg)

``` java
CALL PGM(QMQMSAMP/AMQ3PUT4) PARM('Queue_Name','Queue_Manager_Name')
```

![](../../../images/cl/CL_program_to_ibmmq_publisher.jpg)

## SQL CL examples

``` java
PGM
RUNSQL SQL('PREPARE MYSTMT FROM SELECT * FROM VIEWSQ')
ENDPGM
```

![](../../../images/cl/cl_exec_sql_example.jpg)

## SQL Script CL example

``` java
PGM
RUNSQLSTM  SRCFILE(CGSRC/QSQLSRC) SRCMBR(S_REQ46) +
           COMMIT(*NC)
ENDPGM
```

![](../../../images/cl/cl_call_to_sql_file_example.png)

>Note: The callLink between "CL Call to Sql File" and "SQL Script" is created by the [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest) (≥ 1.7.30).

## STRQMQRY CL example

``` java
PGM
    STRQMQRY   QMQRY(MYLIB/S_REQ46)  QMFORM(FORM1)
ENDPGM
```
![](../../../images/cl/cl_call_to_sql_file_example.png)
>Note: The basic support of STRQMQRY only looks at the member name within the QMQRY parameters to create the **CL Call to Sql File** object

>Note: The callLink between "CL Call to Sql File" and "SQL Script" is created by the [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest) (≥ 1.7.30).



## DCLF command example

Two modelisations have been made for DCLF instructions based on the type of file. DCLF commands can declare **database file** or **display device file**. To distinguish between both types of file, we must look at the presence of other commands:

| Command   | available to file type | description |
|-----------|------------------------|---------------------|
|   RCVF    | both                   | read the file within the CL program | 
|   SNDF    | display device file    | write the file within the CL program | 
|   SNDRCVF | display device file    | write and then read the file within the CL program | 

Also, when multiple declarations of DCLF are present in the same file, then a matching OPNID or RCDFMT must be present between a specific DCLF command and for example a RCVF command.

### Disk File (alias for database file)
For the following sample file 'TAACFGAC.clp':

``` java 
    PGM
      DCLF FILE(QADSPOBJ)
      DCL VAR(&MBRNAME) TYPE(*CHAR) LEN(10)
      DCL VAR(&MBR) TYPE(*CHAR) LEN(10)
    ENDPGM
```
The associated results:

![](../../../images/cl/MissingDiskFile.png)


When the **DDS Physical File** or **DDS Logical File** is present the **CL Missing Disk File**  is replaced by the **DDS Physical File** or **DDS Logical File** object and an additional link is made from the CL program to the **DDS Physical File** or **DDS Logical File**. 

Also, If **Table** or **View** are found by the [com.castsoftware.sqlanalyzer](../../../../../sql/extensions/com.castsoftware.sqlanalyzer) extension then these objects are prioritized as follows:

![](../../../images/cl/DiskFileToTable.png)

> if **View** will match with the **DDS Logical File** while **Table** with **DDS Physical File**.

### Display File

``` java 
  PGM                                                                        
  DCLF FILE(TAADTQCD)                                                        
  DCL VAR(&DTAQ) TYPE(*CHAR) LEN(10) VALUE(DTQ1)                             
  DCL VAR(&LIB) TYPE(*CHAR) LEN(10) VALUE(*LIBL)                             
  DCL VAR(&ENTLEN) TYPE(*DEC) LEN(5 0)
  DCL VAR(&ENTRY) TYPE(*CHAR) LEN(80)
  SNDF RCDFMT(FMT1)
  CHGVAR VAR(&RCDINPUT) VALUE('')                                                                                   
  CHGVAR VAR(&WAIT) VALUE(99999)                                                                                     
  CALL PGM(QRCVDTAQ) PARM(&DTAQ &LIB &ENTLEN &ENTRY &WAIT)                                                           
  IF COND('*DSPF' *EQ %SST(&ENTRY 1 5)) THEN(DO)                                                                     
  RCVF RCDFMT(FMT1)   
  ENDPGM
```
![](../../../images/cl/MissingDisplay.png)


> The ***Read Access Link*** is added based on the ***RCVF*** command when the ***Write Access Link*** is added based on the ***SNDF*** command. In case of ***SDNRCVF*** then both ***Read Access Link*** and ***Write Access Link*** are added.  


## Data Area Example

Two modelisations have been made for Data Area instruction based on the type of Data Area. 
Data Area are either local or non-local. 
There is five commands available to manipulate the Data Area listed in the below table:

| Command   | Link Type         | description            |
|-----------|-------------------|------------------------|
| CRTDTAARA | Access Link       | create the Data Area   | 
| CHGDTAARA | Write Access Link | update the Data Area   | 
| RTVDTAARA | Read Access Link  | retrieve the Data Area | 
| DSPDTAARA | Read Access Link  | display the Data Area  | 
| DLTDTAARA | Write Access Link | delete the Data Area   | 

### Local Data Area (*LDA)

``` java 
     DCL        VAR(&QNOM) TYPE(*CHAR) LEN(10)                                                                  
    /*=========================================================*/                                                       
    /* RECHERCHE ENVIRONNEMENT DE TRAVAIL                      */                                                       
    /*=========================================================*/                                                       
     RTVDTAARA  DTAARA(*LDA (24 2)) RTNVAR(&SEC)    
```

![data_area1.png](../../../images/cl/data_area1.png)

> The ***Read Access Link*** is added based on the ***RTVDTAARA*** command. 
> The parent of Local Data Area (*LDA) are the Program containing them.

### Non-Local Data Area
#### Example with constant string 
``` java 
    RTVDTAARA  DTAARA(DMAMELERR *ALL) RTNVAR(&DATANO)                                                          
    CHGVAR     VAR(&QMDATANO) VALUE(&QUOTE *TCAT &DATANO +                                                     
                 *TCAT &QUOTE)                                                                                 
    RTVSYSVAL  SYSVAL(QDATE) RTNVAR(&QDATE)                                                                    
    RTVSYSVAL  SYSVAL(QTIME) RTNVAR(&QTIME)                                                                    
    CALLSUBR   SUBR(EXTRACTION)                                                                                
    CVTDAT     DATE(&QDATE) TOVAR(&DATE) FROMFMT(*SYSVAL) +                                                    
                 TOFMT(*YYMD) TOSEP('-')                                                                       
    CHGVAR     VAR(&DATANO) VALUE(&DATE *TCAT '-' *TCAT +                                                      
                 %SST(&QTIME 1 2) *TCAT '.' *TCAT +                                                            
                 %SST(&QTIME 3 2) *TCAT '.' *TCAT +                                                            
                 %SST(&QTIME 5 2) *TCAT '.000000')                                                             
    CHGDTAARA  DTAARA(DMAMELERR *ALL) VALUE(&DATANO)                                                           
```

![data_area2.png](../../../images/cl/data_area2.png)

> The ***Read Access Link*** is added based on the ***RTVDTAARA*** command 
> when the ***Write Access Link*** is added based on the ***CHGDTAARA***.
> The parent of Non-Local Data Area are the project containing them.

#### Example with variable

``` java 
     DSPDTAARA DTAARA(&OBJLIB/&OBJ) OUTPUT(*PRINT)   
```

![data_area3.png](../../../images/cl/data_area3.png)

> The ***DTAARA*** parameter determine the name of the CL Data Area Object. 
> Evaluation is attempted on the both Qualifier (i.e. the Library and the Name). 
> The variable names for both Qualifier are used when evaluation is not able to retrieve them, respectively.

 

Limitations: 

- If evaluation of the file type failed, the default type is 'Disk File'.
- If evaluations of the data area failed, the default name is the variable name. 