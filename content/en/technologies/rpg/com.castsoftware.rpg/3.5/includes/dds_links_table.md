| Caller Icon                                 | Caller Type        | Link Type       | Callee Icon                                 | Callee Type       | Remarks |
|---------------------------------------------|--------------------|-----------------|---------------------------------------------|-------------------|---------|
| ![](../images/icones/dds_logical_file.png)  | DDS Logical File   | Use Select Link | ![](../images/icones/dds_logical_file.png)  | DDS Logical File  |         |
| ![](../images/icones/dds_logical_file.png)  | DDS Logical File   | Use Select Link | ![](../images/icones/dds_physical_file.jpg) | DDS Physical File |         |
| ![](../images/icones/dds_physical_file.jpg) | DDS Physical File  | Use Select Link | ![](../images/icones/dds_physical_file.jpg) | DDS Physical File |         |
