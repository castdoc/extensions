| Caller Icon                               | Caller Type            | Link Type       | Callee Icon                                   | Callee Type            | Remarks    |
|-------------------------------------------|------------------------|-----------------|-----------------------------------------------|------------------------|------------|
| ![](../images/icones/disk_file.jpg)       | DDS Display File       | call Link       | ![](../images/icones/program_icon.png)        | RPG III Program        |            |
| ![](../images/icones/disk_file.jpg)       | RPG III File Disk      | Relyon Link     | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/disk_file.jpg)       | RPG III File Disk      | Relyon Link     | ![](../images/icones/dds_physical_file.jpg)   | DDS Physical File      |            |
| ![](../images/icones/subroutine_icon.png) | RPG III MainSubroutine | Use Update Link | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/subroutine_icon.png) | RPG III MainSubroutine | Use Insert Link | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/subroutine_icon.png) | RPG III MainSubroutine | Use Select Link | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/subroutine_icon.png) | RPG III MainSubroutine | Use Insert Link | ![](../images/icones/dds_physical_file.jpg)   | DDS Physical File      |            |
| ![](../images/icones/subroutine_icon.png) | RPG III MainSubroutine | Use Select Link | ![](../images/icones/dds_physical_file.jpg)   | DDS Physical File      |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Delete Link | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Update Link | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Select Link | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Insert Link | ![](../images/icones/dds_logical_file.png)    | DDS Logical File       |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Insert Link | ![](../images/icones/dds_physical_file.jpg)   | DDS Physical File      |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Update Link | ![](../images/icones/dds_physical_file.jpg)   | DDS Physical File      |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Select Link | ![](../images/icones/dds_physical_file.jpg)   | DDS Physical File      |            |
| ![](../images/icones/subroutine_icon.png) | RPG III Subroutine     | Use Delete Link | ![](../images/icones/dds_physical_file.jpg)   | DDS Physical File      |            |
