| Caller Icon                                   | Caller Type                | Link Type | Callee Icon                                  | Callee Type        | Remarks |
|-----------------------------------------------|----------------------------|-----------|----------------------------------------------|--------------------|---------|
| ![](../images/icones/subroutine_icon.png)     | RPG III MainSubroutine     | call Link | ![](../images/icones/program_icon.png)       | CL Program         |         |
| ![](../images/icones/subroutine_icon.png)     | RPG III Subroutine         | call Link | ![](../images/icones/program_icon.png)       | CL Program         |         |
| ![](../images/icones/subroutine_icon.png)     | RPG IV MainSubroutine      | call Link | ![](../images/icones/program_icon.png)       | CL Program         |         |
| ![](../images/icones/rpg_proc_proto_icon.jpg) | RPG IV Procedure Prototype | call Link | ![](../images/icones/program_icon.png)       | CL Program         |         |
| ![](../images/icones/subroutine_icon.png)     | RPG IV Subroutine          | call Link | ![](../images/icones/program_icon.png)       | CL Program         |         |
| ![](../images/icones/program_icon.png)        | CL Program                 | call Link | ![](../images/icones/program_icon.png)       | RPG III Program    |         |
| ![](../images/icones/program_icon.png)        | CL Program                 | call Link | ![](../images/icones/copy_member.jpg)        | RPG IV Copy Member |         |
| ![](../images/icones/program_icon.png)        | CL Program                 | call Link | ![](../images/icones/rpg_procedure_icon.jpg) | RPG IV Procedure   |         |
| ![](../images/icones/program_icon.png)        | CL Program                 | call Link | ![](../images/icones/program_icon.png)       | RPG IV Program     |         |
| ![](../images/icones/subroutine_icon.png)     | CL Subroutine              | call Link | ![](../images/icones/program_icon.png)       | RPG IV Program     |         |
