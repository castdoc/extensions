
| Caller Icon                              | Caller Type           | Link Type           | Callee Icon                                        | Callee Type           | Remarks |
|------------------------------------------|-----------------------|---------------------|----------------------------------------------------|-----------------------|---------|
| ![](../images/icones/disk_file.jpg)      | CL Disk File          | Relyon Link         | ![](../images/icones/disk_file.jpg)                | DDS Display File      |         |
| ![](../images/icones/disk_file.jpg)      | CL Disk File          | Relyon Link         | ![](../images/icones/dds_logical_file.png)         | DDS Logical File      |         |
| ![](../images/icones/disk_file.jpg)      | CL Disk File          | Relyon Link         | ![](../images/icones/dds_physical_file.jpg)        | DDS Physical File     |         |
| ![](../images/icones/disk_file.jpg)      | DDS Display File      | call Link           | ![](../images/icones/program_icon.png)             | CL Program            |         |
| ![](../images/icones/program_icon.png)   | CL Program            | Use Select Link     | ![](../images/icones/dds_logical_file.png)         | DDS Logical File      |         |
| ![](../images/icones/program_icon.png)   | CL Program            | Use Select Link     | ![](../images/icones/dds_physical_file.jpg)        | DDS Physical File     |         |
