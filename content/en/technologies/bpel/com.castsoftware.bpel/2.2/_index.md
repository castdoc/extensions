---
title: "BPEL - 2.2"
linkTitle: "2.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.bpel

## What's new?

See [Release Notes](rn/) for more information.

## Description 

This extension provides support for applications using BPELscript (Business Process Execution Language script).

## In what situation should you install this extension?

This extension should be installed when you want to analyze an
application that uses BPELscript and WSDL service files.

BPELscript is a language used to specify BPEL processes. It provides
a compact syntax inspired by scripting languages such as JavaScript and
Ruby and offers a full coverage of all features provided by BPEL. A BPEL
process specifies in which order Web services should be invoked. This
means that when a BPEL process receives a request, it will invoke the
appropriate Web services to fulfil the request and then responds back to
the requester. It is also possible that a Web service can depend on
previously invoked Web services.

Note that the WSDL service files are handled by the [Universal Linker](../../../multi/com.castsoftware.wbslinker/).

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Supported file types

|  File       | Description  |
|---------|------------------|
| `*.bpel` | BPELscript files |

## Download and installation instructions

This extension is not installed automaticlly. If you need to use it, install it manually.

## Source code discovery

A discoverer is provided with the extension to automatically detect BPEL code: when a `.bpel` file is found in a folder, one "BPEL" project will be discovered, resulting in a corresponding Analysis Unit.

## What results can you expect?

### Example code

Example of BPEL Invoke, Process and Receive:

 ![](../images/382625649.jpg)

### Dependency to Java

Links will be created where there is a dependency with Java source code.
Take the following two examples:

#### Example 1

Java embedded in BPEL source code:

``` java
<bpws:invoke name="Vorinitialisierung" operation="null" partnerLink="null" portType="wpc:null" wpc:continueOnError="inherit" wpc:displayName="Vorinitialisierung" wpc:id="45">
      <wpc:script>
        <wpc:javaCode>
           <![CDATA[
           commonj.sdo.DataObject dO = (commonj.sdo.DataObject)SIInputDataWrapper.getDataObject("SGMIMPORT")
           commonj.sdo.DataObject ordnungsbegriffTypFeld = dO.getDataObject("DOKIMPORT[1]/DOKREF/CONTENTPARAM/VORGREF[1]/KEY");
           styp = ordnungsbegriffTypFeld.getString("styp");
           vorgangsnummer =  dO.getDataObject("DOKIMPORT[1]/DOKREF/CONTENTPARAM/VORGREF[1]/KEY").getString(0);]
          ]>
         </wpc:javaCode>
      </wpc:script>
</bpws:invoke>
```

Corresponding Java source code:

![](../images/382625652.png)  

Resulting link from BEPL to Java method:

![](../images/382625650.png)  

#### Example 2

BPEL Web Service invoking web services:

``` java
<bpws:invoke name="InvokeSDPOrderDBretrieveBaseOrder" operation="retrieveBaseOrder" partner-Link="SDPOrderDBCompensableService_InterfacePartner" portType="ns3:SDPOrderDBCompensableService_Interface" wpc:continueOnError="inherit" wpc:displayName="InvokeSDPOrderDB.retrieveBaseOrder" wpc :id="72">
      <wpc:input>
        <wpc:parameter name="order" variable="processingOrder"/>
      </wpc:input>
      <wpc:output>
  ...
</bpws:invoke>
```

Resulting link from BEPL to web service:

![Alt text](../images/382625651.png)  

### Objects 

| Icon | Metamodel name |
|------|----------------|
| ![](../images/382625658.png) | BPEL Invoke    |
| ![](../images/382625659.png) | BPEL Process   |
| ![](../images/382625660.png) | BPEL Receive   |

### Links

#### Call links

-   BPEL Receive \> BPEL Process
-   BPEL Invoke \> BPEL Receive
-   BPEL Process \> BPEL Invoke

### Rules

None.

### Limitations

- `Invokes` inside `Pick` activities are not currently supported. Their modelization (together with `onMessage` and `onAlarm` elements) will be considered in futire releases.
