---
title: "Analysis configuration"
linkTitle: "Analysis configuration"
type: "docs"
weight: 4
---

>The information below is valid only for the legacy analyzers
(PL/SQL/T-SQL) embedded in CAST Imaging Core. See [SQL - Available
extensions](../extensions) for information about SQL
related extensions.

## Introduction

CAST Imaging Console exposes the Technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
PL/SQL Technology/MS-SQL Technology/Sybase Technology to display the
available options:

![](414941530.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same as at Application level) that allow you to
        make fine-grained configuration changes.

You should check that settings are as required and that at least one
Analysis Unit exists for the specific technology.

![](414941529.jpg)

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Dependencies</th>
<td class="confluenceTd"><p>Dependencies are configured
at <strong>Application level</strong> for<strong> each
technology</strong> and are configured between <strong>individual
Analysis Units/groups of Analysis Units </strong>(dependencies between
technologies are not available as is the case in the CAST Management
Studio). You can find out more detailed information about how to ensure
dependencies are set up correctly, in Validate dependency
configuration.</p>
<p>Note that for legacy SQL technologies, dependencies will not usually
be created automatically for links between server side and client side
(as this direction does not usually occur). You are free to create
custom dependencies between PL/SQL and T-SQL analysis units if necessary
however, using the ADD button.</p></td>
</tr>
<tr class="even">
<th class="confluenceTh">Analysis Units</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>It is possible to click into each Analysis Unit (corresponding to one
database delivered) detected during the source code delivery, however,
for legacy PL/SQL and T-SQL technologies, there are no configuration
options available and it is only possible to view the deployment
path:</p>
<p><img src="414941526.jpg" /></p>
<p>You can however:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<th class="confluenceTh"><div class="content-wrapper">
<img src="414941528.jpg" draggable="false"
data-image-src="414941528.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="414941528"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="analyze.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="234431032"
data-linked-resource-container-version="8" height="42" />
</div></th>
<td class="confluenceTd"><p>Analyze a specific Analysis Unit on its own,
specifically for testing purposes.</p></td>
</tr>
<tr class="even">
<th class="confluenceTh"><div class="content-wrapper">
<img src="414941527.jpg" draggable="false"
data-image-src="414941527.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="414941527"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="toggle.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="234431032"
data-linked-resource-container-version="8" height="28" />
</div></th>
<td class="confluenceTd">Disable or enable the analysis of a specific
Analysis Unit, when the next Application level analysis/snapshot is run.
By default all Analysis Units are always set to
<strong>enable</strong>. When an Analysis Unit is set to disable, this
is usually a temporary action to prevent an Analysis Unit from being
analyzed.</td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
</tbody>
</table>