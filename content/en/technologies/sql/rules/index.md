---
title: "Structural rules"
linkTitle: "Structural rules"
type: "docs"
weight: 5
---

>The information below is valid only for the legacy PL/SQL and T-SQL
analyzers embedded in CAST AIP. See [SQL - Available extensions](../extensions) for information about SQL
related extensions.

  

|Technology | URL |
|-----------|-----|
| Microsoft SQL Server T-SQL | [https://technologies.castsoftware.com/rules?sec=t\_-13&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_-13&ref=%7C%7C)      |
| Oracle Server PL/SQL       | [https://technologies.castsoftware.com/rules?sec=t_139287&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_139287&ref=%7C%7C) |
| Sybase T-SQL               | [https://technologies.castsoftware.com/rules?sec=t_141001&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_141001&ref=%7C%7C) |