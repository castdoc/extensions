---
title: "Missing tables and procedures for C and Cpp - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.cpp.missingtable

## What's new?

See [Release Notes](rn/).

## Description

This extension is designed to analyze SQL queries embedded in C/C++
code and create "missing" SQL and C/C++ table / procedure objects
to represent SQL objects that have not been delivered for analysis.
Links to these objects are also created. The primary goal of the
extension is to ensure that a full transaction is identified by
CAST, resulting from the SQL queries embedded in the C/C++ code, even if
none or only part of the referenced SQL objects are delivered for
analysis.

## Supported client languages

| Language | Supported? |
|---|:-:|
| C/C++ | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Prerequisites

| Item  | Description |
|:-:|---|
| :white_check_mark: | An installation of any compatible release of CAST Core (see table above) |
| :white_check_mark: | An installation of [com.castsoftware.sqlanalyzer](../../../com.castsoftware.sqlanalyzer/) (from version ≥ 3.6.9-funcrel) |

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST
Console when C/C++ source code is delivered for analysis. You can manage
it using the Application - Extensions interface.

## What results can you expect?

### Objects

| Icon | Object Type | Description | Metamodel Type |
|:---:|---|---|---|
| ![](../../images/Schema.png) | Missing Schema  | Parent of the missing tables and procedures, attached to the C/C++ Project. | CAST_CPP_MissingTable_Schema |
| ![](../../images/Table.png) | Missing Table | A table or a view selected/updated/deleted/inserted in a C/C++ Query missing from the DDL file. | CAST_CPP_MissingTable_Table |
| ![](../../images/Procedure.png) | Missing Procedure | A procedure or a function called in a C/C++ Query missing from the DDL file. | CAST_CPP_MissingTable_Procedure |
| ![](../../images/Table.png) | SQL Missing Table | A table or a view selected/updated/deleted/inserted in a C/C++ Query missing from the DDL file, but we have a SQL Schema, and only one. | SQLScript_Missing_Table |
| ![](../../images/Procedure.png) | SQL Missing Procedure | A procedure or a function called in a C/C++ Query missing from the DDL file but we have a SQL Schema, and only one. | SQLScript_Missing_Procedure |

### Links

Links are created for transaction and function point needs. You can
expect the following links on the C/C++ Embedded SQL queries:

-   useSelect/useUpdate/useDelete/useInsert from C/C++ Query 
    to Missing Table/SQL Missing Table
-   call from C/C++ Query to Missing Procedure/SQL Missing
    Procedure
