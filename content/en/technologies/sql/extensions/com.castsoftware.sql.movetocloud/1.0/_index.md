---
title: "RDBMS Move to Cloud - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.sql.movetocloud

## What's new?

See [Release Notes](rn/).

## Description

This extension provides a dedicated set of quality rules (i.e.
recommendations) that have been designed to help plan a modernization
project for taking an on premise RDBMS to the Cloud or to migrate to
another RDBMS. You should install this extension:

-   If you need to have a set of recommendations because you plan to
    move from Oracle Database to Oracle Cloud
-   If you need to have a set of recommendations because you plan to
    move from Oracle / DB2 / SQL Server Database to PostgreSQL
-   If you need to have a set of recommendations because you plan to
    move from DB2 Database to MySQL

Results are shown in CAST Imaging and the CAST Engineering
Dashboard. You can view the rules provided in this extension in the
"Structural Rules" section below.

When in the same analysis result you mix 2 RDBMS files, the rules for
both RDBMS are calculated. E.g.: analyzing DB2 and Oracle SQL files will
trigger Oracle and DB2 quality rules.

When vendor is not detected and file is considered ANSI sql, those
quality rules are not calculated.

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-:|:--:|
| :x: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The RDBMS Move to Cloud extension
requires that the following other CAST extensions are also installed:

-   [SQL Analyzer](../../com.castsoftware.sqlanalyzer) (≥ 3.6.7)

>Note that when using CAST Console to download and install the
extension, any dependent extensions are automatically downloaded and
installed.

## Prerequisites

| Item  | Description |
|:-:|---|
| :white_check_mark: | An installation of any compatible release of CAST Core (see table above) |
| :white_check_mark: | An installation of [com.castsoftware.sqlanalyzer](../../../com.castsoftware.sqlanalyzer/) (≥ 3.6.7-funcrel) |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Console. If you need to use it, should manually install the
extension using the Application - Extensions interface.

## What results can you expect?

### Structural rules

| Release | Link |
|---------|------|
| 1.0.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.2-funcrel) |
| 1.0.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.1-funcrel) |
| 1.0.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.0-funcrel) |
| 1.0.0-beta9 | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta9](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.0-beta9) |
| 1.0.0-beta8 | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta8](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.0-beta8) |
| 1.0.0-beta7 | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta7](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.0-beta7) |
| 1.0.0-beta6 | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta6](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.0-beta6) |
| 1.0.0-beta5 | [https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta5](https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=%7C%7C1.0.0-beta5) |
| 1.0.0-beta4 | https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta4                                                                                                              |
| 1.0.0-beta3 | https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta3                                                                                                              |
| 1.0.0-beta2 | https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta2                                                                                                              |
| 1.0.0-beta1 | https://technologies.castsoftware.com/rules?sec=srs_sql.movetocloud&ref=\|\|1.0.0-beta1                                                                                                              |

### CAST Imaging Viewer

Results are provided in the "Imaging Advisor":

![](../images/636059736.jpg)

### CAST Engineering Dashboard

Example results for Oracle Database to Oracle Cloud:

![](../images/614826140.png)

Example results for Oracle Database to Amazon Aurora:

![](../images/614826139.png)

