---
title: "Release Notes - 3.6"
linkTitle: "Release Notes"
type: "docs"
rnurl: "https://extend.castsoftware.com/api/delta/export/release-notes/format/markdown?id=com.castsoftware.sqlanalyzer&major=3&minor=6"
---

***

{{% fetch-release-notes %}}
