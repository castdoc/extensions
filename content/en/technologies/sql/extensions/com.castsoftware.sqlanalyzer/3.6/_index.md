---
title: "SQL Analyzer - 3.6"
linkTitle: "3.6"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.sqlanalyzer

## What's new ?

See [Release Notes](rn/).

## Description

The SQL Analyzer provides support for database technologies using
the ANSI SQL-92/99 language. This extension uses the Universal
Analyzer framework and is intended to analyse DDL, DML and SQL
exports for a large variety of SQL variants:

-   This extension provides source code analysis support for DDL and
    DML \*.sql files using an over language of the various sql
    variants.
-   See Supported File Extensions below for the list of supported files.

In what situation should you install this extension?

-   If you need to analyze PostgreSQL, MySQL, MariaDB,
    SQLite, Db2, Sybase, Microsoft SQL
    Server, Teradata, Informix, CockroachDB or NonStop SQL  
    
-   If you need to analyze Oracle for the first time or you want to
    transition to SQL Analyzer and you have not analyzed Oracle
    Forms / Reports
-   If your application contains schemas from database vendors not
    supported "out of the box" by CAST AIP but, which are
    compliant with ANSI SQL-92 / 99
-   When you do not have access to the online database to perform an
    extraction for use with CAST AIP and have instead been provided with
    DDL scripts

### Transitioning from from the CAST AIP Db2 Analyzer to the SQL Analyzer extension

If you have been actively analyzing Db2 (z/OS or LUW) with the Db2
Analyzer (provided out-of-the-box in CAST AIP) you can transition to
using the SQL Analyzer extension to analyze your Db2 source code. The
process of transitioning is described in [SQL Analyzer - To do
transition from the CAST AIP Db2 Analyzer to the SQL Analyzer
extension](../additional-doc/transition-db2/).

Reversed links

When transitioning from the Db2 Analyzer to the SQL Analyzer, links
between Tables and Indexes, Foreign Keys, Primary Keys and Unique Keys
will appear to be reversed when comparing the analysis results of the
Db2 Analyzer and the SQL Analyzer. This is because the representation of
links in the SQL Analyzer uses a different method (which is identical
for all supported RDBMS) to the Db2 Analyzer.

### Transitioning from the CAST AIP Oracle Analyzer to the SQL Analyzer extension

If you have been actively analyzing Oracle with the Oracle Analyzer
(provided out-of-the-box in CAST AIP) you can transition to using the
SQL Analyzer extension to analyze your Oracle source code. The
process of transitioning is described in [SQL Analyzer - To do
transition from the CAST AIP Oracle Analyzer to the SQL Analyzer
extension](../additional-doc/transition-oracle/).

Analyzing the same code will generate differences in results across
object types, object links and structural rules. If you do decide to
move to the SQL Analyzer extension, see [SQL Analyzer - Mapping between
SQL analyzers](../additional-doc/mapping/) for more
information about what you can expect.

>![(warning)](/images/icons/emoticons/warning.svg) Important!
Don't change analyzed sources, keep analyzing src files. Only GUIDs are changed during transitions, checksums are recalculated,
and that will impact transactions. 

#### Oracle Forms and Reports particular case

-   If you have already analyzed Oracle Forms and Reports code that is
    linked to PL/SQL code analyzed with the SQL analyzer embedded in
    CAST AIP, CAST recommends that you continue using the SQL analyzer
    embedded in CAST AIP.

### Transitioning from the CAST AIP SQL Server Analyzer to the SQL Analyzer extension

If you have been actively analyzing MS SQL Server with the MS SQL
Server Analyzer (provided out-of-the-box in CAST AIP) you can
transition to using the SQL Analyzer extension to analyze your SQL
Server source code. The process of transitioning is described in [How to
do transition from the CAST MS SQL Server Analyzer to the SQL Analyzer
extension](../additional-doc/transition-ms-sql/).

Analyzing the same code will generate differences in results across
object types, object links and structural rules. If you do decide to
move to the SQL Analyzer extension, see [SQL Analyzer - Mapping between
SQL analyzers](../additional-doc/mapping/) for more
information about what you can expect.

>![(warning)](/images/icons/emoticons/warning.svg) Important!
Don't change analyzed sources, keep analyzing src files. Only GUIDs are changed during transitions, checksums are recalculated,
and that will impact transactions. 

### Transitioning from the CAST AIP ASE Sybase Analyzer to the SQL Analyzer extension

If you have been actively analyzing ASE Sybase with the ASE Sybase
Analyzer (provided out-of-the-box in CAST AIP) you can transition to
using the SQL Analyzer extension to analyze your ASE Sybase source
code. The process of transitioning is described in [SQL Analyzer - To do
transition from the CAST AIP ASE Sybase Analyzer to the SQL Analyzer
extension](../additional-doc/transition-sybase/).

Analyzing the same code will generate differences in results across
object types, object links and structural rules. If you do decide to
move to the SQL Analyzer extension, see [SQL Analyzer - Mapping between
SQL analyzers](../additional-doc/mapping/) for more
information about what you can expect.

>![(warning)](/images/icons/emoticons/warning.svg) Important!
Don't change analyzed sources, keep analyzing src files. Only GUIDs are changed during transitions, checksums are recalculated,
and that will impact transactions.  

## Supported file extensions

The following table summarizes the source file extensions managed by the
SQL Analyzer by default. Other file extensions could be used, upon
manual configuration (within CAST Console):

<table>
<colgroup>
<col />
<col />
<col />
<col />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>File</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Extension</p></th>
<th class="confluenceTh">Mandatory/Optional</th>
<th class="confluenceTh" style="text-align: left;"><p>Note</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">SQL source files</td>
<td class="confluenceTd" style="text-align: left;"><p>.sql</p>
<p>.sqlt</p></td>
<td rowspan="4" class="confluenceTd">Mandatory</td>
<td class="confluenceTd" style="text-align: left;"><p><strong>DDL / DML
files</strong></p>
<ul>
<li>See <a href="../additional-doc/categorized/">SQL
Analyzer - How my SQL file is categorized</a> (DDL or DML?)</li>
<li>See <a href="../additional-doc/ddl-extraction/">SQL Analyzer -
DDL extraction examples</a> (How to generate these files from a live
database, if so .sql file is maintained in the Source Code Management
System)</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">CAST
generated Files</td>
<td class="confluenceTd" style="text-align: left;"><p>.uaxdirectory</p>
<p>.src </p></td>
<td class="confluenceTd" style="text-align: left;"><p>.src files are SQL
sources files generated with <a
href="https://doc.castsoftware.com/display/DOCCOM/CAST+Database+Extractor">CAST
Database Extractor</a>. Supported for compatibility reasons
only.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">IBM Db2 for i Logical
File</td>
<td class="confluenceTd" style="text-align: left;"><p>.lf</p>
<p>.lf38</p></td>
<td class="confluenceTd" style="text-align: left;">IBM Db2 for i View in
DDS format</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">IBM Db2 for
i Physical File</td>
<td class="confluenceTd" style="text-align: left;"><p>.pf</p>
<p>.pf38</p></td>
<td class="confluenceTd" style="text-align: left;">IBM Db2 for i
Table in DDS format</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">CAST SQL Table
Size File</td>
<td class="confluenceTd" style="text-align: left;">.sqltablesize</td>
<td class="confluenceTd">Optional</td>
<td class="confluenceTd" style="text-align: left;">See <a
href="../additional-doc/xxl/">SQL
Analyzer - working with XXL or XXS tables#Introduction</a>.</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">CAST Data Sensitive
File</td>
<td class="confluenceTd" style="text-align: left;">.datasensitive</td>
<td class="confluenceTd">Optional</td>
<td class="confluenceTd" style="text-align: left;">See <a
href="../additional-doc/sensitive-data/">SQL Analyzer - RDBMS
Table Sensitive Data</a>.</td>
</tr>
</tbody>
</table>

## Vendor compatibility matrix - official support

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Vendor</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh">Supported?</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984018.png" class="image-center"
draggable="false" data-image-src="../images/653984018.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984018"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.db2.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">IBM Db2 for LUW</td>
<td class="confluenceTd"><p>Up to version 11.x</p></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984018.png" class="image-center"
draggable="false" data-image-src="../images/653984018.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984018"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.db2.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">IBM Db2 for z/OS</td>
<td class="confluenceTd">Up to version 12</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983913.png" class="image-center"
draggable="false" data-image-src="../images/653983913.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983913"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="ibm-i-logo-2021.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">IBM Db2 for i</td>
<td class="confluenceTd">Up to version 7.4</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984020.png" class="image-center"
draggable="false" data-image-src="../images/653984020.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984020"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.informix.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">IBM Informix</td>
<td class="confluenceTd">Up to version 14.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984015.png" class="image-center"
draggable="false" data-image-src="../images/653984015.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984015"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.mariadb.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">MariaDB</td>
<td class="confluenceTd">Up to version 10.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984022.png" class="image-center"
draggable="false" data-image-src="../images/653984022.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984022"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.sqlserver.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">Microsoft SQL Server</td>
<td class="confluenceTd">Up to version 2019</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984016.png" class="image-center"
draggable="false" data-image-src="../images/653984016.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984016"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.mysql.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">MySQL</td>
<td class="confluenceTd">Up to version 8.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984019.png" class="image-center"
draggable="false" data-image-src="../images/653984019.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984019"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.oracleplsql.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">Oracle Server</td>
<td class="confluenceTd">Up to version 19c</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984014.png" class="image-center"
draggable="false" data-image-src="../images/653984014.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984014"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.postgre.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">PostgreSQL</td>
<td class="confluenceTd">Up to version 15.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984023.png" class="image-center"
draggable="false" data-image-src="../images/653984023.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984023"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.sapase.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">SAP ASE (formerly known as Sybase ASE)</td>
<td class="confluenceTd">Up to version 16</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984017.png" class="image-center"
draggable="false" data-image-src="../images/653984017.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984017"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.sqlite.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">SQLite</td>
<td class="confluenceTd">Up to version 3.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653984021.png" class="image-center"
draggable="false" data-image-src="../images/653984021.png"
data-unresolved-comment-count="0" data-linked-resource-id="653984021"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="com.castsoftware.teradata.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">Teradata</td>
<td class="confluenceTd">Up to version 16</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983911.png" class="image-center"
draggable="false" data-image-src="../images/653983911.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983911"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="7e951840cd536c6bd529af6e5cc2c139b485f0ca0970710d2de7319682b82e11.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="50" /></p>
</div></td>
<td class="confluenceTd">CockroachDB</td>
<td class="confluenceTd">Up to version 21</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983910.png" class="image-center"
draggable="false" data-image-src="../images/653983910.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983910"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Snip20191210_1.png.280x250_q85.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="77" /></p>
</div></td>
<td class="confluenceTd">NonStop SQL</td>
<td class="confluenceTd">Up to version 3.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
</tbody>
</table>

\*) added in 3.6.0-beta1

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner (for example via CAST Enlighten) - *click
to enlarge*:

![](../images/653983991.png)

You can also use the CAST Management Studio option View Analysis Unit
Content to see the objects that have been created following the
analysis:

![](../images/653983993.png)

### Objects

| Icon | Object Type | Description |
|:---:|---|---|
| ![](../images/653983936.png) | Database | Objects created for MS SQL Server, ASE Sybase, IBM Db2, CockroachDB and NonStop SQL, see details about how database is determined here: [SQL Analyzer - How object identity is determined](../additional-doc/identity/) |
| ![](../images/653983969.png) |  Schema  | Objects created for all RDBMS, see details about how schema is determined here : [SQL Analyzer - How object identity is determined](../additional-doc/identity/) |
| ![](../images/653983950.png) | Table | Objects created with CREATE TABLE statements.    Records in *.pf *.pf38 files, specific to Db2fori variant. Table name is given by the file name.  |
| ![](../images/653983947.png) | View | Objects created with CREATE VIEW / REPLACE VIEW statements.   Records in *.lf *.lf38 files, specific to Db2fori variant. View name is given by the file name.  |
| ![](../images/653983951.png) | Table Column | Columns detected in CREATE TABLE statements.   For CREATE VIEW statements only aliases, the names for the expressions selected by the defining query of the view, are considered as columns names. |
| ![](../images/653983981.png) | Index | Objects created with CREATE INDEX statements. |
| ![](../images/653983995.png) | Foreign Key | Objects created with CREATE TABLE ... (... FOREIGN KEY ..) / ALTER TABLE .... FOREIGN KEY ... statements. |
| ![](../images/653983977.png) | Unique Constraint | Objects created with CREATE UNIQUE INDEX or CREATE PRIMARY KEY statements.   For the CREATE PRIMARY KEY / ALTER TABLE ADD CONSTRAINT PRIMARY KEY / ALTER TABLE ADD PRIMARY KEY / CREATE TABLE .... ( ... PRIMARY/UNIQUE KEY ...) statements, tables are marked as having a primary key.   Key in *.pf *.pf38 files, specific to Db2fori variant. Unique constraint name is given by the file name prefixed with PK_.  |
| ![](../images/653983948.png) | Trigger | Objects created with CREATE TRIGGER / CREATE RULE statements. |
| ![](../images/653983999.png) | Event | Objects created with CREATE EVENT statements. |
| ![Alt text](../images/SQLScriptMacro.png) | Macro | Objects created with CREATE MACRO statements. |
| ![](../images/653983975.png) | Package  | Objects created with CREATE PACKAGE/PACKAGE BODY statements. |
| ![](../images/653983978.png) | Procedure | Objects created with CREATE PROCEDURE / REPLACE PROCEDURE statements. The same type cover Package's procedures. |
| ![](../images/653983992.png) | Function | Objects created with CREATE FUNCTION statements. The same type cover Package's functions. |
| ![](../images/653983975.png) | Type | Objects created with CREATE TYPE/TYPE BODY statements. |
| ![](../images/653983992.png) | Method | Object Type's methods. |
| ![](../images/653983953.png) | Synonym | Object created with CREATE SYNONYM, CREATE ALIAS, CREATE SQLMP ALIAS and CREATE NICKNAME statements. |
| ![](../images/653983924.png) | Database Link | Object created with CREATE DATABASE LINK statements. |
| ![](../images/653983919.png) | Unresolved Schema | When objects accessed via a database link in a DML statement are not resolved, they are missing from analysis result, an Unresolved table or Unresolved Object is created, in a Unresolved Schema.   When the object is more "table like", we add an Unresolved Table.   When the object is more "procedure / function like" we add an Unresolved Object.   In the database link access objects are prefixed with schema name, that schema will belongs to the Database Link object, Unresolved Table / Object will belongs to Unresolved Schema.  See example [SQL Analyzer - How object identity is determined](../additional-doc/identity/). |
| ![](../images/653983916.png) | Unresolved Table | As above |
| ![](../images/653983915.png) | Unresolved Object | As above |
| ![](../images/653983949.png) | DML Script File | File with DML SQL statements, like INSERT/UPDATE/DELETE/etc.   DML Script File candidates with more than 80% INSERT statements are considered as data exports and will not be analyzed.   Sometimes DML and DDL statements are mixed and DML Script Files are also considered as DDL. |
| ![](../images/653983955.png) | sourceFile |   This is a logical file.   For each source file we add a property named Vendor, see [SQL file Vendor - This file is analyzed against XXX variant](../additional-doc/variant/). |

#### Table deletion and renaming

DROP TABLE syntax is supported for table objects within the same
file. When creating a table through CREATE TABLE tableName (colName
int, ...) followed by a DROP TABLE tableName, the table will not be
recorded and thus will not be displayed in CAST Enlighten. Similarly,
if a table is renamed with a RENAME TABLE statement
(or ALTER TABLE RENAME TO as in SQLite and PostgreSQL), this change
will be reflected in CAST Enlighten. Presently we consider
*case-insensitive* names, i.e., objects named tableName, TABLEname are
considered to be the same object.

#### Database object

##### Db2 analyses

>Please read this note if you move from sqlanalyzer \<= 2.6.9-funcrel to
sqlanalyzer \>= 3.0.0-alpha2 and if the AIP core is from 8.3.16 to
8.3.28: [SQL Analyzer - Upgrading SQL Analyzer when AIP core is from 8.3.16 to
8.3.28](../additional-doc/upgrading/).

The following CREATE TABLE ... IN ... statements will generate a new
object: a database for Db2 analyses:

``` sql
CREATE TABLE .... () ... IN DATABASE_NAME.TABLESPACE_NAME ....  --see ABC 02 example

CREATE TABLE .... () IN DATABASE DATABASE_NAME --see ABC 03 example
```

Object Full Name will change from \<SCHEMA NAME\>.\<OBJECT NAME\> to
\<DATABASE NAME\>.\<SCHEMA NAME\>.\<OBJECT NAME:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">prior to 3.0.0-alpha2</th>
<th class="confluenceTh">3.0.0-alpha2</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983935.png" draggable="false"
data-image-src="../images/653983935.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983935"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-23_9-25-14.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="383"
height="250" /><img src="../images/653983934.png"
draggable="false" data-image-src="../images/653983934.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983934"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-23_9-37-56.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="479"
height="250" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983932.png" draggable="false"
data-image-src="../images/653983932.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983932"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-24_21-44-7.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="753"
height="250" /><img src="../images/653983933.png"
draggable="false" data-image-src="../images/653983933.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983933"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-24_15-25-12.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="469"
height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983931.png" draggable="false"
data-image-src="../images/653983931.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983931"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-23_9-25-53.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="386"
height="250" /><img src="../images/653983930.png"
draggable="false" data-image-src="../images/653983930.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983930"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-23_9-38-42.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="555"
height="250" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983929.png" draggable="false"
data-image-src="../images/653983929.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983929"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-24_21-42-27.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="713"
height="250" /><img src="../images/653983928.png"
draggable="false" data-image-src="../images/653983928.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983928"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-24_15-26-16.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="590"
height="250" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983927.png" draggable="false"
data-image-src="../images/653983927.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983927"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-23_9-26-46.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="244"
height="250" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983926.png" draggable="false"
data-image-src="../images/653983926.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983926"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2018-10-24_15-26-46.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="149"
height="250" /></p>
</div></td>
</tr>
</tbody>
</table>

##### SQL Server analyses

>Please read this note if you move from sqlanalyzer \< 3.4.0-beta5 to
sqlanalyzer \>= 3.4.0-beta5 and if the AIP core is from 8.3.16 to
8.3.28: [SQL Analyzer - Upgrading SQL Analyzer when AIP core is from 8.3.16 to
8.3.28](../additional-doc/upgrading/).

The following statements will generate a database for SQL Server and
Sybase analyses:

``` sql
USE <DATABASE_NAME>

CREATE DATABASE <DATABASE_NAME>

CREATE <OBJECT_TYPE>*)  <DATABASE_NAME>.<SCHEMA_NAME>.<OBJECT_NAME>

*) TABLE, VIEW, etc
```

Object Full Name will change from \<SCHEMA NAME\>.\<OBJECT NAME\> to
\<DATABASE NAME\>.\<SCHEMA NAME\>.\<OBJECT NAME.

#### Database Link object

Added support for CREATE DATABASE LINK statement
: [https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5005.htm](https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5005.htm) .

The caller object is linked with database link object via relyOn links.
The called object is directly linked to the caller object, via Use /
Call links.

Example 1 : with a call via database link 

``` sql
Create Procedure MYPROC_DBLINK
Is
Begin
  MYREMOTESCHEMA.MYREMOTEPROC@DBLINK.DOMAIN(0, 'string1');
End;
 /
 CREATE SHARED PUBLIC DATABASE LINK DBLINK.DOMAIN
  CONNECT TO TOTO
  USING 'TOTO.db.toto.com'
/
 create procedure MYREMOTESCHEMA.MYREMOTEPROC(I_1 int, 
 I_2 varchar2(500)
 as
 begin
     return 1;
 end;
 /
```

The caller object, MYPROC_DBLINK, is linked with :

-   the database link object DBLINK.DOMAIN via a relyOn link
-   the called object MYREMOTEPROC via a call link

![](../images/653983923.png)

Example 2 : with a call via a synonym on a database link 

``` sql
create procedure MYREMOTESCHEMA.MYREMOTEPROC(I_1 int, 
 I_2 varchar2(500)
 as
 begin
     return 1;
 end;
 /
CREATE SHARED PUBLIC DATABASE LINK DBLINK.DOMAIN
  CONNECT TO TOTO
  USING 'TOTO.db.toto.com'
/
CREATE PUBLIC SYNONYM MYPROC_SYN_DBLINK 
   FOR MYREMOTESCHEMA.MYREMOTEPROC@DBLINK.DOMAIN;
/
 Create Procedure MYPROC_DBLINK
Is
Begin
  MYPROC_SYN_DBLINK(0, 'string1');
End;
/
```

The caller object, MYPROC_DBLINK, is linked with the called object
MYREMOTEPROC via a call link

The PUBLIC synonyms is on a procedure accessed via a database link and
has relyOn links with database link DBLINK.DOMAIN and the real procedure
MYREMOTEPROC .

![](../images/653983921.png)

##### Unresolved objects

When the object accessed via a database link is missing, we add an
Unresolved object directly attached to the database link.

``` sql
 CREATE SHARED PUBLIC DATABASE LINK DBLINK.DOMAIN
  CONNECT TO TOTO
  USING 'TOTO.db.toto.com'
/
Create Procedure MYPROC_UNRESOVLED_DBLINK
Is
Begin
  select MYREMOTESCHEMA.MYREMOTEPROC@DBLINK.DOMAIN()
  from dual;
End;
 /
```

The caller object, MYPROC_UNRESOLVED_DBLINK, is linked with the called
object MYREMOTEPROC via a call link

The PUBLIC synonyms is on a procedure accessed via a database link and
has relyOn links with database link DBLINK.DOMAIN and the real procedure
MYREMOTEPROC.

MYREMOTEPROC is missing from analysis result, so it is considered
Unresolved. 

![](../images/653983914.png)

[SQL Analyzer - How object identity is determined](../additional-doc/identity)

#### Oracle Public schema

Synonyms and Database Links declared as PUBLIC are from now attached to
PUBLIC schema.

Example:

``` sql
 CREATE SHARED PUBLIC DATABASE LINK DBLINK.DOMAIN
  CONNECT TO TOTO
  USING 'TOTO.db.toto.com'
/
```

![](../images/653983922.png)

### Links

Links are created for transaction and function point needs: 

#### DDL

You can expect the following links on the DDL side within the same sql
file:

-   useSelect, useInsert, useUpdate, useDelete Links
    from Procedure / Function / Macro
    / Event  to Table / View
-   callLink
    from Procedure / Function / Event to Procedure / Function
-   callLink from Macro to Function
-   callLink from Procedure / Function  to Cobol Program

![](../images/653984013.png)  


-   callLink from Procedure / Function  to C/C++
    Function

![](../images/653984011.png)

-   callLink from Procedure / Function  to Java
    Method

![](../images/653984012.png)  


-   useSelect from View to Table / View used in the query of the
    view
-   callLink from View to Function
-   relyonLink from Index to the Table
-   relyonLink from Index to the Column implied in the index
-   relyonLink
    from Synonym to Table / View / Function / Procedure / Package aliased
    by Synonym
-   referLink from Table / Table Column to a Table / Table
    Column referenced in a Foreign Key 
-   referDelete, referUpdate from Table to
    a Table referenced in a Foreign Key. Examples:  
    -   referDelete:

referUpdate

``` sql
ALTER TABLE A.TABLE_1 ADD CONSTRAINT FK_NAME_1 FOREIGN KEY (COL_1) REFERENCES A.TABLE_2 (COL_2) ON DELETE;
```

-   -   referUpdate:

referUpdate

``` sql
ALTER TABLE A.TABLE_1 ADD CONSTRAINT FK_NAME_1 FOREIGN KEY (COL_1) REFERENCES A.TABLE_2 (COL_2) ON UPDATE;
```

-   -   referDelete with a Referential Action property *Restrict for
        Delete*: 

referDelete

``` sql
ALTER TABLE A.TABLE_1 ADD CONSTRAINT FK_NAME_1 FOREIGN KEY (COL_1) REFERENCES A.TABLE_2 (COL_2) ON DELETE RESTRICT;
```

-   -   referDelete with a Referential Action property *Cascade for
        Delete* : 

referDelete

``` sql
ALTER TABLE A.TABLE_1 ADD CONSTRAINT FK_NAME_1 FOREIGN KEY (COL_1) REFERENCES A.TABLE_2 (COL_2) ON DELETE CASCADE;
```

-   -   referDelete with a Referential Action property *Cascade for
        Delete* , referUpdate with a Referential Action property *No
        Action for Update*

referDelete - referUpdate

``` sql
CREATE TABLE IF NOT EXISTS `TABLE_1` (
  `COL_1` int NOT NULL,
  `COL_2` char(4) COLLATE latin1_bin NOT NULL,
  KEY `FK__TABLE_1__TABLE_2___5AEE82B9` (`COL_1`),
  CONSTRAINT `FK__TABLE_1__TABLE_2___5AEE82B9` FOREIGN KEY (`COL_1`) REFERENCES `TABLE_1` (`COL_1`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
```

-   -   referDelete with a Referential Action property *No Action for
        Delete* , referUpdate with a Referential Action property *No
        Action for Update*

![](../images/653983940.png)

-   -   referDelete with a Referential Action property *Cascade for
        Delete* 

![](../images/653983937.png)

-   callLink to the correct Trigger where the tables is accessed in
    insert/update/delete
    -   example a Trigger declared as BEFORE INSERT on a table, any
        insert to that table will call the trigger...

![](../images/653983996.png)

-   example: a table, with an after update trigger, updated in
    a Client code :

![](../images/653983925.png)

-   inheritLink from sub Type to super Type:  
    -   example : CREATE OR REPLACE type gayerrortype under gayxmlmsg
        ....

![](../images/653983944.png)

-   useLink for PL/SQL tables, example : CREATE OR REPLACE TYPE
    "CNCUBLIST_2" is table of "HIS_TEST_A" ;

![](../images/653983956.png)

#### DML

You can expect the following links on the DML side :

-   useSelect, useInsert, useUpdate, useDelete Links from SQL Script
    / SQL Named Query to Table / View

-   call Links from SQL Script / SQL Named
    Query to Procedure / Function / Macro

-   useSelect Links from Cobol object to Table / View via the
    FETCH statements when the useSelect Links already exists via DECLARE
    CURSOR statements

Example: The table *ACTVITIY* is selected in the *DECLARE C1 CURSOR* so
we consider it is selected in the *FETCH C1* statement.

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Declare Cursor Statement</th>
<th class="confluenceTh">Fetch Statement</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img
src="https://jira.castsoftware.com/secure/attachment/309290/DeclareC1.png"
draggable="false"
data-image-src="https://jira.castsoftware.com/secure/attachment/309290/DeclareC1.png"
width="251" alt="DeclareC1.png" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/653983912.png" draggable="false"
data-image-src="../images/653983912.png"
data-unresolved-comment-count="0" data-linked-resource-id="653983912"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-10-29_14-33-48.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="653983909"
data-linked-resource-container-version="1" width="251" /></p>
</div></td>
</tr>
</tbody>
</table>

### Rules

You can find a full list of rules delivered with this extension here:

| Release        | Link                                                                                   |
|----------------|----------------------------------------------------------------------------------------|
| 3.6.19 | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.19](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.19) |
| 3.6.18 | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.18](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.18) |
| 3.6.17 | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.17](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.17) |
| 3.6.16 | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.16](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.16) |
| 3.6.16-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.16-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.16-funcrel) |
| 3.6.15-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.15-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.15-funcrel) |
| 3.6.14-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.14-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.14-funcrel) |
| 3.6.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.13-funcrel) |
| 3.6.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.12-funcrel) |
| 3.6.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.11-funcrel) |
| 3.6.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.10-funcrel) |
| 3.6.9-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.9-funcrel)  |
| 3.6.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.8-funcrel)  |
| 3.6.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.7-funcrel)  |
| 3.6.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.6-funcrel)  |
| 3.6.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.5-funcrel)  |
| 3.6.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.4-funcrel)  |
| 3.6.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.3-funcrel)  |
| 3.6.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.2-funcrel)  |
| 3.6.1-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.1-funcrel)  |
| 3.6.0-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.0-funcrel)  |
| 3.6.0-beta2    | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.0-beta2)    |
| 3.6.0-beta1    | [https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_sqlanalyzer&ref=\|\|3.6.0-beta1)    |

#### Client Side Support

-   COBOL
-   PowerBuilder (PB)
-   Visual Basic (VB)
-   .NET
-   JAVA
-   C/C++
-   PYTHON
-   RPG/CL\*
-   DML code

The quality rules are calculated on the client side on the SQL Query
objects, even when there is no SQL Analyzer database / schema.

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><div>
Name
</div></th>
<th class="confluenceTh" style="text-align: center;">Calculated only
when we have SQL Analyzer database / schema</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd">Never
use SQL queries with a cartesian product (SQL) (1101000)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Never use SQL queries with a cartesian product
on XXL Tables (SQL) (1101002)</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid
non-indexed SQL queries</a> (1101004)</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid non-indexed XXL SQL queries
(1101006)</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid non-SARGable queries (1101008)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid NATURAL JOIN queries (1101010)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Specify column names instead of column numbers
in ORDER BY clauses (1101012)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid queries using old style join convention
instead of ANSI-Standard joins (SQL) (1101014)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid using the GROUP BY clause (1101018)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Always define column names when inserting
values (1101026)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Use MINUS or EXCEPT operator instead of NOT
EXISTS and NOT IN subqueries (1101028)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p>Avoid exists and not exists independent clauses (SQL) (1101032)</p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p>Avoid using DISTINCT in SQL SELECT statements (1101034)</p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p>Use ANSI standard operators in SQL WHERE clauses (EMBEDDED SQL)
(1101036)</p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p>Avoid OR conditions testing equality on the same identifier in SQL
WHERE clauses (1101038)</p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid mixing ANSI and non-ANSI JOIN syntax in
the same query (1101058)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid using LIKE conditions starting with a
wildcard character (1101060)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid improperly written  triangular joins with
XXL tables (1101066)</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid explicit comparison with NULL
(1101070)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid not aliased Tables (1101072)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid Tables aliases ending with a numeric
suffix (1101074)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid unqualified column
references (1101076)</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid using LIKE conditions without wildcards
(1101102)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid Cobol SQL Cursors without FOR READ ONLY
or FOR FETCH ONLY or FOR UPDATE clauses (EMBEDDED SQL) (1101108)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid LOCK TABLE statements in SQL code for
COBOL Programs (1101112)</td>
<td class="confluenceTd" style="text-align: center;"><br />
</td>
</tr>
</tbody>
</table>

  

#### Special note about XXL/XXS support

See [Working with XXL or XXS tables](../additional-doc/xxl/).

#### Special notes about Quality Rules on client side

Some Quality Rules are calculated on SQL queries on the client-side with
some limitations:

-   Quality Rules will be enabled on client-side code only if the
    server-side code has been analyzed with SQL Analyzer extension.
-   For Java client-side code, SQL statements used in parameters of
    methods including a SQL parametrization rule are analyzed.

Example of call to a parametrized method

``` java
class Foo
{
   final static String TABLE_NAME = "Person";

    void method()
    {
        String query = "select * from " + this.TABLE_NAME;
        java.sql.Statement.execute(query );
    }
}
```

-   But 'queries' visible in the DLM (that need reviewing) are not
    analyzed:

Example ofa query visible in the DLM

``` java
class Foo
{
    // not passed to an execute something
    private final static String text = "select name from Person";
}
```

-   Explicit queries used in an ORM context are analyzed (or not) based
    on if they are visible in Enlighten

-   The following types of queries are analyzed:
    -   COBOL EXEC SQL
    -   RPG/CL
    -   Pro\*C

-   SQL queries found in Python code are analyzed

-   SQL queries found in .properties (Java Property Mapping objects) are
    analyzed

#### Special note about redundant Quality Rules

Please see SQL Analyzer - Redundant Quality Rules not included in the
SQL Analyzer.

#### LOC - Line of Code Counting in SQL Analyzer

LOC (Line of Code) values reported by the CAST Dashboards are
specifically and only for files that are classed by CAST as
"sourceFiles" (File which contains source code), with the ID 1000007.
LOC values are not reported for objects.

## Errors and warning

Please see [Log messages](../additional-doc/logs) for the full list of
analysis errors and warnings.

## Known limitations and issues

### Installation

If you encounter the following error in CAST Server Manager while
installing the SQL Analyzer extension, please perform the workaround
described here and then attempt the installation again. This error may
occur if you have installed a very old and unsupported custom Universal
Analyzer language pack that used the same metamodel type names as used
in the official SQL Analyzer extension.

``` text
SQL Analyzer is incompatible with the schema metamodel. It is generally due to an extension that has changed its ids.
```

### Analysis

-   All name resolution is considered as case insensitive: this may
    produce erroneous links on a case insensitive platform 'playing with
    case': two different tables with the same case insensitive name will
    be both called
-   Procedure resolution handles overriding when the number of
    parameters are matched or number and optionals are matched.
    Otherwise, when calling an overridden procedure, all overrides will
    be called. Below are some examples here is a single call Link,
    between the second func1 and func2:

Match number of parameters

``` sql
CREATE FUNCTION func1() RETURNS integer AS
begin
    DELETE FROM table1 WHERE ID in (SELECT ID FROM table2);
end;

CREATE FUNCTION func1(mode integer) RETURNS integer AS
begin
    DELETE FROM table1 WHERE ID (SELECT ID FROM table2);
end;

CREATE FUNCTION func2(mode integer) RETURNS integer AS
begin
    select func1 (mode); 
end;
```

Match number of parameters and how many are optionals

``` sql
CREATE FUNCTION func1(i_mode integer) RETURNS integer AS
begin
    DELETE FROM table1 WHERE ID in (SELECT ID FROM table2);
end;

CREATE FUNCTION func1(mode integer := 1) RETURNS integer AS
begin
    DELETE FROM table1 WHERE ID in (SELECT ID FROM table2);
end;

CREATE FUNCTION func2(mode integer) RETURNS integer AS
begin
    select func1 (); 
end;
```

-   Dynamic SQL statements are resolved when:
    -   the SQL statement is readable, even for sliced statements.

    -   TABLE1, TABLE2, TABLE3 and TABLE4 are visibles and useSelect
        link were be added

``` sql
CREATE PROCEDURE test
AS
L_QryStr varchar2(4000);
begin
L_QryStr := 'select S.COL1, P.COL2, P.COL3, P.COL4, P.COL5, ' ||
' P.COL6, B.COL7, R.COL8, B.COL9, B.COL10' ||
' from TABLE1 S, TABLE2 P, TABLE3 B, ' ||
' ( select distinct R.COL1, R.COL2 ' ||
' from TABLE4 R ' ||
' where R.COL3 = 99999 ';
EXECUTE IMMEDIATE L_QryStr;
end;
/
```

-   -   test_table is visible and useDelete link is added:

``` sql
CREATE PROCEDURE test
AS
begin
EXECUTE IMMEDIATE 'truncate table test_table'; 
end;
/
```

-   -   test_table is visible and useDelete link is added:

``` sql
CREATE PROCEDURE test
AS
L_QryStr varchar2(4000);
begin
    L_QryStr := 'truncate table ';
    L_QryStr := L_QryStr || ' test_table ';
    EXECUTE IMMEDIATE L_QryStr; 
end;
/
```

-   -   table name is valued via a variable which could be resolved.
    -   emp table is visible and useSelect link will be added

``` sql
CREATE PROCEDURE test
AS
emp_rec  emp%ROWTYPE;
sql_stmt VARCHAR2(200);
begin
    sql_stmt := 'SELECT * FROM || emp_rec.T1 ||  WHERE job = 1';
    EXECUTE IMMEDIATE sql_stmt;
end;
/
```

-   -   emp table is visible and useSelect link will be added

``` sql
CREATE OR REPLACE package body test_package as
 
 type T_1 is table of varchar2(22);
 emp_rec  emp%ROWTYPE;

 PROCEDURE test
is
sql_stmt VARCHAR2(2000);
emp_tab VARCHAR2(200);
begin
    emp_tab := emp_rec;
    sql_stmt := 'SELECT * FROM || emp_tab ||  WHERE job = :j';
    EXECUTE IMMEDIATE sql_stmt;
end test;
 
end;
/
```

-   -   OPEN-FOR-USING : emp table and test procedure are linked by a
        use SELECT link

``` sql
create table emp (col1 int);
/
CREATE OR REPLACE type body test_body as

 type t_emp is table of t_table index by varchar2(22);
 emp_rec  emp%ROWTYPE;

 member PROCEDURE test(o_cursor OUT my_cursor)
IS
BEGIN
   OPEN    o_cursor FOR
   'SELECT  rule_id,
           expression_id,
           parent_expression_id,
           operator
   FROM    emp
   ORDER   BY rule_id, expression_id';
END test;

end;
/
```

``` sql
create table emp (col1 int);
/
CREATE OR REPLACE type body test_body as

 type t_emp is table of t_table index by varchar2(22);
 emp_rec  emp%ROWTYPE;

 member PROCEDURE test(o_cursor OUT my_cursor)
IS
L_SQL varchar(20000) := '';
BEGIN
    L_SQL := 'SELECT  rule_id,
           expression_id,
           parent_expression_id,
           operator
   FROM    emp
   ORDER   BY rule_id, expression_id';
   
   OPEN o_cursor FOR L_SQL;
END test;

end;
/
```

-   ALTER TABLE ... ADD ..., ALTER TABLE/VIEW ... SET SCHEMA
    ..., ALTER DATABASE ... CONVERT TO SCHEMA WITH PARENT ...
    syntaxes are supported. All other syntaxes, such as ALTER TABLE
    ... DELETE .. or ALTER TABLE ... DROP ... or ALTER TABLE ... MODIFY
    ... etc. are not supported.

-   Moving a table from one database/scheme to another is not
    supported through RENAME TABLE *schema1*.tableName1
    TO *schema2*.tableName2.   

-   Sequences are not taken into account and that is not a
    limitation but a choice because they have no effect on transactions
    nor Quality Rules

-   Oracle synonyms on packages are not taken into account.

-   For the QR 1101012 Specify column names instead of column numbers in
    ORDER BY clauses, the case when a function that returns a number or
    a numeric variable is used in order by is not reported to violate
    the rule.
