---
title: "Dealing with this file is analyzed against XXX variant"
linkTitle: "Dealing with this file is analyzed against XXX variant"
type: "docs"
---

## SQL file Vendor

SQL Analyzer identifies few SQL variants, on a basic word matching for
each line of a file (no matter if it is a comment/keyword, etc). If no
matching, then file is considered to be ANSI SQL.

The matching is done during the CREATE SYMBOL step and logged like:

```
*This file is analyzed against SQL Server variant *

*or*

*This file is analyzed against ANSI variant*
```

This is quite "experimental" and when the vendor is not the one you
expect, you can replace it with the real one.

## How to update a vendor

Connected to the local KB execute the following update :

```
Update ObjDsc

Set InfVal = 'Expected vendor'

Where InfTyp = 1101000 and InfSubTyp = 38 and InfVal = 'Wrong vendor'
```

E.g. : When your file has been wrongly identified as SQL Server and you
know is Oracle, just run the following update:

```
Update ObjDsc

Set InfVal = 'Oracle'

Where InfTyp = 1101000 and InfSubTyp = 38 and InfVal = 'SQL Server'
```

If analyzed files are extracted with CAST Database extractor
(uaxDirectory and src files), then vendor is retrieved from
UaxDirectory.

Here is the full list of possible vendors:

-   ANSI
-   PostgreSQL
-   MySQL
-   Db2
-   Db2 for i
-   Informix
-   Oracle
-   SQL Server
-   Teradata
-   CockroachDB
-   NonStop SQL

See also: [SQL Analyzer Patterns](546209865.xlsx) (Microsoft Excel file).

## Vendor algorithm

-   Lf, pf, lf38 and pf38 files are considered as Db2 for i, they are
    not scanned.
-   The vendor of uaxDirectory based analysis is retrieved from the
    uaxDirectory file, they are not scanned.
-   We start by matching the pattern for the case of MySQL statement, if
    matching, file is considered as MySQL.
-   We continue with the statement pattern for Db2 for i. If matching,
    the file is considered as Db2 for i.
-   We continue with the statement pattern for Db2. If matching, the
    file is considered as Db2.
-   We continue with the statement pattern for Teradata. If matching,
    the file is considered as Teradata.
-   We continue with the statement pattern for Informix. If matching,
    the file is considered as Informix.
-   We continue with basic patterns, than with the one line patterns -
    statement separators.
-   The pattern of CREATE PACKAGE BODY is considered to be matched by
    MySQL only if the pattern SET SQL_MODE = ORACLE is also matched.
-   If a single vendor is detected, we stop.
-   If more than 1 vendor is detected we calculate a score : it wins the
    one that match the more patterns. If we have more than 1 with the
    same number of matched patterns : we chose the first one, in
    alphabetic order. If the the pattern is shared by Oracle and
    PostgreSQL and the folder name is matching the variant name, we take
    the variant from the folder name.
-   If in the same AU we have Db2 and Db2 for i files, we consider Db2
    files as Db2 for i.
-   If in the same AU some files are ANSI and we detect a real vendor
    for the rest, the ANSI files are also consider to have the same
    vendor.
-   If in the same folder at least a file is non-ANSI, all others are
    also non-ANSI.
-   The variant could be retrieved from parent folder: if in a folder we
    have only ANSI and in the parent folder we have a non-ANSI, files
    from the child folder are also non-ANSI.
-   For 2 folders, having the same parent folder: in a child folder we
    have non-ANSI and a child folder we have just ANSI, the ANSI files
    were be considered as non-ANSI, they will retrieve the vendor from
    the parent folder. 
-   If no vendor has been detected and the parent name is matching the
    name of one of the supported vendors, the variant is the name of the
    parent folder. E.g.: when the the name of the parent is postgresql,
    and the file is ANSI, we consider the file as PostgreSQL. 
