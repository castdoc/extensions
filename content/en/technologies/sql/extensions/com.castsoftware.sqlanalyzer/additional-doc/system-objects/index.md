---
title: "System Objects"
linkTitle: "System Objects"
type: "docs"
---

## Description

System objects are specific to the database vendor. Sometimes they are
stored in a specific database(s), otherwise in specific
schema(s).
Starting with the version 3.5.8-funcrel we exclude System
Objects from metric calculations, and consider them as external. The
names of the system database / schema can be changed directly in the
configuration files, located in the system_objects subdirectory.
Starting with the version 3.7.4-funcrel, system objects have a new property named "System object".

## System objects by vendor

Some vendors store system objects in specific databases, this is the
case for Microsoft SQL Server and ASE Sybase, which are identified as
"SQL Server". Other vendors store system objects in specific schemas,
and this is the case for almost all other vendors. In the table below we
list the names of System Database / Schema by vendor. They are now
considered as external and we systematically exclude them from metric
calculations.

<table>
<colgroup>
<col />
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Vendor</th>
<th class="confluenceTh">System Database : system_databases.config</th>
<th class="confluenceTh">System Databases that begin with a prefix
: prefixes_system_databases.config</th>
<th class="confluenceTh">System Schema : system_schemas.config</th>
<th class="confluenceTh">System Schemas that begin with a prefix
: prefixes_system_schemas.config</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Db2</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">information_schema syscat systat sysfun sysibm
sysibmadm sysproc syspublic sysadm systools qsys qsys2 sysstat system
sysibminternal sysibmts</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Db2 for i</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">information_schema syscat systat sysfun sysibm
sysibmadm sysproc syspublic sysadm systools qsys qsys2 sysstat system
sysibminternal sysibmts</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Informix</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">sysams</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">MySQL *)</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">mysql information_schema performance_schema
sysmysql sys</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Oracle</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">anonymous appqossys ctxsys dbsnmp dip exfsys ix
mddata mdsys mgmt_view oe olapsys oracle_ocm orddata ordplugins ordsys
outln owbsys owbsys_audit si_informtn_schema spatial_csw_admin_usr
spatial_wfs_admin_usr sys sysman system wmsys xdb xs$null</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">PostgreSQL</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Informix</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">sysams</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Sqlserver )</td>
<td class="confluenceTd">master model msdb resource tempdb
sybsystemprocs sybsystemdb sybsecurity pub2 pubs3 dbccdb sybmgmtdb</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">sys</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Teradata</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">CockroachDB</td>
<td class="confluenceTd">system</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">information_schema crdb_internal pg_catalog
pg_extension</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">NonStop SQL</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">nonstop_sqlmx_</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><br />
</td>
<td class="confluenceTd">definition_schema_version_</td>
</tr>
</tbody>
</table>

-   \*) MySQL and MariaDb
-   \*\*) Microsoft SQL Server and ASE Sybase
