---
title: "Upgrading SQL Analyzer when AIP core is from 8.3.16 to ≥ 8.3.28"
linkTitle: "Upgrading from 8.3.16 to ≥ 8.3.28"
type: "docs"
---

## When analyzed sources are DB2

### Moving from sqlanalyzer\<= 2.6.9-funcrel to sqlanalyzer\>= 3.0.0-alpha2

When analyzed sources are DB2, a new functionality has been added
starting with 3.0.0-alpha2: Database object. When Database is detected
on DB2 table, the analyzer will keep previous internal IDs of tables, in
order to migrate them, to avoid added/delete objects.

If you are migrating from sqlanalyzer \<= 2.6.9-funcrel to sqlanalyzer
\>= 3.0.0-alpha2, please install com.castsoftware.internal.platform
version 0.9.0-beta3 which contains the fix on migration.

## When analyzed sources are SQL Server or Sybase

### Moving from SQL Analyzer\< 3.4.0-beta5 to SQL Analyzer\>= 3.4.0-beta5

When analyzed sources are SQL Server or Sybase, a new functionality
has been added starting with 3.4.0-beta5: Database object. When Database
is detected, the analyzer will keep previous internal IDs of tables, in
order to migrate them, to avoid added/delete objects.

If you are migrating from from SQL Analyzer\< 3.4.0-beta5 to SQL
Analyzer \>= 3.4.0-beta5 please install
com.castsoftware.internal.platform version 0.9.0-beta3 which
contains the fix on migration.
