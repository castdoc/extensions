---
title: "RDBMS Table Sensitive Data"
linkTitle: "RDBMS Table Sensitive Data"
type: "docs"
---

## Introduction

Some RDBMS tables (i.e. SQL Tables and Missing Tables) can
contain sensitive information, for example, information such as:

-   Salary
-   Bonus
-   First Name
-   Last Name
-   Contact details
-   etc.

When using SQL Analyzer ≥ 3.6.10-funcrel, it is possible to
configure a list of custom key words together with their
sensitivity level and then during the analysis, when a key word is
found a property will be added to the object that marks it with the
defined sensitivity level. This property can then be seen and exploited
in CAST Imaging.

CAST Console ≥ 1.26 also provides the ability to check data
sensitive keywords (via a predefined list) for GDPR/PCI-DSS
requirements.

## Missing Table supported languages and extensions

If you need to define custom key words for data sensitivity for "Missing
Tables" objects, you must ensure that the corresponding "Missing Table"
extension is ALSO installed and run during your analysis:

| Language            |  Supported Extension  |
|--------------------:|:----------------------|
| Mainframe Cobol/JCL | ![(tick)](/images/icons/emoticons/check.svg) | [com.castsoftware.mainframe.missingtable](Missing_tables_and_procedures_for_Mainframe) |
| Node.js             | ![(tick)](/images/icons/emoticons/check.svg) | [com.castsoftware.nodejs.missingtable](Missing_tables_and_procedures_for_Node.js)      |
| .NET                | ![(tick)](/images/icons/emoticons/check.svg) | [com.castsoftware.dotnet.missingtable](Missing_tables_and_procedures_for_.NET)         |
| Java/JEE            | ![(tick)](/images/icons/emoticons/check.svg) | [com.castsoftware.jee.missingtable](Missing_tables_and_procedures_for_JEE)             |
| C/C++               | ![(tick)](/images/icons/emoticons/check.svg) | [com.castsoftware.cpp.missingtable](Missing_tables_and_procedures_for_C_and_CPP)       |
| Python              | ![(tick)](/images/icons/emoticons/check.svg) | [com.castsoftware.python.missingtable](Missing_tables_and_procedures_for_Python)       |

## Configuration instructions

### Custom keywords

#### Define the .datasensitive file

Before running a new analysis, you must first define the key words
that will be used to identify the data which you want to flag as
sensitive. To do this, you will need to create an empty text file
with the extension .datasensitive. You should then fill this file
with your key word definitions, using the format shown below:

-   one key word per line
-   three levels of sensitivity can be defined

``` java
keyword=Highly sensitive
keyword=Very sensitive
keyword=Sensitive
```

For example:

``` java
bike=Highly Sensitive
cars=Very Sensitive
```

The three levels of sensitivity that can be defined are case sensitive
and must respect the format listed above otherwise they will be ignored.

#### Deliver the .datasensitive file

The .datasensitive file should be delivered in the same
folder alongside the source code.

### GDPR and PCI-DSS Sensitivity Indicators

There is no configuration required for GDPR and PCI-DSS: CAST Console ≥
1.26 will automatically retrieve the necessary files before analysis, so
you do not need to provide them (as they are standard files).
