---
title: "Use case 2 - JEE and Oracle"
linkTitle: "Use case 2"
type: "docs"
---

## Introduction

After moving from Oracle AIP to SQL Analyzer as explained here [Transitioning from Oracle Server](..),
at the end of the new scan the following report has been
generated: [SQLAnalyzerMetricsReport.csv](415236124.csv).

During transition, Oracle source will not change, the same extraction
will be now analyzed by SQL Analyzer. A new UA analysis unit, equivalent
of all previous Oracle analysis units, has been added. Dependency
between "*JEE and the the new SQL analysis unit"* has been added at the
end.

## New dashboard

![](415236123.png)

## Numbers 

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Before</th>
<th class="confluenceTh">After</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 9<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 9<br />
Nb of saved objects: 75275</td>
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 8<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 8<br />
Nb of saved objects: 75187</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Number of applications with full call graph
: 1 (#32598)<br />
Number of relevant objects in repository : 78924<br />
Number of OMG AFP-links in repository : 519702<br />
Number of DF records / TR end points : 308<br />
Number of transactions : 1<br />
Number of empty transactions : 1<br />
Number of links -&gt; DF records / TR end points : 0<br />
Number of links -&gt; other objects in full graph : 65<br />
Number of transaction links generated (sum) : 65<br />
Number of objects in largest transaction : 0<br />
Number of elements in largest SCC group : 13</p></td>
<td class="confluenceTd"><p>Number of applications with full call graph
: 1 (#32598)<br />
Number of relevant objects in repository : 78776<br />
Number of OMG AFP-links in repository : 519476<br />
Number of DF records / TR end points : 310<br />
Number of transactions : 1<br />
Number of empty transactions : 1<br />
Number of links -&gt; DF records / TR end points : 0<br />
Number of links -&gt; other objects in full graph : 65<br />
Number of transaction links generated (sum) : 65<br />
Number of objects in largest transaction : 0<br />
Number of elements in largest SCC group : 13</p></td>
</tr>
</tbody>
</table>
