---
title: "Use case 5 - JEE, HTML5 and Oracle"
linkTitle: "Use case 5"
type: "docs"
---

## Introduction

After moving from Oracle AIP to SQL Analyzer as explained here [Transitioning from Oracle Server](..),
at the end of the new scan the following report has been
generated: [SQLAnalyzerMetricsReport.csv](416940091.csv).

During transition, Oracle source will not change, the same extraction
will be now analyzed by SQL Analyzer. A new UA analysis unit, equivalent
of all previous Oracle analysis units, has been added. Dependency
between "*JEE and the new SQL analysis unit*", and "*HTML5 analysis unit
and the new SQL analysis unit*" has been added at the end.

## New dashboard

![](416940090.png)

## Numbers

<table class="wrapped relative-table confluenceTable"
style="width: 69.5689%;">
<colgroup>
<col style="width: 50%" />
<col style="width: 49%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Before</th>
<th class="confluenceTh">After</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 10<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 10<br />
Nb of saved objects: 42078</td>
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 7<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 7<br />
Nb of saved objects: 81289</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of applications with full call graph: 1
(#59815)<br />
Number of relevant objects in repository: 76888<br />
Number of OMG AFP-links in repository: 1146090<br />
Number of DF records / TR end points: 801<br />
Number of transactions: 423<br />
Number of empty transactions: 337<br />
Number of links -&gt; DF records/TR end points: 585<br />
Number of links -&gt; other objects in full graph : 5771<br />
Number of transaction links generated (sum): 6356<br />
Number of objects in largest transaction: 213<br />
Number of elements in largest SCC group: 40</td>
<td class="confluenceTd">Number of applications with full call graph: 1
(#59815)<br />
Number of relevant objects in repository: 74255<br />
Number of OMG AFP-links in repository: 1160277<br />
Number of DF records/TR end points: 7576<br />
Number of transactions: 423<br />
Number of empty transactions: 337<br />
Number of links -&gt; DF records/TR end points: 585<br />
Number of links -&gt; other objects in full graph: 5771<br />
Number of transaction links generated (sum): 6356<br />
Number of objects in largest transaction: 213<br />
Number of elements in largest SCC group: 2</td>
</tr>
</tbody>
</table>
