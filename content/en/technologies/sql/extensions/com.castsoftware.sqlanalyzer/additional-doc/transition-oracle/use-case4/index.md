---
title: "Use case 4 - JEE, HTML5 and Oracle"
linkTitle: "Use case 4"
type: "docs"
---

## Introduction

After moving from Oracle AIP to SQL Analyzer as explained here [Transitioning from Oracle Server](..),
at the end of the new scan the following report has been
generated: [SQLAnalyzerMetricsReport.csv](416055673.csv).

During transition, Oracle source will not change, the same extraction
will be now analyzed by SQL Analyzer. A new UA analysis unit, equivalent
of all previous Oracle analysis units, has been added. Dependency
between "*JEE and the new SQL analysis unit"*, and "*HTML5 analysis unit
and the new SQL analysis unit*" has been added at the end.

## New dashboard

![](416055672.png)

Now, you can also note SQL quality rules applied on client code, e.g.:

![](416055671.png)

## Numbers

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Before</th>
<th class="confluenceTh">After</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 54<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 54<br />
Nb of saved objects: 104648</td>
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 51<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 51<br />
Nb of saved objects: 103291</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Number of applications with full call graph:
1 (#32602)<br />
Number of relevant objects in repository: 103346<br />
Number of OMG AFP-links in repository: 303487<br />
Number of DF records/TR end points: 3240<br />
Number of transactions: 1274<br />
Number of empty transactions: 350<br />
Number of links -&gt; DF records/TR end points: 32698<br />
Number of links -&gt; other objects in full graph: 178998<br />
Number of transaction links generated (sum): 211696<br />
Number of objects in largest transaction: 5011<br />
Number of elements in largest SCC group: 8</p></td>
<td class="confluenceTd"><p>Number of applications with full call graph:
1 (#32602)<br />
Number of relevant objects in repository: 99513<br />
Number of OMG AFP-links in repository: 304088<br />
Number of DF records/TR end points: 3381<br />
Number of transactions : 1274<br />
Number of empty transactions: 344<br />
Number of links -&gt; DF records/TR end points: 39353<br />
Number of links -&gt; other objects in full graph: 203867<br />
Number of transaction links generated (sum): 243220<br />
Number of objects in largest transaction: 4877<br />
Number of elements in largest SCC group: 8</p></td>
</tr>
</tbody>
</table>
