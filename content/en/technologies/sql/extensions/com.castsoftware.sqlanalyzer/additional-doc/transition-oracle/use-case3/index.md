---
title: "Use case 3 - COBOL, JCL, HTML5, Oracle and SQL Analyzer"
linkTitle: "Use case 3"
type: "docs"
---

## Introduction

After moving from Oracle AIP to SQL Analyzer as explained here [Transitioning from Oracle Server](..),
at the end of the new scan the following report has been
generated: [SQLAnalyzerMetricsReport.csv](415236131.csv)

During transition, Oracle source will not change, the same extraction
will be now analyzed by SQL Analyzer. A new UA analysis unit, equivalent
of all previous Oracle analysis units, has been added. Dependency
between "*Mainframe and the new SQL analysis unit"*, and "*HTML5
analysis unit and the SQL new analysis unit*" has been added at the end.

## New dashboard

![](416055668.png)

## Numbers

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Before</th>
<th class="confluenceTh">After</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 5<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 5<br />
Nb of saved objects: 208301</td>
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 4<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 4<br />
Nb of saved objects: 206062</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of applications with full call graph : 1
(#65050)<br />
Number of relevant objects in repository : 217085<br />
Number of OMG AFP-links in repository : 241689<br />
Number of DF records / TR end points : 18175<br />
Number of transactions : 3863<br />
Number of empty transactions : 1450<br />
Number of links -&gt; DF records / TR end points : 376822<br />
Number of links -&gt; other objects in full graph : 149314<br />
Number of transaction links generated (sum) : 526136<br />
Number of objects in largest transaction : 6337<br />
Number of elements in largest SCC group : 8</td>
<td class="confluenceTd">Number of applications with full call graph : 1
(#65050)<br />
Number of relevant objects in repository : 210069<br />
Number of OMG AFP-links in repository : 269773<br />
Number of DF records / TR end points : 19067<br />
Number of transactions : 3863<br />
Number of empty transactions : 1450<br />
Number of links -&gt; DF records / TR end points : 376822<br />
Number of links -&gt; other objects in full graph : 149903<br />
Number of transaction links generated (sum) : 526725<br />
Number of objects in largest transaction : 6337<br />
Number of elements in largest SCC group : 8</td>
</tr>
</tbody>
</table>

