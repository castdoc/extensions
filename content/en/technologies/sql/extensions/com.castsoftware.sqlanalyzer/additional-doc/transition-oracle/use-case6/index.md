---
title: "Use case 6 - Cobol, JEE, HTML5 and Oracle"
linkTitle: "Use case 6"
type: "docs"
---

## Introduction

After moving from Oracle AIP to SQL Analyzer as explained here [Transitioning from Oracle Server](..),
at the end of the new scan the following report has been generated:  

[SQLAnalyzerMetricsReport.csv](416940095.csv).

During transition, Oracle source will not change, the same extraction
will be now analyzed by SQL Analyzer. A new UA analysis unit, equivalent
of two previous Oracle analysis units, has been added. Dependency
between, "*JEE and the new one SQL analysis unit*", "*Cobol and the new
one SQL analysis unit*", and "*HTML5 analysis unit and the new one SQL
analysis*" has been added at the end.

## New dashboard

![](416940094.png)

Now, you can also note SQL quality rules applied on client code, e.g.:

![](416940093.png)

## Numbers

<table class="wrapped relative-table confluenceTable"
style="width: 69.5689%;">
<colgroup>
<col style="width: 50%" />
<col style="width: 49%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Before</th>
<th class="confluenceTh">After</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 9<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 9<br />
Nb of saved objects: 417741</td>
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 4<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 4<br />
Nb of saved objects: 407039</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of applications with full call graph: 1
(#31463)<br />
Number of relevant objects in repository: 423494<br />
Number of OMG AFP-links in repository: 611958<br />
Number of DF records / TR end points: 11605<br />
Number of transactions: 192<br />
Number of empty transactions: 165<br />
Number of links -&gt; DF records / TR end points: 10521<br />
Number of links -&gt; other objects in full graph: 13282<br />
Number of transaction links generated (sum): 23803<br />
Number of objects in largest transaction: 9563<br />
Number of elements in largest SCC group: 98</td>
<td class="confluenceTd">Number of applications with full call graph: 1
(#31463)<br />
Number of relevant objects in repository: 407149<br />
Number of OMG AFP-links in repository: 596093<br />
Number of DF records/TR end points: 11641<br />
Number of transactions: 192<br />
Number of empty transactions: 165<br />
Number of links -&gt; DF records/TR end points: 10532<br />
Number of links -&gt; other objects in full graph: 13293<br />
Number of transaction links generated (sum): 23825<br />
Number of objects in largest transaction: 9574<br />
Number of elements in largest SCC group: 98</td>
</tr>
</tbody>
</table>
