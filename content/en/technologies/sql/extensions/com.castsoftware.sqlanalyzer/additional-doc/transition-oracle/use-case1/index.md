---
title: "Use case 1 - JEE, HTML5 and Oracle"
linkTitle: "Use case 1"
type: "docs"
---


## Introduction

After moving from Oracle AIP to SQL Analyzer as explained here [Transitioning from Oracle Server](..),
at the end of the new scan the following report has been
generated: [SQLAnalyzerMetricsReport.csv](415236117.csv)

During transition, Oracle source will not change, the same extraction
will now be analyzed by SQL Analyzer. A new UA analysis unit, equivalent
of all previous Oracle analysis units, has been added. Dependency
between "*JEE and the new SQL analysis unit*", and "*HTML5 analysis unit
and the new SQL analysis unit*" has been added at the end.

## New dashboard

![](416055664.png)

Now, you can also note the SQL quality rules applied on client code,
e.g.:

![](416055663.png)

## Numbers

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Before</th>
<th class="confluenceTh">After</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 6<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 6<br />
Nb of saved objects: 98504</td>
<td class="confluenceTd">Nb of modules: 1<br />
Nb of active analysis units: 4<br />
Nb of inactive analysis units: 0<br />
Total nb of analysis units: 4<br />
Nb of saved objects: 99243</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Number of applications with full call graph
: 1 (#31461)<br />
Number of relevant objects in repository : 109846<br />
Number of OMG AFP-links in repository : 163976<br />
Number of DF records / TR end points : 2657<br />
Number of transactions : 718<br />
Number of empty transactions : 481<br />
Number of links -&gt; DF records / TR end points : 3502<br />
Number of links -&gt; other objects in full graph : 75413<br />
Number of transaction links generated (sum) : 78915<br />
Number of objects in largest transaction : 699<br />
Number of elements in largest SCC group : 10</p></td>
<td class="confluenceTd"><p>Number of applications with full call graph
: 1 (#31461)<br />
Number of relevant objects in repository : 101457<br />
Number of OMG AFP-links in repository : 180292<br />
Number of DF records / TR end points : 11119<br />
Number of transactions : 718<br />
Number of empty transactions : 352<br />
Number of links -&gt; DF records / TR end points : 4130<br />
Number of links -&gt; other objects in full graph : 74515<br />
Number of transaction links generated (sum) : 78645<br />
Number of objects in largest transaction : 696<br />
Number of elements in largest SCC group : 10</p></td>
</tr>
</tbody>
</table>
