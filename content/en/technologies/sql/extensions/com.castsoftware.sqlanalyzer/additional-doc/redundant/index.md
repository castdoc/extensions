---
title: "Redundant Quality Rules not included in the SQL Analyzer"
linkTitle: "Redundant Quality Rules"
type: "docs"
---

The following table lists the Quality Rules that have not been included
in the SQL Analyzer, and provides an explanation for this decision.

<table>
<colgroup>
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">#</th>
<th class="confluenceTh">Name of Quality Rule when not using the SQL
Analyzer extension</th>
<th class="confluenceTh">Name of Quality Rule when using the SQL
Analyzer</th>
<th class="confluenceTh" style="text-align: center;">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">1</td>
<td class="confluenceTd"><em>Avoid SQL queries that no index can
support</em></td>
<td class="confluenceTd"><em>Avoid non-indexed SQL queries</em></td>
<td class="confluenceTd"
style="text-align: center;"><strong>Renamed</strong> for clarity</td>
</tr>
<tr class="even">
<td class="confluenceTd">2</td>
<td class="confluenceTd"><em>Avoid SQL queries using functions on
indexed columns in the WHERE clause</em> *)</td>
<td class="confluenceTd"><em>Avoid non-indexed SQL queries</em></td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<strong>Removed</strong> because:<br />
&#10;<ul>
<li>It is a sub case of <em>Avoid SQL queries that no index can
support</em></li>
<li><p>In some cases, when the join is indexable by another constraint,
then the specification itself is "wrong" (i.e. a false positive exists
by design). Here is an example with <strong>t1.c1</strong> being indexed
- this is an indexed join that the query optimizer uses (t1.c1 = t2.c1)
but by rule design, the query is marked as a violation:</p></li>
</ul>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: DJango"
data-theme="DJango"
style="brush: java; gutter: false; theme: DJango"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a>select <span class="op">*</span> </span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>from Table1 t1<span class="op">,</span> Table2 t2 </span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>where t1<span class="op">.</span><span class="fu">c1</span> <span class="op">=</span> t2<span class="op">.</span><span class="fu">c1</span> </span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a>  and <span class="fu">f</span><span class="op">(</span>t1<span class="op">.</span><span class="fu">c1</span><span class="op">)</span> <span class="op">&gt;</span> <span class="dv">10</span><span class="op">;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">3</td>
<td class="confluenceTd"><em>Avoid SQL queries not using the first
column of a composite index in the WHERE clause</em> *)</td>
<td class="confluenceTd"><em>Avoid non-indexed SQL queries</em></td>
<td class="confluenceTd"
style="text-align: center;"><strong>Removed</strong> because:<br />
&#10;<ul>
<li>It is a sub case of <em>Avoid SQL queries that no index can
support</em></li>
<li>Any violation for this is also a violation for <em>Avoid SQL queries
that no index can support</em></li>
</ul></td>
</tr>
</tbody>
</table>

Note that a \* denotes that the same thing has been done for the
equivalent "XXL" Quality Rule.
