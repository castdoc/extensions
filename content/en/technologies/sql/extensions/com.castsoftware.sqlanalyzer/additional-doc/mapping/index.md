---
title: "Mapping between SQL analyzers"
linkTitle: "Mapping"
type: "docs"
---

## Introduction

The comparison has been done between PL/SQL Analyzer 8.3.8 and SQL
Analyzer 3.5.0-alpha1 (AIP 8.3.8 + datacolumnaccess 1.0.0-beta2).

### Objects

Here is the list of the equivalence between PL/SQL Analyzer and SQL
Analyzer objects:

-   Most of the objects have equivalents on both analyzers
-   Some objects exists only on SQL Analyzer, with no
    equivalent object on PL/SQL Analyzer.
-   Some objects which exists only on PL/SQL Analyzer, with no
    equivalent object on SQL Analyzer.

<table>
<colgroup>
<col />
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Object family</th>
<th class="confluenceTh">Original PL/SQL Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Files</td>
<td class="confluenceTd">(139266) source file (subject to parsing)</td>
<td class="confluenceTd">(1000007) File which contains source code</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="8" class="confluenceTd">Structure</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1000003) Universal Directory</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138000) Oracle Project</td>
<td class="confluenceTd">(1101005) SQL Analyzer Project</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1000001) Universal Project</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /><br />
</td>
<td rowspan="2" class="confluenceTd">No impact, just technical
types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(141813) The result project of a plugin</td>
</tr>
<tr class="even">
<td class="confluenceTd">(139278) Oracle Database Subset</td>
<td class="confluenceTd">(1101002) SQL Analyzer Subset</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138012) Oracle instance</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd"><p>No impact, just structure</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">(138014) Oracle schema</td>
<td class="confluenceTd">(1101011) Schema</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(140519) SQL unknown schema</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd"><p>No impact, just structure</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Script</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101010) SQL Script</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="3" class="confluenceTd">Package</td>
<td class="confluenceTd"><p>(138510) Oracle package header</p></td>
<td rowspan="2" class="confluenceTd">(1101015) Package</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /><br />
</td>
<td rowspan="2" class="confluenceTd"><p>No impact, merged types</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">(138511) Oracle package body</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(140285) Oracle package cursor</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd"><p>No impact on function points.</p>
<p>TQI is impacted only if <em>(1564) Cursor naming convention - prefix
control</em> has been activated.</p>
<p>Normally the PL/SQL rule <em>(1564) Cursor naming convention - prefix
control</em> is inactive by default and it make sense only when
application contains Forms.</p></td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd">Table/View
<p><br />
</p></td>
<td class="confluenceTd"><p>(138017) Oracle table</p></td>
<td rowspan="2" class="confluenceTd"><p>(1101006) Table</p>
<p>(1101093) Unresolved Table</p></td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(140697) Oracle object table</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>(138183) Oracle view</p></td>
<td rowspan="2" class="confluenceTd">(1101013) View</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138791) Oracle materialized view</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"><p>Column Like</p></td>
<td class="confluenceTd"><p>(138832) Oracle table column</p></td>
<td rowspan="2" class="confluenceTd">(1101007) Table Column</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(139223) Oracle view column</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd">Constraint Like</td>
<td class="confluenceTd">(138315) Oracle check table constraint</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact on function points or TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(36117) Oracle default table constraint</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact on function points or TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd">(138393) Oracle foreign key table
constraint</td>
<td class="confluenceTd">(1101016) Foreign Key</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="4" class="confluenceTd">Index Like</td>
<td class="confluenceTd">(138159) Oracle index</td>
<td rowspan="3" class="confluenceTd">(1101008) Index<br />
<br />
</td>
<td rowspan="3" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="3" class="confluenceTd">No impact, merged types<br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(138774) Oracle bitmap index</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138743) Oracle function based index</td>
</tr>
<tr class="even">
<td class="confluenceTd">(138389) Oracle primary key table
constraint</td>
<td class="confluenceTd">(1101020) Unique Constraint</td>
<td class="confluenceTd"></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="8" class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">(138245) Oracle function</td>
<td rowspan="2" class="confluenceTd"><p>(1101012) Function</p>
<p>(1101094) Unresolved Function</p></td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140119) Oracle private stored function</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138244) Oracle procedure</td>
<td rowspan="2" class="confluenceTd">(1101009) Procedure</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140118) Oracle private stored procedure</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138795) Oracle database event trigger</td>
<td rowspan="3" class="confluenceTd">(1101014) Trigger</td>
<td rowspan="3" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="3" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(138219) Oracle ddl trigger</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138220) Oracle dml trigger</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101058) Method</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="6" class="confluenceTd">Type Like</td>
<td class="confluenceTd">(138433) Oracle cursor datatype</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact on function points or TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>(138591) Oracle collection nested table
type</p></td>
<td rowspan="5" class="confluenceTd">(1101049) Type</td>
<td rowspan="5" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="5" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138590) Oracle collection varray type</td>
</tr>
<tr class="even">
<td class="confluenceTd">(138592) Oracle object type</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138628) Oracle object type body</td>
</tr>
<tr class="even">
<td class="confluenceTd">(138589) Oracle incomplete type</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Synonym/Alias Like</td>
<td class="confluenceTd">(140584) Oracle private synonym</td>
<td rowspan="2" class="confluenceTd">(1101040) Synonym</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types<br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140583) Oracle public synonym</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Database link Like</td>
<td class="confluenceTd">(138732) Oracle database link</td>
<td class="confluenceTd">(1101091) Database Link</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Sequence Like</td>
<td class="confluenceTd">(138207) Oracle sequence</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact on function points or TQI</td>
</tr>
</tbody>
</table>

  

### Links

Here is the list of the equivalence between PL/SQL Analyzer and SQL
Analyzer links:

-   Most of the links have equivalents on both analyzers

-   Some links exists only on SQL Analyzer, with no
    equivalent links on PL/SQL Analyzer.

-   Some links which exists only on PL/SQL Analyzer, with no
    equivalent links on SQL Analyzer.  
      

Please find the detailed list,

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Original PL/SQL Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td rowspan="6" class="confluenceTd">Table/View</td>
<td rowspan="3" class="confluenceTd">Table/View/Unresolved Table<br />
<br />
</td>
<td class="confluenceTd">useSelect</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">The case of Table / View / Unresolved Table
selected in a View</td>
</tr>
<tr class="even">
<td class="confluenceTd">referLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">referDelete, referUpdate</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We should install
<strong><strong><strong>com.castsoftware.datacolumnaccess</strong></strong></strong>
to have them.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Fire: Before Insert/Update/Delete, After
Insert/Update/Delete, For each row Insert/Update/Delete and For all row
Insert/Update/Delete </td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">Incorrect modelisation for function points</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">referLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">The case of Columns referenced in a Foreigner
Key</td>
</tr>
<tr class="even">
<td rowspan="5" class="confluenceTd">Procedure Like<br />
<br />
<br />
</td>
<td class="confluenceTd">Procedure Like
<p><br />
</p></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></p>
<p><br />
</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : when table is INSERTED/ UPDATED /
DELETED in a Procedure / Function we add a call link between the
Procedure / Functions and the Trigger.</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>Between Triggers and Tables</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">monitorInsert, monitorUpdate,
monitorDelete</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : adding monitor link between the
Trigger and the triggered Table/View</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We should install
<strong><strong><strong>com.castsoftware.datacolumnaccess</strong></strong></strong>
to have them.</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Constraint Like</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Index Like</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Type Like</td>
<td class="confluenceTd">Type Like</td>
<td class="confluenceTd">inheritLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, new link from Type to super
Type</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd">Package<br />
<br />
</td>
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd">When Columns are accessed directly from
packages.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><p>When Procedure Like are called directly from
packages, TQI is impacted via Unreferenced rules.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><p>When Table / View are accessed directly from
packages, TQI is impacted via Unreferenced rules.</p></td>
</tr>
<tr class="odd">
<td rowspan="3" class="confluenceTd">Synonym / Alias Like<br />
<br />
</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Package</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd">Script</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd">Client Code</td>
<td class="confluenceTd"><p>Table/View</p></td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We add use links when dependency is added
between the client code and SQL code.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We add use links when dependency is added
between the client code and SQL code.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : when table is INSERTED/ UPDATED /
DELETED in a Client Code we add a call link between the Client Code and
the Trigger.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature. We add access links when
dependency is added between the client code and SQL code and
com.castsoftware.datacolumnaccess is installed.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Oracle Forms and Reports Analyzer objects</td>
<td class="confluenceTd">PL/SQL Analyzer objects</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd">Not supported</td>
</tr>
</tbody>
</table>

### Rules

Here is the list of the equivalence between PL/SQL Analyzer and SQL
Analyzer rules:

-   Some quality rules exists only on SQL Analyzer, with no
    equivalent quality rule on PL/SQL Analyzer.
-   Some quality rules which exists only on PL/SQL Analyzer,
    with no equivalent quality rule on SQL Analyzer.
-   Most of the rules have equivalents on both analyzers

  

Please find the detailed list,

<table class="wrapped relative-table confluenceTable"
style="width: 100.0%;">
<colgroup>
<col style="width: 41%" />
<col style="width: 34%" />
<col style="width: 5%" />
<col style="width: 18%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Original PL/SQL Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">(7902) Avoid SQL queries that no index can
support<br />
(7418) Avoid SQL queries using functions on indexed columns in the WHERE
clause<br />
(7428) Avoid SQL queries not using the first column of a composite index
in the WHERE clause</td>
<td class="confluenceTd">(1101004) Avoid non-indexed SQL queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>(7904) Avoid SQL queries on XXL tables that
no index can support </p>
<p>(7658) Avoid SQL queries on XXL Tables using Functions on indexed
Columns in the WHERE clause</p>
<p>(7642) Avoid SQL queries on XXL tables not using the first column of
a composite index in the WHERE clause</p></td>
<td class="confluenceTd">(1101006) Avoid non-indexed XXL SQL
queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7950) Avoid definition of synonym as PUBLIC in
PL/SQL context</td>
<td class="confluenceTd">(1101078) Always prefer PRIVATE to PUBLIC
synonym</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7958) Avoid orphaned synonyms in PL/SQL
context</td>
<td class="confluenceTd">(1101080) Avoid orphaned synonyms</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7946) Avoid queries using old style join
convention  instead of ANSI-Standard joins</td>
<td class="confluenceTd">(1101014) Avoid queries using old style join
convention instead of ANSI-Standard joins</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1576) Use varchar2 instead of char and
varchar</td>
<td class="confluenceTd">(1101086) Always use VARCHAR2 and NVARCHAR2
instead of CHAR, VARCHAR or NCHAR</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7806) Avoid Artifacts with Group By</td>
<td class="confluenceTd">(1101018) Avoid using the GROUP BY clause</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7790) Avoid Cursors inside a loop</td>
<td class="confluenceTd">(1101084) Avoid Cursors inside a loop
(SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(1558) Package naming convention - prefix
control</td>
<td class="confluenceTd">(1101046) Package naming convention - prefix
control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1560) Package Function naming convention -
prefix control </td>
<td class="confluenceTd">(1101048) Package Function naming convention -
prefix control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(1562) Package Stored Procedure naming
convention - prefix control</td>
<td class="confluenceTd">(1101050) Package Stored Procedure naming
convention - prefix control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1574) Use at most one statement per line</td>
<td class="confluenceTd">(1101062) Avoid using multiple statements per
line (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(1578) Avoid using LONG &amp; LONG RAW datatype
for Table Columns </td>
<td class="confluenceTd">(1101088) Avoid using LONG and LONG RAW
datatypes</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1580) Avoid using execute immediate</td>
<td class="confluenceTd">(1101024) Avoid using dynamic SQL in SQL
Artifacts</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(1582) Avoid large Tables - too many
columns</td>
<td class="confluenceTd">(1101056) Avoid large Tables - too many columns
(SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1588) Use WHEN OTHERS in exception
management</td>
<td class="confluenceTd">(1101052) Avoid using WHEN OTHERS without
exception management (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(1608) Avoid cascading Triggers</td>
<td class="confluenceTd">(1101064) Avoid cascading Triggers (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7660) Never use SQL queries with a cartesian
product on XXL Tables</td>
<td class="confluenceTd">(1101002) Never use SQL queries with a
cartesian product on XXL Tables (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7820) Never use SQL queries with a cartesian
product</td>
<td class="confluenceTd">(1101000) Never use SQL queries with a
cartesian product</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7770) Avoid Artifacts with too many
parameters</td>
<td class="confluenceTd">(1101016) Avoid Artifacts with too many
parameters (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7822) Avoid Artifacts with queries on more
than 4 Tables </td>
<td class="confluenceTd">(1101030) Avoid Artifacts with queries on too
many Tables and/or Views</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7948) Do not mix Ansi joins syntax  with
Oracle proprietary joins syntax in the same query</td>
<td class="confluenceTd">(1101058) Avoid mixing ANSI and non-ANSI JOIN
syntax in the same query</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7952) Avoid synonym with both private &amp;
PUBLIC Definition in PL/SQL context </td>
<td class="confluenceTd">(1101068) Avoid synonym with both private and
public definition</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7960) Avoid looping chain of synonyms in
PL/SQL context </td>
<td class="confluenceTd">(1101082) Avoid looping chain of synonyms</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(8036) Avoid improperly written triangular
joins with XXL tables in PL/SQL code </td>
<td class="confluenceTd">(1101066) Avoid improperly written triangular
joins with XXL tables</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(8080) Avoid exists independent clauses</td>
<td class="confluenceTd">(1101032) Avoid exists and not exists
independent clauses (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(8082) Avoid Tables without Primary Key</td>
<td class="confluenceTd">(1101022) Avoid Tables without Primary Key
(SQL)</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI : the rule changed from CRITICAL to
generic</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1596) Avoid using "nullable" Columns except in
the last position in a Table</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd"><p>TQI</p>
<p>Replaced by (1101008) Avoid non-SARGable queries.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">(1564) Cursor naming convention - prefix
control </td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /><br />
</td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1598) Avoid Rule HINT /*+ rule */ or --+ rule
in PL/SQL code </td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /><br />
</td>
<td class="confluenceTd"><p>TQI</p>
<p>RBO is no more supported since Oracle 10g.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7420) Avoid SQL queries with implicit
conversions in the WHERE clause </td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /><br />
</td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7662) Avoid SQL queries on XXL Tables with
implicit conversions in the WHERE clause</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /><br />
</td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7810) Avoid Artifacts with a Complex SELECT
Clause</td>
<td class="confluenceTd">(1101098) Avoid Artifacts with a Complex SELECT
Clause (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101026) Always define column names when
inserting values</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101034) Avoid using DISTINCT in SQL SELECT
statements</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101038) Avoid OR conditions testing equality
on the same identifier in SQL WHERE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101040) Avoid empty catch blocks</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101008) Avoid non-SARGable queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101060) Avoid using LIKE conditions starting
with a wildcard character</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101070) Avoid explicit comparison with
NULL</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101072) Avoid not aliased Tables</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101076) Avoid unqualified column
references</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101074) Avoid Tables aliases ending with a
numeric suffix</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101054) Never use WHEN OTHER THEN NULL</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101044) View naming convention - character
set control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101042) Table naming convention - character
set control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101036) Use ANSI standard operators in SQL
WHERE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature for the client code</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101028) Use MINUS or EXCEPT operator instead
of NOT EXISTS and NOT IN subqueries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101020) Avoid using quoted identifiers</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101012) Avoid specifying column numbers
instead of column names in ORDER BY clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101010) Avoid NATURAL JOIN queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101092) Avoid Procedures using an Insert,
Update, Delete, Create Table or Select without including error
management (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101096) Avoid SQL injection in dynamic SQL
for Oracle</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
</td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101102) Avoid using LIKE conditions without
wildcards</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101104) Avoid XXL tables without primary key
/ unique key constraint / unique index</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101106) Avoid tables without primary key /
unique key constraint / unique index</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101108) Avoid Cobol SQL Cursors without FOR
READ ONLY or FOR FETCH ONLY or FOR UPDATE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, only for SQL in Cobol</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101112) Avoid LOCK TABLE statements in SQL
code for COBOL Programs</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, only for SQL in Cobol</td>
</tr>
</tbody>
</table>

## DB2 UDB Analyzer and DB2 z/OS Analyzer - SQL Analyzer

The comparison has been done between DB2 UDB Analyzer 8.2.1 / DB2 zOS
Analyzer 8.2.1 and SQL Analyzer 3.1.0-funcrel (AIP 8.3.8 +
datacolumnaccess 1.0.0-beta2).

### Objects

Here is the list of the equivalence between DB2 UDB Analyzer, DB2 z/OS
Analyzer and SQL Analyzer objects:

-   Most of the objects have equivalents on both analyzers

-   Some objects exists only on SQL Analyzer, with no
    equivalent object on DB2 UDB Analyzer, DB2 z/OS Analyzer.

-   Some objects which exists only on DB2 UDB Analyzer, DB2 z/OS
    Analyzer, with no equivalent object on SQL Analyzer.

<table class="relative-table wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Object family</th>
<th class="confluenceTh">Original DB2 UDB Analyzer, DB2 z/OS
Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Files</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1000007) File which contains source code</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td rowspan="8" class="confluenceTd">Structure</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1000003) Universal Directory</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /><br />
<br />
</td>
<td class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>(299) DB2 System</p></td>
<td rowspan="2" class="confluenceTd">(1101005) SQL Analyzer Project</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, just structure, merged
types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(300) DB2 Instance</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><br />
</td>
<td class="confluenceTd"><p>(1000001) Universal Project</p></td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, just technical
types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(141813) The result project of a plugin</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(107001) DB2 Subset</td>
<td class="confluenceTd">(1101002) SQL Analyzer Subset</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="even">
<td class="confluenceTd">(313) DB2 Database</td>
<td class="confluenceTd">(1101061) Database</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(301) Schema</td>
<td class="confluenceTd">(1101011) Schema</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Script</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101010) SQL Script</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Table/View</td>
<td class="confluenceTd">(1) SQL Table</td>
<td class="confluenceTd">(1101006) Table</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(9) SQL View</td>
<td class="confluenceTd">(1101013) View</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101007) Table Column</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd">Constraint Like</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101016) Foreign Key</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Index Like</td>
<td class="confluenceTd">(6) SQL Index</td>
<td class="confluenceTd">(1101008) Index</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101020) Unique Constraint</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="3" class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">(3) SQL Procedure</td>
<td class="confluenceTd">(1101009) Procedure</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(10) SQL Function</td>
<td class="confluenceTd">(1101012) Function</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(4) SQL Trigger</td>
<td class="confluenceTd">(1101014) Trigger</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd">UDT Like</td>
<td class="confluenceTd">(358) DB2 Distinct UDT</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">Impact on TQI, when UDT is linked to a DB2
object covered by an Unreferenced rule</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(302) DB2 Structured UDT</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">Impact on TQI, when UDT is linked to a DB2
object covered by an Unreferenced rule</td>
</tr>
<tr class="even">
<td class="confluenceTd">Type Like</td>
<td class="confluenceTd">(16) TYPE</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact on function points or TQI</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Synonym/Alias Like</td>
<td class="confluenceTd">(303) DB2 Alias</td>
<td rowspan="2" class="confluenceTd">(1101040) Synonym</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(304) DB2 Nickname</td>
</tr>
</tbody>
</table>

### Links

Here is the list of the equivalence between DB2 UDB Analyzer, DB2 z/OS
Analyzer and SQL Analyzer links:

-   Most of the links have equivalents on both analyzers
-   Some links exists only on SQL Analyzer, with no
    equivalent links on DB2 UDB Analyzer, DB2 z/OS Analyzer.
-   Some links which exists only on DB2 UDB Analyzer, DB2 z/OS
    Analyzer, with no equivalent links on SQL Analyzer.

Please find the detailed list,

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Original DB2 UDB Analyzer, DB2 z/OS
Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td rowspan="7" class="confluenceTd">Table/View</td>
<td rowspan="3" class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">referLink, referDelete, referUpdate</td>
<td class="confluenceTd"><p><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">inheritLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><p>Impact on TQI related to Unreferenced
rules</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : we should install
<strong><strong><strong>com.castsoftware.datacolumnaccess</strong></strong></strong>
to have them.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Fire: Before Insert/Update/Delete, After
Insert/Update/Delete, For each row Insert/Update/Delete and For all row
Insert/Update/Delete </td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">Incorrect modelisation for function points</td>
</tr>
<tr class="odd">
<td class="confluenceTd">UDT Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd">Impact on TQI related to Unreferenced
rules</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">referLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature for the case of Columns referenced
in a Foreigner Key</td>
</tr>
<tr class="odd">
<td rowspan="10" class="confluenceTd">Procedure Like<br />
<br />
<br />
<br />
<br />
<br />
</td>
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></p>
<p><br />
</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : when table is INSERTED/ UPDATED /
DELETED in a Procedure / Function we add a call link between the
Procedure / Functions and the Trigger</td>
</tr>
<tr class="odd">
<td rowspan="3" class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">monitorInsert, monitorUpdate,
monitorDelete</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : adding monitor link between the
Trigger and the triggered Table/View</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>ddlLinkCreate, ddlLinkDrop,
lockLink</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd">Impact on TQI related to Unreferenced
rules</td>
</tr>
<tr class="even">
<td class="confluenceTd">UDT Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd">Impact on TQI related to Unreferenced
rules</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We should install
<strong><strong><strong>com.castsoftware.datacolumnaccess</strong></strong></strong>
to have them.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Cobol Program</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, when the LANGUAGE parameter of the
procedure is Cobol and external name is matched with an existing Cobol
Program and dependency is added between client code and SQL code.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">C/C++ Function</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, when the LANGUAGE of the procedure
is C and external name is matched with an existing C/C++ Function and
dependency is added between client code and SQL code.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Java Method</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, when the LANGUAGE of the procedure
is JAVA and external name is matched with an existing Java Method and
dependency is added between client code and SQL code.</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Constraint Like</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Index Like</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="3" class="confluenceTd">Synonym / Alias Like<br />
<br />
</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd"><p>DB2 : prototypeLink</p>
<p>SQL : relyonLink</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd">UDT Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact</td>
</tr>
<tr class="even">
<td class="confluenceTd">UDT Like</td>
<td class="confluenceTd">UDT Like</td>
<td class="confluenceTd">inheritLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td class="confluenceTd">No impact</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Script</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="4" class="confluenceTd">Client Code</td>
<td class="confluenceTd"><p>Table/View</p></td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We add use links when dependency is added
between the client code and SQL code.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We add call links when dependency is added
between the client code and SQL code.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : when table is INSERTED/ UPDATED /
DELETED in a Client Code we add a call link between the Client Code and
the Trigger.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd"><p>New feature. We add access links when
dependency is added between the client code and SQL code and
<strong>com.castsoftware.datacolumnaccess</strong> is
installed.</p></td>
</tr>
</tbody>
</table>

### Rules

Here is the list of the equivalence between DB2 UDB Analyzer, DB2 z/OS
Analyzer and SQL Analyzer rules:

-   Some quality rules exists only on SQL Analyzer, with no
    equivalent quality rule on DB2 UDB Analyzer, DB2 z/OS Analyzer.

-   Some quality rules which exists only on DB2 UDB Analyzer, DB2
    z/OS Analyzer, with no equivalent quality rule on SQL Analyzer.

-   Most of the rules have equivalents on both analyzers  
      

<table class="wrapped relative-table confluenceTable"
style="width: 100.0%;">
<colgroup>
<col style="width: 42%" />
<col style="width: 35%" />
<col style="width: 4%" />
<col style="width: 17%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Original DB2 UDB Analyzer, DB2 z/OS
Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">(7902) Avoid SQL queries that no index can
support<br />
(7418) Avoid SQL queries using functions on indexed columns in the WHERE
clause<br />
(7428) Avoid SQL queries not using the first column of a composite index
in the WHERE clause</td>
<td class="confluenceTd">(1101004) Avoid non-indexed SQL queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Avoid Artifacts with too many
parameters(7904) Avoid SQL queries on XXL tables that no index can
support </p>
<p>(7658) Avoid SQL queries on XXL Tables using Functions on indexed
Columns in the WHERE clause</p>
<p>(7642) Avoid SQL queries on XXL tables not using the first column of
a composite index in the WHERE clause</p></td>
<td class="confluenceTd">(1101006) Avoid non-indexed XXL SQL
queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101078) Always prefer PRIVATE to PUBLIC
synonym</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101080) Avoid orphaned synonyms</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101014) Avoid queries using old style join
convention instead of ANSI-Standard joins</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7806) Avoid Artifacts with Group By</td>
<td class="confluenceTd">(1101018) Avoid using the GROUP BY clause</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7420) Avoid SQL queries with implicit
conversions in the WHERE clause </td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7662) Avoid SQL queries on XXL Tables with
implicit conversions in the WHERE clause</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7810) Avoid Artifacts with a Complex SELECT
Clause</td>
<td class="confluenceTd">(1101098) Avoid Artifacts with a Complex SELECT
Clause (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101084) Avoid Cursors inside a loop
(SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101062) Avoid using multiple statements per
line (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101024) Avoid using dynamic SQL in SQL
Artifacts</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101056) Avoid large Tables - too many columns
(SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101064) Avoid cascading Triggers (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7660) Never use SQL queries with a cartesian
product on XXL Tables</td>
<td class="confluenceTd">(1101002) Never use SQL queries with a
cartesian product on XXL Tables (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7820) Never use SQL queries with a cartesian
product</td>
<td class="confluenceTd">(1101000) Never use SQL queries with a
cartesian product</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7770) Avoid Artifacts with too many
parameters</td>
<td class="confluenceTd">(1101016) Avoid Artifacts with too many
parameters (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7822) Avoid Artifacts with queries on more
than 4 Tables</td>
<td class="confluenceTd">(1101030) Avoid Artifacts with queries on too
many Tables and/or Views</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">New feature : the number of accessed Tables /
Views could be changed, default value is 4.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101058) Avoid mixing ANSI and non-ANSI JOIN
syntax in the same query</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101068) Avoid synonym with both private and
public definition</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101082) Avoid looping chain of synonyms</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101066) Avoid improperly written triangular
joins with XXL tables</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101032) Avoid exists and not exists
independent clauses (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101022) Avoid Tables without Primary Key
(SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101026) Always define column names when
inserting values</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101034) Avoid using DISTINCT in SQL SELECT
statements</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101038) Avoid OR conditions testing equality
on the same identifier in SQL WHERE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101040) Avoid empty catch blocks</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101008) Avoid non-SARGable queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101060) Avoid using LIKE conditions starting
with a wildcard character</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101070) Avoid explicit comparison with
NULL</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101072) Avoid not aliased Tables</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101076) Avoid unqualified column
references</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101074) Avoid Tables aliases ending with a
numeric suffix</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(6588) View naming convention - character set
control</td>
<td class="confluenceTd">(1101044) View naming convention - character
set control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(6590) Table naming convention - character set
control</td>
<td class="confluenceTd">(1101042) Table naming convention - character
set control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101036) Use ANSI standard operators in SQL
WHERE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature for the client code</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101028) Use MINUS or EXCEPT operator instead
of NOT EXISTS and NOT IN subqueries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101020) Avoid using quoted identifiers</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101012) Avoid specifying column numbers
instead of column names in ORDER BY clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101010) Avoid NATURAL JOIN queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101090) Avoid Tables without a clustered
Index (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101102) Avoid using LIKE conditions without
wildcards</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101104) Avoid XXL tables without primary key
/ unique key constraint / unique index</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101106) Avoid tables without primary key /
unique key constraint / unique index</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101108) Avoid Cobol SQL Cursors without FOR
READ ONLY or FOR FETCH ONLY or FOR UPDATE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, only for SQL in Cobol</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101112) Avoid LOCK TABLE statements in SQL
code for COBOL Programs</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, only for SQL in Cobol</td>
</tr>
</tbody>
</table>

## T-SQL Analyzer - SQL Analyzer

The comparison has been done between T-SQL Analyzer 8.3.8 and SQL
Analyzer 3.2.0-funcrel (AIP 8.3.8 + datacolumnaccess 1.0.0-beta2).

### Objects

Here is the list of the equivalence between T-SQL Analyzer and SQL
Analyzer objects:

-   Most of the objects have equivalents on both analyzers
-   Some objects exists only on SQL Analyzer, with no
    equivalent object on T-SQL Analyzer.

<!-- -->

-   Some objects which exists only on T-SQL Analyzer, with no
    equivalent object on SQL Analyzer.

  

Please find the detailed list:

<table class="relative-table wrapped confluenceTable"
style="width: 95.344%;">
<colgroup>
<col style="width: 8%" />
<col style="width: 16%" />
<col style="width: 16%" />
<col style="width: 4%" />
<col style="width: 54%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Object family</th>
<th class="confluenceTh">Original T-SQL Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Files</td>
<td class="confluenceTd">(139266) source file (subject to parsing)</td>
<td class="confluenceTd">(1000007) File which contains source code</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="20" class="confluenceTd">Structure
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1000003) Universal Directory</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>(138003) Sybase Project</p></td>
<td rowspan="2" class="confluenceTd">(1101005) SQL Analyzer Project</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, just structure, merged
types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(138069) Microsoft Project</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1000001) Universal Project</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, just technical
types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(141813) The result project of a plugin</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>(141014) Sybase Database Subset</p></td>
<td rowspan="2" class="confluenceTd">(1101002) SQL Analyzer Subset</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="even">
<td class="confluenceTd">(141003) Microsoft Database Subset</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(140982) Sybase instance</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td rowspan="2" class="confluenceTd"><p>No impact, just
structure</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">(140952) Microsoft instance</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>(140981) Sybase database</p>
<p><br />
</p></td>
<td rowspan="2" class="confluenceTd">(1101061) Database</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140949) Microsoft database</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(140980) Sybase schema</td>
<td rowspan="2" class="confluenceTd">(1101011) Schema</td>
<td rowspan="2" class="confluenceTd"></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140950) Microsoft schema</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(141783) Sybase unknown instance</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td rowspan="2" class="confluenceTd"><p>No impact, just
structure</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">(141784) Microsoft unknown instance</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(141751) Sybase unknown database</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td rowspan="2" class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="even">
<td class="confluenceTd">(141752) Microsoft unknown database</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(140979) Sybase unknown schema</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><img
src="images/icons/emoticons/information.svg"
class="emoticon emoticon-information" data-emoticon-name="information"
alt="(info)" /></td>
<td rowspan="2" class="confluenceTd">No impact, just structure</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140951) Microsoft unknown schema</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Script</td>
<td class="confluenceTd">(1101010) SQL Script</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd">Table/View<br />
&#10;<p><br />
</p></td>
<td class="confluenceTd">(140978) Sybase table<br />
&#10;<p><br />
</p></td>
<td rowspan="2" class="confluenceTd"><p>(1101006) Table</p></td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138871) Microsoft table</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140976) Sybase view</td>
<td rowspan="2" class="confluenceTd">(1101013) View</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>(138873) Microsoft view</p></td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd"><p>Column Like</p></td>
<td class="confluenceTd"><p>(140977) Sybase table column</p></td>
<td rowspan="4" class="confluenceTd">(1101007) Table Column</td>
<td rowspan="4" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="4" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138872) Microsoft table column</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140975) Sybase view column</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(139224) Microsoft view column</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd">Constraint Like</td>
<td class="confluenceTd">(141498) Sybase foreign key table
constraint</td>
<td rowspan="2" class="confluenceTd">(1101016) Foreign Key</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(141495) Microsoft foreign key table
constraint</td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd">Index Like</td>
<td class="confluenceTd">(140971) Sybase index</td>
<td rowspan="2" class="confluenceTd">(1101008) Index<br />
<br />
</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /><br />
<br />
</td>
<td rowspan="2" class="confluenceTd">No impact, merged types<br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138877) Microsoft index</td>
</tr>
<tr class="even">
<td class="confluenceTd">(141497) Sybase primary key table
constraint</td>
<td rowspan="2" class="confluenceTd">(1101020) Unique Constraint</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(141494) Microsoft primary key table
constraint</td>
</tr>
<tr class="even">
<td rowspan="6" class="confluenceTd">Procedure Like</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101012) Function</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138876) Microsoft function</td>
<td class="confluenceTd">(1101012) Function</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140973) Sybase procedure</td>
<td rowspan="2" class="confluenceTd">(1101009) Procedure</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(138875) Microsoft procedure</td>
</tr>
<tr class="even">
<td class="confluenceTd">(140967) Sybase DML trigger</td>
<td rowspan="2" class="confluenceTd">(1101014) Trigger</td>
<td rowspan="2" class="confluenceTd"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td rowspan="2" class="confluenceTd">No impact, merged types</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>(138891) Microsoft DML trigger</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Synonym/Alias Like</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101040) Synonym</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /><br />
&#10;<p><br />
</p></td>
<td class="confluenceTd">New feature<br />
<br />
</td>
</tr>
</tbody>
</table>

### Links

Here is the list of the equivalence between T-SQL Analyzer and SQL
Analyzer links:

-   Most of the links have equivalents on both analyzers

-   Some links exists only on SQL Analyzer, with no
    equivalent links on T-SQL Analyzer.

-   Some links which exists only on T-SQL Analyzer, with no
    equivalent links on SQL Analyzer.  
      

  
Please find the detailed list,

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Original T-SQL Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td rowspan="6" class="confluenceTd">Table/View</td>
<td rowspan="3" class="confluenceTd">Table/View<br />
<br />
</td>
<td class="confluenceTd">useSelect</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">The case of Table / View selected in a
View</td>
</tr>
<tr class="even">
<td class="confluenceTd">referLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">referDelete, referUpdate</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We should install
<strong>com.castsoftware.datacolumnaccess</strong> to have them.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Fire: Before Insert/Update/Delete, After
Insert/Update/Delete, For each row Insert/Update/Delete and For all row
Insert/Update/Delete </td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">Incorrect modelisation for function points</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">referLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">The case of Columns referenced in a Foreigner
Key</td>
</tr>
<tr class="even">
<td rowspan="5" class="confluenceTd">Procedure Like<br />
<br />
<br />
</td>
<td class="confluenceTd">Procedure Like
<p><br />
</p></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></p>
<p><br />
</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : when table is INSERTED/ UPDATED /
DELETED in a Procedure / Function we add a call link between the
Procedure / Functions and the Trigger.</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>Between Triggers and Tables</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">monitorInsert, monitorUpdate,
monitorDelete</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : adding monitor link between the
Trigger and the triggered Table/View</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We should install
<strong>com.castsoftware.datacolumnaccess</strong> to have them.</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Constraint Like</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Index Like</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Synonym / Alias Like<br />
<br />
</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td rowspan="2" class="confluenceTd">New feature<br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd">Script</td>
<td class="confluenceTd">Table/View</td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td rowspan="4" class="confluenceTd">Client Code</td>
<td class="confluenceTd"><p>Table/View</p></td>
<td class="confluenceTd">useSelect, useInsert, useUpdate, useDelete</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We add use links when dependency is added
between the client code and SQL code.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Procedure Like</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">We add use links when dependency is added
between the client code and SQL code.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature : when table is INSERTED/ UPDATED /
DELETED in a Client Code we add a call link between the Client Code and
the Trigger.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column Like</td>
<td class="confluenceTd">accessRead, accessWrite</td>
<td class="confluenceTd"><img src="images/icons/emoticons/forbidden.svg"
class="emoticon emoticon-minus" data-emoticon-name="minus"
alt="(minus)" /></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature. We add access links when
dependency is added between the client code and SQL code and
<strong>com.castsoftware.datacolumnaccess</strong> is installed.</td>
</tr>
</tbody>
</table>

### Rules

Here is the list of the equivalence between T-SQL Analyzer and SQL
Analyzer rules:

-   Some quality rules exists only on SQL Analyzer, with no
    equivalent quality rule on T-SQL Analyzer.
-   Some quality rules which exists only on T-SQL Analyzer,
    with no equivalent quality rule on SQL Analyzer.
-   Most of the rules have equivalents on both analyzers

  

Please find the detailed list,

<table class="relative-table wrapped confluenceTable"
style="width: 99.9305%;">
<colgroup>
<col style="width: 37%" />
<col style="width: 29%" />
<col style="width: 4%" />
<col style="width: 28%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Original T-SQL Analyzer</th>
<th class="confluenceTh">Counterpart SQL Analyzer</th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">(7902) Avoid SQL queries that no index can
support<br />
(7418) Avoid SQL queries using functions on indexed columns in the WHERE
clause<br />
(7428) Avoid SQL queries not using the first column of a composite index
in the WHERE clause</td>
<td class="confluenceTd">(1101004) Avoid non-indexed SQL queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>(7904) Avoid SQL queries on XXL tables that
no index can support </p>
<p>(7658) Avoid SQL queries on XXL Tables using Functions on indexed
Columns in the WHERE clause</p>
<p>(7642) Avoid SQL queries on XXL tables not using the first column of
a composite index in the WHERE clause</p></td>
<td class="confluenceTd">(1101006) Avoid non-indexed XXL SQL
queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7946) Avoid queries using old style join
convention  instead of ANSI-Standard joins</td>
<td class="confluenceTd">(1101014) Avoid queries using old style join
convention instead of ANSI-Standard joins</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7806) Avoid Artifacts with Group By</td>
<td class="confluenceTd">(1101018) Avoid using the GROUP BY clause</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7790) Avoid Cursors inside a loop</td>
<td class="confluenceTd">(1101084) Avoid Cursors inside a loop
(SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(1574) Use at most one statement per line</td>
<td class="confluenceTd">(1101062) Avoid using multiple statements per
line (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(1582) Avoid large Tables - too many
columns</td>
<td class="confluenceTd">(1101056) Avoid large Tables - too many columns
(SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7660) Never use SQL queries with a cartesian
product on XXL Tables</td>
<td class="confluenceTd">(1101002) Never use SQL queries with a
cartesian product on XXL Tables (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7820) Never use SQL queries with a cartesian
product</td>
<td class="confluenceTd">(1101000) Never use SQL queries with a
cartesian product</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7770) Avoid Artifacts with too many
parameters</td>
<td class="confluenceTd">(1101016) Avoid Artifacts with too many
parameters (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7822) Avoid Artifacts with queries on more
than 4 Tables </td>
<td class="confluenceTd">(1101030) Avoid Artifacts with queries on too
many Tables and/or Views</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(8080) Avoid exists independent clauses</td>
<td class="confluenceTd">(1101032) Avoid exists and not exists
independent clauses (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(8082) Avoid Tables without Primary Key</td>
<td class="confluenceTd">(1101022) Avoid Tables without Primary Key
(SQL)</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI : the rule changed from CRITICAL to
generic</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7420) Avoid SQL queries with implicit
conversions in the WHERE clause </td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7662) Avoid SQL queries on XXL Tables with
implicit conversions in the WHERE clause</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd">(4062) Avoid Functions and Procedures doing an
Insert, Update or Delete without managing a transaction</td>
<td class="confluenceTd">(1101100) Avoid Functions and Procedures doing
an Insert, Update or Delete without managing a transaction (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(4064) Avoid Procedures using an Insert,
Update, Delete, Create Table or Select without including error
management</td>
<td class="confluenceTd">(1101092) Avoid Procedures using an Insert,
Update, Delete, Create Table or Select without including error
management (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(4066) Avoid Stored Procedures not returning a
status value</td>
<td class="confluenceTd">(1101094) Avoid Stored Procedures not returning
a status value (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(4070) Avoid use of "truncate table"</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd">(4076) Avoid using temporary Objects</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(4084) Avoid nested Stored Procedures using
temporary Tables</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><img src="images/icons/emoticons/warning.svg"
class="emoticon emoticon-warning" data-emoticon-name="warning"
alt="(warning)" /></td>
<td class="confluenceTd">TQI</td>
</tr>
<tr class="even">
<td class="confluenceTd">(7386) Avoid Tables without a clustered
Index</td>
<td class="confluenceTd">(1101090) Avoid Tables without a clustered
Index (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(7810) Avoid Artifacts with a Complex SELECT
Clause</td>
<td class="confluenceTd">(1101098) Avoid Artifacts with a Complex SELECT
Clause (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101026) Always define column names when
inserting values</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101034) Avoid using DISTINCT in SQL SELECT
statements</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101038) Avoid OR conditions testing equality
on the same identifier in SQL WHERE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101066) Avoid improperly written triangular
joins with XXL tables</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101082) Avoid looping chain of synonyms</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101080) Avoid orphaned synonyms</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101058) Avoid mixing ANSI and non-ANSI JOIN
syntax in the same query</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101064) Avoid cascading Triggers (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101024) Avoid using dynamic SQL in SQL
Artifacts</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101040) Avoid empty catch blocks</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101008) Avoid non-SARGable queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101060) Avoid using LIKE conditions starting
with a wildcard character</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101070) Avoid explicit comparison with
NULL</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101072) Avoid not aliased Tables</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101076) Avoid unqualified column
references</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101074) Avoid Tables aliases ending with a
numeric suffix</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101054) Never use WHEN OTHER THEN NULL</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101044) View naming convention - character
set control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101042) Table naming convention - character
set control (SQL)</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101036) Use ANSI standard operators in SQL
WHERE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature for the client code</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101028) Use MINUS or EXCEPT operator instead
of NOT EXISTS and NOT IN subqueries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101020) Avoid using quoted identifiers</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101012) Avoid specifying column numbers
instead of column names in ORDER BY clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101010) Avoid NATURAL JOIN queries</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101102) Avoid using LIKE conditions without
wildcards</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101104) Avoid XXL tables without primary key
/ unique key constraint / unique index</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101106) Avoid XXL tables without primary key
/ unique key constraint / unique index</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101108) Avoid Cobol SQL Cursors without FOR
READ ONLY or FOR FETCH ONLY or FOR UPDATE clauses</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, only for SQL in Cobol</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">(1101112) Avoid LOCK TABLE statements in SQL
code for COBOL Programs</td>
<td class="confluenceTd"><img src="/images/icons/emoticons/add.svg"
class="emoticon emoticon-plus" data-emoticon-name="plus"
alt="(plus)" /></td>
<td class="confluenceTd">New feature, only for SQL in Cobol</td>
</tr>
</tbody>
</table>
