---
title: "SQL file categorization"
linkTitle: "SQL file categorization"
type: "docs"
---

## Description

Some SQL files are considered as DDL, even if they have some DML
statements externals to DDL statements. Others are even considered as
data export like or script like. We explain here how SQL files are
categorized.

## How SQL files are categorized

Before start analyzing an SQL file we put it in a specific category :

<table>
<colgroup>
<col/>
<col/>
<col/>
<col/>
<col/>
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">File scope</th>
<th class="confluenceTh">File category</th>
<th colspan="2" class="confluenceTh">Why</th>
<th class="confluenceTh">Logged message</th>
</tr>
&#10;<tr class="odd">
<td rowspan="6" class="confluenceTd">File content will contribute to
analysis</td>
<td rowspan="4" class="confluenceTd">DDL<br />
<br />
</td>
<td colspan="2" class="confluenceTd">Analysis files are based on a
uaxDirectory file, they could be only DDL </td>
<td rowspan="4" class="confluenceTd">DEBUG: File &lt;file_name&gt; is
considered as DDL<br />
<br />
</td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd">For the SAP Sqlscript
variant, files could be only DDL</td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><p>Mandatory files *.pf, *.pf38,
*.lf, *.lf38, *.sqlt, *.ddl, *.plsql, *.pgsql, *.mysql are considered
DDL.</p>
<p>Optional files *.bdy, *.db2, *.fnc, *.pck, *.pkb, *.pkg, *.pks,
*.plb, *.pls, *.spc, *.tab, *.tpb, *.tps, *.trg, *.trigddl, *.tsql,
*.udf, *.viewddl, *.viw, *.vw are considered DDL.</p></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><p>Otherwise, when a first DDL
statement is detected, the file is considered to be fully DDL if no DML
has been detected.</p>
<p>From the list of DDL, the ALTER TABLE ... MODIFY is excluded because
it could be used in a data export context.</p>
<p>When we detected a single DML before DDL, we consider the file
DDL.</p>
<p>When detected a table / view / schema / database, even if we detected
DML statements, we consider the file DDL.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">DML</td>
<td colspan="2" class="confluenceTd"><p>Mandatory files *.dml are
considered DML.</p>
<p>No DDL statements is detected in the first 100 lines, the number of
INSERTS is &lt;=80% with only static data.</p></td>
<td class="confluenceTd">DEBUG : File &lt;file_name&gt; is considered as
DML</td>
</tr>
<tr class="even">
<td class="confluenceTd">Mixed DDL DML</td>
<td colspan="2" class="confluenceTd">When we detect DML statements
before DDL statements, in the first/last 100 lines and also in the last
5000 lines for files with more than 5000 lines, when no DDL has been
detected in the first/last 100 lines.</td>
<td class="confluenceTd">DEBUG: File &lt;file_name&gt; is considered
as a mix of DDL and DML</td>
</tr>
<tr class="odd">
<td rowspan="6" class="confluenceTd">File content will be fully
ignored<br />
<br />
</td>
<td class="confluenceTd">Data export/loading like file</td>
<td colspan="2" class="confluenceTd"><p>The INSERTs statements are
considered as more data export/loading like than DML.</p>
<p>When in the first 100 lines we have only DML statements and when more
than 80% are INSERTS, the file is considered as data export/loading file
like.</p>
<p><br />
</p>
<p>If DDL is detected in the last 100/5000 lines, file is considered as
DDL and data export/loading statements are ignored.</p></td>
<td class="confluenceTd">INFO: The file &lt;file_name&gt; is considered
as data export/loading and it will not be analyzed because in the first 100
lines, from the total number of DML statements,
&lt;percent_of_INSERTs%&gt; are INSERT.</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd">Script like file</td>
<td colspan="2" class="confluenceTd"><p>One of the first 100 lines
starts with</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">#!</td>
<td class="confluenceTd">The file is more a Shell script than SQL
file, <a href="https://linux.goffinet.org/administration/scripts-shell/"
rel="nofollow">https://linux.goffinet.org/administration/scripts-shell/</a></td>
<td rowspan="2" class="confluenceTd">INFO: The file &lt;file_name&gt;
will be skipped because it is more a script like file than SQL
file.<br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">?tacl</td>
<td class="confluenceTd">The file is more Tacl script than SQL file, <a
href="http://nonstoptools.com/manuals/Tacl-Guide.pdf"
rel="nofollow">http://nonstoptools.com/manuals/Tacl-Guide.pdf</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Empty file</td>
<td colspan="2" class="confluenceTd">One if the first 100 lines are
empty</td>
<td class="confluenceTd">INFO: The file &lt;file_name&gt; will be
skipped because is empty.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Unknown</td>
<td colspan="2" class="confluenceTd"><p>Having DDL statements not in the
list of supported DDLs :</p>
<p>Having DML statements not in the list of supported DMLs, like
INSERT/UPDATE/UPSERT/MERGE/TRUNCATE/DELETE/SELECT. When a call of a
Function / Procedure is detected, they are counted in the list of DML
statements, like CALL/EXEC/EXECUTE/PERFORM.</p></td>
<td class="confluenceTd">INFO: The file &lt;file_name&gt; it will not be
analyzed, because it is not DDL neither DML or none of the statements
are supported.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">XXL and XXS rules</td>
<td class="confluenceTd">.sqltablesize files</td>
<td colspan="2" class="confluenceTd">See <a
href="../xxl/">SQL Analyzer -
working with XXL or XXS tables</a></td>
<td class="confluenceTd">INFO: Using table size file &lt;file_name&gt;
for XXL rules</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd">Data sensitivity indicators</td>
<td rowspan="2" class="confluenceTd">.datasensitive files</td>
<td colspan="2" class="confluenceTd"><p>See <a
href="../../../com.castsoftware.datacolumnaccess/">Data
Column Access</a></p></td>
<td class="confluenceTd"><p>INFO: Using General Data Protection
Regulation safety indicator configuration file &lt;file_name&gt; for
table columns</p>
<p>INFO: Using Payment Card Industry Data Security Standards safety
indicator configuration file &lt;file_name&gt; for table
columns</p></td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd">See <a
href="../../../com.castsoftware.datacolumnaccess/">Data
Column Access</a></td>
<td class="confluenceTd">INFO : Using Custom Data sensitivity indicator
configuration file &lt;file_name&gt; for table columns</td>
</tr>
</tbody>
</table>
