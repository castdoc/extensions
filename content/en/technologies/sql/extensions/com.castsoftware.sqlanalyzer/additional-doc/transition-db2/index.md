---
title: "Transitioning from the CAST AIP Db2 Analyzer to the SQL Analyzer extension"
linkTitle: "Transitioning from Db2"
type: "docs"
---

## Introduction

If you have been actively analyzing Db2 (z/OS or LUW) with the Db2
Analyzer (provided out-of-the-box in CAST AIP) you can transition to
using the SQL Analyzer extension to analyze your Db2 source code. The
process of transitioning is described in this page.

Note that it is only possible to analyze Db2 when using the SQL Analyzer
extension ≥ 2.x.

## Step 1: Take a snapshot

Take a snapshot of the existing Application prior to making any further
changes.

## Step 2: Backup CAST AIP schemas and Delivery/Deployment folders

-   To backup the CAST AIP schemas use the CSSBackup
    tool (located in the CSSAdmin folder in the CAST AIP installation
    folder) use the following scripts adapted to your environment:

Backup CAST AIP schemas

``` powershell
CSSBackup.exe -schema db2_udb_local -password CastAIP -file c:\temp\DB2\db2_udb_local.cssdmp -log c:\temp\DB2\db2_udb_local.log
CSSBackup.exe -schema db2_udb_central -password CastAIP -file c:\temp\DB2\db2_udb_central.cssdmp -log c:\temp\DB2\db2_udb_central.log
CSSBackup.exe -schema db2_udb_measure -password CastAIP -file c:\temp\DB2\db2_udb_measure.cssdmp -log c:\temp\DB2\\db2_udb_measure.log
CSSBackup.exe -schema db2_udb_mngt -password CastAIP -file c:\temp\DB2\db2_udb_mngt.cssdmp -log c:\temp\DB2\\db2_udb_mngt.log
```

-   Then ZIP the Delivery and Deployment folders for easy
    restoration:

![](235483718.png)

## Step 3: Check results and grades

Before proceeding, check your results and grades using the CAST
dashboards. Example below show a Db2 LUW analysis:

Portfolio



![](235483736.png)

Quality and size evolution



![](235483735.png)

Violated rules by technology



![](235483738.png)

## Step 4: Download and install the latest version of the SQL Analyzer extension

This will be automatic when using CAST Console.

Note that you need to install the extension into the CAST AIP schemas
that you were using previously to host your Application containing the
Db2 analysis results.

## Step 5: Retrieve existing Db2 LUW connection details

This step is only relevant for those analyzing Db2 LUW online.

Use the following query, run against your CAST AIP Management Service
schema, to retrieve the connection details to the live Db2 LUW databases
that are used during the analysis with the Db2 Analyzer
(provided out-of-the-box in CAST AIP). This information can be reused
when extracting the DDL for use with the SQL Analyzer extension:

For Oracle and CSS (postgreSQL) Management databases::

``` sql
select partdetails.Object_Name PackageName, UserName, Host, Port, DbName, projdetails.Object_Name SchemaName 
from 
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DatabaseServerParticipatingDB2') part,
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DB2UDBSourceCodeRepository') proj,
CMS_INF_PARTP_DBUDB partdetails, 
CMS_UDB_Project projdetails
where part.Field_Value like proj.Field_Value || '%'
and partdetails.Object_ID = part.Object_ID
and proj.Object_ID = projdetails.Resource_ID
order by Host, DbName, SchemaName
```

For Microsoft SQL Management databases:

``` sql
select partdetails.Object_Name PackageName, UserName, Host, Port, DbName, projdetails.Object_Name SchemaName 
from 
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DatabaseServerParticipatingDB2') part,
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DB2UDBSourceCodeRepository') proj,
CMS_INF_PARTP_DBUDB partdetails, 
CMS_UDB_Project projdetails
where part.Field_Value like proj.Field_Value + '%'
and partdetails.Object_ID = part.Object_ID
and proj.Object_ID = projdetails.Resource_ID
order by Host, DbName, SchemaName
```

The output of the above queries will result in similar output

| CAST DMT Package Name | Db2 login | Host name | Host port | Database Name | Schema Name |
|-----------------------|-----------|-----------|-----------|---------------|-------------|
| UDB_Package           | db2admin  | UDBSERVER | 50000     | TEST          | CASTPUBS    |

## Step 6: Generate SQL source

This step explains how to generate the DDL from the Db2 database that
will be fed into the CAST Delivery Manager Tool for analysis with
the SQL Analyzer extension.  

-   See also [SQL Analyzer - generate DDL for a Db2
    database](../ddl-extraction/db2/) for more
    information.
-   Note that the db2look tool is only available with an
    installation of a Db2 LUW instance. If you are using a Db2
    z/OS instance, CAST recommends the use of the RC/Migrator tool
    which is part of the CA Database Management Solution for Db2 for
    Z/OS, Version 19. There are no instructions provided for this.

### db2look command line

The tool used to achieve this is the db2look tool provided with the
Db2 LUW server (see the full documentation for the tool
here: https://www.ibm.com/support/knowledgecenter/en/SSEPGG_10.5.0/com.ibm.db2.luw.admin.cmd.doc/doc/r0002051.html). This
example will generate an .SQL file containing the DDL for the CASTPUBS
schema on the TEST database accessed with the db2admin/db2admin
credentials:

Generate db2look commands:

``` sql
db2look -d TEST -z CASTPUBS -i db2admin -w db2admin -e -o E:\tmp\SQL_DB2\TEST_CASTPUBS.sql
```

You can view example DDL output generated by db2look in the attached
files:

-   CASTPUBS
-   [DB2_DIAGS](235483732.sql)

### Generate db2look commands using a query

This section is only relevant for those analyzing Db2 LUW online.

If you need to generate DDL for multiple schemas use the following
query (run against your CAST AIP Management Service schema) to
automatically generate commands that can be used with the db2look
tool:

For Oracle and CSS (postgreSQL) Management databases:

``` sql
select 'db2look -d ' || DbName || ' -z ' || projdetails.Object_Name || ' -i ' || UserName || ' -w db2admin -e  -o ' || DbName ||  '_' || projdetails.Object_Name || '_DDL.sql' batch
from
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DatabaseServerParticipatingDB2') part,
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DB2UDBSourceCodeRepository') proj,
CMS_INF_PARTP_DBUDB partdetails, 
CMS_UDB_Project projdetails
where part.Field_Value like proj.Field_Value || '%'
and partdetails.Object_ID = part.Object_ID
and proj.Object_ID = projdetails.Resource_ID
order by batch
```

For Microsoft SQL Management databases:

``` sql
select 'db2look -d ' + DbName + ' -z '+ projdetails.Object_Name + ' -i ' + UserName + ' -w db2admin -e  -o ' + DbName + '_' + projdetails.Object_Name + '_DDL.sql' batch
from
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DatabaseServerParticipatingDB2') part,
(select * from CMS_DynamicFields where Entity_GUID = 'dbtwoserver.DB2UDBSourceCodeRepository') proj,
CMS_INF_PARTP_DBUDB partdetails, 
CMS_UDB_Project projdetails
where part.Field_Value like proj.Field_Value + '%'
and partdetails.Object_ID = part.Object_ID
and proj.Object_ID = projdetails.Resource_ID
order by batch
```

The result of the above querieswill be as follows - one db2look
command per schema on the target Db2 server. Each command will output
the specified schema as DDL into one .SQL file:

Resulting db2look commands

``` text
db2look -d TEST -z CASTPUBS -i db2admin -w db2admin -e -o E:\tmp\SQL_DB2\CASTPUBS.sql
db2look -d TEST -z DB2_DIAGS -i db2admin -w db2admin -e -o  E:\tmp\SQL_DB2\DB2_DIAGS.sql
```

## Step 7: Add a new version

-   In the CAST Management Studio, add a new version for the existing
    Application in which the online Db2 LUW analysis or the z/OS
    analysis is configured*.* Examples below show "Db2 LUW online"
    *(click to enlarge)*:

![](235483764.png)

-   In the CAST Delivery Manager Tool, remove the existing Db2 LUW
    online or z/OS package as shown below *(click to enlarge)*:

![](235483743.png)

-   Add a new package to deliver the Db2 DDL that you have already
    generated, using the Files on your system option *(click to
    enlarge)*:

![](235483746.png)![](235483745.png)

-   Configure the package and define the path to the folder that
    contains the .SQL files generated by the db2look tool *(click to
    enlarge)*:

![](235483749.png)

-   As shown below, the new package called ddl udb is listed along
    side the existing "cobol" package *(click to enlarge)*:

![](235483748.png)

-   Run the packaging action *(click to enlarge)*:

![](235483747.png)

-   Deliver and then set as the current version in the CAST Management
    Studio *(click to enlarge)*:

![](235483763.png)

-   Once the "set as current version" action is complete, the delivered
    .SQL files can be seen in the Deployment folder:

![](235483754.png)

-   Add a new Universal Analyzer Analysis Unit pointing to the .SQL
    files in the Deployment folder, ensuring the SQL Analyzer
    language is ticked *(click to enlarge)*:

![](235483753.png)![](235483752.png)

-   Modify the dependencies if you necessary. In this example we have a
    Mainframe Cobol Analysis Unit and therefore we will replace the
    existing Mainframe / IBM Db2 LUW auto created dependency with 
    custom cobol / ddl udb dependency *(click to enlarge)*:

![](235483751.png)![](235483755.png)

## Step 8: Recalibrate your Application

Take two new snapshots (ensure that you run an analysis as well)
and then check the results in the CAST dashboards:

Portfolio



![](235483756.png)

Quality and size evolution



![](235483758.png)

  

Note that new Quality Rules will be calculated for the SQL Analyzer
extension in comparison to the online Db2 analyzer:

-   Avoid unreferenced Functions
-   Avoid redundant indexes
-   Avoid Tables without Primary Key
