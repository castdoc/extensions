/*
o 	Database name, if one
o	Schema name
o	Table name
o	Column name
o	Column type

*/

/* 
 Replace <LOCAL_SCHEMA> with your AIP local schema
 E.g. : 
	set search_path to <LOCAL_SCHEMA>;
		will change to 
	set search_path to AIP836_LOCAL;
*/
set search_path to <LOCAL_SCHEMA>;

select distinct 
	coalesce(db.object_name, '-') as "Database Name", 
	own.object_name as "Schema Name", 
	tbl.object_name as "Table Name", 
	col.object_name as "Column Name", 
	col.object_type_str "Column Type"
from 
csv_object_columns col 
	join ctt_object_parents tblp on tblp.object_id=col.parent_id
	join cdt_objects tbl on tbl.object_id=col.parent_id
	join cdt_objects own on tblp.parent_id=own.object_id
	left join ctt_object_parents ownp on ownp.object_id=own.object_id
	left join cdt_objects db on ownp.parent_id=db.object_id
							and lower(db.object_type_str) like '%database'	
where lower(tbl.object_type_str) like '%table'	
	and lower(own.object_type_str) like '%schema'
order by 1,2,3,4;