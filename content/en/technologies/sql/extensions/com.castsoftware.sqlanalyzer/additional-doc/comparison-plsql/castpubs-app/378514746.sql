﻿set search_path to castpubs_ora_local;

create or replace view TransformOracleObjects
as
select distinct lower(substring(ofn.fullname, position('.' in ofn.fullname) + 1)) ObjectName, 
	case 
		when t.TypDsc = 'Oracle check table constraint' then 'Check Constraint'
		when t.TypDsc = 'Oracle default table constraint' then 'Default Constraint'
		when t.TypDsc = 'Oracle foreign key table constraint' then 'Foreign Key'
		when t.TypDsc = 'Oracle primary key table constraint' then 'Indexes Primary Keys Unique Constraints'
		when t.TypDsc = 'Oracle index' then 'Indexes Primary Keys Unique Constraints'
		when t.TypDsc = 'Oracle bitmap index' then 'Indexes Primary Keys Unique Constraints'
		when t.TypDsc = 'Oracle function based index' then 'Indexes Primary Keys Unique Constraints'
		when t.TypDsc = 'Oracle table' then 'Table'
		when t.TypDsc = 'Oracle instance' then 'Instance'
		when t.TypDsc like '%collection%' then 'Collection'
		when t.TypDsc like '%object%' then 'Type'
		when t.TypDsc = 'Oracle function' then 'Function'
		when t.TypDsc = 'Oracle private stored function' then 'Function'
		when t.TypDsc like '%procedure%' then 'Procedure'
		when t.TypDsc like '%trigger%' then 'Trigger'
		when t.TypDsc like '%package header%' or t.TypDsc like '%package body%' then 'Package'
		when t.TypDsc like '%column%' then 'Column'
		when t.TypDsc like '%synonym%' then 'Synonym'
		when t.TypDsc like '%schema%' then 'Schema'
		when t.TypDsc like '%unique table constraint%' then 'Indexes Primary Keys Unique Constraints'
		when t.TypDsc like '%view%' then 'View'
		when t.TypDsc like '%sequence%' then 'Sequence'
		when t.TypDsc like '%Database Link%' then 'Database Link'
		when t.TypDsc like '%package cursor%' then 'Package Cursor'
		when t.TypDsc like '%cursor datatype%' then 'Cursor Datatype'
		when t.TypDsc like '%Package Variable%' then 'Package Variable'
	else t.TypDsc
	end ObjectType
from ObjFulNam ofn join
Keys k 
on k.IdKey = ofn.IdObj
join Typ t
on k.ObjTyp = t.IdTyp
where t.IdTyp in (select IdTyp from TypCat where IdCatParent = 140750)
and (	t.TypDsc not like '%Project%' 
	and t.TypDsc not like '%Subset%'
	)
order by ObjectType, ObjectName;

create or replace view Count_TransformOracleObjects
as
select ObjectType, count(1) NumberOfObjects
from TransformOracleObjects
group by ObjectType
order by ObjectType;



