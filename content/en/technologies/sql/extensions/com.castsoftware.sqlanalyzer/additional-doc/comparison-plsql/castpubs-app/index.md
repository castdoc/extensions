---
title: "Use case - CASTPUBS app"
linkTitle: "Use case - CASTPUBS app"
type: "docs"
---

## Introduction

The comparison has been done between PL-SQL Analyzer 8.3.8 and SQL
Analyzer 3.4.0-beta3 (AIP 8.3.8 + datacolumnaccess 1.0.0-beta2), on the attached [source code](378514752.7z).

CASTPUBS cover almost all kind of Oracle objects, links and quality
rules, that's why it has been selected as example.

You can find here bellow differences.

/\*\*/ 1 TQI 2 Number of data functions and transaction entry points per
APPLICATION 3 Number of objects by type 4 Compare equivalent quality
rules

## TQI

[\<query\>](378514740.sql)
| analyzer | tqi              |
|----------|------------------|
| SQL      | 2.59073431994084 |
| Oracle   | 2.58794953216398 |

##  Number of data functions and transaction entry points per APPLICATION

[\<query\>](378514750.sql)

| analyzer | cnt_datafunctions | cntdfp_functionpoints | cntdf_calibratedfunctionpoints | cnt_transactions | cnttfp_functionpoints | cntt_calibratedfunctionpoints |
|----------|-------------------|-----------------------|--------------------------------|------------------|-----------------------|-------------------------------|
| SQL      | 16                | 100                   | 100                            | 0                | 0                     | 0                             |
| Oracle   | 12                | 72                    | 72                             | 0                | 0                     | 0                             |

## Number of objects by type

[\<query\>](378514744.sql)

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd">objecttype</td>
<td class="confluenceTd">numberofobjects</td>
<td class="confluenceTd">Compared with Oracle</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column</td>
<td class="confluenceTd">100</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Foreign Key</td>
<td class="confluenceTd">16</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Function</td>
<td class="confluenceTd">24</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Indexes Primary Keys Unique Constraints</td>
<td class="confluenceTd">21</td>
<td class="confluenceTd"><p>-11</p>
<p>We are betters on SQL Analyzer, on PL/SQL Analyzer a part of indexes
are duplicated.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Package</td>
<td class="confluenceTd">14</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">17</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Schema</td>
<td class="confluenceTd">2</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">2</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Table</td>
<td class="confluenceTd">16</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">4</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">View</td>
<td class="confluenceTd">5</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Check Constraint</td>
<td class="confluenceTd">56</td>
<td class="confluenceTd">Object type not implemented on SQL</td>
</tr>
<tr class="even">
<td class="confluenceTd">Instance</td>
<td class="confluenceTd">1</td>
<td class="confluenceTd">Object type not implemented on SQL</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Package Cursor</td>
<td class="confluenceTd">2</td>
<td class="confluenceTd">Object type not implemented on SQL</td>
</tr>
<tr class="even">
<td class="confluenceTd">Sequence</td>
<td class="confluenceTd">1</td>
<td class="confluenceTd">Object type not implemented on SQL</td>
</tr>
</tbody>
</table>

Compare links by type

[\<query\>](378514739.sql){linked-resource-id="378514739"
linked-resource-version="2" linked-resource-type="attachment"
linked-resource-default-alias="CompareSQLORALinks.sql"
linked-resource-content-type="application/octet-stream"
linked-resource-container-id="378514735"
linked-resource-container-version="4"}

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">kindoflink</th>
<th class="confluenceTh">callertype</th>
<th class="confluenceTh">calleetype</th>
<th class="confluenceTh">Compared with Oracle</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd"><p>3 </p>
<p>We discover more links than PL/SQL Analyzer</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">Package</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd"><p>Detected only by PL/SQL Analyzer</p>
<p>This is the case of package headers</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd"><p>-16</p>
<p>PL/SQL discover 16 links more than SQL Analyzer but a part of them
are false links, they don't really exists</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Package</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd"><p>Detected only by PL/SQL Analyze</p>
<p>Same comment as for columns, this is the case of package
headers</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
</tbody>
</table>

  

## Compare equivalent quality rules

[\<query\>](378514737.sql){linked-resource-id="378514737"
linked-resource-version="2" linked-resource-type="attachment"
linked-resource-default-alias="CompareSQLORAQRs.sql"
linked-resource-content-type="application/octet-stream"
linked-resource-container-id="378514735"
linked-resource-container-version="4"}

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">qualityrulename</th>
<th class="confluenceTh">objecttype</th>
<th class="confluenceTh">Compared with Oracle</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Always define column names when inserting
values</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Always define column names when inserting
values</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Average Reuse by Call</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Average Reuse by Call</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Average Reuse by Call</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Average Reuse by Call</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Average Size Artifacts</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Average Size Artifacts</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Average Size Artifacts</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid "SELECT *" queries</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid "SELECT *" queries</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid Artifacts with High Essential
Complexity</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid Artifacts with High Essential
Complexity</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid Artifacts with High Essential
Complexity</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid Artifacts with SQL statement including
subqueries</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">-2</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid Artifacts with SQL statement including
subqueries</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid Artifacts with a Complex SELECT
Clause</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">The rationale describe a client server context
there is no value to apply-it on SQL code</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid Artifacts with a Complex SELECT
Clause</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">The rationale describe a client server context
there is no value to apply-it on SQL code</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid Artifacts with high Commented-out Code
Lines/Code Lines ratio</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid Artifacts with lines longer than X
characters</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid Artifacts with lines longer than X
characters</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid Tables without Primary Key</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid Too Many Copy Pasted Artifacts</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">-2</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid cascading Triggers</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid empty catch blocks</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid exists and not exists independent
clauses</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid having multiple Artifacts inserting data
on the same SQL Table</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid having multiple artifacts deleting data
on the same SQL table</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid large Artifacts - too many Lines of
Code</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid large Artifacts - too many Lines of
Code</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid large Artifacts - too many Lines of
Code</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid long Table or View names</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid non-SARGable queries</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid non-indexed SQL queries</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">-1</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid non-indexed SQL queries</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">-1</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid non-indexed SQL queries</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">-1</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid orphaned synonyms</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid queries using old style join convention
instead of ANSI-Standard joins</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid queries using old style join convention
instead of ANSI-Standard joins</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid queries using old style join convention
instead of ANSI-Standard joins</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid redundant indexes</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid triggers functions and procedures with a
very low comment/code ratio</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid triggers functions and procedures with a
very low comment/code ratio</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid triggers functions and procedures with a
very low comment/code ratio</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid undocumented Triggers Functions and
Procedures</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid undocumented Triggers Functions and
Procedures</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid undocumented Triggers Functions and
Procedures</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid unreferenced Functions</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid unreferenced Functions</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid unreferenced Tables</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid unreferenced views</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid using "nullable" Columns except in the
last position in a Table</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">Exists only on PL/SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Avoid using GOTO statement</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Avoid using SQL queries inside a loop</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd"><p>Detected only by PL/SQL Analyzer</p>
<p>False violation, the loop is a cursor, that's why the violation is
not raised on SQL Analyzer since 3.4.0-beta3. Since 3.4.0-beta3 the rule
is calculated by extension and not by MA.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Column references should be qualified</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Column references should be qualified</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Cyclomatic Complexity Distribution</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Cyclomatic Complexity Distribution</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Cyclomatic Complexity Distribution</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Cyclomatic Complexity Distribution</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Detected only by SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">DISTINCT should not be used in SQL SELECT
statements</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Low Complexity Artifacts</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">7</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Low Complexity Artifacts</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Low Complexity Artifacts</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Low SQL Complexity Artifacts</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">-2</td>
</tr>
<tr class="even">
<td class="confluenceTd">Low SQL Complexity Artifacts</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Low SQL Complexity Artifacts</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">1</td>
</tr>
<tr class="even">
<td class="confluenceTd">Moderate Complexity Artifacts</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">1</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Never use WHEN OTHER THEN NULL</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of Code Lines</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Number of Code Lines</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of Code Lines</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Number of Code Lines</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of Datablocks</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Number of Datablocks</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of Forms</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Number of Forms</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of Forms</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Number of Forms</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of Functions</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Number of Tables</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Number of Views</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Prefer PRIVATE to PUBLIC synonym</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">SQL Complexity Distribution</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">2</td>
</tr>
<tr class="odd">
<td class="confluenceTd">SQL Complexity Distribution</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">1</td>
</tr>
<tr class="even">
<td class="confluenceTd">SQL Complexity Distribution</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">SQL Complexity Distribution</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">Small Size Artifacts</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">-11</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Small Size Artifacts</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">-11</td>
</tr>
<tr class="even">
<td class="confluenceTd">Small Size Artifacts</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Small Size Artifacts</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Tables aliases should not end with a numeric
suffix</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Tables aliases should not end with a numeric
suffix</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Tables aliases should not end with a numeric
suffix</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Tables should be aliased</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Tables should be aliased</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Triggers should not directly modify tables a
procedure or function should be used instead</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="even">
<td class="confluenceTd">VARCHAR2 and NVARCHAR2 should be used</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">12</td>
</tr>
<tr class="odd">
<td class="confluenceTd">VARCHAR2 and NVARCHAR2 should be used</td>
<td class="confluenceTd">Package</td>
<td class="confluenceTd">Detected only by PL/SQL Analyzer but the same
violation is duplicated : reported one time on the package function /
procedure and also directly on the package</td>
</tr>
<tr class="even">
<td class="confluenceTd">VARCHAR2 and NVARCHAR2 should be used</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">=</td>
</tr>
<tr class="odd">
<td class="confluenceTd">VARCHAR2 and NVARCHAR2 should be used</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">=</td>
</tr>
</tbody>
</table>
