---
title: "Use case - Packages/Types app"
linkTitle: "Use case - Packages/Types app"
type: "docs"
---

## Introduction

The comparison has been done between PL-SQL Analyzer 8.3.16 and SQL
Analyzer 3.4.0-beta3 (AIP 8.3.16 + datacolumnaccess 1.0.0-beta2), on a
representative sample focused on Oracle PL-SQL Packages and Types.
Source code is confidential, extracted with CAST Database Extractor,
without parameters (full Instance), Oracle version is 11.2.0.2.0.

You can find the differences below.

## TQI

| Analyzer | TQI              |
|----------|------------------|
| SQL      | 2.44900109665184 |
| Oracle   | 2.69593522967072 |

## Number of data functions and transaction entry points per APPLICATION

| Analyzer | cnt_datafunctions | cntdfp_functionpoints | cntdf_calibratedfunctionpoints | cnt_transactions | cnttfp_functionpoints | cntt_calibratedfunctionpoints |
|----------|-------------------|-----------------------|--------------------------------|------------------|-----------------------|-------------------------------|
| SQL      | 1796              | 10007                 | 10007                          | 0                | 0                     | 0                             |
| Oracle   | 1530              | 8533                  | 8533                           | 0                | 0                     | 0                             |

## Delta compared with Oracle

### By object type

<table class="wrapped relative-table confluenceTable"
style="width: 100.0%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 26%" />
<col style="width: 58%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">ObjectType</th>
<th class="confluenceTh">Delta</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Column</td>
<td class="confluenceTd">- 0.003 % (Oracle : 67249 SQL : 67247)</td>
<td class="confluenceTd"><p>Even if we have less, we have the goods
ones. </p>
<p>Some Oracle columns, on objects tables are fakes.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Foreign Key</td>
<td class="confluenceTd">- 2.617 % (Oracle : 917 SQL : 893)</td>
<td class="confluenceTd"><p>We miss details when src files are
malformed, e.g.:</p>
<p>CONSTRAINT FK_NAME_01 (
%CAST_AST_WithResolutionSymbolReference.resolutionSymbol% ) REFERENCES
TABLE_02 ( %CAST_AST_WithResolutionSymbolReference.resolutionSymbol% )
ON DELETE NO ACTION VALIDATE,</p>
<p><br />
CONSTRAINT FK_NAME_02 (
%CAST_AST_WithResolutionSymbolReference.resolutionSymbol% ) REFERENCES
TABLE_03 ( %CAST_AST_WithResolutionSymbolReference.resolutionSymbol% )
ON DELETE NO ACTION VALIDATE</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Function</td>
<td class="confluenceTd">+ 1.517 % (Oracle : 2791 SQL : 2834)</td>
<td class="confluenceTd"><p>We detect more functions compared with
Oracle</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Indexes Primary Keys Unique Constraints</td>
<td class="confluenceTd">- 71.986 % (Oracle : 3741 SQL : 1048)</td>
<td class="confluenceTd"><p>We miss indexes on SQL compared with Oracle,
when src files are malformed.</p>
<p>E.g. : </p>
<p>CREATE  INDEX %CAST_Symbol_WithParent.parent%.IDX_01 ON
%CAST_ANSISQL_Index.indexedObject% ( COL1, COL2)</p>
<p>or</p>
<p>CONSTRAINT PK_NAME PRIMARY KEY (
%CAST_AST_WithResolutionSymbolReference.resolutionSymbol% )
VALIDATE</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Method</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 745)</td>
<td class="confluenceTd">Exists only on SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">Package</td>
<td class="confluenceTd"> 100 % (Oracle : 698 SQL : 698)</td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">- 9.297 % (Oracle : 3668 SQL : 3327)</td>
<td class="confluenceTd"><p>We miss some procedures, this is a bug that
should be fixed</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Schema</td>
<td class="confluenceTd">- 33.333 % (Oracle : 21 SQL : 14)</td>
<td class="confluenceTd">We miss empty schema (with no objects)
referenced only in uaxDirectory file, with no impact on function points
or TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd"> 100 % (Oracle : 3764 SQL : 3764)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Table</td>
<td class="confluenceTd"> 100 % (Oracle : 1982 SQL : 1982)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd"> 100 % (Oracle : 7 SQL : 7)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Type</td>
<td class="confluenceTd">- 2.516 % (Oracle : 914 SQL : 891)</td>
<td class="confluenceTd"><p>We miss 23 Type on SQL compared with Oracle,
when src files are malformed, e.g. : src file contain only CREATE OR
REPLACE and nothing else. We also miss some TYPE creates as tables of
NUMBER, and should investigated why we miss them.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">View</td>
<td class="confluenceTd"> 100 % (Oracle : 4073 SQL : 4073)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Check Constraint</td>
<td class="confluenceTd">- 100 % (Oracle : 5138 SQL : 0)</td>
<td class="confluenceTd">The object is missing on SQL Analyzer, with no
impact on function points or TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Database Link</td>
<td class="confluenceTd">- 100 % (Oracle : 2 SQL : 0)</td>
<td class="confluenceTd"><p>The object is missing on SQL Analyzer,
gm</p>
<p>with no impact on function points or TQI</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Instance</td>
<td class="confluenceTd">- 100 % (Oracle : 1 SQL : 0)</td>
<td class="confluenceTd">The object is missing on SQL Analyzer, with no
impact on function points or TQI</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Sequence</td>
<td class="confluenceTd"><p>- 100 % (Oracle : 260 SQL : 0)</p></td>
<td class="confluenceTd">The object is missing on SQL Analyzer, with no
impact on function points or TQI</td>
</tr>
</tbody>
</table>

### By link type

<table class="wrapped relative-table confluenceTable"
style="width: 93.9541%;">
<colgroup>
<col style="width: 7%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 16%" />
<col style="width: 62%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">KindOfLink</th>
<th class="confluenceTh">CallerType</th>
<th class="confluenceTh">CalleeType</th>
<th class="confluenceTh">Delta</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd">+ 0.620 % (Oracle : 12973 SQL : 13054)</td>
<td class="confluenceTd"><p>We detect more links compared with
Oracle</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">Package</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd">- 100 % (Oracle : 169 SQL : 0)</td>
<td class="confluenceTd"><p>We miss columns on SQL for table accessed
directly in packages.</p>
<p>Should be fixed.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd">+ 5.005 % (Oracle : 17329 SQL : 18242)</td>
<td class="confluenceTd"><p>We detect more links compared with
Oracle</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Column</td>
<td class="confluenceTd">- 18.093 % (Oracle : 59995 SQL : 49140)</td>
<td class="confluenceTd">Bug related to synonym on views that should be
fixed</td>
</tr>
<tr class="odd">
<td class="confluenceTd">ACCESS</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Type</td>
<td class="confluenceTd">- 100 % (Oracle : 2 SQL : 0)</td>
<td class="confluenceTd">PL/SQL add 2 false links when selected columns
have the same name as an object type.</td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">+ 42.246 % (Oracle : 689 SQL : 1193)</td>
<td class="confluenceTd">We detect more links compared with Oracle</td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">- 37.678 % (Oracle : 913 SQL : 569)</td>
<td class="confluenceTd">Should be fixed.</td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Method</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 1)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Method</td>
<td class="confluenceTd">Method</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 14)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Method</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 7)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Method</td>
<td class="confluenceTd">Type</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 7)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Package</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">- 100 % (Oracle : 2 SQL : 0)</td>
<td class="confluenceTd"><p>We miss columns on SQL for procedures
accessed directly in a PL/SQL block.</p>
<p>Should be fixed.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">+ 0.644 % (Oracle : 617 SQL : 621)</td>
<td class="confluenceTd"><p>We detect more links compared with
Oracle</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">- 14.702 % (Oracle : 1510 SQL : 1288)</td>
<td class="confluenceTd"><p>Should be fixed.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">- 100 % (Oracle : 9 SQL : 0)</td>
<td class="confluenceTd"><p>The case of DDL Trrigers.</p>
<p>Should be fixed.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">- 75.000 % (Oracle : 4 SQL : 1)</td>
<td class="confluenceTd"><p>We miss Function / Procedures, and that's
why we also miss links.</p>
<p>Should be fixed.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">- 15.000 % (Oracle : 320 SQL : 272)</td>
<td class="confluenceTd"><p>We miss Function / Procedures, and that's
why we also miss links.</p>
<p>Should be fixed.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">CALL</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Method</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 7)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 55)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">+ 18.068 % (Oracle : 3410 SQL : 4162)</td>
<td class="confluenceTd">We detect more links compared with Oracle</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Function</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">+ 12.957 % (Oracle : 262 SQL : 301)</td>
<td class="confluenceTd">We detect more links compared with Oracle</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Method</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 1)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 198)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">+ 3.136 % (Oracle : 4201 SQL : 4337)</td>
<td class="confluenceTd"><p>We detect more links compared with
Oracle</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Procedure</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">- 23.629 % (Oracle : 237 SQL : 181)</td>
<td class="confluenceTd"><p>We miss homonymous package which has as
effect, we miss function / procedures from missed packages and that's
why we miss links.</p>
<p>Should be fixed.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Trigger</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd"> 100 % (Oracle : 1 SQL : 1)</td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">Type</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 23)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">Type</td>
<td class="confluenceTd">Type</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 48)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Synonym</td>
<td class="confluenceTd">+ 100 % (Oracle : 0 SQL : 6)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">Table</td>
<td class="confluenceTd">+ 0.908 % (Oracle : 7638 SQL : 7708)</td>
<td class="confluenceTd">We detect more links compared with Oracle</td>
</tr>
<tr class="odd">
<td class="confluenceTd">USE</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">View</td>
<td class="confluenceTd">+ 9.064 % (Oracle : 3371 SQL : 3707)</td>
<td class="confluenceTd">We detect more links compared with Oracle</td>
</tr>
</tbody>
</table>

### By equivalent quality rule

Missed functions/procedures/tables have an impact on quality rules.

| Quality Rule Name                                                                                          | Object Type          | Number of violations detected by SQL | Number of violations detected by Oracle |
|------------------------------------------------------------------------------------------------------------|----------------------|--------------------------------------|-----------------------------------------|
| Always define column names when inserting values                                                           | Function             | 24                                   | 0                                       |
| Always define column names when inserting values                                                           | Procedure            | 73                                   | 0                                       |
| Average Coupling Artifacts                                                                                 | Function             | 26                                   | 5                                       |
| Average Coupling Artifacts                                                                                 | Procedure            | 21                                   | 10                                      |
| Average Coupling Artifacts                                                                                 | View                 | 26                                   | 28                                      |
| Average Reuse by Call                                                                                      | Function             | 2764                                 | 1449                                    |
| Average Reuse by Call                                                                                      | Method               | 745                                  | 0                                       |
| Average Reuse by Call                                                                                      | Procedure            | 3308                                 | 1124                                    |
| Average Reuse by Call                                                                                      | Trigger              | 7                                    | 7                                       |
| Average Reuse by Call                                                                                      | View                 | 3881                                 | 3888                                    |
| Average Size Artifacts                                                                                     | Function             | 27                                   | 400                                     |
| Average Size Artifacts                                                                                     | Procedure            | 11                                   | 326                                     |
| Average Size Artifacts                                                                                     | View                 | 2                                    | 530                                     |
| Avoid SELECT \*" queries"                                                                                  | Function             | 55                                   | 163                                     |
| Avoid SELECT \*" queries"                                                                                  | Procedure            | 40                                   | 73                                      |
| Avoid SELECT \*" queries"                                                                                  | View                 | 47                                   | 239                                     |
| Avoid Artifacts with High Cyclomatic Complexity                                                            | Function             | 128                                  | 111                                     |
| Avoid Artifacts with High Cyclomatic Complexity                                                            | Procedure            | 69                                   | 81                                      |
| Avoid Artifacts with High Cyclomatic Complexity                                                            | View                 | 14                                   | 0                                       |
| Avoid Artifacts with High Depth of Code                                                                    | Function             | 11                                   | 35                                      |
| Avoid Artifacts with High Depth of Code                                                                    | Procedure            | 7                                    | 30                                      |
| Avoid Artifacts with High Depth of Nested Subqueries                                                       | Function             | 95                                   | 10                                      |
| Avoid Artifacts with High Depth of Nested Subqueries                                                       | Procedure            | 78                                   | 4                                       |
| Avoid Artifacts with High Depth of Nested Subqueries                                                       | View                 | 72                                   | 6                                       |
| Avoid Artifacts with High Essential Complexity                                                             | Function             | 747                                  | 128                                     |
| Avoid Artifacts with High Essential Complexity                                                             | Method               | 1                                    | 0                                       |
| Avoid Artifacts with High Essential Complexity                                                             | Procedure            | 353                                  | 82                                      |
| Avoid Artifacts with High Essential Complexity                                                             | Trigger              | 1                                    | 0                                       |
| Avoid Artifacts with High Essential Complexity                                                             | View                 | 137                                  | 0                                       |
| Avoid Artifacts with High Fan-In                                                                           | Function             | 64                                   | 38                                      |
| Avoid Artifacts with High Fan-In                                                                           | Procedure            | 55                                   | 50                                      |
| Avoid Artifacts with High Fan-In                                                                           | View                 | 92                                   | 88                                      |
| Avoid Artifacts with High Fan-Out                                                                          | Function             | 70                                   | 65                                      |
| Avoid Artifacts with High Fan-Out                                                                          | Method               | 1                                    | 0                                       |
| Avoid Artifacts with High Fan-Out                                                                          | Procedure            | 65                                   | 95                                      |
| Avoid Artifacts with High Fan-Out                                                                          | View                 | 42                                   | 23                                      |
| Avoid Artifacts with High Integration Complexity                                                           | Function             | 132                                  | 62                                      |
| Avoid Artifacts with High Integration Complexity                                                           | Procedure            | 69                                   | 51                                      |
| Avoid Artifacts with High Integration Complexity                                                           | View                 | 14                                   | 0                                       |
| Avoid Artifacts with High RAW SQL Complexity                                                               | Function             | 64                                   | 145                                     |
| Avoid Artifacts with High RAW SQL Complexity                                                               | Procedure            | 5                                    | 92                                      |
| Avoid Artifacts with High RAW SQL Complexity                                                               | View                 | 0                                    | 41                                      |
| Avoid Artifacts with SQL statement including subqueries                                                    | Function             | 457                                  | 506                                     |
| Avoid Artifacts with SQL statement including subqueries                                                    | Procedure            | 391                                  | 493                                     |
| Avoid Artifacts with SQL statement including subqueries                                                    | View                 | 734                                  | 890                                     |
| Avoid Artifacts with a Complex SELECT Clause                                                               | Function             | 0                                    | 383                                     |
| Avoid Artifacts with a Complex SELECT Clause                                                               | Procedure            | 0                                    | 178                                     |
| Avoid Artifacts with high Commented-out Code Lines/Code Lines ratio                                        | Function             | 307                                  | 253                                     |
| Avoid Artifacts with high Commented-out Code Lines/Code Lines ratio                                        | Method               | 4                                    | 0                                       |
| Avoid Artifacts with high Commented-out Code Lines/Code Lines ratio                                        | Procedure            | 262                                  | 174                                     |
| Avoid Artifacts with high Commented-out Code Lines/Code Lines ratio                                        | View                 | 82                                   | 81                                      |
| Avoid Artifacts with lines longer than X characters                                                        | Function             | 291                                  | 333                                     |
| Avoid Artifacts with lines longer than X characters                                                        | Procedure            | 268                                  | 273                                     |
| Avoid Artifacts with lines longer than X characters                                                        | Trigger              | 1                                    | 1                                       |
| Avoid Artifacts with lines longer than X characters                                                        | View                 | 1070                                 | 1066                                    |
| Avoid Artifacts with queries on too many Tables and/or Views                                               | Function             | 223                                  | 279                                     |
| Avoid Artifacts with queries on too many Tables and/or Views                                               | Procedure            | 206                                  | 219                                     |
| Avoid Artifacts with queries on too many Tables and/or Views                                               | View                 | 797                                  | 0                                       |
| Avoid Artifacts with too many parameters                                                                   | Function             | 141                                  | 104                                     |
| Avoid Artifacts with too many parameters                                                                   | Procedure            | 137                                  | 47                                      |
| Avoid Cursors inside a loop                                                                                | Procedure            | 2                                    | 6                                       |
| Avoid Rule HINT  /\*+ rule \*/ or --+ rule in PL/SQL code                                                  | View                 | 0                                    | 2                                       |
| Avoid SQL queries with implicit conversions in the WHERE clause                                            | Function             | 0                                    | 224                                     |
| Avoid SQL queries with implicit conversions in the WHERE clause                                            | Procedure            | 0                                    | 214                                     |
| Avoid SQL queries with implicit conversions in the WHERE clause                                            | View                 | 0                                    | 39                                      |
| Avoid Tables not using referential integrity                                                               | Table                | 1507                                 | 1478                                    |
| Avoid Tables not using referential integrity                                                               | Type                 | 0                                    | 29                                      |
| Avoid Tables with more than 20 columns on an OLTP system                                                   | Table                | 202                                  | 191                                     |
| Avoid Tables with more than 20 columns on an OLTP system                                                   | Type                 | 0                                    | 12                                      |
| Avoid Tables without Primary Key                                                                           | Table                | 1013                                 | 984                                     |
| Avoid Tables without Primary Key                                                                           | Type                 | 0                                    | 8                                       |
| Avoid Too Many Copy Pasted Artifacts                                                                       | Function             | 526                                  | 747                                     |
| Avoid Too Many Copy Pasted Artifacts                                                                       | Method               | 30                                   | 0                                       |
| Avoid Too Many Copy Pasted Artifacts                                                                       | Procedure            | 440                                  | 563                                     |
| Avoid Too Many Copy Pasted Artifacts                                                                       | View                 | 1957                                 | 1963                                    |
| Avoid artifacts having recursive calls                                                                     | Function             | 22                                   | 1                                       |
| Avoid artifacts having recursive calls                                                                     | Procedure            | 21                                   | 3                                       |
| Avoid cascading Triggers                                                                                   | Trigger              | 1                                    | 0                                       |
| Avoid empty catch blocks                                                                                   | Procedure            | 6                                    | 0                                       |
| Avoid empty catch blocks                                                                                   | SQL Analyzer Project | 1                                    | 0                                       |
| Avoid empty catch blocks                                                                                   | Universal Project    | 1                                    | 0                                       |
| Avoid exists and not exists independent clauses                                                            | Function             | 8                                    | 0                                       |
| Avoid exists and not exists independent clauses                                                            | Procedure            | 6                                    | 0                                       |
| Avoid exists and not exists independent clauses                                                            | View                 | 419                                  | 0                                       |
| Avoid explicit comparison with NULL                                                                        | View                 | 1                                    | 0                                       |
| Avoid having multiple artifacts deleting data on the same SQL table                                        | Table                | 905                                  | 785                                     |
| Avoid having multiple artifacts deleting data on the same SQL table                                        | Type                 | 0                                    | 30                                      |
| Avoid having multiple artifacts inserting data on the same SQL Table                                       | Table                | 4110                                 | 3700                                    |
| Avoid having multiple artifacts inserting data on the same SQL Table                                       | Type                 | 0                                    | 68                                      |
| Avoid having multiple artifacts updating data on the same SQL Table                                        | Table                | 1732                                 | 2302                                    |
| Avoid having multiple artifacts updating data on the same SQL Table                                        | Type                 | 0                                    | 98                                      |
| Avoid large Artifacts - too many Lines of Code                                                             | Function             | 582                                  | 561                                     |
| Avoid large Artifacts - too many Lines of Code                                                             | Method               | 13                                   | 0                                       |
| Avoid large Artifacts - too many Lines of Code                                                             | Procedure            | 517                                  | 500                                     |
| Avoid large Artifacts - too many Lines of Code                                                             | View                 | 548                                  | 549                                     |
| Avoid large Tables - too many columns                                                                      | Table                | 42                                   | 36                                      |
| Avoid large Tables - too many columns                                                                      | Type                 | 0                                    | 6                                       |
| Avoid long Table or View names                                                                             | Table                | 1440                                 | 1417                                    |
| Avoid long Table or View names                                                                             | Type                 | 0                                    | 23                                      |
| Avoid long Table or View names                                                                             | View                 | 3107                                 | 3107                                    |
| Avoid non-SARGable queries                                                                                 | Function             | 70                                   | 0                                       |
| Avoid non-SARGable queries                                                                                 | Procedure            | 111                                  | 0                                       |
| Avoid non-SARGable queries                                                                                 | View                 | 222                                  | 0                                       |
| Avoid non-indexed SQL queries                                                                              | Function             | 676                                  | 404                                     |
| Avoid non-indexed SQL queries                                                                              | Procedure            | 618                                  | 450                                     |
| Avoid non-indexed SQL queries                                                                              | View                 | 1876                                 | 1199                                    |
| Avoid orphaned synonyms                                                                                    | Synonym              | 566                                  | 62                                      |
| Avoid queries using old style join convention instead of ANSI-Standard joins                               | Function             | 361                                  | 348                                     |
| Avoid queries using old style join convention instead of ANSI-Standard joins                               | Procedure            | 450                                  | 438                                     |
| Avoid queries using old style join convention instead of ANSI-Standard joins                               | View                 | 2044                                 | 2001                                    |
| Avoid redundant indexes                                                                                    | Table                | 0                                    | 79                                      |
| Avoid synonym with both private and public definition                                                      | Synonym              | 5                                    | 5                                       |
| Avoid too many Indexes on one Table                                                                        | Table                | 0                                    | 15                                      |
| Avoid too many Indexes on one Table                                                                        | Type                 | 0                                    | 14                                      |
| Avoid triggers, functions and procedures with a very low comment/code ratio                                | Function             | 1060                                 | 545                                     |
| Avoid triggers, functions and procedures with a very low comment/code ratio                                | Procedure            | 957                                  | 341                                     |
| Avoid triggers, functions and procedures with a very low comment/code ratio                                | Trigger              | 6                                    | 6                                       |
| Avoid undocumented Triggers, Functions and Procedures                                                      | Function             | 965                                  | 458                                     |
| Avoid undocumented Triggers, Functions and Procedures                                                      | Procedure            | 863                                  | 276                                     |
| Avoid undocumented Triggers, Functions and Procedures                                                      | Trigger              | 6                                    | 6                                       |
| Avoid unreferenced Functions                                                                               | Function             | 2025                                 | 2172                                    |
| Avoid unreferenced Functions                                                                               | Procedure            | 2739                                 | 3538                                    |
| Avoid unreferenced Tables                                                                                  | Table                | 533                                  | 540                                     |
| Avoid unreferenced Tables                                                                                  | Type                 | 0                                    | 6                                       |
| Avoid unreferenced views                                                                                   | View                 | 739                                  | 822                                     |
| Avoid using nullable" Columns except in the last position in a Table"                                      | Table                | 0                                    | 1850                                    |
| Avoid using GOTO statement                                                                                 | Function             | 8                                    | 8                                       |
| Avoid using SQL queries inside a loop                                                                      | Function             | 79                                   | 59                                      |
| Avoid using SQL queries inside a loop                                                                      | Procedure            | 158                                  | 216                                     |
| Avoid using dynamic SQL in SQL Artifacts                                                                   | Function             | 236                                  | 105                                     |
| Avoid using dynamic SQL in SQL Artifacts                                                                   | Procedure            | 115                                  | 92                                      |
| Avoid using dynamic SQL in SQL Artifacts                                                                   | Trigger              | 0                                    | 2                                       |
| Avoid using the GROUP BY clause                                                                            | Function             | 71                                   | 134                                     |
| Avoid using the GROUP BY clause                                                                            | Procedure            | 161                                  | 166                                     |
| Avoid using the GROUP BY clause                                                                            | View                 | 115                                  | 0                                       |
| Column references should be qualified                                                                      | Function             | 8                                    | 0                                       |
| Column references should be qualified                                                                      | Procedure            | 53                                   | 0                                       |
| Column references should be qualified                                                                      | View                 | 227                                  | 0                                       |
| Cyclomatic Complexity Distribution                                                                         | Function             | 1480                                 | 922                                     |
| Cyclomatic Complexity Distribution                                                                         | Method               | 743                                  | 0                                       |
| Cyclomatic Complexity Distribution                                                                         | Procedure            | 2923                                 | 692                                     |
| Cyclomatic Complexity Distribution                                                                         | Trigger              | 6                                    | 6                                       |
| Cyclomatic Complexity Distribution                                                                         | View                 | 3912                                 | 0                                       |
| DISTINCT should not be used in SQL SELECT statements                                                       | Function             | 208                                  | 0                                       |
| DISTINCT should not be used in SQL SELECT statements                                                       | Procedure            | 190                                  | 0                                       |
| DISTINCT should not be used in SQL SELECT statements                                                       | View                 | 84                                   | 0                                       |
| Do not mix ANSI and non-ANSI JOIN syntax in the same query                                                 | Function             | 22                                   | 24                                      |
| Do not mix ANSI and non-ANSI JOIN syntax in the same query                                                 | Procedure            | 10                                   | 17                                      |
| Do not mix ANSI and non-ANSI JOIN syntax in the same query                                                 | View                 | 23                                   | 25                                      |
| Errors should be handled in stored procedures and functions                                                | Function             | 221                                  | 0                                       |
| Errors should be handled in stored procedures and functions                                                | Procedure            | 268                                  | 0                                       |
| High Complexity Artifacts                                                                                  | Function             | 72                                   | 68                                      |
| High Complexity Artifacts                                                                                  | Procedure            | 26                                   | 44                                      |
| High Complexity Artifacts                                                                                  | View                 | 3                                    | 0                                       |
| High Complexity classes                                                                                    | Type                 | 1                                    | 0                                       |
| High Coupling Artifacts                                                                                    | Function             | 5                                    | 3                                       |
| High Coupling Artifacts                                                                                    | Procedure            | 6                                    | 2                                       |
| High Coupling Artifacts                                                                                    | View                 | 10                                   | 8                                       |
| High Reuse by Call                                                                                         | Function             | 67                                   | 23                                      |
| High Reuse by Call                                                                                         | Procedure            | 41                                   | 26                                      |
| High Reuse by Call                                                                                         | View                 | 156                                  | 149                                     |
| High SQL Complexity Artifacts                                                                              | Function             | 15                                   | 203                                     |
| High SQL Complexity Artifacts                                                                              | Procedure            | 0                                    | 178                                     |
| High SQL Complexity Artifacts                                                                              | View                 | 0                                    | 107                                     |
| LIKE operator should not start with a wildcard character                                                   | Function             | 2                                    | 0                                       |
| LIKE operator should not start with a wildcard character                                                   | Procedure            | 12                                   | 0                                       |
| LIKE operator should not start with a wildcard character                                                   | View                 | 11                                   | 0                                       |
| LONG and LONG RAW datatypes should no longer be used                                                       | Function             | 0                                    | 1                                       |
| LONG and LONG RAW datatypes should no longer be used                                                       | Procedure            | 0                                    | 2                                       |
| LONG and LONG RAW datatypes should no longer be used                                                       | Table                | 52                                   | 52                                      |
| LONG and LONG RAW datatypes should no longer be used                                                       | View                 | 0                                    | 221                                     |
| Large Size Artifacts                                                                                       | Function             | 10                                   | 170                                     |
| Large Size Artifacts                                                                                       | Procedure            | 8                                    | 184                                     |
| Large Size Artifacts                                                                                       | View                 | 0                                    | 39                                      |
| Low Complexity Artifacts                                                                                   | Function             | 1194                                 | 347                                     |
| Low Complexity Artifacts                                                                                   | Method               | 2                                    | 0                                       |
| Low Complexity Artifacts                                                                                   | Procedure            | 294                                  | 250                                     |
| Low Complexity Artifacts                                                                                   | Trigger              | 1                                    | 1                                       |
| Low Complexity Artifacts                                                                                   | View                 | 130                                  | 0                                       |
| Low Coupling Artifacts                                                                                     | Function             | 67                                   | 23                                      |
| Low Coupling Artifacts                                                                                     | Procedure            | 41                                   | 26                                      |
| Low Coupling Artifacts                                                                                     | View                 | 156                                  | 149                                     |
| Low OO Complexity Class                                                                                    | Type                 | 890                                  | 0                                       |
| Low SQL Complexity Artifacts                                                                               | Function             | 560                                  | 375                                     |
| Low SQL Complexity Artifacts                                                                               | Procedure            | 512                                  | 384                                     |
| Low SQL Complexity Artifacts                                                                               | Trigger              | 1                                    | 0                                       |
| Low SQL Complexity Artifacts                                                                               | View                 | 747                                  | 653                                     |
| Moderate Complexity Artifacts                                                                              | Function             | 116                                  | 89                                      |
| Moderate Complexity Artifacts                                                                              | Procedure            | 133                                  | 159                                     |
| Moderate Complexity Artifacts                                                                              | View                 | 28                                   | 0                                       |
| Moderate Complexity classes                                                                                | Type                 | 890                                  | 0                                       |
| Moderate Fan-In classes                                                                                    | Type                 | 891                                  | 0                                       |
| Moderate Fan-Out classes                                                                                   | Type                 | 891                                  | 0                                       |
| Moderate OO Complexity Class                                                                               | Type                 | 1                                    | 0                                       |
| Moderate SQL Complexity Artifacts                                                                          | Function             | 43                                   | 99                                      |
| Moderate SQL Complexity Artifacts                                                                          | Procedure            | 2                                    | 53                                      |
| Moderate SQL Complexity Artifacts                                                                          | View                 | 0                                    | 479                                     |
| Never use SQL queries with a cartesian product                                                             | Function             | 14                                   | 19                                      |
| Never use SQL queries with a cartesian product                                                             | Procedure            | 72                                   | 85                                      |
| Never use SQL queries with a cartesian product                                                             | View                 | 57                                   | 73                                      |
| Never use WHEN OTHER THEN NULL                                                                             | Procedure            | 5                                    | 0                                       |
| Number of Code Lines                                                                                       | Function             | 2862                                 | 1480                                    |
| Number of Code Lines                                                                                       | Method               | 745                                  | 0                                       |
| Number of Code Lines                                                                                       | Procedure            | 3376                                 | 1162                                    |
| Number of Code Lines                                                                                       | Trigger              | 7                                    | 7                                       |
| Number of Code Lines                                                                                       | View                 | 4073                                 | 4073                                    |
| Number of Datablocks                                                                                       | Function             | 2862                                 | 1480                                    |
| Number of Datablocks                                                                                       | Procedure            | 3376                                 | 1162                                    |
| Number of Files                                                                                            | Type                 | 891                                  | 0                                       |
| Number of Forms                                                                                            | Function             | 2862                                 | 1480                                    |
| Number of Forms                                                                                            | Method               | 745                                  | 0                                       |
| Number of Forms                                                                                            | Procedure            | 3376                                 | 1162                                    |
| Number of Forms                                                                                            | Trigger              | 7                                    | 7                                       |
| Number of Forms                                                                                            | View                 | 4073                                 | 4073                                    |
| Number of Functions                                                                                        | Table                | 1982                                 | 1953                                    |
| Number of Functions                                                                                        | Type                 | 0                                    | 29                                      |
| Number of Tables                                                                                           | View                 | 4073                                 | 4073                                    |
| Number of Views                                                                                            | Trigger              | 7                                    | 7                                       |
| Prefer PRIVATE to PUBLIC synonym                                                                           | Synonym              | 3200                                 | 3200                                    |
| Prefer UNION ALL to UNION                                                                                  | Function             | 70                                   | 83                                      |
| Prefer UNION ALL to UNION                                                                                  | Procedure            | 59                                   | 60                                      |
| Prefer UNION ALL to UNION                                                                                  | View                 | 122                                  | 147                                     |
| Replace OR conditions testing equality on the same identifier in SQL WHERE clauses by an IN test condition | Function             | 10                                   | 0                                       |
| Replace OR conditions testing equality on the same identifier in SQL WHERE clauses by an IN test condition | Procedure            | 11                                   | 0                                       |
| Replace OR conditions testing equality on the same identifier in SQL WHERE clauses by an IN test condition | View                 | 34                                   | 0                                       |
| Reuse by Call Distribution                                                                                 | Function             | 5                                    | 3                                       |
| Reuse by Call Distribution                                                                                 | Procedure            | 6                                    | 2                                       |
| Reuse by Call Distribution                                                                                 | View                 | 6                                    | 8                                       |
| SQL Complexity Distribution                                                                                | Function             | 2244                                 | 803                                     |
| SQL Complexity Distribution                                                                                | Method               | 745                                  | 0                                       |
| SQL Complexity Distribution                                                                                | Procedure            | 2862                                 | 547                                     |
| SQL Complexity Distribution                                                                                | Trigger              | 6                                    | 7                                       |
| SQL Complexity Distribution                                                                                | View                 | 3326                                 | 2834                                    |
| Small Size Artifacts                                                                                       | Function             | 345                                  | 466                                     |
| Small Size Artifacts                                                                                       | Method               | 1                                    | 0                                       |
| Small Size Artifacts                                                                                       | Procedure            | 305                                  | 356                                     |
| Small Size Artifacts                                                                                       | Trigger              | 0                                    | 2                                       |
| Small Size Artifacts                                                                                       | View                 | 174                                  | 2830                                    |
| Specify column names instead of column numbers in ORDER BY clauses                                         | Function             | 3                                    | 0                                       |
| Specify column names instead of column numbers in ORDER BY clauses                                         | Procedure            | 1                                    | 0                                       |
| Table naming convention - character set control                                                            | SQL Analyzer Project | 1                                    | 0                                       |
| Table naming convention - character set control                                                            | Table                | 1170                                 | 0                                       |
| Table naming convention - character set control                                                            | Universal Project    | 1                                    | 0                                       |
| Tables aliases should not end with a numeric suffix                                                        | Function             | 98                                   | 0                                       |
| Tables aliases should not end with a numeric suffix                                                        | Procedure            | 50                                   | 0                                       |
| Tables aliases should not end with a numeric suffix                                                        | View                 | 216                                  | 0                                       |
| Tables should be aliased                                                                                   | Function             | 2                                    | 0                                       |
| Tables should be aliased                                                                                   | Procedure            | 1                                    | 0                                       |
| Tables should be aliased                                                                                   | View                 | 42                                   | 0                                       |
| Triggers should not directly modify tables, a procedure or function should be used instead                 | Trigger              | 1                                    | 0                                       |
| Use MINUS or EXCEPT operator instead of NOT EXISTS and NOT IN subqueries                                   | Function             | 64                                   | 0                                       |
| Use MINUS or EXCEPT operator instead of NOT EXISTS and NOT IN subqueries                                   | Procedure            | 37                                   | 0                                       |
| Use MINUS or EXCEPT operator instead of NOT EXISTS and NOT IN subqueries                                   | View                 | 123                                  | 0                                       |
| Use WHEN OTHERS in exception management                                                                    | Function             | 62                                   | 4                                       |
| Use WHEN OTHERS in exception management                                                                    | Procedure            | 141                                  | 2                                       |
| Use at most one statement per line                                                                         | Function             | 17                                   | 80                                      |
| Use at most one statement per line                                                                         | Procedure            | 3                                    | 126                                     |
| VARCHAR2 and NVARCHAR2 should be used                                                                      | Function             | 32                                   | 65                                      |
| VARCHAR2 and NVARCHAR2 should be used                                                                      | Package              | 27                                   | 66                                      |
| VARCHAR2 and NVARCHAR2 should be used                                                                      | Procedure            | 50                                   | 50                                      |
| VARCHAR2 and NVARCHAR2 should be used                                                                      | Table                | 94                                   | 94                                      |
| Very High Reuse by Call                                                                                    | Function             | 26                                   | 5                                       |
| Very High Reuse by Call                                                                                    | Procedure            | 21                                   | 10                                      |
| Very High Reuse by Call                                                                                    | View                 | 30                                   | 28                                      |
| View naming convention - character set control                                                             | View                 | 1620                                 | 0                                       |