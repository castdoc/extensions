﻿set search_path to castpubs_sql_src_local;

create or replace view TransformSQLObjects
as
select distinct lower(ofn.fullname) ObjectName, 
	case
		when t.TypDsc like '%Column%' then 'Column'
		when t.TypDsc like '%Unique%' or t.TypDsc like '%Index%' or t.TypDsc like '%Primary%' then 'Indexes Primary Keys Unique Constraints'
	else t.TypDsc 
	end ObjectType
from ObjFulNam ofn join
Keys k 
on k.IdKey = ofn.IdObj
join Typ t
on k.ObjTyp = t.IdTyp
where t.IdTyp in (select IdTyp from TypCat where IdCatParent = 1101004)
and (	t.TypDsc not like '%Project%' 
	and t.TypDsc not like '%Subset%'
	)
order by ObjectType, ObjectName;


create or replace view Count_TransformSQLObjects
as
select ObjectType, count(1) NumberOfObjects
from TransformSQLObjects
group by ObjectType
order by ObjectType;
