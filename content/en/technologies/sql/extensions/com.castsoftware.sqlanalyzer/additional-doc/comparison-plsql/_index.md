---
title: "Comparison with PL-SQL Analyzer"
linkTitle: "Comparison with PL-SQL Analyzer"
type: "docs"
---

The mapping between PL-SQL Analyzer and SQL Analyzer objects, links and
rules is available at: [SQL Analyzer - Mapping between SQL](../mapping).

In order to explain the differences between the two analyzers and the
impacts on the different metrics, we have made some tests using the
applications. See below: