﻿set search_path to castoncast_sql_src_local;


select 'SQL' Analyzer, CNT_DataFunctions, CNTDFP_FunctionPoints, CNTDF_CalibratedFunctionPoints, CNT_Transactions, CNTTFP_FunctionPoints, CNTT_CalibratedFunctionPoints
 from castoncast_sql_src_local.IDRE_NOOF_DFT
 union all
select  'Oracle', CNT_DataFunctions, CNTDFP_FunctionPoints, CNTDF_CalibratedFunctionPoints, CNT_Transactions, CNTTFP_FunctionPoints, CNTT_CalibratedFunctionPoints
 from castoncast_ora_local.IDRE_NOOF_DFT
 ;