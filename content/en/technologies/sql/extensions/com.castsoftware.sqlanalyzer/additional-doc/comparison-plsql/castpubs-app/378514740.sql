﻿set search_path to castpubs_ora_local;

create or replace view ORA_TQI
as
select dmr.metric_num_value TQI
from dss_metric_results dmr, dss_objects dso
where dso.object_type_id = 139278
and dmr.metric_id in (60017) /* Health Factors */
and dmr.snapshot_id = -3
and dmr.metric_value_index = 0
and dmr.object_id = dso.object_id;

set search_path to castpubs_sql_src_local;

create or replace view SQL_TQI
as
select dmr.metric_num_value TQI
from dss_metric_results dmr, dss_objects dso
where dso.object_type_id = 1101002
and dmr.metric_id in (60017) /* Health Factors */
and dmr.snapshot_id = -3
and dmr.metric_value_index = 0
and dmr.object_id = dso.object_id;

create or replace view TQI
as
select 'SQL' as analyzer, (select TQI from castpubs_sql_src_local.SQL_TQI) as TQI
union all 
select 'Oracle' as analyzer, (select TQI from castpubs_ora_local.ORA_TQI) as TQI;


select * from TQI;


