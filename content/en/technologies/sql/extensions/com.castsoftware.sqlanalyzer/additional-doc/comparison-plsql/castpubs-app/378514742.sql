﻿set search_path to castpubs_sql_src_local;

create or replace view CompareSQLORADetailsLinks
as
select case 
	when sql.KindOfLink is null then ora.KindOfLink  
	when ora.KindOfLink is null then sql.KindOfLink
	else sql.KindOfLink  
	end KindOfLink,
case 
	when sql.CallerType is null then ora.CallerType  
	when ora.CallerType is null then sql.CallerType
	else sql.CallerType
	end CallerType,
case 
	when sql.CallerName is null then ora.CallerName  
	when ora.CallerName is null then sql.CallerName
	else sql.CallerName  
	end CallerName,
case 
	when sql.CalleeType is null then ora.CalleeType  
	when ora.CalleeType is null then sql.CalleeType
	else sql.CalleeType  
	end CalleeType,
case 
	when sql.CalleeName is null then ora.CalleeName  
	when ora.CalleeName is null then sql.CalleeName
	else sql.CalleeName  
	end CalleeName,	
case 
	when sql.CallerType is null then 'Detected only by PL/SQL Analyzer' 
	when ora.CallerType is null then 'Exists only on SQL'
	when ora.CallerName = sql.CallerName and ora.CalleeName = sql.CalleeName then '='
	end "Compared with Oracle" 
from 
castpubs_sql_src_local.Details_SQLLinks sql full outer join castpubs_ora_local.Details_ORALinks ora
on ora.CallerType = sql.CallerType 
	and ora.CallerName = sql.CallerName
	and ora.CalleeType = sql.CalleeType 
	and ora.CalleeName = sql.CalleeName
	and ora.KindOfLink = sql.KindOfLink
where sql.CallerType is NULL or ora.CallerType is NULL
order by KindOfLink, CallerType, CalleeType, CallerName, CalleeName;


select * from CompareSQLORADetailsLinks;