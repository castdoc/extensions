---
title: "Generate DDL for SQLite"
linkTitle: "SQLite"
type: "docs"
---

## Introduction

To generate DDL from SQLite, CAST recommends using the sqlite3 tool.
This tool is shipped with all instances of SQLite. There are two
commands available:

- `.dump`: This command will dump an entire database, including the data.
- `.schema`: This command will dump only CREATE statements for the entire database, excluding the data.

Each of the commands above can be output to an .SQL file using the
.output command.

CAST has not tested these commands extensively and you should therefore
refer to third-party documentation for more
information: [https://www.sqlitetutorial.net/sqlite-dump/](https://www.sqlitetutorial.net/sqlite-dump/)

## Examples

### .dump

``` java
sqlite3 c:/sqlite/mydatabase.db
.output c:/export/mydatabase.sql
.dump
.exit
```

### .schema

``` java
sqlite3 c:/sqlite/mydatabase.db
.output c:/export/mydatabase.sql
.schema
.exit
```
