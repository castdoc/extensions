---
title: "Database Discoverer"
linkTitle: "Database Discoverer"
type: "docs"
no_list: false
---

>The Database Discoverer is not currently provided as an extension:
instead it is embedded in CAST Imaging Core and is therefore present "out of the
box".

<table>
<thead>
<tr>
<th><p>Supported project type</p></th>
<th><p>CAST Delivery Manager Tool
interface</p></th>
</tr>
</thead>
<tbody>
<tr>
<td>Database/schema project</td>
<td><div class="content-wrapper">
<p>Configures a project/Analysis Unit for
each <strong>database/schema</strong> delivered as source
code. Displayed as follows:</p>
<p><strong>AIP Console</strong></p>
<p>Example for Oracle schemas:</p>
<p><strong></strong></p>
<p><strong>CAST Management Studio</strong></p>
<p>Example for Oracle schemas:</p>
<p><img src="414941551.jpg" /></p>
</div></td>
</tr>
</tbody>
</table>

Note that this discoverer is only invoked when delivering the results of
a [database
extraction](https://doc.castsoftware.com/display/DOCCOM/CAST+Database+Extractor)
as discussed in [SQL - Prepare and deliver the source code](../../prepare/). It is not invoked
when using the [SQL Analyzer](../com.castsoftware.sqlanalyzer/).

