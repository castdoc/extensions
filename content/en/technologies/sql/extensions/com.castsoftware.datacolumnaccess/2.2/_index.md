---
title: "Data Column Access - 2.2"
linkTitle: "2.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.datacolumnaccess

## What's new?

See [Release Notes](rn/).

## Description

The Data Column Access extension provides the following features:

-   Data sensitivity checks on table columns (custom and
    predefined)
-   Creation of access links for table columns

In what situation should you install this extension?

-   If you need to check that the application is data sensitive
    compliant using custom and predefined indicators for table columns
-   If you need to see Access Read and Access Write links for columns on
    SELECT, INSERT, UPDATE, DELETE and MERGE statements
-   If you need to see Access Write links between columns and Cobol
    data, from INSERT, UPDATE and SELECT .. INTO statements
-   If you need to see Access Write links for columns selected in the
    DECLARE cursor statement and the Cobol data fetched in FETCH cursor
    statement

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Supported client languages

All supported client languages are listed in the [SQL Analyzer](../../com.castsoftware.sqlanalyzer/) extension documentation. From a technical point of view, the following list of Embedded SQL Queries are supported:

| Embedded SQL Queries | Metamodel details  |
|----------------------|--------------------|
| SQL Named Queries  | `CAST_SQL_MetricableQuery.sqlQuery` on objects which inherit from the category `CAST_SQL_MetricableQuery` |
| Java Property Mapping Queries | `CAST_JEE_MappingProperties.mapValue` on Java Property Mapping objects |
| Inference Engine Queries | `physicalLink.inferenceEngineRequests` on Inference Engine links |

## Download and installation instructions

This extension will be automatically downloaded and installed when
SQL source code is delivered for analysis.

![](../images/install_datacolumnaccess.png)
  
>Note that if you are using the legacy workflow without Fast Scan in CAST
Console (not required when using the Fast Scan workflow where this
option is automatically enabled), you must also manually enable the Data
Sensitivity option as part of the source code delivery process -
see Application onboarding without Fast Scan - Standard onboarding -
perform all actions - choose objectives:
![](../images/631177260.jpg)

## Data sensitivity configuration

See [Data Sensitivity](../../../../multi/data-sensitivity/) for more information about
setting a property on specific objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

## What results can you expect?

### Table/column properties for data sensitivity (custom and predefined)

-   Data Sensitivity Indicator: displays the most
    sensitive value (Sensitive, Very Sensitive, Highly Sensitive) for
    any of the columns in the selected table.
-   List of the specific Sensitive / Very sensitive / Highly
    sensitive columns

Example:

![](../images/629735484.jpg)

### Links

-   Links are created for transaction and function point needs.
-   You can expect the following links on the DDL side within the same sql file:
    -   `accessRead` from View / Procedure / Function / Trigger / Event  to Column
    -   `accessWrite` from Procedure / Function / Trigger / Event to Column     
-   You can expect the same links for the client technologies supported by the SQL Analyzer, for more details please refer to SQL Analyzer documentation.

#### Cobol additional links
  
When Cobol Data are created during Cobol Analysis and the production option `Save data and links to other data` is activated, you can expect the following links:
    -   `accessWrite` from Cobol Data to Column, for the case of `insert into` and `update`. For example, for `insert into table1 (col1, col2, ...) values (:data1, data2)` the `data1, data2` will `access write` the `col1, col2`. And for example for `Update table1 set col1 = :data1, col2 = :data2` the `data1, data2` will `access write` `col1, col2`
        -   Sometimes those links have bookmarks but sometimes the bookmarks are missing, for details refer: Limitations. When
            the bookmark exists, it will pointing to the Cobol Data definition and not to the SQL statement.
    -   `accessWrite` from Column to Cobol Data, for the case of `select into`. For example for, `select col1, col2, ... into :data1, :data2` the `col1, col2` will `access write` the `data1, data2`. 
        -   Those links have no bookmarks, for details refer: Limitations.
    -   `accessWrite` from Column selected in DECLARE cursor to Cobol Data fetched in FETCH cursor. For example for, `declare toto_curs cursor` for `select col_toto from toto`, followed by fetch `toto_curs into :var1`. The `col_toto` will `access write` the `var1`. 
        -   Those links have no bookmarks, for details refer: Limitations.

#### Why does CAST not create accessRead links between COBOL data variables and SQL columns ?

CAST does not create links between the COBOL data variables and selected
columns in the WHERE clauses because those links are not significant for
"how data circulates in an application". To resume, CAST only
creates accessWrite links as follows:

-   SELECT: Only the INTO clause is considered because the SQL Column
    will write into the Cobol Data variables
-   UPDATE: Only the SET clause is be considered because he Cobol Data
    variables will write into the SQL Column
-   INSERT: Only the Cobol Data variables that will write into the SQL
    Columns is considered
-   FETCH: The SQL columns selected in the declare cursor will write
    into the fetched Cobol Data variables

#### Example

##### Select Into, Move, Update

The value we have in the column COL_TOTO will be written in the Cobol
data VAR1. What we have in VAR1 will move into the Cobol Data VAR2. At
the end, VAR2 will update the column COL_TOTO, here is the source code :

``` java
       IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID.   TESTMOVE.                                          00020000
       DATA DIVISION.                                                   00340000
       WORKING-STORAGE SECTION.                                         00010000
       01 VAR1 PIC X.                                                   00010000
       02 VAR2 PIC X.                                                   00010000
                                                                        00010000
       PROCEDURE DIVISION.                                              00010000
                                                                        00010000
       EXEC SQL                                                         00010000
        SELECT COL_TOTO                                                 00010000
            INTO :VAR1                                                  00010000
        FROM TOTO                                                       00010000
       END-EXEC                                                         00010000
                                                                        00010000
       MOVE VAR1 TO VAR2                                                00010000
                                                                        00010000
       EXEC SQL                                                         00010000
       UPDATE TITI                                                      00010000
        SET COL_TITI = :VAR2                                            00010000
       END-EXEC                                                         00010000
```

![](../images/selmoveupd.png)
![](../images/selvar2.png)
![](../images/updvar1.png)

### Performance

When using the Data Column Access extension 1.0.0, analysis execution
time is slightly different (usually slightly longer). Here are some
details of the performance we have experienced using AIP Core 8.3.24 and
SQL Analyzer 3.4.4-funcrel:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Use case</th>
<th class="confluenceTh">Technical details</th>
<th class="confluenceTh">Execution details</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>100% SQL (Teradata)</p></td>
<td class="confluenceTd"><p>1 single SQL file, file size 14.5MB</p>
<p>Detected variant: Ansisql</p>
<p>129,529 LOC</p></td>
<td class="confluenceTd"><p>Analysis duration for 1 analysis unit:</p>
<ul>
<li>without Data Column Access extension = 20 min 20 sec</li>
<li>with Data Column Access extension = 21 min 26 sec  </li>
</ul>
<p>No data sensitivity</p>
<p>988 access links are added</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Cobol + SQL (Db2)</td>
<td class="confluenceTd"><p>SQL: 1 single file, file size 289KB</p>
<p>Detected variant: Db2</p>
<p>Cobol: 1,996 files, 11 folders, 52MB</p>
<p>443.8K LOC</p></td>
<td class="confluenceTd"><p>Analysis duration for 2 analysis units:</p>
<ul>
<li>without Data Column Access extension = 19 min 19 sec</li>
<li>with Data Column Access extension = 19 min 27 sec</li>
</ul>
<p>No data sensitivity</p>
<p>15,245 access links are added, all of them on the Cobol side</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">.NET + SQL (Microsoft SQL Server)</td>
<td class="confluenceTd"><p>SQL: 6 files (3 files of 6 are
.sqltablesize), 1 folder, 4.08 MB</p>
<p>Detected variant: SQL Server</p>
<p>.NET: 14,062 files, 2,747 folders, 1.04GB</p>
<p>Assemblies: 218 files, 214 folders, 68.2MB</p>
<p>698K LOC</p></td>
<td class="confluenceTd"><p>Analysis duration for 185 analysis
units:</p>
<ul>
<li>without Data Column Access extension = 2 hours 9 min 34 sec</li>
<li>with Data Column Access extension = 2 hours 5min 27 sec</li>
</ul>
<p>No data sensitivity</p>
<p>From a total of 2,562 added access links, 6 are detected on the .NET
side</p></td>
</tr>
</tbody>
</table>
