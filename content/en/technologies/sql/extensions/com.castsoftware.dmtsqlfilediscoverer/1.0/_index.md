---
title: "Database File Discoverer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtsqlfilediscoverer

## What's new?

See [Release Notes](rn/).

## Extension description

This discoverer detects a project for each of the following file
extensions identified in the source code:

| File extension | Description of source file content |
|---|---|
| `.lf` | Logical File, in DDS format, extracted from IBM i Series library QDBSRC or QDDSSRC. |
| `.lf38` | Logical File, in DDS format, extracted from IBM i Series library QDBSRC or QDDSSRC. |
| `.pf` | Physical File, in DDS format, extracted from IBM i Series library QDBSRC or QDDSSRC. |
| `.pf38` | Physical File, in DDS format, extracted from IBM i Series library QDBSRC or QDDSSRC. |
| `*.sql` | Source file with DDL (CREATE TABLE, ...)  and/or DML (SELECT / INSERT / ...) statements. |
| `*.sqlt` | Source file with DDL and/or DML statements. |
| `*.uaxdirectory`, `*.uax`, `*.src` | Physical files created by the [CAST Database Extractor](../../../../../export/DOCCOM/CAST+Database+Extractor) containing the results of the extraction process. |

## In what situation should you install this extension?

This extension should be used when you are delivering SQL or DDS source
code in file format for analysis with the [SQL Analyzer](../../com.castsoftware.sqlanalyzer). The discoverer will automatically
detect one project for each matching file found in the source
code. One corresponding SQL related Analysis Unit (per project) will
then be created for analysis purposes.

{{% alert color="info" %}}This extension complements (and does not replace)
the standard `AUTO-UA-SQL` analysis unit created automatically when SQL
files are delivered for analysis.{{% /alert %}}

{{% alert color="warning" %}}This extension should NOT be used when working with CAST Console 1.x: a bug exists where two analysis units will be created for the same source code, one by this extension and one automatically via `AUTO-UA-SQL`, therefore duplicating analysis results. In later releases of CAST Console, this behaviour has been modified and only Analysis Units created by this extension will be used.{{% /alert %}}

## Specific behaviour in CAST Console

When using CAST Console ≥ 2.8 the following behaviour will occur:

### For RPG related source code extracted from IBM i Series

#### For applications created from scratch in CAST Console ≥ 2.8

- **Scenario 1**: when the delivered source code contains only `.lf`/`.lf38` and `.pf`/`.pf38` files, then only SQL Analyzer Analysis Units are created.
- **Scenario 2**: when source code contains `.lf`/`.lf38`, `.pf`/`.pf38` and `.sql` files, then
    only SQL Analyzer Analysis Units are created.
- **Scenario 3**: when source code contains `.lf`/`.lf38`, `.pf`/`.pf38`, `.sql` and other file extensions identified by the RPG discoverer such as `.cl` files, then SQL Analyzer Analysis Units and RPG related Analysis Units are created.

#### For existing applications post upgrade to CAST Console ≥ 2.8

- **Scenario 1**: when RPG related Analysis Units and the `AUTO-UA-SQL` Analysis Unit already exists for source code containing `.lf`/`.lf38`, `.pf`/`.pf38` and `.sql` files then NO additional SQL Analysis Units corresponding to the **com.castsoftware.dmtsqlfilediscoverer** will be created. CAST Console will, however, clean up any duplicate SQL Analyzer Analysis Units and retain existing RPG related Analysis Units and the `AUTO-UA-SQL` Analysis Unit.
- **Scenario 2**: when only RPG related Analysis Units exist (i.e. there is no `AUTO-UA-SQL` Analysis Unit) for source code containing only `.lf`/`.lf38` and `.pf`/`.pf38` files, then NO additional SQL Analysis Units corresponding to the **com.castsoftware.dmtsqlfilediscoverer** will be created. CAST Console will, however, clean up any duplicate SQL Analyzer Analysis Units and retain existing RPG related Analysis Units.

### For legacy \*.uaxdirectory, \*.uax, \*.src files extracted using the CAST Database Extractor

#### For applications created from scratch in CAST Console ≥ 2.8

Only SQL Analyzer Analysis Units are created. No legacy Analysis
Units for PL/SQL, Microsoft SQL Server and Sybase ASE will be generated.

#### For existing applications post upgrade to CAST Console ≥ 2.8

Only legacy Analysis Units for PL/SQL, Microsoft SQL Server and Sybase
ASE will be present. CAST Console will, however, clean up any
duplicate SQL Analyzer Analysis Units.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

This extension will be automatically installed in CAST Console
≥ 2.5 when any one of the file types listed above is delivered for
analysis. You can manage the extension using UI:

![](../images/625705169.jpg)

In older releases of CAST Console, you will need to manually install the
extension:

![](../images/619053252.jpg) 
