---
title: "Prepare and deliver the source code"
linkTitle: "Prepare and deliver the source code"
type: "docs"
weight: 3
---

## Introduction

Discovery is a process that is actioned during the packaging process
whereby CAST will attempt to automatically identify projects within your
application using a set of predefined rules. Discoverers are
currently embedded in CAST Imaging:

-   [Database Discoverer](../extensions/db-discoverer/)

You should read the relevant documentation for each discoverer (provided
in the link above) to understand how the source code will be handled.

When the Package action is complete, you can view the projects that have
been identified in the Package Content tab. When a successfully
Packaged application is subsequently deployed in the CAST Management
Studio, an Analysis Unit will be created for each project that
has been identified during the Discovery process and is not excluded by
a rule or filter.

## Using CAST Imaging Console

CAST Imaging Console expects either a ZIP/archive file or source code
located in a folder configured in CAST Imaging Console. You should include in
the ZIP/source code folder all the output from the [CAST Database
Extractor](https://doc.castsoftware.com/display/DOCCOM/CAST+Database+Extractor),
i.e:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh"><p>.castextraction </p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>CAST Imaging Console supports this file type as follows:</p>
<ul>
<li>for extractions of <strong>PL/SQL</strong> (Oracle Server) schemas
in <strong>all CAST Imaging Console releases</strong></li>
<li>for extractions <strong>T-SQL</strong> (Microsoft SQL Server, Sybase
ASE and Azure SQL) in <strong>CAST Imaging Console ≥ 1.22</strong></li>
</ul>
<p>When a .castextraction file is delivered to CAST Imaging Console, it will be
transformed automatically into the required
<strong>.uaxDirectory/.uax./src</strong> files during the source code
delivery process. Technically the following occurs:</p>
<ul>
<li>For each .castextraction file CAST Imaging Console checks if there is no
equivalent .uaxdirectory for the same extraction (in the delivery)
<ul>
<li>If <strong>YES</strong>, then the .castextraction file is removed
from the list of extraction files and the .uaxDirectory is used
instead.</li>
<li>If <strong>NO</strong>, then CAST Imaging Console checks the type of
.castexraction file that has been delivered.
<ul>
<li>If a <strong>PL/SQL</strong> .castextraction file has been
delivered, then no immediate preprocessing is necessary and the list of
extraction files is not changed. The transformation to .uaxDirectory is
actioned when the version is <strong>set as the current
version</strong>.</li>
<li>If a <strong>T-SQL</strong> .castextraction file has been delivered,
then this will undergo a preprocessing action: 
<ul>
<li>CAST Imaging Console cleans up the <strong>preprocessed</strong> folder on
the node (located
in <strong>%PROGRAMDATA%\CAST\AipConsole\AipNode\upload\preprocessed</strong>)</li>
<li>Folder structure is created within the <strong>preprocessed</strong>
folder to support multiple .castextraction files if necessary</li>
<li>The delivered .castextraction file is transformed into uaxDirectory
format</li>
<li>The .castextraction file is replaced by the new uaxdirectory format
files in the list of extraction files.</li>
</ul></li>
</ul></li>
</ul></li>
</ul>
<div>
<div>
<p>With regard to T-SQL .castextraction files:</p>
<ul>
<li>If using <strong>CAST Imaging Core</strong> ≤ 8.3.25, the creation of invalid
or unsupported .castextraction files was allowed. As such, if (during a
new source code delivery) AIP Console finds an existing package
containing an invalid or unsupported .castextraction file , a popup
stating that the package is invalid will be displayed. This invalid
package should be should be removed using the <img
src="486310096.png" />  - this will
then allow the new .castextraction file to be processed.</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh"><ul>
<li><strong>.uaxDirectory</strong></li>
<li><strong>.uax</strong></li>
<li><strong>.src</strong></li>
</ul></th>
<td class="confluenceTd">Supported for both PL/SQL (Oracle Server)
schemas and T-SQL (Microsoft SQL Server/Sybase ASE/Azure SQL)
databases.</td>
</tr>
</tbody>
</table>

CAST highly recommends placing the files in a folder dedicated to your
extraction. If you are using a ZIP/archive file, zip the folders in the
"temp" folder - but do not zip the "temp" folder itself, nor create any
intermediary folders:

``` java
D:\temp
    |-----LegacySQLExtaction
    |-----OtherTechno1
    |-----OtherTechno2
```
