---
title: "Required RDBMS rights for packaging a database using the legacy CAST Delivery Manager Tool"
linkTitle: "Required RDBMS rights for packaging a database using the legacy CAST Delivery Manager Tool"
type: "docs"
weight: 1
---

## Introduction

The CAST Delivery Manager Tool provides the means for Delivery Managers
to configure a connection to a live Oracle Server/Microsoft SQL
Server/Sybase ASE and then use this connection to perform an
extraction of the relevant schemas to file using CAST's own SQL
extractor.  Delivery Managers must ensure that they configure their
package with a user that has sufficient rights to perform an extraction,
otherwise essential data may not be included. This page provides the
required rights for all RDBMS systems supported by CAST for live
extraction.

## Oracle Server

![](248815215.jpg)

In order to carry out an extraction of the required schemas, the person
in charge of the extraction (Delivery Manager or DBA) must use one of
the following Oracle users:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">User</th>
<th class="confluenceTh">Notes</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><strong>Extraction user</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CAST highly recommends using a dedicated <strong>extraction
user</strong> with <strong>specific privileges</strong> in order to
access the required data for extraction. When using this dedicated
<strong>extraction user</strong>, CAST will query the <strong>DBA_*
views</strong> to access the required data - these views give access to
ALL objects. Please also read the section below entitled "<strong>Note
about the DBA_ARGUMENTS view and the dedicated extraction
user</strong>".</p>
<p>To create the dedicated extraction user, please run the following
script as the <strong>SYS</strong> user - it will create the dedicated
user and then grant the required privileges to the user (where
USER_FOR_EXTRACTION is the dedicated extraction user):</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: sql; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: sql; gutter: false; theme: Confluence"><pre
class="sourceCode sql"><code class="sourceCode sql"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="kw">create</span> <span class="fu">user</span> USER_FOR_EXTRACTION <span class="kw">identified</span> <span class="kw">by</span> <span class="fu">cast</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">connect</span> <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">create</span> <span class="kw">session</span> <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_arguments <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_col_comments <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_tab_comments <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-13"><a href="#cb1-13" aria-hidden="true" tabindex="-1"></a><span class="co">/*  </span></span>
<span id="cb1-14"><a href="#cb1-14" aria-hidden="true" tabindex="-1"></a><span class="co">*   The view dba_mview_comments does not exist on Oracle 9.x, therefore the</span></span>
<span id="cb1-15"><a href="#cb1-15" aria-hidden="true" tabindex="-1"></a><span class="co">*   following grant should not be executed when running the extraction on Oracle 9i.</span></span>
<span id="cb1-16"><a href="#cb1-16" aria-hidden="true" tabindex="-1"></a><span class="co">*/</span></span>
<span id="cb1-17"><a href="#cb1-17" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_mview_comments <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-18"><a href="#cb1-18" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-19"><a href="#cb1-19" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_coll_types <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-20"><a href="#cb1-20" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-21"><a href="#cb1-21" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_cons_columns <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-22"><a href="#cb1-22" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-23"><a href="#cb1-23" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_constraints <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-24"><a href="#cb1-24" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-25"><a href="#cb1-25" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_db_links <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-26"><a href="#cb1-26" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-27"><a href="#cb1-27" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_dependencies <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-28"><a href="#cb1-28" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-29"><a href="#cb1-29" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_ind_columns <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-30"><a href="#cb1-30" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-31"><a href="#cb1-31" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_ind_expressions <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-32"><a href="#cb1-32" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-33"><a href="#cb1-33" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_indexes <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-34"><a href="#cb1-34" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-35"><a href="#cb1-35" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_mviews <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-36"><a href="#cb1-36" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-37"><a href="#cb1-37" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_object_tables <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-38"><a href="#cb1-38" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-39"><a href="#cb1-39" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_objects <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-40"><a href="#cb1-40" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-41"><a href="#cb1-41" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_procedures <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-42"><a href="#cb1-42" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-43"><a href="#cb1-43" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_sequences <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-44"><a href="#cb1-44" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-45"><a href="#cb1-45" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_source <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-46"><a href="#cb1-46" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-47"><a href="#cb1-47" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_synonyms <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-48"><a href="#cb1-48" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-49"><a href="#cb1-49" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_tab_columns <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-50"><a href="#cb1-50" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-51"><a href="#cb1-51" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_tables <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-52"><a href="#cb1-52" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-53"><a href="#cb1-53" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_triggers <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-54"><a href="#cb1-54" aria-hidden="true" tabindex="-1"></a><span class="op">/</span>  </span>
<span id="cb1-55"><a href="#cb1-55" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_types <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-56"><a href="#cb1-56" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span>
<span id="cb1-57"><a href="#cb1-57" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_users <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-58"><a href="#cb1-58" aria-hidden="true" tabindex="-1"></a><span class="op">/</span> </span>
<span id="cb1-59"><a href="#cb1-59" aria-hidden="true" tabindex="-1"></a><span class="kw">grant</span> <span class="kw">select</span> <span class="kw">on</span> dba_views <span class="kw">to</span> USER_FOR_EXTRACTION</span>
<span id="cb1-60"><a href="#cb1-60" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span></code></pre></div>
</div>
</div>
<h2
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-NoteabouttheDBA_USERSviewandthededicatedextractionuser">Note
about the DBA_USERS view and the dedicated extraction user</h2>
<h3
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-Situation">Situation</h3>
<ul>
<li>If you are using the <strong>dedicated extraction user</strong> (as
recommended and outlined above) to perform your extraction, a view
called <strong>DBA_USERS</strong> is queried to gain access to the data
required for extraction</li>
<li>If you do not want to grant the select right on this view for
security reasons, you can change the script above to use a synonym
instead of the a direct grant on the <strong>DBA_USERS</strong>
view</li>
</ul>
<h3
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-Action">Action</h3>
<ul>
<li>Comment the following lines in the above script as follows:</li>
</ul>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: sql; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: sql; gutter: false; theme: Confluence"><pre
class="sourceCode sql"><code class="sourceCode sql"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="co">-- grant select on dba_users to USER_FOR_EXTRACTION</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="co">-- /</span></span></code></pre></div>
</div>
</div>
<ul>
<li>Add two new lines to the script as follows:</li>
</ul>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: sql; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: sql; gutter: false; theme: Confluence"><pre
class="sourceCode sql"><code class="sourceCode sql"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="kw">create</span> <span class="kw">synonym</span> USER_FOR_EXTRACTION.DBA_USERS <span class="cf">for</span> SYS.ALL_USERS</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="op">/</span></span></code></pre></div>
</div>
</div>
<ul>
<li>Re-run the script as the SYS user</li>
<li>Use the dedicated extraction user in any future extractions</li>
</ul>
<h3
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-Impacts">Impacts</h3>
<ul>
<li>There are no impacts - results when using the synonym instead of the
grant select on the DBA_USERS view are identical.</li>
</ul>
<h2
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-NoteaboutDBA_MVIEW_COMMENTS/ALL_MVIEW_COMMENTSviewsandOracle9.x">Note
about DBA_MVIEW_COMMENTS / ALL_MVIEW_COMMENTS views and Oracle 9.x</h2>
<p>The views DBA_MVIEW_COMMENTS / ALL_MVIEW_COMMENTS do not exist on
Oracle 9.x, therefore when running an extraction on Oracle 9.x in
whatever mode (extraction user/SYSTEM/other Oracle user), the extractor
will use DBA_TAB_COMMENTS to extract comments on materialized views
instead. Results are not impacted. This will typically be displayed in
the log as follows:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: java; gutter: true; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: true; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a>Unable to use access mode<span class="op">:</span> Extracting comments on materialized views using DBA_MVIEW_COMMENTS</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>No access to<span class="op">:</span> DBA_MVIEW_COMMENTS</span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>Unable to use access mode<span class="op">:</span> Extracting comments on materialized views using ALL_MVIEW_COMMENTS<span class="op">.</span> <span class="fu">Using</span> ALL_MVIEW_COMMENTS has no impact on results<span class="op">,</span> including AFP<span class="op">.</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="fu">No</span> access to<span class="op">:</span> ALL_MVIEW_COMMENTS</span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a>Using access mode<span class="op">:</span> Extracting comments on materialized views using DBA_TAB_COMMENTS on Oracle <span class="dv">9</span>i</span></code></pre></div>
</div>
</div>
<h2
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-NoteabouttheDBA_ARGUMENTSviewandthededicatedextractionuser">Note
about the DBA_ARGUMENTS view and the dedicated extraction user</h2>
<h3
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-Situation.1">Situation</h3>
<ul>
<li>If you are using the <strong>dedicated extraction user</strong> (as
recommended and outlined above) to perform your extraction, a view
called <strong>DBA_ARGUMENTS</strong> is queried to gain access to the
data required for extraction</li>
<li>On some Oracle Servers (Oracle 10.2.0.4.0 or any earlier Oracle 10
version and all Oracle 9 versions) this view is <strong>not present by
default</strong>, therefore the following is true:</li>
</ul>
<h3
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-Impacts.1">Impacts</h3>
<p>The extraction will succeed, but:</p>
<ul>
<li>Oracle extractor log contains ‘Extraction error: ORA-00942: table or
view does not exist’ in section ‘Extracting: Oracle wrapped valid
functions...’ ; and:</li>
<li>Extraction will be incomplete - i.e. the following information will
be missing:
<ul>
<li><p>IN/OUT parameters will be missing for all procedures and
functions outside of the user’s own schema, unless the user has the
EXECUTE privilege on those procedures and functions. The same is true
for the return code of functions. This will impact the analysis and
Quality Rules based on datatypes of functions/procedures/parameters or
the return code of functions.</p></li>
<li><p>IN/OUT parameters will be missing from the source code generated
for wrapped procedures or functions, therefore they will not be visible
in CAST Enlighten or in the CAST Engineering Dashboard.</p></li>
<li><p>Return code will be missing from the source code generated for
wrapped functions, therefore they will not be visible in CAST Enlighten
or in the CAST Engineering Dashboard.</p></li>
</ul></li>
</ul>
<div>
<div>
Note that there is no impact on the source code for unwrapped
function/procedures because this code is not generated and is instead
extracted as is.
</div>
</div>
<h3
id="SQLRequiredRDBMSrightsforpackagingadatabaseusingthelegacyCASTDeliveryManagerTool-Options">Options</h3>
<p>Two options exist if you find yourself in this situation:</p>
<ol>
<li>Manually create the <strong>DBA_ARGUMENTS</strong> view on the
Oracle server prior to running the extraction. To do so, please use this
<a href="248815210.sql"
data-linked-resource-id="248815210" data-linked-resource-version="2"
data-linked-resource-type="attachment"
data-linked-resource-default-alias="DBA_ARGUMENTS.sql"
data-linked-resource-content-type="text/x-sql"
data-linked-resource-container-id="248815209"
data-linked-resource-container-version="6">script</a>. Note that the
script must be run by the <strong>Oracle SYS user</strong>. When the
view has been created, you can perform an extraction and analysis
results will be correct.</li>
<li><p>If it is not possible to create the DBA_ARGUMENTS view:</p>
<ol>
<li><p>then if the "<strong>ALL Access</strong>" mode is unchecked the
extraction will not be possible and will fail</p></li>
<li><p>then if the "<strong>ALL Access</strong>" mode is checked the
extraction will be possible but you must accept that the extraction
results will be incomplete.</p></li>
</ol></li>
</ol>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>SYSTEM</strong></td>
<td class="confluenceTd"><p>When it is not possible to use the
<strong>dedicated extraction user</strong>, CAST recommends using the
<strong>SYSTEM</strong> user instead. CAST will query the <strong>DBA_*
views</strong> to access the required data. These views give access to
ALL objects.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Other Oracle users</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>When it is not possible to use the <strong>dedicated extraction
user</strong> or the <strong>SYSTEM</strong> user as outlined above, it
is possible to use any other user (i.e. non-DBA user) to run your
extraction - for example: the <strong>schema owner user</strong>.
However, there are several drawbacks to doing this and <strong>CAST
therefore does not recommend using this type of user</strong>:</p>
<ul>
<li>CAST will query the <strong>ALL_* views</strong> to access the
required data -  these views only give access to objects that the user
is entitled to access. This means that CAST <strong>cannot
guarantee</strong> that the extraction will contain all the required
data.</li>
<li>Performance of the extraction will be reduced.</li>
</ul>
<div>
<div>
<p>Note also that in order for the <strong>ALL_* views</strong> to be
queried, the <strong>ALL access mode</strong> option must be explicitly
selected in the CAST Delivery Manager Tool:</p>
<p><img src="248815216.jpg" draggable="false"
data-image-src="248815216.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="248815216"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="all_access.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="248815209"
data-linked-resource-container-version="6" width="852"
height="142" /></p>
<p>This option (<strong>when selected</strong>) explicitly allows the
CAST Delivery Manager Tool to query the <strong>ALL_* views</strong> to
access the required data -  these views only give access to objects that
the user is entitled to access. This means that CAST <strong>cannot
guarantee</strong> that the extraction will contain all the required
data. In addition, performance of the extraction will be reduced.</p>
<p>By default this option is <strong>not selected</strong>, which
automatically prevents the CAST Delivery Manager Tool from using the
<strong>ALL_* views</strong> to access the required data. In other
words, if the user name that you have entered into the "User Name" field
above does not have sufficient rights to query the <strong>DBA_*
views</strong> then the CAST Delivery Manager Tool will attempt to query
the <strong>ALL_* views</strong> and if the <strong>ALL access
mode</strong> option is not selected, then the extraction will fail.</p>
</div>
</div>
<div>
<div>
Please avoid using the SYS user to perform extractions. Results cannot
be guaranteed.
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Microsoft SQL Server

![](248815212.jpg)

In order to carry out an extraction of the required databases, the
person in charge of the extraction (Delivery Manager or DBA) must use a
Microsoft SQL Server login (whether using Windows or SQL
authentication) that has the following roles and permissions:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">User</th>
<th class="confluenceTh">Required roles and permissions</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><strong>Any user with the following permissions
and roles<br />
</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>"<strong>public</strong>" Server Role</li>
<li>"<strong>db-datareader</strong>" Database Role on all databases that
require extraction (i.e. the <strong>login</strong> is mapped as a
<strong>User</strong> on the target databases and is given the
"db-datareader" Database Role)</li>
<li>"<strong>View definition</strong>" + "<strong>Grant</strong>"
explicit permission on all databases that require extraction (i.e. the
explicit permission is given to the <strong>User</strong>)</li>
</ul>
<p>An example script to assign the required role and permissions is
shown below:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: sql; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: sql; gutter: false; theme: Confluence"><pre
class="sourceCode sql"><code class="sourceCode sql"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="co">-- Create an SQL Server login</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="kw">CREATE</span> LOGIN <span class="op">&lt;</span>login<span class="op">&gt;</span> <span class="kw">WITH</span> <span class="kw">PASSWORD</span> <span class="op">=</span> <span class="st">&#39;&lt;password&gt;&#39;</span>;</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a><span class="co">-- Create a database user (on all databases that require extraction) for the login created above</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="co">-- this will also automatically assign the CONNECT + GRANT explicit permission</span></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a><span class="kw">Use</span> <span class="op">&lt;</span><span class="kw">database</span><span class="op">&gt;</span></span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a><span class="kw">CREATE</span> <span class="fu">USER</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span> <span class="cf">FOR</span> LOGIN <span class="op">&lt;</span>login<span class="op">&gt;</span>;</span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a><span class="co">-- Assign the db_datareader database role to the user created above for all databases that require extraction</span></span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a><span class="kw">Use</span> <span class="op">&lt;</span><span class="kw">database</span><span class="op">&gt;</span></span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb1-13"><a href="#cb1-13" aria-hidden="true" tabindex="-1"></a><span class="kw">exec</span> sp_addrolemember <span class="st">&#39;db_datareader&#39;</span>, <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb1-14"><a href="#cb1-14" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb1-15"><a href="#cb1-15" aria-hidden="true" tabindex="-1"></a><span class="co">-- Assign your user the View definition + GRANT explicit permission on all databases that require extraction</span></span>
<span id="cb1-16"><a href="#cb1-16" aria-hidden="true" tabindex="-1"></a><span class="kw">Use</span> <span class="op">&lt;</span><span class="kw">database</span><span class="op">&gt;</span></span>
<span id="cb1-17"><a href="#cb1-17" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb1-18"><a href="#cb1-18" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">View</span> definition <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb1-19"><a href="#cb1-19" aria-hidden="true" tabindex="-1"></a>GO</span></code></pre></div>
</div>
</div>
<p>The following screenshots show the same changes performed in the
Microsoft SQL Server GUI:</p>
<ul>
<li>A login (whether using Windows or SQL authentication) must be
defined in the SQL Server. This login requires NO "Server Roles" at all,
apart from "public" which is granted automatically when the login is
created:</li>
</ul>
<p><img src="248815213.jpg" draggable="false"
data-image-src="248815213.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="248815213"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="MS1.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="248815209"
data-linked-resource-container-version="6" /></p>
<ul>
<li>Under "User Mapping" (login properties), the login must be assigned
the "db_datareader" database role for for all databases that require
extraction:</li>
</ul>
<p><img src="248815214.jpg" draggable="false"
data-image-src="248815214.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="248815214"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="MS2.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="248815209"
data-linked-resource-container-version="6" /></p>
<ul>
<li>Lastly in the Properties of all databases that require extraction,
assign your user the "View definition" + "Grant" explicit
permission:</li>
</ul>
<p><img src="248815211.jpg" draggable="false"
data-image-src="248815211.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="248815211"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="MS3.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="248815209"
data-linked-resource-container-version="6" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Extraction user</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>When it is not possible to grant a user the "<strong>public</strong>"
Server Role and the "<strong>db-datareader</strong>" Database Role as
outlined above, CAST recommends using a dedicated <strong>extraction
user</strong> with <strong>specific privileges</strong> in order to
access the required data for extraction.</p>
<p>An example script to assign the required role and permissions is
shown below:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: sql; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: sql; gutter: false; theme: Confluence"><pre
class="sourceCode sql"><code class="sourceCode sql"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="co">-- Create an SQL Server login</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="kw">CREATE</span> LOGIN <span class="op">&lt;</span>login<span class="op">&gt;</span> <span class="kw">WITH</span> <span class="kw">PASSWORD</span> <span class="op">=</span> <span class="st">&#39;&lt;password&gt;&#39;</span>;</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="co">-- Create a database user (on all databases that require extraction) for the login created above</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a><span class="co">-- this will also automatically assign the CONNECT + GRANT explicit permission</span></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a><span class="kw">Use</span> <span class="op">&lt;</span><span class="kw">database</span><span class="op">&gt;</span></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a><span class="kw">CREATE</span> <span class="fu">USER</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span> <span class="cf">FOR</span> LOGIN <span class="op">&lt;</span>login<span class="op">&gt;</span>;</span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-12"><a href="#cb2-12" aria-hidden="true" tabindex="-1"></a><span class="co">-- Issue grant on specific tables</span></span>
<span id="cb2-13"><a href="#cb2-13" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> <span class="kw">master</span>.dbo.sysdatabases <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-14"><a href="#cb2-14" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-15"><a href="#cb2-15" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> <span class="kw">master</span>.dbo.spt_values <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-16"><a href="#cb2-16" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-17"><a href="#cb2-17" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> <span class="kw">master</span>.dbo.syscharsets <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-18"><a href="#cb2-18" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-19"><a href="#cb2-19" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> <span class="kw">master</span>.dbo.syscurconfigs <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-20"><a href="#cb2-20" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-21"><a href="#cb2-21" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.databases <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-22"><a href="#cb2-22" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-23"><a href="#cb2-23" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.schemas <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-24"><a href="#cb2-24" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-25"><a href="#cb2-25" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.<span class="kw">columns</span> <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-26"><a href="#cb2-26" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-27"><a href="#cb2-27" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.<span class="kw">types</span> <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-28"><a href="#cb2-28" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-29"><a href="#cb2-29" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.foreign_keys <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-30"><a href="#cb2-30" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-31"><a href="#cb2-31" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.sysforeignkeys <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-32"><a href="#cb2-32" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-33"><a href="#cb2-33" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.<span class="kw">tables</span> <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-34"><a href="#cb2-34" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-35"><a href="#cb2-35" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.foreign_key_columns <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-36"><a href="#cb2-36" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-37"><a href="#cb2-37" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.views <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-38"><a href="#cb2-38" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-39"><a href="#cb2-39" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.procedures <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-40"><a href="#cb2-40" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-41"><a href="#cb2-41" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.numbered_procedures <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-42"><a href="#cb2-42" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-43"><a href="#cb2-43" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.objects <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-44"><a href="#cb2-44" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-45"><a href="#cb2-45" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.trigger_events <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-46"><a href="#cb2-46" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-47"><a href="#cb2-47" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> sys.<span class="kw">triggers</span> <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-48"><a href="#cb2-48" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-49"><a href="#cb2-49" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> dbo.sysobjects <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-50"><a href="#cb2-50" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-51"><a href="#cb2-51" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> dbo.sysusers <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-52"><a href="#cb2-52" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-53"><a href="#cb2-53" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> dbo.systypes <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-54"><a href="#cb2-54" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-55"><a href="#cb2-55" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> dbo.sysforeignkeys <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-56"><a href="#cb2-56" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-57"><a href="#cb2-57" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">SELECT</span> <span class="kw">ON</span> dbo.syscomments <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-58"><a href="#cb2-58" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-59"><a href="#cb2-59" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-60"><a href="#cb2-60" aria-hidden="true" tabindex="-1"></a><span class="co">--Issue grant select to extraction user on databases that require extraction</span></span>
<span id="cb2-61"><a href="#cb2-61" aria-hidden="true" tabindex="-1"></a><span class="kw">use</span> <span class="op">&lt;</span><span class="kw">database</span><span class="op">&gt;</span></span>
<span id="cb2-62"><a href="#cb2-62" aria-hidden="true" tabindex="-1"></a>GO</span>
<span id="cb2-63"><a href="#cb2-63" aria-hidden="true" tabindex="-1"></a><span class="kw">GRANT</span> <span class="kw">VIEW</span> <span class="kw">ANY</span> DEFINITION <span class="kw">TO</span> <span class="op">&lt;</span><span class="fu">user</span><span class="op">&gt;</span></span>
<span id="cb2-64"><a href="#cb2-64" aria-hidden="true" tabindex="-1"></a>GO</span></code></pre></div>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## Sybase ASE

In order to carry out an extraction of the required databases, the
person in charge of the extraction (Delivery Manager or DBA) must use a
Sybase ASE login that has the following roles and permissions:

-   CONNECT role
-   SELECT permission on the following tables:
    -   master.dbo.sysdatabases
    -   master.dbo.spt_values
    -   master.dbo.syscurconfigs
-   For each target database you want to extract, the SELECT
    permission is required on the following tables:
    -   \[%SQLdatabase%\].dbo.sysusers 
    -   \[%SQLdatabase%\].dbo.sysconstraints 
    -   \[%SQLdatabase%\].dbo.sysreferences 
    -   \[%SQLdatabase%\].dbo.sysobjects 
    -   \[%SQLdatabase%\].dbo.syscolumns 
    -   \[%SQLdatabase%\].dbo.sysindexes 
    -   \[%SQLdatabase%\].dbo.syscomments 
    -   \[%SQLdatabase%\].dbo.systypes
