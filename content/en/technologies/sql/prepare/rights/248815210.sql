create or replace Noforce VIEW SYS.DBA_ARGUMENTS
(
	OWNER,
	OBJECT_NAME,
	PACKAGE_NAME,
	OBJECT_ID,
	OVERLOAD,
	SUBPROGRAM_ID,
	ARGUMENT_NAME,
	POSITION,
	SEQUENCE,
	DATA_LEVEL,
	DATA_TYPE,
	DEFAULT_VALUE,
	DEFAULT_LENGTH,
	IN_OUT,
	DATA_LENGTH,
	DATA_PRECISION,
	DATA_SCALE,
	RADIX,
	CHARACTER_SET_NAME,
	TYPE_OWNER,
	TYPE_NAME,
	TYPE_SUBNAME,
	TYPE_LINK,
	PLS_TYPE,
	CHAR_LENGTH,
	CHAR_USED
)
As 
select                               
/* VIEW ADDED FOR CAST USAGE */
u.name, /* OWNER */
nvl(a.procedure$,o.name), /* OBJECT_NAME */
decode(a.procedure$,null,null, o.name), /* PACKAGE_NAME */
o.obj#, /* OBJECT_ID */
decode(a.overload#,0,null,a.overload#), /* OVERLOAD */
a.procedure#, /* SUBPROGRAM ID */
a.argument, /* ARGUMENT_NAME */
a.position#, /* POSITION */
a.sequence#, /* SEQUENCE */
a.level#, /* DATA_LEVEL */
decode(a.type#,  /* DATA_TYPE */
0, null,
1, decode(a.charsetform, 2, 'NVARCHAR2', 'VARCHAR2'),
2, decode(a.scale, -127, 'FLOAT', 'NUMBER'),
3, 'NATIVE INTEGER',
8, 'LONG',
9, decode(a.charsetform, 2, 'NCHAR VARYING', 'VARCHAR'),
11, 'ROWID',
12, 'DATE',
23, 'RAW',
24, 'LONG RAW',
29, 'BINARY_INTEGER',
69, 'ROWID',
96, decode(a.charsetform, 2, 'NCHAR', 'CHAR'),
100, 'BINARY_FLOAT',
101, 'BINARY_DOUBLE',
102, 'REF CURSOR',
104, 'UROWID',
105, 'MLSLABEL',
106, 'MLSLABEL',
110, 'REF',
111, 'REF',
112, decode(a.charsetform, 2, 'NCLOB', 'CLOB'),
113, 'BLOB', 114, 'BFILE', 115, 'CFILE',
121, 'OBJECT',
122, 'TABLE',
123, 'VARRAY',
178, 'TIME',
179, 'TIME WITH TIME ZONE',
180, 'TIMESTAMP',
181, 'TIMESTAMP WITH TIME ZONE',
231, 'TIMESTAMP WITH LOCAL TIME ZONE',
182, 'INTERVAL YEAR TO MONTH',
183, 'INTERVAL DAY TO SECOND',
250, 'PL/SQL RECORD',
251, 'PL/SQL TABLE',
252, 'PL/SQL BOOLEAN',
'UNDEFINED'),
default$, /* DEFAULT_VALUE */
deflength, /* DEFAULT_LENGTH */
decode(in_out,null,'IN',1,'OUT',2,'IN/OUT','Undefined'), /* IN_OUT */
length, /* DATA_LENGTH */
precision#, /* DATA_PRECISION */
decode(a.type#, 2, scale, 1, null, 96, null, scale), /* DATA_SCALE */
radix, /* RADIX */
decode(a.charsetform, 1, 'CHAR_CS',           /* CHARACTER_SET_NAME */
                      2, 'NCHAR_CS',
                      3, NLS_CHARSET_NAME(a.charsetid),
                      4, 'ARG:'||a.charsetid),
a.type_owner, /* TYPE_OWNER */
a.type_name, /* TYPE_NAME */
a.type_subname, /* TYPE_SUBNAME */
a.type_linkname, /* TYPE_LINK */
a.pls_type, /* PLS_TYPE */
decode(a.type#, 1, a.scale, 96, a.scale, 0), /* CHAR_LENGTH */
decode(a.type#,
        1, decode(bitand(a.properties, 128), 128, 'C', 'B'),
       96, decode(bitand(a.properties, 128), 128, 'C', 'B'), 0) /* CHAR_USED */
from obj$ o,argument$ a,user$ u
where o.obj# = a.obj#
and o.owner# = u.user#

/
COMMENT ON COLUMN DBA_ARGUMENTS.OBJECT_NAME
	IS '-- Procedure or function name'
/
COMMENT ON COLUMN DBA_ARGUMENTS.PACKAGE_NAME
	IS '-- Package name'
/
COMMENT ON COLUMN DBA_ARGUMENTS.OBJECT_ID
	IS '-- Object number of the object'
/
COMMENT ON COLUMN DBA_ARGUMENTS.OVERLOAD
	IS '-- Overload unique identifier'
/
COMMENT ON COLUMN DBA_ARGUMENTS.SUBPROGRAM_ID
	IS '-- Unique sub-program Identifier'
/
COMMENT ON COLUMN DBA_ARGUMENTS.ARGUMENT_NAME
	IS '-- Argument name'
/
COMMENT ON COLUMN DBA_ARGUMENTS.POSITION
	IS '-- Position in argument list, or null for function return value'
/
COMMENT ON COLUMN DBA_ARGUMENTS.SEQUENCE
	IS '-- Argument sequence, including all nesting levels'
/
COMMENT ON COLUMN DBA_ARGUMENTS.DATA_LEVEL
	IS '-- Nesting depth of argument for composite types'
/
COMMENT ON COLUMN DBA_ARGUMENTS.DATA_TYPE
	IS '-- Datatype of the argument'
/
COMMENT ON COLUMN DBA_ARGUMENTS.DEFAULT_VALUE
	IS '-- Default value for the argument'
/
COMMENT ON COLUMN DBA_ARGUMENTS.DEFAULT_LENGTH
	IS '-- Length of default value for the argument'
/
COMMENT ON COLUMN DBA_ARGUMENTS.IN_OUT
	IS '-- Argument direction (IN, OUT, or IN/OUT)'
/
COMMENT ON COLUMN DBA_ARGUMENTS.DATA_LENGTH
	IS '-- Length of the column in bytes'
/
COMMENT ON COLUMN DBA_ARGUMENTS.DATA_PRECISION
	IS '-- Length: decimal digits (NUMBER) or binary digits (FLOAT)'
/
COMMENT ON COLUMN DBA_ARGUMENTS.DATA_SCALE
	IS '-- Digits to right of decimal point in a number'
/
COMMENT ON COLUMN DBA_ARGUMENTS.RADIX
	IS '-- Argument radix for a number'
/
COMMENT ON COLUMN DBA_ARGUMENTS.CHARACTER_SET_NAME
	IS '-- Character set name for the argument'
/
COMMENT ON COLUMN DBA_ARGUMENTS.TYPE_OWNER
	IS '-- Owner name for the argument type in case of object types'
/
COMMENT ON COLUMN DBA_ARGUMENTS.TYPE_NAME
	IS '-- Object name for the argument type in case of object types'
/
COMMENT ON COLUMN DBA_ARGUMENTS.TYPE_SUBNAME
	IS '-- Subordinate object name for the argument type in case of object types'
/
COMMENT ON COLUMN DBA_ARGUMENTS.TYPE_LINK
	IS '-- Database link name for the argument type in case of object types'
/
COMMENT ON COLUMN DBA_ARGUMENTS.PLS_TYPE
	IS '-- PL/SQL type name for numeric arguments'
/
COMMENT ON COLUMN DBA_ARGUMENTS.CHAR_LENGTH
	IS '-- Character limit for string datatypes'
/
COMMENT ON COLUMN DBA_ARGUMENTS.CHAR_USED
	IS '-- Is the byte limit (B) or char limit (C) official for this string?'
/

COMMENT ON TABLE SYS.DBA_ARGUMENTS IS 'All arguments for objects in the database'
/
CREATE PUBLIC SYNONYM DBA_ARGUMENTS FOR SYS.DBA_ARGUMENTS  
/
grant select on dba_arguments to USER_FOR_EXTRACTION
/  