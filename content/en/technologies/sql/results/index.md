---
title: "Analysis results"
linkTitle: "Analysis results"
type: "docs"
weight: 7
---

## What results can you expect?

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Package.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ANSISQL_Domain | ANSI SQL Domain |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataModuleExternal.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ANSISQL_ExternalObject | Unknown SQL object |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ANSISQL_Owner | ANSI SQL Object Owner |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_AST_ANSISQL_AnonymousDatatype | Oracle datatype |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionExternal.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SQL_UnresolvedFunction | SQL unresolved procedure | Database Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptFunction | Function | Database Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptMethod | Method | Database Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionExternal.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptUnresolvedFunction | Unresolved Function | Database Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptMacro | Macro | Macro |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcedureGrey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SQL_UnresolvedProcedure | SQL unresolved procedure | Procedure |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcedureGrey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScript_Missing_Procedure | SQL Missing Procedure | Procedure |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Procedure.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptProcedure | Procedure | Procedure |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnknownSchema.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SQL_UnresolvedSchema | SQL unresolved Schema | Schema |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnknownSchema.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScript_Missing_Schema | SQL Missing Schema | Schema |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Schema.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptSchema | Schema | Schema |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnknownSchema.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptUnresolvedSchema | Unresolved Schema | Schema |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnknownDatabaseConnection.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SQL_UnresolvedServer | SQL unresolved server | Storage Connection |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataLink.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptDatabaseLink | Database Link | Storage Connection |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnresolvedTable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SQL_UnresolvedTableSource | SQL unresolved table source | Table |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnresolvedTable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScript_Missing_Table | SQL Missing Table | Table |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Table.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptTable | Table | Table |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnresolvedTable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptUnresolvedTable | Unresolved Table | Table |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/TableColumn.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ANSISQL_UnresolvedColumn | ANSI SQL unresolved column | Table Column |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/TableColumn.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptTableColumn | Table Column | Table Column |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Trigger.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptTrigger | Trigger | Trigger |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Variable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SQLScriptType | Type | Variable |

## Legacy Results

>This section provides information about the analysis
results you can expect from an analysis of SQL technologies using the
legacy PL/SQL and T-SQL analyzers embedded in CAST Imaging Core.

### Microsoft SQL Server and Sybase ASE T-SQL

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802379.png" draggable="false"
data-image-src="229802379.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802379"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_Instance.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" /></p>
</div></td>
<td class="confluenceTd">Instance</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802380.png" draggable="false"
data-image-src="229802380.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802380"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_Database.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Database</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802381.png" draggable="false"
data-image-src="229802381.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802381"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_Schema.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Schema</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802382.png" draggable="false"
data-image-src="229802382.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802382"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_DML_Trigger.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Trigger</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802383.png" draggable="false"
data-image-src="229802383.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802383"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_Function.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Function</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802384.png" draggable="false"
data-image-src="229802384.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802384"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_Procedure.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Procedure</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802385.png" draggable="false"
data-image-src="229802385.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802385"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_RelationalTable.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Table</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802386.png" draggable="false"
data-image-src="229802386.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802386"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_View.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">View</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802387.png" draggable="false"
data-image-src="229802387.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802387"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_TableColumn.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Table Column / View Column</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="229802388.png" draggable="false"
data-image-src="229802388.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802388"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_MSTSQL_ForeignKeyConstraint.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" />
</div></td>
<td class="confluenceTd">Index, Foreign Key Constraint, Primary Key
Constraint, Unique Constraint</td>
</tr>
</tbody>
</table>

#### Links

Links detected by the SQL Analyzer are listed in the attached Excel
file:

-   [TSQLAnalyzerLinks.xls](229802413.xls)

### Oracle PL/SQL

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802389.png" draggable="false"
data-image-src="229802389.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802389"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav978436fe65c5daebb736da5ba78146b6.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Collection Nested Table Type</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802390.png" draggable="false"
data-image-src="229802390.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802390"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav28e0c8d93a1a7da1ef8167a847b5c383.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Collection Varray</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802391.png" draggable="false"
data-image-src="229802391.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802391"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddavc255b05910293011f87b15cb0cb05af1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Cursor</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802392.png" draggable="false"
data-image-src="229802392.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802392"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav1a3fc362b54a33715ec7b8f3c83bfb38.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Database Link</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802393.png" draggable="false"
data-image-src="229802393.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802393"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddave6ef1bbc8e47d48eebdb735f3fd83ed9.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Function</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802394.png" draggable="false"
data-image-src="229802394.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802394"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddavf7152fcc1e8e6eddb25ce98cc61cad17.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Function (Private)</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802395.png" draggable="false"
data-image-src="229802395.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802395"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav1fdbbd38cbd3af85df71965c79a6f1c3.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="30"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Instance</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802396.png" draggable="false"
data-image-src="229802396.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802396"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav731b8f2e321ad02434a46a0d99bc24e8.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Index, Foreign Key Constraint, Primary Key
Constraint, Check Constraint and Unique Key Constraint</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802397.png" draggable="false"
data-image-src="229802397.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802397"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav08888c3c8fd98ecabe0160074c09033b.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Object Type (Incomplete, Body and
Header)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802398.png" draggable="false"
data-image-src="229802398.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802398"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddave1157a21839f656aaa76a4bd6d66e7dd.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Package Body</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802399.png" draggable="false"
data-image-src="229802399.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802399"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav3435e4b634a0df5f8f1deeb694d5b5ff.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Package Header</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802400.png" draggable="false"
data-image-src="229802400.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802400"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav92a9c4b9982d044311d51d10417028bd.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Procedure</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802401.png" draggable="false"
data-image-src="229802401.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802401"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddave35082da76aced86e5f088e61562d11b.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Procedure (Private)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802402.png" draggable="false"
data-image-src="229802402.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802402"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav3eed27922aefab0258e021b64c220d0f.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Schema</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802403.png" draggable="false"
data-image-src="229802403.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802403"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddavbbbbbc7aab237fa65ccefce262ee7748.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Schema (Unknown)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802404.png" draggable="false"
data-image-src="229802404.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802404"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddavfb10838fbf1b53b7a42d85ce8f84a434.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Sequence</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802405.png" draggable="false"
data-image-src="229802405.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802405"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav685ab3d3d2d2684707dceafc02ca916b.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Synonym (Public/Private)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802389.png" draggable="false"
data-image-src="229802389.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802389"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav978436fe65c5daebb736da5ba78146b6.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Table</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802406.png" draggable="false"
data-image-src="229802406.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802406"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav05a11a67506067c02e1781fd7d0787aa.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Table Column</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802407.png" draggable="false"
data-image-src="229802407.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802407"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav0da1bdbd837ccb0c094885658c3963c0.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>Trigger</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802408.png" draggable="false"
data-image-src="229802408.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802408"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddavb2c4390c84af79a1982a1cde69545a6a.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>View (Normal and Materialized)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="229802406.png" draggable="false"
data-image-src="229802406.png"
data-unresolved-comment-count="0" data-linked-resource-id="229802406"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav05a11a67506067c02e1781fd7d0787aa.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="229802364"
data-linked-resource-container-version="13" width="25"
height="25" /></p>
</div></td>
<td class="confluenceTd"><p>View Column</p></td>
</tr>
</tbody>
</table>

Object properties that are supported by the current release of the
embedded SQL analyzer are described in the attached Excel file:

-   [SQLAnalyzerObjectProperties.xls](229802410.xls)

#### Links

Links detected by the embedded SQL analyzer are listed in the attached
Excel file:

-   [SQLAnalyzerLinks.xls](229802411.xls)