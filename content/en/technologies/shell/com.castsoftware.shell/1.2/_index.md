---
title: "Shell - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.shell

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications written using UNIX
Shell languages.

Although this extension is officially supported by CAST, please note
that it has been developed within the technical constraints of the CAST
Universal Analyzer technology and to some extent adapted to meet
specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other CAST AIP analyzers.

## In what situation should you install this extension?

If your application contains source code written using UNIX Shells and
you want to view these object types and their links with other objects,
then you should install this extension.

## Supported UNIX shells

This version of the extension provides partial support for:

| UNIX shell | Supported |
|---|:-:|
| Bourne shell (bsh/sh/shell) | :white_check_mark: |
| Bourne-Again Shell (bash)   | :white_check_mark: |
| C shell (csh)               | :white_check_mark: |
| KornShell (ksh)             | :white_check_mark: |
| Secure Shell (ssh)          | :white_check_mark: |
| Tenex C Shell (tcsh)        | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :x: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed when you deliver Shell code (i.e. any file with an extension listed below).

## Prepare and deliver the source code

Once the extension is downloaded and installed, you can now package your
source code and run an analysis. The process of preparing and delivering
your source code is described below:

### Source code preparation

Only files with following extensions will be analyzed:

-   \*.bash
-   \*.bsh
-   \*.csh
-   \*.ksh
-   \*.sh
-   \*.shell
-   \*.ssh
-   \*.tsch

If any files intended for other applications are included in the
delivery and which are renamed to supported Shell extensions, the
following effects might be observed during an analysis:

-   inconsistent objects may get created
-   end of string ''' not found errors
-   if the file is binary: Invalid UTF-8 sequence found in text to be
    matched or searched for a regular expression

### Source code preprocessing 

Shell source code needs to be preprocessed so that CAST can understand
it and analyze it correctly. This code preprocessing is actioned
automatically when an analysis is launched or a snapshot is generated
(the code is preprocessed before the analysis starts). The Shell
Preprocessor log file is stored in the following location:

``` java
%PROGRAMDATA%\CAST\CAST\Logs\<application_name>\Execute_Analysis_<guid>\com.castsoftware.shell.<_extension_version>.prepro_YYYYMMDDHHMMSS.log
```

{{% alert color="info" %}}Note that the LISA folder will be used to analyze the preprocessed files - this is in contrast to standard behaviour for most other analyzers, where source code is analyzed directly from the Delivery folder. Due to this, if you use [User Defined Modules based on subfolders](../../../../interface/analysis-config/config/modules/udm/#module-per-subfolder) then you will need to ensure that you configure your paths based on the LISA folder.{{% /alert %}}

## Analysis configuration and execution

There are no analysis/technology configuration options available for
Shell, however you should check that at least one Shell analysis unit
has been created as shown below.

AIP Console exposes the technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Universal Technology (3) in the Config (1) \> Analysis
(2) tab to display the available options for your Shell source code:

![](../images/576750006.jpg)

Then choose the relevant Analysis Unit (1) to view the
configuration:

![](../images/576750009.jpg)

![](../images/576750010.jpg)

### Logging mechanism

#### Analysis log files

Analysis logs are stored in the default locations.

#### Shell Preprocessor

Shell Preprocessor log files (the preprocessor is launched automatically
during an analysis) are stored in the following locations:

| Location | Log file name |
|---|---|
| %PROGRAMDATA%\CAST\CAST\Logs\\unique_application_id\>\\ | com.castsoftware.shell.prepro\_\<ExtensionVersion\>\_\<YYYYMMDDHHMMSS\>.log |

## What results can you expect?

### Objects

<table class="wrapped confluenceTable" style="width: 697.0px;">
<tbody>
<tr class="header">
<th class="confluenceTh" style="width: 57.0px">Icon</th>
<th class="confluenceTh" style="width: 640.0px">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762539.png" draggable="false"
data-image-src="../images/639762539.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762539"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLProject.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Project</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762538.png" draggable="false"
data-image-src="../images/639762538.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762538"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLProgram.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Program</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762537.png" draggable="false"
data-image-src="../images/639762537.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762537"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLFunction.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell Function</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762537.png" draggable="false"
data-image-src="../images/639762499.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762499"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLVariable.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">SHELL Special Function</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762535.png" draggable="false"
data-image-src="../images/639762535.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762535"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLJavaCall.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Call to Java
Program</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762536.png" draggable="false"
data-image-src="../images/639762536.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762536"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLGeneralCall.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Call to a
Program</td>
</tr>
</tbody>
</table>

### Links

| Source Object                           | Link Type                          | Target Object                   |
|-------------------------------|---------------------|---------------------|
| Shell Program                           | callProgLink                       | Shell Program                   |
| Shell Program                           | include                            | Shell Program                   |
| Shell Program                           | callLink                           | Shell Function/Special Function |
| Shell Function/Special Function         | callLink                           | Shell Program                   |
| Shell Function/Special Function         | callLink                           | Shell Function/Special Function |
| Shell Function/Special Function/Program | use select, update, insert, delete | data functions                  |
| Shell Function/Special Function/Program | callLink                           | Shell Call to Java Program      |
| Shell Function/Special Function/Program | callLink                           | Shell Call to Generic Program   |
| Shell Function/Special Function/Program | callLink                           | SQL Table                       |
| Shell Function/Special Function/Program | callLink                           | SQL Script                      |

#### Embedded SQL

Shell allows the use of here-document to write SQL queries, for example:

``` bash
##!/bin/sh
/opt/solid/bin/solsql db db <<abc
select count(*) from table_1;
select count(*) from table_2;
exit;
!;
abc
```

The Shell extension can use these queries to create links.

#### Call to SQL script

The Shell extension allows links to SQL script when used with a
dependency to the SQL analyzer

For example:

script.sh

``` bash
##!/bin/env bash

sqlplus script.sql
```

script.sql

``` sql
CREATE TABLE ...
```

In Enlighten

![](../images/576750005.png)

#### Links to external programs

The following call pattern are supported

-   ./exe
-   /path/to/exe
-   python
-   java
-   nohup
-   eval
-   wlst.sh

#### Shell to Java links

The extension does support links between Shell programs and Java objects
(for example methods). Links will be created between these technologies.

The extension manages call to Java classes and .jar files. For .jar
files no links will go further as .jar aren't handle by any extensions,
the link is purely informative.

##### Basic Case

``` bash
...
/bin/java UpdateRepartiteurs
...
```

wil generate the following diagram

![](../images/576749979.png)

##### Function Case

``` bash
...
MajRepartiteurs ()
{
    ...
    java UpdateRepartiteurs
    ...
}
...
```

will generate the following diagram

![](../images/576749980.png)

#### Shell to COBOL links

The extension does support links between Shell programs and COBOL
objects (for example programs). Links will be created between these
technologies.

##### Basic Case

COBOL file

``` java
IDENTIFICATION DIVISION.
PROGRAM-ID. HELLO.
* simple hello world program
PROCEDURE DIVISION.
    DISPLAY 'Hello world!'.
```

SHELL script

``` bash
./hello
```

will generate the following diagram

![](../images/576749978.png)

#### Shell to Python links

The extension does support links between Shell programs and Python
objects (for example methods). Links will be created between these
technologies.

The following script

``` java
...
python foo.py
...
```

will generate the following diagram, assuming that foo.py exists and has
been analyzed

![](../images/576749977.png)

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 1.2.13 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.13 |
| 1.2.12 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.12 |
| 1.2.11 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.11 |
| 1.2.10 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.10 |
| 1.2.9 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.9 |
| 1.2.8 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.8 |
| 1.2.7 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.7 |
| 1.2.6 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.6 |
| 1.2.5 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.5 |
| 1.2.4 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.4 |
| 1.2.3 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.3 |
| 1.2.3-funcrel | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.3-funcrel |
| 1.2.2-funcrel | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.2-funcrel |
| 1.2.1-funcrel  | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.1-funcrel |
| 1.2.0-funcrel | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.0-funcrel |
| 1.2.0-beta1  | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.2.0-beta1 |

You can also find a global list
here: [https://technologies.castsoftware.com/rules?sec=t_1016000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1016000&ref=%7C%7C).


## Limitations/known issues

### Script file import

This extension does not support the "import" of functions defined in
other script files using "sourcing" 
(see [https://www.mkssoftware.com/docs/man1/dot.1.asp](https://www.mkssoftware.com/docs/man1/dot.1.asp "https://www.mkssoftware.com/docs/man1/dot.1.asp")),
therefore no links are expected between functions declared in other
script files Note that "sourcing" is supported when used in variable
definitions.

### Deployment folder path

The deployment folder path should contain only ASCII characters (due to
the way the command line text is passed in a Windows operating
system). Names of folders and files inside the deployment folder can
contain non-ASCII characters.

### Links to database objects

When your Shell scripts contain references to database objects in the
server side element of an application and these references use fully
qualified names for the database object, no links will be created
between Shell and the database objects when you have used the [SQL
Analyzer extension](SQL_Analyzer) to analyze the SQL if SQL Analyzer
does not create database objects with same name. If you have used
the SQL analyzers embedded in CAST AIP to analyze the SQL, then links
will be created as normal. This is a known issue.

### Metrics Assistant (embedded in CAST AIP) limitations

#### Searches not limited only to embedded SQL

The MA (Metric Assistant) which is used for metric search cannot search
only in embedded SQL. Some Shell rules may be affected by this
limitation and may produce false violations.

#### Cannot calculate metric excluding comments

The MA (Metric Assistant) which is used for metric search cannot search
correctly while excluding comments especially if comments start or end
adjacent to the keyword. If such a condition exists, random false
violations may occur.

### Shell embedded strings

Shell code allows string to be embedded in strings as shown in the code
sample below. Currently, the Shell extension (and other Universal
Analyzer type extensions) will consider this as one continuous string.
Because we do not have any way to identify perfect end string patterns
in this case, we cannot find the end of string and therefore the file
will be skipped during the analysis.

``` bash
echo '              
Outer string ;              
cat '$file03'             
Another outer string              
'$id'             
' | $command
```

### KSH: guessing of ending single\double quote

Note that this limitation is no longer applicable to Shell ≥ 1.0.10.

KSH supports the *guessing* of ending single/double quotes. The Shell
extension supports this when the string is in single line, however, it
is not supported when the string is in a multiple line, for example:

``` bash
export OUTPUT=`basename $SOME_VARIABLE | $AWK 'FS="-" {
  i=3
  tmpMachineName= "mach_"$2
  while ( i <= NF){
    tmpMachineName=tmpMachineName"-"$i;
    i++;
  }
  print(tmpMachineName);
}`
```

### Multi-line document markers

Note that this limitation is no longer applicable to Shell ≥ 1.0.10.

When a document marker is in a multi-line string, the Shell extension
will not be able detect that it is in a string, for example:

``` bash
some_multiline_string="a;b;c;d;\
e;f;g;h;\
x;y;z<<;strong text"
```

In both of these cases, the file will be skipped and logs will contain
the warning: "File Skipped".
