---
title: "Shell - 1.3"
linkTitle: "1.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.shell

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications written using UNIX
Shell languages.

Although this extension is officially supported by CAST, please note
that it has been developed within the technical constraints of the CAST
Universal Analyzer technology and to some extent adapted to meet
specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other CAST AIP analyzers.

## In what situation should you install this extension?

If your application contains source code written using UNIX Shells and
you want to view these object types and their links with other objects,
then you should install this extension.

## Supported UNIX shells

This version of the extension provides partial support for:

| UNIX shell | Supported |
|---|:-:|
| Bourne shell (bsh/sh/shell) | :white_check_mark: |
| Bourne-Again Shell (bash)   | :white_check_mark: |
| C shell (csh)               | :white_check_mark: |
| KornShell (ksh)             | :white_check_mark: |
| Secure Shell (ssh)          | :white_check_mark: |
| Tenex C Shell (tcsh)        | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :x: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed in Console when you deliver Shell code (i.e. any file with an extension listed below).

## Prepare and deliver the source code

Once the extension is downloaded and installed, you can now package your
source code and run an analysis. The process of preparing and delivering
your source code is described below:

### Source code preparation

Only files with following extensions will be analyzed:

-   \*.bash
-   \*.bsh
-   \*.csh
-   \*.ksh
-   \*.sh
-   \*.shell
-   \*.ssh
-   \*.tsch

If any files intended for other applications are included in the
delivery and which are renamed to supported Shell extensions, the
following effects might be observed during an analysis:

-   inconsistent objects may get created
-   end of string ''' not found errors
-   if the file is binary: Invalid UTF-8 sequence found in text to be
    matched or searched for a regular expression

### Source code preprocessing 

Shell source code needs to be preprocessed so that CAST can understand
it and analyze it correctly. This code preprocessing is actioned
automatically when an analysis is launched or a snapshot is generated
(the code is preprocessed before the analysis starts). The Shell
Preprocessor log file is stored in the following location:

``` java
%PROGRAMDATA%\CAST\CAST\Logs\<application_name>\Execute_Analysis_<guid>\com.castsoftware.shell.<_extension_version>.prepro_YYYYMMDDHHMMSS.log
```

{{% alert color="info" %}}Note that the LISA folder will be used to analyze the preprocessed files - this is in contrast to standard behaviour for most other analyzers, where source code is analyzed directly from the Delivery folder. Due to this, if you use [User Defined Modules based on subfolders](../../../../interface/analysis-config/config/modules/udm/#module-per-subfolder) then you will need to ensure that you configure your paths based on the LISA folder.{{% /alert %}}

## Analysis configuration and execution

There are no analysis/technology configuration options available for
Shell, however you should check that at least one Shell analysis unit
has been created as shown below.

AIP Console exposes the technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Universal Technology (3) in the Config (1) \> Analysis
(2) tab to display the available options for your Shell source code:

![](../images/576750006.jpg)

Then choose the relevant Analysis Unit (1) to view the
configuration:

[![](../images/576750009.jpg)](https://doc.castsoftware.com/display/TECHNOSDRAFT/Shell+1.2?preview=%2F383889399%2F530317345%2Fconsole_shell2.jpg)

[![](../images/576750010.jpg)](https://doc.castsoftware.com/download/attachments/383889399/console_shell1.jpg?version=1&modificationDate=1629976576057&api=v2)

### Logging mechanism

#### Analysis log files

Analysis logs are stored in the default locations.

#### Shell Preprocessor

Shell Preprocessor log files (the preprocessor is launched automatically
during an analysis) are stored in the following locations:

| Location                                                | Log file name                                                               |
|---------------------------------------------------------|-----------------------------------------------------------------------------|
| %PROGRAMDATA%\CAST\CAST\Logs\\unique_application_id\>\\ | com.castsoftware.shell.prepro\_\<ExtensionVersion\>\_\<YYYYMMDDHHMMSS\>.log |

## What results can you expect?

### Objects

<table class="wrapped confluenceTable" style="width: 697.0px;">
<tbody>
<tr class="header">
<th class="confluenceTh" style="width: 57.0px">Icon</th>
<th class="confluenceTh" style="width: 640.0px">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762539.png" draggable="false"
data-image-src="../images/639762539.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762539"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLProject.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Project</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762538.png" draggable="false"
data-image-src="../images/639762538.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762538"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLProgram.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Program</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762537.png" draggable="false"
data-image-src="../images/639762537.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762537"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLFunction.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell Function, Shell
Special Function</p>
<p>Note that these objects are note displayed in CAST Imaging's Reduced
or Simplified Call Graph view.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762499.png" draggable="false"
data-image-src="../images/639762499.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762499"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLVariable.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Variable</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762535.png" draggable="false"
data-image-src="../images/639762535.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762535"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLJavaCall.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Call to Java
Program</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762536.png" draggable="false"
data-image-src="../images/639762536.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762536"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SHELLGeneralCall.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Call to Generic
Program</td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTh"
style="width: 697.0px"><strong>Amazon Web Services</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762498.png" draggable="false"
data-image-src="../images/639762498.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762498"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_Lambda.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell AWS Lambda
Function</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762509.png" draggable="false"
data-image-src="../images/639762509.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762509"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_CallTo_AWS_Lambda.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Call to AWS Lambda
Function</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762508.png" draggable="false"
data-image-src="../images/639762508.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762508"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_CallTo_AWS_Unknown_Lambda.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell Call to AWS
Unknown Lambda Function</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762510.png" draggable="false"
data-image-src="../images/639762510.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762510"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_S3_Bucket.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell AWS S3 Bucket</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762507.png" draggable="false"
data-image-src="../images/639762507.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762507"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_Unknown_S3_Bucket.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell AWS Unknown S3
Bucket</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762542.png" draggable="false"
data-image-src="../images/639762542.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762542"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_DynamoDB_Database.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell AWS DynamoDB
Database</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762506.png" class="image-center"
draggable="false" data-image-src="../images/639762506.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762506"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_DynamoDB_Table.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell AWS DynamoDB
Table</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762505.png" draggable="false"
data-image-src="../images/639762505.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762505"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_Unknown_DynamoDB_Table.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell AWS Unknown
DynamoDB Table</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762504.png" draggable="false"
data-image-src="../images/639762504.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762504"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_SNS_Publisher.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell AWS SNS
Publisher, Shell AWS SQS Publisher</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762503.png" draggable="false"
data-image-src="../images/639762503.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762503"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_SQS_Receiver.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell AWS SNS
Subscriber, Shell AWS SQS Receiver</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762502.png" draggable="false"
data-image-src="../images/639762502.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762502"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_SNS_Unknown_Publisher.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell AWS Unknown SNS
Publisher, Shell AWS Unknown SQS Publisher</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762501.png" draggable="false"
data-image-src="../images/639762501.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762501"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_AWS_SNS_Unknown_Subscriber.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell AWS Unknown SNS
Subscriber, Shell AWS Unknown SQS Receiver</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762497.png" draggable="false"
data-image-src="../images/639762497.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762497"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_GetServiceRequest.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell GET service
request</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762495.png" draggable="false"
data-image-src="../images/639762495.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762495"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_PostServiceRequest.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell POST service
request</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762496.png" draggable="false"
data-image-src="../images/639762496.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762496"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_PutServiceRequest.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell PUT service
request</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762494.png" draggable="false"
data-image-src="../images/639762494.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762494"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_DeleteServiceRequest.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px"><p>Shell DELETE service
request</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center; width: 57.0px;"><div
class="content-wrapper">
<p><img src="../images/639762500.png" draggable="false"
data-image-src="../images/639762500.png"
data-unresolved-comment-count="0" data-linked-resource-id="639762500"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SHELL_Email.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576749956"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="width: 640.0px">Shell SMS, Shell
Email</td>
</tr>
</tbody>
</table>

### Links

| Source Object                           | Link Type                          | Target Object                   |
|-------------------------------|---------------------|---------------------|
| Shell Program                           | callProgLink                       | Shell Program                   |
| Shell Program                           | include                            | Shell Program                   |
| Shell Program                           | callLink                           | Shell Function/Special Function |
| Shell Function/Special Function         | callLink                           | Shell Program                   |
| Shell Function/Special Function         | callLink                           | Shell Function/Special Function |
| Shell Function/Special Function/Program | use select, update, insert, delete | data functions                  |
| Shell Function/Special Function/Program | callLink                           | Shell Call to Java Program      |
| Shell Function/Special Function/Program | callLink                           | Shell Call to Generic Program   |
| Shell Function/Special Function/Program | callLink                           | SQL Table                       |
| Shell Function/Special Function/Program | callLink                           | SQL Script                      |

#### Embedded SQL

Shell allows the use of here-document to write SQL queries, for example:

``` bash
##!/bin/sh
/opt/solid/bin/solsql db db <<abc
select count(*) from table_1;
select count(*) from table_2;
exit;
!;
abc
```

The Shell extension can use these queries to create links.

#### Call to SQL script

The Shell extension allows links to SQL script when used with a
dependency to the SQL analyzer

For example:

script.sh

``` bash
##!/bin/env bash

sqlplus script.sql
```

script.sql

``` sql
CREATE TABLE ...
```

In Enlighten

![](../images/576750005.png)

#### Links to external programs

The following call pattern are supported

-   ./exe
-   /path/to/exe
-   python
-   java
-   nohup
-   eval
-   wlst.sh
-   runcobol \| runcobol.exe

#### Shell to Java links

The extension does support links between Shell programs and Java objects
(for example methods). Links will be created between these technologies.

The extension manages call to Java classes and .jar files. For .jar
files no links will go further as .jar aren't handle by any extensions,
the link is purely informative.

##### Basic Case

``` bash
...
/bin/java UpdateRepartiteurs
...
```

wil generate the following diagram

![](../images/576749979.png)

##### Function Case

``` bash
...
MajRepartiteurs ()
{
    ...
    java UpdateRepartiteurs
    ...
}
...
```

will generate the following diagram

![](../images/576749980.png)

#### Shell to COBOL links

The extension does support links between Shell programs and COBOL
objects (for example programs). Links will be created between these
technologies.

##### Basic Case

COBOL file

``` java
IDENTIFICATION DIVISION.
PROGRAM-ID. HELLO.
* simple hello world program
PROCEDURE DIVISION.
    DISPLAY 'Hello world!'.
```

SHELL script

``` bash
./hello
```

will generate the following diagram

![](../images/576749978.png)

#### Shell to Python links

The extension does support links between Shell programs and Python
objects (for example methods). Links will be created between these
technologies.

The following script

``` java
...
python foo.py
...
```

will generate the following diagram, assuming that foo.py exists and has
been analyzed

![](../images/576749977.png)

### Support for AWS CLI

#### Lambda

<table>
<colgroup>
<col />
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Supported API</th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">aws lambda <strong>invoke</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Shell Program</td>
<td class="confluenceTd">Shell Call to AWS Lambda Function</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">aws
lambda <strong>create-function</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">Creates a Shell AWS Lambda Function with
<em>handler</em> and <em>runtime</em> properties (when resolved).</td>
</tr>
</tbody>
</table>

An example of a command invoking a lambda named *my-function*.

``` bash
$ aws lambda invoke --function-name my-function
```

When analyzing it, a *Shell Call to AWS Lambda function* object is
created. This object will be linked to lambda functions objects (which
may be created by other extensions) having the same name during
application level analysis stage by the *web services linker*
extension. When the name of an invoked Lamba function is not evaluated
(either because of the absence of information or technical limitations)
a Shell Call to AWS Unknown Lambda Function* *is created instead. A
maximum of one call for each lambda (including unknowns) is created per
caller.

An example of a command creating an AWS Lambda of name *my-function*
where the handler is a javascript method.

``` bash
$ aws lambda create-function --function-name my-function --zip-file fileb://function.zip --handler index.handler --runtime nodejs12.x --role arn:aws:iam::123456789012:role/lambda-ex
```

When analyzing it, a *Shell AWS Lambda Function* object is created.
It contains the runtime and handler properties (here *nodejs12.x* and
index*.handle*r, respectively). The linking from the lambda function to
the handler function is then carried out during application level
analysis stage by one of the following extensions (depending on the
runtime):

| Runtime | Extension | Minimum required extension release |
|---|---|---|
| Java | [com.castsoftware.awsjava](../../../cloud-services/aws/extensions/com.castsoftware.awsjava/) | 1.2.0-alpha3 |
| .NET | [com.castsoftware.awsdotnet](../../../cloud-services/aws/extensions/com.castsoftware.awsdotnet/) | 1.0.0-alpha5 |
| Python | [com.castsoftware.python](../../../python/com.castsoftware.python/) | 1.4.0-beta7 |
| Node.js | [com.castsoftware.nodejs](../../../web/nodejs/com.castsoftware.nodejs/) | 2.7.0-beta3 (when the handler is written in .js) |
| TypeScript | [com.castsoftware.typescript](../../../web/typescript/com.castsoftware.typescript/) | 1.9.0-alpha1 (when the handler is written in .ts) |

Below we illustrate the expected results with a few aws commands. The
code samples used are adaptations from available examples in official
AWS documentation pages.

##### Example 

In the following code we enclose three cli calls to aws lambda services
in three different shell functions:

``` bash
CreateFunction() {
  aws lambda create-function --function-name my-lambda --runtime nodejs10.x --zip-file fileb://my-function.zip --handler my-function.handler --role arn:aws:iam::123456789012:role/service-role/MyTestFunction-role-tges6bf4
}

InvokeFunction1() {
  aws lambda invoke --function-name my-function --payload '{ "name": "Bob" }' response.json
}

## it contains an unresolved variable
InvokeFunction2() {
  aws lambda invoke --function-name $VarFunctionName --payload '{ "name": "Bob" }' response.json
}
```

As a result of the code above, a AWS Lambda function is created, and two
calls to a AWS Lambda functions (one resolved and one unresolved).

![](../images/576749966.png)

The Lambda function object contains the runtime and handler properties
(see details above in the section) necessary for linking to the
respective handler in different technologies.

#### S3

There are at least two different APIs for manipulation of S3 buckets.
The s3api command is used for low level manipulation
where s3 has a higher-level functionality.

<table>
<colgroup>
<col />
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">API (s3)</th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">aws s3 <strong>cp</strong></td>
<td class="confluenceTd">useSelectLink, useInsertLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3 <strong>mb</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">Creates a bucket</td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3 <strong>mv</strong></td>
<td class="confluenceTd">useDeleteLink, useInsertLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3 <strong>ls</strong></td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd">When no bucket is specified, a link to each
buckets is created</td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3 <strong>presign</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd">No URL creation is modelized.</td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3 <strong>rb</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd"><em>rb</em> deletes an <em>empty</em> bucket
(so the creation  of a <em>useDeleteLink</em> is excluded).</td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3 <strong>rm</strong></td>
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3 <strong>sync</strong></td>
<td class="confluenceTd">useSelectLink, useInsertLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3 <strong>website</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

In the absence of explicit bucket creation, references to buckets in
other commands are used to create table objects.

<table >
<colgroup>
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">API (s3api) with CRUD links</th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">aws s3api <strong>get-object</strong></td>
<td rowspan="6" class="confluenceTd">useSelectLink</td>
<td rowspan="6" class="confluenceTd">Shell Program or Function</td>
<td rowspan="6" class="confluenceTd">Shell AWS S3 Bucket</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>aws
s3api <strong>get-object-torrent</strong></p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3api select-object-content</td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3api <strong>list-objects</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3api <strong>list-objects-v2</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3api <strong>list-parts</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3api <strong>delete-bucket</strong></td>
<td rowspan="3" class="confluenceTd">useDeleteLink</td>
<td rowspan="3" class="confluenceTd">Shell Program or Function<br />
<br />
</td>
<td rowspan="3" class="confluenceTd">Shell AWS S3 Bucket<br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3api <strong>delete-object</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3api <strong>delete-objects</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3api <strong>put-object</strong></td>
<td rowspan="3" class="confluenceTd">useInsertLink</td>
<td rowspan="3" class="confluenceTd">Shell Program or Function</td>
<td rowspan="3" class="confluenceTd">Shell AWS S3 Bucket</td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3api <strong>restore-object</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd">aws s3api <strong>upload-part</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd">aws s3api <strong>copy-object</strong></td>
<td class="confluenceTd">useSelectLink,<br />
useInsertLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
</tr>
</tbody>
</table>

For the list of commands acting on a bucket that are modelized with a
callLink, expand the list below

Click here to expand other methods...

<table>
<colgroup>
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">API </th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><ul>
<li>abort-multipart-upload</li>
<li>complete-multipart-upload</li>
<li>create-multipart-upload</li>
<li>delete-bucket-analytics-configuration</li>
<li>delete-bucket-cors</li>
<li>delete-bucket-encryption</li>
<li>delete-bucket-intelligent-tiering-configuration</li>
<li>delete-bucket-inventory-configuration</li>
<li>delete-bucket-lifecycle</li>
<li>delete-bucket-metrics-configuration</li>
<li>delete-bucket-ownership-controls</li>
<li>delete-bucket-policy</li>
<li>delete-bucket-replication</li>
<li>delete-bucket-tagging</li>
<li>delete-bucket-website</li>
<li>delete-object-tagging</li>
<li>delete-public-access-block</li>
<li>get-bucket-accelerate-configuration</li>
<li>get-bucket-acl</li>
<li>get-bucket-analytics-configuration</li>
<li>get-bucket-cors</li>
<li>get-bucket-encryption</li>
<li>get-bucket-intelligent-tiering-configuration</li>
<li>get-bucket-inventory-configuration</li>
<li>get-bucket-lifecycle-configuration</li>
<li>get-bucket-location</li>
<li>get-bucket-logging</li>
<li>get-bucket-metrics-configuration</li>
<li>get-bucket-notification-configuration</li>
<li>get-bucket-ownership-controls</li>
<li>get-bucket-policy</li>
<li>get-bucket-policy-status</li>
<li>get-bucket-replication</li>
<li>get-bucket-request-payment</li>
<li>get-bucket-tagging</li>
<li>get-bucket-versioning</li>
<li>get-bucket-website</li>
<li>get-object-acl</li>
<li>get-object-attributes</li>
<li>get-object-legal-hold</li>
<li>get-object-lock-configuration</li>
<li>get-object-retention</li>
<li>get-object-tagging</li>
<li>get-public-access-block</li>
<li>head-bucket</li>
<li>head-object</li>
<li>list-bucket-analytics-configurations</li>
<li>list-bucket-intelligent-tiering-configurations</li>
<li>list-bucket-inventory-configurations</li>
<li>list-bucket-metrics-configurations</li>
<li>list-buckets</li>
<li>list-multipart-uploads</li>
<li>list-object-versions</li>
<li>put-bucket-accelerate-configuration</li>
<li>put-bucket-acl</li>
<li>put-bucket-analytics-configuration</li>
<li>put-bucket-cors</li>
<li>put-bucket-encryption</li>
<li>put-bucket-intelligent-tiering-configuration</li>
<li>put-bucket-inventory-configuration</li>
<li>put-bucket-lifecycle-configuration</li>
<li>put-bucket-logging</li>
<li>put-bucket-metrics-configuration</li>
<li>put-bucket-notification-configuration</li>
<li>put-bucket-ownership-controls</li>
<li>put-bucket-policy</li>
<li>put-bucket-replication</li>
<li>put-bucket-request-payment</li>
<li>put-bucket-tagging</li>
<li>put-bucket-versioning</li>
<li>put-bucket-website</li>
<li>put-object-acl</li>
<li>put-object-legal-hold</li>
<li>put-object-lock-configuration</li>
<li>put-object-retention</li>
<li>put-object-tagging</li>
<li>put-public-access-block</li>
<li>wait</li>
<li>write-get-object-response</li>
</ul></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Shell Program or Function</td>
<td class="confluenceTd">Shell AWS S3 Bucket</td>
</tr>
</tbody>
</table>

##### Example 

In the following code we show a few calls to s3 and s3api commands.

``` bash
Hello () {
   aws s3 cp test.txt s3://bucket1/test2.txt
}

MoveFile () {
   aws s3 mv s3://bucket19/test.txt s3://${BUCKET}/
}

GetObject () {
   aws s3api get-object --bucket text-content --key dir/my_images.tar.bz2 my_images.tar.bz2
}

GetObjectUnresolved () {
   aws s3api get-object --bucket $UnknownVar --key dir/my_images.tar.bz2 my_images.tar.bz2
}
```

The resulting buckets and links:

![](../images/576749959.png)

If the name of a bucket is not resolved, an unknown object is created as
shown above. Note that a maximum of one unknown (SHELL) bucket will be
created per project. However, if the name of the bucket is partially
resolved, a normal bucket object will be created, with the unresolved
name fragments replaced by the "?" character (example not shown here).

#### SQS

The supported API:

| API  | Link Type | Caller  | Callee  |
|------------------|----------------|----------------|----------------------|
| aws sqs send-message       | callLink  | Shell Program or Function | Shell AWS SQS Publisher              |
| aws sqs send-message-batch | callLink  |                           |                                      |
| aws sqs receive-message    | callLink  | Shell AWS SQS Receiver    | Shell Program or Function (optional) |

  

##### Example 

In the following code we send a message with the Send function and we
retrieve the message in the Receive function:

``` bash
Send () {
   aws sqs send-message --queue-url https://sqs.us-east-1.amazonaws.com/80398EXAMPLE/MyQueue --message-body "Information about the largest city in Any Region." --delay-seconds 10 --message-attributes file://send-message.json
}

Receive () {
   aws sqs receive-message --queue-url https://sqs.us-east-1.amazonaws.com/80398EXAMPLE/MyQueue --attribute-names All --message-attribute-names All --max-number-of-messages 10
}
```

The results in Enlighten show the created objects with name equal to the
Queue url:

![](../images/576749957.png)

The *callLink* links between the Publisher and the respective Receivers
are created by the Web Services Linker extension during application
level based on the Queue name registered in the queueName property. The
handler of the receiver is the function Receive itself, responsible of
treating the received data.

When the evaluation of the queue name fails, a Shell AWS Unknown SQS
Publisher (or Receiver) object will be created.

#### SNS

| API                       | Link Type | Caller                    | Callee                             |
|------------|------------|-------------------|----------------------|
| aws sns publish       | callLink  | Shell Program or Function | Shell AWS SNS Publisher, Shell SMS |
| aws sns publish_batch | callLink  |                           |                                    |
| aws sns subscribe     | callLink  | Shell AWS SNS Publisher   | Shell SMS, Shell Email             |


The supported protocols are the following:

| Protocol             | Object/s created                  | Name of the object                                            |
|:---------------------|:----------------------------------|:--------------------------------------------------------------|
| email/email-json | Shell Email                       | *an Email*   (the email addresses are not evaluated)          |
| http/https       | Shell POST service request        | the url (evaluated from the endpoint)                         |
| lambda           | Shell Call to AWS Lambda Function | the name of the lambda function (evaluated from the endpoint) |
| sms              | Shell SMS                         | *an SMS*   (the SMS numbers are not evaluated)                |
| sqs              | Shell AWS SQS Publisher           | the name of the queue (evaluated from the endpoint)           |

The *callLink* links between the Publisher and the respective
Subscribers are created by the Web Services Linker extension during
application level.

For each method a maximum of one subscriber per given topic will be
created as shown in the image above.

##### Example 1

In the following code we have to functions, one publishing into a topic,
and the other one subscribing to different notification endpoints of
different protocols.

``` bash
Publish () {
   aws sns publish --topic-arn "arn:aws:sns:us-west-2:123456789012:my-topic" --message file://message.txt
}

Receive() {
    aws sns subscribe --topic-arn arn:aws:sns:us-west-2:123456789012:my-topic --protocol email --notification-endpoint my-email@example.com
    aws sns subscribe --topic-arn arn:aws:sns:us-west-2:123456789012:my-topic --protocol sms --notification-endpoint 123456789
    aws sns subscribe --topic-arn arn:aws:sns:us-west-2:123456789012:my-topic --protocol sqs --notification-endpoint arn:partition:service:region:account-id:queueName
    aws sns subscribe --topic-arn arn:aws:sns:us-west-2:123456789012:my-topic --protocol lambda --notification-endpoint fooarn:function:lambda_name:v2
    aws sns subscribe --topic-arn arn:aws:sns:us-west-2:123456789012:my-topic --protocol http --notification-endpoint http://foourl
}
```

The *callLink* links between the Publisher and the respective
Subscribers are created by the Web Services Linker extension during
application level.

![Alt text](../images/576749962.png)

For each method a maximum of one subscriber per given topic will be
created as shown in the image above.

When the evaluation of the topic name fails, a Shell AWS Unknown SNS
Publisher (or Subscriber) object will be created.

##### Example 2

We can also have direct sms deliveries from calls to publish API
command:

``` bash
PublishSMS () {
    aws sns publish --message "Hello world!" --phone-number +1-555-555-0100
}
```

resulting in the direct callLink to the Shell SMS object:

![Alt text](../images/576749961.png)

#### DynamoDB

Please refer: [DynamoDB support for Shell source
code](DynamoDB_support_for_Shell_source_code)

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 1.3.6 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.3.6 |
| 1.3.5 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.3.5 |
| 1.3.4 | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.3.4 |
| 1.3.2-funcrel | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.3.2-funcrel] |
| 1.3.1-funcrel | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.3.1-funcrel] |
| 1.3.0-funcrel | https://technologies.castsoftware.com/rules?sec=srs_shell&ref=\|\|1.3.0-funcrel] |

You can also find a global list
here: [https://technologies.castsoftware.com/rules?sec=t_1016000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1016000&ref=%7C%7C).

### Data sensitivity

This extension is capable of setting a property on Shell objects (S3 bucket + DynamoDb collection) for the following:

- custom sensitivity
- GDPR
- PCI-DSS

See [Data Sensitivity](../../../multi/data-sensitivity) for more information.

## Limitations/known issues

### Script file import

This extension does not support the "import" of functions defined in
other script files using "sourcing" 
(see [https://www.mkssoftware.com/docs/man1/dot.1.asp](https://www.mkssoftware.com/docs/man1/dot.1.asp "https://www.mkssoftware.com/docs/man1/dot.1.asp")),
therefore no links are expected between functions declared in other
script files Note that "sourcing" is supported when used in variable
definitions.

### Deployment folder path

The deployment folder path should contain only ASCII characters (due to
the way the command line text is passed in a Windows operating
system). Names of folders and files inside the deployment folder can
contain non-ASCII characters.

### Links to database objects

When your Shell scripts contain references to database objects in the
server side element of an application and these references use fully
qualified names for the database object, no links will be created
between Shell and the database objects when you have used the [SQL
Analyzer extension](SQL_Analyzer) to analyze the SQL if SQL Analyzer
does not create database objects with same name. If you have used
the SQL analyzers embedded in CAST AIP to analyze the SQL, then links
will be created as normal. This is a known issue.

### Metrics Assistant (embedded in CAST AIP) limitations

#### Searches not limited only to embedded SQL

The MA (Metric Assistant) which is used for metric search cannot search
only in embedded SQL. Some Shell rules may be affected by this
limitation and may produce false violations.

#### Cannot calculate metric excluding comments

The MA (Metric Assistant) which is used for metric search cannot search
correctly while excluding comments especially if comments start or end
adjacent to the keyword. If such a condition exists, random false
violations may occur.

### Shell embedded strings

Shell code allows string to be embedded in strings as shown in the code
sample below. Currently, the Shell extension (and other Universal
Analyzer type extensions) will consider this as one continuous string.
Because we do not have any way to identify perfect end string patterns
in this case, we cannot find the end of string and therefore the file
will be skipped during the analysis.

``` bash
echo '              
Outer string ;              
cat '$file03'             
Another outer string              
'$id'             
' | $command
```

### KSH: guessing of ending single\double quote

Note that this limitation is no longer applicable to Shell ≥ 1.0.10.

KSH supports the *guessing* of ending single/double quotes. The Shell
extension supports this when the string is in single line, however, it
is not supported when the string is in a multiple line, for example:

``` bash
export OUTPUT=`basename $SOME_VARIABLE | $AWK 'FS="-" {
  i=3
  tmpMachineName= "mach_"$2
  while ( i <= NF){
    tmpMachineName=tmpMachineName"-"$i;
    i++;
  }
  print(tmpMachineName);
}`
```

### Multi-line document markers

Note that this limitation is no longer applicable to Shell ≥ 1.0.10.

When a document marker is in a multi-line string, the Shell extension
will not be able detect that it is in a string, for example:

``` bash
some_multiline_string="a;b;c;d;\
e;f;g;h;\
x;y;z<<;strong text"
```

In both of these cases, the file will be skipped and logs will contain
the warning: "File Skipped".