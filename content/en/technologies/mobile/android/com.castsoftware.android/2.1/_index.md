---
title: "Android - 2.1"
linkTitle: "2.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.android

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Android technologies. This extension
works together with
the [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest)
and the
[com.castsoftware.kotlin](https://extend.castsoftware.com/#/extension?id=com.castsoftware.kotlin&version=latest) extension.
If your JEE or Kotlin application source code uses Android
technologies you should install this extension.

## Features

### AndroidManifest file

This file will be analyzed to understand whether the application is
indeed an Android application. Some objects (exposed
service/activity/receiver) are created under the manifest to represent
exposed services. Then links are created from these services to the
methods which are listeners of these services.

### Android Event handling support from XML file

- `onClickListener()`
- `onLongClickListener()`
- `onFocusChangeListener()`
- `onKeyListener()`
- `onTouchListener()`
- `onCreateContextMenu()`

### Android Event handling support from setEventListenner and addEventListener in widgets

The lambda function can be used to define the event handler. Sample:

```java
Button.setOnClickListenner(
    new View.OncLickListentHandler{...}),
    with lamda function: Button.setOnClickListenner( (v) → {}
    )
```

### Android Type XML support

- Widget Layout
- AndroidManifest file

### WebServices support

Support for third-party WebServices is provided:

- `httpClient `library with namespace `cz.msebera.android.httpclient`.
- `httpClient` library with namespace `org.apache.http.client.HttpClient`.
- `httpUrlConnection` from `java.net.URL` and `java.net.HttpURLConnection`.
- Create webservice via `AsyncTask` class of Android.
- Transaction between client and server on fullstack project.
- Transaction between event handler widget and http method call.
- Android Volley Http library (https://github.com/google/volley) (Java support).

### End to End transactions

- End to End transactions are resolved:
    - Entry point: layout ui xml file, exposed services
    - End point: web service, persisting data, intent resource calls

## Supported Android versions

| Version | Supported |
|---|:-:|
| 4.4 - 10.0 | :white_check_mark: |

## Technology support

| Technology | Supported |
|---|:-:|
| Java | :white_check_mark: |
| Kotlin | :white_check_mark: |

## Supported libraries

### androidx.room for kotlin

``` java
import androidx.room.Entity

@Entity(tableName = "contact", primaryKeys = ["contactId", "contactUuid"])
data class Contact(
    var contactId: String = "",
    var contactUuid: String = "",
    var displayName: String = "",
    var pictureUri: String? = null,
    var contactInfo: List<ContactAddressBook> = listOf()
) : Parcelable
```

``` java
import androidx.room.*

@Dao
abstract class ContactDao {
    @Query("SELECT * FROM contact")
    abstract fun getAll(): LiveData<List<Contact>>

    @Update
    abstract fun updateContact(contact: Contact)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(contact: List<Contact>): List<Long>
}
```

### android.app.Application and com.ad4screen.sdk.A4SApplication for java and kotlin

"onCreate" and "onApplicationCreate" methods are now called from the
android application object.

## Function Point, Quality and Sizing support

- Function Points (transactions): A green tick indicates that OMG Function Point
counting and Transaction Risk Index are supported.
- Quality and Sizing: A green tick indicates that CAST can measure
size and that a minimum set of Quality Rules exist.

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Android extension requires that the
following other CAST extensions are also installed:

-   [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest)
-   [com.castsoftware.kotlin](https://extend.castsoftware.com/#/extension?id=com.castsoftware.kotlin&version=latest)
-   com.castsoftware.internal.platform (internal extension) ≥ 0.8.0
-   [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest) (internal
    technical extension) ≥ 1.6.1 funcrel

## Download and installation instructions

The extension will be automatically installed by CAST Imaging
Console.

## Source code discovery

The Android extension does not contain any discoverers or extractors,
therefore, no "Android" specific projects will be detected. Your Android
source code should be part of a larger Java/JEE related project which
you are also analyzing, and as such, JEE Analysis Units and/or Kotlin
Analysis Units will be created automatically - simply ensure that the
path to your Android source code is included in these JEE/Kotlin
Analysis Units:

![](../images/668336249.jpg)

## What results can you expect?

### Android application

![](../images/668336248.png)

### Transactions

*![](../images/668336247.png)*

### Web services linker

![](../images/668336246.png)

### Function points

![](../images/668336245.png)

### Objects

| Icon | Description |
|---|---|
| ![](../images/668336244.png) | Android EditText, Android TextView  |
| ![](../images/668336243.png) | Android Button, CheckBox, Item |
| ![](../images/668336242.png) | Android EventHandler |
| ![](../images/668336241.png) | Android Get Resource Service |
| ![](../images/668336240.png) | Android Put Resource Service |
| ![](../images/668336239.png) | Android Post Resource Service |
| ![](../images/668336238.png) | Android Delete Resource Service |
| ![](../images/668336237.png) | Android Shared preferences, Android internal storage, Android external storage |
| ![](../images/668336236.png) | Android Application |
| ![](../images/668336235.png) | Android UI XML File |
| ![](../images/668336234.png) | Android exposed service, Android exposed activity |
| ![](../images/668336233.png) | Android exposed receiver |
| ![](../images/668336232.png) | Android intent resource |
| ![](../images/668336231.png) | Android room entity |
| ![](../images/668336230.png) | Android GridView, Android Group, Android Layout, Android LinearLayout, Android ListView, Android Menu, Android RelativeLayout, Android Spinner, Android TimePicker, Android Undefined Widget, Android WebView. |

## Structural Rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 2.1.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.10-funcrel) |
| 2.1.9-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.9-funcrel) |
| 2.1.8-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.8-funcrel) |
| 2.1.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.7-funcrel) |
| 2.1.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.6-funcrel) |
| 2.1.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.5-funcrel) |
| 2.1.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.4-funcrel) |
| 2.1.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.3-funcrel) |
| 2.1.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.2-funcrel) |
| 2.1.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.1-funcrel) |
| 2.1.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_android&ref=\|\|2.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_android&ref=%7C%7C2.1.0-funcrel) |

## Known limitations

- CAST recommends using the most recent stable release of CAST Imaging Core with the extension for best results.
- Some links won't be resolved because of missing jar file.
- There are some limitations for transactions with Lamda Expressions.
- Web Service URI name isn't stable for more complex situations.
