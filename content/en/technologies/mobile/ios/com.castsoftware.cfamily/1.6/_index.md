---
title: "iOS - Objective-C - 1.6"
linkTitle: "1.6"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.cfamily

## What's new?

See [Release Notes](rn/).

## Description

If you have an iOS mobile application written in Objective-C and
built with Xcode and you want to view the source code object types
and their links with other objects, then you should install this
extension. Regarding Front-End to Back-End connections, we support the
following cross-technology stacks:

<table>
<tbody>
<tr>
<td><div>
<img src="../images/668336378.png" width="500" />
</div></td>
<td><div>
<img src="../images/668336377.png" width="500" />
</div></td>
</tr>
<tr>
<td style="text-align: center;"><em>iOS Front-End
connected JEE Back-End</em></td>
<td style="text-align: center;"><em>iOS Front-End
connected Node.js Back-End</em></td>
</tr>
</tbody>
</table>

### How CAST detects XCode projects

The iOS Objective-C extension includes a discoverer that enables Xcode
projects to be detected (see
[com.castsoftware.dmtxcodediscoverer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.dmtxcodediscoverer&version=latest)).
CAST will search for the project.pbxproj (or a .pbxproj file) - when one
is found, the contents are inspected and are used to determine the list
of projects present in the root folder.

### What are the differences with the extensions for Objective-C and C/C++?

The iOS Objective-C extension supports Objective-C and
the [com.castsoftware.cpp](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cpp&version=latest)
extension supports C/C++, however, the iOS Objective-C extension has
built in support for discovering Xcode projects that are primarily used
to build Mobile applications for the iOS Operating System. Therefore, if
your project is an Xcode project, you should use the iOS Objective-C
extension to take advantage of the built in discovery feature.

## Supported Languages

The following languages used to write iOS mobile applications are
supported by this extension:

| Technology | Supported |
|---|---|
| Objective-C    | :white_check_mark: |
| C/C++          | :white_check_mark: |
| Storyboard/XIB | :white_check_mark: |
| Plist          | :white_check_mark: |

When analysing application with mixed code Objective-C/Swift, links will
be created between methods from Objective-C and methods from Swift on
both directions.

## Technology support

| Version | Supported |
|---|---|
| iOS 2/6  | :x: |
| iOS 7/10 | :white_check_mark: |
| iOS 11   | :white_check_mark: |
| iOS 12   | :white_check_mark: |

## Supported iOS and third-party frameworks

| Framework | Supported |
|---|---|
| AVFoundation        | :white_check_mark: |
| AVKit               | :white_check_mark: |
| Accelerate          | :white_check_mark: |
| Accounts            | :white_check_mark: |
| AdSupport           | :white_check_mark: |
| AddressBook         | :white_check_mark: |
| AddressBookUI       | :white_check_mark: |
| AFNetworking        | :white_check_mark: |
| AssetsLibrary       | :white_check_mark: |
| AudioToolbox        | :white_check_mark: |
| AudioUnit           | :white_check_mark: |
| AutoLayout          | :white_check_mark: |
| CFNetwork           | :white_check_mark: |
| CloudKit            | :white_check_mark: |
| Contacts            | :white_check_mark: |
| CoreAudio           | :white_check_mark: |
| CoreAudioKit        | :white_check_mark: |
| CoreBluetooth       | :white_check_mark: |
| CoreData            | :white_check_mark: |
| CoreFoundation      | :white_check_mark: |
| CoreGraphics        | :white_check_mark: |
| CoreImage           | :white_check_mark: |
| CoreLocation        | :white_check_mark: |
| CoreMIDI            | :white_check_mark: |
| CoreMedia           | :white_check_mark: |
| CoreMotion          | :white_check_mark: |
| CoreTelephony       | :white_check_mark: |
| CoreText            | :white_check_mark: |
| CoreVideo           | :white_check_mark: |
| EventKit            | :white_check_mark: |
| EventKitUI          | :white_check_mark: |
| Facebook            | :x: |
| Foundation          | :white_check_mark: |
| GLKit               | :white_check_mark: |
| GameController      | :white_check_mark: |
| GameKit             | :white_check_mark: |
| GamePlayKit         | :white_check_mark: |
| HealthKit           | :white_check_mark: |
| HomeKit             | :white_check_mark: |
| iAd                 | :white_check_mark: |
| ImageIO             | :white_check_mark: |
| LocalAuthentication | :white_check_mark: |
| Logging             | :white_check_mark: |
| MapKit              | :white_check_mark: |
| MediaAccessibility  | :white_check_mark: |
| MediaPlayer         | :white_check_mark: |
| MessageUI           | :white_check_mark: |
| Metal               | :white_check_mark: |
| MobileCoreServices  | :white_check_mark: |
| OpenGLES            | :white_check_mark: |
| QuartzCore          | :white_check_mark: |
| QuickLook           | :white_check_mark: |
| SafariServices      | :white_check_mark: |
| Security            | :white_check_mark: |
| Social              | :white_check_mark: |
| Starboard           | :white_check_mark: |
| StoreKit            | :white_check_mark: |
| SystemConfiguration | :white_check_mark: |
| Twitter             | :white_check_mark: |
| UIKit               | :white_check_mark: |
| WebKit              | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The iOS extension requires that the
following other CAST extensions are also installed:

-   com.castsoftware.dmtxcodediscoverer (to identify Mobile application
    projects in the CAST Delivery Manager Tool)
-   [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest)

## Download and installation instructions

The extension will be automatically installed by CAST Imaging
Console.

## Source code discovery

A discoverer (see com.castsoftware.dmtxcodediscoverer) is provided with
the extension to automatically detect Xcode code: CAST will search for
the project.pbxproj (or a .pbxproj file) - when one is found, the
contents are inspected and are used to determine the list of projects
present in the root folder. For every Xcode project located, one C/C++
Technology Analysis Unit will be created:

![](../images/668336376.jpg)

## Analysis configuration

For each C/C++ Technology Analysis Unit created for an Xcode project
discovered by CAST, the following options should be set as shown below:

![](../images/668336375.jpg)

## What results can you expect?

### Example project

![](../images/668336374.png)

### Objects

#### C/C++

| Icon | Description |
|---|---|
| ![](../images/CAST_Cpp_AdditionalFile.jpg) | Additional File |
| ![](../images/668336373.png) | Class |
| ![](../images/668336372.png) | Class Folder |
| ![](../images/668336371.png) | Constructor |
| ![](../images/668336370.png) | Destructor |
| ![](../images/668336369.png) | Directory |
| ![](../images/668336368.png) | Enum |
| ![](../images/668336367.png) | Enum Item |
| ![](../images/668336366.png) | Free Function |
| ![](../images/668336365.png) | Global Variable |
| ![](../images/668336364.png) | Header File |
| ![](../images/668336363.png) | Macro |
| ![](../images/668336362.png) | Member Function |
| ![](../images/668336361.png) | Member Variable |
| ![](../images/668336360.png) | Namespace |
| ![](../images/668336359.png) | Parameter |
| ![](../images/668336358.png) | Root Directory |
| ![](../images/668336357.png) | Source File |
| ![](../images/668336356.png) | Template Class |
| ![](../images/668336355.png) | Template Class Instance |
| ![](../images/668336354.png) | Template Constructor |
| ![](../images/668336353.png) | Template Constructor Instance |
| ![](../images/668336352.png) | Template Free Function |
| ![](../images/668336351.png) | Template Free Function Instance |
| ![](../images/668336350.png) | Template Member Function |
| ![](../images/668336349.png) | Template Member Function Instance |
| ![](../images/668336348.png) | Template Parameter |
| ![](../images/668336347.png) | Template Union |
| ![](../images/668336346.png) | Template Union Instance |
| ![](../images/668336345.png) | Typedef |
| ![](../images/668336344.png) | Union |

#### iOS & Objective-C

| Icon | Description |
|---|---|
| ![](../images/668336343.png) | Project |
| ![](../images/668336342.png) | Workspace |
| ![](../images/668336341.png) | Source File |
| ![](../images/668336340.png) | Plist File |
| ![](../images/668336339.png) | Story Board File |
| ![](../images/668336338.png) | XIB File |
| ![](../images/668336337.png) | UIApplication Delegate |
| ![](../images/668336336.png) | UIApplication |
| ![](../images/668336335.png) | Interface |
| ![](../images/668336334.png) | Category |
| ![](../images/668336333.png) | Protocol |
| ![](../images/668336332.png) | Class Method |
| ![](../images/668336331.png) | Method / Block |
| ![](../images/668336330.png) | Action |
| ![](../images/668336329.png) | Get Resource Service:<br>NSURLConnection<br>NSURLSession<br>NSURLSessionTask<br>NSURLSessionDownloadTask<br>NSURLSessionUploadTaskNSURLSessionDataTask<br>AFURLSessionManager<br>AFHTTPSessionManager<br>UIWebView |
| ![](../images/668336328.png) | Post Resource Service:<br>NSURLConnection<br>NSURLSession<br>NSURLSessionTask<br>NSURLSessionDownloadTask<br>NSURLSessionUploadTask<br>NSURLSessionDataTask<br>AFURLSessionManager<br>AFHTTPSessionManager |
| ![](../images/668336327.png) | Put Resource Service:<br>NSURLConnection<br>NSURLSession<br>NSURLSessionTask<br>NSURLSessionDownloadTask<br>NSURLSessionUploadTask<br>NSURLSessionDataTask<br>AFURLSessionManager<br>AFHTTPSessionManager |
| ![](../images/668336326.png) | Delete Resource Service:<br>NSURLConnection<br>NSURLSession<br>NSURLSessionTask<br>NSURLSessionDownloadTask<br>NSURLSessionUploadTask<br>NSURLSessionDataTask<br>AFURLSessionManager<br>AFHTTPSessionManager |
| ![](../images/668336325.png) | Interface Variable |
| ![](../images/668336324.png) | Outlet |
| ![](../images/668336323.png) | Property |
| ![](../images/668336322.png) | Property Getter |
| ![](../images/668336321.png) | Property Setter |
| ![](../images/668336320.png) | UI Button |
| ![](../images/668336319.png) | UI Control |
| ![](../images/668336318.png) | UI TextField |

{{% alert color="info" %}}If your application is supposed to be communicating with services, ensure you have GET, POST, PUT, DELETE Service objects created after the
analysis.{{% /alert %}}

## Structural Rules

| Release | Link |
|---|---|
| 1.6.1         | [https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=\|\|1.6.1](https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=%7C%7C1.6.1)                 |
| 1.6.0         | [https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=\|\|1.6.0](https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=%7C%7C1.6.0)                 |
| 1.6.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=\|\|1.6.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=%7C%7C1.6.0-funcrel) |
| 1.6.0-beta1   | [https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=\|\|1.6.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_cfamily&ref=%7C%7C1.6.0-beta1)     |

## Known Limitations

-   awakeFromNib() method will be linked to NSObject and not the class
    instance that overwrites this method.
-   "id" object does not guarantee we could find the real Class object.
