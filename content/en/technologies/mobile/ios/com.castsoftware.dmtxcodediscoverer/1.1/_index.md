---
title: "Xcode Discoverer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtxcodediscoverer

## What's new?

See [Release Notes](rn/).

## Description

This extension is a project discoverer: delivered source code will be
searched for the project.pbxproj (or a `.pbxproj` file) - when one is
found, the contents are inspected and are used to determine the list of
projects present in the root folder. Projects then form Analysis Units
(an item that defines the source code analysis perimeter).

This extension is a dependency of the **iOS - Objective-C** and **iOS - Swift** extensions - when either of these extensions are downloaded, the
Xcode Discoverer extension will also be downloaded automatically.
The extension can also be downloaded in a standalone manner - i.e.
without the [com.castsoftware.swift](https://extend.castsoftware.com/#/extension?id=com.castsoftware.swift&version=latest)
and [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest)
extensions if you just need to patch your existing Xcode discoverer
extension.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |
