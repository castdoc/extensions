---
title: "iOS - Swift - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.swift

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for iOS mobile applications written in
Swift built with Xcode.

## In what situation should you install this extension?

If you have an iOS mobile application written in Swift and built with
Xcode and you want to view the source code object types and their links
with other objects, then you should install this extension.

### How CAST detects XCode projects

The Swift extension is dependent on the [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest), which includes a discoverer that enables Xcode projects to be detected (see [com.castsoftware.dmtxcodediscoverer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.dmtxcodediscoverer&version=latest)). CAST will search for the `project.pbxproj` (or a `.pbxproj` file) - when one is found, the contents are inspected and are used to determine the list of projects present in the root folder.

### What are the differences with the extension for iOS Objective-C?

The [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest) extension is aimed at analyzing iOS mobile applications written in Objective-C (and to a lesser extent C and C++) and built with XCode. The Swift extension can do the same thing, however, it also supports source code written using the Swift technology.

## Supported Languages

The following languages used to write iOS mobile applications are
supported by this extension:

| Technology | Supported | Notes |
|---|:-:|---|
| Objective-C | :white_check_mark: | Supported via the dependent [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest) extension. |
| C/C++ | :white_check_mark: | Supported via the dependent [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest) extension. |
| Swift | :white_check_mark: | \- |

## Technology support

| Version | Supported |
|---|:-:|
| Swift 3.2 - 5.2 | :white_check_mark: |

## Supported iOS and third-party frameworks

Please see the same section in the [com.castsoftware.cfamily](../../com.castsoftware.cfamily) for more information.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Swift extension requires that the
following other CAST extensions are also installed:

-   [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest) 
-   [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest)

## Download and installation instructions

The extension will be automatically installed by CAST Imaging
Console.

## Source code discovery

### .swift files

A discoverer is provided with the extension to automatically detect
Swift specific code: a Universal Technology project will be
discovered for the package's root folder when at least one `.swift`
file is detected in the root folder or any sub-folders. For every
project located, one Universal Technology Analysis Unit will be created.

### Xcode (.pbxproj files)

A discoverer (see
[com.castsoftware.dmtxcodediscoverer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.dmtxcodediscoverer&version=latest))
is provided with the extension to automatically detect Xcode code: CAST
will search for the `project.pbxproj` (or a `.pbxproj` file) - when one is
found, the contents are inspected and are used to determine the list of
projects present in the root folder. For every Xcode project located,
one C/C++ Technology Analysis Unit will be created:

![](../images/557023276.jpg)

## Analysis configuration

For each C/C++ Technology Analysis Unit created for an Xcode project
discovered by CAST, the following options should be set as shown below:

![](../images/557023275.jpg)

### Automatic exclusion of test and external dependency files from analysis

Analyzers are aimed at the study of the application source code.
References and calls to external libraries are conveniently left out of
the analysis, except for those that can be interpreted as elements
taking part in a transaction, such as *web service requests* or
*database queries*. 

The Swift analyzer will skip files found inside the two standard
locations of the two leading dependency managers: *\Carthage\Checkouts*
and *Pods*. On the other hand, all files found inside a folder (or
within sub-folders) with name ending "Tests"  are considered as test
files (consistent with Xcode conventions). This feature works upon
detection of the project '.xcodeproj\\ folder to assure the files belong
to a Swift project. However, files with name ending as 'Tests.swift' are
automatically skipped.

## What results can you expect?

### Objects

#### Swift

| Icon | Description  |
|---|---|
| ![](../images/557023358.png) | Swift Class |
| ![](../images/665813628.png) | Swift DefaultUnit  |
| ![](../images/665813627.png) | Swift Enumeration |
| ![](../images/557023356.png) | Swift Function  |
| ![](../images/665813625.png) | Swift Method |
| ![](../images/557023353.png) | Swift Protocol |
| ![](../images/557023351.png) | Swift SourceCode |
| ![](../images/665813622.png) | Swift Structure |
| ![](../images/557023357.png) | Swift DELETE http service |
| ![](../images/557023355.png) | Swift GET http service |
| ![](../images/557023354.png) | Swift POST http service |
| ![](../images/557023352.png) | Swift PUT http service |

{{% alert color="info" %}}Tip: If your application is supposed to be communicating with services, ensure you have GET, POST, PUT, DELETE Service objects created after the analysis.{{% /alert %}}

#### C/C++

See the same section in the documentation for [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest).

#### iOS & Objective-C

See the same section in the documentation for [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest).

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 1.2.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_swift&ref=\|\|1.2.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_swift&ref=%7C%7C1.2.1-funcrel) |
| 1.2.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_swift&ref=\|\|1.2.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_swift&ref=%7C%7C1.2.0-funcrel) |
| 1.2.0-alpha2  | [https://technologies.castsoftware.com/rules?sec=srs_swift&ref=\|\|1.2.0-alpha2](https://technologies.castsoftware.com/rules?sec=srs_swift&ref=%7C%7C1.2.0-alpha2)   |
| 1.2.0-alpha1  | [https://technologies.castsoftware.com/rules?sec=srs_swift&ref=\|\|1.2.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_swift&ref=%7C%7C1.2.0-alpha1)   |

You can also find a global list here:
[https://technologies.castsoftware.com/rules?sec=t_1055000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1055000&ref=%7C%7C)

## Technical information

### Web Services

Web service queries are supported for URLSession and the third-party
library Alamofire. The same web service objects are used for both of
them. For the former, as an example, the code of the form as below will
create a Swift Delete http service object with url
"http://localhost:8001/rest/todos/{}":

``` text
func deleteTodo(str: String) {
    var urlString = "http://localhost:8001/rest/todos/" + str;
    guard let url = URL(string: urlString) else { return }
    var deleteTask = URLRequest(url: (url as URL?)!, ...)
    deleteTask.httpMethod = "DELETE"
    URLSession.shared.dataTask(with: deleteTask) {(data, response, error) in
        if let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) {
    …
    }.resume()
}
```

For *Alamofire* we support the request function. In the following
example the web service call is represented by a Swift Get http service
object

``` text
func getTodo(...) {
  guard let url = URL(string: "http://localhost:8001/rest/todos/") else {return}
  
  Alamofire.request(url, method: .get, ...)
  ...
  }
}
```

The (call) links from Swift objects (functions, methods, ...) to Swift
Web Service objects will be created by the analyzer as well. Similar to
other analyzers, the responsible of further linking Services and
Operations (possible inter-technology) is the dependency
extension [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest).
A picture of a simple call-graph including StoryBoard objects as entry
points and Web Services as end points is shown in the following section.

### Transactions for Function Points

In the example below we illustrate the results one can expect concerning
transactions. The call graph starts from the StoryBoard widgets (eg
Button and TextField) which are by default set as entry points. The
StoryBoard defining these widgets is interpreted by the
[com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest)
extension. After this; the Swift extension will then generate the links
between these widgets and Swift objects, such as functions and methods.
Eventually the call graph might reach different end points contributing
to function point counting. In this example most of the StoryBoard
widgets finish calling web services (these could potentially call web
service operations inside the very same application so that the
transaction would continue):

![](../images/557023362.png)

## Known Limitations

-   Some limitations come from the iOS / Objective-C extension,
    please refer to the
    [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest) documentation.

-   Violations localized in Class Property Initializer objects are not
    visible in the Dashboard (to be included in forthcoming releases).

-   Operator overloading and definition of custom operators is
    supported (at parsing level) but use of uncommon symbols may raise
    errors during the analysis.

-   Duplication of objects is avoided when preprocessing directives
    (#if, #endif) are used to specialize class, structure, prototype and
    Enum objects according to used frameworks. In the example below, a
    single Swift Structure object is generated, specifically, the first
    one appearing in the code:

``` java
#if os(iOS) || os(tvOS)
    import UIKit
    public struct TextInput<Base: UITextInput> {
    }
#endif
#if os(macOS)
    import Cocoa
    public struct TextInput<Base: NSTextInputClient> {
    }
#endif
```
