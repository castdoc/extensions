---
title: "CocoaPods Discoverer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtcocoapodsdiscoverer

## What's new?

See [Release Notes](rn/).

## Description

This extension is a project discoverer for https://cocoapods.org/ based
projects: delivered source code will be searched for podfile files -
each podfile file is equivalent to one "project". Source code is also
searched for \*.podsec files, which are CocoaPods profile files used by
podfile project files: when one is found, the contents are inspected and
are used to determine the list of projects present in the root folder.
For each discovered project (with source code) one Analysis Unit is
created (an item that defines the source code analysis perimeter).

This extension is for use with the **iOS - Objective-C** and **iOS - Swift** extensions - and is designed to automatically create CocoaPods related Analysis Units. The CocoaPods discoverer is not a dependency of the
[com.castsoftware.swift](https://extend.castsoftware.com/#/extension?id=com.castsoftware.swift&version=latest)
and [com.castsoftware.cfamily](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cfamily&version=latest) extensions, therefore it must be downloaded/installed manually before it can be used.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |
