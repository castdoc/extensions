---
title: "Mobile"
linkTitle: "Mobile documentation"
type: "docs"
no_list: false
weight: 10
---

## Language

| Icon                                                                                                                                                              | Name        | Versions        | Quality                                     | Extension                                                                   |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------------|:------------|:----------------|:--------------------------------------------|:----------------------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/kotlin/kotlin-original.svg" alt="icon" style="width: 32px; height: 32px;">         | Kotlin      | 1.x             | ISO 5055 Security                           | [com.castsoftware.kotlin](../kotlin/extensions/com.castsoftware.kotlin/)    |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/objectivec/objectivec-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Objective-C | Objective-C ARC | ISO 5055 Security, OWASP, CWE               | [com.castsoftware.cfamily](../cfamily/extensions/com.castsoftware.cfamily/) |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/swift/swift-original.svg" alt="icon" style="width: 32px; height: 32px;">           | Swift       | 3.2 - 5.2       | ISO 5055 Security, ISO 5055 Maintainability | [com.castsoftware.swift](../swift/extensions/com.castsoftware.swift/)       |

## Communication

| Icon                                                                                                                                                        | Name             | Versions   | Quality                                             | Extension                                                                                                                                          |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------|:-----------|:----------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/apple/apple-original.svg" alt="icon" style="width: 32px; height: 32px;">     | iOS URLSession   | 7.x - 12.x | ISO 5055 Security, ISO 5055 Reliability, OWASP, CWE | [com.castsoftware.cfamily](../cfamily/extensions/com.castsoftware.cfamily/), [com.castsoftware.swift](../swift/extensions/com.castsoftware.swift/) |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/apple/apple-original.svg" alt="icon" style="width: 32px; height: 32px;">     | iOS AFNetworking |            | ISO 5055 Security, ISO 5055 Reliability, OWASP, CWE | [com.castsoftware.cfamily](../cfamily/extensions/com.castsoftware.cfamily/)                                                                        |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/apple/apple-original.svg" alt="icon" style="width: 32px; height: 32px;">     | iOS Alamofire    |            | ISO 5055 Security, ISO 5055 Reliability, OWASP, CWE | [com.castsoftware.swift](../swift/extensions/com.castsoftware.swift/)                                                                              |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/android/android-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Android          | 4.4 - 10.0 | ISO 5055 Security, ISO 5055 Reliability, OWASP, CWE | [com.castsoftware.android](../android/extensions/com.castsoftware.android/)                                                                        |

## Presentation

| Icon                                                                                                                                                        | Name      | Versions   | Quality                                             | Extension                                                                                                                                          |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------|:----------|:-----------|:----------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/apple/apple-original.svg" alt="icon" style="width: 32px; height: 32px;">     | iOS UIKit | 7.x - 12.x | ISO 5055 Security, ISO 5055 Reliability, OWASP, CWE | [com.castsoftware.cfamily](../cfamily/extensions/com.castsoftware.cfamily/), [com.castsoftware.swift](../swift/extensions/com.castsoftware.swift/) |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/android/android-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Android   | 4.4 - 10.0 | ISO 5055 Security, ISO 5055 Reliability, OWASP, CWE | [com.castsoftware.android](../android/extensions/com.castsoftware.android/)                                                                        |

## Data Access

| Icon                                                                                                                                                        | Name    | Versions   | Quality                                             | Extension                                                                   |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------|:-----------|:----------------------------------------------------|:----------------------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/android/android-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Android | 4.4 - 10.0 | ISO 5055 Security, ISO 5055 Reliability, OWASP, CWE | [com.castsoftware.android](../android/extensions/com.castsoftware.android/) |

