---
title: "Fortran - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.fortran

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications written using Fortran
languages.

{{% alert color="info" %}}Although this extension is officially supported by CAST, please note
that it has been developed within the technical constraints of the CAST
Universal Analyzer technology and to some extent adapted to meet
specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other analyzers.{{% /alert %}}

## In what situation should you install this extension?

If your application contains source code written using Fortran and you
want to view these object types and their links with other objects, then
you should install this extension.

## Supported Fortran versions

This version of the extension provides support for:

| Fortran | Supported |
|---|:-:|
| Fortran 77 | :white_check_mark: |
| Fortran 90 | :white_check_mark: |
| Fortran 2003 | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Supported file types

Only files with following extensions will be analyzed:

- `*.for`
- `*.FOR`
- `*.inc`
- `*.INC`
- `*.pfo`
- `*.PFO`
- `*.f`
- `*.F`
- `*.f03`
- `*.F03`
- `*.f90`
- `*.F90`
- `*.f77`
- `*.F77`
- `*.h` (`.h` files are used as include files, however for the most part, `*.inc` are used as include files) 

Below is the list of extensions that will enable you to tell what type
of Fortran is provided:

| Fortran version | Extension |
|---|---|
| Fortran 2003 | `*.f03`, `*.F03` |
| Fortran 90 | `*.f90`, `*.F90`, `*.g90`, `*.f95`, `*.F95`, `*.g95`, `*.f`, `*.F`, `*.for`, `*.FOR` |
| Fortran 77 | `*.f77`, `*.F77`, `*.g77`, |

## Download and installation instructions

The extension will be automatically downloaded and installed when at least one of the supported file types is delivered for analysis.

## Source code discovery

A discoverer is provided with the extension to automatically detect
Fortran code: when one of the supported file types is found in a
folder, one "Fortran" project will be discovered, resulting in a
corresponding Universal Technology Analysis Unit.

## Tools

### Fortran Preprocessor

Fortran source code needs to be preprocessed so that CAST can understand
it and analyze it correctly. This code preprocessing is actioned
automatically when an analysis is launched or a snapshot is generated
(the code is preprocessed before the analysis starts). The Fortran
Preprocessor log file is stored in the following location:

```java
%PROGRAMDATA%\CAST\CAST\Logs\<application_name>\Execute_Analysis_<guid>\com.castsoftware.fortran.<_extension_version>.prepro_YYYYMMDDHHMMSS.log
```

## What results can you expect?

### Objects

| Icon | Metamodel description |
|---|---|
|![](../images/382626049.png) | Fortran Do Construct |
|![](../images/382626048.png) | Fortran External Function / Fortran Internal Function |
|![](../images/382626047.png) | Fortran External Subroutine / Fortran Internal Subroutine |
|![](../images/382626046.png) | Fortran IF Construct |
|![](../images/382626044.png) | Fortran Interface |
|![](../images/382626042.png) | Fortran IOSTAT Variable / Fortran STAT Variable |
|![](../images/382626041.png) | Fortran Module |
|![](../images/382626040.png) | Fortran Program |
|![](../images/382626039.png) | Fortran Project |
|![](../images/382626038.png) | Fortran Select Case |

### Structural Rules

The following structural rules are provided:

| Release | Link |
| ------- | ---- |
| 1.1.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=\|\|1.1.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=%7C%7C1.1.4-funcrel) |
| 1.1.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=\|\|1.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=%7C%7C1.1.3-funcrel) |
| 1.1.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=\|\|1.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=%7C%7C1.1.2-funcrel) |
| 1.1.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=\|\|1.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=%7C%7C1.1.1-funcrel) |
| 1.1.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=\|\|1.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_fortran&ref=%7C%7C1.1.0-funcrel) |

You can also find a global list here:

[https://technologies.castsoftware.com/rules?sec=t_1006000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1006000&ref=%7C%7C)

#### Exceptions

The following rules/metrics are not triggered during a Fortran analysis:

- Class Fan-in distribution
- Class Fan-out distribution
- Class Complexity distribution
- SQL Complexity distribution
- OO Complexity distribution
- Copy/Paste metrics
- Other generic diagnostics present in CAST AIP