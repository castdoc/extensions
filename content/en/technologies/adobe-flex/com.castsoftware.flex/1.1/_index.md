---
title: "Flex - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.flex

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications written using Adobe Flex
languages.

{{% alert color="info" %}}Although this extension is officially supported by CAST, please note
that it has been developed within the technical constraints of the CAST
Universal Analyzer technology and to some extent adapted to meet
specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other analyzers.{{% /alert %}}

## In what situation should you install this extension?

If your application contains source code written using Adobe Flex and you want to view these object types and their links with other objects, then you should install this extension.

## Support Overview

| Icon | Name | Versions | Size/Complexity | Features | Quality |
|---|---|---|---|:-:|---|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/apacheflex/apacheflex-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Adobe Flex | 3.x        | :white_check_mark: | Presentation, Object-Oriented, Object Pattern, Function Points | ISO 5055 Efficiency, ISO 5055 Maintainability, ISO 5055 Reliability |

A Transaction definition is automatically imported when the extension is installed.

## Supported file types

<!-- Automatically generated from fileextensions-flex.sh script in technologyguru/fileextensions -->

| Name | Programming Language | Supported |
| --- | -------------------- | --------- |
| *.as | ActionScript | True |
| *.mxml | ActionScript | True |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed when at least one of the supported file types is delivered for analysis.

## Source code discovery

A discoverer is provided with the extension to automatically detect Flex
code: when one of the supported file types is found in a folder, one
"Flex" project will be discovered, resulting in a corresponding
Universal Technology Analysis Unit in AIP Console.

## Tools

### Flex preprocessor

Flex source code needs to be preprocessed so that CAST can understand it
and analyze it correctly. This code preprocessing is actioned
automatically when an analysis is launched or a snapshot is generated
(the code is preprocessed before the analysis starts). Flex Preprocessor
log file is stored in the following location:

```text
%PROGRAMDATA%\CAST\CAST\Logs\<application_name>\Execute_Analysis_<guid>\com.castsoftware.flex.<_extension_version>.prepro_YYYYMMDDHHMMSS.log
```

### FlexPMD

Metrics/Rule data are generated using an external tool provided by
Adobe  called FlexPMD. The FlexPMD log file is stored in the following
location:

```text
%PROGRAMDATA%\CAST\CAST\Logs\<application_name\Execute_Analysis_<guid>\com.castsoftware.flex.plugin<_extension_version>_YYYYMMDDHHMMSS.log
```

The license for this tool is listed below:

*Copyright (c) 2009, Adobe Systems, Incorporated. All rights reserved.*

*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:*

- *Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.*
- *Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the
  distribution.*
- *Neither the name of the Adobe Systems, Incorporated. nor the names
  of its contributors may be used to endorse or promote products
  derived from this software without specific prior written
  permission.*

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*

### Errors and Warnings

The Flex configuration included in the extension uses external plugins.
During the analysis, the Universal Analyzer or the plugin may throw
errors or warnings. The table below list the most significant
errors/warnings and lists a suggested remediation action:


| Tool  | Error or Warning | Action |
| :---- | :--------------- | :----- |
| Flex Plugin | UA Plugin : No property (......) found in meta model for Flex... | No action required. The analyzer is telling you that not all the properties are considered to be injected into the Analysis Service. |

## What results can you expect?

### Objects

<!-- the table is generated using the script render-flex.sh in technologiesguru/listalltypes -->

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_destination | FLEX Destination |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessGreen.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_DestinationSource | FLEX Destination Source |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UITextField.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_field | FLEX Field |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_package | FLEX Package |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_RemoteObject | FLEX RemoteObject |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_script | FLEX Script |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Class.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_class | FLEX Class | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Class.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_RemoteClass | FLEX RemoteClass | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionGreen.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_function | FLEX Function | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PropertyGetter.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_getter | FLEX Getter | Property Getter |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PropertySetter.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | FLEX_setter | FLEX Setter | Property Setter |


### Structural rules

The following structural rules are provided:


| Release | Link |
|---|---|
| 1.1.3 | [https://technologies.castsoftware.com/rules?sec=srs_flex&ref=\|\|1.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_flex&ref=%7C%7C1.1.3-funcrel) |
| 1.1.2 | [https://technologies.castsoftware.com/rules?sec=srs_flex&ref=\|\|1.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_flex&ref=%7C%7C1.1.2-funcrel) |
| 1.1.1 | [https://technologies.castsoftware.com/rules?sec=srs_flex&ref=\|\|1.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_flex&ref=%7C%7C1.1.1-funcrel) |
| 1.1.0 | [https://technologies.castsoftware.com/rules?sec=srs_flex&ref=\|\|1.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_flex&ref=%7C%7C1.1.0-funcrel) |

You can also find a global list here:

[https://technologies.castsoftware.com/rules?sec=t_1007000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1007000&ref=%7C%7C)

## Limitations

### Duplicate Object of type … is detected warnings

The analysis log will contain the warning "Duplicate Object of type … is
detected" in the following situations:

- When members or functions are overloaded using "static" - this is
  partially a limitation inherent in the Universal Analyzer (the
  "engine" used for Flex analyses).
- When functions/variables are defined using different cases (for
  example "sort" and "Sort").

### String support

Strings are not supported in:

- XMLList
- ActionScript regex for example: var filterVal:String =
  aText.toLowerCase().replace( /\\/g, '/');

In both cases, if these strings contain single/double quote marks, the
file may be skipped.

### Name matching links - Universal Analyzer limitation

Due to a limitation in the Universal Analyzer (the "engine" used for
Flex analyses), links will be created from any name to any matching
name. At a minimum the following rule may be impacted and give erroneous
results:

| Rule ID | Description                            |
| --------- | :--------------------------------------- |
| 7388    | Avoid artifacts having recursive calls |

### Dynamic links which require dataflow/interpretation of parameters across multiple artifacts

Because of way the Universal Analyzer (analysis engine used by the Flex
extension)functions, dynamic links which require dataflow/interpretation
of parameters across multiple artifacts are not supported. If links are
required for these situations, a custom reference pattern can be created
to manually create the links.
