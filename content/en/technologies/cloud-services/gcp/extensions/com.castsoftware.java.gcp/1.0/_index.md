---
title: "GCP Java - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.gcp

## What's new

See [Release Notes](rn/).

## In what situation should you install this extension?

This extension should be used for analyzing java source code using any of the supported GCP services.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Supported services and frameworks

|Framework →<br>Services ↓  | SDK 1.x | SDK 2.x |
|---|:-:|:-:|
|Bigtable | :x: | :white_check_mark: |
|Cloud Storage | :x: | :white_check_mark: |
|PubSub | :white_check_mark: | N/A |

## Objects

| Icon | Description |
|---|---|
| ![](../images/666371439.png) | Java GCP Bigtable Table |
| ![](../images/666371438.png) | Java GCP Unknown Bigtable Table |
| ![](../images/666371437.png) | Java GCP Cloud Storage Bucket |
| ![](../images/666371436.png) | Java GCP Unknown Cloud Storage Bucket |
| ![](../images/666371435.png) | Java GCP Pub/Sub Publisher |
| ![](../images/666371434.png) | Java GCP Unknown Pub/Sub Publisher |
| ![](../images/666371432.png) | Java GCP Pub/Sub Receiver |
| ![](../images/666371431.png) | Java GCP Unknown Pub/Sub Receiver |

## Support for SDK

### Support for Cloud Bigtable

#### Supported Cloud Bigtable APIs for SDK v2

For the following methods of the
clients com.google.cloud.bigtable.data.v2.BigtableDataClient
and com.google.cloud.bigtable.admin.v2.BigtableInstanceAdminClient, we
create links to the Table:

| Link Type | Client | Methods |
|---|---|---|
| useInsertLink | com.google.cloud.bigtable.data.v2.BigtableDataClient | bulkMutateRows<br>bulkMutateRowsAsync<br>bulkMutationCallable<br>mutateRow<br>mutateRowAsync<br>mutateRowCallable<br>checkAndMutateRow<br>checkAndMutateRowAsync<br>checkAndMutateRowCallable<br>newBulkMutationBatcher |
| useUpdateLink | com.google.cloud.bigtable.data.v2.BigtableDataClient | readModifyWriteRow<br>readModifyWriteRowAsync<br>readModifyWriteRowCallable<br>bulkMutateRows<br>bulkMutateRowsAsync<br>bulkMutationCallable<br>mutateRow<br>mutateRowAsync<br>mutateRowCallable<br>checkAndMutateRow<br>checkAndMutateRowAsync<br>checkAndMutateRowCallable<br>newBulkMutationBatcher |
| useSelectLink | com.google.cloud.bigtable.data.v2.BigtableDataClient | readRow<br>readRowAsync<br>readRowCallable<br>readRows<br>readRowsAsync<br>readRowsCallable<br>sampleRowKeys<br>sampleRowKeysAsync<br>sampleRowKeysCallable<br>newBulkReadRowsBatcher |
| useDeleteLink | com.google.cloud.bigtable.data.v2.BigtableDataClient | bulkMutateRows<br>bulkMutateRowsAsync<br>bulkMutationCallable<br>mutateRow<br>mutateRowAsync<br>mutateRowCallable<br>checkAndMutateRow<br>checkAndMutateRowAsync<br>checkAndMutateRowCallable<br>newBulkMutationBatcher |
| useDeleteLink | com.google.cloud.bigtable.admin.v2.BigtableInstanceAdminClient | dropAllRows<br>dropAllRowsAsync<br>dropRowRange<br>dropRowRangeAsync |


#### What results you can expect?

When the name of a given referred table in the code is not resolved, an
unknown table object will be created with name Unknown. There can be a
maximum of one unknown table per project.

The analysis of the following source code will create a Java Call to GCP
Bigtable object named mobile-time-series with a useUpdateLink from the
WriteIncrement to that object:

``` java
package com.example.bigtable;

import com.google.cloud.bigtable.data.v2.BigtableDataClient;
import com.google.cloud.bigtable.data.v2.models.ReadModifyWriteRow;
import com.google.cloud.bigtable.data.v2.models.Row;
import java.nio.charset.Charset;

public class WriteIncrement {
  private static final String COLUMN_FAMILY_NAME = "stats_summary";

  public static void writeIncrement(String projectId, String instanceId, String tableId) {
    String projectId = "my-project-id";
    String instanceId = "my-instance-id";
    String tableId = "mobile-time-series";

    try (BigtableDataClient dataClient = BigtableDataClient.create(projectId, instanceId)) {
        
      String rowkey = "phone#4c410523#20190501";
      ReadModifyWriteRow mutation =
          ReadModifyWriteRow.create(tableId, rowkey)
              .increment(COLUMN_FAMILY_NAME, "connected_cell", -1);
      Row success = dataClient.readModifyWriteRow(mutation);

      System.out.printf(
          "Successfully updated row %s", success.getKey().toString(Charset.defaultCharset()));
    } catch (Exception e) {
      System.out.println("Error during WriteIncrement: \n" + e.toString());
    }
  }
}
```

![](../images/666371430.png)

### Support for Cloud Storage

#### Supported Cloud Storage APIs for SDK v2

For the following methods of the client  com.google.cloud.storage, we
create links to the Bucket:

| Link Type | Methods |
|---|---|
| useUpdateLink | Blob.writer<br>Storage.createFrom<br>Storage.copy<br>Storage.writer |
| useSelectLink | Blob.copyTo<br>Blob.downloadTo<br>Blob.reader<br>Blob.getContent<br>Storage.downloadTo<br>Storage.reader<br>Storage.copy<br>Storage.readAllBytes |
| useInsertLink | Blob.copyTo<br>Blob.writer<br>Storage.createFrom<br>Storage.copy<br>Storage.writer<br>Storage.create |
| useDeleteLink | Storage.delete<br>Blob.delete |

#### What results you can expect?

When the name of a given referred bucket in the code is not resolved, an
unknown bucket object will be created with name Unknown. There can be a
maximum of one unknown bucket per project.

The analysis of the following source code will create a Java GCP Cloud
Storage Bucket object named my-delete-bucket-o1 with a useDeleteLink.

``` java
package com.example.storage.object;

import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import java.util.LinkedList;
import java.util.List;

public class DeleteOldVersionOfObject {

public static void deleteOverloadedOne(String projectId, String bucketName, String objectName, long generationToDelete){
    bucketName = "my-delete-bucket-o1";
    String blobName1 = "my-blob-name1";
    String blobName2 = "my-blob-name2";
    Storage storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();
    BlobId firstBlob = BlobId.of(bucketName, blobName1);
    BlobId secondBlob = BlobId.of(bucketName, blobName2);
    storage.delete(firstBlob, secondBlob);
  }
}
```

![](../images/666371429.png)

### Support for PubSub

#### Supported PubSub APIs for SDK v1

The following APIs are supported:

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Object Created</th>
<th class="confluenceTh">Methods</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Publisher</td>
<td class="confluenceTd"
style="text-align: left;"><p>com.google.cloud.pubsub.v1.Publisher.publish</p>
<p>com.google.cloud.pubsub.v1.TopicAdminClient.publish</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Subscription</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>com.google.cloud.pubsub.v1.SubscriptionAdminClient.createSubscription</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Receiver</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>com.google.cloud.pubsub.v1.SubscriptionAdminClient.pull</p>
<p>com.google.cloud.pubsub.v1.Subscriber.startAsync</p>
<p>com.google.api.gax.rpc.UnaryCallable.call</p>
<p>com.google.api.gax.rpc.UnaryCallable.futureCall</p>
</div></td>
</tr>
</tbody>
</table>

-   For the publish method a Java GCP Pub/Sub Publisher object is
    created. Its name is of topic name. 
-   For the create subscription method, this extension evaluates the
    subscription name as well as the name of the topic to which the
    subscription is made. This information is used by the [com.castsoftware.wbslinker](../../../../../multi/com.castsoftware.wbslinker/) extension to link publishers with matching receivers.
-   For the startAsync or pull method a Java GCP Pub/Sub Receiver object
    is created. Its name is of the subscription.

#### What results you can expect?

When the name of a given referred publisher/subscriber in the code is
not resolved, an unknown publisher/receiver object will be created with
name Unknown.

The analysis of the following source code will create a Java GCP Pub/Sub
Publisher object with a callLink to a Java GCP Pub/Sub Receiver object.


Publisher example:

Publishing a message

``` java
import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.TopicName;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class PublisherExample {
  public static void main(String... args) throws Exception {
    String projectId = "your-project-id";
    String topicId = "topic_id_01";
    publisherExample(projectId, topicId);
  }
  public static void publisherExample(String projectId, String topicId)
      throws IOException, ExecutionException, InterruptedException {
    TopicName topicName = TopicName.of(projectId, topicId);

    Publisher publisher = null;
    try {
      publisher = Publisher.newBuilder(topicName).build();
      String message = "Hello World!";
      ByteString data = ByteString.copyFromUtf8(message);
      PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();
      ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
    } finally {
      if (publisher != null) {
        publisher.shutdown();
        publisher.awaitTermination(1, TimeUnit.MINUTES);
      }
    }
  }
}
```

Subscription example:

Create a subscription to a given topic

``` java
import com.google.cloud.pubsub.v1.SubscriptionAdminClient;
import com.google.pubsub.v1.PushConfig;
import com.google.pubsub.v1.Subscription;
import com.google.pubsub.v1.SubscriptionName;
import com.google.pubsub.v1.TopicName;
import java.io.IOException;

public class CreatePullSubscriptionExample {
  public static void main(String... args) throws Exception {
    String projectId = "your-project-id";
    String subscriptionId = "subscription_id_01";
    String topicId = "topic_id_01";
    createPullSubscriptionExample(projectId, subscriptionId, topicId);
  }
  public static void createPullSubscriptionExample(
      String projectId, String subscriptionId, String topicId) throws IOException {
    try (SubscriptionAdminClient subscriptionAdminClient = SubscriptionAdminClient.create()) {
      TopicName topicName = TopicName.of(projectId, topicId);
      SubscriptionName subscriptionName = SubscriptionName.of(projectId, subscriptionId);
      Subscription subscription =
          subscriptionAdminClient.createSubscription(
              subscriptionName, topicName, PushConfig.getDefaultInstance(), 10);
      System.out.println("Created pull subscription: " + subscription.getName());
    }
  }
}
```

Receiver example:

Receiving a message

``` java
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SubscribeAsyncExample {
  public static void main(String... args) throws Exception {
    String projectId = "your-project-id";
    String subscriptionId = "subscription_id_01";
    subscribeAsyncExample(projectId, subscriptionId);
  }
  public static void subscribeAsyncExample(String projectId, String subscriptionId) {
    ProjectSubscriptionName subscriptionName =
        ProjectSubscriptionName.of(projectId, subscriptionId);
    MessageReceiver receiver =
        (PubsubMessage message, AckReplyConsumer consumer) -> {
          consumer.ack();
        };
    Subscriber subscriber = null;
    try {
      subscriber = Subscriber.newBuilder(subscriptionName, receiver).build();
      subscriber.startAsync().awaitRunning();
      subscriber.awaitTerminated(30, TimeUnit.SECONDS);
    } catch (TimeoutException timeoutException) {
      subscriber.stopAsync();
    }
  }
}
```

![](../images/666371428.png)


### Data sensitivity

This extension is capable of setting a property on GCP Cloud Storage Bucket objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

See [Data Sensitivity](../../../../../multi/data-sensitivity/) for more information.

## Known limitations

-   LinkType is not evaluated for mutateRow calls, all linktypes are
    created by default.