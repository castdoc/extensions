---
title: "GCP .NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.gcp

## What's new

See [Release Notes](rn/).

## In what situation should you install this extension?

This extension should be used for analyzing .NET source code using any of the supported Google Cloud Platform Bigtable and CloudStorage services. 

## Supported services and frameworks

| Framework →<br> <br>Services ↓ | SDK <br> <br>2.x | SDK <br> <br>3.x | SDK <br> <br>4.x |
|---|:---:|:---:|:---:|
| Bigtable | :white_check_mark: | :x: | N/A |
| CloudStorage | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| PubSub | :white_check_mark: | :white_check_mark: |   N/A |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Objects

| Icon | Description |
|---|---|
| ![](../images/666371397.png) | DotNet GCP Bigtable Table |
| ![](../images/666371396.png) | DotNet GCP Unknown Bigtable Table |
| ![](../images/666371395.png) | DotNet GCP CloudStorage Bucket |
| ![](../images/666371394.png) | DotNet GCP CloudStorage Unknown Bucket |
| ![](../images/666371393.png) | DotNet GCP PubSub Publisher |
| ![](../images/666371392.png) | DotNet GCP PubSub Unknown Publisher |
| ![](../images/666371390.png) | DotNet GCP PubSub Receiver |
| ![](../images/666371389.png) | DotNet GCP PubSub Unknown Receiver |

## Support for SDK

### Support for Cloud Bigtable APIs for SDK v2

For the following methods of the clients `Google.Cloud.Bigtable.V2.BigtableClient` and `Google.Cloud.Bigtable.Admin.V2.BigtableTableAdminClient`, links are created to the Table:

| Link Type | Client | Methods |
|---|---|---|
| useInsertLink | Google.Cloud.Bigtable.V2.BigtableClient | mutateRow, mutateRows, mutateRowAsync, mutateRowsAsync<br> <br>checkAndMutateRow, checkAndMutateRowAsync |
| useUpdateLink | Google.Cloud.Bigtable.V2.BigtableClient | readModifyWriteRow, readModifyWriteRowAsync<br> <br>mutateRow, mutateRows, mutateRowAsync, mutateRowsAsync<br> <br>checkAndMutateRow, checkAndMutateRowAsync  |
| useSelectLink | Google.Cloud.Bigtable.V2.BigtableClient | readRow, readRowAsync, readRows, SampleRowKeys  |
| useDeleteLink | Google.Cloud.Bigtable.V2.BigtableClient | mutateRow, mutateRows,  mutateRowAsync, mutateRowsAsync<br> <br>checkAndMutateRow, checkAndMutateRowAsync  |
| useDeleteLink | Google.Cloud.Bigtable.Admin.V2.BigtableTableAdminClient | dropAllRows, dropAllRowsAsync, dropRowRange, dropRowRangeAsync |

#### What results you can expect?

The analysis of the following source code will create a `Dotnet GCP Bigtable Table` object named `Hello-Bigtable` with a `useSelectLink`.

When the name of a given referred table in the code is not resolved, an unknown table object will be created with name `Unknown`. There can be a maximum of one unknown table per project.

Example:

``` c#
using Google.Cloud.Bigtable.Admin.V2;
using Google.Cloud.Bigtable.V2;
using System;

namespace GoogleCloudSamples.Bigtable
{
    public class HelloWorld
    {

        private const string projectId = "YOUR-PROJECT-ID";
        private const string instanceId = "YOUR-INSTANCE-ID";
        private const string tableId = "Hello-Bigtable";

        Google.Cloud.Bigtable.Common.V2.TableName tableName = new Google.Cloud.Bigtable.Common.V2.TableName(projectId, instanceId, tableId);
        BigtableTableAdminClient bigtableTableAdminClient = BigtableTableAdminClient.Create();
        BigtableClient bigtableClient = BigtableClient.Create();

        bigtableTableAdminClient.CreateTable(new InstanceName(projectId, instanceId,tableId)
                        
        private static void ReadRowMethod()
        {
            
            RowFilter filter = RowFilters.CellsPerRowLimit(1);
            int rowIndex = 0;

            Row rowRead = bigtableClient.ReadRow(tableName, rowKey: rowKeyPrefix + rowIndex, filter: filter); 
        }
     }
}
```

![](../images/666371388.png)

### Support for CloudStorage

#### Supported CloudStorage APIs for SDK v2, v3 and v4

For the following methods of the client
`Google.Cloud.Storage.V1.StorageClient`, we create links to the `Bucket`:

| Link Type | Client | Methods |
|---|---|---|
| useInsertLink | Google.Cloud.Storage.V1.StorageClient | UploadObject, UploadObjectAsync |
| useUpdateLink | Google.Cloud.Storage.V1.StorageClient | CopyObject  |
| useSelectLink | Google.Cloud.Storage.V1.StorageClient | DownloadObject, DownloadObjectAsync<br> <br>CopyObject  |
| useDeleteLink | Google.Cloud.Storage.V1.StorageClient | DeleteObject, DeleteObjectAsync  |

#### What results you can expect?

The analysis of the following source code will create a
`Dotnet GCP CloudStorage Bucket` object named `your-unique-bucket-name` with a `useDeleteLink`.

When the name of a given referred bucket in the code is not resolved, an unknown bucket object will be created with name `Unknown`. There can be a maximum of one unknown bucket per project.

Example:

``` c#
using Google.Cloud.Storage.V1;
using System;

public class DeleteFileSample
{
    public void DeleteFile(
        string bucketName = "your-unique-bucket-name",
        string objectName = "your-object-name")
    {
        var storage = StorageClient.Create();
        storage.DeleteObject(bucketName, objectName);
        Console.WriteLine($"Deleted {objectName}.");
    }
}
```

![](../images/666371387.png)

### Support for PubSub

#### Supported PubSub APIs for SDK v2 and v3

The following APIs are supported:

| Object Created | Methods |
|---|---|
| Publisher | Google.Cloud.PubSub.V1.PublisherServiceApiClient.Publish<br> <br>Google.Cloud.PubSub.V1.PublisherServiceApiClient.PublishAsync<br> <br>Google.Cloud.PubSub.V1.PublisherClient.PublishAsync<br> <br>Google.Cloud.PubSub.V1.PublisherClientImpl.PublishAsync |
| Subscription | Google.Cloud.PubSub.V1.SubscriberServiceApiClient.CreateSubscription<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClient.CreateSubscriptionAsync<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClientImpl.CreateSubscription<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClientImpl.CreateSubscriptionAsync  |
| Receiver | Google.Cloud.PubSub.V1.SubscriberClient.StartAsync<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClient.StartAsync<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClient.Pull<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClient.PullAsync<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClient.StreamingPull<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClientImpl.Pull<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClientImpl.PullAsync<br> <br>Google.Cloud.PubSub.V1.SubscriberServiceApiClientImpl.StreamingPull  |

-   For the Publisher methods, a `Dotnet GCP PubSub Publisher` object is created. Its name is that of the topic.
-   For the Receiver methods, a `Dotnet GCP PubSub Receiver` object is created. Its name is that of the subscription. 
-   For the create subscription method, this extension evaluates the subscription name as well as the name of the topic to which the subscription is made. This information is used by the [com.castsoftware.wbslinker](../../../../../multi/com.castsoftware.wbslinker/) extension to link publishers with matching receivers.

#### What results you can expect?

The analysis of the following source code will create a `Dotnet GCP PubSub Publisher` object named `topicId` with a `callLink` to a `Dotnet GCP PubSub Receiver`.

When the name of a given referred publisher in the code is not resolved, an unknown publisher object will be created with name `Unknown`. When the
name of a given referred subscriber in the code is not resolved, an unknown receiver object will be created with name `Unknown`.

Publisher example:

``` c#
public class PublishMessagesAsyncSample
{
    public async Task<int> PublishMessagesAsync(string project, string topic, IEnumerable<string> messageTexts)
    {
        TopicName topicName = TopicName.FromProjectTopic("projectId", "topicId");
        PublisherClient publisher = await PublisherClient.CreateAsync(topicName);

        int publishedMessageCount = 0;
        var publishTasks = messageTexts.Select(async text =>
        {
            try
            {
                string message = await publisher.PublishAsync(text);
                Console.WriteLine($"Published message {message}");
                Interlocked.Increment(ref publishedMessageCount);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"An error ocurred when publishing message {text}: {exception.Message}");
            }
        });
        await Task.WhenAll(publishTasks);
        return publishedMessageCount;
    }
}
```

Subscription example:

``` c#
public class CreateSubscriptionSample
{
    public Subscription CreateSubscription(string project, string topic, string subscription)
    {
        SubscriberServiceApiClient subscriber = SubscriberServiceApiClient.Create();
        TopicName topicName = TopicName.FromProjectTopic("projectId", "topicId");

        SubscriptionName subscriptionName = SubscriptionName.FromProjectSubscription("projectId", "subscriptionId");
        Subscription subscription = null;

        try
        {
            subscription = subscriber.CreateSubscription(subscriptionName, topicName, pushConfig: null, ackDeadlineSeconds: 60);
        }
        catch (RpcException e) when (e.Status.StatusCode == StatusCode.AlreadyExists)
        {
            // Already exists.  That's fine.
        }
        return subscription;
    }
}
```

Receiver example:

``` c#
public class PullMessagesAsyncSample
{
    public async Task<int> PullMessagesAsync(string project, string subscription, bool acknowledge)
    {
        SubscriptionName subscriptionName = SubscriptionName.FromProjectSubscription("projectId", "subscriptionId");
        SubscriberClient subscriber = await SubscriberClient.CreateAsync(subscriptionName);
        // SubscriberClient runs your message handle function on multiple
        // threads to maximize throughput.
        int messageCount = 0;
        Task startTask = subscriber.StartAsync((PubsubMessage message, CancellationToken cancel) =>
        {
            string text = System.Text.Encoding.UTF8.GetString(message.Data.ToArray());
            Console.WriteLine($"Message {message.MessageId}: {text}");
            Interlocked.Increment(ref messageCount);
            return Task.FromResult(acknowledge ? SubscriberClient.Reply.Ack : SubscriberClient.Reply.Nack);
        });
        // Run for 5 seconds.
        await Task.Delay(5000);
        await subscriber.StopAsync(CancellationToken.None);
        // Lets make sure that the start task finished successfully after the call to stop.
        await startTask;
        return messageCount;
    }
}
```

![](../images/dotnet_pubsub.png)

### Data sensitivity

This extension is capable of setting a property on GCP Cloud Storage Bucket objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

See [Data Sensitivity](../../../../../multi/data-sensitivity/) for more information.

## Known limitations

-   LinkType is not evaluated for mutateRow calls, all linkTypes are created by default for bigtable.
