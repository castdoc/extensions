---
title: "GCP Bigtable"
linkTitle: "GCP Bigtable"
type: "docs"
---

>The support for GCP Bigtable is distributed among various CAST
extensions. This page summarizes supported features for the latest
version of the corresponding CAST extension.

## List of supported languages deployment frameworks

The following table summarizes supported packages for GCP Bigtable for
different languages:

| Language |  Supported Library |
|----------|--------------------|
| Node.js (JavaScript) | ![(tick)](/images/icons/emoticons/check.svg) (@google-cloud/bigtable)            |
| Node.js (TypeScript) | ![(tick)](/images/icons/emoticons/check.svg) (@google-cloud/bigtable)            |
| Java                 | ![(tick)](/images/icons/emoticons/check.svg) (com.google.cloud.bigtable.data.v2) |
| .NET                 | ![(tick)](/images/icons/emoticons/check.svg) (Google.Cloud.Bigtable.V2)          |

More information about support for each language can be found on the
pages linked below:

-   [Support of GCP Bigtable for Java](../../extensions/com.castsoftware.java.gcp/)
-   [Support of GCP Bigtable for .NET](../../extensions/com.castsoftware.dotnet.gcp/)
-   [Support of GCP Bigtable for Node.js (JavaScript)](../../../../web/nodejs/)
-   [Support of GCP Bigtable for Node.js(TypeScript)](../../../../web/typescript/)