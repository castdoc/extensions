---
title: "GCP Cloud Storage"
linkTitle: "GCP Cloud Storage"
type: "docs"
---

>The support for GCP Cloud Storage is distributed among various CAST
extensions. This page summarizes supported features for the latest
version of the corresponding CAST extension.

## List of supported languages deployment frameworks

The following table summarizes supported packages for GCP Cloud Storage
for different languages:

| Language  | Supported Library |
|-----------|-------------------|
| Node.js (JavaScript) | ![(tick)](/images/icons/emoticons/check.svg) (@google-cloud/storage)                 |
| Node.js (TypeScript) | ![(tick)](/images/icons/emoticons/check.svg) (@google-cloud/storage)                 |
| Java                 | ![(tick)](/images/icons/emoticons/check.svg) (com.google.cloud.storage)              |
| .NET                 | ![(tick)](/images/icons/emoticons/check.svg) (Google.Cloud.Storage.V1.StorageClient) |

More information about support for each language can be found on the
pages linked below:

- [Support of GCP Cloud Storage for Java](../../extensions/com.castsoftware.java.gcp/)
- [Support of GCP Cloud Storage for .NET](../../extensions/com.castsoftware.dotnet.gcp/)
- [Support of GCP Cloud Storage for Node.js (JavaScript)](../../../../web/nodejs/)
- [Support of GCP Cloud Storage for Node.js (TypeScript)](../../../../web/typescript/)