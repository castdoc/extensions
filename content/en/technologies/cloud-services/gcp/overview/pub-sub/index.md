---
title: "GCP Pub Sub"
linkTitle: "GCP Pub Sub"
type: "docs"
---

>The support for GCP is distributed among various CAST extensions. This
page summarizes supported features for the latest version of the
corresponding CAST extension.

## List of supported languages deployment frameworks

The following table summarizes supported packages for GCP Pub/Sub for
different languages:

| Language  | Supported Library |
|------------|------------------|
| Java                 | ![(tick)](/images/icons/emoticons/check.svg) (com.google.cloud.pubsub.v1) |
| .NET                 | ![(tick)](/images/icons/emoticons/check.svg) (Google.Cloud.PubSub.V1)     |
| Node.js (JavaScript) | ![(tick)](/images/icons/emoticons/check.svg) (@google-cloud/pubsub)       |
| Node.js (TypeScript) | ![(tick)](/images/icons/emoticons/check.svg) (@google-cloud/pubsub)       |

More information about support for each language can be found on the
pages linked below:

- [Support of GCP Pub/Sub for Java](../../extensions/com.castsoftware.java.gcp/)
- [Support of GCP Pub/Sub for .NET](../../extensions/com.castsoftware.dotnet.gcp/)
- [Support of GCP Pub/Sub for Node.js (JavaScript)](../../../../web/nodejs/)
- [Support of GCP Pub/Sub for Node.js (TypeScript)](../../../../web/typescript/)