---
title: "Azure Blobs"
linkTitle: "Azure Blobs"
type: "docs"
---

Azure Blob Storage is Microsoft's object storage solution for the cloud. It is designed to store large amounts of unstructured data, such as text or binary data

>Support for Microsoft Azure technologies is distributed among various CAST extensions. This
page summarizes supported features for the latest version of the
corresponding CAST extension.

## List of supported languages deployment frameworks

The following table summarizes supported packages for Azure Blobs for
different technologies:

| Language              | Supported Library                                                                                     |
|-----------------------|-------------------------------------------------------------------------------------------------------|
| Java                  | com.azure.storage.blob                                                                                 |
|                       | com.microsoft.azure.storage.blob                                                                      |
| .NET                  | Azure.Storage.Blobs                                                                                   |
| Node.js (JavaScript)  | @azure/storage-blob                                                                                   |
|                       | azure-storage                                                                                         |
| Node.js (TypeScript)  | @azure/storage-blob                                                                                   |
|                       | azure-storage                                                                                         |


More information about support for each language can be found on the
pages linked below:

-   [Support of Azure Blobs for Node.js (JavaScript)](../../../../web/nodejs/com.castsoftware.nodejs/)
-   [Support of Azure Blobs for Node.js (TypeScript)](../../../../web/typescript/com.castsoftware.typescript/)
-   [Support of Azure Blobs for Java](../../extensions/com.castsoftware.azure.java/)
-   [Support of Azure Blobs for .NET](../../extensions/com.castsoftware.azure.dotnet/)
