---
title: "Azure Cosmos DB"
linkTitle: "Azure Cosmos DB"
type: "docs"
---

Azure Cosmos DB is a globally distributed, multi-model database service provided by Microsoft Azure. It is designed to enable developers to build highly responsive and available applications across multiple geographical locations

>Support for Microsoft Azure technologies is distributed among various CAST extensions. This
page summarizes supported features for the latest version of the
corresponding CAST extension.

## List of supported languages deployment frameworks

The following table summarizes supported packages for Azure CosmosDB for
different languages:

| Language              | Supported Library                                                                                     |
|-----------------------|-------------------------------------------------------------------------------------------------------|
| Java                  | com.microsoft.azure.documentdb                                                                        |
|                       | com.microsoft.azure.cosmosdb.rx                                                                       |
|                       | com.azure.cosmos                                                                                      |
| .NET                  | Microsoft.Azure.Documents                                                                             |
|                       | Microsoft.Azure.Cosmos                                                                               |
|                       | Cosmonaut.CosmonautClient                                                                            |
| Node.js (JavaScript)  | @azure/cosmos                                                                                         |
| Node.js (TypeScript)  | @azure/cosmos                                                                                         |


More information about support for each language can be found on the
pages linked below:

-   [Support of Cosmos DB for Java](../../../../nosql/overview/azure-cosmos-db/java/)
-   [Support of Cosmos DB for .NET](../../../../nosql/overview/azure-cosmos-db/dotnet/)
-   [Support of Cosmos DB for Node.js (JavaScript)](../../../../nosql/overview/azure-cosmos-db/nodejs/)
-   [Support of Cosmos DB for Node.js (TypeScript)](../../../../web/typescript/com.castsoftware.typescript/)
-   [Support of Cosmos DB for Spring Data](../../../../nosql/overview/azure-cosmos-db/spring-data/)
