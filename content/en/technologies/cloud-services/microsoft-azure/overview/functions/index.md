---
title: "Azure Functions"
linkTitle: "Azure Functions"
type: "docs"
---

>Support for Microsoft Azure technologies is distributed among various CAST extensions. This
page summarizes supported features for the latest version of the
corresponding CAST extension.


## List of supported languages deployment frameworks

The following table summarizes supported packages for Azure Functions
for different languages:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p><strong>Language</strong></p></th>
<th class="confluenceTh" style="text-align: left;">Supported
Library</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Java</td>
<td class="confluenceTd" style="text-align: left;"><p><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick"
alt="(tick)" /> com.microsoft.azure.functions</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">.NET</td>
<td class="confluenceTd" style="text-align: left;"><p><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /> Microsoft.Azure.WebJobs</p>
<p><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /> Microsoft.Azure.Functions.Worker</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Node.js (JavaScript)</td>
<td class="confluenceTd" style="text-align: left;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /> </td>
</tr>
<tr class="even">
<td class="confluenceTd">Node.js (TypeScript)</td>
<td class="confluenceTd" style="text-align: left;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /> </td>
</tr>
</tbody>
</table>

More information about support for each language can be found on the
pages linked below:

-   [Support of Azure Functions for Java](../../extensions/com.castsoftware.azure.java/)
-   [Support of Azure Functions for .NET](../../extensions/com.castsoftware.azure.dotnet/)
-   [Support of Azure Functions for Node.js (JavaScript)](../../../../web/nodejs/com.castsoftware.nodejs/)
-   [Support of Azure Functions for Node.js (TypeScript)](../../../../web/typescript/com.castsoftware.typescript/)
