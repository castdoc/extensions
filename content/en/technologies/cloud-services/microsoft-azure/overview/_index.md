---
title: "Support overview"
linkTitle: "Support overview"
type: "docs"
weight: 50
no_list: false
---

>Support for Microsoft Azure technolgies is distributed among several of CAST extensions. This page and its sub-pages summarize what is supported for Microsoft Azure when using the most recent versions of CAST extensions. 