---
title: "Microsoft Azure"
linkTitle: "Microsoft Azure"
type: "docs"
no_list: false
---

Microsoft Azure is a cloud computing service created by Microsoft for building, testing, deploying, and managing applications and services through Microsoft-managed data centers. It provides a broad range of cloud services, including those for computing, analytics, storage, and networking.

| Key Features                    | Category                      | Description                                                                                   | Support |
|---------------------------------|-------------------------------|-----------------------------------------------------------------------------------------------|---------|
| Azure Active Directory          | Security and Management   | Identity and access management, providing SSO, multifactor authentication, and conditional access |         |
| Azure Key Vault                 | Security and Management**   | Secure storage for secrets, keys, and certificates                                            |         |
| Azure Virtual Machines          | Compute                   | Deploy scalable virtualized computing environments                                            |         |
| Azure Kubernetes Service (AKS)  | Compute                   | Simplifies the deployment and management of Kubernetes clusters                               |         |
| **App Service**                     | Web and Mobile            | Host web apps, RESTful APIs, and mobile backends                                              |    :heavy_check_mark:    |
| **Azure Functions**                 | Web and Mobile            | Serverless compute service to run code on-demand without managing infrastructure               |    :heavy_check_mark:    |
| Visual Studio                   | Developer Services        | Integrated development environment (IDE) for building, testing, and deploying applications     |         |
| Azure DevOps                    | Developer Services        | Development collaboration tools, including CI/CD pipelines                                    |         |
| Azure Arc                       | Hybrid Operations         | Extends Azure management across on-premises, multi-cloud, and edge environments                |         |
| Azure Stack                     | Hybrid Operations         | Allows running Azure services in on-premises datacenters                                      |         |
| **Azure Service Bus**               | Integration**               | Managed enterprise message broker for reliable messaging between applications                 |    :heavy_check_mark:    |
| **Azure Event Grid**                | Integration**               | Enables event-based architectures by managing event routing                                   |    :heavy_check_mark:    |
| Azure Media Services            | Media and CDN**             | Media processing and delivery solutions                                                       |         |
| Azure Content Delivery Network (CDN) | Media and CDN           | High-bandwidth content delivery to users globally                                          |         |
| Azure IoT Hub                   | Analytics and IoT         | Facilitates communication between IoT applications and devices                                |         |
| Azure Machine Learning          | Analytics and IoT         | Comprehensive service for building, training, and deploying machine learning models           |         |
| Azure Stream Analytics          | Analytics and IoT         | Real-time data stream processing service                                                      |         |
| **Azure SQL Database**              | Data                      | Managed relational database service                                                           |     :heavy_check_mark:   |
| **Azure Cosmos DB**                 | Data                  | Globally distributed database service for managing data at scale                              |     :heavy_check_mark:   |
| Azure Data Lake Storage         | Data                   | Scalable storage for big data analytics                                                       |         |
| Azure Virtual Network           | Networking                | Core service for creating isolated networks within Azure                                      |         |
| Azure ExpressRoute              | Networking                | Provides dedicated private connections between Azure datacenters and on-premises infrastructure |         |
| Azure Application Gateway       | Networking               | Application-level routing and load balancing                                                  |         |
