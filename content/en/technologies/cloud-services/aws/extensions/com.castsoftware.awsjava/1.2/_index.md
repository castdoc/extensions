---
title: "AWS Java - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.awsjava

## What's new?

See [Release Notes](rn/).

## Supported services and frameworks

| Services | Framework: SDK 1.x, 2.x |
|---|:-:|
| S3 | :white_check_mark: |
| Lambda creation and link to handler | :x: |
| Lambda invoke | :white_check_mark: |
| SNS publish | :white_check_mark: |
| SNS subscription: http | :white_check_mark: |
| SNS subscription: email/SMS | :white_check_mark: |
| SNS subscription: sqs | :white_check_mark: |
| SNS subscription: lambda | :white_check_mark: |

Note that:
- the support of SQS services with SDK for java is provided by
[com.castsoftware.mqe](https://extend.castsoftware.com/#/extension?id=com.castsoftware.mqe&version=latest).
- the support of AWS dynamodb services with SDK for Java is provided by
[com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest).

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## What results can I expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/666370904.png) | Java Call to AWS Lambda Function |
| ![](../images/666370903.png) | Java Call to AWS unknown Lambda Function |
| ![](../images/666370902.png) | Java AWS Simple Queue Service Receiver |
| ![](../images/666370901.png) | Java AWS Simple Queue Service Publisher |
| ![](../images/666370900.png) | Java AWS Unknown Simple Queue Service Receiver |
| ![](../images/666370899.png) | Java AWS Unknown Simple Queue Service Publisher |
| ![](../images/666370898.png) | Java AWS SNS Publisher |
| ![](../images/666370897.png) | Java AWS SNS Subscriber |
| ![](../images/666370896.png) | Java AWS SNS Unknown Publisher |
| ![](../images/666370899.png) | Java AWS SNS Unknown Subscriber |
| ![](../images/666370895.png) | Java AWS SMS, Java AWS Email |
| ![](../images/666370894.png) | Java AWS Post Resource Service |
| ![](../images/666370893.png) | Java AWS S3 Bucket |
| ![](../images/666370892.png) | Java AWS unknown S3 Bucket |

## Supported elements

### Support for Lambda functions 

-   Lambda services allow executing some source code on the cloud. The
    execution can be set to be triggered by some AWS events.  
-   Lambda functions can be deployed using several deployment
    frameworks. The supported deployment frameworks are listed on [AWS
    Lambda](AWS_Lambda).
-   When a lambda function is created and its runtime is java, the
    current extension is responsible for linking the lambda objects and
    their triggers with the java handler functions.

#### Example

Let us consider a source code defining a lambda function having a java
runtime (for instance java8) and the handler function is given by the
handler function fullname. If the lambda function is deployed using a
supported deployment framework (such as CloudFormation), the analysis
will create a lambda function. If the current extension finds a java
method matching the handler fullname a link to that java method will be
added from the lambda function.

![](../images/666370891.png)

Some applications are using a monolithic pattern: only one handler
function is used for many (if not all) API Gateways. That handler
function then dispatches the call to sub-handler functions using
switches based on the URLs. Ideally, in the modelization, each API
Gateway should be linked to its dedicated sub-handler function. However,
in our modelization, all API Gateways will be linked to the root handler
function. 

### Support for S3

#### Supported S3 APIs for SDK v1

For the following methods of the com.amazonaws.services.s3.AmazonS3
client we create links to the bucket:

| Link Type | Methods |
|---|---|
| useUpdateLink | putObject, uploadPart |
| useInsertLink | putObject, uploadPart |
| useSelectLink | getObject, listObjects, listObjectsV2, getObjectAsString, listObjects, listObjectsV2 |
| useDeleteLink | deleteBucket, deleteObject, deleteObjects |
| callLink | changeObjectStorageClass<br>deleteBucketAnalyticsConfiguration<br>deleteBucketCrossOriginConfiguration<br>deleteBucketEncryption<br>deleteBucketIntelligentTieringConfiguration<br>deleteBucketInventoryConfiguration<br>deleteBucketLifecycleConfiguration<br>deleteBucketMetricsConfiguration<br>deleteBucketPolicy<br>deleteBucketReplicationConfiguration<br>deleteBucketTaggingConfiguration<br>deleteBucketWebsiteConfiguration<br>deleteObjectTagging<br>deleteVersion<br>disableRequesterPays<br>doesBucketExist<br>doesBucketExistV2<br>doesObjectExist<br>download<br>enableRequesterPays<br>generatePresignedUrl<br>getBucketAccelerateConfiguration<br>getBucketAcl<br>getBucketAnalyticsConfiguration<br>getBucketCrossOriginConfiguration<br>getBucketIntelligentTieringConfiguration<br>getBucketInventoryConfiguration<br>getBucketLifecycleConfiguration<br>getBucketLocation<br>getBucketLoggingConfiguration<br>getBucketMetricsConfiguration<br>getBucketNotificationConfiguration<br>getBucketOwnershipControls<br>getBucketPolicy<br>getBucketReplicationConfiguration<br>getBucketTaggingConfiguration<br>getBucketVersioningConfiguration<br>getBucketWebsiteConfiguration<br>getObjectAcl<br>AccessControlList<br>getObjectMetadata<br>getObjectTagging<br>getUrl<br>headBucket<br>initiateMultipartUpload<br>isRequesterPaysEnabled<br>listParts<br>listVersions<br>restoreObject<br>restoreObjectV2<br>setBucketAccelerateConfiguration<br>setBucketAcl<br>setBucketAnalyticsConfiguration<br>setBucketCrossOriginConfiguration<br>setBucketIntelligentTieringConfiguration<br>setBucketInventoryConfiguration<br>setBucketLifecycleConfiguration<br>setBucketLoggingConfiguration<br>setBucketMetricsConfiguration<br>setBucketNotificationConfiguration<br>setBucketOwnershipControls<br>setBucketPolicy<br>setBucketReplicationConfiguration<br>setBucketTaggingConfiguration<br>setBucketVersioningConfiguration<br>setBucketWebsiteConfiguration<br>setObjectAcl<br>setObjectRedirectLocation<br>setObjectTagging<br>setEndpoint<br>setRequestPaymentConfiguration<br>shutdown<br>createBucket |

#### Supported S3 APIs for SDK v2

For the following methods of
the software.amazon.awssdk.services.s3.S3Client client we create links
to the bucket:

| Link Type | Methods |
|---|---|
| useUpdateLink | putObject, uploadPart, restoreObject |
| useInsertLink | completeMultipartUpload, putObject |
| useSelectLink | getObject, getObjectAsBytes, listObjects, listObjectsV2, listObjectsV2Paginator |
| useDeleteLink | deleteBucket, deleteObject, deleteObjects |
| callLink | abortMultipartUpload<br>createBucket<br>createMultipartUpload<br> deleteBucketAnalyticsConfiguration<br> deleteBucketCors<br> deleteBucketEncryption<br> deleteBucketIntelligentTieringConfiguration<br> deleteBucketInventoryConfiguration<br> deleteBucketLifecycle<br> deleteBucketMetricsConfiguration<br> deleteBucketOwnershipControls<br> deleteBucketPolicy<br> deleteBucketReplication<br> deleteBucketTagging<br> deleteBucketWebsite<br> deleteObjectTagging<br> deletePublicAccessBlock<br> getBucketAccelerateConfiguration<br> getBucketAcl<br> getBucketAnalyticsConfiguration<br> getBucketCors<br> getBucketEncryption<br> getBucketIntelligentTieringConfiguration<br> getBucketInventoryConfiguration<br> getBucketLifecycleConfiguration<br> getBucketLocation<br> getBucketLogging<br> getBucketMetricsConfiguration<br> getBucketNotificationConfiguration<br> getBucketOwnershipControls<br> getBucketPolicy<br> getBucketPolicyStatus<br> getBucketReplication<br> getBucketRequestPayment<br> getBucketTagging<br> getBucketVersioning<br> getBucketWebsite<br> getObjectAcl<br> getObjectLegalHold<br> getObjectLockConfiguration<br> getObjectRetention<br> getObjectTagging<br> getObjectTorrent<br> getObjectTorrentAsBytes<br> getPublicAccessBlock<br> headBucket<br> headObject<br> listBucketAnalyticsConfigurations<br> listBucketIntelligentTieringConfigurations<br> listBucketInventoryConfigurations<br> listBucketMetricsConfigurations<br> listMultipartUploads<br> listMultipartUploadsPaginator<br> listObjectVersions<br> listObjectVersionsPaginator<br> listParts<br> listPartsPaginator<br> putBucketAccelerateConfiguration |

##### Example

This code will create a S3 Bucket named "foo_bucket" on an AWS server in
region "AP_SOUTH_1" and puts an object in it

``` java
package aws.example.s3;

import com.amazonaws.regions.Regions;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import com.amazonaws.services.s3.model.Bucket;

import java.io.File;
import java.util.List;

public class S3Manager {

    public void createBucket() {
        Region region = Region.getRegion(Regions.AP_SOUTH_1);
        final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(region).build();
        Bucket b = s3.createBucket("foo_bucket");
    }
    
    public void  putFile(String file_path) {
        Region region = Region.getRegion(Regions.AP_SOUTH_1);
        final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(region).build();
        String key_name = Paths.get(file_path).getFileName().toString();
        s3.putObject("foo_bucket", key_name, new File(file_path));
    }
}
```

![](../images/666370890.png)

#### Known limitations for S3

-   createPresignedPost API is not supported. However the call to this
    method is represented by a callLink to the bucket similar to other
    API methods in S3Client.

### Support for Lambda invocation

Only the invoke method
of software.amazon.awssdk.services.lambda.LambdaClient (SDK V1)
and com.amazonaws.services.lambda.AWSLambda (SDK V2) is currently
supported.

#### Example

The analysis of the following source code will create a Java Call to AWS
Lambda Function object named functionName with a callLink from the
invokeFunction to that object:

``` java
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.ServiceException;

import java.nio.charset.StandardCharsets;

public class FooClass {   

    public static void invokeFunction(LambdaClient awsLambda, String functionName) {

        InvokeRequest invokeRequest = new InvokeRequest()
                .withFunctionName("functionName")
                .withPayload("{\n" +
                        " \"Hello \": \"Paris\",\n" +
                        " \"countryCode\": \"FR\"\n" +
                        "}");
        InvokeResult invokeResult = null;

        AWSLambda awsLambda = AWSLambdaClientBuilder.standard()
                                  .withCredentials(new ProfileCredentialsProvider())
                                  .withRegion(Regions.US_WEST_2).build();

        invokeResult = awsLambda.invoke(invokeRequest);
   }
}
```

#### @LambdaFunction annotation

The following two annotations (equally named) are supported (one for
Android and the other one for Java applications):

-   com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction
-   com.amazonaws.services.lambda.invoke.LambdaFunction

The example below reproduced from the official
documentation [https://docs.aws.amazon.com/lambda/latest/dg/with-android-example.html](https://docs.aws.amazon.com/lambda/latest/dg/with-android-example.html),
illustrates how one can define (but not implement) the lambda function
by annotating the AndroidBackendLambdaFunction method. 

``` java
// https://docs.aws.amazon.com/lambda/latest/dg/with-android-example.html
import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction;
public interface MyInterface {

    /
     * Invoke the Lambda function "AndroidBackendLambdaFunction". 
     * The function name is the method name.
     */
    @LambdaFunction
     ResponseClass AndroidBackendLambdaFunction(RequestClass request);

}
```

The @LambdaFunction annotation maps the specific client method to the
same-name Lambda function. The expected behavior of the JEE analyzer is
to resolve the actual Lambda Function invocation via method calls
(elsewhere) to the annotated interface methods. The
com.castsoftware.awsjava extension on the other hand will create a Java
Call to AWS Lambda Function object and the respective incoming callLink
from the annotated interface method AndroidBackendLambdaFunction.

### Support for SNS

The following APIs are supported:

For SDK V1:

-   com.amazonaws.services.sns.AmazonSNS.publish
-   com.amazonaws.services.sns.util.Topics.subscribeQueue
-   com.amazonaws.services.sns.AmazonSNS.subscribe

For SDK V2

-   software.amazon.awssdk.services.sns.SnsClient.publish
-   software.amazon.awssdk.services.sns.SnsClient.subscribe

For the publish method a Java AWS SNS Publisher object is created. Its
name is that of the topic. For the subscribe methods, a Java AWS SNS
Subscriber object is created. Its name is that of the topic. Then for
each supported protocol, an object is created with a callLink from the
subscriber to that object. The supported protocols are the following:

| Protocol   | Object created                          | Name of the object                                            |
|------------|-----------------------------------------|---------------------------------------------------------------|
| email      | Java AWS Email                          | an Email   (the email addresses are not evaluated)            |
| sms        | Java AWS SMS                            | an SMS   (the SMS numbers are not evaluated)                  |
| http/https | Java AWS Post Service                   | the url (evaluated from the endpoint)                         |
| sqs        | Java AWS Simple Queue Service Publisher | the name of the queue (evaluated from the endpoint)           |
| lambda     | Java Call to AWS Lambda Function        | the name of the lambda function (evaluated from the endpoint) |

The
[com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest)
extension will create a callLink between the SNS Publishers and SNS
Subscribers which have the same name.

#### Example

When analyzing the following source code:

``` java
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sns.model.SubscribeRequest;

public class Example {

    public static void main(String[] args) {
        final String TOPIC_NAME = "extended-client-topic";
        final Regions region = Regions.DEFAULT_REGION;

        final AmazonSNS snsClient = AmazonSNSClientBuilder.standard().withRegion(region).build();
        final String topicArn = snsClient.createTopic(
                new CreateTopicRequest().withName("TOPIC1")
        ).getTopicArn();
        snsClient.publish(topicArn, "message");
   }

    public static void subscribe(String[] args) {
        final String TOPIC_NAME = "extended-client-topic";
        final Regions region = Regions.DEFAULT_REGION;

        final AmazonSNS snsClient = AmazonSNSClientBuilder.standard().withRegion(region).build();
        final String topicArn1 = snsClient.createTopic(
                new CreateTopicRequest().withName("TOPIC1")
        ).getTopicArn();
        snsClient.subscribe(topicArn1, "email", "lili@lala.com");
        snsClient.subscribe(new SubscribeRequest(topicArn1, "email", "lili@lala.com"));
        snsClient.subscribe(new SubscribeRequest(topicArn1, "sqs", "arn:partition:service:region:account-id:queueName"));
        snsClient.subscribe(new SubscribeRequest()
                                   .setTopicArn(topicArn1)
                                   .setProtocol("http")
                                   .setEndpoint("http://foourl"));
        snsClient.subscribe(new SubscribeRequest()
                                   .withTopicArn(topicArn1)
                                   .withProtocol("lambda")
                                   .withEndpoint("fooarn:function:lambda_name:v2"));

    }
}
```

![](../images/666370888.png)

### Linking

The com.castsoftware.wbslinker extension is responsible for
matching Java Call to AWS Lambda Function objects to Lambda Function
objects such as Java AWS Lambda Function during application-level
analysis.

### Data sensitivity

This extension is capable of setting a property on AWS S3 Bucket objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

See [Data Sensitivity](../../../../../multi/data-sensitivity/) for more information.

## Known limitations

-   No custom lambda function resolver is supported, i.e. that passed
    to lambdaFunctionNameResolver (connected to the building
    of LambdaInvokerFactory).
-   Use of Consumer Builder for SDK is not supported.
-   Monolithic pattern for lambda functions is not properly supported.