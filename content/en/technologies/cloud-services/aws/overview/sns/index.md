---
title: "AWS SNS"
linkTitle: "AWS SNS"
type: "docs"
---

>Support for AWS is distributed among several CAST extensions. This page
summarizes supported features for the latest version of the
corresponding CAST extension.

## List of supported languages deployment frameworks

AWS Lambda functions can be implemented and deployed with several tools.
The following table summarizes supported functionalities in deployment
frameworks and SDKs for different languages:

### SDK

|Language ↓  |  [AWS Software Development Kit (SDK)](https://aws.amazon.com/tools/?nc1=h_ls) |  [Cloud Development Kit (CDK)](https://docs.aws.amazon.com/cdk/latest/guide/home.html)  |
|------|------|-----|
| Node.js (JavaScript) | ![(tick)](/images/icons/emoticons/check.svg) (v2.x)        | ![(error)](/images/icons/emoticons/error.svg)         |
| Node.js (TypeScript) | ![(tick)](/images/icons/emoticons/check.svg) (v2.x, v3.x)  | ![(error)](/images/icons/emoticons/error.svg)         |
|         Java         | ![(tick)](/images/icons/emoticons/check.svg) (v1.x, v2.x)  | ![(error)](/images/icons/emoticons/error.svg)         |
|         .NET         | ![(tick)](/images/icons/emoticons/check.svg) (v3.x)        | ![(error)](/images/icons/emoticons/error.svg)         |
|        Python        | ![(tick)](/images/icons/emoticons/check.svg) (boto3)       | ![(error)](/images/icons/emoticons/error.svg)         |

### CLI tools

|  Language ↓  |  [AWS CLI](https://aws.amazon.com/) |  [AWS CDK Toolkit](https://docs.aws.amazon.com/cdk/v2/guide/cli.html) 
|--------------|------------------------------------|----------------------------------------------------------------------|
| Shell      | ![(tick)](/images/icons/emoticons/check.svg) (v1, v2) | ![(error)](/images/icons/emoticons/error.svg) |

## Details about support for each language

-   [Support of AWS SNS for Node.js (JavaScript)](../../../../web/nodejs/com.castsoftware.nodejs/)
-   [Support of AWS SNS for Node.js (TypeScript)](../../../../web/typescript/com.castsoftware.typescript/)
-   [Support of AWS SNS for Python](../../../../python/com.castsoftware.python/)
-   [Support of AWS SNS for Java](../../extensions/com.castsoftware.awsjava/)
-   [Support of AWS SNS for .NET](../../extensions/com.castsoftware.awsdotnet/)
-   [Support of AWS SNS for Shell](../../../../shell/com.castsoftware.shell/)