---
title: "Support of AWS SQS for Python"
linkTitle: "Support of AWS SQS for Python"
type: "docs"
---

See **Amazon Web Services support** in [com.castsoftware.python](../../../../../python/com.castsoftware.python/).