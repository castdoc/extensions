---
title: "AWS SQS"
linkTitle: "AWS SQS"
type: "docs"
no_list: true
---

>Support for AWS is distributed among several CAST extensions. This page
and its sub-pages summarize what is supported for AWS when using most
recent versions of CAST extensions. 

## List of supported languages/deployment frameworks

Amazon Web Services Lambda can be implemented and deployed with several
tools (i.e. deployment frameworks) using several languages. The
following table shows what language is supported with what framework:

### SDK

|  Language ↓ |  [AWS Software Development Kit (SDK)](https://aws.amazon.com/tools/?nc1=h_ls) | [Cloud Development Kit (CDK)](https://docs.aws.amazon.com/cdk/latest/guide/home.html) |
|--------|--------|--------------------|
| Node.js (JavaScript) | ![(tick)](/images/icons/emoticons/check.svg) (v2.x)        | ![(error)](/images/icons/emoticons/error.svg)         |
| Node.js (TypeScript) |  ![(tick)](/images/icons/emoticons/check.svg) (v2.x, v3.x) | ![(error)](/images/icons/emoticons/error.svg)         |
|         Java         | ![(tick)](/images/icons/emoticons/check.svg) (v1.x, v2.x)  | ![(error)](/images/icons/emoticons/error.svg)         |
|         .NET         | ![(tick)](/images/icons/emoticons/check.svg) (v3.x)        | ![(error)](/images/icons/emoticons/error.svg)         |
|        Python        | ![(tick)](/images/icons/emoticons/check.svg) (boto3)       | ![(error)](/images/icons/emoticons/error.svg)         |

### CLI tools

| Language ↓ | AWS CLI | AWS CDK Toolkit |
|------------|---------|-----------------|
| Shell          | ![(tick)](/images/icons/emoticons/check.svg) (v1, v2) | ![(error)](/images/icons/emoticons/error.svg) |

## Details about support for each language

-   [Support of AWS SQS for Node.js (JavaScript)](../../../../web/nodejs/com.castsoftware.nodejs/)
-   [Support of AWS SQS for Node.js (TypeScript)](../../../../web/typescript/com.castsoftware.typescript/)
-   [Support of AWS SQS for Python](../../../../python/com.castsoftware.python/)
-   [Support of AWS SQS for Java](../../extensions/com.castsoftware.awsjava/)
-   [Support of AWS SQS for .NET](../../extensions/com.castsoftware.awsdotnet/)
-   [Support of AWS SQS for Shell](../../../../shell/com.castsoftware.shell/)