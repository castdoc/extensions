---
title: "Support of AWS SQS for Node.js - TypeScript"
linkTitle: "Support of AWS SQS for Node.js - TypeScript"
type: "docs"
---

>Support for AWS SQS is available from version 1.7.0-beta3 of the
[com.castsoftware.typescript](../../../../../web/typescript/com.castsoftware.typescript/)
extension. SQS is currently supported only for SQS queues created using
SDK.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="460095656.png" draggable="false"
data-image-src="460095656.png"
data-unresolved-comment-count="0" data-linked-resource-id="460095656"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Publisher.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="460095655"
data-linked-resource-container-version="9" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Publisher</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="460095657.png" draggable="false"
data-image-src="460095657.png"
data-unresolved-comment-count="0" data-linked-resource-id="460095657"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Receiver.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="460095655"
data-linked-resource-container-version="9" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Receiver</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="460095658.png" draggable="false"
data-image-src="460095658.png"
data-unresolved-comment-count="0" data-linked-resource-id="460095658"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Unknown_Publisher.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="460095655"
data-linked-resource-container-version="9" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Unknown Publisher</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="460095659.png" draggable="false"
data-image-src="460095659.png"
data-unresolved-comment-count="0" data-linked-resource-id="460095659"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Unknown_Receiver.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="460095655"
data-linked-resource-container-version="9" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Unknown Receiver</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>sendMessage</p>
<p>sendMessageBatch</p>
<p>receiveMessage</p></td>
</tr>
</tbody>
</table>

## Support for SDK

This code will publish a message into the "SQS_QUEUE_URL" queue:

``` js
import * as AWS from "aws-sdk";
AWS.config.update({ region: 'REGION' });

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const queueUrl = "SQS_QUEUE_URL"

const params = {
    MessageBody: "This is a message",
    QueueUrl: queueUrl,
    MaxNumberOfMessages: 1,
    VisibilityTimeout: 0,
};


sqs.sendMessage(params, function (err, data) {
    if (err) {
        console.log("Error", err);
    } else {
        console.log("Success", data.MessageId);
    }
});
}
```

This code will receive a message from the queue "SQS_QUEUE_URL"

``` js
import * as AWS from "aws-sdk";
AWS.config.update({ region: 'REGION' });

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const queueUrl = "SQS_QUEUE_URL"

const params = {
    QueueUrl: queueUrl,
    MaxNumberOfMessages: 1,
    VisibilityTimeout: 0,
};

export class SqsReciver {
    constructor() {
        this.reciver();
    }
    private reciver(): void {
        sqs.receiveMessage(params, (err, data) => {
    // do something
        });
    }
}
```

![](460095665.png)

## Known limitations

-   The use of AWS.SQS with promises is not supported. For instance no
    link would be created between the receiver and the handler function
    defined in .then() call in the following source code: 

``` xml
this.sqs.receiveMessage(params).promise().then( () => {})
```

-   If the queueName is set using the createQueue api, the evaluation of
    the queue name will fail.
