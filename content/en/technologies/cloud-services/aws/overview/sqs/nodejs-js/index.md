---
title: "Support of AWS SQS for Node.js - JavaScript"
linkTitle: "Support of AWS SQS for Node.js - JavaScript"
type: "docs"
---

>Support for AWS SQS is available from version 2.5.x of the
[com.castsoftware.nodejs](../../../../../web/nodejs/com.castsoftware.nodejs/) extension.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="457506891.png" draggable="false"
data-image-src="457506891.png"
data-unresolved-comment-count="0" data-linked-resource-id="457506891"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Publisher.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="457506879"
data-linked-resource-container-version="11" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Publisher</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="457506892.png" draggable="false"
data-image-src="457506892.png"
data-unresolved-comment-count="0" data-linked-resource-id="457506892"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Receiver.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="457506879"
data-linked-resource-container-version="11" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Receiver</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="457506893.png" draggable="false"
data-image-src="457506893.png"
data-unresolved-comment-count="0" data-linked-resource-id="457506893"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Unknown_Publisher.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="457506879"
data-linked-resource-container-version="11" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Unknown Publisher</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="457506894.png" draggable="false"
data-image-src="457506894.png"
data-unresolved-comment-count="0" data-linked-resource-id="457506894"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_AWS_SQS_Unknown_Receiver.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="457506879"
data-linked-resource-container-version="11" /></p>
</div></td>
<td class="confluenceTd">Node.js AWS SQS Unknown Receiver</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>sendMessage</p>
<p>sendMessageBatch</p>
<p>receiveMessage</p></td>
</tr>
</tbody>
</table>

## Code samples

This code will publish a message into the "SQS_QUEUE_URL" queue:

``` js
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'REGION'});

// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var params = {
   // Remove DelaySeconds parameter and value for FIFO queues
  DelaySeconds: 10,
  MessageAttributes: {
    "Title": {
      DataType: "String",
      StringValue: "The Whistler"
    },
    "Author": {
      DataType: "String",
      StringValue: "John Grisham"
    },
    "WeeksOn": {
      DataType: "Number",
      StringValue: "6"
    }
  },
  MessageBody: "Information about current NY Times fiction bestseller for week of 12/11/2016.",
  // MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
  // MessageGroupId: "Group1",  // Required for FIFO queues
  QueueUrl: "SQS_QUEUE_URL"
};

sqs.sendMessage(params, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data.MessageId);
  }
});
```

This code will receive a message from the queue "SQS_QUEUE_URL":

``` js
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'REGION'});

// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var params = {
   // Remove DelaySeconds parameter and value for FIFO queues
  DelaySeconds: 10,
  MessageAttributes: {
    "Title": {
      DataType: "String",
      StringValue: "The Whistler"
    },
    "Author": {
      DataType: "String",
      StringValue: "John Grisham"
    },
    "WeeksOn": {
      DataType: "Number",
      StringValue: "6"
    }
  },
  MessageBody: "Information about current NY Times fiction bestseller for week of 12/11/2016.",
  // MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
  // MessageGroupId: "Group1",  // Required for FIFO queues
  QueueUrl: "SQS_QUEUE_URL"
};

sqs.sendMessage(params, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data.MessageId);
  }
});
```

![](457506896.png)

## Known limitations

-   The use of AWS.SQS with promises is not supported. For instance no
    link would be created between the receiver and the handler function
    defined in .then() call in the following source code: 

``` xml
sqs.receiveMessage(params).promise().then( () => {});
```

-   The use of AWS.SQS with send() is not supported. For instance no
    link would be created between the receiver and the handler function
    defined in .send() call in the following source code: 

``` xml
var request = sqs.receiveMessage(params);
request.send(() => {});
```
