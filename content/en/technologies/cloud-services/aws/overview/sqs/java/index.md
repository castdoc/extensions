---
title: "Support of AWS SQS for Java"
linkTitle: "Support of AWS SQS for Java"
type: "docs"
---

## Objects

| Icon | Description |
|:----:|:-----------:|
| ![Alt text](467075112.png)     | Java AWS SQS Publisher |
| ![Alt text](467075113.png)     | Java AWS SQS Receiver  |
| ![Alt text](467075114.png)     | Java AWS SQS Unknown Publisher |
| ![Alt text](467075115.png)     | Java AWS SQS Unknown Receiver |

## Links

| Link Type | Function |
|:---------:|:--------:|
| callLink  | sendMessage, sendMessageBatch, receiveMessage  |

## Code samples

In this code, the sendMessage publishes a into the "testQueue" queue and
the receiveMessage receive a message from the same SQS queue:

``` java
package aws.example.sqs;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.AmazonSQSException;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageRequest;


public class SendReceiveMessages
{

    public static void sendMessage(String[] args)
    {
        final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

        String queueUrl = sqs.getQueueUrl("testQueue").getQueueUrl();

        SendMessageRequest send_msg_request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody("hello world")
                .withDelaySeconds(5);
        sqs.sendMessage(send_msg_request);
    }

    public static void receiveMessage()
    {

        final AmazonSQS sqs = AmazonSQSClientBuilder.defaultClient();

        String queueUrl = sqs.getQueueUrl("testQueue").getQueueUrl(); 

        // receive messages from the queue
        List<Message> messages = sqs.receiveMessage(queueUrl).getMessages();


    }
}
```

![Alt text](467468297.png)

## Known limitations

-   Use of AmazonSQSRequester and AmazonSQSResponder and
    AbstractAmazonSQSClientWrapper is not supported 
