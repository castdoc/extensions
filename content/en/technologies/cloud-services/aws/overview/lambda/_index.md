---
title: "AWS Lambda"
linkTitle: "AWS Lambda"
type: "docs"
no_list: true
---

>The support for AWS is distributed among several CAST extensions. This
page summarizes supported features for the latest version of the
corresponding CAST extensions. 

## List of supported languages and deployment frameworks

AWS Lambda functions can be implemented and deployed with several tools.
The following table summarizes supported functionalities in deployment
frameworks and SDKs for different languages:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Framework/API →
Services and services interactions ↓</th>
<th class="confluenceTh">Cloudformation</th>
<th class="confluenceTh">SAM
<p><br />
</p></th>
<th class="confluenceTh">Serverless v1.x v2.x</th>
<th class="confluenceTh">Shell (AWS Cli)</th>
<th class="confluenceTh" style="text-align: left;">SDK JavaScript
v2.x</th>
<th class="confluenceTh" style="text-align: left;">SDK TypeScript v2.x /
v3.x</th>
<th class="confluenceTh" style="text-align: left;">SDK Java v1.x /
v2.x</th>
<th class="confluenceTh" style="text-align: left;">SDK Python
boto3</th>
<th class="confluenceTh" style="text-align: left;">SDK dotnet</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Lambda invocation</td>
<td class="confluenceTd" style="text-align: center;">-</td>
<td class="confluenceTd" style="text-align: center;">-</td>
<td class="confluenceTd" style="text-align: center;">-</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><p><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></p></td>
<td class="confluenceTd" style="text-align: center;"><p><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></p></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><p><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Lambda creation and link to the
handler</p></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Lambda trigger: REST API gateway</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
</tr>
<tr class="even">
<td class="confluenceTd">Lambda trigger: SQS event</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Lambda trigger: S3 event</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
</tr>
<tr class="even">
<td class="confluenceTd">Lambda trigger: DynamoDB event</td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Lambda trigger: SNS event</td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
<td class="confluenceTd" style="text-align: center;"><a href="index"
rel="nofollow"><img src="/images/icons/emoticons/error.svg"
class="emoticon emoticon-cross" data-emoticon-name="cross"
alt="(error)" /></a></td>
</tr>
</tbody>
</table>

## Details about support for each language

-   [Support of CloudFormation, SAM, and Serverless
    framework](../../../cloud-configuration) (note that for the lambda
    functions objects created by this extension, the link to the handler
    function is created by one of the following extensions)
-   [Support of AWS Lambda for Java](../../extensions/com.castsoftware.awsjava)
-   [Support of AWS Lambda for .NET](../../extensions/com.castsoftware.awsdotnet)
-   [Support of AWS Lambda for Node.js (JavaScript)](nodejs-js)
-   [Support of AWS Lambda for Node.js (TypeScript)](nodejs-ts)
-   [Support of AWS Lambda for Python](../../../../python/com.castsoftware.python)
-   [Support of AWS Lambda for Shell](../../../../shell/com.castsoftware.shell)