---
title: "Support of AWS Lambda for Node.js - JavaScript"
linkTitle: "Support of AWS Lambda for Node.js - JavaScript"
type: "docs"
---

See **Amazon Web Services Lambda support** in [com.castsoftware.nodejs](../../../../../web/nodejs/com.castsoftware.nodejs/).