---
title: "Support of AWS Lambda for Node.js - TypeScript"
linkTitle: "Support of AWS Lambda for Node.js - TypeScript"
type: "docs"
---

See **Node.js - Amazon Web Services support** in [com.castsoftware.typescript](../../../../../web/typescript/com.castsoftware.typescript/).