---
title: "Support of AWS Lambda for Python"
linkTitle: "Support of AWS Lambda for Python"
type: "docs"
---

See **Amazon Web Services support** in [com.castsoftware.python](../../../../../python/com.castsoftware.python/).