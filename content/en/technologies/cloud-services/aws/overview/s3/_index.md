---
title: "AWS S3"
linkTitle: "AWS S3"
type: "docs"
no_list: true
---

## Details about support for each language

-   [Support of AWS S3 for Node.js (JavaScript)](../../../../web/nodejs/com.castsoftware.nodejs/)
-   [Support of AWS S3 for Node.js (TypeScript)](../../../../web/typescript/com.castsoftware.typescript/)
-   [Support of AWS S3 for Python](../../../../python/com.castsoftware.python/)
-   [Support of AWS S3 for Java](../../extensions/com.castsoftware.awsjava/)
-   [Support of AWS S3 for .NET](../../extensions/com.castsoftware.awsdotnet/)
-   [Support of AWS S3 for Shell](../../../../shell/com.castsoftware.shell/)