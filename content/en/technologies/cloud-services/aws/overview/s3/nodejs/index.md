---
title: "Support of AWS S3 for Node.js"
linkTitle: "Support of AWS S3 for Node.js"
type: "docs"
---

>Support for AWS S3 is available:
>- from version 2.1.x of the [com.castsoftware.nodejs](../../../../../web/nodejs/com.castsoftware.nodejs/) extension
>- from version 1.3.x of the [com.castsoftware.typescript](../../../../../web/typescript/com.castsoftware.typescript/) extension.

## Objects

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="382625956.png" /></p>
</div></td>
<td class="confluenceTd">Node.js S3 Region</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="382625955.png" /></p>
</div></td>
<td class="confluenceTd">Node.js S3 Bucket</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">No Link</td>
<td class="confluenceTd"><p>createBucket</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>createMultipartUpload</p>
<p>createPresignedPost</p>
<p>abortMultipartUpload</p>
<p>completeMultipartUpload</p>
<p>deleteBucketAnalyticsConfiguration</p>
<p>deleteBucketCors</p>
<p>deleteBucketEncryption</p>
<p>deleteBucketInventoryConfiguration</p>
<p>deleteBucketLifecycle</p>
<p>deleteBucketMetricsConfiguration</p>
<p>deleteBucketPolicy</p>
<p>deleteBucketReplication</p>
<p>deleteBucketTagging</p>
<p>deleteBucketWebsite</p>
<p>deleteObjectTagging</p>
<p>deletePublicAccessBlock</p>
<p>getBucketAccelerateConfiguration</p>
<p>getBucketAcl</p>
<p>getBucketAnalyticsConfiguration</p>
<p>getBucketCors</p>
<p>getBucketEncryption</p>
<p>getBucketInventoryConfiguration</p>
<p>getBucketLifecycle</p>
<p>getBucketLifecycleConfiguration</p>
<p>getBucketLocation</p>
<p>getBucketLogging</p>
<p>getBucketMetricsConfiguration</p>
<p>getBucketNotification</p>
<p>getBucketNotificationConfiguration</p>
<p>getBucketPolicy</p>
<p>getBucketPolicyStatus</p>
<p>getBucketReplication</p>
<p>getBucketTagging</p>
<p>getBucketVersioning</p>
<p>getBucketWebsite</p>
<p>getObjectAcl</p>
<p>getObjectLegalHold</p>
<p>getObjectLockConfiguration</p>
<p>getObjectRetention</p>
<p>getObjectTagging</p>
<p>getPublicAccessBlock</p>
<p>getSignedUrl</p>
<p>listBuckets</p>
<p>listBucketAnalyticsConfigurations</p>
<p>listBucketInventoryConfigurations</p>
<p>listBucketMetricsConfigurations</p>
<p>listMultipartUploads</p>
<p>listObjectVersions</p>
<p>listParts</p>
<p>putBucketLogging</p>
<p>putBucketAnalyticsConfiguration</p>
<p>putBucketLifecycleConfiguration</p>
<p>putBucketMetricsConfiguration</p>
<p>putBucketNotification</p>
<p>putBucketNotificationConfiguration</p>
<p>putBucketPolicy</p>
<p>putBucketReplication</p>
<p>putBucketRequestPayment</p>
<p>putBucketTagging</p>
<p>putBucketVersioning</p>
<p>putObjectAcl</p>
<p>putObjectLegalHold</p>
<p>putObjectLockConfiguration</p>
<p>putObjectRetention</p>
<p>putObjectTagging</p>
<p>putPublicAccessBlock</p>
<p>putBucketAccelerateConfiguration</p>
<p>putBucketAcl</p>
<p>putBucketCors</p>
<p>putBucketEncryption</p>
<p>putBucketInventoryConfiguration</p>
<p>putBucketLifecycle</p>
<p>upload</p>
<p>uploadPart</p>
<p>uploadPartCopy</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><p>putObject</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>deleteBucket</p>
<p>deleteObject</p>
<p>deleteObjects</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>getObject</p>
<p>getObjectTorrent</p>
<p>listObjects</p>
<p>listObjectsV2</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>putBucketLogging</p>
<p>putBucketAnalyticsConfiguration</p></td>
</tr>
</tbody>
</table>

## Code samples

This code will create a S3 Bucket named "MyBucket" on an AWS server in
region "REGION" and puts an object in it:

``` js
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'REGION'});

// Create S3 service object
s3 = new AWS.S3({apiVersion: '2006-03-01'});

// Create the parameters for calling createBucket
var bucketParams = {
  Bucket : "MyBucket",
  ACL : 'public-read'
};

// call S3 to create the bucket
s3.createBucket(bucketParams, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data.Location);
  }
});

params = {
    // ...
    Bucket: "MyBucket"
};
s3.putObject(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
});
```
