---
title: "Amazon Web Services"
linkTitle: "Amazon Web Services"
type: "docs"
no_list: false
---

**Amazon Web Services (AWS)** is a comprehensive cloud computing platform provided by Amazon. It offers a wide range of services for various computing needs


| **Service**    | **Key Features**                                 | **Category**                 | **Description**                                                                            | **Support** |
|----------------|--------------------------------------------------|------------------------------|--------------------------------------------------------------------------------------------|:---:|
| **EC2**        | Resizable virtual servers                        | Compute                      | Provides scalable computing capacity with various instance types.                          |
| **Lambda**     | Serverless computing                             | Compute                      | Allows running code without provisioning servers, with automatic scaling.                  | :heavy_check_mark: |
| **ECS**        | Docker container management                      | Compute                      | Managed service for running and scaling Docker containers.                                 |
| **EKS**        | Kubernetes management                            | Compute                      | Managed service for running Kubernetes clusters.                                           |
| **S3**         | Scalable object storage                          | Storage                      | Durable storage for data backup, archiving, and analytics.                                 | :heavy_check_mark: |
| **EBS**        | Block storage for EC2                            | Storage                      | Persistent block storage for use with EC2 instances.                                       |
| **Glacier**    | Low-cost archival storage                        | Storage                      | Designed for data archiving with long retrieval times.                                     |
| **EFS**        | Managed file storage                             | Storage                      | Provides scalable, elastic file storage for use with AWS services and on-premises resources.|
| **RDS**        | Managed relational databases                     | Database                     | Supports multiple database engines like MySQL, PostgreSQL, and Oracle.                     | :heavy_check_mark: |
| **DynamoDB**   | NoSQL database service                           | Database                     | High-performance, scalable NoSQL database for web-scale applications.                      | :heavy_check_mark: |
| **Redshift**   | Data warehousing                                 | Database                     | Designed for large-scale data analytics and business intelligence.                         |
| **Aurora**     | High-performance relational database             | Database                     | Compatible with MySQL and PostgreSQL, providing increased performance and reliability.     | :heavy_check_mark: |
| **VPC**        | Virtual network management                       | Networking & Content Delivery| Provides isolated networks within the AWS cloud.                                           |
| **Route 53**   | Scalable DNS service                             | Networking & Content Delivery| Domain Name System service for routing end-user requests to Internet applications.         |
| **CloudFront** | Content delivery network                         | Networking & Content Delivery| Distributes content globally with low latency and high transfer speeds.                    |
| **Direct Connect** | Dedicated network connection               | Networking & Content Delivery| Establishes a private network connection from your premises to AWS.                        |
| **IAM**        | Access management                                | Security, Identity & Compliance | Manages user access and encryption keys for AWS services and resources.                   |
| **KMS**        | Encryption key management                        | Security, Identity & Compliance | Provides control over encryption keys used to secure data.                                |
| **Shield**     | DDoS protection                                  | Security, Identity & Compliance | Protects applications from Distributed Denial of Service attacks.                         |
| **WAF**        | Web application firewall                         | Security, Identity & Compliance | Protects web applications from common web exploits.                                       |
| **CloudWatch** | Monitoring and logging                           | Management & Governance      | Provides data and actionable insights for monitoring applications and resources.           |
| **CloudTrail** | API activity logging                             | Management & Governance      | Logs and monitors API calls for compliance and governance.                                 |
| **Config**     | Resource configuration history                   | Management & Governance      | Tracks AWS resource configurations and changes over time.                                  |
| **CloudFormation** | Infrastructure as code                      | Management & Governance      | Allows provisioning of AWS resources using templates.                                      |
| **EMR**        | Big data processing                              | Analytics                    | Managed Hadoop service for large-scale data processing.                                    |
| **Kinesis**    | Real-time data streaming                         | Analytics                    | Enables real-time data ingestion and processing.                                           |
| **QuickSight** | Business intelligence                            | Analytics                    | Provides data visualization and business analytics tools.                                  |
| **Athena**     | SQL query for S3 data                            | Analytics                    | Allows querying data directly in S3 using SQL.                                             |
| **Glue**           | Data integration                                 | Analytics                    | Fully managed ETL (extract, transform, load) service for preparing and loading data.       |
| **SageMaker**  | Machine learning model management                | Machine Learning             | End-to-end machine learning service for building, training, and deploying models.          |
| **Rekognition** | Image and video analysis                       | Machine Learning             | Detects objects, scenes, and activities in images and videos.                              |
| **Comprehend** | Natural language processing                      | Machine Learning             | Provides text analysis capabilities like sentiment analysis and entity recognition.        |
| **Lex**        | Conversational interfaces                        | Machine Learning             | Builds chatbots and voice assistants using machine learning.                               |
| **SQS**        | Message queuing                                  | Application Integration      | Enables decoupling of application components via message queues.                           | :heavy_check_mark: |
| **SNS**        | Pub/sub messaging                                | Application Integration      | Provides notifications and messaging to various endpoints.                                 | :heavy_check_mark: |
| **Step Functions** | Workflow automation                          | Application Integration      | Coordinates microservices and serverless applications.                                     |
| **EventBridge**    | Event bus service                                | Application Integration      | Serverless event bus for integrating AWS services and SaaS applications.                   |
| **CodeCommit** | Source control                                   | Developer Tools              | Managed Git repositories for source code management.                                       |
| **CodeBuild**  | Continuous integration build service             | Developer Tools              | Compiles code, runs tests, and produces software artifacts.                                |
| **CodeDeploy** | Automated deployment                             | Developer Tools              | Automates the deployment of applications to various services.                              |
| **CodePipeline** | Continuous delivery                            | Developer Tools              | Automates software release processes.                                                      |
| **DMS**        | Database migration                               | Migration & Transfer         | Helps migrate databases to AWS with minimal downtime.                                      |
| **Snowball**   | Data transfer devices                            | Migration & Transfer         | Transfers large amounts of data into and out of AWS using physical devices.                |
| **Transfer Family** | Managed file transfer                        | Migration & Transfer         | Supports secure transfer protocols like SFTP, FTPS, and FTP.                               |
| **GameLift**   | Game server management                           | Game Development             | Deploys and scales game servers for multiplayer games.                                     |
| **Lumberyard** | 3D game engine                                   | Game Development             | Integrated with AWS services and Twitch for game development.                              |
| **IoT Core**   | Connects IoT devices                             | IoT                          | Connects and manages IoT devices securely at scale.                                        |
| **IoT Greengrass** | Edge computing                               | IoT                          | Extends AWS to edge devices for local data processing.                                     |
| **IoT Analytics** | IoT data processing and analytics             | IoT                          | Collects, processes, and analyzes IoT data.                                                |
| **Sumerian**   | AR and VR development tools                      | AR & VR                      | Creates and runs 3D, AR, and VR applications.                                              |
| **Managed Blockchain** | Blockchain network management           | Blockchain                   | Creates and manages scalable blockchain networks.                                          |
| **QLDB**       | Ledger database                                  | Blockchain                   | Provides a transparent, immutable, and cryptographically verifiable transaction log.       |
