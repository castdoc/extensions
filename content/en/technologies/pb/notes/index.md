---
title: "Technical notes"
linkTitle: "Technical notes"
type: "docs"
weight: 2
---

## Inference Engine

### Dynamic SQL links found by the inference engine

-   When the inference engine perceives that a string is an SQL string,
    then this string will not be passed through the Grep search.

### C/S links found by the inference engine

-   When the inference engine finds a C/S link, the analyzer will place
    the link at the level of the object that contains the string and not
    at the level of the exec. There is various information about these
    links:

    STRING: Concatenated SQL string

    CONCATENATION: Compete, partial or not at all. This will enable you
    to find out whether the inference engine has managed to  concatenate
    the SQL string.

    EXECUTED IN: This is the qualified name of the object that contains
    the SQL exe.

    STRING BUILD IN OBJECTS: a list of qualified names of objects that
    have participated in the construction of the SQL string. The number
    of objects is limited to 10. For example:

    -   STRING = DELETE FROM authors WHERE authors.au_lname = (SELECT
        discounts.stor_id  FROM discounts  WHERE discounts.stor_id =
        '2')
    -   CONCATENATION = COMPLETE
    -   EXECUTED IN = w1.fct3(Line1,Col51)
    -   STRING BUILT IN OBJECTS = typelnk.open ; w1.fct1

### Links found by the inference engine for virtual functions

-   For virtual functions, the link is found on the function call. For
    the link, information will be added about the location of the
    "create". E.g.:
    -   STATEMENT "CREATE" IN OBJECT(S) :
        w_dessin.wof_aire(Line=4,Col=2)

### Information about other C/C links found by the inference engine (TiggerEVent, Open etc...)

-   There is one link at the string level. The inference engine will
    gather and store the following information:
    -   EXECUTED IN OBJECT - the location of the TriggerEVent  
          
        E.g.: EXECUTED IN OBJECT : w_macro.of_f

## Links in general

-   Client/Server links to quoted server objects (e.g.: "Product Sales")
    are not found during a scan for links in embedded SQL, datawindows,
    RPC Func, and declare cursors or procedures.
-   C/S Use() type links in dynamic declare procedures and dynamic
    declare cursors are identified by the PB analyzer. However, the PB
    analyzer will identify the object caller as the object which
    contains the dynamic string identifying the SQL statement executed
    by the dynamic procedure or cursor and not the declare procedure or
    declare cursor (which is the correct object caller).

## Other miscellaneous

-   When analyzing an entire workspace in one analysis, and if the
    workspace contains two targets using the same application, the PB
    analyzer always creates two different application objects in the
    Analysis Service.
-   The PB analyzer cannot handle the pronoun ParentWindow. If it is
    used in a menu script, no links will be created to windows that use
    this menu.An extract from the PowerBuilder documentation
    follows: *ParentWindow property: You can use the ParentWindow
    property of the Menu object like a pronoun in Menu scripts. It
    identifies the window that the menu is associated with when your
    program is running.Using ParentWindow: You can use the
    reserved word ParentWindow to refer to the window that the menu is
    associated with during execution. For example, the following
    statement closes the window the menu is associated
    with:Close(ParentWindow)You can also use ParentWindow to refer to
    properties of the window a menu is associated with, but not to refer
    to properties of controls or user objects in the window.For example,
    the following statement is valid, because it refers to properties of
    the window itself:ParentWindow.Height = ParentWindow.Height/2*
-   Virtual functions are handled by the Inference Engine
-   There are some limitations regarding the counting and inclusion of
    comments during a PB analysis:
    -   Comments between a "select" and a "from" in a cursor declaration
        are not counted.
    -   Comments in embedded SQL are not counted.
    -   Comments that follow a comment in a declaration with nothing
        behind it (and that has been included in the analysis) are
        ignored.For example, here is variable declaration in PB (full
        declaration block):integer gi_comments // comment 1// comment
        2/\* comment 3\*/Comment 1 is associated to the global
        variableCommens 2 and 3 are ignored and not counted.
    -   Number of lines of code and comments are counted in x examples
        when x variables are declared in the same line.
-   In some circumstances certain PB menu items may not be imported into
    the Analysis Service during an analysis. This occurs when the PB
    menu containing the PB menu items (non-modified) in question is
    inherited from another PB menu.
-   Some PB reserved words might appear in the middle of a qualified
    identifier, for example, in the following piece of code: OLEObject
    ol_app, ol_varol_var = ol_app.Variables.Add("app_id") the keyword
    'Variables' appears in the middle of a qualified identifier where it
    is not recognized as a keyword by the PB compiler but the CAST PB
    Analyzer fails to do so, leading to an incorrect syntax error. The
    problem particularly occurs when using OLE objects and the keywords
    that can cause this incorrect syntax error are all the PB reserved
    words except the following:close, delete, insert, ref, select,
    system, type, update.
-   Metrics are not calculated on PowerBuilder Cursors that belong to
    Methods. Instead, metrics are calculated on the Method that contains
    the Cursor