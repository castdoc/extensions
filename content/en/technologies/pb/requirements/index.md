---
title: "Requirements"
linkTitle: "Requirements"
type: "docs"
weight: 3
---

## Install on workstation running Console
Nothing required.
> Note that CAST Imaging Console only requires the location of the .PBT files for source code delivery.

## Install on node

CAST uses the SAP PowerBuilder API during an analysis. As such, the same version of the SAP PowerBuilder IDE used to compile the target application source code must be installed on the node.

In addition, when analyzing SAP PowerBuilder 2021, 2022 or 2023, the path to the SAP PowerBuilder runtime (provided by the IDE) must be manually added to the "PATH" system environment variable.