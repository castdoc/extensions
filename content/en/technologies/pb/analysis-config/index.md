---
title: "Analysis configuration"
linkTitle: "Analysis configuration"
type: "docs"
weight: 5
---

CAST Imaging Console exposes the Technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
PowerBuilder to display the available options:

![](416940124.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same as at Application level) that allow you to
        make fine-grained configuration changes.

You should check that settings are as required and that at least one
Analysis Unit exists for the specific technology.

*Click to enlarge*

![](416940125.jpg)

<table>
<tbody>
<tr>
<th><strong>PowerBuilder version</strong></th>
<td>Select the <strong>PowerBuilder
version</strong> that corresponds to the version of the application you
want to analyze.</td>
</tr>
<tr>
<th><strong>Dashes in identifiers
allowed</strong></th>
<td><div>
<p>This option is identical to the PowerBuilder option of the same name.
Activating this option (the option is activated by default) will force
the analyzer to take into account code that includes dashes in
identifiers. For example:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="dt">int</span> <span class="op">-</span>Myvar</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="dt">int</span> a</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>a <span class="op">=</span> a <span class="op">-</span> <span class="op">-</span>Myvar</span></code></pre></div>
</div>
</div>
<p>If the option is deactivated, the above code would produce syntax
errors and will not be correctly interpreted by the analyzer.</p>
</div></td>
</tr>
<tr>
<th>Dependencies</th>
<td><p>Dependencies are configured
at <strong>Application level</strong> for<strong> each
technology</strong> and are configured between <strong>individual
Analysis Units/groups of Analysis Units </strong>(dependencies between
technologies are not available as is the case in the CAST Management
Studio). You can find out more detailed information about how to ensure
dependencies are set up correctly, in Validate dependency
configuration.</p></td>
</tr>
<tr>
<th>Analysis Units</th>
<td><div>
<p>For PowerBuilder, there are <strong>no Analysis Unit level settings
available</strong>. You can however:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr>
<th><div>
<img src="416940106.jpg" />
</div></th>
<td><p>Analyze a specific Analysis Unit (and all
Analysis Units within the Execution Unit), specifically for testing
purposes.</p></td>
</tr>
<tr>
<th><div>
<img src="416940107.jpg" />
</div></th>
<td>Disable or enable the analysis of a specific
Analysis Unit, when the next Application level analysis/snapshot is run.
By default all Analysis Units are always set to
<strong>enable</strong>. When an Analysis Unit is set to disable, this
is usually a temporary action to prevent an Analysis Unit from being
analyzed.</td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
</tbody>
</table>
