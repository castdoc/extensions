---
title: "Analysis results"
linkTitle: "Analysis results"
type: "docs"
weight: 6
---

## What results can you expect?

### Objects

| Icon |                         Name                          |
|------|-------------------------------------------------------|
| ![Alt text](383886020.png)     |                  PB Program/Analysis                  |
| ![Alt text](383886018.png)     |                      PB Library                       |
| ![Alt text](383886019.png)     |                       PB Window                       |
| ![Alt text](383886017.png)     |                    PB User Object                     |
| ![Alt text](383886016.png)     |                     PB DataStore                      |
| ![Alt text](383886015.png)     |                     PB DataWindow                     |
| ![Alt text](383886013.png)     |                 PB Structure (Global)                 |
| ![Alt text](383886014.png)     |                        PB Menu                        |
| ![Alt text](383886012.png)     |                    PB Application                     |
| ![Alt text](383886008.png)     |                  PB Function Global                   |
| ![Alt text](383886010.png)     |                      PB Control                       |
| ![Alt text](383886008.png)     |                      PB Function                      |
| ![Alt text](383886009.png)     |                       PB Event                        |
| ![Alt text](383886007.png)     |                    PB RPC Function                    |
| ![Alt text](383886006.png)     |                 PB Declare Procedure                  |
| ![Alt text](383886006.png)      |                   PB Declare Cursor                   |
| ![Alt text](383886005.png)     |                 PB External Function                  |
| ![Alt text](383886003.png)     | Variable (Member, Instance, Shared, Global, Variable) |
| ![Alt text](383886004.png)     |                     PB8 Workspace                     |
| ![Alt text](383886002.png)     |                    PB Proxy Object                    |
| ![Alt text](383886001.png)     |                       PB Target                       |
| ![Alt text](383886000.png)     |                       PB Subset                       |

## Links

### Client/Client

<table>
<tbody>
<tr>
<td colspan="2"><strong>Link Type </strong></td>
<td><strong>When is this type of link
created?</strong></td>
</tr>
<tr>
<td rowspan="5"><strong>Access</strong></td>
<td>- Read</td>
<td><div class="content-wrapper">
Caller: Code object - fct, Event...
<p>Callee: Variable (instance, shared or global)</p>
<p>Eg.:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>if a = 2 then
...
endif</code></pre>
</div>
</div>
</div></td>
</tr>
<tr>
<td>- Write</td>
<td><div class="content-wrapper">
Caller: Code object - fct, Event...
<p>Callee: Variable (instance, shared or global)</p>
<p>Eg.:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>a = 2</code></pre>
</div>
</div>
</div></td>
</tr>
<tr>
<td>- Exec</td>
<td><div class="content-wrapper">
Caller: Code objetc - fct, Event...
<p>Callee: Fct, Evt, Global Fct</p>
<p>Eg.:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>a.f()</code></pre>
</div>
</div>
<p>Please note that when a window is opened (open(w1) for example),
there will be an Access Read on w1</p>
</div></td>
</tr>
<tr>
<td>- Member</td>
<td><div class="content-wrapper">
Caller: Code object - fct, Event...
<p>Callee: Variable (instance, shared or global)</p>
<p>Eg.:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>a.b = 2</code></pre>
</div>
</div>
<p>(member on a and write on b)</p>
</div></td>
</tr>
<tr>
<td>- Array</td>
<td><div class="content-wrapper">
Caller: Code object - fct, Event...
<p>Callee: Variable (instance, shared or global)</p>
<p>Eg.:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>A[5].b = 2 </code></pre>
</div>
</div>
<p>(array on a and write on b)</p>
</div></td>
</tr>
<tr>
<td colspan="2"><strong>Prototype</strong></td>
<td>When declaring an external function in an
object and the function in a DLL that implements it.</td>
</tr>
<tr>
<td colspan="2"><strong>Use</strong>
<p>- Select</p>
<p>- Insert</p>
<p>- Delete</p>
<p>- Update</p></td>
<td>When the property of an object references a
datawindow.</td>
</tr>
<tr>
<td colspan="2"><strong>Inherit</strong></td>
<td><div class="content-wrapper">
<p>When there is inheritance between controls, user objects, menus, and
windows. In the following example, the window <em>“w_gui_authors2”</em>
inherits from <em>“w_gui_authors”:</em></p>
<p><img src="383885999.jpg" /></p>
</div></td>
</tr>
<tr>
<td colspan="2"><strong>Mention</strong></td>
<td><div class="content-wrapper">
<p>During the creation of a class. The callee is always a class:
(W,App,UO,M,Struct). E.g.:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>a = Create UO</code></pre>
</div>
</div>
</div></td>
</tr>
<tr>
<td colspan="2"><strong>Rely On</strong></td>
<td><div class="content-wrapper">
<p>This link is displayed when there is a link between a variable and a
type. When the link is on a local type variable, the link will then be
escalated internally. E.g.:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>w1 var</code></pre>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

### Client/Server

<table>
<tbody>
<tr>
<td><strong>Type of Link</strong></td>
<td><strong>Where is this type of link
created?</strong></td>
<td><strong>When is this type of link
created?</strong></td>
</tr>
<tr>
<td rowspan="4"><strong>Use()</strong>:
<p>- Select</p>
<p>- Insert</p>
<p>- Delete</p>
<p>- Update</p></td>
<td>Embedded SQL</td>
<td>-</td>
</tr>
<tr>
<td rowspan="2">Datawindow</td>
<td>- Link between Datawindow and table (or view)
with PB SELECT</td>
</tr>
<tr>
<td>- Link between Datawindow and table (or view)
with SQL Select</td>
</tr>
<tr>
<td>Dynamic SQL</td>
<td>- Link found by the inference engine</td>
</tr>
<tr>
<td><strong>Prototype</strong></td>
<td>RPC
<p>Declare Procedure</p>
<p>Declare Cursor</p></td>
<td>- Link between the logical object corresponding
to the logical name in PB and the server object.</td>
</tr>
<tr>
<td rowspan="2"><strong>Call</strong></td>
<td>Datawindow</td>
<td>- Link between Datawindow and Procedure (or sub
object in Oracle)</td>
</tr>
<tr>
<td>Dynamic SQL</td>
<td>- Link found by the inference engine for server
functions or procedures</td>
</tr>
<tr>
<td><strong>Use</strong></td>
<td>Dynamic SQL</td>
<td>- in dynamic SQL character strings.</td>
</tr>
</tbody>
</table>