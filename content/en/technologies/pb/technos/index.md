---
title: "Covered Technologies"
linkTitle: "Covered Technologies"
type: "docs"
weight: 1
---

| Versions supported | Function Points (transactions) | Quality and Sizing | Comments |
|---|:-:|:-:|---|
| PB 8 | :heavy_check_mark: | :heavy_check_mark: | - |
| PB 9 | :heavy_check_mark: | :heavy_check_mark: | - |
| PB 10.0 | :heavy_check_mark: | :heavy_check_mark: | - |
| PB 10.5 | :heavy_check_mark: | :heavy_check_mark: | - |
| PB 11 | :heavy_check_mark: | :heavy_check_mark: | No support of the new features introduced by these versions. However applications built with one of these versions and which are compliant with earlier releases of PowerBuilder, can be analyzed. |
| PB 11.5 | :heavy_check_mark: | :heavy_check_mark: | As above. |
| PB 12 | :heavy_check_mark: | :heavy_check_mark: | As above. |
| PB 12.5 | :heavy_check_mark: | :heavy_check_mark: | As above. |
| PB 12.6 | :heavy_check_mark: | :heavy_check_mark: | As above. |
| 2017, 2017 R2, 2017 R3 (requires CAST Imaging Core 8.3.9) | :heavy_check_mark: | :heavy_check_mark: | As above. |
| 2019 (requires CAST Imaging Core 8.3.30) | :heavy_check_mark: | :heavy_check_mark: | As above. |
| 2021, 2022, 2023  (requires CAST Imaging Core 8.3.50) | :heavy_check_mark: | :heavy_check_mark: | As above. |
