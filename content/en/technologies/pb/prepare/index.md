---
title: "Prepare and deliver the source code"
linkTitle: "Prepare and deliver the source code"
type: "docs"
weight: 4
---

## Information about discovery

Discovery is a process that is actioned during the delivery process.
CAST Imaging will attempt to automatically identify "projects" within your
application using a set of predefined rules. This discovery process also
allows CAST Imaging to set the initial analysis configuration settings. Discoverers are
currently embedded in CAST Imaging Core:

- [PowerBuilder Discoverer](../extensions/pb-discoverer/)

You should read the relevant documentation for each discoverer (provided
in the link above) to understand how the source code will be handled.

## Source code delivery

CAST Imaging Console expects either a ZIP/archive file or source code
located in a folder configured in CAST Imaging Console. You should include in
the ZIP/source code folder all PowerBuilder source code:

- .PBT
- .PBL
- Any associated files.

CAST highly recommends placing the files in a folder dedicated to PowerBuilder. If you are using a ZIP/archive file, zip the folders in the "temp" folder - but do not zip the "temp" folder itself, nor create any intermediary folders:

```
D:\temp
    |-----PowerBuilder
    |-----OtherTechno1
    |-----OtherTechno2
```

## PowerBuilder version

In order for a PowerBuilder analysis to complete successfully

-   the PowerBuilder IDE must be installed on the machine on which
    the analysis is running - i.e. the node
-   or the path to the pborxx.dll file and the PowerBuilder version must
    be populated in the
    `%PROGRAMDATA%\CAST\AipConsole\AipNode\aip-node-app.properties` file
    if the PowerBuilder IDE is not installed on the node.

In essence, CAST Imaging Console needs to know the version of PowerBuilder
you are delivering/analyzing and where the pborxx.dll file is located:

-   CAST ImagingConsole will first attempt to discover the location of the
    pborcXXX.dll file/PowerBuilder version using the Windows
    registry (on the node) during the source code delivery
    process. If CAST Imaging Console finds a registry key on the node, it
    will populate the relevant fields in the CAST Imaging Console GUI (see
    [PowerBuilder - Analysis
    configuration](PowerBuilder_-_Analysis_configuration)).
-   If no registry key is found (i.e. the PowerBuilder IDE is not
    installed on the node), then CAST Imaging Console will look in the
    following file on the target node. For example - these
    locations can be filled in BEFORE delivering the source code (you
    must restart the node so that the changes are taken into
    account):

``` java
≥ 2.x
%PROGRAMDATA%\CAST\AIP-Node\application.yml

1.x
%PROGRAMDATA%\CAST\AipConsole\AipNode\aip-node-app.properties
```

The following sections are relevant and should be uncommented/filled in as appropriate:

``` java
≥ 2.x
# ==============
# PowerBuilder configuration
# --------------
# Provide the full paths of the pborcXXX.dll that are available on this host separated by ';'
#powerbuilder:
#  pborcdll:
#    location: C:\\Users\\ABD\\Documents\\PB9\\Shared_PB9\\pborc90.dll;C:\\Users\\ABD\\Documents\\PB10\\Shared_PB10\\pborc100.dll
# Maximum number of parallel job executions
# If maximum number of parallel job executions is reached, the job will wait in queue

1.x
# ==============
# PowerBuilder configuration
# --------------
# Default version of Power Builder to analyze between (Version8, Version9, VersionX, VersionX5, VersionXI, VersionXI5, VersionXII, VersionXII5, VersionXII6, Version2017, Version2018)
powerbuilder.compile.version=Version8
# Path of the pborcXXX.dll file associated to the default PowerBuilder version
powerbuilder.pborcdll.location=
```

If neither method reveals the location of the pborcXXX.dll
file/PowerBuilder version, then the analysis will fail.  Above all,
the options defined in the CAST Imaging Console GUI have priority over the
Windows registry and the aip-node-app.properties file (in that
order).