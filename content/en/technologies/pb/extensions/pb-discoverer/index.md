---
title: "PowerBuilder Discoverer"
linkTitle: "PowerBuilder Discoverer"
type: "docs"
no_list: false
---

Configures one project for each .PBT file discovered.