---
title: "NoSQL for Java - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.nosqljava

## What's new?

See [Release Notes](rn/).

## Supported NoSQL databases

The NoSQL for Java extension provides support for the following
NoSQL databases:

| Database | Details about how the support is provided |
|---|---|
| Amazon DocumentDB | [Amazon DocumentDB support for Java source code](../../../overview/aws-documentdb/java/)                                                                                                          |
| Apache Lucene     | [Apache Lucene support for Java source code](../../../overview/apache-lucene/java/)                                                                                                               |
| Apache Solr       | [Apache Solr support for Java source code](../../../overview/apache-solr/java/) <br><br> [Apache Solr support for Spring Data source code](../../../overview/apache-solr/spring-data/)                                                                                                                   |
| Azure Cosmos DB   | [Azure Cosmos DB support for Java source code](../../../overview/azure-cosmos-db/java/)<br><br> [Azure Cosmos DB support for Spring Data source code](../../../overview/azure-cosmos-db/spring-data/) |
| Couchbase         | [Couchbase support for Java source code](../../../overview/couchbase/java/)<br><br> [Couchbase support for Spring Data source code](../../../overview/couchbase/spring-data/)                         |
| CouchDB           | [CouchDB support for Java source code](../../../overview/couchdb/java/)                                                                                                                           |
| DynamoDB          | [DynamoDB support for Java source code](../../../overview/dynamodb/java/)<br><br> [DynamoDB support for Spring Data source code](../../../overview/dynamodb/spring-data/)                             |
| Elasticsearch     | [Elasticsearch support for Java source code](../../../overview/elasticsearch/java/)<br><br> [Elasticsearch support for Spring Data source code](../../../overview/elasticsearch/spring-data/)         |
| MarkLogic         | [MarkLogic support for Java source code](../../../overview/marklogic/java/)                                                                                                                       |
| Memcached         | [Memcached support for Java source code](../../../overview/memcached/java/)                                                                                                                       |
| MongoDB           | [MongoDB support for Java source code](../../../overview/mongodb/java/)<br><br> [MongoDB support for Spring Data source code](../../../overview/mongodb/spring-data/)                                 |
| Redis             | [Redis support for Java source code](../../../overview/redis/java/)<br><br> [Redis support for Spring Data source code](../../../overview/redis/spring-data/)                                         |

## Function Point, Quality and Sizing support

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Prerequisites

| Required? | Extensions |
|:-:|---|
| :white_check_mark: | [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest) ≥ 1.3.5-funcrel |
| :white_check_mark: | [com.castsoftware.internal.platform](https://extend.castsoftware.com/#/extension?id=com.castsoftware.internal.platform&version=latest) ≥ 0.9.11 |
| :white_check_mark: | [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest) ≥ 1.7.19-funcrel |

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, should manually install the
extension.

## Handling of NoSQL Java Collection objects for transaction purposes

All NoSQL "Collection" type objects are not classed as having code -
i.e. they are codeless and are created via "getcollection" statements.
As such, these objects either exist or do not and therefore NoSQL for
Java extension does not compute a checksum on them.
See [https://mongodb.github.io/mongo-java-driver/3.4/driver/tutorials/databases-collections/](https://mongodb.github.io/mongo-java-driver/3.4/driver/tutorials/databases-collections/) for
more information. This means that in reality, if a transaction is really
changed to add or remove a collection, then the java code itself is
changed and so the transaction will be  marked as modified. This in turn
means that in an environment where the same release of a the NoSQL for
Java extension is used for successive analyses, changes in java objects
are enough to change the transaction values.

## Data sensitivity

The NoSQL for Java extension is capable of setting a property on
specific objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

See [Data Sensitivity](../../../../multi/data-sensitivity) for more information. For example:

![](../images/635568141.jpg)

## Structural rules

| Release | Link |
|---------|------|
| 2.0.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_nosqljava&ref=\|\|2.0.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_nosqljava&ref=%7C%7C2.0.0-alpha1) |
