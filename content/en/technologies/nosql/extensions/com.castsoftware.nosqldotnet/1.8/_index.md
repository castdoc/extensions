---
title: "NoSQL for .NET - 1.8"
linkTitle: "1.8"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.nosqldotnet

## What's new?

See [Release Notes](rn/).

## Supported NoSQL databases

| Database | Details about how the support is provided |
|---|---|
| Amazon DocumentDB | [Amazon DocumentDB support for .NET source code](../../../overview/aws-documentdb/dotnet/) |
| Apache Solr | [Apache Solr support for .NET source code](../../../overview/apache-solr/dotnet/) |
| Azure Cosmos DB | [Azure Cosmos DB support for .NET source code](../../../overview/azure-cosmos-db/dotnet/) |
| Couchbase | [Couchbase support for .NET source code](../../../overview/couchbase/dotnet/) |
| DynamoDB | [DynamoDB support for .NET source code](../../../overview/dynamodb/dotnet/) |
| Elasticsearch | [Elasticsearch support for .NET source code](../../../overview/elasticsearch/dotnet/) |
| Lucene.NET | [Lucene.NET support for .NET source code](../../../overview/lucene-dotnet/) |
| Memcached | [Memcached support for .NET source code](../../../overview/memcached/dotnet/) |
| MongoDB | [MongoDB support for .NET source code](../../../overview/mongodb/dotnet/) |
| Redis | [Redis support for .NET source code](../../../overview/redis/dotnet/) |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, should manually install the extension.

## Data sensitivity

The NoSQL for .NET extension is capable of setting a property on specific objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

See [Data Sensitivity](../../../../multi/data-sensitivity) for more information. For example:

![](../images/642351156.png)![](../images/642351157.png)![](../images/642351158.png)

## Structural rules

| Release | Link  |
|---------|-------|
| 1.8.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.5-funcrel) |
| 1.8.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.4-funcrel) |
| 1.8.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.3-funcrel) |
| 1.8.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.2-funcrel) |
| 1.8.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.1-funcrel) |
| 1.8.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.0-funcrel) |
| 1.8.0-alpha2  | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.0-alpha2](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.0-alpha2)   |
| 1.8.0-alpha1  | [https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=\|\|1.8.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_nosqldotnet&ref=%7C%7C1.8.0-alpha1)   |
