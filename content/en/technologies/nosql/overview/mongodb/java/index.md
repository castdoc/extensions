---
title: "Support of MongoDB for Java"
linkTitle: "Support of MongoDB for Java"
type: "docs"
---

>CAST supports MongoDB via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about the support provided for Java source code is
discussed below.

## Supported Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh">Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a href="http://jongo.org/"
rel="nofollow">Jongo </a></td>
<td class="confluenceTd">Up to: 1.4.0</td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><a
href="https://docs.mongodb.com/drivers/java-drivers/"
rel="nofollow">Mongo Java Driver </a></td>
<td class="confluenceTd">Up to: 3.12.8</td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
<tr class="odd">
<td class="confluenceTd"><a href="https://mongojack.org"
rel="nofollow">MongoJack</a></td>
<td class="confluenceTd">Up to: 4.11.0</td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
<tr class="odd">
<td class="confluenceTd"><a href="https://www.mongodb.com/docs/drivers/java/sync/current/"
rel="nofollow">Mongo Java Sync Driver</a></td>
<td class="confluenceTd">Up to: 5.0.0</td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><a href="https://mvnrepository.com/artifact/org.mongodb.morphia/morphia"
rel="nofollow">Mongo Morphia</a></td>
<td class="confluenceTd">Up to: 1.3.2</td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><a href="https://mvnrepository.com/artifact/org.mongodb/mongodb-driver-async/3.12.14"
rel="nofollow">Mongo Java Async Driver</a></td>
<td class="confluenceTd">Up to: 3.12.14</td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><a href="https://www.mongodb.com/docs/languages/java/reactive-streams-driver/current/"
rel="nofollow">Mongo Reactive Streams Driver</a></td>
<td class="confluenceTd">Up to: 5.0.0</td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table>
<colgroup>
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Operation</th>
<th class="confluenceTh" style="text-align: left;"><div
class="content-wrapper">
<p>Methods supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>com.mongodb.DBCollection.insert</p>
<p>com.mongodb.client.MongoCollection.insertOne</p>
<p>com.mongodb.client.MongoCollection.insertMany</p>
<p>com.mongodb.DBCollection.save</p>
<p>org.jongo.MongoCollection.insert</p>
<p>org.jongo.MongoCollection.save</p>
<p>org.mongojack.JacksonMongoCollection.insert</p>
<p>org.mongojack.MongoCollectionDecorator.insertMany</p>
<p>org.mongojack.MongoCollectionDecorator.insertOne</p>
<p>com.mongodb.DBCollection.save</p>
<p>org.jongo.MongoCollection.insert</p>
<p>org.jongo.MongoCollection.save</p>
<p>com.mongodb.DBCollection.replaceOrInsert</p>
<p>com.mongodb.DBCollection.setHintFields</p>
<p>com.mongodb.DBCollection.executeWriteOperation</p>
<p>com.mongodb.DBCollection.executeBulkWriteOperation</p>
<p>com.mongodb.async.client.MongoCollection.insertMany</p>
<p>com.mongodb.async.client.MongoCollection.createIndex</p>
<p>com.mongodb.async.client.MongoCollection.createIndexes</p>
<p>com.mongodb.async.client.MongoCollection.insertOne</p>
<p>org.mongodb.morphia.Datastore.save</p>
<p>org.mongodb.morphia.dao.BasicDAO.save</p>
<p>org.mongodb.morphia.dao.DAO.save	</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>com.mongodb.client.MongoDatabase.getCollection</p>
<p>com.mongodb.client.MongoCollection.countDocuments</p>
<p>com.mongodb.client.MongoCollection.count</p>
<p>com.mongodb.client.MongoCollection.find</p>
<p>com.mongodb.client.MongoCollection.findOneAndDelete</p>
<p>com.mongodb.client.MongoCollection.findOneAndReplace</p>
<p>com.mongodb.client.MongoCollection.findOneAndUpdate</p>
<p>com.mongodb.DB.getCollection</p>
<p>com.mongodb.DBCollection.aggregate</p>
<p>com.mongodb.DBCollection.count</p>
<p>com.mongodb.DBCollection.distinct</p>
<p>com.mongodb.DBCollection.find</p>
<p>com.mongodb.DBCollection.findOne</p>
<p>com.mongodb.DBCollection.getCollection</p>
<p>com.mongodb.DBCollection.getCount</p>
<p>com.mongodb.DBCollection.getName</p>
<p>com.mongodb.DBCollection.group</p>
<p>com.mongodb.DBCollection.findAndModify</p>
<p>com.mongodb.DBCollection.findAndRemove</p>
<p>org.jongo.Jongo.getCollection</p>
<p>org.jongo.MongoCollection.aggregate</p>
<p>org.jongo.MongoCollection.count</p>
<p>org.jongo.MongoCollection.distinct</p>
<p>org.jongo.MongoCollection.find</p>
<p>org.jongo.MongoCollection.findAndModify</p>
<p>org.jongo.MongoCollection.findOne</p>
<p>org.jongo.MongoCollection.getDBCollection</p>
<p>org.jongo.MongoCollection.getName</p>
<p>org.mongojack.JacksonMongoCollection.findOne</p>
<p>org.mongojack.JacksonMongoCollection.findOneById</p>
<p>org.mongojack.JacksonMongoCollection.getCollectionKey</p>
<p>org.mongojack.JacksonMongoCollection.getName</p>
<p>org.mongojack.JacksonMongoCollection.getMongoCollection</p>
<p>org.mongojack.MongoCollectionDecorator.countDocuments</p>
<p>org.mongojack.MongoCollectionDecorator.estimatedDocumentCount</p>
<p>org.mongojack.MongoCollectionDecorator.find</p>
<p>org.mongojack.MongoCollectionDecorator.findOneAndDelete</p>
<p>org.mongojack.MongoCollectionDecorator.findOneAndReplace</p>
<p>org.mongojack.MongoCollectionDecorator.findOneAndUpdate</p>
<p>com.mongodb.DBCollection.getFullName</p>
<p>com.mongodb.DBCollection.getHintFields</p>
<p>com.mongodb.DBCollection.getDefaultDBObjectCodec</p>
<p>com.mongodb.client.MongoCollection.aggregate</p>
<p>com.mongodb.client.MongoCollection.watch</p>
<p>org.mongodb.morphia.Datastore.get</p>
<p>org.mongodb.morphia.Datastore.find</p>
<p>org.mongodb.morphia.Datastore.getByKey</p>
<p>org.mongodb.morphia.Datastore.getCollection</p>
<p>org.mongodb.morphia.Datastore.getCount</p>
<p>org.mongodb.morphia.Datastore.findAndDelete</p>
<p>org.mongodb.morphia.Datastore.findAndModify</p>
<p>org.mongodb.morphia.Datastore.getKey</p>
<p>org.mongodb.morphia.Datastore.merge</p>
<p>org.mongodb.morphia.dao.BasicDAO.count</p>
<p>org.mongodb.morphia.dao.BasicDAO.ensureIndexes</p>
<p>org.mongodb.morphia.dao.BasicDAO.exists</p>
<p>org.mongodb.morphia.dao.BasicDAO.findIds</p>
<p>org.mongodb.morphia.dao.BasicDAO.findOne</p>
<p>org.mongodb.morphia.dao.BasicDAO.findOneId</p>
<p>org.mongodb.morphia.dao.BasicDAO.get</p>
<p>org.mongodb.morphia.dao.DAO.count</p>			
<p>org.mongodb.morphia.dao.DAO.exists</p>
<p>org.mongodb.morphia.dao.DAO.find</p>
<p>org.mongodb.morphia.dao.DAO.findIds</p>
<p>org.mongodb.morphia.dao.DAO.findOne</p>
<p>org.mongodb.morphia.dao.DAO.findOneId</p>
<p>org.mongodb.morphia.dao.DAO.get</p>	
<p>org.mongodb.morphia.query.QueryResults.asKeyList</p>
<p>org.mongodb.morphia.query.QueryResults.asList</p>
<p>org.mongodb.morphia.query.QueryResults.countAll</p>
<p>org.mongodb.morphia.query.QueryResults.count</p>
<p>org.mongodb.morphia.query.QueryResults.fetch</p>
<p>org.mongodb.morphia.query.QueryResults.fetchEmptyEntities</p>
<p>org.mongodb.morphia.query.QueryResults.fetchKeys</p>
<p>org.mongodb.morphia.query.QueryResults.get</p>
<p>org.mongodb.morphia.query.QueryResults.getKey</p>
<p>org.mongodb.morphia.query.QueryResults.tail</p>
<p>com.mongodb.async.client.MongoCollection.count</p>
<p>com.mongodb.async.client.MongoCollection.find</p>
<p>com.mongodb.async.client.MongoCollection.countDocuments</p>
<p>com.mongodb.async.client.MongoCollection.estimatedDocumentCount</p>
<p>com.mongodb.async.client.MongoCollection.distinct</p>
<p>com.mongodb.async.client.MongoCollection.FindIterable</p>
<p>com.mongodb.async.client.MongoCollection.AggregateIterable</p>
<p>com.mongodb.async.client.MongoCollection.watch</p>
<p>com.mongodb.async.client.MongoCollection.mapReduce</p>
<p>com.mongodb.async.client.MongoCollection.listIndexes</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>com.mongodb.client.MongoCollection.drop</p>
<p>com.mongodb.client.MongoCollection.deleteOne</p>
<p>com.mongodb.client.MongoCollection.deleteMany</p>
<p>com.mongodb.client.MongoCollection.findOneAndDelete</p>
<p>com.mongodb.DBCollection.drop</p>
<p>com.mongodb.DBCollection.dropIndex</p>
<p>com.mongodb.DBCollection.findAndRemove</p>
<p>org.jongo.MongoCollection.drop</p>
<p>org.jongo.MongoCollection.dropIndex</p>
<p>org.jongo.MongoCollection.dropIndexes</p>
<p>org.jongo.MongoCollection.remove</p>
<p>org.mongojack.JacksonMongoCollection.removeById</p>
<p>org.mongojack.JacksonMongoCollection.drop</p>
<p>org.mongojack.MongoCollectionDecorator.deleteMany</p>
<p>org.mongojack.MongoCollectionDecorator.deleteOne</p>
<p>org.mongojack.MongoCollectionDecorator.drop</p>
<p>org.mongojack.MongoCollectionDecorator.dropIndex</p>
<p>org.mongojack.MongoCollectionDecorator.dropIndexes</p>
<p>org.mongojack.MongoCollectionDecorator.findOneAndDelete</p>
<p>com.mongodb.async.client.MongoCollection.deleteOne</p>
<p>com.mongodb.async.client.MongoCollection.deleteMany</p>
<p>com.mongodb.async.client.MongoCollection.findOneAndDelete</p>
<p>com.mongodb.async.client.MongoCollection.drop</p>
<p>com.mongodb.async.client.MongoCollection.dropIndex</p>
<p>com.mongodb.async.client.MongoCollection.dropIndexes</p>
<p>org.mongodb.morphia.Datastore.delete</p>
<p>org.mongodb.morphia.dao.BasicDAO.delete</p>
<p>org.mongodb.morphia.dao.BasicDAO.deleteById</p>
<p>org.mongodb.morphia.dao.BasicDAO.deleteByQuery</p>
<p>org.mongodb.morphia.dao.DAO.delete</p>
<p>org.mongodb.morphia.dao.DAO.deleteById</p>
<p>org.mongodb.morphia.dao.DAO.deleteByQuery</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>com.mongodb.client.MongoCollection.updateOne</p>
<p>com.mongodb.client.MongoCollection.updateMany</p>
<p>com.mongodb.client.MongoCollection.findOneAndReplace</p>
<p>com.mongodb.client.MongoCollection.findOneAndUpdate</p>
<p>org.jongo.MongoCollection.update</p>
<p>com.mongodb.DBCollection.update</p>
<p>com.mongodb.DBCollection.updateMulti</p>
<p>com.mongodb.DBCollection.findAndModify</p>
<p>com.mongodb.client.MongoCollection.replaceOne</p>
<p>org.mongojack.JacksonMongoCollection.replaceOne</p>
<p>org.mongojack.MongoCollectionDecorator.findOneAndReplace</p>
<p>org.mongojack.MongoCollectionDecorator.findOneAndUpdate</p>
<p>org.mongojack.MongoCollectionDecorator.renameCollection</p>
<p>org.mongojack.MongoCollectionDecorator.replaceOne</p>
<p>org.mongojack.MongoCollectionDecorator.updateMany</p>
<p>org.mongojack.MongoCollectionDecorator.updateOne</p>
<p>com.mongodb.client.MongoCollection.renameCollection</p>
<p>com.mongodb.async.client.MongoCollection.renameCollection</p>
<p>com.mongodb.async.client.MongoCollection.replaceOne</p>
<p>com.mongodb.async.client.MongoCollection.updateOne</p>
<p>com.mongodb.async.client.MongoCollection.updateMany</p>
<p>com.mongodb.async.client.MongoCollection.findOneAndReplace</p>
<p>com.mongodb.async.client.MongoCollection.findOneAndUpdate</p>
<p>org.mongodb.morphia.Datastore.update</p>
<p>org.mongodb.morphia.Datastore.updateFirst</p>
<p>org.mongodb.morphia.dao.BasicDAO.UpdateOperations</p>
<p>org.mongodb.morphia.dao.BasicDAO.update</p>
<p>org.mongodb.morphia.dao.BasicDAO.updateFirst</p>
<p>org.mongodb.morphia.dao.DAO.update</p>
<p>org.mongodb.morphia.dao.DAO.updateFirst</p>
</div></td>
</tr>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">use</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.reactivestreams.Publisher.subscribe</p>
<p>com.mongodb.async.client.MongoCollection.bulkWrite</p>
<p>com.mongodb.client.MongoCollection.bulkWrite</p> 
<p>com.mongodb.BulkWriteOperation.execute</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370566.png" draggable="false"
data-image-src="666370566.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370556"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java MongoDB database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370565.png" class="image-center"
draggable="false" data-image-src="666370565.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370565"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370556"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java MongoDB collection</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370564.png" class="image-center"
draggable="false" data-image-src="666370564.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370556"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java unknown MongoDB database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370563.png" class="image-center"
draggable="false" data-image-src="666370563.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370563"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370556"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java unknown MongoDB collection</td>
</tr>
</tbody>
</table>

## Links

<table class="relative-table wrapped confluenceTable">
<colgroup>
<col style="width: 11%" />
<col style="width: 55%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh">Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd"><p>From Java MongoDB collection object to Java
MongoDB database object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller Java Method objects
(constructors also) and Java Mongo Collection Object</td>
<td class="confluenceTd"><p>mapReduce</p>
<p>createCollection</p>
<p>subscribe</p>
<p>bulkWrite</p>
<p>execute</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>useSelectLink</p></td>
<td class="confluenceTd">Between the caller Java Method objects
(constructors also) and Java Mongo Collection Object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>getCollection</p>
<p>countDocuments</p>
<p>find </p>
<p>count</p>
<p>findOneAndDelete</p>
<p>findOneandReplace</p>
<p>findOneAndUpdate</p>
<p>aggregate</p>
<p>distinct</p>
<p>findOne</p>
<p>getCount</p>
<p>getName</p>
<p>group</p>
<p>findAndModify</p>
<p>findAndRemove</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller Java Method objects
(constructors also) and Java Mongo Collection Object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>update</p>
<p>updateOne</p>
<p>updateMany</p>
<p>findOneAndUpdate</p>
<p>fineOneAndReplace</p>
<p>findAndModify</p>
<p>rename</p>
<p>replaceOne</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the caller Java Method objects
(constructors also) and Java Mongo Collection Object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>findOneAndDelete</p>
<p>drop</p>
<p>deleteOne</p>
<p>deleteMany</p>
<p>dropIndex</p>
<p>dropIndexes</p>
<p>remove</p>
<p>findAndRemove</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller Java Method objects
(constructors also) and Java Mongo Collection Object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>insert</p>
<p>insertOne</p>
<p>insertMany</p>
<p>bulkWrite</p>
<p>save</p>
</div></td>
</tr>
</tbody>
</table>

## Amazon DocumentDB Identification

The below mentioned objects have properties to indicate whether the
object is of type MongoDB or Amazon DocumentDB:

### Java MongoDB collection object

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Property
Description</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of Property
Value</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Value:
Meaning</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Is an Amazon
DocumentDB collection?</td>
<td class="confluenceTd" style="text-align: left;">Integer</td>
<td class="confluenceTd" style="text-align: left;"><p>1: The collection
object is of type Amazon DocumentDB</p>
<p>0: The collection object is of type MongoDB</p></td>
</tr>
</tbody>
</table>

### Java unknown MongoDB collection object

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Property
Description</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of Property
Value</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Value:
Meaning</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Is an Amazon
DocumentDB collection?</td>
<td class="confluenceTd" style="text-align: left;">Integer</td>
<td class="confluenceTd" style="text-align: left;"><p>1: The collection
object is of type Amazon DocumentDB</p>
<p>0: The collection object is of type MongoDB</p></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Creation of Database and Collection 

``` java
public MongoUserDAO(DB db) {
        Mongo mongo = new Mongo();
        db = mongo.getDB("TestDatabase");
        db.createCollection("User", dbo);
}
```

![](666370562.png)

### Insert Operation

``` java
public void insertDocuments(DBObject[] documents) {
        userCol.insert(documents);  
    }
```

![](666370561.png)

### Select Operation

``` java
public void getCollection() {
        DBCollection col =  db.getCollection("User");
    }
```

![](666370560.png)

### Update Operation

``` java
public void updateDoc(DBObject query, DBObject update) {
        userCol.update(query, update);
    }
```

![](666370559.png)

## Delete Operation

![](666370558.png)

## Amazon DocumentDB Identification

``` java
package com.sample.app;
 
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
 
 
public final class App {
    private App() {
    }
    public static void main(String[] args) {
 
        String connectionString = "mongodb://root:rootroot@sample-cluster.cluster-cqjaxx9hbhi0.us-west-2.docdb.amazonaws.com:27017/?replicaSet=rs0&readpreference=secondaryPreferred";
 
        MongoClientURI clientURI = new MongoClientURI(connectionString);
        MongoClient mongoClient = new MongoClient(clientURI);
 
        MongoDatabase testDB = mongoClient.getDatabase("library");
        MongoCollection<Document> numbersCollection = testDB.getCollection("books");
 
        Document doc = new Document("name", "title").append("value", "Harry Potter");
        numbersCollection.insertOne(doc);
 
        MongoCursor<Document> cursor = numbersCollection.find().iterator();
        try {
            while (cursor.hasNext()) {
                System.out.println(cursor.next().toJson());
            }
        } finally {
            cursor.close();
        }
 
    }
}
```

![](666370557.png)

## Known Limitations

-   For the method 'mapReduce' the type of link produced is 'useLink' as
    its type is not determined
-   If collection name is not resolved in the CRUD API, then link is
    created with unknown collection object.
-   Setting of properties related to Amazon DocumentDB identification
    depends on proper resolution of connection string.
