---
title: "Support of MongoDB for Spring Data"
linkTitle: "Support of MongoDB for Spring Data"
type: "docs"
---

>CAST supports MongoDB via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about the support provided for Java with Spring Data source code is
discussed below.

## Supported Libraries

| Library | Version | Supported |
|---|---|:-:|
| [Spring Data MongoDB ](https://spring.io/projects/spring-data-mongodb) | Up to: 4.4.x | :white_check_mark: |

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Operation</th>
<th class="confluenceTh" style="text-align: left;">Scenario</th>
<th class="confluenceTh" style="text-align: left;"><div
class="content-wrapper">
<p>Methods Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;">Repository APIs</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.springframework.data.repository.CrudRepository.save</p>
<p>org.springframework.data.repository.CrudRepository.saveAll</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.save</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.saveAll</p>
<p>org.springframework.data.repository.MongoRepository.insert</p>
<p>org.springframework.data.repository.MongoRepository.save</p>
<p>org.springframework.data.repository.MongoRepository.saveAll</p>
<p>org.springframework.data.mongodb.repository.MongoRepository.insert</p>
<p>org.springframework.data.mongodb.repository.ReactiveMongoRepository.insert</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.saveAll</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.insert</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.save</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.saveAll</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.insert</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.save</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.saveAll</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.insert</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;">Template and
operation APIs</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.springframework.data.mongodb.core.MongoTemplate.insert</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.insertAll</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.save</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.upsert</p>
<p>org.springframework.data.mongodb.core.BulkOperations.insert</p>
<p>org.springframework.data.mongodb.core.BulkOperations.upsert</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.insert</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.insertAll</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.save</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.upsert</p>
<p>org.springframework.data.mongodb.core.MongoOperations.insert</p>
<p>org.springframework.data.mongodb.core.MongoOperations.insertAll</p>
<p>org.springframework.data.mongodb.core.MongoOperations.save</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.insert</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.insertAll</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.upsert</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;">Repository APIs</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.springframework.data.repository.CrudRepository.findAll</p>
<p>org.springframework.data.repository.CrudRepository.count</p>
<p>org.springframework.data.repository.CrudRepository.findById</p>
<p>org.springframework.data.repository.CrudRepository.findAllById</p>
<p>org.springframework.data.repository.CrudRepository.existsById</p>
<p>org.springframework.data.repository.PagingAndSortingRepository.findAll</p>
<p>org.springframework.data.repository.QueryByExampleExecutor.count</p>
<p>org.springframework.data.repository.QueryByExampleExecutor.exists</p>
<p>org.springframework.data.repository.QueryByExampleExecutor.findAll</p>
<p>org.springframework.data.repository.QueryByExampleExecutor.findOne</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.findAll</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.count</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.findById</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.findAllById</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.existsById</p>
<p>org.springframework.data.repository.ReactiveQueryByExampleExecutor.count</p>
<p>org.springframework.data.repository.ReactiveQueryByExampleExecutor.exists</p>
<p>org.springframework.data.repository.ReactiveQueryByExampleExecutor.findAll</p>
<p>org.springframework.data.repository.ReactiveQueryByExampleExecutor.findOne</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.findAll</p>
<p>org.springframework.data.repository.MongoRepository.findAll</p>
<p>org.springframework.data.repository.MongoRepository.count</p>
<p>org.springframework.data.repository.MongoRepository.findById</p>
<p>org.springframework.data.repository.MongoRepository.findAllById</p>
<p>org.springframework.data.repository.MongoRepository.existsById</p>
<p>org.springframework.data.repository.MongoRepository.findOne</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.count</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.findById</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.findAllById</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.existsById</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.findOne</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.findById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.existsById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.findAll</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.findAllById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.count</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.findOne</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.exists</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.findBy</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.findById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.existsById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.findAll</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.findAllById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.count</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.findOne</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.exists</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.findBy</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;">Template and
operation APIs</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.springframework.data.mongodb.core.MongoTemplate.getCollection</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.find</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAll</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAndModify</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAndReplace</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findById</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findDistinct</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findOne</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.exists</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.count</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.collectionExists</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.stream</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.group</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.geoNear</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.aggregate</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.aggregateStream</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.aggregateAndReturn</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.executeQuery</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.getCollection</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.find</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAll</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAndModify</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAndReplace</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findById</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findDistinct</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findOne</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.exists</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.count</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.collectionExists</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.stream</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.group</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.geoNear</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.aggregate</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.aggregateStream</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.aggregateAndReturn</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.executeQuery</p>
<p>org.springframework.data.mongodb.core.MongoOperations.aggregate</p>
<p>org.springframework.data.mongodb.core.MongoOperations.aggregateStream</p>
<p>org.springframework.data.mongodb.core.MongoOperations.aggregateAndReturn</p>
<p>org.springframework.data.mongodb.core.MongoOperations.collectionExists</p>
<p>org.springframework.data.mongodb.core.MongoOperations.count</p>
<p>org.springframework.data.mongodb.core.MongoOperations.exists</p>
<p>org.springframework.data.mongodb.core.MongoOperations.find</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAll</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAndModify</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAndReplace</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findById</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findOne</p>
<p>org.springframework.data.mongodb.core.MongoOperations.geoNear</p>
<p>org.springframework.data.mongodb.core.MongoOperations.getCollection</p>
<p>org.springframework.data.mongodb.core.MongoOperations.group</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.aggregate</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.aggregateStream</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.aggregateAndReturn</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.collectionExists</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.count</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.exists</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.find</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAll</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAndModify</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAndReplace</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findById</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findOne</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.geoNear</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.getCollection</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.group</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;">Repository APIs</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.springframework.data.repository.CrudRepository.deleteAll</p>
<p>org.springframework.data.repository.CrudRepository.deleteById</p>
<p>org.springframework.data.repository.CrudRepository.delete</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.deleteAll</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.deleteById</p>
<p>org.springframework.data.repository.ReactiveCrudRepository.delete</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.deleteAll</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.deleteById</p>
<p>org.springframework.data.repository.ReactiveMongoRepository.delete</p>
<p>org.springframework.data.repository.MongoRepository.delete</p>
<p>org.springframework.data.repository.MongoRepository.deleteAll</p>
<p>org.springframework.data.repository.MongoRepository.deleteById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.deleteById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.delete</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.deleteAllById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleMongoRepository.deleteAll</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.deleteById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.delete</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.deleteAllById</p>
<p>org.springframework.data.mongodb.repository.support.SimpleReactiveMongoRepository.deleteAll</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;">Template and
operation APIs</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.springframework.data.mongodb.core.MongoTemplate.findAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.dropCollection</p>
<p>org.springframework.data.mongodb.core.BulkOperations.remove</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.remove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.remove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.dropCollection</p>
<p>org.springframework.data.mongodb.core.ReactiveBulkOperations.remove</p>
<p>org.springframework.data.mongodb.core.MongoOperations.dropCollection</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.MongoOperations.remove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.dropCollection</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAllAndRemove</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.remove</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;">Template and
operation APIs</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>org.springframework.data.mongodb.core.MongoTemplate.upsert</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.update</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.updateFirst</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.updateMulti</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAndModify</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.findAndReplace</p>
<p>org.springframework.data.mongodb.core.MongoTemplate.replace</p>
<p>org.springframework.data.mongodb.core.BulkOperations.updateOne</p>
<p>org.springframework.data.mongodb.core.BulkOperations.updateMulti</p>
<p>org.springframework.data.mongodb.core.BulkOperations.upsert</p>
<p>org.springframework.data.mongodb.core.BulkOperations.replaceOne</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.upsert</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.update</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.updateFirst</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.updateMulti</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAndModify</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.findAndReplace</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoTemplate.replace</p>
<p>org.springframework.data.mongodb.core.ReactiveBulkOperations.updateOne</p>
<p>org.springframework.data.mongodb.core.ReactiveBulkOperations.updateMulti</p>
<p>org.springframework.data.mongodb.core.ReactiveBulkOperations.upsert</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAndModify</p>
<p>org.springframework.data.mongodb.core.MongoOperations.findAndReplace</p>
<p>org.springframework.data.mongodb.core.MongoOperations.upsert</p>
<p>org.springframework.data.mongodb.core.MongoOperations.updateFirst</p>
<p>org.springframework.data.mongodb.core.MongoOperations.updateMulti</p>
<p>org.springframework.data.mongodb.core.MongoOperations.replace</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAndModify</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.findAndReplace</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.upsert</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.updateFirst</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.updateMulti</p>
<p>org.springframework.data.mongodb.core.ReactiveMongoOperations.replace</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370615.png" draggable="false"
data-image-src="666370615.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370615"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370603"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java MongoDB database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370614.png" class="image-center"
draggable="false" data-image-src="666370614.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370614"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370603"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java MongoDB collection</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370613.png" class="image-center"
draggable="false" data-image-src="666370613.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370613"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370603"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java unknown MongoDB database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370612.png" class="image-center"
draggable="false" data-image-src="666370612.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370612"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370603"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Java unknown MongoDB collection</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh">Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">parentLink</td>
<td class="confluenceTd"><p>Between Mongo Objects (Collection →
Database → Project)</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Mongo Collection Object</td>
<td class="confluenceTd"><p>mapReduce</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>useSelectLink</p></td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Mongo Collection Object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>find</p>
<p>findAll</p>
<p>findById</p>
<p>findAllById</p>
<p>findAll</p>
<p>count</p>
<p>existsById</p>
<p>exists</p>
<p>findAndRemove</p>
<p>findAllAndRemove</p>
<p>findAndReplace</p>
<p>findAndModify</p>
<p>findOne</p>
<p>stream</p>
<p>group</p>
<p>geoNear</p>
<p>aggregate</p>
<p>aggregateStream</p>
<p>aggregateAndReturn</p>
<p>executeQuery</p>
<p>getCollection</p>
<p>findDistinct</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Mongo Collection Object</td>
<td class="confluenceTd"><p>upsert</p>
<p>update</p>
<p>updateFirst</p>
<p>updateOne</p>
<p>updateMulti</p>
<p>findAndModify</p>
<p>findAndReplace</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Mongo Collection Object</td>
<td class="confluenceTd"><p>deleteAll</p>
<p>deleteById</p>
<p>delete</p>
<p>findAndRemove</p>
<p>dropCollection</p>
<p>remove</p>
<p>findAllAndRemove</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Mongo Collection Object</td>
<td class="confluenceTd"><p>save</p>
<p>saveAll</p>
<p>insert</p>
<p>insertAll</p>
<p>upsert</p></td>
</tr>
</tbody>
</table>

## What results can you expect

Some example scenarios are shown below:

### Creation of database objects

#### With application.properties

``` java
spring.data.mongodb.host=localhost
spring.data.mongodb.port=27017
spring.data.mongodb.database=hc4

logging.level.org.springframework.data=debug
logging.level.=error
```

![](666370611.png)

#### With Java configuration file

``` java
package com.mkyong;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories
public class ApplicationConfig extends AbstractMongoConfiguration{
     @Override
     @Bean
      protected String getDatabaseName() {
        return "e-store";
      }

      @Override
      protected String getMappingBasePackage() {
        return "com.oreilly.springdata.mongodb";
      }
    @Override
    @Bean
    public Mongo mongo() throws Exception {
        // TODO Auto-generated method stub
        return new Mongo("localhost");
    }
}
```

![](666370610.png)

### Insert Operation

``` java
public Product getMrepefOne () {
    Product product = new Product("LN1", "London", 5.0f);
    productRepository.save(product);

    return productRepository.findOne("LN1");
}
```

![](666370609.png)

``` java
@Override
    public BulkWriteResult bulk(Collection<? extends T> objects) {
      Assert.notNull(objects, "Objects must not be null!");
      return mongoTemplate.bulkOps(bulkMode != null ? bulkMode : BulkMode.ORDERED, Product.class)
          .insert(new ArrayList<>(objects)).execute();
    }
```

![](666370608.png)

### Update Operation

``` java
public String updateAlertStatus(String alertId) {

        LOGGER.debug("FanAlertsRepository.deleteAlert() with param{0}" + alertId);
        Query query = new Query();

        query.addCriteria(Criteria.where(AlertServiceConstants._ID_FILED).is(new ObjectId(alertId)));

        FanAlerts fanAlert = mongoOperation.findOne(query, FanAlerts.class,
                AlertServiceConstants.FAN_ALERTS_COLLECTION);
        if (fanAlert == null) {
            return AlertServiceConstants.failure;
    
```

![](666370607.png)

### Delete Operation

``` java
public Response deleteTn(String siteId, String tn) {
        DeleteResult deleteres = mongoCon.getCollection()
                    .deleteOne(new Document().append(DETAILSSITE_IDEN, siteId).append(DETAILS_TN, tn));
}
```

![](666370606.png)

### Select Operation

``` java
public GeoResults getProductByLocation (double longitude,double latitude,double distance){
    Point p = new Point(longitude, latitude);
    NearQuery nearQuery  = NearQuery.near(p, Metrics.KILOMETERS).maxDistance(distance);
    return mongoTemplate.geoNear(nearQuery, Product.class);
    
}
```

![](666370605.png)

### Query Methods 

``` java
@Query("{ 'age' : { $gt: ?0, $lt: ?1 } }")
    List<User> findUsersByAgeBetween(int ageGT, int ageLT);
```

![](666370604.png)

## Known Limitations

-   Resolution of Database and Collection is limited, "Unknown" is used
    when not resolved.
-   Query method results in link between query method and repository
    collection (domain entity) and not between the actual caller method
    and repository collection
