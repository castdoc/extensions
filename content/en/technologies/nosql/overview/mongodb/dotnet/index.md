---
title: "Support of MongoDB for .NET"
linkTitle: "Support of MongoDB for .NET"
type: "docs"
---

>CAST supports MongoDB via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about the support provided for .NET source code is discussed below.


## Supported Client Libraries

<table>
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a
href="https://docs.mongodb.com/drivers/csharp/"
rel="nofollow">Mongodb.dotnet driver</a></td>
<td class="confluenceTd">2.25.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table>
<colgroup>
<col />
<col />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Operation</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Methods
Supported</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>MongoDB.Driver.IMongoCollection.InsertOne</p>
<p>MongoDB.Driver.IMongoCollection.InsertOneAsync</p>
<p>MongoDB.Driver.IMongoCollection.InsertManyAsync</p>
<p>MongoDB.Driver.IMongoCollection.InsertMany</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="margin-left: 0.0px;">
<p>MongoDB.Driver.IMongoCollection.UpdateManyAsync</p>
<p>MongoDB.Driver.IMongoCollection.UpdateOneAsync</p>
<p>MongoDB.Driver.IMongoCollection.UpdateOne</p>
<p>MongoDB.Driver.IMongoCollection.UpdateMany</p>
<p>MongoDB.Driver.IMongoCollection.ReplaceOne</p>
<p>MongoDB.Driver.IMongoCollection.ReplaceOneAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.ReplaceOne</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.ReplaceOneAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateMany</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateManyAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateOne</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateOneAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="margin-left: 0.0px;">
<p>MongoDB.Driver.IMongoCollection.Find</p>
<p>MongoDB.Driver.IMongoCollection.FindSync</p>
<p>MongoDB.Driver.IMongoCollection.FindAsync</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndDelete</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndDeleteAync</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndReplace</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndReplaceAsync</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndUpdate</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndUpdateAsync</p>
<p>MongoDB.Driver.IMongoCollection.Count</p>
<p>MongoDB.Driver.IMongoCollection.CountAsync</p>
<p>MongoDB.Driver.IMongoCollection.Aggregate</p>
<p>MongoDB.Driver.IMongoCollection.AggregateAsync</p>
<p>MongoDB.Driver.IMongoCollection.BulkWrite</p>
<p>MongoDB.Driver.IMongoCollection.BulkWriteAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.ReplaceOne</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.ReplaceOneAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateMany</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateManyAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateOne</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.UpdateOneAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.FindOneAndDelete</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.FindOneAndDeleteAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.FindOneAndReplace</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.FindOneAndReplaceAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.FindOneAndUpdate</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.FindOneAndUpdateAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.Distinct</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.DistinctAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.CountDocuments</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.CountDocumentsAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.Find</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.Aggregate</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.Count</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.CountAsync</p>
<p>MongoDB.Driver.IMongoCollection.CountDocuments</p>
<p>MongoDB.Driver.IMongoCollection.CountDocumentsAsync</p>
<p>MongoDB.Driver.IMongoCollection.Distinct</p>
<p>MongoDB.Driver.IMongoCollection.DistinctAsync</p>
<p>MongoDB.Driver.IMongoCollection.DistinctMany</p>
<p>MongoDB.Driver.IMongoCollection.DistinctManyAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.DistinctMany</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.DistinctManyAsync</p>
<p>MongoDB.Driver.IMongoCollection.AggregateToCollection</p>
<p>MongoDB.Driver.IMongoCollection.AggregateToCollectionAsync</p>
<p>MongoDB.Driver.IMongoCollection.EstimatedDocumentCount</p>
<p>MongoDB.Driver.IMongoCollection.EstimatedDocumentCountAsync</p>
<p>MongoDB.Driver.IMongoCollection.Watch</p>
<p>MongoDB.Driver.IMongoCollection.WatchAsync</p>
<p>MongoDB.Driver.IMongoCollection.MapReduce</p>
<p>MongoDB.Driver.IMongoCollection.MapReduceAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Delete </td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>MongoDB.Driver.IMongoCollection.DeleteMany</p>
<p>MongoDB.Driver.IMongoCollection.DeleteManyAsync</p>
<p>MongoDB.Driver.IMongoCollection.DeleteOne</p>
<p>MongoDB.Driver.IMongoCollection.DeleteOneAsync</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndDelete</p>
<p>MongoDB.Driver.IMongoCollection.FindOneAndDeleteAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.DeleteOne</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.DeleteOneAsync</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.DeleteMany</p>
<p>MongoDB.Driver.IMongoCollectionExtensions.DeleteManyAsync</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table>
<colgroup>
<col/>
<col/>
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370539.png" draggable="false"
data-image-src="666370539.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370539"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370528"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd"><p>DotNet MongoDB database</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370538.png" class="image-center"
draggable="false" data-image-src="666370538.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370538"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370528"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">DotNet MongoDB collection</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370537.png" class="image-center"
draggable="false" data-image-src="666370537.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370537"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370528"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">DotNet unknown MongoDB database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370536.png" class="image-center"
draggable="false" data-image-src="666370536.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370536"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370528"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">DotNet unknown MongoDB collection</td>
</tr>
</tbody>
</table>

## Links

<table>
<colgroup>
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Link type</th>
<th class="confluenceTh" style="text-align: left;">Source and
destination of link</th>
<th class="confluenceTh" style="text-align: left;"> Methods
supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">belongsTo</td>
<td class="confluenceTd" style="text-align: left;"><p>From DotNet
MongoDB Collection object to DotNet MongoDB Database object</p></td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><br />
 useInsertLink</td>
<td class="confluenceTd" style="text-align: left;"><p>Between the caller
.NET Method (C#) object and DotNet  MongoDB Collection object</p>
<p>Between the caller .NET Method (C#) object and DotNet unknown MongoDB
Collection object</p>
<p><br />
</p></td>
<td class="confluenceTd" style="text-align: left;"><p>InsertOne</p>
<p>InsertOneAsync</p>
<p>InsertMany</p>
<p>InsertManyAsync</p>
<p>Insert</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useUpdateLink</td>
<td class="confluenceTd" style="text-align: left;"><p>Between the caller
.NET Method (C#) object and DotNet  MongoDB Collection object</p>
<p>Between the caller .NET Method (C#) object and DotNet unknown MongoDB
Collection object</p></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="margin-left: 0.0px;">
<p>UpdateManyAsync</p>
<p>UpdateOneAsync</p>
<p>UpdateOne</p>
<p>UpdateMany</p>
<p>ReplaceOne</p>
<p>ReplaceOneAsync</p>
<p>Update</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"> useSelectLink</td>
<td class="confluenceTd" style="text-align: left;"><p>Between the caller
.NET Method (C#) object and DotNet  MongoDB Collection object</p>
<p>Between the caller .NET Method (C#) object and DotNet unknown MongoDB
Collection object</p></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>Find</p>
<p>FindSync</p>
<p>FindAsync</p>
<p>FindOne</p>
<p>FindOneAs</p>
<p>FindOneAndDelete</p>
<p>FindOneAndDeleteAync</p>
<p>FindOneAndReplace</p>
<p>FindOneAndReplaceAsync</p>
<p>FindOneAndUpdate</p>
<p>FinOneAndUpdateAsync</p>
<p>BulkWrite</p>
<p>BulkWriteAsync</p>
<p>Count</p>
<p>CountAsync</p>
<p>Aggregate</p>
<p>AggregateAsync</p>
<p>CountDocuments</p>
<p>CountDocumentsAsync</p>
<p>Distinct</p>
<p>DistinctMany</p>
<p>DistinctManyAsync</p>
<p>Watch</p>
<p>WatchAsync</p>
<p>MapReduce</p>
<p>MapReduceAsync</p>
<p>AggregateToCollection</p>
<p>AggregateToCollectionAsync</p>
<p>EstimatedDocumentCount</p>
<p>EstimatedDocumentCountAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useDeleteLink </td>
<td class="confluenceTd" style="text-align: left;"><p>Between the caller
.NET Method (C#) object and DotNet  MongoDB Collection object</p>
<p>Between the caller .NET Method (C#) object and DotNet unknown MongoDB
Collection object</p></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>DeleteMany</p>
<p>DeleteManyAsync</p>
<p>DeleteOne</p>
<p>DeleteOneAsync</p>
<p>FindOneAndDelete</p>
<p>FindOneAndDeleteAsync</p>
</div></td>
</tr>
</tbody>
</table>

## Amazon DocumentDB Identification

The below mentioned objects have properties to indicate whether the
object is of type MongoDB or Amazon DocumentDB:

### DotNet MongoDB collection object

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Property
Description</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of Property
Value</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Value:
Meaning</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Is an Amazon
DocumentDB collection?</td>
<td class="confluenceTd" style="text-align: left;">Integer</td>
<td class="confluenceTd" style="text-align: left;"><p>1: The collection
object is of type Amazon DocumentDB</p>
<p>0: The collection object is of type MongoDB</p></td>
</tr>
</tbody>
</table>

### DotNet unknown MongoDB collection object

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Property
Description</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of Property
Value</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Value:
Meaning</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Is an Amazon
DocumentDB collection?</td>
<td class="confluenceTd" style="text-align: left;">Integer</td>
<td class="confluenceTd" style="text-align: left;"><p>1: The collection
object is of type Amazon DocumentDB</p>
<p>0: The collection object is of type MongoDB</p></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Database and Collection Creation

``` c#
static void InsertOneInMongoDB()
        {
        
            MongoClient client = new MongoClient("mongodb://127.0.0.1:27017");
          
            IMongoDatabase db = client.GetDatabase("TestDb1");
     
            IMongoCollection<Person> persons = db.GetCollection<Person>("Persons");
            IMongoCollection<Dog> dogs = db.GetCollection<Dog>("Dogs");
}
```

![](666370535.png)

### Insert Operation

``` c#
{
            MongoClient client = new MongoClient("mongodb://127.0.0.1:27017");
            IMongoDatabase db = client.GetDatabase("TestDb1");
            IMongoCollection<BsonDocument> dogs = db.GetCollection<BsonDocument>("Dogs");

            string json = "{Name:'大黄',Age:10,Weight:50}";
            BsonDocument d1 = BsonDocument.Parse(json);//Json
            dogs.InsertOne(d1);

}
```

![](666370534.png)

### Delete Operation

``` c#
 public void DeleteAllPurchases()
        {
            MyShoppingCollection.DeleteMany(FilterDefinition<PurchaseBson>.Empty, null);
        }
```

![](666370533.png)

### Select Operation

``` java
public PurchaseBson[] FindPurchases()
        {
            var purchases = MyShoppingCollection.Find(FilterDefinition<PurchaseBson>.Empty, null).ToList();
            return purchases.ToArray();
        }
```

![](666370532.png)

### Update Operation

``` c#
 public async Task<IActionResult> Update()
        {
            var query = from k in (from a in _context.Books.AsQueryable()
                                    where a.Title == "Test Book 2"
                                    from b in a.Posts
                                    select b)
                         where k.Title == "Second One"
                         select k;
            var result = await query.FirstOrDefaultAsync();
            result.ReadCount = 5;

            _context.Books
                .FindOneAndUpdate(x => x.Title == "Test Book 2" && x.Posts.Any(p => p.Title == "Second One"),
                                    Builders<Book>.Update.Set(x => x.Posts[-1], result));

            return Redirect("/");
        }
```

![](666370531.png)

### Amazon DocumentDB Identification

``` c#
class Program
    {
       static void Main(string[] args)
        {
           string template = "mongodb://{0}:{1}@{2}/?replicaSet=rs0&readpreference={3}";
           string username = "root";
           string password = "rootroot";
           string clusterEndpoint = "sample-cluster.cluster-cqjaxx9hbhi0.us-west-2.docdb.amazonaws.com:27017";
           string readPreference = "secondaryPreferred";

           string connectionString = String.Format(template, username, password, clusterEndpoint, readPreference);
           var settings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
           var client = new MongoClient(settings);

           var database = client.GetDatabase("library");
           var collection = database.GetCollection<BsonDocument>("Books");

           var docToInsert = new BsonDocument { { "title", "Harry Potter" } };
           collection.InsertOne(docToInsert);
        }
    }
```

![](666370530.png)

### Reading Database and Collection from Json File

#### .cs code

``` c#
 public class CustomerService
    {
        private readonly IMongoCollection<Customer> _customer;
        private readonly DeveloperDatabaseConfiguration _settings;

        public CustomerService(IOptions<DeveloperDatabaseConfiguration> settings)
        {
            _settings = settings.Value;
            var client = new MongoClient(_settings.ConnectionString);
            var database = client.GetDatabase(_settings.DatabaseName);
            _customer = database.GetCollection<Customer>(_settings.CustomerCollectionName);
        }

        public async Task<List<Customer>> GetAllAsync()
        {
            return await _customer.Find(c => true).ToListAsync();
        }
    }
```

#### .cs file

``` c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkingWithMongoDB.WebAPI.Configuration
{
    public class DeveloperDatabaseConfiguration
    {
        public string CustomerCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
```

#### json file

``` java
{
  "DeveloperDatabaseConfiguration": {
    "CustomerCollectionName": "Customers",
    "ConnectionString": "mongodb://localhost:27017",
    "DatabaseName": "DevelopmentDatabase"
  },
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*"
}
```

![](666370529.png)

Known Limitations

-   Database and Collections are resolved in following situation: 
    -   Anywhere in the .cs files  and JSON files
-   If Database and Collection values are not resolved, will create
    Unknown Database and Collection objects
-   Setting of properties related to Amazon DocumentDB identification
    depends on proper resolution of connection string. 
