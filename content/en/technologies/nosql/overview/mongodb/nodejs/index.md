---
title: "Support of MongoDB for Node.js"
linkTitle: "Support of MongoDB for Node.js"
type: "docs"
---

>CAST supports MongoDB via
its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
extension. Details about the support provided for Node.js source code is
discussed below.

## Supported Libraries

The following libraries are supported:
- MongoDB
- Mongoose
- Prisma

## Objects

| Icon                                              | Description                        |
|---------------------------------------------------|------------------------------------|
| ![](322084700.png)                              | Node.js MongoDB connection         |
| ![](CAST_NodeJS_Unknown_MongoDB_Connection.png) | Node.js unknown MongoDB connection |
| ![](322084699.png)                              | Node.js MongoDB collection         |

## Supported MongoDB

### Supported links

| Link Type | Source and destination of link | Supported APIs |
|---|---|---|
| useInsertLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>db.collection.insertMany</li></ul> |
| useUpdateLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>db.collection.findOneAndUpdate</li><li>db.collection.findOneAndReplace</li><li>db.collection.replaceOne</li><li>db.collection.updateOne</li><li>db.collection.updateMany</li></ul> |
| useDeleteLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>db.collection.findOneAndDelete</li><li>db.collection.deleteOne</li><li>db.collection.deleteMany</li><li>db.collection.remove</li></ul> |
| useSelectLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>db.collection.find</li><li>db.collection.findOne</li></ul>   |

### Example

Taking the following codes:

``` js
var MongoClient = require('mongodb').MongoClient

var url = 'mongodb://localhost:27017/myproject';

var insertDocuments = function(db, callback) {
  var collection = db.collection('documents');
  collection.insertMany([
    {a : 1}, {a : 2}, {a : 3}
  ], function(err, result) {
    callback(result);
  });
}

var updateDocument = function(db, callback) {
  var collection = db.collection('documents');
  for (i = 0; i < 2; i += 1) {
	collection.updateOne({ a : 2 }
	  , { $set: { b : 1 } }, function(err, result) {
	  callback(result);
	});
  }
}

MongoClient.connect(url, function(err, db) {
  insertDocuments(db, function() {
    updateDocument(db, function() {
      db.close();
    });
  });
});
```

In this example, a 'Node.js MongoDB connection' and a 'Node.js MongoDB collection' objects are created.
This extension creates a 'useInsert' link from function 'insertDocuments' to
the collection 'documents'
and a 'useUpdate' link from function 'updateDocument' to the collection 'documents':

![](nodejs_mongodb.png)

## Supported Mongoose

### Supported links

| Link Type | Source and destination of link | Supported APIs |
|---|---|---|
| useInsertLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>model.create</li><li>model.insertMany</li></ul>   |
| useUpdateLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>model.findByIdAndUpdate</li><li>model.findOneAndUpdate</li><li>model.findOneAndReplace</li><li>model.replaceOne</li><li>model.updateOne</li><li>model.updateMany</li></ul> |
| useDeleteLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>model.deleteOne</li><li>model.deleteMany</li><li>model.findByIdAndDelete</li><li>model.findOneAndDelete</li></ul>   |
| useSelectLink | Between JavaScript Function (JavaScript Initialisation also) and Node.js MongoDB collection | <ul><li>model.exists</li><li>model.find</li><li>model.findOne</li><li>model.findById</li><li>model.where</li></ul> |

### Example

Taking the following codes:

``` js
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/analyzerlauncher', function(err) {
  if (err) { throw err; }
});

userModel = mongoose.model('users', userSchema);

function find(req,res) {
  userModel.findOne(req.params.id, function (err, authorize) {})
}
```

In this example, a 'Node.js MongoDB connection' and a 'Node.js MongoDB collection' objects are created.
This extension creates a 'useSelect' link from function 'find' to the collection 'users':

![](nodejs_mongoose.png)

## Supported Prisma

See [Prisma support for Node.js - MongoDB database](../../../../web/nodejs/com.castsoftware.nodejs/2.11/results/prisma/#supported-persistence-mongodb-database).
