---
title: "Support of MongoDB for TypeScript"
linkTitle: "Support of MongoDB for TypeScript"
type: "docs"
---

>CAST supports MongoDB via its
[com.castsoftware.typescript](https://extend.castsoftware.com/#/extension?id=com.castsoftware.typescript&version=latest)
extension. Details about the support provided for TypeScript source code is discussed below.

## Supported Libraries

The following libraries are supported:
- MongoDB
- Mongoose
- Prisma

## Objects

| Icon                                | Description                        |
|-------------------------------------|------------------------------------|
| ![](mongodb_connection.png)         | Node.js MongoDB connection         |
| ![](unknown_mongodb_connection.png) | Node.js unknown MongoDB connection |
| ![](mongodb_collection.png)         | Node.js MongoDB collection         |
| ![](unknown_mongodb_collection.png) | Node.js unknown MongoDB collection |

## Supported MongoDB

### Supported links

| Link Type     | Source and destination of link                                           | Supported APIs                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|---------------|--------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| useInsertLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>db.collection.insert</li><li>db.collection.insertMany</li><li>db.collection.insertOne</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| useUpdateLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>db.collection.bulkWrite</li><li>db.collection.findAndModify</li><li>db.collection.findOneAndReplace</li><li>db.collection.findOneAndUpdate</li><li>db.collection.replaceOne</li><li>db.collection.update</li><li>db.collection.updateMany</li><li>db.collection.updateOne</li></ul>                                                                                                                                                                                                                                                                                    |
| useDeleteLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>db.collection.deleteMany</li><li>db.collection.deleteOne</li><li>db.collection.drop</li><li>db.collection.findOneAndDelete</li><li>db.collection.remove</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                      |
| useSelectLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>db.collection.aggregate</li><li>db.collection.count</li><li>db.collection.countDocuments</li><li>db.collection.createIndex</li><li>db.collection.createIndexes</li><li>db.collection.distinct</li><li>db.collection.dropIndex</li><li>db.collection.dropIndexes</li><li>db.collection.ensureIndex</li><li>db.collection.estimatedDocumentCount</li><li>db.collection.find</li><li>db.collection.findOne</li><li>db.collection.isCapped</li><li>db.collection.mapReduce</li><li>db.collection.reIndex</li><li>db.collection.stats</li><li>db.collection.watch</li></ul> |

### Example

Taking the following codes:

``` ts
import * as http from 'http';
import {MongoClient} from 'mongodb';

const server = http.createServer(async (req, res) => {
  await new Promise((resolve, reject) => {
    MongoClient.connect('mongodb://localhost:2701/mongodb', function(err: any, client: any) {
      client.db('admin').collection('docs').findOne().then(
        (resDB: any) => {
          res.end('${resDB}');
          client.close();
        },
        (err: any) => {
          res.end('${err}');
          client.close();
        },
      );
    });
  });
});
```

In this example, a 'Node.js MongoDB connection' and a 'Node.js MongoDB collection' objects are created.
This extension creates a 'useSelect' link from function 'Anonymous1' to the collection 'docs':

![](typescript_mongodb.png)

## Supported Mongoose

### Supported links

| Link Type     | Source and destination of link                                           | Supported APIs                                                                                                                                                                                             |
|---------------|--------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| useInsertLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>model.create</li><li>model.insertMany</li></ul>                                                                                                                                                    |
| useUpdateLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>model.aggregate</li><li>model.findByIdAndUpdate</li><li>model.findOneAndUpdate</li><li>model.findOneAndReplace</li><li>model.replaceOne</li><li>model.updateOne</li><li>model.updateMany</li></ul> |
| useDeleteLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>model.deleteOne</li><li>model.deleteMany</li><li>model.findByIdAndDelete</li><li>model.findOneAndDelete</li></ul>                                                                                  |
| useSelectLink | Between TypeScript Function/Method/Module and Node.js MongoDB collection | <ul><li>model.exists</li><li>model.find</li><li>model.findOne</li><li>model.findById</li><li>model.where</li></ul>                                                                                         |

### Example

Taking the following codes:

``` ts
import * as mongoose from 'mongoose'

class DbService {
  private db: any;
  private dbConn: any = null;
  constructor (conn: any = connection) {
    this.dbConn = conn
  };
  connect (): Promise<string> {
    mongoose.connect('mongodb://localhost:2701/mongoose', this.dbConn.options)
      .then(success => {
        this.db = mongoose.connection
      })
  }
  async findbyId (tprid, id?) {
    response = await this.db.models.Engagement.find(query);
  }
}
```

In this example, a 'Node.js MongoDB connection' and a 'Node.js MongoDB collection' objects are created.
This extension creates a 'useSelect' link from method 'findbyId' to the collection 'Engagement':

![](typescript_mongoose.png)

## Supported Prisma

See [Node.js - Prisma support - MongoDB database](../../../../web/typescript/com.castsoftware.typescript/1.15/results/nodejs-prisma/#supported-persistence-mongodb-database).
