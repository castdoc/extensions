---
title: "Support of Apache Solr for Spring Data"
linkTitle: "Support of Apache Solr for Spring Data"
type: "docs"
---


>CAST supports Apache Solr for Spring Data via its [com.castsoftware.nosqljava](../../../extensions/com.castsoftware.nosqljava/) extension, since release 1.9. Details about the support provided for Java with Spring Data source code are discussed below.

## Supported Libraries

| Library                                                                | Version      | Supported          |
|------------------------------------------------------------------------|--------------|:------------------:|
| [Spring Data Apache Solr](https://spring.io/projects/spring-data-solr) | Up to: 4.3.x | :heavy_check_mark: |

## Supported Operations

| Operation | Scenario        | Methods Supported |
|-----------|-----------------|-------------------|
| Insert    | Repository APIs | org.springframework.data.repository.CrudRepository.save<br>org.springframework.data.repository.CrudRepository.saveAll 
|| Solr Template APIs | org.springframework.data.solr.core.SolrTemplate.saveBean<br>org.springframework.data.solr.core.SolrTemplate.saveBeans<br>org.springframework.data.solr.core.SolrTemplate.saveDocument<br>org.springframework.data.solr.core.SolrTemplate.saveDocuments|
| Select    | Repository APIs | org.springframework.data.repository.CrudRepository.findAll<br>org.springframework.data.repository.CrudRepository.count<br>org.springframework.data.repository.CrudRepository.findById   org.springframework.data.repository.CrudRepository.findAllById<br>org.springframework.data.repository.CrudRepository.existsById<br>org.springframework.data.repository.PagingAndSortingRepository.findAll |
|| Solr Template APIs | org.springframework.data.solr.core.SolrTemplate.query<br>org.springframework.data.solr.core.SolrTemplate.queryForCursor<br>org.springframework.data.solr.core.SolrTemplate.queryForFacetAndHighlightPage<br>org.springframework.data.solr.core.SolrTemplate.queryForFacetPage<br>org.springframework.data.solr.core.SolrTemplate.queryForGroupPage<br>org.springframework.data.solr.core.SolrTemplate.queryForHighlightPage<br>org.springframework.data.solr.core.SolrTemplate.queryForObject<br>org.springframework.data.solr.core.SolrTemplate.queryForPage<br>org.springframework.data.solr.core.SolrTemplate.doQueryForPage<br>org.springframework.data.solr.core.SolrTemplate.querySolr<br>org.springframework.data.solr.core.SolrTemplate.getById|
| Delete    | Repository APIs | org.springframework.data.repository.CrudRepository.deleteAll<br>org.springframework.data.repository.CrudRepository.deleteById<br>org.springframework.data.repository.CrudRepository.delete |
|| Solr Template APIs | org.springframework.data.solr.core.SolrTemplate.delete<br>org.springframework.data.solr.core.SolrTemplate.deleteByIds<br>org.springframework.data.solr.core.SolrTemplate.saveDocument<br>org.springframework.data.solr.core.SolrTemplate.saveDocuments|


## Objects

| Icon               | Description                   |
|:------------------:|-------------------------------|
| ![](665813700.png) | Java ApacheSolr Client        |
| ![](665813699.png) | Java ApacheSolr Index         |
| ![](665813698.png) | Java Unknown ApacheSolr Index |

## Links

| Link type     | Source and destination of link                                           | Methods supported                                                    |
|---------------|--------------------------------------------------------------------------|----------------------------------------------------------------------|
| parentLink    | Between Apache Solr Index Object (Collection → Client )                  | -                                                                    |
| useSelectLink | Between the caller Spring Data Java Method objects and Apache Solr Index | findAll<br>findById<br>findAllById<br>findAll<br>count<br>existsById |
| useDeleteLink | Between the caller Spring Data Java Method objects and Apache Solr Index | deleteAll<br>deleteById<br>delete                                    |
| useInsertLink | Between the caller Spring Data Java Method objects and Apache Solr Index | save<br>saveAll                                                      |

## What results can you expect?

Some example scenarios are shown below:

### ApacheSolr Client


``` java
@SolrDocument(collection="Order")
public class Order {
    
    @Id
    @Indexed(name = "oid", type = "long")
    private Long orderid;
 
    @Indexed(name = "oname", type = "string")
    private String orderName;
    
    @Indexed(name = "odesc", type = "string")
    private String orderDescription;
    
    @Indexed(name = "pname", type = "string")
    private String productName;

    @Indexed(name = "cname", type = "string")
    private String customerName;
    
    @Indexed(name = "cmobile", type = "string")
    private String customerMobile;

	public Long getOrderid() {
		return orderid;
	}

	public void setOrderid(Long orderid) {
		this.orderid = orderid;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerMobile() {
		return customerMobile;
	}

	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}
    
}
```
### Insert Operation

Insert Operation

``` java
	@PutMapping("/order")
	public String updateOrder(@RequestBody Order order){
		String description = "Order Updated";
		solrOrderRepository.save(order);
		return description;
	}
```
![](insert_operation.png)


### Select Operation

Select Operation

``` java
	@GetMapping("/getALL")
	public Iterable<Order> getOrder() {
		return solrOrderRepository.findAll();
	}
```
![](select_operation.png)

``` java
    public ScoredPage<Book> searchDocuments(String searchTerm) {
        Query query = new SimpleQuery(searchTerm);
        return solrTemplate.query("Book", query, Book.class);
    }
```
![](solrTemplate_query.png)

``` java
    @Query("odesc:*?0* OR oname:*?0* OR pname:*?0*")
    Page<Order> findByCustomerQuery(String searchTerm, Pageable pageable);
```
![](query_method.png)
### Delete Operation

Delete Operation

``` java
	@DeleteMapping("/order/{orderid}")
	public String deleteOrder(@PathVariable Long orderid){
		String description = "Order Deleted";
		solrOrderRepository.delete(solrOrderRepository.findByOrderid(orderid));
		return description;
	}
```

![](delete_operation.png)

## Known Limitations

-   If the Index/core name is not resolved in the CRUD API, then a link
    is created with an unknown index object.
