---
title: "Support of Apache Solr for Java"
linkTitle: "Support of Apache Solr for Java"
type: "docs"
---

## Supported Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh">Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a href="https://solr.apache.org/"
rel="nofollow">Apache Solr</a></td>
<td class="confluenceTd">From 1.0 Up to: 9.3</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operations </th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td
class="confluenceTd"><p>org.apache.solr.client.solrj.SolrClient.add</p>
<p>org.apache.solr.client.solrj.SolrClient.addBean</p>
<p>org.apache.solr.client.solrj.SolrServer.add</p>
<p>org.apache.solr.client.solrj.SolrServer.addBean</p>
<p>org.apache.solr.client.solrj.SolrServer.addBeans</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td
class="confluenceTd"><p>org.apache.solr.client.solrj.SolrClient.getById</p>
<p>org.apache.solr.client.solrj.SolrClient.query</p>
<p>org.apache.solr.client.solrj.SolrClient.queryAndStreamResponse</p>
<p>org.apache.solr.client.solrj.SolrServer.query</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td
class="confluenceTd"><p>org.apache.solr.client.solrj.SolrClient.deleteById</p>
<p>org.apache.solr.client.solrj.SolrClient.deleteByQuery</p>
<p>org.apache.solr.client.solrj.SolrServer.deleteById</p>
<p>org.apache.solr.client.solrj.SolrServer.deleteByQuery</p></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813700.png" /></p>
</div></td>
<td class="confluenceTd">Java ApacheSolr Client</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813699.png" /></p>
</div></td>
<td class="confluenceTd">Java ApacheSolr Index</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813698.png" /></p>
</div></td>
<td class="confluenceTd">Java Unknown ApacheSolr Index</td>
</tr>
</tbody>
</table>

## Links

All links are created between the caller Java Method objects and Java
ApachSolr Index objects:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><p>add</p>
<p>addBean</p>
<p>addBeans</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>getById</p>
<p>query</p>
<p>queryAndStreamResponse</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>deleteById</p>
<p>deleteByQuery</p></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### ApacheSolr Client

Solr client cloud

``` java
public class CoreLevelSolrCRUDExample {

    private static final String SOLR_URL = "http://localhost:8983/solr"; // Solr base URL
    private static final String CORE_NAME = "my_core"; // Replace with your core name

    private static final SolrClient solrClient = new HttpSolrClient.Builder(SOLR_URL).build();

    public static void main(String[] args) {
        CoreLevelSolrCRUDExample solrClient = new CoreLevelSolrCRUDExample();

        // Create a document and add it to Solr core
        SolrInputDocument document = new SolrInputDocument();
        document.addField("id", "1");
        document.addField("title", "Sample Document");
        document.addField("content", "This is a sample document content.");

        solrClient.createDocument(document);

        // Read the document by ID
        solrClient.readDocument("1");

        // Update the document's title
        solrClient.updateDocument("1", "Updated Document Title");

        // Read the updated document
        solrClient.readDocument("1");

        // Delete the document by ID
        solrClient.deleteDocument("1");

        // Don't forget to close the SolrClient when done
        solrClient.closeSolrClient();
    }

    public void createDocument(SolrInputDocument document) {
        try {
            UpdateResponse response = solrClient.add(CORE_NAME, document);
            solrClient.commit(CORE_NAME);
            System.out.println("Document created successfully. Response: " + response);
        } catch (IOException e) {
            System.err.println("Error creating document: " + e.getMessage());
        }
    }
```

![](648052796.png)

### Insert Operation

Insert Operation

``` java
 public void createDocument(SolrInputDocument document) {
        try {
            UpdateResponse response = solrClient.add(CORE_NAME, document);
            solrClient.commit(CORE_NAME);
            System.out.println("Document created successfully. Response: " + response);
        } catch (IOException e) {
            System.err.println("Error creating document: " + e.getMessage());
        }
    }
```

![](648052800.png)

### Select Operation

Select Operation

``` java
 public void readDocument(String documentId) {
        try {
            SolrDocument solrDocument = solrClient.getById(CORE_NAME, documentId);
            System.out.println("Read Document: " + solrDocument);
        } catch (IOException e) {
            System.err.println("Error reading document: " + e.getMessage());
        }
    }
```

![](648052799.png)

### Delete Operation

Delete Operation

``` java
public void deleteDocument(String documentId) {
        try {
            UpdateResponse response = solrClient.deleteById(CORE_NAME, documentId);
            solrClient.commit(CORE_NAME);
            System.out.println("Document deleted successfully. Response: " + response);
        } catch (IOException e) {
            System.err.println("Error deleting document: " + e.getMessage());
        }
    }
```

![](648052798.png)

## Known Limitations

-   If the Index/core name is not resolved in the CRUD API, then a link
    is created with an unknown index object.
