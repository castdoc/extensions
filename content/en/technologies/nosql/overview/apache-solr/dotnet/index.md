---
title: "Support of Apache Solr for .NET"
linkTitle: "Support of Apache Solr for .NET"
type: "docs"
---

## Supported Client Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh">Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a href="https://github.com/solrnet/solrnet"
rel="nofollow">SolrNet</a></td>
<td class="confluenceTd" style="text-align: center;">Up to:1.1</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><a
href="https://www.nuget.org/packages/SolrNet.Cloud"
rel="nofollow">SolrNet.Cloud</a></td>
<td class="confluenceTd" style="text-align: center;">Up to:1.1</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operations</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd"><p>SolrNet.ISolrOperations.Add</p>
<p>SolrNet.ISolrOperations.AddAsync</p>
<p>SolrNet.ISolrOperations.AddWithBoost</p>
<p>SolrNet.ISolrOperations.AddWithBoostAsync</p>
<p>SolrNet.ISolrOperations.Extract</p>
<p>SolrNet.ISolrOperations.ExtractAsync</p>
<p>SolrNet.ISolrOperations.AddRange</p>
<p>SolrNet.ISolrOperations.AddRangeAsync</p>
<p>SolrNet.ISolrOperations.AddRangeWithBoost</p>
<p>SolrNet.ISolrOperations.AddRangeWithBoostAsync</p>
<p>SolrNet.ISolrBasicOperations.AddWithBoost</p>
<p>SolrNet.ISolrBasicOperations.AddWithBoostAsync</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>SolrNet.ISolrReadOnlyOperations.Query</p>
<p>SolrNet.ISolrReadOnlyOperations.QueryAsync</p>
<p>SolrNet.ISolrBasicReadOnlyOperations.Query</p>
<p>SolrNet.ISolrBasicReadOnlyOperations.QueryAsync</p>
<p>SolrNet.ISolrBasicReadOnlyOperations.MoreLikeThis</p>
<p>SolrNet.ISolrBasicReadOnlyOperations.MoreLikeThisAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd"><p>SolrNet.ISolrOperations.Delete</p>
<p>SolrNet.ISolrOperations.DeleteAsync</p>
<p>SolrNet.ISolrBasicOperations.Delete</p>
<p>SolrNet.ISolrBasicOperations.DeleteAsync</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>SolrNet.ISolrOperations.AtomicUpdate</p>
<p>SolrNet.ISolrOperations.AtomicUpdateAsync</p>
<p>SolrNet.ISolrBasicOperations.AtomicUpdate</p>
<p>SolrNet.ISolrBasicOperations.AtomicUpdateAsync</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="673415221.png" draggable="false"
data-image-src="673415221.png"
data-unresolved-comment-count="0" data-linked-resource-id="673415221"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="648052801"
data-linked-resource-container-version="1" height="34" /></p>
</div></td>
<td class="confluenceTd">DotNet SolrNet Index</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="673415220.png" draggable="false"
data-image-src="673415220.png"
data-unresolved-comment-count="0" data-linked-resource-id="673415220"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="648052801"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown SolrNet Index</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="673415219.png" draggable="false"
data-image-src="673415219.png"
data-unresolved-comment-count="0" data-linked-resource-id="673415219"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="client.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="648052801"
data-linked-resource-container-version="1" height="33" /></p>
</div></td>
<td class="confluenceTd">DotNet SolrNet Client</td>
</tr>
</tbody>
</table>

## Links

All links are created between the caller .NET Method objects and DotNet
SolrNet Index objects:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><strong>Link type</strong></th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>Query</p>
<p>QueryAsync</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>AtomicUpdate</p>
<p>AtomicUpdateAsync</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><p>Add</p>
<p>AddAsync</p>
<p>AddWithBoost</p>
<p>AddWithBoostAsync</p>
<p>Extract</p>
<p>ExtractAsync</p>
<p>AddRange</p>
<p>AddRangeAsync</p>
<p>AddRangeWithBoost</p>
<p>AddRangeWithBoostAsy</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>Delete</p>
<p>DeleteAsync</p></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### DotNet SolrNet Client

``` c#
using CommonServiceLocator;
using SolrNet;

namespace SOlrStandaloneApp
{
    public class Tests
    {
        public void Setup()
        {
            Startup.Init<Product>("http://localhost:8983/solr/mycore");
        }
    }
}
```

![](648052806.png)

### Select Operation

``` c#
using CommonServiceLocator;
using SolrNet;

namespace SOlrStandaloneApp
{
    public class Tests
    {    
        [Test]
        public void Query()
        {
            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<Product>>();
            var results = solr.Query(new SolrQueryByField("id", "SP2514N"));
            Assert.AreEqual(1, results.Count);
            Console.WriteLine(results[0].Price);
        }
        
    }
}
```

![](648052805.png)

### Insert Operation

``` c#
using CommonServiceLocator;
using SolrNet;

namespace SOlrStandaloneApp
{
    public class Tests
    {
        [Test]
        public void Add()
        {
            var p = new Product
            {
                Id = "SP2514N",
                Manufacturer = "Samsung Electronics Co. Ltd.",
                Categories = new[] {
            "electronics",
            "hard drive",
        },
                Price = 92,
                InStock = true,
            };

            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<Product>>();
            solr.Add(p);
            solr.Commit();
        }

    }
}
```

![](648052804.png)

### Update Operation

``` c#
using CommonServiceLocator;
using SolrNet;

namespace SOlrStandaloneApp
{
    [Test]
        public void AtomicUpdate()
        {
            var p = new Product
            {
                Id = "SP2514N",
                Manufacturer = "Samsung Electronics Co. Ltd.",
                Categories = new[] {
            "electronics",
            "hard drive",
        },
                Price = 92,
                InStock = true,
            };

            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<Product>>();
            solr.AtomicUpdate(p,null);
            solr.Commit();
        }

    }
}
```

![](648052803.png)

### Delete Operation

``` c#
using CommonServiceLocator;
using SolrNet;

namespace SOlrStandaloneApp
{
        [Test]
        public void Remove()
        {
            var p = new Product
            {
                Id = "SP2514N",
                Manufacturer = "Samsung Electronics Co. Ltd.",
                Categories = new[] {
            "electronics",
            "hard drive",
        },
                Price = 92,
                InStock = true,
            };

            var solr = ServiceLocator.Current.GetInstance<ISolrOperations<Product>>();
            solr.Delete(p);
            solr.Commit();
        }
}
```

![](648052802.png)

## Known Limitations

-   Startup.Init and Startup.InitAsync are only API currently supported
    for server connection of SolrNet and SolrNet Cloud.