---
title: "Support of DynamoDB for Spring Data"
linkTitle: "Support of DynamoDB for Spring Data"
type: "docs"
---

CAST supports DynamoDB via its [com.castsoftware.nosqljava](../../../extensions/com.castsoftware.nosqljava/) extension. Details about the support provided for Java with Spring Data source code is explained below.

## Supported Client Libraries

| Library | Version | Supported|
|---|---|:-:|
| [spring-data-dynamodb](https://github.com/derjust/spring-data-dynamodb/tree/master) | Up to: 5.1.x | :white_check_mark: |


## Supported Operations

| Operations | Scenario | Method Supported |
|---|---|---|
| Insert | Spring Framework | org.springframework.data.repository.CrudRepository.save<br>org.springframework.data.repository.CrudRepository.saveAll |
| Insert | Social SignIn | org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.save<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.saveAll<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.save<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.saveAll<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.save<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.batchSave<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.load<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.query<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.scan<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.count<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.batchLoad |
| Select | Spring Framework | org.springframework.data.repository.CrudRepository.existsById<br>org.springframework.data.repository.CrudRepository.findAll<br>org.springframework.data.repository.CrudRepository.findById<br>org.springframework.data.repository.PagingAndSortingRepository.findAll |
| Select | Social SignIn | org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.existsById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.findAll<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.findById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.existsById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.findById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.findAll |
| Delete | Spring Framework | org.springframework.data.repository.CrudRepository.delete<br>org.springframework.data.repository.CrudRepository.deleteById<br>org.springframework.data.repository.CrudRepository.deleteAllById<br>org.springframework.data.repository.CrudRepository.deleteAll |
| Delete | Social SignIn | org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.delete<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.deleteById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.deleteAllById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository.deleteAll<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.delete<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.deleteById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.deleteAllById<br>org.socialsignin.spring.data.dynamodb.repository.DynamoDBPagingAndSortingRepository.deleteAll<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.delete<br>org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate.batchDelete |

## Objects

| Icon               | Description                  |
| :----------------: | ---------------------------- |
| ![](372679240.png) | Java_DynamoDB_Client         |
| ![](372679239.png) | Java_DynamoDB_Table          |
| ![](372679238.png) | Java_Unknown_DynamoDB_Client |
| ![](372679237.png) | Java_Unknown_DynamoDB_Table  |

## Links

| Link type     | Source and destination of link                             | Methods Supported                                  |
|---------------|------------------------------------------------------------|----------------------------------------------------|
| parentLink    | Between DynamoDB client object and DynamoDB table          | -                                                  |
| useInsertLink | Between the caller Java Method objects and DynamoDB client | save<br>saveAll                                    |
| useSelectLink | Between the caller Java Method objects and DynamoDB client | existsById<br>findById<br>findAll                  |
| useDeleteLink | Between the caller Java Method objects and DynamoDB client | delete<br>deleteById<br>deleteAll<br>deleteAllById |

## What results can you expect?

Some example scenarios are shown below:

### DynamoDB Client with Java Configuration

``` java
@Configuration
@EnableDynamoDBRepositories(basePackages = "com.javasampleapproach.dynamodb.repo")
public class DynamoDBConfig {

    @Value("${amazon.dynamodb.endpoint}")
    private String dBEndpoint;

    @Value("${amazon.aws.accesskey}")
    private String accessKey;

    @Value("${amazon.aws.secretkey}")
    private String secretKey;

    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        AmazonDynamoDB dynamoDB = new AmazonDynamoDBClient(amazonAWSCredentials());

        if (!StringUtils.isNullOrEmpty(dBEndpoint)) {
            dynamoDB.setEndpoint(dBEndpoint);
        }

        return dynamoDB;
    }

    @Bean
    public AWSCredentials amazonAWSCredentials() {
        return new BasicAWSCredentials(accessKey, secretKey);
    }
}
```

### Select Operation

``` java
public String findAll() {
        String result = "";
        Iterable<Customer> customers = repository.findAll();

        for (Customer cust : customers) {
            result += cust.toString() + "<br>";
        }

        return result;
    }
```

![](528252977.png)

### Insert Operation

``` java
public String save() {
        // save a single Customer
        repository.save(new Customer("JSA-1", "Jack", "Smith"));

}
```

![](625705341.png)

### Delete Operation

``` java
public String delete() {
        repository.delete(new Customer("JSA-1", "Jack", "Smith"));
        repository.deleteAll();
        return "Done";
    }
```

![](625705340.png)

## Known Limitations

-   Client is created as unknown, if the name is not retrieved from the
    properties file or if the name is not be resolved.
