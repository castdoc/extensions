---
title: "Support of DynamoDB for Node.js - TypeScript"
linkTitle: "Support of DynamoDB for Node.js - TypeScript"
type: "docs"
---

>CAST supports DynamoDB via its
[com.castsoftware.typescript](https://extend.castsoftware.com/#/extension?id=com.castsoftware.typescript&version=latest) extension.
Details about how this support is provided is discussed below.

## Supported APIs

Only the SDK V2 is currently supported.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370197.png" draggable="false"
data-image-src="666370197.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370197"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="533069868"
data-linked-resource-container-version="3" height="32" /></p>
</div></td>
<td class="confluenceTd">Node.js DynamoDB Endpoint</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370196.png" draggable="false"
data-image-src="666370196.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370196"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="533069868"
data-linked-resource-container-version="3" height="34" /></p>
</div></td>
<td class="confluenceTd">Node.js DynamoDB Table</td>
</tr>
</tbody>
</table>

## Links

### API: DynamoDB (SDK V2 or SDK V3)

``` js
//SDKV2
import AWS from "aws-sdk";
const dynamodb = new AWS.DynamoDB();
```

or

``` js
//SDKV3
import * as AWS from "@aws-sdk/client-dynamodb";
const dynamodb = new AWS.DynamoDB();
```

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">No Link</td>
<td class="confluenceTd"><p>createGlobalTable</p>
<p>createTable</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>createBackup</p>
<p>getItem</p>
<p>batchGetItem</p>
<p>transactWriteItems</p>
<p>batchWriteItem</p>
<p>restoreTableToPointInTime</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>deleteTable</p>
<p>deleteItem</p>
<p>transactWriteItems</p>
<p>batchWriteItem</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>transactWriteItems</p>
<p>batchWriteItem</p>
<p>updateItem</p>
<p>createBackup</p>
<p>updateTable</p>
<p>putItem</p>
<p>restoreTableToPointInTime</p>
<p>restoreTableFromBackup</p></td>
</tr>
</tbody>
</table>

### API: Commands from SDK V3

``` java
imported from '@aws-sdk/client-dynamodb'
```

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">No Link</td>
<td class="confluenceTd"><p>CreateGlobalTableCommand</p>
<p>CreateTableCommand</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>CreateBackupCommand</p>
<p>GetItemCommand</p>
<p>BatchGetItemCommand</p>
<p>TransactGetItemsCommand</p>
<p>BatchGetItemCommand</p>
<p>BatchWriteItemCommand</p>
<p>TransactWriteItemsCommand</p>
<p>RestoreTableToPointInTimeCommand</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>DeleteItemCommand</p>
<p>DeleteTableCommand</p>
<p>TransactWriteItemsCommand</p>
<p>BatchWriteItemCommand</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>TransactWriteItemsCommand</p>
<p>BatchWriteItemCommand</p>
<p>UpdateItemCommand</p>
<p>CreateBackupCommand</p>
<p>UpdateTableCommand</p>
<p>PutItemCommand</p>
<p>RestoreTableFromBackupCommand</p></td>
</tr>
</tbody>
</table>

### API: DocumentClient

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>batchGet</p>
<p>transactGet</p>
<p>get</p>
<p>scan</p>
<p>query</p>
<p>batchWrite</p>
<p>transactWrite</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>batchWrite</p>
<p>transactWrite</p>
<p>delete</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>put</p>
<p>update</p>
<p>batchWrite</p>
<p>transactWrite</p></td>
</tr>
</tbody>
</table>

## What results can you expect?

### Code samples

These declarations will establish a connection to the database located
on localhost:

``` js
import AWS from "aws-sdk";

AWS.config.update({
  region: "us-west-2",
  endpoint: "http://localhost:8000"
});

var dynamodb = new AWS.DynamoDB();
```

These declarations will create a useUpdateLink from code to the table
"Music".

``` js
/* This example adds a new item to the Music table. */

var params = {
  Item: {
    "AlbumTitle": {
      S: "Somewhat Famous"
    }, 
    "Artist": {
      S: "No One You Know"
    }, 
    "SongTitle": {
      S: "Call Me Today"
    }
  }, 
  ReturnConsumedCapacity: "TOTAL", 
  TableName: "Music"
};
dynamodb.putItem(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});
```
