---
title: "Support of DynamoDB for Node.js - JavaScript"
linkTitle: "Support of DynamoDB for Node.js - JavaScript"
type: "docs"
---

>CAST supports DynamoDB via its
[com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest) extension.
Details about how this support is provided for Node.js JavaScript source
code is discussed below.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813942.png" draggable="false"
data-image-src="665813942.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813942"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813938"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Node.js DynamoDB Endpoint</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813941.png" draggable="false"
data-image-src="665813941.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813941"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813938"
data-linked-resource-container-version="1" height="34" /></p>
</div></td>
<td class="confluenceTd">Node.jsDynamoDB Table</td>
</tr>
</tbody>
</table>

## Links

### DynamoDB API

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function api v2</th>
<th class="confluenceTh"><p>Commands from SDK V3</p>
<p>imported from '@aws-sdk/client-dynamodb'</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">No Link</td>
<td class="confluenceTd"><p>createGlobalTable</p>
<p>createTable</p></td>
<td class="confluenceTd"><p>-</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>createBackup</p>
<p>getItem</p>
<p>batchGetItem</p>
<p>transactWriteItems</p>
<p>batchWriteItem</p>
<p>restoreTableToPointInTime</p></td>
<td class="confluenceTd"><p>CreateTableCommand</p>
<p>BatchGetItemCommand</p>
<p>GetItemCommand</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>deleteTable</p>
<p>deleteItem</p>
<p>transactWriteItems</p>
<p>batchWriteItem</p></td>
<td class="confluenceTd"><p>DeleteTableCommand<br />
DeleteItemCommand</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>transactWriteItems</p>
<p>batchWriteItem</p>
<p>updateItem</p>
<p>updateTable</p>
<p>putItem</p>
<p>restoreTableToPointInTime</p>
<p>restoreTableFromBackup</p></td>
<td class="confluenceTd"><p>PutItemCommand<br />
UpdateItemCommand<br />
UpdateCommand</p></td>
</tr>
</tbody>
</table>

### DocumentClient

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function api v2</th>
<th class="confluenceTh"><p>Commands from SDK V3</p>
<p>imported from '@aws-sdk/client-dynamodb'</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><ul>
<li>batchGet</li>
<li>transactGet</li>
<li>get</li>
<li>scan</li>
<li>query</li>
<li>batchWrite</li>
<li>transactWrite</li>
</ul></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><ul>
<li>batchWrite</li>
<li>transactWrite</li>
<li>delete</li>
</ul></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><ul>
<li>put</li>
<li>update</li>
<li>batchWrite</li>
<li>transactWrite</li>
</ul></td>
<td class="confluenceTd">-</td>
</tr>
</tbody>
</table>

## Code samples

### API v2 sample

These declaration will establish a connection to the database located on
localhost:

``` js
var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-west-2",
  endpoint: "http://localhost:8000"
});

var dynamodb = new AWS.DynamoDB();
```

These declarations will create a *useUpdateLink* from code to the
database "myDatabase":

``` js
/* This example adds a new item to the Music table. */

var params = {
  Item: {
    "AlbumTitle": {
      S: "Somewhat Famous"
    }, 
    "Artist": {
      S: "No One You Know"
    }, 
    "SongTitle": {
      S: "Call Me Today"
    }
  }, 
  ReturnConsumedCapacity: "TOTAL", 
  TableName: "Music"
};
dynamodb.putItem(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});
```

## What results can you expect?

![](665813940.png)

### Api v3 sample

These declaration swill establish a connection to the database located
on localhost, create two tables, and create links:

``` js
const {DynamoDBClient} = require("@aws-sdk/client-dynamodb");
const REGION = "us-east-1";
const ddbClient = new DynamoDBClient({region: REGION,
                                      endpoint: "http://localhost:8000" });
export { ddbClient };

const {CreateTableCommand} = require("@aws-sdk/client-dynamodb");
const {PutItemCommand } = require("@aws-sdk/client-dynamodb");
const {DeleteTableCommand } = require("@aws-sdk/client-dynamodb");

// Set the parameters
export const params_put = {
  TableName: "TABLE_NAME_2",
  Item: {
    CUSTOMER_ID: { N: "001" },
    CUSTOMER_NAME: { S: "Richard Roe" },
  },
};

export const create = async () => {
  try {
    const data = await ddbClient.send(new CreateTableCommand({TableName: "TABLE_NAME"}));
    console.log("Table Created", data);
    return data;
  } catch (err) {
    console.log("Error", err);
  }
};
create();

export const put_item = async () => {
  try {
    const data = await ddbClient.send(new PutItemCommand(params_put));
    console.log("Table Updated", data);
    return data;
  } catch (err) {
    console.log("Error", err);
  }
};
put_item();

export const delete_item = async () => {
  try {
    const data = await ddbClient.send(new DeleteTableCommand({TableName: "TABLE_NAME"}));
    console.log("Success, table deleted", data);
    return data;
  } catch (err) {
      console.log("Error", err);
    }
};
delete_item();
```

![](665813939.png)