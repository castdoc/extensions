---
title: "Support of DynamoDB for Java"
linkTitle: "Support of DynamoDB for Java"
type: "docs"
---
>CAST supports DynamoDB via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about how this support is provided for Java source
code is discussed below.

## Supported Client Libraries

| Library | Supported |
|---|:-:|
| [AWS SDK for Java API - v1](https://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/) | :white_check_mark: |
| [AWS SDK for Java API - v2](https://sdk.amazonaws.com/java/api/latest/) | :white_check_mark: |

## Supported Operations

<table>
<colgroup>
<col/>
<col/>
<col/>
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Operations</th>
<th class="confluenceTh">Scenario</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">AWS SDK JAVA v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111565" class="expand-container">
<details><summary>com.amazonaws.services.dynamodbv2</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.putItem</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.putItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.putItemAsync</li>
<li>com.amazonaws.services.dynamodbv2.document.DynamoDB.createTable</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.createTableAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.createTable</li>
<li>com.amazonaws.services.dynamodbv2.util.TableUtils.createTableIfNotExists</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.batchWriteItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.batchWriteItemAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.transactWriteItems</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.transactWriteItemsAsync</li>
</ul>
</div>
</details></div>

</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">DDB Mapper v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111566" class="expand-container">
<details><summary>com.amazonaws.services.dynamodbv2.datamodeling</summary>
<div id="expander-content-1998111566" class="expand-content">
<ul>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.save</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.save</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.batchSave</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.batchSave</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.batchWrite</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.batchWrite</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.transactionWrite</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.TransactionWriteRequest.addPut</li>
</ul>
</div>
</details></div>

</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">AWS SDK JAVA v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.putItem</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.putItem</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.createTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.createTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.batchWriteItem</li>
<li>software.amazon.awssdk.services.dynamodb.waiters.DynamoDbWaiter.createTable</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">Enhanced v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.putItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.putItemWithResponse</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.putItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.putItemWithResponse</li>
<li>software.amazon.awssdk.enhanced.dynamodb.model.TransactWriteItemsEnhancedRequest.Builder.addPutItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.model.BatchWriteItemEnhancedRequest.Builder.writeBatches</li>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">AWS SDK JAVA v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>com.amazonaws.services.dynamodbv2.document.Table.updateTable</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.upateItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.updateItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.updateItemAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.updateTable</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.updateTableAsync</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">DDB Mapper v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>com.amazonaws.services.dynamodbv2.datamodeling.TransactionWriteRequest.addUpdate</li>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">AWS SDK JAVA v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.updateTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.updateTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.updateItem</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.updateItem</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">Enhanced v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.updateItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.updateItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.updateItemWithResponse</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.updateItemWithResponse</li>
<li>software.amazon.awssdk.enhanced.dynamodb.model.TransactWriteItemsEnhancedRequest.Builder.addUpdateItem</li>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">AWS SDK JAVA v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111565" class="expand-container">
<details><summary>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.queryAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.scanAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.getItemAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.describeTableAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.batchGetItemAsync</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.transactGetItemsAsync</li>
</ul>
</div>
</details></div>
<div id="expander-1998111565" class="expand-container">
<details><summary>com.amazonaws.services.dynamodbv2.AmazonDynamoDB</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.query</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.scan</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.getItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.describeTable</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.batchGetItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.transactGetItems</li>
</ul>
</div>
</details></div>
<div id="expander-1998111565" class="expand-container">
<details><summary>com.amazonaws.services.dynamodbv2.document.Table</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>com.amazonaws.services.dynamodbv2.document.Table.query</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.scan</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.describe</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.getTableName</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.getItemOutcome</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.getItem</li>
</ul>
</div>
</details></div>
<ul>
<li>com.amazonaws.services.dynamodbv2.document.DynamoDB.getTable</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">DDB Mapper v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111565" class="expand-container">
<details><summary>com.amazonaws.services.dynamodbv2.datamodeling</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.load</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.load</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.TransactionLoadRequest.addLoad</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.scan</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.scan</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.query</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.query</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.count</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.count</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.scanPage</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.ScanPage</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.queryPage</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.queryPage</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.parallelScan</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.parallelScan</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.getTableModel</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.getTableModel</li>
</ul>
</div>
</details></div>

</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">AWS SDK JAVA v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.describeTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.describeTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.getItem</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.getItem</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.query</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.query</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.scan</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.scan</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">Enhanced v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient.table</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedAsyncClient.table</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.getItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.getItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.query</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.query</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.scan</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.scan</li>
<li>software.amazon.awssdk.enhanced.dynamodb.model.BatchGetItemEnhancedRequest.Builder.readBatches</li>
<li>software.amazon.awssdk.enhanced.dynamodb.model.TransactGetItemsEnhancedRequest.Builder.addGetItem</li>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">AWS SDK JAVA v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>com.amazonaws.services.dynamodbv2.document.Table.deleteItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.deleteItem</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.deletItemAsync</li>
<li>com.amazonaws.services.dynamodbv2.document.Table.delete</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDB.deleteTable</li>
<li>com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync.deleteTableAsync</li>
<li>com.amazonaws.services.dynamodbv2.util.TableUtils.deleteTableIfExists</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">DDB Mapper v1</td>
<td class="confluenceTd"><div class="content-wrapper">
<details><summary>com.amazonaws.services.dynamodbv2.datamodeling</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.delete</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.delete</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.AbstractDynamoDBMapper.batchDelete</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.batchDelete</li>
<li>com.amazonaws.services.dynamodbv2.datamodeling.TransactionWriteRequest.addDelete</li>
</ul>
</div>
</details></div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">AWS SDK JAVA v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.deleteTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.deleteTable</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbClient.deleteItem</li>
<li>software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient.deleteItem</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">Enhanced v2</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.deleteItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.deleteItem</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable.deleteItemWithResponse</li>
<li>software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable.deleteItemWithResponse</li>
<li>software.amazon.awssdk.enhanced.dynamodb.model.TransactWriteItemsEnhancedRequest.Builder.addDeleteItem</li>
</div></td>
</tr>
</tbody>
</table>

Note: batchWriteItems, transactWriteItems may result in useInsertLink,
useUpdateLink or useDeleteLink depnding on the usage.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="665813922.png" draggable="false"
data-image-src="665813922.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813922"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813903"
data-linked-resource-container-version="1" /></li>
</div></td>
<td class="confluenceTd">Java AWS DynamoDB Client</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="665813921.png" class="image-center"
draggable="false" data-image-src="665813921.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813921"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813903"
data-linked-resource-container-version="1" /></li>
</div></td>
<td class="confluenceTd">Java AWS DynamoDB Table</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="665813920.png" class="image-center"
draggable="false" data-image-src="665813920.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813920"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813903"
data-linked-resource-container-version="1" /></li>
</div></td>
<td class="confluenceTd">Java AWS Unknown DynamoDB Client</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="665813919.png" class="image-center"
draggable="false" data-image-src="665813919.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813919"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813903"
data-linked-resource-container-version="1" /></li>
</div></td>
<td class="confluenceTd">Java AWS Unknown DynamoDB Table</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Link type</th>
<th class="confluenceTh" style="text-align: left;">Source and
destination of link</th>
<th class="confluenceTh">Methods Supported in AWS SDK Java v1</th>
<th class="confluenceTh">Methods Supported in AWS SDK Java v1 DynamoDB
Mapper</th>
<th class="confluenceTh">Methods Supported in AWS SDK Java v2</th>
<th class="confluenceTh">Methods Supported in v2 Enhanced</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">belongsTo</td>
<td class="confluenceTd" style="text-align: left;"><li>From Java
AWS DynamoDB Table object to Java AWS DynamoDB Client object</li></td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd" style="text-align: left;">-</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java Method objects and Java AWS DynamoDB table object</td>
<td class="confluenceTd"><li>createTable</li>
<li>createTableAsync</li></td>
<td class="confluenceTd">-</td>
<td class="confluenceTd"><li>createTable</li></td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useInsertLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java Method objects and Java AWS DynamoDB table object</td>
<td class="confluenceTd"><li>putItem</li>
<li>putItemAsync</li></td>
<td class="confluenceTd"><li>save</li>
<li>batchSave</li></td>
<td class="confluenceTd"><li>putItem</li></td>
<td class="confluenceTd"><li>putItem</li>
<li>putItemWithResponse</li></td>
<td class="confluenceTd">batchWriteItems, transactWriteItems may result
in useInsertLink, useUpdateLink or useDeleteLink depnding on the
usage.</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useUpdateLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java Method objects and Java AWS DynamoDB table object</td>
<td class="confluenceTd"><li>updateTable</li>
<li>updateItem</li>
<li>updateTableAsync</li>
<li>updateItemAsync</li></td>
<td class="confluenceTd">-</td>
<td class="confluenceTd"><li>updateTable</li>
<li>updateItem</li></td>
<td class="confluenceTd"><li>updateItem</li>
<li>updateItemWithReponse</li></td>
<td class="confluenceTd">As above</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><li>useSelectLink</li></td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java Method objects and Java AWS DynamoDB table object</td>
<td class="confluenceTd" style="text-align: left;"><li>query</li>
<li>queryAsync</li>
<li>describeTable</li>
<li>describeTableAsync</li>
<li>getItem</li>
<li>getItemAsync</li>
<li>batchGetItem</li>
<li>batchGetItemAsync</li>
<li>transactGetItems</li>
<li>transactGetItemsAsync</li></td>
<td class="confluenceTd"><li>load</li>
<li>query</li>
<li>count</li>
<li>scan</li>
<li>scanPage</li>
<li>queryPage</li>
<li>parallelScan</li>
<li>getTableModel</li>
<li>batchLoad</li>
<li>transasctionLoad</li></td>
<td class="confluenceTd"><li>describeTable</li>
<li>getItem</li>
<li>query</li>
<li>scan</li></td>
<td class="confluenceTd"><li>getItem</li>
<li>query</li>
<li>scan</li>
<li>readBatches</li>
<li>batchGetItem</li>
<li>transactGetItems</li></td>
<td class="confluenceTd">As above</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useDeleteLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java Method objects and Java AWS DynamoDB table object</td>
<td class="confluenceTd" style="text-align: left;"><li>DeleteTable</li>
<li>DeleteItem</li></td>
<td class="confluenceTd"><li>delete</li>
<li>batchDelete</li></td>
<td class="confluenceTd"><li>deleteTable</li>
<li>deleteItem</li></td>
<td class="confluenceTd"><li>deleteItem</li>
<li>deleteItemWithResponse</li></td>
<td class="confluenceTd">As above</td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### AWS SDK JAVA v1

#### DynamoDB Client Configuration

``` java
public static void main(String[] args)
    {
        final String USAGE = "\n" +
            "Usage:\n" +
            "    CreateTable <table>\n\n" +
            "Where:\n" +
            "    table - the table to create.\n\n" +
            "Example:\n" +
            "    CreateTable HelloTable\n";

        if (args.length < 1) {
            System.out.println(USAGE);
            System.exit(1);
        }

        /* Read the name from command args */
        String table_name = "Customer Table";

        System.out.format(
            "Creating table \"%s\" with a simple primary key: \"Name\".\n",
            table_name);

        CreateTableRequest request = new CreateTableRequest()
            .withAttributeDefinitions(new AttributeDefinition(
                     "Name", ScalarAttributeType.S))
            .withKeySchema(new KeySchemaElement("Name", KeyType.HASH))
            .withProvisionedThroughput(new ProvisionedThroughput(
                     new Long(10), new Long(10)))
            .withTableName(table_name);

        final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();

        try {
            CreateTableResult result = ddb.createTable(request);
            System.out.println(result.getTableDescription().getTableName());
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
        System.out.println("Done!");
    }
```

![](665813918.png)

#### Insert Operation

``` java
public static void main(String args[]) throws InterruptedException {

        AmazonDynamoDB dynamoDBClient = AmazonDynamoDBClientBuilder
            .standard()
            .withRegion(Regions.US_EAST_2)
            .withCredentials(new DefaultAWSCredentialsProviderChain())
            .build();
        int maxItemCount = 100;
        for (Integer i = 1; i <= maxItemCount; i++) {
            System.out.println("Processing item " + i + " of " + maxItemCount);

            // Write a new item
            Map<String, AttributeValue> item = new HashMap<>();
            item.put("Id", new AttributeValue().withN(i.toString()));
            item.put("Message", new AttributeValue().withS("New item!"));
            dynamoDBClient.putItem(tableName, item);
    }
}
```

![](665813917.png)

#### Delete Operation

``` java
public static void TestDeleteTable() {
        String table_name = "Products2";

        DeleteTableRequest request = new DeleteTableRequest().withTableName(table_name);

        final AmazonDynamoDB ddb2 = AmazonDynamoDBClientBuilder.defaultClient();

        try {
            ddb2.deleteTable(request);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
        System.out.println("Done!");

    }
```

![](665813916.png)

#### Update Operation

``` java
 public static void main(String args[]) throws InterruptedException {

        AmazonDynamoDB dynamoDBClient = AmazonDynamoDBClientBuilder
            .standard()
            .withRegion(Regions.US_EAST_2)
            .withCredentials(new DefaultAWSCredentialsProviderChain())
            .build();
    
         Map<String, AttributeValue> key = new HashMap<>();
            key.put("Id", new AttributeValue().withN(i.toString()));
            Map<String, AttributeValueUpdate> attributeUpdates = new HashMap<>();
            attributeUpdates.put("Message", new AttributeValueUpdate()
                .withAction(AttributeAction.PUT)
                .withValue(new AttributeValue()
                    .withS("This item has changed")));
            dynamoDBClient.updateItem(tableName, key, attributeUpdates);
```

![](665813915.png)

#### Select Operation

``` java
 public static void main(String args[]) throws InterruptedException {

        AmazonDynamoDB dynamoDBClient = AmazonDynamoDBClientBuilder
            .standard()
            .withRegion(Regions.US_EAST_2)
            .withCredentials(new DefaultAWSCredentialsProviderChain())
            .build();
        String tableName = "TestTableForStreams";
        // Print the stream settings for the table
        DescribeTableResult describeTableResult = dynamoDBClient.describeTable(tableName);
        String streamArn = describeTableResult.getTable().getLatestStreamArn();
        System.out.println("Current stream ARN for " + tableName + ": " +
            describeTableResult.getTable().getLatestStreamArn());
```

![](665813914.png)

### Batch Operations

#### BatchWriteRequest

``` java
Map<String, AttributeValue> book_item1 = new HashMap<String, AttributeValue>();

WriteRequest req1= new WriteRequest().withDeleteRequest(new DeleteRequest(book_item1));
WriteRequest req2= new WriteRequest().withPutRequest(new PutRequest(book_item2));

List<WriteRequest> requests1=new ArrayList<WriteRequest>();
requests1.add(req1);
requests1.add(req2);

List<WriteRequest> requests2=new ArrayList<WriteRequest>();
requests2.add(new WriteRequest().withDeleteRequest(new DeleteRequest(book_item3)));

Map<String, List<WriteRequest>> requestItems = new HashMap<String, List<WriteRequest>>() ;
requestItems.put("table1",requests1);
requestItems.put("table2",requests2);
BatchWriteItemRequest batchWriteItemRequest1=new BatchWriteItemRequest().withRequestItems(requestItems);
ddb1.batchWriteItem(batchWriteItemRequest1) ;   
```

#### BatchGetItemRequest

``` java
KeysAndAttributes item1 =  null;
KeysAndAttributes item2 =  null;
        
Map<String, KeysAndAttributes> requestItems = new HashMap<String, KeysAndAttributes>();
requestItems.put("table1",item1);
requestItems.put("table2",item2);
        
        
BatchGetItemRequest batchGetItemRequest1 = new BatchGetItemRequest().withRequestItems(requestItems);
ddb2.batchGetItem(batchGetItemRequest1) ;   
```

#### TransactWriteItemsRequest

``` java
Put putItem1= new Put().withTableName("table1").withItem(book_item1);
Delete deleteItem1= new Delete().withTableName("table1").withKey(book_item2);
Update updateItem1=new Update().withTableName("table2").withKey(book_item3);
        
TransactWriteItem transactItem1=new TransactWriteItem().withPut(putItem1);
TransactWriteItem transactItem2=new TransactWriteItem().withDelete(deleteItem1);
TransactWriteItem transactItem3=new TransactWriteItem().withUpdate(updateItem1);
  
TransactWriteItemsRequest  transactWriteItemsRequest= new TransactWriteItemsRequest().withTransactItems(transactItem1,transactItem2,transactItem3);
            
ddb3.transactWriteItems(transactWriteItemsRequest);
```

#### TransactGetItemsRequest

``` java
Get getItem1= new Get().withTableName("table1").withKey(book_item1);
Get getItem2=new Get().withTableName("table2").withKey(book_item2);
        
TransactGetItem transactItem1=new TransactGetItem().withGet(getItem1);
TransactGetItem transactItem2=new TransactGetItem().withGet(getItem2);
  
TransactGetItemsRequest  transactGetItemsRequest= new TransactGetItemsRequest().withTransactItems(transactItem1,transactItem2);
        
ddb4.transactGetItems(transactGetItemsRequest);
```

### AWS SDK JAVA v1 DynamoDB Mapper

#### Insert Operation

``` java
public void likeMysfit(String mysfitId) {

        Mysfit mysfitToUpdate = mapper.load(Mysfit.class, mysfitId);
        Integer likes = mysfitToUpdate.getLikes() + 1;
        mysfitToUpdate.setLikes(likes);
        mapper.save(mysfitToUpdate);

    }
```

![](665813913.png)

#### Select Operation

``` java
public Mysfits queryMysfits(String filter, String value) {

        HashMap<String, AttributeValue> attribValue = new HashMap<String, AttributeValue>();
        attribValue.put(":"+value,  new AttributeValue().withS(value));

        DynamoDBQueryExpression<Mysfit> queryExpression = new DynamoDBQueryExpression<Mysfit>()
                .withIndexName(filter+"Index")
                .withKeyConditionExpression(filter + "= :" + value)
                .withExpressionAttributeValues(attribValue)
                .withConsistentRead(false);

        List<Mysfit> mysfits = mapper.query(Mysfit.class, queryExpression);
        Mysfits allMysfits = new Mysfits(mysfits);

        return allMysfits;
    }
```

![](665813912.png)

### AWS SDK JAVA v2 

#### Insert Operation

``` java
public static void putItemInTable(DynamoDbClient ddb,
                                      String tableName,
                                      String key,
                                      String keyVal,
                                      String albumTitle,
                                      String albumTitleValue,
                                      String awards,
                                      String awardVal,
                                      String songTitle,
                                      String songTitleVal){

        HashMap<String,AttributeValue> itemValues = new HashMap<String,AttributeValue>();

        // Add all content to the table
        itemValues.put(key, AttributeValue.builder().s(keyVal).build());
        itemValues.put(songTitle, AttributeValue.builder().s(songTitleVal).build());
        itemValues.put(albumTitle, AttributeValue.builder().s(albumTitleValue).build());
        itemValues.put(awards, AttributeValue.builder().s(awardVal).build());

        PutItemRequest request = PutItemRequest.builder()
                .tableName(tableName)
                .item(itemValues)
                .build();

        try {
            ddb.putItem(request);
            System.out.println(tableName +" was successfully updated");

        } catch (ResourceNotFoundException e) {
            System.err.format("Error: The Amazon DynamoDB table \"%s\" can't be found.\n", tableName);
            System.err.println("Be sure that it exists and that you've typed its name correctly!");
            System.exit(1);
}
```

![](665813911.png)

#### Delete Operation

``` java
public static void deleteDynamoDBTable(DynamoDbClient ddb, String tableName) {

        DeleteTableRequest request = DeleteTableRequest.builder()
                .tableName(tableName)
                .build();

        try {
            ddb.deleteTable(request);

        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        System.out.println(tableName +" was successfully deleted!");
    }
```

![](665813910.png)

#### Update Operation

``` java
public static void updateDynamoDBTable(DynamoDbClient ddb,
                                           String tableName,
                                           Long readCapacity,
                                           Long writeCapacity) {

        System.out.format(
                "Updating %s with new provisioned throughput values\n",
                tableName);
        System.out.format("Read capacity : %d\n", readCapacity);
        System.out.format("Write capacity : %d\n", writeCapacity);

        ProvisionedThroughput tableThroughput = ProvisionedThroughput.builder()
                .readCapacityUnits(readCapacity)
                .writeCapacityUnits(writeCapacity)
                .build();

        UpdateTableRequest request = UpdateTableRequest.builder()
                .provisionedThroughput(tableThroughput)
                .tableName(tableName)
                .build();

        try {
            ddb.updateTable(request);
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        System.out.println("Done!");
    }
```

``` java
public static void updateDynamoDBTable(DynamoDbClient ddb,
                                           String tableName,
                                           Long readCapacity,
                                           Long writeCapacity) {

        System.out.format(
                "Updating %s with new provisioned throughput values\n",
                tableName);
        System.out.format("Read capacity : %d\n", readCapacity);
        System.out.format("Write capacity : %d\n", writeCapacity);

        ProvisionedThroughput tableThroughput = ProvisionedThroughput.builder()
                .readCapacityUnits(readCapacity)
                .writeCapacityUnits(writeCapacity)
                .build();

        UpdateTableRequest request = UpdateTableRequest.builder()
                .provisionedThroughput(tableThroughput)
                .tableName(tableName)
                .build();

        try {
            ddb.updateTable(request);
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        System.out.println("Done!");
    }
```

![](665813909.png)

#### Select Operation

``` java
public static int queryTable(DynamoDbClient ddb,
                                 String tableName,
                                 String partitionKeyName,
                                 String partitionKeyVal,
                                 String partitionAlias) {

        HashMap<String,String> attrNameAlias = new HashMap<String,String>();
        attrNameAlias.put(partitionAlias, partitionKeyName);

        // Set up mapping of the partition name with the value
        HashMap<String, AttributeValue> attrValues =
                new HashMap<String,AttributeValue>();

        attrValues.put(":"+partitionKeyName, AttributeValue.builder()
                .s(partitionKeyVal)
                .build());

        QueryRequest queryReq = QueryRequest.builder()
                .tableName(tableName)
                .keyConditionExpression(partitionAlias + " = :" + partitionKeyName)
                .expressionAttributeNames(attrNameAlias)
                .expressionAttributeValues(attrValues)
                .build();

        try {
            QueryResponse response = ddb.query(queryReq);
            return response.count();
        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
       return -1;
    }
```

![](665813908.png)

### AWS JAVA SDK v2 - Enhanced

#### Insert Operation

``` java
public static void putRecord(DynamoDbEnhancedClient enhancedClient) {

        try {
            DynamoDbTable<Customer> custTable = enhancedClient.table("Customer", TableSchema.fromBean(Customer.class));

            // Create an Instant
            LocalDate localDate = LocalDate.parse("2021-08-16");
            LocalDateTime localDateTime = localDate.atStartOfDay();
            Instant instant = localDateTime.toInstant(ZoneOffset.UTC);

            // Populate the Table
            Customer custRecord = new Customer();
            custRecord.setCustName("Susan red");
            custRecord.setId("id66");
            custRecord.setEmail("abc@xyz.com");
            custRecord.setRegistrationDate(instant) ;

            // Put the customer data into a DynamoDB table
            custTable.putItem(custRecord);

        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        System.out.println("done");
    }
```

![](665813907.png)

#### Update Operation

``` java
public static String modifyItem(DynamoDbEnhancedClient enhancedClient, String keyVal, String email) {
        try {
            //Create a DynamoDbTable object
            DynamoDbTable<Customer> mappedTable = enhancedClient.table("Customer", TableSchema.fromBean(Customer.class));

            //Create a KEY object
            Key key = Key.builder()
                    .partitionValue(keyVal)
                    .build();

            // Get the item by using the key and update the email value.
            Customer customerRec = mappedTable.getItem(r->r.key(key));
            customerRec.setEmail(email);
            mappedTable.updateItem(customerRec);
            return customerRec.getEmail();

        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        return "";
    }
```

![](665813906.png)

#### Select Operation

``` java
public static String getItem(DynamoDbEnhancedClient enhancedClient) {
        try {
            //Create a DynamoDbTable object
            DynamoDbTable<Customer> mappedTable = enhancedClient.table("Customer", TableSchema.fromBean(Customer.class));

            //Create a KEY object
            Key key = Key.builder()
                    .partitionValue("id120")
                    .build();

            // Get the item by using the key
            Customer result = mappedTable.getItem(r->r.key(key));
            return "The email valie is "+result.getEmail();

        } catch (DynamoDbException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

}
```

![](665813905.png)

### Region_property

``` java
public static void main(String[] args) {
        
        final String usage = "\n" +
             "Usage:\n" +
             "    <tableName>\n\n" +
             "Where:\n" +
             "    tableName - The Amazon DynamoDB table to get information about (for example, Music3).\n\n" ;

        if (args.length != 1) {
            System.out.println(usage);
            System.exit(1);
        }

        String tableName = "Test";
        System.out.format("Getting description for %s\n\n", tableName);

                
        Regions region = Regions.US_WEST_2;     
        AmazonDynamoDB ddb1 = AmazonDynamoDBClientBuilder.standard()
        .withRegion(region)
        .build();
        
        CreateTableRequest request = new CreateTableRequest()
                .withAttributeDefinitions(new AttributeDefinition("Name", ScalarAttributeType.S))
                .withKeySchema(new KeySchemaElement("Name", KeyType.HASH))
                .withProvisionedThroughput(new ProvisionedThroughput(new Long(10), new Long(10))).withTableName(Table1);

        
        
        TestDescribeTable(ddb1,tableName);
        ddb1.close();
    
        System.out.println("Done!");

    }

    

    public static void TestDescribeTable(AmazonDynamoDB ddb1,String tableName) {

        
        System.out.format("Getting description for %s\n\n", table_name);

        

        try {
            DescribeTableResult table_info1 = ddb1.describeTable(tableName);
//               ddb4.describeTable(table_name).getTable();

            TableDescription table_info = table_info1.getTable();

            if (table_info != null) {
                System.out.format("Table name  : %s\n", table_info.getTableName());
                System.out.format("Table ARN   : %s\n", table_info.getTableArn());
                System.out.format("Status      : %s\n", table_info.getTableStatus());
                System.out.format("Item count  : %d\n", table_info.getItemCount().longValue());
                System.out.format("Size (bytes): %d\n", table_info.getTableSizeBytes().longValue());

                ProvisionedThroughputDescription throughput_info = table_info.getProvisionedThroughput();
                System.out.println("Throughput");
                System.out.format("  Read Capacity : %d\n", throughput_info.getReadCapacityUnits().longValue());
                System.out.format("  Write Capacity: %d\n", throughput_info.getWriteCapacityUnits().longValue());

                List<AttributeDefinition> attributes = table_info.getAttributeDefinitions();
                System.out.println("Attributes");
                for (AttributeDefinition a : attributes) {
                    System.out.format("  %s (%s)\n", a.getAttributeName(), a.getAttributeType());
                }
            }
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
        System.out.println("\nDone!");
    }

        }
```

![](665813904.png)

## Known Limitations

-   If the table name is not resolved in the CRUD API, then the link is
    created with an unknown Table object
