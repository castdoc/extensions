---
title: "Support of DynamoDB for Shell"
linkTitle: "Support of DynamoDB for Shell"
type: "docs"
---

CAST supports DynamoDB via its com.castsoftware.shell extension. Details
about how this support is provided for Shell source code is discussed
below.

## Supported versions

| Versions | Supported                                                                                   |
|----------|---------------------------------------------------------------------------------------------|
| V1       | ![(tick)](/images/icons/emoticons/check.svg)  |
| V2       | ![(tick)](/images/icons/emoticons/check.svg)  |

## Supported commands

All commands listed below have as caller a Shell Program or Function and
as callee a SHELL AWS (Unknown) DynamoDB Table:

| API (with CRUD links)                       | Link Type                    | Remark                                |
|---------------------------------------------|------------------------------|---------------------------------------|
| aws dynamodb get-item                       | useSelectLink                | \-                                    |
| aws dynamodb scan                           | useSelectLink                | \-                                    |
| aws dynamodb query                          | useSelectLink                | \-                                    |
| aws dynamodb create-backup                  | useSelectLink                | \-                                    |
| aws dynamodb batch-get-item                 | useSelectLink                | Only inline json input data supported |
| aws dynamodb batch-write-item               | useInsertLink, useDeleteLink | Only inline json input data supported |
| aws dynamodb restore-table-to-point-in-time | useSelectLink, useInsertLink | \-                                    |
| aws dynamodb restore-table-from-backup      | useUpdateLink                | \-                                    |
| aws dynamodb delete-table                   | useDeleteLink                | \-                                    |
| aws dynamodb delete-item                    | useDeleteLink                | \-                                    |
| aws dynamodb transact-write-items           | useUpdateLink, useDeleteLink | Only inline json input data supported |
| aws dynamodb update-item                    | useUpdateLink                | \-                                    |
| aws dynamodb update-table                   | useUpdateLink                | \-                                    |
| aws dynamodb put-item                       | useUpdateLink, useInsertLink | \-                                    |

| API                              | Effect                       |
|----------------------------------|------------------------------|
| aws dynamodb create-table        | A DynamoDB table is created  |
| aws dynamodb create-global-table | A DynamoDB table is created  |

## Objects

Objects created specific for DynamoDB AWS service interpretation:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Metamodel Name</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370208.png" draggable="false"
data-image-src="666370208.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370208"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="database.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576750011"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Shell AWS DynamoDB Database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370207.png" draggable="false"
data-image-src="666370207.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370207"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576750011"
data-linked-resource-container-version="1" height="34" /></p>
</div></td>
<td class="confluenceTd">Shell AWS DynamoDB Table</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370206.png" draggable="false"
data-image-src="666370206.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370206"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="576750011"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Shell AWS Unknown DynamoDB Table</td>
</tr>
</tbody>
</table>

## What results can you expect?

A DynamoDB Table object can be created by two different situations. In
the first one, explicit creation of tables is detected by the presence
of create-table or create-global-table commands and therefore bookmarked
to the corresponding command string. In the second situation, there is
no explicit table creation, but references to tables are present in
different commands. In this case, a DynamoDB Table object is created by
reference and all references are bookmarked into the object.

When the name of a table is not resolved (either because of absence of
information or technical limitations) a Shell AWS Unknown DynamoDB
Table is created instead. A single unknown table is created per project.

Below we illustrate the expected results with various examples:

### Example 1

The following code in a file app.sh contains a single aws-dynamodb
command enclosed in a Shell Function (GetItem) requesting an item from a
DynamoDB table of name ProductCatalog:

``` bash
#!/bin/sh


GetItem () {
   aws dynamodb get-item --table-name ProductCatalog --key '{"Id": {"N": "102"}}' --projection-expression "#T, #C, #P" --expression-attribute-names file://names.json
}
```

As a result, a Shell AWS DynamobDB Table is created after the reference
to the table, together with a parent Shell AWS DynamoDB Database objet.
A link is also created of type useSelectLink between the caller GetItem
function and the table. 

![](576750023.png)

>Note that a single Shell AWS DynamoDB Database objet is created per
project (if Shell AWS DynamoDB Tables are present).

### Example 2

The following code in a file app.sh contains a single aws-dynamodb
command enclosed in a Shell Function (TransactWriteItems2) writing and
deleting items (details are omitted for simplicity) from a DynamoDB
table of name MusicCollection:

``` bash
#!/bin/sh


TransactWriteItems2() {
   aws dynamodb transact-write-items --transact-items '[{"Update": {"TableName": "MusicCollection"}},{"Delete": {"TableName": "MusicCollection"}}]' --return-consumed-capacity TOTAL --return-item-collection-metrics SIZE
}
```

The references to the table are inside the  --transact-items argument
given as a json string together with the actions (Update, Delete). The
analyzer will parse the json input string and create the corresponding
objects and links (useUpdateLink and useDeleteLink):

![](576750022.png)

## Known Limitations

-   Using external .json files as inputs is not yet supported
    (ex. --transact-items file://transact-items.json)
