---
title: "Support of DynamoDB for .NET"
linkTitle: "Support of DynamoDB for .NET"
type: "docs"
---

>CAST supports DynamoDB via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about how this support is provided for .NET source
code is discussed below.

## Supported Client Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a
href="https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/LowLevelDotNetItemCRUD.html"
rel="nofollow">AWS DynamoDB .NET SDK</a></td>
<td class="confluenceTd">up to: 2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.CreateBackup</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.CreateBackupAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.CreateGlobalTable</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.CreateGlobalTableAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.CreateTable</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.CreateTableAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.ImportTable</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.ImportTableAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.PutItem</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.PutResourcePolicy</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.PutResourcePolicyAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.RestoreTableFromBackup</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.RestoreTableFromBackupAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.ExecuteStatement</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.ExecuteStatementAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.ExecuteTransaction</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.PutItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.PutItemAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.TryPutItem</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.Save</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.SaveAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.CreateTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.CreateBackupAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ImportTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.RestoreTableFromBackupAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.BatchWriteItemAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.UpdateGlobalTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.UpdateGlobalTableSettingsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteBackupAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteResourcePolicyAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.TryPutItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.CreateBatchWrite</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.CreateTransactWrite</p>


</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.UpdateItem</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.UpdateItemAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.UpdateTable</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.UpdateTableAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchExecuteStatement</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchExecuteStatementAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.GetItem</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.GetItemAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.Scan</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.ScanAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.UpdateItem</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.UpdateItemAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.UpdateTable</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.UpdateTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.TransactGetItemsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.TransactWriteItemsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ExecuteStatementAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ExecuteTransactionAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.UpdateItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.UpdateItemAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.TryUpdateItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.UpdateHelper</p>


</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select </td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.Scan</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.ScanAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchGetItem</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchGetItemAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchWriteItem</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchWriteItemAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.GetItem</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.GetItemAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchExecuteStatement</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.BatchExecuteStatementAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeTable</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.BatchGetItem</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.BatchGetItemAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.Query</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.QueryAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeBackupAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeContinuousBackupsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeEndpointsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeExportAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeGlobalTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeGlobalTableSettingsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeImportAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DescribeTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ImportTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ListBackupsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ListImportsAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ListTablesAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ScanAsync</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.QueryAsync</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.LoadAsync</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.Load</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.Scan</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.ScanAsync</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.FromDocuments</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.FromQuery</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.DescribeTable</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.GetItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.GetItemAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.Query</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.Scan</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.TryLoadTable</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.TryLoadTable</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.CreateBatchGet</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.CreateTransactGet</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.GetItemHelper</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.GetItemHelperAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.LoadTableInfo</p>

</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteItem</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteItemAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteTable</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteTableAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteBackup</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteBackupAsync</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteResourcePolicy</p>
<p>Amazon.DynamoDBv2.AmazonDynamoDBClient.DeleteResourcePolicyAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteItem</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteItemAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteTable</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteTableAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ExecuteStatementAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.ExecuteTransactionAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteBackupAsync</p>
<p>Amazon.DynamoDBv2.IAmazonDynamoDB.DeleteResourcePolicyAsync</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.Delete</p>
<p>Amazon.DynamoDBv2.DataModel.DynamoDBContext.DeleteAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.DeleteItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.DeleteItemAsync</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.TryDeleteItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.TryDeleteItem</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.DeleteHelper</p>
<p>Amazon.DynamoDBv2.DocumentModel.Table.DeleteHelperAsync</p>

</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813875.png" draggable="false"
data-image-src="665813875.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813875"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813866"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">DotNet AWS DynamoDB Client</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813874.png" draggable="false"
data-image-src="665813874.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813874"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813866"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">DotNet AWS DynamoDB Table</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813873.png" draggable="false"
data-image-src="665813873.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813873"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813866"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">DotNet AWS Unknown DynamoDB Client</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="665813872.png" draggable="false"
data-image-src="665813872.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813872"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813866"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet AWS Unknown DynamoDB Table</td>
</tr>
</tbody>
</table>

## Links

<table class="relative-table wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh"> Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>belongsTo</p></td>
<td class="confluenceTd"><p>From DotNet AWS DynamoDB Table to DotNet
AWS DynamoDB Client object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>useInsertink</p></td>
<td class="confluenceTd"><p>Between the caller .NET Method (C#) object
and DotNet AWS DynamoDB Table</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>PutItem</li>
<li>PutItemAsync</li>
<li>TryPutItem</li>
<li>Save</li>
<li>QueryAsync</li>
<li>CreateBackup</li>
<li>CreateBackupAsync</li>
<li>CreateGlobalTable</li>
<li>CreateGlobalTableAsync</li>
<li>CreateTable</li>
<li>CreateTableAsync</li>
<li>ImportTable</li>
<li>ImportTableAsync</li>
<li>PutResourcePolicy</li>
<li>PutResourcePolicyAsync</li>
<li>RestoreTableFromBackup</li>
<li>RestoreTableFromBackupAsync</li>
<li>TryPutItem</li>
<li>CreateBatchWrite</li>
<li>CreateTransactWrite</li>
<li>SaveAsync</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>Between the caller .NET Method (C#) object
and DotNet AWS DynamoDB Table</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>UpdateTable</li>
<li>UpdateTableAsync</li>
<li>UpdateItem</li>
<li>UpdateItemAsync</li>
<li>UpdateGlobalTable</p>
<li>UpdateGlobalTableAsync</li>
<li>UpdateGlobalTableSettings</li>
<li>UpdateGlobalTableSettingsAsync</li>
<li>UpdateTableReplicaAutoScaling</li>
<li>UpdateTableReplicaAutoScalingAsync</p>
<li>UpdateTimeToLive</li>
<li>UpdateTimeToLiveAsync</li>
<li>BatchWriteItemAsync</li>
<li>UpdateHelper</li>
<li><p>TryUpdateItem</p></li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>Between the caller .NET Method (C#) object
and DotNet AWS DynamoDB Table</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>Query</li>
<li>QueryAsync</li>
<li>Scan</li>
<li>ScanAsync</li>
<li>DescribeTable</li>
<li>GetItem</li>
<li>GetItemAsync</li>
<li>BatchGetItem</li>
<li>BatchGetItemAsync</li>
<li>BatchWriteItem</li>
<li>BatchWriteItemAsync</li>
<li>Load</li>
<li>LoadAsync</li>
<li>FromDocuments</li>
<li>FromQuery</li>
<li>BatchExecuteStatement</li>
<li>BatchExecuteStatementAsync</li>
<li>DescribeBackup</li>
<li>DescribeBackupAsync</li>
<li>DescribeContinuousBackups</li>
<li>DescribeContinuousBackupsAsync</li>
<li>DescribeContributorInsights</li>
<li>DescribeContributorInsightsAsync</li>
<li>DescribeEndpoints</li>
<li>DescribeEndpointsAsync</li>
<li>DescribeExport</li>
<li>DescribeExportAsync</li>
<li>DescribeGlobalTable</li>
<li>DescribeGlobalTableAsync</li>
<li>DescribeGlobalTableSettings</li>
<li>DescribeGlobalTableSettingsAsync</li>
<li>DescribeImport</li>
<li>DescribeImportAsync</li>
<li>DescribeLimits</li>
<li>DescribeLimitsAsync</li>
<li>DescribeTableReplicaAutoScaling</li>
<li>DescribeTableReplicaAutoScalingAsync</li>
<li>DescribeTimeToLive</li>
<li>DescribeTimeToLiveAsync</li>
<li>ExecuteStatement</li>
<li>ExecuteStatementAsync</li>
<li>ExecuteTransaction</li>
<li>GetResourcePolicy</li>
<li>GetResourcePolicyAsync</li>
<li>GetItemHelper</li>
<li>GetItemHelperAsync</li>
<li>ListBackupsAsync</li>
<li>ListImportsAsync</li>
<li>ListTablesAsync</li>
<li>LoadTableInfo</li>
<li>TryLoadTable</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>Between the caller .NET Method (C#) object
and DotNet AWS DynamoDB Table</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>DeleteItem</li>
<li>DeleteItemAsync</li>
<li>DeleteTable</li>
<li>DeleteTableAsync</li>
<li>Delete</li>
<li>DeleteBackup</li>
<li>DeleteBackupAsync</li>
<li>DeleteResourcePolicy</li>
<li>DeleteResourcePolicyAsync</li>
<li>DeleteHelper</li>
<li>DeleteHelperAsync</li>
<li>TryDeleteItem</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### AmazonDynamoDB Client and Table Creation

``` c#
class MidlevelItemCRUD
    {
        AmazonDynamoDBConfig clientConfig = new AmazonDynamoDBConfig();
        // Set the endpoint URL
        clientConfig.ServiceURL = "http://localhost:8000";
        AmazonDynamoDBClient client = new AmazonDynamoDBClient(clientConfig);
        private static string tableName = "ProductCatalog";
        // The sample uses the following id PK value to add book item.
        private static int sampleBookId = 555;

        static void Main(string[] args)
        {
            try
            {
                Table productCatalog = Table.LoadTable(client, tableName);
                CreateBookItem(productCatalog);
                RetrieveBook(productCatalog);
                // Couple of sample updates.
                UpdateMultipleAttributes(productCatalog);
                UpdateBookPriceConditionally(productCatalog);

                // Delete.
                DeleteBook(productCatalog);
                Console.WriteLine("To continue, press Enter");
                Console.ReadLine();
            }
            catch (AmazonDynamoDBException e) { Console.WriteLine(e.Message); }
            catch (AmazonServiceException e) { Console.WriteLine(e.Message); }
            catch (Exception e) { Console.WriteLine(e.Message); }
        }
                   
     
```

![](665813871.png)

### Insert Operation

``` c#
 private static void CreateBookItem(Table productCatalog)
        {
            Console.WriteLine("\n* Executing CreateBookItem() *");
            var book = new Document();
            book["Id"] = sampleBookId;
            book["Title"] = "Book " + sampleBookId;
            book["Price"] = 19.99;
            book["ISBN"] = "111-1111111111";
            book["Authors"] = new List<string> { "Author 1", "Author 2", "Author 3" };
            book["PageCount"] = 500;
            book["Dimensions"] = "8.5x11x.5";
            book["InPublication"] = new DynamoDBBool(true);
            book["InStock"] = new DynamoDBBool(false);
            book["QuantityOnHand"] = 0;

            productCatalog.PutItem(book);
        }
```

![](665813870.png)

### Select Operation

``` c#
 private static void RetrieveBook(Table productCatalog)
        {
            Console.WriteLine("\n* Executing RetrieveBook() *");
            // Optional configuration.
            GetItemOperationConfig config = new GetItemOperationConfig
            {
                AttributesToGet = new List<string> { "Id", "ISBN", "Title", "Authors", "Price" },
                ConsistentRead = true
            };
            Document document = productCatalog.GetItem(sampleBookId, config);
            Console.WriteLine("RetrieveBook: Printing book retrieved...");
            PrintDocument(document);
        }
```

![](665813869.png)

### Update Operation

``` c#
private static void UpdateBookPriceConditionally(Table productCatalog)
        {
            Console.WriteLine("\n* Executing UpdateBookPriceConditionally() *");

            int partitionKey = sampleBookId;

            var book = new Document();
            book["Id"] = partitionKey;
            book["Price"] = 29.99;

            // For conditional price update, creating a condition expression.
            Expression expr = new Expression();
            expr.ExpressionStatement = "Price = :val";
            expr.ExpressionAttributeValues[":val"] = 19.00;

            // Optional parameters.
            UpdateItemOperationConfig config = new UpdateItemOperationConfig
            {
                ConditionalExpression = expr,
                ReturnValues = ReturnValues.AllNewAttributes
            };
            Document updatedBook = productCatalog.UpdateItem(book, config);
            Console.WriteLine("UpdateBookPriceConditionally: Printing item whose price was conditionally updated");
            PrintDocument(updatedBook);
        }
```

![](665813868.png)

### Delete Operation

``` c#
private static void DeleteBook(Table productCatalog)
        {
            Console.WriteLine("\n* Executing DeleteBook() *");
            // Optional configuration.
            DeleteItemOperationConfig config = new DeleteItemOperationConfig
            {
                // Return the deleted item.
                ReturnValues = ReturnValues.AllOldAttributes
            };
            Document document = productCatalog.DeleteItem(sampleBookId, config);
            Console.WriteLine("DeleteBook: Printing deleted just deleted...");
            PrintDocument(document);
        }
```

![](665813867.png)

## Known Limitations

-   Client objects are always default with the name 'client'
-   If the client and table are not resolved will create Unknown
    Objects.
