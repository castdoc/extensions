---
title: "Support of DynamoDB for Python"
linkTitle: "Support of DynamoDB for Python"
type: "docs"
---

>CAST supports DynamoDB via its
[com.castsoftware.python](https://extend.castsoftware.com/#/extension?id=com.castsoftware.python&version=latest) extension.
Details about how this support is provided is discussed below.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813953.png" draggable="false"
data-image-src="665813953.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813953"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="database.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="542343355"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Python DynamoDB Database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813954.png" draggable="false"
data-image-src="665813954.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813954"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="542343355"
data-linked-resource-container-version="1" height="34" /></p>
</div></td>
<td class="confluenceTd">Python DynamoDB Table</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813955.png" draggable="false"
data-image-src="665813955.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813955"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="542343355"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Python Unknown DynamoDB Table</td>
</tr>
</tbody>
</table>

## Boto3 framework

The support for DynamoDB in boto3 is focused on creating DynamoDB tables
and adding links representing CRUD-like operations on them.

### Methods for creation of tables

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
methods</p></th>
<th class="confluenceTh" style="text-align: left;">Link Type</th>
<th class="confluenceTh" style="text-align: left;">Caller</th>
<th class="confluenceTh" style="text-align: left;">Callee</th>
<th class="confluenceTh" style="text-align: left;">Remark</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.DynamoDB.create_table()</p></td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd" style="text-align: left;"><p>N/A</p>
<p><br />
</p></td>
<td class="confluenceTd" style="text-align: left;">Side effect: creation
of DynamoDB tables and parent database</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>boto3.resources.factory.dynamodb.ServiceResource.create_table()</p></td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd" style="text-align: left;"><p>N/A</p>
<p><br />
</p></td>
<td class="confluenceTd" style="text-align: left;">Side effect: creation
of DynamoDB tables and parent database</td>
</tr>
</tbody>
</table>

### Methods for getting a paginator from a DynamoDB client object

| Supported API methods                    | Link Type | Caller | Callee | Remark                                                                 |
|:-----------------------------------------|:----------|:-------|:-------|:-----------------------------------------------------------------------|
| botocore.client.DynamoDB.get_paginator() | N/A       | N/A    | N/A    | Returns a Paginator object (see below for supported Paginator methods) |

### High-level DynamoDB resource methods

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Supported API methods <br />
(boto3.resources.factory.dynamodb) </p></th>
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">batch_get_item()</td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python (Unknown) DynamoDB Table</td>
</tr>
<tr class="even">
<td class="confluenceTd">batch_write_item()</td>
<td class="confluenceTd">useInsertLink, useDeleteLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python (Unknown) DynamoDB Table</td>
</tr>
</tbody>
</table>

### High-level table resource methods for the DynamoDB database API

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Supported API methods <br />
(boto3.resources.factory.dynamodb.Table) </p></th>
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Remark</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">batch_writer()</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">Returns a BatchWriter object (see below table
for supported BatchWriter methods)</td>
</tr>
<tr class="even">
<td class="confluenceTd">delete()</td>
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Python callable artifact<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</td>
<td class="confluenceTd">Python (Unknown) DynamoDB Table<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd">delete_item()</td>
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">get_item()</td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd">load()</td>
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">put_item()</td>
<td class="confluenceTd">useInsertLink,
useDeleteLink, useSelectLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd">query()</td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">reload()</td>
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd">scan()</td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">update_item()</td>
<td
class="confluenceTd">useUpdateLink, useInsertLink, useSelectLink</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
<td class="confluenceTd">-</td>
</tr>
</tbody>
</table>

### Batch-writer methods

| Supported API methods (boto3.resources.factory.dynamodb.table.BatchWriter) | Link Type                                   | Caller                   | Callee                          |
|:---------------------------------------------------------------------------|---------------------------------------------|--------------------------|:--------------------------------|
| put_item()                                                                 | useInsertLink, useDeleteLink, useSelectLink | Python callable artifact | Python (Unknown) DynamoDB Table |
| delete_item()                                                              | useDeleteLink                               | \-                       | \-                              |

### Paginator methods

| Supported API methods                                 | Link Type     | Caller                   | Callee                          |
|-------------------------------------------------------|---------------|--------------------------|---------------------------------|
| botocore.client.DynamoDB.Paginator.Query.paginate | useSelectLink | Python callable artifact | Python (Unknown) DynamoDB Table |
| botocore.client.DynamoDB.Paginator.Scan.paginate  | useSelectLink | \-                       | \-                              |

### Low-level client methods for the DynamoDB database API

| API methods            | Link Type                                   | Caller                   | Callee                          |
|------------------------|---------------------------------------------|--------------------------|:--------------------------------|
| batch_get_item()       | useSelectLink                               | Python callable artifact | Python (Unknown) DynamoDB Table |
| batch_write_item()     | useInsertLink, useDeleteLink                | Python callable artifact | Python (Unknown) DynamoDB Table |
| delete_item()          | useDeleteLink                               | Python callable artifact | Python (Unknown) DynamoDB Table |
| delete_table()         | useDeleteLink                               | Python callable artifact | Python (Unknown) DynamoDB Table |
| get_item()             | useSelectLink                               | Python callable artifact | Python (Unknown) DynamoDB Table |
| put_item()             | useInsertLink, useDeleteLink, useSelectLink | Python callable artifact | Python (Unknown) DynamoDB Table |
| query()                | useSelectLink                               | Python callable artifact | Python (Unknown) DynamoDB Table |
| scan()                 | useSelectLink                               | Python callable artifact | Python (Unknown) DynamoDB Table |
| transact_get_items()   | useSelectLink                               | Python callable artifact | Python (Unknown) DynamoDB Table |
| transact_write_items() | useInsertLink, useDeleteLink                | Python callable artifact | Python (Unknown) DynamoDB Table |
| update_item()          | useUpdateLink, useInsertLink, useSelectLink | Python callable artifact | Python (Unknown) DynamoDB Table |

## What results can you expect?

### Creation of DynamoDB tables

The two similar create_table() methods provided by boto3 are supported:
for the low-level client object and the resource object (as in the
example below):

``` py
# https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.01.html

import boto3

def create_movie_table(dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8000")

    table = dynamodb.create_table(
        TableName='Movies',
        KeySchema=[...],
        AttributeDefinitions=[...],
    )
    return table
```

Upon resolution of the table name, a Python DynamoDB Table object is
created under a Python DynamoDB Database:

Python DynamoDB Database objects are only created if dynamodb tables are
present in the results. A single Python DynamoDB Database object is
created per project at most. No link is created between the artifact
invoking table creation and the resulting table object. However, the
bookmark of the table does reflect the corresponding method call.

When the name of a table is not resolved (either because of absence of
information or technical limitations) a Python Unknown DynamoDB Table is
created instead. A single unknown table is created per project.

### Actions on DynamoDB tables

Only actions that have an impact on data in DynamoDB tables are
modelized. These actions are represented with CRUD links. In the example
below the method batch_write_item() of the low-level client object
(DynamoDB) is called. This method can insert new table items or delete
them, therefore the presence of the two link types (insertUseLink,
deleteUseLink).

``` py
# sample_batch_write_item.py
import boto3

TABLE = 'fabric'
dynamodb = boto3.client('dynamodb')

def write_item():
    response = dynamodb.batch_write_item(
        RequestItems = {
          TABLE: [{"PutRequest": { "Item": { ... } }}]
        }
    )
```

![](542343356.png)

## Known Limitations

-   Accessing tables via tables collections such as
    "dynamodb.tables.all()" is not supported.
-   The query language PartiQL is not supported (and thus related API
    calls listed below).
-   The following methods might be supported in the future:

| Non-supported                    | Possible impact                        |
|----------------------------------|----------------------------------------|
| batch_execute_statement()        | Missing CRUD links to tables (PartiQL) |
| execute_statement()              | Missing CRUD links to tables (PartiQL) |
| execute_transaction()            | Missing CRUD links to tables (PartiQL) |
| export_table_to_point_in_time()  | Missing link to S3 bucket              |
| restore_table_from_backup()      | Missing table                          |
| restore_table_to_point_in_time() | Missing table                          |
