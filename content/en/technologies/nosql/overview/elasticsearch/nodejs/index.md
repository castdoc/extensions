---
title: "Support of Elasticsearch for Node.js"
linkTitle: "Support of Elasticsearch for Node.js"
type: "docs"
---

>CAST supports Elasticsearch via its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest) extension.
Details about how this support is provided for Node.js source code is
discussed below.

## Supported Client Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a
href="https://www.npmjs.com/package/elasticsearch"
rel="nofollow">Elasticsearch</a></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd"><p>index</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>update</p>
<p>updateByQuery</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>search</p>
<p>msearch</p>
<p>explain</p>
<p>get</p>
<p>mget</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd"><p>delete</p>
<p>deleteByQuery</p></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon </th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="666370349.png" draggable="false"
data-image-src="666370349.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370349"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370331"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Node.js Elasticsearch Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="666370348.png" draggable="false"
data-image-src="666370348.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370348"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370331"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Node.js Elasticsearch Unknown Cluster</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="666370347.png" draggable="false"
data-image-src="666370347.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370347"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370331"
data-linked-resource-container-version="1" height="34" /></p>
</div></td>
<td class="confluenceTd">Node.js Elasticsearch Index</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="666370346.png" draggable="false"
data-image-src="666370346.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370346"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370331"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Node.js Elasticsearch Unknown Index </td>
</tr>
</tbody>
</table>

## Links

Links are created for transaction and function point needs:

<table class="relative-table wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh"> Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">parentLink</td>
<td class="confluenceTd"><p>Between Elasticsearch Cluster object and
Elasticsearch Index object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useXLink</td>
<td class="confluenceTd">Between the caller Node.js Initialization /
Function objects and Elasticsearch Index objects</td>
<td class="confluenceTd"><p>bulk</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller Node.js Initialization /
Function objects and Elasticsearch Index objects</td>
<td class="confluenceTd"><p>index</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the caller Node.js Initialization /
Function objects and Elasticsearch Index objects</td>
<td class="confluenceTd"><p>delete</p>
<p>deleteByQuery</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>useSelectLink</p></td>
<td class="confluenceTd">Between the caller Node.js Initialization /
Function objects and Elasticsearch Index objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>search</p>
<p>msearch</p>
<p>explain</p>
<p>get</p>
<p>mget</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller Node.js Initialization /
Function objects and Elasticsearch Index objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>update</p>
<p>updateByQuery</p>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Cluster creation

#### Older version: 16.x

``` js
const elasticsearch = require('elasticsearch');

// list of string
let client = new elasticsearch.Client({
    hosts: [
        'https://[username]:[password]@[server]:[port]/',
        'https://[username]:[password]@[server]:[port]/'
    ]
});

// object
let client1 = new elasticsearch.Client({
    host: {
        protocol: 'https',
        host: 'my-site.com',
        port: 80,
        path: '/elasticsearch/'
    }
});

// list of object
let client2 = new elasticsearch.Client({
    hosts: [
        {
            protocol: 'https',
            host: 'box1.server.org',
            port: 56394,
            // these custom values are used below by the selector
            country: 'EU',
            weight: 10
        },
        {
            protocol: 'https',
            host: 'box2.server.org',
            port: 56394,
            // these custom values are used below by the selector
            country: 'US',
            weight: 50
        }
    ]
});

// list of string and object
let client3 = new elasticsearch.Client({
    hosts: [
        'https://[username]:[password]@[server]:[port]/',
        {
            protocol: 'https',
            host: 'my-site.com',
            port: 80,
            path: '/elasticsearch/'
        }
    ]
});
```

![](666370345.png)

![](666370344.png)

![](666370343.png)

![](666370342.png)

#### Newer version: \>= 5.x

``` js
const {Client, errors} = require('@elastic/elasticsearch');

// string
const client = new Client({node: 'http://localhost:9200'});

// URL object
const client2 = new Client({node: new URL('http://127.0.0.1')});

// list of string and URL object
const client3 = new Client({
    nodes: [
        'http://192.168.167.1',
        new URL('google.com')
    ]
});
```

![](666370341.png)

![](666370340.png)

![](666370339.png)

### Insert operation

``` c#
const response = client.index({
  index: 'myindex',
  type: 'mytype',
  id: '1',
  body: {
    title: 'Test 1',
    tags: ['y', 'z'],
    published: true,
  }
});
```

![](666370338.png)

### Update operation

``` js
// string
let response = client.update({
    index: 'indexA',
    type: 'mytype',
    id: '1',
    body: {
        // put the partial document under the `doc` key
        doc: {
            title: 'Updated'
        }
    }
});

// string
response = client.updateByQuery({
        index: 'indexB',
        type: type,
        body: {
            "query": {"match": {"animal": "bear"}},
            "script": {"inline": "ctx._source.color = 'pink'"}
        }
    }, function (err, res) {
        if (err) {
            reportError(err)
        }
        cb(err, res)
    }
);

// list of string
response = client.updateByQuery({
        index: ['indexC', 'indexD'],
        type: type,
        body: {
            "query": {"match": {"animal": "bear"}},
            "script": {"inline": "ctx._source.color = 'pink'"}
        }
    }, function (err, res) {
        if (err) {
            reportError(err)
        }
        cb(err, res)
    }
);
```

![](666370337.png)

![](666370336.png)

![](666370335.png)

### Select operation 

``` js
let response = client.get({
  index: 'indexA',
  type: 'mytype',
  id: 1
});
```

![](666370334.png)

### Delete operation

``` js
client.delete({
    index: 'indexA',
    type: 'mytype',
    id: '1'
});
```

![](666370333.png)

### Bulk

``` js
client.bulk({
  body: [
    // action description
    { index:  { _index: 'indexA', _type: 'mytype', _id: 1 } },
     // the document to index
    { title: 'foo' },
    // action description
    { update: { _index: 'indexB', _type: 'mytype', _id: 2 } },
    // the document to update
    { doc: { title: 'foo' } },
    // action description
    { delete: { _index: 'indexC', _type: 'mytype', _id: 3 } },
    // no document needed for this delete
  ]
}, function (err, resp) {
  // ...
});
```

![](666370332.png)

## Known Limitations

-   For HTML5 Extension version \< ...
    -   Identifiers inside a hardcoded list into a dictionary are not
        resolved, see:

``` js
// LIMITATION
a = 'foo'
client.updateByQuery({
    index: ['bar', a] // In this case « a » won't be resolved 
});

// NO LIMITATION
b = ['bar', a];
client.updateByQuery({
    index: b // In this case « a » will be resolved 
});
```

-   Operation operating on all indexes (no index argument or index =
    \_all) do not create links

-   Using client inside object is not supported, see:

``` js
// In wrapper.js
const elasticsearch = require('elasticsearch');

var self = {};

self.client = new elasticsearch.Client({
  host: 'client.org'
});


module.exports = self;

//----------------------------------------------------------

// In app.js
const ElasticsearchWrapper = require('./wrapper.js');

ElasticsearchWrapper.client.deleteByQuery({  // -> this call won't be detected as the client is wrapped inside an object
        index: 'some_index',
    },
    function (error, response) {
      if (error)
        sails.log.info("error ", error);
    }
);
```
