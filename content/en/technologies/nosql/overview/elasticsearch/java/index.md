---
title: "Support of Elasticsearch for Java"
linkTitle: "Support of Elasticsearch for Java"
type: "docs"
---

>CAST supports Elasticsearch via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about how this support is provided for Java source
code is discussed below.

## Supported Client Libraries

| Library | Version | Supported |
|---|---|:---:|
| [TransportClient](https://www.javadoc.io/doc/org.elasticsearch/elasticsearch/8.0.0/index.html) | up to: 8.x.x | :white_check_mark: |
| [LowLevelRestClient](https://www.elastic.co/guide/en/elasticsearch/client/java-api-client/current/java-rest-low.html) | up to: 6.0.1 | :white_check_mark: |
| [HighLevelRestClient](https://artifacts.elastic.co/javadoc/org/elasticsearch/client/elasticsearch-rest-high-level-client/6.2.4/org/elasticsearch/client/RestHighLevelClient.html) | up to: 8.x.x | :white_check_mark: |
| [JestClient](https://mvnrepository.com/artifact/io.searchbox/jest-common) | Up to: 6.x.x | :white_check_mark: |

## Supported Operations
<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.elasticsearch.client</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.elasticsearch.client.support.AbstractClient.prepareIndex</li>
<li>org.elasticsearch.client.RestHighLevelClient.index</li>
<li>org.elasticsearch.client.RestHighLevelClient.indexAsync</li>
<li>org.elasticsearch.client.Client.index</li>
<li>org.elasticsearch.client.Client.prepareIndex</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>co.elastic.clients.elasticsearch</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.create</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.index</li>
<li>co.elastic.clients.elasticsearch.indices.ElasticsearchIndicesClient.create</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.create</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.index</li>
</ul>
</div>
</details></div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.elasticsearch.client</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.elasticsearch.client.support.AbstractClient.prepareUpdate</li>
<li>org.elasticsearch.client.support.AbstractClient.update</li>
<li>org.elasticsearch.client.RestHighLevelClient.update</li>
<li>org.elasticsearch.client.RestHighLevelClient.updateAsync</li>
<li>org.elasticsearch.client.Client.update</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>co.elastic.clients.elasticsearch</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.update
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.updateByQuery</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.update</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.updateByQuery</li>
</ul>
</div>
</details></div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111564" class="expand-container">
<details><summary>org.elasticsearch.client.support.AbstractClient</summary>
<div id="expander-content-1998111564" class="expand-content">
<ul>
<li>org.elasticsearch.client.support.AbstractClient.prepareExplain</li>
<li>org.elasticsearch.client.support.AbstractClient.prepareGet</li>
<li>org.elasticsearch.client.support.AbstractClient.prepareMultiGet</li>
<li>org.elasticsearch.client.support.AbstractClient.multiGet</li>
<li>org.elasticsearch.client.support.AbstractClient.multiSearch</li>
<li>org.elasticsearch.client.support.AbstractClient.fieldCaps</li>
<li>org.elasticsearch.client.support.AbstractClient.get</li>
<li>org.elasticsearch.client.support.AbstractClient.explain</li>
</ul>
</div>
</details></div>
<div id="expander-1998111565" class="expand-container">
<details><summary>org.elasticsearch.client.RestHighLevelClient</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>org.elasticsearch.client.RestHighLevelClient.multiGet</li>
<li>org.elasticsearch.client.RestHighLevelClient.multiGetAsync</li>
<li>org.elasticsearch.client.RestHighLevelClient.multiSearch</li>
<li>org.elasticsearch.client.RestHighLevelClient.multiSearchAsync</li>
<li>org.elasticsearch.client.RestHighLevelClient.search</li>
<li>org.elasticsearch.client.RestHighLevelClient.searchAsync</li>
<li>org.elasticsearch.client.RestHighLevelClient.searchScroll</li>
<li>org.elasticsearch.client.RestHighLevelClient.searchScrollAsync</li>
<li>org.elasticsearch.client.RestHighLevelClient.exists</li>
<li>org.elasticsearch.client.RestHighLevelClient.existsAsync</li>
<li>org.elasticsearch.client.RestHighLevelClient.get</li>
<li>org.elasticsearch.client.RestHighLevelClient.getAsync</li>
</ul>
</div>
</details></div>
<div id="expander-1998111566" class="expand-container">
<details><summary>org.elasticsearch.client.Client</summary>
<div id="expander-content-1998111566" class="expand-content">
<ul>
<li>org.elasticsearch.client.Client.explain</li>
<li>org.elasticsearch.client.Client.get</li>
<li>org.elasticsearch.client.Client.multiGet</li>
<li>org.elasticsearch.client.Client.multiSearch</li>
<li>org.elasticsearch.client.Client.search</li>
<li>org.elasticsearch.client.Client.prepareSearch</li>
</ul>
</div>
</details></div>
<div id="expander-1998111567" class="expand-container">
<details><summary>co.elastic.clients.elasticsearch.ElasticsearchClient</summary>
<div id="expander-content-1998111567" class="expand-content">
<ul>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.count</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.exists</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.get</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.mget</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.msearch</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.search</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.scroll</li>
</ul>
</div>
</details></div>
<div id="expander-1998111568" class="expand-container">
<details><summary>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient</summary>
<div id="expander-content-1998111568" class="expand-content">
<ul>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.count</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.exists</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.get</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.mget</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.msearch</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.search</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.scroll</li>
</ul>
</div>
</details></div>
<ul>
<li>co.elastic.clients.elasticsearch.indices.ElasticsearchIndicesClient.exists</li>
<li>co.elastic.clients.elasticsearch.indices.ElasticsearchIndicesClient.get</li>
<li>co.elastic.clients.elasticsearch.indices.ElasticsearchIndicesClient.refresh</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.elasticsearch.client</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.elasticsearch.client.support.AbstractClient.prepareDelete</li>
<li>org.elasticsearch.client.support.AbstractClient.delete</li>
<li>org.elasticsearch.client.RestHighLevelClient.delete</li>
<li>org.elasticsearch.client.RestHighLevelClient.deleteAsync</li>
<li>org.elasticsearch.client.Client.delete</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>co.elastic.clients.elasticsearch</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.delete</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.deleteByQuery</li>
<li>co.elastic.clients.elasticsearch.indices.ElasticsearchIndicesClient.delete</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.delete</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.deleteByQuery</li>
</ul>
</div>
</details></div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Use </td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.elasticsearch.client</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.elasticsearch.client.support.AbstractClient.execute</li>
<li>org.elasticsearch.client.support.AbstractClient.bulk</li>
<li>org.elasticsearch.client.RestClient.performRequest</li>
<li>org.elasticsearch.client.RestClient.performRequestAsync</li>
<li>org.elasticsearch.client.Client.bulk</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.elasticsearch.client.RestHighLevelClient</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.elasticsearch.client.RestHighLevelClient.bulk</li>
<li>org.elasticsearch.client.RestHighLevelClient.bulkAsync</li>
<li>org.elasticsearch.client.RestHighLevelClient.performRequest</li>
<li>org.elasticsearch.client.RestHighLevelClient.performRequestAsync</li>
</ul>
</div>
</details></div>
<ul>
<li>io.searchbox.client.JestClient.execute</li>
<li>io.searchbox.client.JestClient.executeAsync</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchClient.bulk</li>
<li>co.elastic.clients.elasticsearch.ElasticsearchAsyncClient.bulk</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon </th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370301.png" draggable="false"
data-image-src="666370301.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370301"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="database.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370282"
data-linked-resource-container-version="1" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Elasticsearch Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370300.png" draggable="false"
data-image-src="666370300.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370300"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370282"
data-linked-resource-container-version="1" height="34" /></li>
</div></td>
<td class="confluenceTd">Java Elasticsearch Index</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370299.png" draggable="false"
data-image-src="666370299.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370299"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370282"
data-linked-resource-container-version="1" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Unknown Elasticsearch Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370298.png" draggable="false"
data-image-src="666370298.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370298"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370282"
data-linked-resource-container-version="1" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Unknown Elasticsearch Index</td>
</tr>
</tbody>
</table>

## Links

<table class="relative-table wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh"> Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd"><li>From Java Elasticsearch Index object to Java
Elasticsearch Cluster object</li></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Java Elasticsearch Index objects</td>
<td class="confluenceTd"><li>bulk</li>
<li>bulkAsync</li>
<li>performRequest</li>
<li>performRequestAsync</li>
<li>execute</li>
<li>executeAsync</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Java Elasticsearch Index objects</td>
<td class="confluenceTd"><li>index</li>
<li>prepareIndex</li>
<li>indexAsync</li></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Java Elasticsearch Index objects</td>
<td class="confluenceTd"><li>delete</li>
<li>prepareDelete</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><li>useSelectLink</li></td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Java Elasticsearch Index objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>get</li>
<li>prepareGet</li>
<li>multiGet</li>
<li>multigetAsync</li>
<li>prepareMultiGet</li>
<li>multiSearch</li>
<li>multisearchAsync</li>
<li>prepareMultiSearch</li>
<li>search</li>
<li>searchAsync</li>
<li>prepareSearch</li>
<li>searchScroll</li>
<li>searchScrollAsync</li>
<li>explain</li>
<li>prepareExplain</li>
<li>exists</li>
<li>existsAsync</li>
<li>fieldCaps</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Java Elasticsearch Index objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>update</li>
<li>prepareUpdate</li>
<li>updateAsync</li>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Cluster and Index creation

``` java
 public test() throws UnknownHostException {
        Settings setting = Settings.builder()
                .put("cluster.name", "cluster1")
                .put("client.transport.sniff", true).build();
        this.client = new PreBuiltTransportClient(setting)
                .addTransportAddresses(new InetSocketTransportAddress(InetAddress.getByName(host), 9300),new InetSocketTransportAddress(InetAddress.getByName(host), 9301));
 }
```

![](666370297.png)

``` java
public class restClientConnection {

    private HttpHost httpHost;
    private RestClient restClient;
    private static HttpHost httpHost2 = new HttpHost("localhost",9303,"http");
    
    public restClientConnection(String host, int port, String protocol) {
        
        this.httpHost = new HttpHost(host, port, protocol);
    }
    
    public RestClient connect() {
        
        this.restClient = RestClient.builder(this.httpHost,httpHost2).build();
        return this.restClient;
        
    }
}
```

![](666370296.png)

### Insert Operation

#### IndexDocument

``` c#
 public String insertid(@PathVariable final String id) throws IOException {

        IndexResponse response = this.client.prepareIndex("person", "id", id)
                .setSource(jsonBuilder()
                        .startObject()
                        .field("fname", "shakeel")
                        .field("fplace", "bangalore" )
                        .field("fteamName", "R&D")
                        .endObject()
                )
                
                .get();
        return response.getResult().toString();
    }
```

#### IndexDocumentAsync

``` c#
public String insertpid(@PathVariable final String id) throws IOException {

        IndexResponse response = this.client.prepareIndex()
                .setIndex("person")
                .setType("id")
                .setId(id)
                .setSource(jsonBuilder()
                        .startObject()
                        .field("fullname", "shakeel")
                        .field("age", "31" )
                        .field("qualification", "graduate")
                        .endObject()
                )
                
                .get();
        return response.getResult().toString();
    }
```

![](666370294.png)

#### ElasticsearchClient

```java
package realworld.db;

import co.elastic.clients.elasticsearch.ElasticsearchClient;

import static realworld.constant.Constants.*;

@Configuration
public class ElasticClient {

    @Bean
    public ElasticsearchClient elasticRestClient() throws IOException {

    private void createIndexWithDateMapping(ElasticsearchClient esClient) throws IOException {
        BooleanResponse indexRes = esClient.indices().exists(ex -> ex.index("articles"));
        if (!indexRes.value()) {
            esClient.indices().create(c -> c
                .index("articles")
                .mappings(m -> m
                    .properties("createdAt", p -> p
                        .date(d -> d))
                    .properties("updatedAt", p -> p
                        .date(d -> d))));

        }
    }
}
```
![](es_client.png)

### Update Operation

``` c#
 public String update1(@PathVariable final String id) throws IOException {

        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("employee")
                .type("id")
                .id(id)
                .doc(jsonBuilder()
                        .startObject()
                        .field("gender", "male")
                        .endObject());
        try {
            UpdateResponse updateResponse = this.client.update(updateRequest).get();
            System.out.println(updateResponse.status());
            return updateResponse.status().toString();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e);
        }
        return "Exception";
    }
```

![](666370293.png)

``` java
public String execute() throws IOException {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.index("person")
                .type("id")
                .id("123")
                .doc(jsonBuilder()
                        .startObject()
                        .field("gender", "male")
                        .endObject());
        UpdateResponse gResponse = client.execute(UpdateAction.INSTANCE,updateRequest).actionGet();
        System.out.println(gResponse.toString());
        return gResponse.toString();
    }
```

![](666370292.png)

### Select Operation 

``` c#
public String explain1() throws IOException {
        ExplainRequest explainRequest = new ExplainRequest("person","id","123");
        QueryBuilder queryBuilder = QueryBuilders.matchQuery("lastname","khan");
        explainRequest = explainRequest.query(queryBuilder);
        ExplainResponse eResponse = client.explain(explainRequest).actionGet();
        System.out.println(eResponse.getExplanation().getDescription());
        return eResponse.getExplanation().getDescription();
    }

public String fieldcap() throws IOException {
        FieldCapabilitiesRequest fRequest = new FieldCapabilitiesRequest();
        fRequest.fields("name").indices("employee","person");
        FieldCapabilitiesResponse fResponse = client.fieldCaps(fRequest).actionGet();
        System.out.println(fResponse.get().toString());
        return fResponse.get().toString();
    }
```

![](666370291.png)

![](666370290.png)

![](666370289.png)

### Delete Operation

``` c#
 public String delete1(@PathVariable final String id) {

        DeleteResponse deleteResponse = client.prepareDelete("employee", "id", id).get();

        System.out.println(deleteResponse.getResult().toString());
        return deleteResponse.getResult().toString();
    }
```

![](666370288.png)

### Jest Elasticsearch Java Client

APIs such as execute and executeAsync are used to perform all the CRUD
operations. To identify which operation to be performed, type of Builder
Request is analyzed.

#### Insert Operation

``` java
public static void main(String[] args) throws IOException {
        // Demo the JestClient
        JestClient jestClient = jestClient();
        // Index a document from String
        ObjectMapper mapper = new ObjectMapper();
        JsonNode employeeJsonNode = mapper.createObjectNode()
                .put("name", "Michael Pratt")
                .put("title", "Java Developer")
                .put("yearsOfService", 2)
                .set("skills", mapper.createArrayNode()
                        .add("java")
                        .add("spring")
                        .add("elasticsearch"));
        jestClient.execute(new Index.Builder(employeeJsonNode.toString()).index("employees").build());
}
```

![](666370287.png)

#### Delete Operation

``` java
public static void main(String[] args) throws IOException {
        // Demo the JestClient
        JestClient jestClient = jestClient();
        // Delete documents
        jestClient.execute(new Delete.Builder("2") .index("employees") .build());
}
```

![](666370286.png)

#### Update Operation

``` java
public static void main(String[] args) throws IOException {
        // Demo the JestClient
        JestClient jestClient = jestClient();
        // Update document
        employee.setYearsOfService(3);
        jestClient.execute(new Update.Builder(employee).index("employees").id("1").build());
}
```

![](666370285.png)

#### Select Operation

``` java
public static void main(String[] args) throws IOException {
        // Demo the JestClient
        JestClient jestClient = jestClient();
        // Read document by ID
        Employee getResult = jestClient.execute(new Get.Builder("employees", "1").build()).getSourceAsObject(Employee.class);
}
```

![](666370284.png)

#### Bulk Operations

``` java
public static void main(String[] args) throws IOException {
        // Demo the JestClient
        JestClient jestClient = jestClient();

        // Bulk operations
        Employee employeeObject1 = new Employee();
        employee.setName("John Smith");
        employee.setTitle("Python Developer");
        employee.setYearsOfService(10);
        employee.setSkills(Arrays.asList("python"));

        Employee employeeObject2 = new Employee();
        employee.setName("Kate Smith");
        employee.setTitle("Senior JavaScript Developer");
        employee.setYearsOfService(10);
        employee.setSkills(Arrays.asList("javascript", "angular"));

        jestClient.execute(new Bulk.Builder().defaultIndex("employees")
                .addAction(new Index.Builder(employeeObject1).build())
                .addAction(new Index.Builder(employeeObject2).build())
                .addAction(new Delete.Builder("3").build()) .build());
}
```

![](666370283.png)

## Known Limitations

-   APIs such as bulk (only for Transport Client, High Level Client) and
    performRequst result in useLinks
-   Bulk API used in Jest Client always results in appropriate CRUD
    links with unknown Index.
-   Multiple clusters used for CRUD operations are not supported. Only
    first found cluster is used.
