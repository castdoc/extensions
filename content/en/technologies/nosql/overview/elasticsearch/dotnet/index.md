---
title: "Support of Elasticsearch for .NET"
linkTitle: "Support of Elasticsearch for .NET"
type: "docs"
---

>CAST supports Eleasticsearch via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about how this support is provided for .NET source
code is discussed below.

## Supported Client Libraries

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a
href="https://www.elastic.co/guide/en/elasticsearch/client/net-api/current/elasticsearch-net.html"
rel="nofollow">Elasticsearch.NET</a></td>
<td class="confluenceTd">7.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><a
href="https://www.elastic.co/guide/en/elasticsearch/client/net-api/current/nest.html"
rel="nofollow">NEST</a></td>
<td class="confluenceTd">7.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table>
<colgroup>
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Scenario</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">Elasticsearch.Net</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Elasticsearch.Net.ElasticLowLevelClient.Index</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.IndexAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Bulk</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.BulkAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Index</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.IndexAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.IndexUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.IndexUsingTypeAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">NEST</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Nest.ElasticClient.Index</p>
<p>Nest.ElasticClient.IndexAsync</p>
<p>Nest.ElasticClient.IndexDocument</p>
<p>Nest.ElasticClient.IndexDocumentAsync</p>
<p>Nest.ElasticClient.Bulk</p>
<p>Nest.ElasticClient.BulkAsync</p>
<p>Nest.ElasticClient.Index</p>
<p>Nest.ElasticClient.IndexAsync</p>
<p>Nest.ElasticClient.IndexDocument</p>
<p>Nest.ElasticClient.IndexDocumentAsync</p>
<p>Nest.ElasticClient.Create</p>
<p>Nest.ElasticClient.CreateAsync</p>
<p>Nest.ElasticClient.CreateUsingType</p>
<p>Nest.ElasticClient.CreateUsingTypeAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">Elasticsearch.Net</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Elasticsearch.Net.ElasticLowLevelClient.Update</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.UpdateAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.UpdateByQuery</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.UpdateByQueryAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Update</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.UpdateAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.UpdateByQuery</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.UpdateByQueryAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">NEST</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Nest.ElasticClient.Update</p>
<p>Nest.ElasticClient.UpdateAsync</p>
<p>Nest.ElasticClient.UpdateByQuery</p>
<p>Nest.ElasticClient.UpdateByQueryAsync</p>
<p>Nest.ElasticClient.Update</p>
<p>Nest.ElasticClient.UpdateAsync</p>
<p>Nest.ElasticClient.UpdateByQuery</p>
<p>Nest.ElasticClient.UpdateByQueryAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">Elasticsearch.Net</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Elasticsearch.Net.ElasticLowLevelClient.Search</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SearchAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DocumentExists</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DocumentExistsAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Explain</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.ExplainAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Count</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.CountAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.CountUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.CountUsingTypeAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DocumentExists</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DocumentExistsAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Explain</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.ExplainAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Get</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.GetAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiGet</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiGetAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiGetUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiGetUsingTypeAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiTermVectors</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiTermVectorsAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiTermVectorsUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.MultiTermVectorsUsingTypeAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Search</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SearchAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SearchMvt</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SearchMvtAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Source</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SourceAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SourceExists</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SourceExistsAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SourceExistsUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SourceExistsUsingTypeAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SourceUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.SourceUsingTypeAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.TermVectors</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.TermVectorsAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.TermVectorsUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.TermVectorsUsingTypeAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">NEST</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Nest.ElasticClient.Search</p>
<p>Nest.ElasticClient.SearchAsync</p>
<p>Nest.ElasticClient.DocumentExists</p>
<p>Nest.ElasticClient.DocumentExistsAsync</p>
<p>Nest.ElasticClient.Explain</p>
<p>Nest.ElasticClient.ExplainAsync</p>
<p>Nest.ElasticClient.Get</p>
<p>Nest.ElasticClient.GetAsync</p>
<p>Nest.ElasticClient.Count</p>
<p>Nest.ElasticClient.CountAsync</p>
<p>Nest.ElasticClient.DocumentExists</p>
<p>Nest.ElasticClient.DocumentExistsAsync</p>
<p>Nest.ElasticClient.Explain</p>
<p>Nest.ElasticClient.ExplainAsync</p>
<p>Nest.ElasticClient.Get</p>
<p>Nest.ElasticClient.GetAsync</p>
<p>Nest.ElasticClient.MultiGet</p>
<p>Nest.ElasticClient.MultiGetAsync</p>
<p>Nest.ElasticClient.MultiSearch</p>
<p>Nest.ElasticClient.MultiSearchAsync</p>
<p>Nest.ElasticClient.MultiSearchTemplate</p>
<p>Nest.ElasticClient.MultiSearchTemplateAsync</p>
<p>Nest.ElasticClient.MultiTermVectors</p>
<p>Nest.ElasticClient.MultiTermVectorsAsync</p>
<p>Nest.ElasticClient.Source</p>
<p>Nest.ElasticClient.SourceAsync</p>
<p>Nest.ElasticClient.SourceExists</p>
<p>Nest.ElasticClient.SourceExistsAsync</p>
<p>Nest.ElasticClient.TermVectors</p>
<p>Nest.ElasticClient.TermVectorsAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd">Elasticsearch.Net</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Elasticsearch.Net.ElasticLowLevelClient.Delete</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQuery</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQueryAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.Delete</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQuery</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQueryAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQueryRethrottle</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQueryRethrottleAsync</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQueryUsingType</p>
<p>Elasticsearch.Net.ElasticLowLevelClient.DeleteByQueryUsingTypeAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">NEST</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Nest.ElasticClient.Delete</p>
<p>Nest.ElasticClient.DeleteAsync</p>
<p>Nest.ElasticClient.DeleteByQuery</p>
<p>Nest.ElasticClient.DeleteByQueryAsync</p>
<p>Nest.ElasticClient.Delete</p>
<p>Nest.ElasticClient.DeleteAsync</p>
<p>Nest.ElasticClient.DeleteByQuery</p>
<p>Nest.ElasticClient.DeleteByQueryAsync</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon </th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370250.png" draggable="false"
data-image-src="666370250.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370250"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370237"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Elasticsearch Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370249.png" draggable="false"
data-image-src="666370249.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370249"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370237"
data-linked-resource-container-version="1" height="34" /></p>
</div></td>
<td class="confluenceTd">DotNet Elasticsearch Index</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370248.png" draggable="false"
data-image-src="666370248.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370248"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370237"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Elasticsearch Index </td>
</tr>
</tbody>
</table>

## Links

<table class="relative-table confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh"> Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd"><p>From DotNet Elasticsearch Index object
to DotNet Elasticsearch Cluster object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Elasticsearch Index objects</td>
<td class="confluenceTd"><p>Bulk</p>
<p>BulkAsync</p>
<p>DefaultIndex</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"> useInsertLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Elasticsearch Index objects</td>
<td class="confluenceTd"><p>Index</p>
<p>IndexAsync</p>
<p>IndexDocument</p>
<p>IndexDocumentAsync</p>
<p>Create</p>
<p>CreateAsync</p>
<p>CreateUsingType</p>
<p>CreateUsingTypeAsync</p>
<p>IndexUsingType</p>
<p>IndexUsingTypeAsync</p>
<p>IndexDocument</p>
<p>Map</p>
<p>MapAsync</p>
<p>IndexDocumentAsync</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"> useDeleteLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Elasticsearch Index objects</td>
<td class="confluenceTd"><p>Delete</p>
<p>DeleteAsync</p>
<p>DeleteByQuery</p>
<p>DeleteByQueryAsync</p>
<p>DeleteByQueryRethrottle</p>
<p>DeleteByQueryRethrottleAsync</p>
<p>DeleteByQueryUsingType</p>
<p>DeleteByQueryUsingTypeAsync</p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>useSelectLink</p></td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Elasticsearch Index objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Search</p>
<p>SearchAsync</p>
<p>DocumentExists</p>
<p>DocumentExistsAsync</p>
<p>Explain</p>
<p>ExplainAsync</p>
<p>Get</p>
<p>GetAsync</p>
<p>Count</p>
<p>CountAsync</p>
<p>CountUsingType</p>
<p>CountUsingTypeAsync</p>
<p>DocumentExists</p>
<p>DocumentExistsAsync</p>
<p>Explain</p>
<p>ExplainAsync</p>
<p>Get</p>
<p>GetAsync</p>
<p>IndexExists</p>
<p>IndexExistsAsync</p>
<p>MultiGet</p>
<p>MultiGetAsync</p>
<p>MultiGetUsingType</p>
<p>MultiGetUsingTypeAsync</p>
<p>MultiTermVectors</p>
<p>MultiTermVectorsAsync</p>
<p>MultiTermVectorsUsingType</p>
<p>MultiTermVectorsUsingTypeAsync</p>
<p>Search</p>
<p>SearchAsync</p>
<p>SearchMvt</p>
<p>SearchMvtAsync</p>
<p>Source</p>
<p>SourceAsync</p>
<p>SourceExists</p>
<p>SourceExistsAsync</p>
<p>SourceUsingType</p>
<p>SourceUsingTypeAsync</p>
<p>TermVectors</p>
<p>TermVectorsAsync</p>
<p>TermVectorsUsingType</p>
<p>TermVectorsUsingTypeAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Elasticsearch Index objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Update</p>
<p>UpdateAsync</p>
<p>UpdateByQuery</p>
<p>UpdateByQueryAsync</p>
<p>UpdateByQueryRethrottle</p>
<p>UpdateByQueryRethrottleAsync</p>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Cluster Creation and Default Index(Elasticsearch.Net and NEST)

``` c#
class HighLevel
{
        public void UsingConnectionPool()
        {            var uris = new[]
                 {
                    new Uri("http://localhost:9200"),
                    new Uri("http://localhost:9201"), 
                   new Uri("http://localhost:9202"), 
                 };
                var connectionPool = new SniffingConnectionPool(uris);
                var settings = new ConnectionSettings(connectionPool)
                        .DefaultIndex("people");
                var client = new ElasticClient(settings);
        }
}
```

![](666370247.jpg)

### Cluster Creation From Configuration File

``` java
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="resizer" type="ImageResizer.ResizerSection,ImageResizer" requirePermission="false" />
    <section name="quartz" type="System.Configuration.NameValueSectionHandler, System, Version=1.0.5000.0,Culture=neutral, PublicKeyToken=b77a5c561934e089" />
    <section name="rockConfig" type="Rock.Configuration.RockConfig, Rock" />
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
  </configSections>
  <appSettings>
    <add key="baseUrl" value="http://127.0.0.1:9200" />
    <add key="AllowDuplicateGroupMembers" value="false" />
    <add key="EnableRedisCacheCluster" value="False" />
    <add key="RedisPassword" value="" />
    <add key="RedisEndpointList" value="" />
    <add key="RedisDatabaseNumber" value="0" />
    <add key="CacheManagerEnableStatistics" value="False" />
  </appSettings>

  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.IdentityModel.Tokens" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.1.0" newVersion="5.2.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security" publicKeyToken="31bf3856ad364e35" culture="neutral" />
          <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.IdentityModel.Protocol.Extensions" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.0.40306.1554" newVersion="1.0.40306.1554" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.AspNetCore.DataProtection.Abstractions" publicKeyToken="adb9793829ddae60" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.codedom>
    <compilers>
      <compiler extension=".cs" language="c#;cs;csharp" warningLevel="4" compilerOptions="/langversion:7.3 /nowarn:1659;1699;1701" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=3.6.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35">
        <providerOption name="UseAspNetSettings" value="false" />
      </compiler>
    </compilers>
  </system.codedom>
</configuration>
```

![](666370246.jpg)

### Insert Operation(Elasticsearch.Net and NEST)

#### IndexDocument

``` c#
public void Indexing()
{
            var person = new Person
            {
                Id = 1,
                FirstName = "Martijn",
                LastName = "Laarman"
            };
            var ndexResponse = client.IndexDocument(person); //<1>
}
```

![](666370245.jpg)

#### IndexDocumentAsync

``` c#
public async Task IndexingAsync()
{            var person = new Person
            {
                Id = 2,
                FirstName = "Jack",
                LastName = "Mas" 
           };
           var asyncIndexResponse = await client.IndexDocumentAsync(person); 
}

```

![](666370244.jpg)

### Update Operation(Elasticsearch.Net and NEST)

``` c#
public static bool updateDocument(string searchID, string first_name, string last_name)
{
        bool status;
             //Update by Partial Document
               var response = client.Update<DocumentAttributes, UpdateDocumentAttributes>(searchID, d => d
                .Index(index)
                .Doc(new UpdateDocumentAttributes
                {
                    FirstName = first_name,
                    LastName = last_name
                 }));            
}
```

![](666370243.jpg)

### Select Operation(Elasticsearch.Net and NEST)

``` c#
public void SearchingOnDefaultIndex()        
{
    var searchResponse = client.Search<Person>(s => s
                .From(0)
                .Size(10)
                .Query(q => q
                     .Match(m => m
                        .Field(f => f.FirstName)
                        .Query("Martijn")
                     )
                )
            );
            var people = searchResponse.Documents;        
}
```

![](666370242.jpg)

![](666370241.jpg)

### Delete Operation(Elasticsearch.Net and NEST)

``` c#
public static bool deleteDocument(string index, string searchID)        
{
        bool status;
             var response = client.Delete<DocumentAttributes>(searchID, d => d
                 .Index(index));
        if (response.IsValid)
        {
                 status = true;
             }
           else
           {
                    status = false;
               }
            return status;
}   
```

![](666370240.jpg)

### Bulk Operations(Elasticsearch.Net and NEST)

``` c#
public void BulkOperations()        
{
            var people = new object[]
            {
                new { index = new { _index = "people", _type = "person", _id = "1"  }},
                new { FirstName = "Martijn", LastName = "Laarman" },
                new { index = new { _index = "people", _type = "person", _id = "2"  }},
                new { FirstName = "Greg", LastName = "Marzouka" },
                new { index = new { _index = "people", _type = "person", _id = "3"  }},
                new { FirstName = "Russ", LastName = "Cam" },
             };
            var ndexResponse = lowlevelClient.Bulk<StringResponse>(PostData.MultiJson(people));
            string responseStream = ndexResponse.Body;
}
```

![](666370239.jpg)

### Bulk Operations Using Fluent DSL

``` c#
public void BulkOperationsUsingFluentDSL ()
{
            var result = client.Bulk(b => b
            .Index<Person>(i => i
                .Document(new Person {Id = 2})
             )   
             .Create<Person>(c => c
                .Document(new Person { Id = 3 })
            )
            .Delete<Person>(d => d
                .Document(new Person { Id = 4 })
            ));
}
```

![](666370238.jpg)

## Known Limitations

-   If index names are not resolved, the links are created between
    methods and default or unknown index objects