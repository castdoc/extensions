---
title: "Support of Elasticsearch for Spring Data"
linkTitle: "Support of Elasticsearch for Spring Data"
type: "docs"
---

>CAST supports Elasticsearch via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about how this support is provided for Java with Spring Data source code is discussed below.

## Supported Client Libraries

| Library | Version | Supported |
|---|---|:-:|
| [spring-data-elasticsearch](https://docs.spring.io/spring-data/elasticsearch/docs/5.4.2/api/) | Up to: 5.4.2 | :white_check_mark: |
| [spring-data-jest](https://mvnrepository.com/artifact/com.github.vanroy/spring-data-jest/3.3.1.RELEASE) | Up to: 3.3.1 | :white_check_mark: |

## Supported Operations

<table>
<colgroup>
<col/>
<col/>
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><li>Operation</li></th>
<th class="confluenceTh" style="text-align: left;"><li>Methods
Supported</li></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>com.github.vanroy.springdata.jest.JestElasticsearchTemplate</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.prepareIndex</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.putMapping</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.createIndex</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.createIndexIfNotCreated</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.createIndexWithSettings</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.prepareIndex</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.putMapping</li>
</ul>
</div>
</details></div>
<div id="expander-1998111564" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core</summary>
<div id="expander-content-1998111564" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.saveAll</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.saveAll</li>
<li>org.springframework.data.elasticsearch.core.IndexOperations.create</li>
<li>org.springframework.data.elasticsearch.core.IndexOperations.putMapping</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.save</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.save</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.save</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.save</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.saveAll</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.saveAll</li>
<li>org.springframework.data.elasticsearch.core.ReactiveIndexOperations.create</li>
<li>org.springframework.data.elasticsearch.core.ReactiveIndexOperations.putMapping</li>
</ul>
</div>
</details></div>
<div id="expander-1998111564" class="expand-container">
<details><summary>org.springframework.data.repository</summary>
<div id="expander-content-1998111564" class="expand-content">
<ul>
<li>org.springframework.data.repository.CrudRepository.save</li>
<li>org.springframework.data.repository.CrudRepository.saveAll</li>
<li>org.springframework.data.repository.ElasticsearchRepository.save</li>
<li>org.springframework.data.repository.ElasticsearchRepository.saveAll</li>
</ul>
</div>
</details></div>
<div id="expander-1998111564" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.repository.support</summary>
<div id="expander-content-1998111564" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.save</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.saveAll</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleReactiveElasticsearchRepository.save</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleReactiveElasticsearchRepository.saveAll</li>
</ul>
</div>
</details></div>
<ul>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchIndicesClient.create</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.Indices.createIndex</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111565" class="expand-container">
<details><summary>org.springframework.data.repository.CrudRepository</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>org.springframework.data.repository.CrudRepository.existsById</li>
<li>org.springframework.data.repository.CrudRepository.findAll</li>
<li>org.springframework.data.repository.CrudRepository.findById</li>
<li>org.springframework.data.repository.CrudRepository.findAllById</li>
<li>org.springframework.data.repository.CrudRepository.count</li>
</ul>
</div>
</details></div>
<div id="expander-1998111566" class="expand-container">
<details><summary>org.springframework.data.repository.ElasticsearchRepository</summary>
<div id="expander-content-1998111566" class="expand-content">
<ul>
<li>org.springframework.data.repository.ElasticsearchRepository.existsById</li>
<li>org.springframework.data.repository.ElasticsearchRepository.findAll</li>
<li>org.springframework.data.repository.ElasticsearchRepository.findById</li>
<li>org.springframework.data.repository.ElasticsearchRepository.findAllById</li>
<li>org.springframework.data.repository.ElasticsearchRepository.count</li>
<li>org.springframework.data.repository.ElasticsearchRepository.searchSimilar</li>
</ul>
</div>
</details></div>
<div id="expander-1998111567" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository</summary>
<div id="expander-content-1998111567" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.findAll</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.existsById</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.count</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.findById</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.findAllById</li>
</ul>
</div>
</details></div>
<div id="expander-1998111568" class="expand-container">
<details><summary>org.springframework.data.repository.reactive.ReactiveCrudRepository</summary>
<div id="expander-content-1998111568" class="expand-content">
<ul>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.count</li>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.existsById</li>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.findAll</li>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.findAllById</li>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.findById</li>
</ul>
</div>
</details></div>
<div id="expander-1998111569" class="expand-container">
<details><summary>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository</summary>
<div id="expander-content-1998111569" class="expand-content">
<ul>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.count</li>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.existsById</li>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.findAll</li>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.findAllById</li>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.findById</li>
</ul>
</div>
</details></div>
<div id="expander-1998111570" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate</summary>
<div id="expander-content-1998111570" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.count</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.exists</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.get</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.multiGet</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.multiSearch</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.search</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.searchForStream</li>
</ul>
</div>
</details></div>
<div id="expander-1998111571" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate</summary>
<div id="expander-content-1998111571" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.count</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.exists</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.get</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.multiGet</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.multiSearch</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.search</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.searchForStream</li>
</ul>
</div>
</details></div>
<div id="expander-1998111572" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.SearchOperations</summary>
<div id="expander-content-1998111572" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.SearchOperations.count</li>
<li>org.springframework.data.elasticsearch.core.SearchOperations.multiSearch</li>
<li>org.springframework.data.elasticsearch.core.SearchOperations.search</li>
<li>org.springframework.data.elasticsearch.core.SearchOperations.searchForStream</li>
<li>org.springframework.data.elasticsearch.core.SearchOperations.searchOne</li>
</ul>
</div>
</details></div>
<div id="expander-1998111573" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient</summary>
<div id="expander-content-1998111573" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.count</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.exists</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.get</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.search</li>
</ul>
</div>
</details></div>
<div id="expander-1998111573" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient</summary>
<div id="expander-content-1998111573" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.count</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.exists</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.get</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.search</li>
</ul>
</div>
</details></div>
<div id="expander-1998111574" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations</summary>
<div id="expander-content-1998111574" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.count</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.findById</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.exists</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.find</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.searchOne</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.search</li>
</ul>
</div>
</details></div>
<div id="expander-1998111575" class="expand-container">
<details><summary>com.github.vanroy.springdata.jest.JestElasticsearchTemplate</summary>
<div id="expander-content-1998111575" class="expand-content">
<ul>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.getMapping</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.getMultiResponse</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.getPersistentEntityFor</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.getSetting</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.indexExists</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.multiGet</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.prepareScroll</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.prepareQuery</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.prepareSearch</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.query</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.queryForAlias</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.queryForIds</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.queryForList</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.queryForObject</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.queryForPage</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.refresh</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.retrieveIndexNameFromPersistentEntity</li>
</ul>
</div>
</details></div>
<div id="expander-1998111576" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.ElasticsearchTemplate</summary>
<div id="expander-content-1998111576" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.indexOps</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.count</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.get</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.multiGet</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.search</li>
</ul>
</div>
</details></div>
<div id="expander-1998111577" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate</summary>
<div id="expander-content-1998111577" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.indexOps</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.getIndexCoordinatesFor</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.search</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.count</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.aggregate</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.exists</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.get</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.multiGet</li>
</ul>
</div>
</details></div>
<div id="expander-1998111565" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.ReactiveSearchOperations.count</li>
<li>org.springframework.data.elasticsearch.core.ReactiveSearchOperations.search</li>
<li>org.springframework.data.elasticsearch.core.ReactiveSearchOperations.searchForHits</li>
<li>org.springframework.data.elasticsearch.core.ReactiveSearchOperations.searchForPage</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.exists</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.get</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.multiget</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchOperations.indexOps</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchOperations.getIndexCoordinatesFor</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.indexOps</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.getIndexCoordinatesFor</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate.indexOps</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate.count</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate.get</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate.multiGet</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate.search</li>
<li>org.springframework.data.elasticsearch.core.DocumentOperations.exists</li>
<li>org.springframework.data.elasticsearch.core.DocumentOperations.get</li>
<li>org.springframework.data.elasticsearch.core.DocumentOperations.multiGet</li>
<li>org.springframework.data.elasticsearch.core.ReactiveIndexOperations.exists</li>
<li>org.springframework.data.elasticsearch.core.ReactiveIndexOperations.refresh</li>
<li>org.springframework.data.elasticsearch.core.ReactiveIndexOperations.getMapping</li>
</ul>
</div>
</details></div>
<div id="expander-1998111577" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.client</summary>
<div id="expander-content-1998111577" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchIndicesClient.exists</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchIndicesClient.get</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchIndicesClient.refresh</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.Indices.existsIndex</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.Indices.refreshIndex</li>
</ul>
</div>
</details></div>
<ul>
<li>org.springframework.data.repository.PagingAndSortingRepository.findAll</li>
<li>org.springframework.data.repository.reactive.ReactiveSortingRepository.findAll</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111578" class="expand-container">
<details><summary>com.github.vanroy.springdata.jest.JestElasticsearchTemplate</summary>
<div id="expander-content-1998111578" class="expand-content">
<ul>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.bulkUpdate</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.prepareUpdate</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.update</li>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.bulkIndex</li>
</ul>
</div>
</details></div>
<div id="expander-1998111579" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.client</summary>
<div id="expander-content-1998111579" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.bulk</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.index</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.update</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.bulk</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.index</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.update</li>
</ul>
</div>
</details></div>
<div id="expander-1998111580" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate</summary>
<div id="expander-content-1998111580" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.bulkUpdate</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.bulkIndex</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.index</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.update</li>
</ul>
</div>
</details></div>
<div id="expander-1998111581" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate</summary>
<div id="expander-content-1998111581" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.bulkUpdate</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.bulkIndex</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.index</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.update</li>
</ul>
</div>
</details></div>
<ul>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.update</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.updateByQuery</li>
<li>org.springframework.data.elasticsearch.core.DocumentOperations.bulkIndex</li>
<li>org.springframework.data.elasticsearch.core.DocumentOperations.bulkUpdate</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111582" class="expand-container">
<details><summary>org.springframework.data.repository.CrudRepository</summary>
<div id="expander-content-1998111582" class="expand-content">
<ul>
<li>org.springframework.data.repository.CrudRepository.delete</li>
<li>org.springframework.data.repository.CrudRepository.deleteById</li>
<li>org.springframework.data.repository.CrudRepository.deleteAllById</li>
<li>org.springframework.data.repository.CrudRepository.deleteAll</li>
</ul>
</div>
</details></div>

<div id="expander-1998111583" class="expand-container">
<details><summary>org.springframework.data.repository.ElasticsearchRepositoryy</summary>
<div id="expander-content-1998111583" class="expand-content">
<ul>
<li>org.springframework.data.repository.ElasticsearchRepository.delete</li>
<li>org.springframework.data.repository.ElasticsearchRepository.deleteById</li>
<li>org.springframework.data.repository.ElasticsearchRepository.deleteAllById</li>
<li>org.springframework.data.repository.ElasticsearchRepository.deleteAll</li>
</ul>
</div>
</details></div>
<div id="expander-1998111584" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository</summary>
<div id="expander-content-1998111584" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.delete</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.deleteById</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleElasticsearchRepository.deleteAll</li>
</ul>
</div>
</details></div>
<div id="expander-1998111585" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.repository.support.SimpleReactiveElasticsearchRepository</summary>
<div id="expander-content-1998111585" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.repository.support.SimpleReactiveElasticsearchRepository.delete</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleReactiveElasticsearchRepository.deleteById</li>
<li>org.springframework.data.elasticsearch.repository.support.SimpleReactiveElasticsearchRepository.deleteAll</li>
</ul>
</div>
</details></div>
<div id="expander-1998111586" class="expand-container">
<details><summary>org.springframework.data.repository.reactive.ReactiveCrudRepository</summary>
<div id="expander-content-1998111586" class="expand-content">
<ul>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.delete</li>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.deleteAll</li>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.deleteById</li>
<li>org.springframework.data.repository.reactive.ReactiveCrudRepository.deleteAllById</li>
</ul>
</div>
</details></div>
<div id="expander-1998111587" class="expand-container">
<details><summary>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository</summary>
<div id="expander-content-1998111587" class="expand-content">
<ul>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.delete</li>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.deleteAll</li>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.deleteById</li>
<li>org.springframework.data.repository.reactive.ReactiveElasticsearchRepository.deleteAllById</li>
</ul>
</div>
</details></div>
<div id="expander-1998111579" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.core</summary>
<div id="expander-content-1998111579" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.DocumentOperations.delete</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.delete</li>
<li>org.springframework.data.elasticsearch.core.IndexOperations.delete</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.deleteIndex</li>
<li>org.springframework.data.elasticsearch.core.AbstractElasticsearchTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.AbstractReactiveElasticsearchTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.delete</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.deleteById</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchOperations.deleteBy</li>
<li>org.springframework.data.elasticsearch.core.ReactiveDocumentOperations.delete</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate.delete</li>
<li>org.springframework.data.elasticsearch.core.DocumentOperations.delete</li>
<li>org.springframework.data.elasticsearch.core.ReactiveIndexOperations.delete</li>
</ul>
</div>
</details></div>
<div id="expander-1998111587" class="expand-container">
<details><summary>org.springframework.data.elasticsearch.client</summary>
<div id="expander-content-1998111587" class="expand-content">
<ul>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.delete</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient.deleteByQuery</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.delete</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.deleteByQuery</li>
<li>org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchIndicesClient.delete</li>
<li>org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient.Indices.deleteIndex</li>
</ul>
</div>
</details></div>
<ul>
<li>com.github.vanroy.springdata.jest.JestElasticsearchTemplate.delete</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon </th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370382.png" draggable="false"
data-image-src="666370382.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370382"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="cluster.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370372"
data-linked-resource-container-version="1" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Elasticsearch Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370381.png" draggable="false"
data-image-src="666370381.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370381"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370372"
data-linked-resource-container-version="1" height="34" /></li>
</div></td>
<td class="confluenceTd">Java Elasticsearch Index</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370380.png" draggable="false"
data-image-src="666370380.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370380"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370372"
data-linked-resource-container-version="1" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Unknown Elasticsearch Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="666370379.png" draggable="false"
data-image-src="666370379.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370379"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370372"
data-linked-resource-container-version="1" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Unknown Elasticsearch Index</td>
</tr>
</tbody>
</table>

## Links

All links are created between Elasticsearch Cluster object and
Elasticsearch Index objects:

<table class="relative-table confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh"> Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">parentLink</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><li>delete</li>
<li>deleteAll</li>
<li>deleteById</li>
<li>deleteAllById</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><li>save</li>
<li>saveAll</li></td>
</tr>
<tr class="even">
<td class="confluenceTd"><li>useSelectLink</li></td>
<td class="confluenceTd"><li>findAllById</li>
<li>findAll</li>
<li>findById</li>
<li>count</li>
<li>existsById</li>
<li>searchSimilar</li>
<li>indexOps</li>
<li>get</li>
<li>multiGet</li>
<li>search</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>bulkUpdate</li>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Cluster and Index Creation

``` java
public class EsConfig {


    @Autowired
    private EsSinkProperties properties;


    @Bean
    public Client client() throws Exception {

        Settings esSettings = Settings.builder()
                .put("cluster.name", properties.getClusterName())
                .build();

        TransportClient client = new PreBuiltTransportClient(esSettings)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(properties.getHost()), Integer.parseInt(properties.getPort())));

     }
}
```

``` java
public class EsSinkProperties {

    /
     * Elasticsearch cluster name.
     */
    private String clusterName = "elasticsearch";

    /
     * Elasticsearch host name.
     */
    private String host = "localhost";

    /
     * Elasticsearch native port.
     */
    private String port = "9300";

    @NotBlank
    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }
}
```

``` java
@Document(indexName = "trader", type = "trade")
public class Trade {

    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
```

![](666370378.png)

### Insert Operation

``` c#
public override void Configure(EntityTypeBuilder<Contractor> builder)
        {
            builder.ToTable("Trial");
            builder.OwnsOne(m => m.Name, a =>
            {
                a.Property(p => p.FirstName).HasMaxLength(300)
                    .HasColumnName("FirstName")
                    .HasDefaultValue("");
                a.Property(p => p.LastName).HasMaxLength(300)
                    .HasColumnName("LastName")
                    .HasDefaultValue("");
                a.Ignore(p => p.FullName);
            });
```

``` java
public Book save(Book book) {
        return bookRepository.save(book);
    }
```

![](666370377.png)

### Delete Operation

``` java
public void delete(Book book) {
        bookRepository.delete(book);
    }
```

![](666370376.png)

### Select Operation

``` java
public Iterable<Book> findAll() {
        return bookRepository.findAll();
    }
```

![](666370375.png)

### Query Methods

``` java
public interface BookRepository extends ElasticsearchRepository<Book, String> {

    Page<Book> findByAuthor(String author, Pageable pageable);

    List<Book> findByTitle(String title);

}
```

``` java
public Page<Book> findByAuthor(String author, PageRequest pageRequest) {
        return bookRepository.findByAuthor(author, pageRequest);
    }
```

![](666370374.png)

### Elasticsearch Operations / Elasticsearch Template

``` java
public void deleteIndex() {
        operations.indexOps(Conference.class).delete();
}
```
![](666370373.png)

### ReactiveElasticsearchClient

```java
import org.springframework.data.elasticsearch.client.elc.ReactiveElasticsearchClient;

@Repository
public class ElasticRepository {

    public static final String COURSE_INDEX = "course-store";
    private final ReactiveElasticsearchClient client;
    private final ObjectMapper objectMapper;

    public ElasticRepository(ReactiveElasticsearchClient client, ObjectMapper objectMapper) {
        this.client = client;
        this.objectMapper = objectMapper;
    }

    public Mono<DeleteResponse> deleteCourse() {
        return client.delete(deleteRequest -> deleteRequest.index(COURSE_INDEX));
    }
}


```
![](spring_es.png)

## Known Limitations

-   Index is created as unknown, if the name is not retrieved from the
    properties file or if the name could not be resolved.
-   Limited support for Spring Data Elasticsearch 3.x
    -   In 3.x, CRUD operations performed using ElasticsearchRepository
        are supported
    -   In 3.x no support for CRUD operations performed using
        ElasticsearchTemplate. However, if the user configures version
        3.x jars inside its class path, then ElasticsearchTemplate will
        produce links.
