---
title: "Support of AWS DocumentDB for .NET"
linkTitle: "Support of AWS DocumentDB for .NET"
type: "docs"
---

>CAST supports Amazon DocumentDB via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. As Amazon DocumentDB is MongoDB compatible, supported
operations, objects and links are identical to those listed in [MongoDB](../../mongodb/dotnet/).

## Supported Amazon DocumentDB versions

| Version | Supported                                                                                  |
|---------|--------------------------------------------------------------------------------------------|
| 4.0     | ![(tick)](/images/icons/emoticons/check.svg) |
| 3.6     | ![(tick)](/images/icons/emoticons/check.svg) |

## Amazon DocumentDB Identification

The below mentioned objects in MongoDB have properties to indicate
whether the object is of type MongoDB or Amazon DocumentDB:

| Object                            | Type of Property Value | Possible Value(s) | Meaning                                            |
|-----------------------------------|------------------------|-------------------|----------------------------------------------------|
| DotNet MongoDB collection         | Integer                | 1                 | The collection object is of type Amazon DocumentDB |
| DotNet MongoDB collection         | Integer                | 0                 | The collection object is of type MongoDB           |
| DotNet unknown MongoDB collection | Integer                | 1                 | The collection object is of type Amazon DocumentDB |
| DotNet unknown MongoDB collection | Integer                | 0                 | The collection object is of type MongoDB           |

## What results can you expect?

``` c#
class Program
    {
       static void Main(string[] args)
        {
           string template = "mongodb://{0}:{1}@{2}/?replicaSet=rs0&readpreference={3}";
           string username = "root";
           string password = "rootroot";
           string clusterEndpoint = "sample-cluster.cluster-cqjaxx9hbhi0.us-west-2.docdb.amazonaws.com:27017";
           string readPreference = "secondaryPreferred";

           string connectionString = String.Format(template, username, password, clusterEndpoint, readPreference);
           var settings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
           var client = new MongoClient(settings);

           var database = client.GetDatabase("library");
           var collection = database.GetCollection<BsonDocument>("Books");

           var docToInsert = new BsonDocument { { "title", "Harry Potter" } };
           collection.InsertOne(docToInsert);
        }
    }
```

![](609517578.png)

For detailed information on results related to objects and links
see MongoDB.

## Known Limitations

-   Setting of properties related to Amazon DocumentDB identification
    depends on proper resolution of connection string.
