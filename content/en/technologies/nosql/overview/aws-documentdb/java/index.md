---
title: "Support of AWS DocumentDB for Java"
linkTitle: "Support of AWS DocumentDB for Java"
type: "docs"
---

>CAST supports Amazon DocumentDB via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. As Amazon DocumentDB is MongoDB compatible, supported
operations, objects and links are identical to those listed in [MongoDB](../../mongodb/java/).

## Supported Amazon DocumentDB versions

|                   Library                   |  Version   | Supported                                                                                  |
|:-------------------------------------------:|:----------:|--------------------------------------------------------------------------------------------|
| [Amazon Document Db](https://docs.aws.amazon.com/documentdb/) | Up to: 4.0 | ![(tick)](/images/icons/emoticons/check.svg) |

## Amazon DocumentDB Identification

The below mentioned objects in MongoDB have properties to indicate
whether the object is of type MongoDB or Amazon DocumentDB:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Object</p></th>
<th class="confluenceTh"><p>Type of Property Value</p></th>
<th class="confluenceTh"><p>Possible Value(s)</p></th>
<th class="confluenceTh"><p>Meaning</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Java MongoDB collection</td>
<td class="confluenceTd">Integer</td>
<td class="confluenceTd">1</td>
<td class="confluenceTd"><p>The collection object is of type Amazon DocumentDB</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Java MongoDB collection</td>
<td class="confluenceTd">Integer</td>
<td class="confluenceTd">0</td>
<td class="confluenceTd">The collection object is of type MongoDB</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Java unknown MongoDB collection</td>
<td class="confluenceTd">Integer</td>
<td class="confluenceTd">1</td>
<td class="confluenceTd"><p>The collection object is of type Amazon DocumentDB</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Java unknown MongoDB collection</td>
<td class="confluenceTd">Integer</td>
<td class="confluenceTd">0</td>
<td class="confluenceTd">The collection object is of type MongoDB</td>
</tr>
</tbody>
</table>

## What results can you expect?

``` java
package com.sample.app;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ServerAddress;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import org.bson.Document;


public final class App {
    private App() {
    }
    public static void main(String[] args) {

        String connectionString = "mongodb://root:rootroot@sample-cluster.cluster-cqjaxx9hbhi0.us-west-2.docdb.amazonaws.com:27017/?replicaSet=rs0&readpreference=secondaryPreferred";

        MongoClientURI clientURI = new MongoClientURI(connectionString);
        MongoClient mongoClient = new MongoClient(clientURI);

        MongoDatabase testDB = mongoClient.getDatabase("library");
        MongoCollection<Document> numbersCollection = testDB.getCollection("books");

        Document doc = new Document("name", "title").append("value", "Harry Potter");
        numbersCollection.insertOne(doc);

        MongoCursor<Document> cursor = numbersCollection.find().iterator();
        try {
            while (cursor.hasNext()) {
                System.out.println(cursor.next().toJson());
            }
        } finally {
            cursor.close();
        }

    }
}
                    
```

![](607223836.png)

For detailed information on results related to objects and links
see MongoDB.

## Known Limitations

-   Setting of properties related to Amazon DocumentDB identification
    depends on proper resolution of connection string.
