---
title: "Support of Memcached for Node.js"
linkTitle: "Support of Memcached for Node.js"
type: "docs"
---

>CAST supports Memcached via
its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
extension. Details about how this support is provided for .Node.js
source code is discussed below.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Metamodel Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="343736422.png" draggable="false"
data-image-src="343736422.png"
data-unresolved-comment-count="0" data-linked-resource-id="343736422"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_Memcached_Value.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="343736421"
data-linked-resource-container-version="3" width="32" /></p>
</div></td>
<td class="confluenceTd">NodeJS Memcached Value</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="343736423.png" draggable="false"
data-image-src="343736423.png"
data-unresolved-comment-count="0" data-linked-resource-id="343736423"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_Memcached_Connection.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="343736421"
data-linked-resource-container-version="3" width="32" /></p>
</div></td>
<td class="confluenceTd">NodeJS Memcached Connection</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Function</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useInsertLink</td>
<td class="confluenceTd"><p>touch</p>
<p>set</p>
<p>add</p>
<p>cas</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>del</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>get</p>
<p>gets</p>
<p>getMulti</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>replace</p>
<p>append</p>
<p>prepend</p>
<p>incr</p>
<p>decr</p></td>
</tr>
</tbody>
</table>

## Code samples

The following declarations will create a connection to the server
'192.168.0.102:11211':

``` java
var Memcached = require('memcached');
var memcached = new Memcached('192.168.0.102:11211');
```

This statement will create a useInsertLink from code to the value "foo":

``` java
memcached.add('foo', 'bar', 10, function (err) { /* stuff */ });
```

This statement will create a useDeleteLink from code to the value "foo":

``` java
memcached.del('foo', function (err) { /* stuff */ });
```

For example:

![Alt text](343736424.png)
