---
title: "Support of Memcached for .NET"
linkTitle: "Support of Memcached for .NET"
type: "docs"
---

>CAST supports Memcached via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about the support provided for .NET source code is
discussed below.

## Supported Client Libraries

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Library</p></th>
<th class="confluenceTh"><p>Version</p></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p><a
href="https://github.com/enyim/EnyimMemcached"
rel="nofollow">EnyimMemcached</a> </p></td>
<td class="confluenceTd">1.4.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p><a
href="http://blog.smarx.com/posts/memcached-in-windows-azure"
rel="nofollow">WazMemcachedServer</a></p></td>
<td class="confluenceTd">1.x.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p><a
href="http://blog.smarx.com/posts/memcached-in-windows-azure"
rel="nofollow">WazMemcachedClient</a></p></td>
<td class="confluenceTd">1.x.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><a href="https://cachemanager.michaco.net/"
rel="nofollow">CacheManager</a></td>
<td class="confluenceTd">1.2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><a
href="https://www.hanselman.com/blog/using-lazycache-for-clean-and-simple-net-core-inmemory-caching"
rel="nofollow">LazyCache</a></td>
<td class="confluenceTd">2.4.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</tr>
</tbody>
</table>

## Supported Operations

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Scenario</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">Enyim Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Enyim.Caching.MemcachedClient.Get</p>
<p>Enyim.Caching.MemcachedClient.GetAsync</p>
<p>Enyim.Caching.MemcachedClient.ExecuteGet</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">CacheManager Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CacheManager.Core.ICache.Get</p>
<p>CacheManager.Core.ICache.Exists</p>
<p>CacheManager.Core.ICache.Expire</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">LazyCache Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>LazyCache.AppCacheExtensions.GetOrAdd</p>
<p>LazyCache.AppCacheExtensions.GetOrAddAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">Enyim Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Enyim.Caching.MemcachedClient.Store</p>
<p>Enyim.Caching.MemcachedClient.StoreAsync</p>
<p>Enyim.Caching.MemcachedClient.ExecuteStore</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">CacheManager Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CacheManager.Core.ICacheManager.GetOrAdd</p>
<p>CacheManager.Core.ICacheManager.TryGetOrAdd</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">LazyCache Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>LazyCache.AppCacheExtensions.GetOrAdd</p>
<p>LazyCache.AppCacheExtensions.GetOrAddAsync</p>
<p>LazyCache.AppCacheExtensions.Add</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">Enyim Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Enyim.Caching.MemcachedClient.Remove</p>
<p>Enyim.Caching.MemcachedClient.RemoveAsync</p>
<p>Enyim.Caching.MemcachedClient.ExecuteRemove</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">CacheManager Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CacheManager.Core.ICache.Remove</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">LazyCache Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>LazyCache.IAppCache.Remove</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">Enyim Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Enyim.Caching.MemcachedClient.Increment</p>
<p>Enyim.Caching.MemcachedClient.IncrementAsync</p>
<p>Enyim.Caching.MemcachedClient.ExecuteIncrement</p>
<p>Enyim.Caching.MemcachedClient.Decrement</p>
<p>Enyim.Caching.MemcachedClient.DecrementAsync</p>
<p>Enyim.Caching.MemcachedClient.ExecuteDecrement</p>
<p>Enyim.Caching.MemcachedClient.Append</p>
<p>Enyim.Caching.MemcachedClient.AppendAsync</p>
<p>Enyim.Caching.MemcachedClient.PrependAsync</p>
<p>Enyim.Caching.MemcachedClient.ExecuteAppend</p>
<p>Enyim.Caching.MemcachedClient.Prepend</p>
<p>Enyim.Caching.MemcachedClient.ExecutePrepend</p>
<p>Enyim.Caching.MemcachedClient.Replace</p>
<p>Enyim.Caching.MemcachedClient.ReplaceAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">CacheManager Library</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CacheManager.Core.ICache.Put</p>
<p>CacheManager.Core.ICacheManager.Update</p>
<p>CacheManager.Core.ICacheManager.TryUpdate</p>
<p>CacheManager.Core.ICacheManager.AddOrUpdate</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370454.png"/></p>
</div></td>
<td class="confluenceTd">DotNet Memcached Connection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370453.png"/></p>
</div></td>
<td class="confluenceTd">DotNet Memcached Data</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370452.png" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Memcached Connection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370451.png" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Memcached Data</td>
</tr>
</tbody>
</table>

## Links

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><strong>Link
type</strong></th>
<th class="confluenceTh" style="text-align: left;">Source and
destination of link</th>
<th class="confluenceTh" style="text-align: left;"> Methods
supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">belongsTo</td>
<td class="confluenceTd" style="text-align: left;"><p>From DotNet
Memcached Data object to DotNet Memcached connection object</p></td>
<td class="confluenceTd" style="text-align: left;">-</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useSelectLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
.Net method object and DotNet Memcached Data object</td>
<td class="confluenceTd" style="text-align: left;"><p>Get</p>
<p>GetAsync</p>
<p>ExecuteGet</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useInsertLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
.Net method object and DotNet Memcached Data object</td>
<td class="confluenceTd" style="text-align: left;"><p>Store</p>
<p>StoreAsync</p>
<p>ExecuteStore</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>useDeleteLink</p></td>
<td class="confluenceTd" style="text-align: left;">Between the caller
.Net method object and DotNet Memcached Data object</td>
<td class="confluenceTd" style="text-align: left;"><p>Remove</p>
<p>RemoveAsync</p>
<p>ExecuteRemove</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useUpdateLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
.Net method object and DotNet Memcached Data object</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p>Increment</p>
<p>IncrementAsync</p>
<p>ExecuteIncrement</p>
<p>Decrement</p>
<p>DecrementAsync</p>
<p>ExecuteDecrement</p>
<p>Append</p>
<p>AppendAsync</p>
<p>ExecuteAppend</p>
<p>Prepend</p>
<p>PrependAsync</p>
<p>ExecutePrepend</p>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Memcached Client Configuration

In the Memcached class file, when we add the server information by
parsing the ipaddress and provide the port Number:

``` c#
public MemcacedCache()
      {
         _logger = new Logger();
         var config = new MemcachedClientConfiguration();
         config.Servers.Add(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 11211));
         config.Protocol = MemcachedProtocol.Binary;
         
         _cache = new MemcachedClient(null, config);
      }
```

![](666370450.png)

In the Memcached class file, when we pass the server information as a
parameter:

``` c#
namespace MemcachedTryout
{
   internal class MemcacedCache : ICache
   {  const string MEMCACHE_HOST = "127.0.0.1";
      const int MEMCACHE_PORT = 14551;
      public MemcacedCache()
      {
        _logger = new Logger();
         var config = new MemcachedClientConfiguration();
         config.Servers.Add(GetIPEndPointFromHostName(MEMCACHE_HOST, MEMCACHE_PORT));
         _cache = new MemcachedClient(null, config);
      }
  ---
}
```

![](666370449.png)

If the server information is provided in xml file:

``` java
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
  <configSections>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,log4net" />
    <sectionGroup name="enyim.com">
      <section name="memcached" type="Enyim.Caching.Configuration.MemcachedClientSection, Enyim.Caching" />
    </sectionGroup>
  </configSections>

  <enyim.com>
    <memcached protocol="Binary">
      <servers>
         <!--put your own server(s) here-->
        <add address="127.0.0.1" port="11211" />
      </servers>
    </memcached>
  </enyim.com>

  <log4net>
    <appender name="ConsoleAppender" type="log4net.Appender.ConsoleAppender">
      <layout type="log4net.Layout.PatternLayout,log4net">
        <param name="ConversionPattern" value="%d [%t] %-5p %c %m%n" />
      </layout>
    </appender>
    <root>
      <level value="Debug" />
      <appender-ref ref="ConsoleAppender" />
    </root>
  </log4net>
</configuration>
```

![](666370448.jpg)

### Memcached Unknown Connection Object

In the Memcached class file, when we pass the server information as a
parameter and if passed parameter is unresolved it will create Unknown
Connection Object:

``` c#
namespace MemcachedTryout
{
   internal class MemcacedCache : ICache
   {
      private readonly MemcachedClient _cache;
      private readonly ILogger _logger;

      public MemcacedCache()
      {
         _logger = new Logger();
         var config = new MemcachedClientConfiguration();
         config.Servers.Add(GetIPEndPointFromHostName(MEMCACHE_HOST, MEMCACHE_PORT));     
         _cache = new MemcachedClient(null, config);
      }
 ----
}
```

![](666370447.png)

### WazMemcached Library

``` c#
public string MemcachedRoleName { get; set; }
public string EndpointName { get; set; }
private MemcachedClient Client
{
   get { return _client ?? (_client = WindowsAzureMemcachedHelpers.CreateDefaultClient(MemcachedRoleName, EndpointName)); }
}
```

![](666370446.png)

### Insert Operations

#### Enyim

``` c#
  public async Task<bool> Addsync<T>(string key, T value)
      {
         _logger.Debug("Adding item to cache with key {0}", key);
         return await _cache.Store(StoreMode.Set, "Hello", value, TimeSpan.FromDays(90));
      }
```

![](666370445.jpg)

#### CacheManager

``` c#
public ValuesController(ICacheManager<string> valuesCache, ICacheManager<int> intCache, ICacheManager<DateTime> dates)
{
    _cache = valuesCache;

    dates.Add("now", DateTime.UtcNow);
    intCache.Add("count", 1);
}
```

![](666370444.jpg)

### Select Operations

#### Enyim

``` java
public async Task<T> Getsync<T>(string key)
      {
         var cacheItem = await _cache.Get<T>(Constants.num33);
        
         if (cacheItem == null)
         {
            _logger.Debug("Cache miss with key {0}", key);
            return default(T);
         }
         _logger.Debug("Cache hit with key {0}", key);
         return cacheItem;
      }
```

![](666370443.jpg)

#### CacheManager

``` java
public IActionResult Get(string key)
{
    var value = _cache.GetCacheItem(key);
    if (value == null)
    {
        return NotFound();
    }

    return Json(value.Value);
}
```

![](666370442.jpg)

#### LazyCache

``` java
[HttpGet]
[Route("api/dbtime")]
public DbTimeEntity Get()
{
    Func<DbTimeEntity> actionThatWeWantToCache = () => dbContext.GeDbTime();

    var cachedDatabaseTime = cache.GetOrAdd(cacheKey, actionThatWeWantToCache);

    return cachedDatabaseTime;
}
```

![](666370441.jpg)

### Select Operations

#### Enyim

``` java
public async Task<bool> Removesync(string key)
      {
         _logger.Debug("Removing item from cache with key {0}", key);
         return await _cache.Remove(Constants.key);
      }
```

![](666370440.jpg)

#### CacheManager

``` java
public IActionResult Delete(string key)
{
if (_cache.Remove(key))
{
return Ok();
}

return NotFound();
}
```

![](666370439.jpg)

#### LazyCache

``` java
[HttpDelete]
[Route("api/dbtime")]
public IActionResult DeleteFromCache()
{
cache.Remove(cacheKey);
var friendlyMessage = new {Message = $"Item with key '{cacheKey}' removed from server in-memory cache"};
return Ok(friendlyMessage);
}
```

![](666370438.jpg)

### Update Operations

#### Enyim

``` java
public async Task<bool> Updatesync<T>(string key, T value)
      {
         _cache.Increment("num1", 1, 10);
         
      }
```

![](666370437.jpg)

#### CacheManager

``` java
public IActionResult Put(string key, [FromBody]string value)
{
    if (_cache.AddOrUpdate(key, value, (v) => value) != null)
    {
        return Ok();
    }

    return NotFound();
}
```

![](666370436.jpg)

## Known Limitations

-   In case data is not resolved, Unknown data objects are created.
