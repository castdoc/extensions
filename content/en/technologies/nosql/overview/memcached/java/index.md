---
title: "Support of Memcached for Java"
linkTitle: "Support of Memcached for Java"
type: "docs"
---

>CAST supports Memcached via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about the support provided for Java source code is
discussed below.

## Supported Libraries

| Library                                                                     | Version      | Supported                                    |
|-----------------------------------------------------------------------------|--------------|----------------------------------------------|
| [spymemcached](https://repo1.maven.org/maven2/net/spy/spymemcached/2.12.0/) | Up to: 2.7.1 | ![(tick)](/images/icons/emoticons/check.svg) |

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operations</th>
<th class="confluenceTh">Method Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>net.spy.memcached.MemcachedClient.set</p>
<p>net.spy.memcached.MemcachedClient.add</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>net.spy.memcached.MemcachedClient.incr</p>
<p>net.spy.memcached.MemcachedClient.asyncIncr</p>
<p>net.spy.memcached.MemcachedClient.decr</p>
<p>net.spy.memcached.MemcachedClient.asyncDecr</p>
<p>net.spy.memcached.MemcachedClient.replace</p>
<p>net.spy.memcached.MemcachedClient.append</p>
<p>net.spy.memcached.MemcachedClient.prepend </p>
<p>net.spy.memcached.MemcachedClient.getAndLock</p>
<p>net.spy.memcached.MemcachedClient.getAndTouch</p>
<p>net.spy.memcached.MemcachedClient.asyncGetAndTouch</p>
<p>net.spy.memcached.MemcachedClient.asyncGetAndLock</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>net.spy.memcached.MemcachedClient.get</p>
<p>net.spy.memcached.MemcachedClient.asyncGet</p>
<p>net.spy.memcached.MemcachedClient.getBulk</p>
<p>net.spy.memcached.MemcachedClient.asyncGetBulk</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>net.spy.memcached.MemcachedClient.delete</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370511.png" draggable="false"
data-image-src="666370511.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370511"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Memcached_Connection.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370494"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Java Memcached Connection</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370510.png" draggable="false"
data-image-src="666370510.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370510"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Memcached_Data.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370494"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Java Memcached Data</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370509.png" draggable="false"
data-image-src="666370509.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370509"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Unknown_Memcached_Connection.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370494"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Java Unknown Memcached Connection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370508.png" draggable="false"
data-image-src="666370508.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370508"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Unknown_Memcached_Data.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370494"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Java Unknown Memcached Data</p></td>
</tr>
</tbody>
</table>

## Links

Links are created for transaction and function point needs:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh"> Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd">From Memcached data object to Memcached
connection object</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the java Method objects and Memcached
data object</td>
<td class="confluenceTd"><p>Insert</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the java Method objects and Memcached
data object</td>
<td class="confluenceTd"><p>Increment</p>
<p>Decrement</p>
<p>Append</p>
<p>Prepend </p>
<p>CAS</p>
<p>asyncCAS</p>
<p>getAndLock</p>
<p>getAndTouch</p>
<p>asyncGetAndTouch</p>
<p>asyncGetAndLock</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Between the java Method objects and Memcached
data object</td>
<td class="confluenceTd"><p>Get</p>
<p>GetAsync</p>
<p>getBulk</p>
<p>asyncGetBulk</p>
<p>CAS</p>
<p>asyncCAS</p>
<p>getAndLock</p>
<p>getAndTouch</p>
<p>asyncGetAndTouch</p>
<p>asyncGetAndLock</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the java Method objects and Memcached
data object</td>
<td class="confluenceTd"><p>Remove</p></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Memcached Connection

#### IP address

In the Memcached class file, when we add the server information by
parsing the IP address and provide the port Number:

``` java
public class Connection {
    
    public static void main(String[] args) throws IOException {
         

          MemcachedClient mcc = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
          
    }
}
```

![](666370507.png)

#### Hostname

In the Memcached class file, when we add the set of server information
by parsing the server address and provide the port Number over the
binary connection protocol:

``` java
public class Connection {
    
    public static void main(String[] args) throws IOException {

            MemcachedClient mcc1 = new MemcachedClient(new BinaryConnectionFactory(), AddrUtil.getAddresses
                                ("server1"));
    }
}
```

![](666370506.png)

#### Unknown Connection Object

In the Memcached class file, when we pass the server information as a
parameter but if passed parameter are unresolved:

``` java
  public class Connection {
    
        public static void main(String[] args) throws IOException {

            InetSocketAddress inet2 = new InetSocketAddress(Connection1.var, 11211);
  
             MemcachedClient mcc = new MemcachedClient(inet2);
        }
}
```

![](666370505.png)

### Insert Operation

#### set command

``` java
mcc.set("tp", 900, "memcached").isDone()
```

![](666370504.png)

#### add command

``` java
mcc.add("data3", 900, "redis").isDone()
```

![](666370503.png)

### Select Operation

#### get command

``` java
mcc.get("data2")
```

![](666370502.png)

#### asyncGet command

``` java
mcc.asyncGet("data2")
```

![](666370501.png)

#### getBulk and asyncGetBulk command

``` java
m = mcc1.getBulk(keySet);
vals = mcc1.asyncGetBulk(keySet);
```

![](666370500.png)

### Delete Operation

#### delete command

``` java
mcc.delete("data1").isDone()
```

![](666370499.png)

### Update Operation

#### replace command

``` java
mcc.replace("tp", 900, "redis").isDone()
```

![](666370498.png)

#### append and prepend commands

``` java
mcc.append(900, "tp", "redis").isDone();
mcc.prepend(900, "tp", "redis").isDone();
```

![](666370497.png)

#### Increment command

``` java
// incrementing the numeric value of existing key   
mcc.incr("tp", 2));
mcc.asyncIncr("tp", 2);
```

![](666370496.png)

#### Decrement command

``` java
//decrementing the numeric value of existing key 
mcc.decr("tp", 1));
mcc.asyncDecr("tp", 1);
```

![](666370495.png)

## Known Limitations

-   If the Data name is not resolved in the CRUD API, then the link will
    be created with an unknown Memcached data object
