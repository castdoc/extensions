---
title: "Support of Apache Lucene for Java"
linkTitle: "Support of Apache Lucene for Java"
type: "docs"
---

## Supported Libraries

| Library | Version | Supported |
|---|---|:---:|
| Apache Lucene | up to: 9.6.0 | :white_check_mark: |

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh" style="text-align: left;">Methods
Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>org.apache.lucene.index.IndexWriter.addDocument</li>
<li>org.apache.lucene.index.IndexWriter.addDocuments</li>
<li>org.apache.lucene.index.IndexWriter.addIndexes</li>
<li>org.apache.lucene.search.IndexSearcher.setQueryCache</li>
<li>org.apache.lucene.index.IndexWriter.commit</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111564" class="expand-container">
<details><summary>org.apache.lucene.index.IndexReader</summary>
<div id="expander-content-1998111564" class="expand-content">
<ul>
<li>org.apache.lucene.index.IndexReader.numDocs</li>
<li>org.apache.lucene.index.IndexReader.maxDoc</li>
<li>org.apache.lucene.index.IndexReader.document</li>
<li>org.apache.lucene.index.IndexReader.docFreq</li>
<li>org.apache.lucene.index.IndexReader.getRefCount</li>
<li>org.apache.lucene.index.IndexReader.numDeletedDocs</li>
<li>org.apache.lucene.index.IndexReader.hasDeletions</li>
<li>org.apache.lucene.index.IndexReader.getSumDocFreq</li>
<li>org.apache.lucene.index.IndexReader.getDocCount</li>
<li>org.apache.lucene.index.IndexReader.getSumTotalTermFreq</li>
</ul>
</div>
</details></div>
<div id="expander-1998111565" class="expand-container">
<details><summary>org.apache.lucene.search.IndexSearcher</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>org.apache.lucene.search.IndexSearcher.search</li>
<li>org.apache.lucene.search.IndexSearcher.explain</li>
<li>org.apache.lucene.search.IndexSearcher.count</li>
<li>org.apache.lucene.search.IndexSearcher.totalTermFreq</li>
<li>org.apache.lucene.search.IndexSearcher.getQueryCache</li>
<li>org.apache.lucene.search.IndexSearcher.getIndexReader</li>
<li>org.apache.lucene.search.IndexSearcher.doc</li>
<li>org.apache.lucene.search.IndexSearcher.storedFields</li>
<li>org.apache.lucene.search.IndexSearcher.getSlices</li>
<li>org.apache.lucene.search.IndexSearcher.searchAfter</li>
</ul>
</div>
</details></div>

</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd"
style="text-align: left;">
<ul>
<li>org.apache.lucene.index.IndexWriter.deleteDocuments</li>
<li>org.apache.lucene.index.IndexWriter.deleteAll</li>
</ul>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"
style="text-align: left;">
<ul>
<li>org.apache.lucene.index.IndexWriter.updateDocument</li>
</ul>
</td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="635568163.png" draggable="false"
data-image-src="635568163.png"
data-unresolved-comment-count="0" data-linked-resource-id="635568163"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="known_index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="635142304"
data-linked-resource-container-version="1" /></li>
</div></td>
<td class="confluenceTd">Java ApacheLucene Index</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<li><img src="635568162.png" draggable="false"
data-image-src="635568162.png"
data-unresolved-comment-count="0" data-linked-resource-id="635568162"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown_index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="635142304"
data-linked-resource-container-version="1" /></li>
</div></td>
<td class="confluenceTd">Java Unknown ApacheLucene Index</td>
</tr>
</tbody>
</table>

## Links

All links are created between the caller Java method object and the
ApacheLucene Index object:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><strong>Link type</strong></th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><li>numDocs</li>
<li>maxDoc</li>
<li>document</li>
<li>docFreq</li>
<li>getRefCount</li>
<li>numDeletedDocs</li>
<li>hasDeletions</li>
<li>getSumDocFreq</li>
<li>getDocCount</li>
<li>getSumTotalTermFreq</li>
<li>search</li>
<li>explain</li>
<li>count</li>
<li>totalTermFreq</li>
<li>getQueryCache</li>
<li>getIndexReader</li>
<li>doc</li>
<li>storedFields</li>
<li>getSlices</li>
<li>searchAfter</li></td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><li>addDocument</li>
<li>addIndexes</li>
<li>setQueryCache</li>
<li>commit</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><li>deleteDocuments</li>
<li>deleteAll</li></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><li>updateDocument</li></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### ApacheLucene Index

``` java
public class LuceneReadIndexExample 
{
    private static final String INDEX_DIR = "c:/temp/lucene6index";

    public static void main(String[] args) throws Exception 
    {
        IndexSearcher searcher = createSearcher();
        
        //Search by ID
        TopDocs foundDocs = searchById(1, searcher);
        
        System.out.println("Toral Results :: " + foundDocs.totalHits);
        
        for (ScoreDoc sd : foundDocs.scoreDocs) 
        {
            Document d = searcher.doc(sd.doc);
            System.out.println(String.format(d.get("firstName")));
        }
        
        //Search by firstName
        TopDocs foundDocs2 = searchByFirstName("Brian", searcher);
        
        System.out.println("Toral Results :: " + foundDocs2.totalHits);
        
        for (ScoreDoc sd : foundDocs2.scoreDocs) 
        {
            Document d = searcher.doc(sd.doc);
            System.out.println(String.format(d.get("id")));
        }
    }
private static IndexSearcher createSearcher() throws IOException {
        Directory dir = FSDirectory.open(Paths.get(INDEX_DIR));
        IndexReader reader = DirectoryReader.open(dir);
        IndexSearcher searcher = new IndexSearcher(reader);
        return searcher;
    }
    
```

![](639336598.png)

``` java
public synchronized IndexWriter retrieveIndexWriter() throws LuceneAlertException
   {

      if (writer == null)
      {

         if (indexPath == null)
         {
            logger.fatal("No Lucene Index Path has been defined.");
            throw new LuceneAlertException(
               "No Lucene Index Path has been defined.Please define that in the configuration file.");
         }
         // using memory index
         if (indexPath != null)
         {

            logger.debug("inside of indexPath !=null");
            try
            {
               if (indexPath.equalsIgnoreCase("RAM"))
               {
                  if (ramDir == null)
                     ramDir = new RAMDirectory();
                  writer =
                     new IndexWriter(ramDir, new StandardAnalyzer(Version.LUCENE_29),
                        IndexWriter.MaxFieldLength.LIMITED);
                  writer.setWriteLockTimeout(WRITER_LOCK_TIMEDOUT);
               }
               else
               {
                  FSDirectory fsDir = FSDirectory.open(new File(indexPath));
                  writer =
                     new IndexWriter(fsDir, new StandardAnalyzer(Version.LUCENE_29), IndexWriter.MaxFieldLength.LIMITED);
                  writer.setWriteLockTimeout(WRITER_LOCK_TIMEDOUT);
               }
            }
            catch (CorruptIndexException ce)
            {
               logger.error("CorruptIndexException  thrown when retrieving writer." + ce.getMessage());
               throw new LuceneAlertException(ce);
            }
            catch (IOException ioe)
            {
               logger.error(" IOException  thrown when retrieving writer." + ioe.getMessage());
               throw new LuceneAlertException(ioe);
            }
         }
      }

      return writer;

   }
```

![](639336599.png)

### Select Operation

``` java
 public synchronized LuceneAlertSearchResult search(Query query, boolean updateRecord, IndexWriter writer,
      int index, int batch)
      throws IOException, LuceneAlertException
   {

      if (logger.isDebugEnabled())
         logger.debug("Entering LuceneAlertSearchResult(), query=" + query + ", updateRecord=" + updateRecord
            + ", index=" + index + ", batch=" + batch);

      LuceneAlertSearchResult result = new LuceneAlertSearchResult();

      IndexSearcher searcher = retrieveIndexSearcher();

      if (logger.isDebugEnabled())
         logger.debug("Hashcode for this=" + this + " ,for indexSearcher=" + searcher);

      TopDocs resultDocs = searcher.search(query, searcher.maxDoc());

      // for clearAlert
      if (updateRecord)
      {
         updateAlertDoc(writer, resultDocs);
         setIndexUpdated(true);

         if (logger.isDebugEnabled())
            logger.debug("setIndexUpdated to true");

      }
      // for alert search
      else
      {

         List<Document> docList = new ArrayList<Document>();

         ScoreDoc[] resultScoreDocs = resultDocs.scoreDocs;

         int totalHits = resultDocs.totalHits;
         int end = index + batch;
         int start = (index > 0) ? index : 0;
         end = (totalHits > end) ? end : totalHits;

         if (logger.isDebugEnabled())
            logger.debug("Total hits=" + totalHits + ", start=" + start + ", end=" + end);

         for (int i = start; i < end; i++)
            docList.add(searcher.doc(resultScoreDocs[i].doc));

         result.setDocs(docList);
      }

      if (logger.isDebugEnabled())
         logger.debug("LuceneAlertSearchResult() DONE");

      return result;

   }
```

![](635142310.png)

### Insert Operation

``` java
    
   @Override
   public void addDocument(AlertDTO alertDTO) throws LuceneAlertException
   {
      Document document = convertToDocument(alertDTO);

      if (logger.isDebugEnabled())
         logger.debug("Document object ready to be added into index: " + document);

      IndexWriter writer = luceneAlertHelper.retrieveIndexWriter();

      if (writer == null)
      {
         logger.error("Failed to obtain index writer when trying to add doc to index");
         throw new LuceneAlertException("Failed to obtain index writer");
      }
      try
      {
         writer.addDocument(document);
         luceneAlertHelper.commit();
         luceneAlertHelper.setIndexUpdated(true);

      }
      catch (CorruptIndexException e)
      {
         throw new LuceneAlertException(e);
      }
      catch (IOException e)
      {
         throw new LuceneAlertException(e);
      }

      if (logger.isInfoEnabled())
         logger.info("Document added for alert: " + alertDTO.getId());

   }
```

![](639336603.png)

### Update Operation

``` java
    public static void main(String[] args)
    {
        //Input folder
        String docsPath = "inputFiles";
         
        //Output folder
        String indexPath = "C:/index/indexedFiles";
 
        //Input Path Variable
        final Path docDir = Paths.get(docsPath);
 
        try
        {
            //org.apache.lucene.store.Directory instance
            Directory dir = FSDirectory.open( Paths.get(indexPath) );
             
            //analyzer with the default stop words
            Analyzer analyzer = new StandardAnalyzer();
             
            //IndexWriter Configuration
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
             
            //IndexWriter writes new index files to the directory
            IndexWriter writer = new IndexWriter(dir, iwc);
             
            //Its recursive method to iterate all files and directories
            indexDocs(writer, docDir);
 
            writer.close();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
     
    static void indexDocs(final IndexWriter writer, Path path) throws IOException 
    {
        //Directory?
        if (Files.isDirectory(path)) 
        {
            //Iterate directory
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() 
            {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException 
                {
                    try
                    {
                        //Index this file
                        indexDoc(writer, file, attrs.lastModifiedTime().toMillis());
                    } 
                    catch (IOException ioe) 
                    {
                        ioe.printStackTrace();
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } 
        else
        {
            //Index this file
            indexDoc(writer, path, Files.getLastModifiedTime(path).toMillis());
        }
    }
 
    static void indexDoc(IndexWriter writer, Path file, long lastModified) throws IOException 
    {
        try (InputStream stream = Files.newInputStream(file)) 
        {
            //Create lucene Document
            Document doc = new Document();
             
            doc.add(new StringField("path", file.toString(), Field.Store.YES));
            doc.add(new LongPoint("modified", lastModified));
            doc.add(new TextField("contents", new String(Files.readAllBytes(file)), Store.YES));
             
            //Updates a document by first deleting the document(s) 
            //containing <code>term</code> and then adding the new
            //document.  The delete and then add are atomic as seen
            //by a reader on the same index
            writer.updateDocument(new Term("path", file.toString()), doc);
        }
    }
}
```

![](639336597.png)

### Delete Operation

``` java
    @Override
   public void deleteDocument(AlertDTO alertDTO) throws LuceneAlertException
   {

      if (logger.isInfoEnabled())
         logger.info("delete document for alert:" + alertDTO.getId());

      IndexWriter writer = luceneAlertHelper.retrieveIndexWriter();

      if (writer == null)
      {
         logger.error("Failed to obtain the index writer." + " Adding document for  alert: " + alertDTO.getId()
            + " failed.");
         throw new LuceneAlertException("Failed to obtain index writer");
      }

      if ((new Long(alertDTO.getId()) == null))
      {
         logger.error("Failed to delete alert document for  alert: " + alertDTO.getId());
         throw new LuceneAlertException("Invalid alert ID.");

      }


      try
      {
         writer.deleteDocuments(new Term(LuceneAlertFields.ID, (new Long(alertDTO.getId()).toString())));
         luceneAlertHelper.commit();
         luceneAlertHelper.setIndexUpdated(true);
      }
      catch (CorruptIndexException e)
      {
         logger.error("A CorrupIndexException is thrown when Document is deleted from  Lucene Interface for " +
            alertDTO.getBan() + ". " + e.getMessage());
         throw new LuceneAlertException(e);
      }
      catch (IOException e)
      {
         logger.error("A IOException is thrown when Document is deleted from  Lucene Interface for " +
            alertDTO.getBan() + ". " + e.getMessage());
         throw new LuceneAlertException(e);
      }


      if (logger.isInfoEnabled())
         logger.info(alertDTO.getId() + " deleted.");


   }
```

![](639336601.png)

## Known Limitations

-   If the index location or path is not found in the source
    application, it will result in an Unknown index object  
    