---
title: "Support of CouchDB for Node.js"
linkTitle: "Support of CouchDB for Node.js"
type: "docs"
---

>CAST supports CouchDB via
its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
extension. Details about how this support is provided for Node.js source
code is discussed below.

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="665813848.png" /></p>
</div></td>
<td class="confluenceTd">Node.js CouchDB database</td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### node-couchdb module

#### Database creation "myDatabase"

``` js
const NodeCouchDb = require('node-couchdb');
 
// node-couchdb instance with default options 
const couch = new NodeCouchDb();
dbName = 'myDatabase';

couch.createDatabase(dbName).then(() => { }, err => {
    // request error occured 
});
```

#### useSelectLink from code to database "myDatabase"

``` js
    couch.get("myDatabase", "some_document_id").then(({data, headers, status}) => {
        // data is json response 
        // headers is an object with all response headers 
        // status is statusCode number 
    }, err => {
        // either request error occured 
        // ...or err.code=EDOCMISSING if document is missing 
        // ...or err.code=EUNKNOWN if statusCode is unexpected 
    });
```

#### useInsertLink from code to database "myDatabase"

``` js
couch.insert("myDatabase", {
    _id: "document_id",
    field: ["sample", "data", true]
}).then(({data, headers, status}) => {
    // data is json response 
    // headers is an object with all response headers 
    // status is statusCode number 
}, err => {
    // either request error occured 
    // ...or err.code=EDOCCONFLICT if document with the same id already exists 
});
```

#### useUpdateLink from code to database "myDatabase"

``` js
couch.update("myDatabase", {
    _id: "document_id",
    _rev: "1-xxx"
    field: "new sample data",
    field2: 1
}).then(({data, headers, status}) => {
    // data is json response 
    // headers is an object with all response headers 
    // status is statusCode number 
}, err => {
    // either request error occured 
    // ...or err.code=EFIELDMISSING if either _id or _rev fields are missing 
});
```

#### useDeleteLink from code to database "myDatabase"

``` js
couch.del("myDatabase", "some_document_id", "document_revision").then(({data, headers, status}) => {
    // data is json response 
    // headers is an object with all response headers 
    // status is statusCode number 
}, err => {
    // either request error occured 
    // ...or err.code=EDOCMISSING if document does not exist 
    // ...or err.code=EUNKNOWN if response status code is unexpected 
});
```

### couchdb module

#### Database creation "myDatabase"

``` js
var myCouchDB = require('couch-db').CouchDB,
    server = new myCouchDB('http://localhost:5984');
  
server.auth(username, password);
 
server.bind('myDatabase');
var db = server.myDatabase;
```

#### useInsertLink from code to database "myDatabase"

``` js
// new document 
var doc = db.testdb.doc({});
doc.attach([{
    name: 'place.css',
    content_type: 'text/css',
    data: 'body { font-size: 12px; }'
}, {
    name: 'script.js',
    content_type: 'script/javascript',
    data: 'window.onload(function() {})'
}]).create(function(err) {
 
});
```

#### useInsertLink from code to database "myDatabase"

Connector "couch-db"

``` js
db.destroy(function(err) {
    // create a new database 
    db.create(function(err) {
        // insert a document with id 'jack johns' 
        db.insert({ _id: 'jack johns', name: 'jack' }, function(err, body) {
            if (err) {
                console.log('insertion failed ', err.message);
                return;
            }
            console.log(body);
            // body will like following: 
            //   { ok: true, 
            //     id: 'jack johns', 
            //     rev: '1-610953b93b8bf1bae12427e2de181307' } 
        });
    });
});
```

#### useUpdateLink from code to database "myDatabase"

``` js
var doc = db.testdb.doc({});
// open to get revision or assign revision to the document 
doc.open(function(err) {
    doc.attach('plain.css', 'body { font-size:12pt; }', 'text/css');
    // save the doc 
    doc.save(function(err, rs) {
        var plain = doc.attachment('plain.txt');
        // retrieve attachment 
        plain.get(function(err, body) {
            assert.equal(body, 'body { font-size:12pt; }');
            // update 
            plain.update('body { font-size:14pt; }', 'text/css', function(err) {
                plain.get(function(err, body) {
                    assert.equal(body, 'body { font-size:14pt; }');
                });
            });
        });
    });
```
