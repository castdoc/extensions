---
title: "Support of CouchDB for Java"
linkTitle: "Support of CouchDB for Java"
type: "docs"
---

>CAST supports CouchDB via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about how this support is provided for Java source
code is discussed below.

## Objects

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" scope="col">Icon</th>
<th class="confluenceTh" scope="col">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
<p><img src="323616995.png" class="image-center"
draggable="false" data-image-src="323616995.png"
data-unresolved-comment-count="0" data-linked-resource-id="323616995"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_Mongoose_Connection32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="323616989"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd"><p>Java CouchDB connection</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
<p><img src="323616994.png" class="image-center"
draggable="false" data-image-src="323616994.png"
data-unresolved-comment-count="0" data-linked-resource-id="323616994"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="323616989"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd"><p>Java CouchDB database</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
<p><img src="323616993.png" class="image-center"
draggable="false" data-image-src="323616993.png"
data-unresolved-comment-count="0" data-linked-resource-id="323616993"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="323616989"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Java CouchDB collection</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
<p><img src="323616992.png" class="image-center"
draggable="false" data-image-src="323616992.png"
data-unresolved-comment-count="0" data-linked-resource-id="323616992"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownDatabaseConnection32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="323616989"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Java Unknown CouchDB connection</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
<p><img src="323616991.png" class="image-center"
draggable="false" data-image-src="323616991.png"
data-unresolved-comment-count="0" data-linked-resource-id="323616991"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="323616989"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Java Unknown CouchDB database</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
<p><img src="323616990.png" class="image-center"
draggable="false" data-image-src="323616990.png"
data-unresolved-comment-count="0" data-linked-resource-id="323616990"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="323616989"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Java Unknown CouchDB collection</td>
</tr>
</tbody>
</table>

## Links

<table class="relative-table wrapped confluenceTable"
style="width: 669.22px;">
<tbody style="margin-left: 0.0px;">
<tr class="header" style="margin-left: 0.0px;">
<th class="confluenceTh" style="text-align: left;">Link type</th>
<th class="confluenceTh" style="text-align: left;">When is this
created?</th>
</tr>
&#10;<tr class="odd" style="margin-left: 0.0px;">
<td class="confluenceTd" style="text-align: left;">belongsTo</td>
<td class="confluenceTd" style="text-align: left;"><p>From Java CouchDB
collection object to Java CouchDB database object</p>
<p>From Java CouchDB database object to Java CouchDB connection
object</p></td>
</tr>
<tr class="even" style="margin-left: 0.0px;">
<td class="confluenceTd" style="text-align: left;">useLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java objects and Java CouchDB database/collection.object</td>
</tr>
<tr class="odd" style="margin-left: 0.0px;">
<td class="confluenceTd" style="text-align: left;"><p>useSelectLink</p>
<p>useUpdateLink</p>
<p>useDeleteLink</p>
<p>useInsertLink</p></td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java object and a Java CouchDB database/collection.object</td>
</tr>
</tbody>
</table>
