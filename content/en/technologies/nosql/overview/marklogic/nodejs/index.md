---
title: "Support of MarkLogic for Node.js"
linkTitle: "Support of MarkLogic for Node.js"
type: "docs"
---

>CAST supports MarkLogic via
its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
extension. Details about how this support is provided for .Node.js
source code is discussed below.

## Objects

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="666370401.png" draggable="false"
data-image-src="666370401.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370401"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_Mongoose_Model32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370399"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Node.js Marklogic Database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370400.png" draggable="false"
data-image-src="666370400.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370400"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_Marklogic_Collection32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370399"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Node.js Marklogic Collection</p>
</div></td>
</tr>
</tbody>
</table>

## Code samples

This declaration will create a MarkLogic database and a collection:

``` js
var db = marklogic.createDatabaseClient(conn);
 
// create Collection countries
var q = marklogic.queryBuilder;
db.documents.query(
    q.where(
        q.collection('countries'),
        q.value('region', 'Africa'),
        q.or(
            q.word('background', 'France'),
            q.word('Legal system', 'French')
        )
    )
).result(function(documents) {
    documents.forEach(function(document){ 
        console.log(JSON.stringify(document)); 
    });
});
```

This declaration will create a MarkLogic collection "fake data" and a
*useDeleteLink* to the collection "fake data":

``` js
db.documents.removeAll({collection: 'fake data'})
            .result()
            .then(response => console.log('Removed collection ' + response.collection))
            .catch(error => console.log(error));
```

This declaration will create a MarkLogic collection "fake data" and a
*useInsertLink* to the collection "fake data":

``` js
db.documents.write(
  data.map((item) => {
    return {
      uri: `/${item.guid}.json`,
      contentType: 'application/json',
      collections: ['fake data'],
      content: item
    }
  })
)
.result()
.then(response => console.dir(JSON.stringify(response)))
.catch(error => console.error(error));
```
