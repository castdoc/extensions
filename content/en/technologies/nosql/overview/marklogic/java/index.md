---
title: "Support of MarkLogic for Java"
linkTitle: "Support of MarkLogic for Java"
type: "docs"
---

>CAST supports MarkLogic via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about how this support is provided for Java source
code is discussed below.

## Objects

<table>
<colgroup>
<col/>
<col/>
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh"><div class="content-wrapper">
<p>Icon</p>
</div></th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="322084604.png" class="image-center"
draggable="false" data-image-src="322084604.png"
data-unresolved-comment-count="0" data-linked-resource-id="322084604"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="322084600"
data-linked-resource-container-version="6" /></p>
</div></td>
<td class="confluenceTd">Java MarkLogic database</td>
</tr>
<tr class="even" >
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="322084603.png" class="image-center"
draggable="false" data-image-src="322084603.png"
data-unresolved-comment-count="0" data-linked-resource-id="322084603"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="322084600"
data-linked-resource-container-version="6" /></p>
</div></td>
<td class="confluenceTd"><p>Java MarkLogic collection</p></td>
</tr>
<tr class="odd" >
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="322084602.png" class="image-center"
draggable="false" data-image-src="322084602.png"
data-unresolved-comment-count="0" data-linked-resource-id="322084602"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="322084600"
data-linked-resource-container-version="6" /></p>
</div></td>
<td class="confluenceTd">Java unknown MarkLogic database</td>
</tr>
<tr class="even" >
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="322084601.png" class="image-center"
draggable="false" data-image-src="322084601.png"
data-unresolved-comment-count="0" data-linked-resource-id="322084601"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="322084600"
data-linked-resource-container-version="6" /></p>
</div></td>
<td class="confluenceTd">Java unknown MarkLogic collection</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable"
style="text-decoration: none;text-align: left;margin-left: 0.0px;">
<tbody >
<tr class="header" >
<th class="confluenceTh" style="text-align: left;">Link type</th>
<th class="confluenceTh" style="text-align: left;">When is this
created?</th>
</tr>
&#10;<tr class="odd" >
<td class="confluenceTd" style="text-align: left;">belongsTo</td>
<td class="confluenceTd" style="text-align: left;"><p>From Java
MarkLogic collection to Java MarkLogic database object</p></td>
</tr>
<tr class="even" >
<td class="confluenceTd" style="text-align: left;">useLink</td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java objects and Java MarkLogic collection object.</td>
</tr>
<tr class="odd" >
<td class="confluenceTd" style="text-align: left;"><p>useSelectLink</p>
<p>useUpdateLink</p>
<p>useDeleteLink</p>
<p>useInsertLink</p></td>
<td class="confluenceTd" style="text-align: left;">Between the caller
Java object and a Java MarkLogic collection object.</td>
</tr>
</tbody>
</table>
