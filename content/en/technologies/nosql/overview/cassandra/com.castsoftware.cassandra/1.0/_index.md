---
title: "Cassandra for CQL - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

## Extension ID

com.castsoftware.cassandra

## What's New?

See [Cassandra support for CQL source code - 1.0 - Release
Notes](rn).

## Description

This extension provides source code analysis support for \*.cql
files.

## Technology support

| Apache Cassandra | Supported                                                                                  |
|------------------|--------------------------------------------------------------------------------------------|
| 4.x              | ![(tick)](/images/icons/emoticons/check.svg) |
| 3.x              | ![(tick)](/images/icons/emoticons/check.svg) |
| 2.x              | ![(tick)](/images/icons/emoticons/check.svg) |
| 1.x              | ![(tick)](/images/icons/emoticons/check.svg) |

## Function Point, Quality and Sizing support

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Function Points<br />
(transactions)</th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">A green tick indicates that OMG Function Point
counting and Transaction Risk Index are supported.</td>
</tr>
<tr class="even">
<th class="confluenceTh" style="text-align: center;">Quality and
Sizing</th>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
<td class="confluenceTd">A green tick indicates that CAST can measure
size and that a minimum set of Quality Rules exist.</td>
</tr>
</tbody>
</table>

## CAST Imaging Core compatibility

| Release | Operating System  | Supported                                                                                  |
|---------|-------------------|--------------------------------------------------------------------------------------------|
| 8.3.x   | Microsoft Windows | ![(tick)](/images/icons/emoticons/check.svg) |

## Download and installation instructions

The extension will not be automatically installed by CAST Imaging
Console.

## Source code discovery

A discoverer is provided with the extension to automatically detect CQL
related code: a project will be discovered for the package's root folder
when at least one .cql file is detected in the root folder or any
sub-folders. For every project located, one Universal Technology
Analysis Unit will be created:

![](../images/609517628.jpg)

## What results can you expect?

### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Icon</th>
<th class="confluenceTh">Object Type</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767491.png" draggable="false"
data-image-src="../images/602767491.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767491"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL Schema</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767490.png" class="image-left"
draggable="false" data-image-src="../images/602767490.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767490"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL Table</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767489.png" draggable="false"
data-image-src="../images/602767489.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767489"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="View32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL View</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767488.png" class="image-left"
draggable="false" data-image-src="../images/602767488.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767488"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="TableColumn32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL Table Column</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767487.png" draggable="false"
data-image-src="../images/602767487.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767487"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Index32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL Index</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767486.png" draggable="false"
data-image-src="../images/602767486.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767486"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Primary Key32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL Primary Key</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767485.png" draggable="false"
data-image-src="../images/602767485.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767485"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Trigger32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL Trigger</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/602767484.png" draggable="false"
data-image-src="../images/602767484.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767484"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Function32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Cassandra CQL Function</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useSelect</td>
<td class="confluenceTd">Cassandra CQL View</td>
<td class="confluenceTd"><p>Cassandra CQL Table</p>
<p>Cassandra CQL View</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/602767481.png" draggable="false"
data-image-src="../images/602767481.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767481"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2022-10-20_19-22-58.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" height="191" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>useSelect</p>
<p>useUpdate</p>
<p>useDelete</p>
<p>useInsert</p></td>
<td class="confluenceTd"><p>Cassandra Java CQL Query</p>
<p>Cassandra Java Operation</p></td>
<td class="confluenceTd"><p>Cassandra CQL Table</p>
<p>Cassandra CQL View</p>
<p>Cassandra Java Table</p></td>
<td class="confluenceTd">See the documentation for the <a
href="https://extend.castsoftware.com/#/extension?id=com.castsoftware.java.cassandra&amp;version=latest"
rel="nofollow">com.castsoftware.java.cassandra</a> extension.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd">Cassandra CQL Index</td>
<td class="confluenceTd">Cassandra CQL Table</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/602767480.png" draggable="false"
data-image-src="../images/602767480.png"
data-unresolved-comment-count="0" data-linked-resource-id="602767480"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2022-10-20_19-24-35.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="602767479"
data-linked-resource-container-version="1" height="250" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd">Cassandra Java Index</td>
<td class="confluenceTd">Cassandra Java Table</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>See image above.</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><p>Cassandra CQL Index</p>
<p>Cassandra CQL Primary Key</p></td>
<td class="confluenceTd">Cassandra CQL Table Column implied in the
index</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>See image above.</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><p>Cassandra Java Index</p>
<p>Cassandra Java Primary Key</p></td>
<td class="confluenceTd">Cassandra Java Table Column implied in the
index</td>
<td class="confluenceTd">See the documentation for the <a
href="https://extend.castsoftware.com/#/extension?id=com.castsoftware.java.cassandra&amp;version=latest"
rel="nofollow">com.castsoftware.java.cassandra</a> extension.</td>
</tr>
</tbody>
</table>

## Log messages

For a list of possible log messages that the extension can output, see [logs](../logs/).

## Known Limitations

-   Primary Key on View is not supported
-   View columns are not supported
-   Trigger is not linked with Java Classes that implements the trigger
-   There is no link between Function and the external objects - Java,
    JavaScript, Python, Ruby, and Scala - from the code block.