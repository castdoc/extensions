---
title: "Support of Redis for Java"
linkTitle: "Support of Redis for Java"
type: "docs"
---

{{% alert color="info" %}}CAST supports Redis via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about the support provided for Java source code is
discussed below.{{% /alert %}}

## Supported Libraries

| Library | Version | Supported |
|---|---|:---:|
| [Jedis](https://www.javadoc.io/doc/redis.clients/jedis/4.3.2/redis/clients/jedis/Jedis.html) | Up to: 4.3.2 | :white_check_mark: |
| [JedisPool](https://javadoc.io/doc/redis.clients/jedis/4.3.2/redis/clients/jedis/JedisPool.html) | Up to: 4.3.2 | :white_check_mark: |
| [JedisSentinePool](https://javadoc.io/doc/redis.clients/jedis/4.3.2/redis/clients/jedis/JedisSentinelPool.html) | Up to: 4.3.2 | :white_check_mark: |
| [JedisCommands](https://www.javadoc.io/doc/redis.clients/jedis/4.3.2/redis/clients/jedis/commands/JedisCommands.html) | Up to: 4.3.2 | :white_check_mark: |
| [JedisTransactions](https://www.javadoc.io/doc/redis.clients/jedis/4.3.2/redis/clients/jedis/Transaction.html) | Up to: 4.3.2 | :white_check_mark: |
| [Jedis TransactionBase](https://www.javadoc.io/static/redis.clients/jedis/4.3.2/redis/clients/jedis/TransactionBase.html) | Up to: 4.3.2 | :white_check_mark: |
| [Lettuce](https://lettuce.io/core/release/api/overview-summary.html) | Up to: 6.2.2 | :white_check_mark: |
| [RedisClient](https://lettuce.io/core/release/api/io/lettuce/core/RedisClient.html) | Up to: 6.2.2 | :white_check_mark: |
| [RedisCommands](https://lettuce.io/core/snapshot/api/io/lettuce/core/api/sync/RedisCommands.html) | Up to: 6.2.2 | :white_check_mark: |
| [RedisSyncCommands](https://lettuce.io/core/release/api/io/lettuce/core/api/sync/package-summary.html) | Up to: 6.2.2 | :white_check_mark: |
| [RedisAsyncCommands](https://lettuce.io/core/release/api/io/lettuce/core/api/async/package-summary.html) | Up to: 6.2.2 | :white_check_mark: |
| [RedisReactiveCommands](https://lettuce.io/core/release/api/io/lettuce/core/api/reactive/package-summary.html) | Up to: 6.2.2 | :white_check_mark: |
| [Redisson](https://www.javadoc.io/static/org.redisson/redisson/3.41.0/org/redisson/api/package-summary.html) | Up to: 3.44.0 | :white_check_mark: |

## Supported Operations

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Operation</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Methods
Supported</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<details><summary>Jedis Base Commands</summary>
<p>redis.clients.jedis.Jedis.copy</p>
<p>redis.clients.jedis.Jedis.expire</p>
<p>redis.clients.jedis.Jedis.expireAt</p>
<p>redis.clients.jedis.Jedis.geoadd</p>
<p>redis.clients.jedis.Jedis.geosearchStore</p>
<p>redis.clients.jedis.Jedis.geosearchStoreStoreDist</p>
<p>redis.clients.jedis.Jedis.hmset</p>
<p>redis.clients.jedis.Jedis.hset</p>
<p>redis.clients.jedis.Jedis.hsetnx</p>
<p>redis.clients.jedis.Jedis.linsert</p>
<p>redis.clients.jedis.Jedis.lpush</p>
<p>redis.clients.jedis.Jedis.lpushx</p>
<p>redis.clients.jedis.Jedis.lset</p>
<p>redis.clients.jedis.Jedis.rpush</p>
<p>redis.clients.jedis.Jedis.rpushx</p>
<p>redis.clients.jedis.Jedis.blmove</p>
<p>redis.clients.jedis.Jedis.brpoplpush</p>
<p>redis.clients.jedis.Jedis.lmove</p>
<p>redis.clients.jedis.Jedis.rpoplpush</p>
<p>redis.clients.jedis.Jedis.append</p>
<p>redis.clients.jedis.Jedis.getSet</p>
<p>redis.clients.jedis.Jedis.set</p>
<p>redis.clients.jedis.Jedis.psetex</p>
<p>redis.clients.jedis.Jedis.setbit</p>
<p>redis.clients.jedis.Jedis.setex</p>
<p>redis.clients.jedis.Jedis.setnx</p>
<p>redis.clients.jedis.Jedis.setrange</p>
<p>redis.clients.jedis.Jedis.setGet</p>
<p>redis.clients.jedis.Jedis.sadd</p>
<p>redis.clients.jedis.Jedis.sdiffstore</p>
<p>redis.clients.jedis.Jedis.sinterstore</p>
<p>redis.clients.jedis.Jedis.sunionstore</p>
<p>redis.clients.jedis.Jedis.smove</p>
<p>redis.clients.jedis.Jedis.zadd</p>
<p>redis.clients.jedis.Jedis.zdiffStore</p>
<p>redis.clients.jedis.Jedis.zinterstore</p>
<p>redis.clients.jedis.Jedis.zunionstore</p>
<p>redis.clients.jedis.Jedis.zrangestore</p>
<p>redis.clients.jedis.Jedis.zrangestorebylex</p>
<p>redis.clients.jedis.Jedis.zrangestorebyscore</p>
<p>redis.clients.jedis.Jedis.zrevrangestore</p>
<p>redis.clients.jedis.Jedis.zrevrangestorebylex</p>
<p>redis.clients.jedis.Jedis.zrevrangestorebyscore</p>
<p>redis.clients.jedis.Jedis.xadd</p>
</details>
<details><summary>Jedis Transaction Commands</summary>
<p>redis.clients.jedis.TransactionBase.copy</p>
<p>redis.clients.jedis.TransactionBase.expire</p>
<p>redis.clients.jedis.TransactionBase.expireAt</p>
<p>redis.clients.jedis.TransactionBase.geoadd</p>
<p>redis.clients.jedis.TransactionBase.geosearchStore</p>
<p>redis.clients.jedis.TransactionBase.geosearchStoreStoreDist</p>
<p>redis.clients.jedis.TransactionBase.hmset</p>
<p>redis.clients.jedis.TransactionBase.hset</p>
<p>redis.clients.jedis.TransactionBase.hsetnx</p>
<p>redis.clients.jedis.TransactionBase.linsert</p>
<p>redis.clients.jedis.TransactionBase.lpush</p>
<p>redis.clients.jedis.TransactionBase.lpushx</p>
<p>redis.clients.jedis.TransactionBase.lset</p>
<p>redis.clients.jedis.TransactionBase.rpush</p>
<p>redis.clients.jedis.TransactionBase.rpushx</p>
<p>redis.clients.jedis.TransactionBase.blmove</p>
<p>redis.clients.jedis.TransactionBase.brpoplpush</p>
<p>redis.clients.jedis.TransactionBase.lmove</p>
<p>redis.clients.jedis.TransactionBase.rpoplpush</p>
<p>redis.clients.jedis.TransactionBase.append</p>
<p>redis.clients.jedis.TransactionBase.getSet</p>
<p>redis.clients.jedis.TransactionBase.set</p>
<p>redis.clients.jedis.TransactionBase.psetex</p>
<p>redis.clients.jedis.TransactionBase.setbit</p>
<p>redis.clients.jedis.TransactionBase.setex</p>
<p>redis.clients.jedis.TransactionBase.setnx</p>
<p>redis.clients.jedis.TransactionBase.setrange</p>
<p>redis.clients.jedis.TransactionBase.setGet</p>
<p>redis.clients.jedis.TransactionBase.sadd</p>
<p>redis.clients.jedis.TransactionBase.sdiffstore</p>
<p>redis.clients.jedis.TransactionBase.sinterstore</p>
<p>redis.clients.jedis.TransactionBase.sunionstore</p>
<p>redis.clients.jedis.TransactionBase.smove</p>
<p>redis.clients.jedis.TransactionBase.zadd</p>
<p>redis.clients.jedis.TransactionBase.zdiffStore</p>
<p>redis.clients.jedis.TransactionBase.zinterstore</p>
<p>redis.clients.jedis.TransactionBase.zunionstore</p>
<p>redis.clients.jedis.TransactionBase.zrangestore</p>
<p>redis.clients.jedis.TransactionBase.zrangestorebylex</p>
<p>redis.clients.jedis.TransactionBase.zrangestorebyscore</p>
<p>redis.clients.jedis.TransactionBase.zrevrangestore</p>
<p>redis.clients.jedis.TransactionBase.zrevrangestorebylex</p>
<p>redis.clients.jedis.TransactionBase.zrevrangestorebyscore</p>
<p>redis.clients.jedis.TransactionBase.xadd</p>
</details>
<details><summary>JedisCommands Commands</summary>
<p>redis.clients.jedis.commands.KeyCommands.copy</p>
<p>redis.clients.jedis.commands.KeyCommands.expire</p>
<p>redis.clients.jedis.commands.KeyCommands.expireAt</p>
<p>redis.clients.jedis.commands.GeoCommands.geoadd</p>
<p>redis.clients.jedis.commands.GeoCommands.geosearchStore</p>
<p>redis.clients.jedis.commands.GeoCommands.geosearchStoreStoreDist</p>
<p>redis.clients.jedis.commands.HashCommands.hmset</p>
<p>redis.clients.jedis.commands.HashCommands.hset</p>
<p>redis.clients.jedis.commands.HashCommands.hsetnx</p>
<p>redis.clients.jedis.commands.ListCommands.linsert</p>
<p>redis.clients.jedis.commands.ListCommands.lpush</p>
<p>redis.clients.jedis.commands.ListCommands.lpushx</p>
<p>redis.clients.jedis.commands.ListCommands.lset</p>
<p>redis.clients.jedis.commands.ListCommands.rpush</p>
<p>redis.clients.jedis.commands.ListCommands.rpushx</p>
<p>redis.clients.jedis.commands.ListCommands.blmove</p>
<p>redis.clients.jedis.commands.ListCommands.brpoplpush</p>
<p>redis.clients.jedis.commands.ListCommands.lmove</p>
<p>redis.clients.jedis.commands.ListCommands.rpoplpush</p>
<p>redis.clients.jedis.commands.StringCommands.append</p>
<p>redis.clients.jedis.commands.StringCommands.getSet</p>
<p>redis.clients.jedis.commands.StringCommands.set</p>
<p>redis.clients.jedis.commands.StringCommands.psetex</p>
<p>redis.clients.jedis.commands.StringCommands.setbit</p>
<p>redis.clients.jedis.commands.StringCommands.setex</p>
<p>redis.clients.jedis.commands.StringCommands.setnx</p>
<p>redis.clients.jedis.commands.StringCommands.setrange</p>
<p>redis.clients.jedis.commands.StringCommands.setGet</p>
<p>redis.clients.jedis.commands.SetCommands.sadd</p>
<p>redis.clients.jedis.commands.SetCommands.sdiffstore</p>
<p>redis.clients.jedis.commands.SetCommands.sinterstore</p>
<p>redis.clients.jedis.commands.SetCommands.sunionstore</p>
<p>redis.clients.jedis.commands.SetCommands.smove</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zadd</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zdiffStore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zinterstore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zunionstore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangestore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangestorebylex</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangestorebyscore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangestore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangestorebylex</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangestorebyscore</p>
<p>redis.clients.jedis.commands.StreamCommands.xadd</p>
</details>
<details><summary>Lettuce Commands</summary>
<details><summary>Sync Commands</summary>
<p>io.lettuce.core.api.sync.RedisListCommands.blmove</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lmove</p>
<p>io.lettuce.core.api.sync.RedisListCommands.rpoplpush</p>
<p>io.lettuce.core.api.sync.RedisListCommands.linsert</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lpush</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lpushx</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lset</p>
<p>io.lettuce.core.api.sync.RedisListCommands.rpush</p>
<p>io.lettuce.core.api.sync.RedisListCommands.rpushx</p>
<p>io.lettuce.core.api.sync.RedisListCommands.brpoplpush</p>
<br/>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopAnd</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopNot</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopOr</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopXor</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.setGet</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.getset</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.append</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.psetex</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.set</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.setbit</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.setex</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.setnx</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.setrange</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.mset</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.msetnx</p>
 <br/>
<p>io.lettuce.core.api.sync.RedisGeoCommands.geosearchstore</p>
<p>io.lettuce.core.api.sync.RedisGeoCommands.geoadd</p>
 <br/>
<p>io.lettuce.core.api.sync.RedisHashCommands.hmset</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hset</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hsetnx</p>
 <br/>
<p>io.lettuce.core.api.sync.RedisSetCommands.sadd</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.smove</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sdiffstore</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sinterstore</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sunionstore</p>
 <br/>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zadd</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zdiffStore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zinterstore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zunionstore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangestore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangestorebylex</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangestorebyscore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangestore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangestorebylex</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangestorebyscore</p>
 <br/>
<p>io.lettuce.core.api.sync.RedisStreamCommands.xadd</p>
</details>
<details><summary>Async Commands</summary>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.blmove</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lmove</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.rpoplpush</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.linsert</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lpush</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lpushx</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lset</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.rpush</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.rpushx</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.brpoplpush</p>
 <br/>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopAnd</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopNot</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopOr</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopXor</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.setGet</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.getset</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.append</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.psetex</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.set</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.setbit</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.setex</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.setnx</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.setrange</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.mset</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.msetnx</p>
<br/>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.geosearchstore</p>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.geoadd</p>
<br/>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hmset</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hset</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hsetnx</p>
<br/>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sadd</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.smove</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sdiffstore</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sinterstore</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sunionstore</p>
<br/>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zadd</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zdiffStore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zinterstore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zunionstore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangestore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangestorebylex</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangestorebyscore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangestore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangestorebylex</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangestorebyscore</p>
<br/>
<p>io.lettuce.core.api.async.RedisStreamAsyncCommands.xadd</p>
</details>
<details><summary>Reactive Commands</summary>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.linsert</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lpush</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lpushx</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lset</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.rpush</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.rpushx</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.blmove</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lmove</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.rpoplpush</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.brpoplpush</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopAnd</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopNot</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopOr</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopXor</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.setGet</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.getset</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.append</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.psetex</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.set</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.setbit</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.setex</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.setnx</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.setrange</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.mset</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.msetnx</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.geosearchstore</p>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.geoadd</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hmset</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hset</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hsetnx</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.smove</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sadd</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sdiffstore</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sinterstore</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sunionstore</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zadd</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zdiffStore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zinterstore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zunionstore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangestore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangestorebylex</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangestorebyscore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangestore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangestorebylex</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangestorebyscore</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisStreamReactiveCommands.xadd</p>
</details>
</details>
<details><summary>Redisson Commands</summary>
<details><summary>Sync Commands</summary>
<p>org.redisson.api.RGeo.add</p>
<p>org.redisson.api.RGeo.addIfExists</p>
<p>org.redisson.api.RGeo.storeSearchTo</p>
<p>org.redisson.api.RGeo.storeSortedSearchTo</p>
<p>org.redisson.api.RGeo.tryAdd</p>
<p>org.redisson.api.RGeo.radiusStoreSortedTo</p>
<p>org.redisson.api.RGeo.radiusStoreTo</p>
<br/>
<p>org.redisson.api.RSet.addAllCounted</p>
<p>org.redisson.api.RSet.tryAdd</p>
<p>org.redisson.api.RSet.move</p>
<br/>
<p>org.redisson.api.RScoredSortedSet.add</p>
<p>org.redisson.api.RScoredSortedSet.addAll</p>
<p>org.redisson.api.RScoredSortedSet.addAllIfAbsent</p>
<p>org.redisson.api.RScoredSortedSet.addAllIfExist</p>
<p>org.redisson.api.RScoredSortedSet.addAllIfGreater</p>
<p>org.redisson.api.RScoredSortedSet.addAllIfLess</p>
<p>org.redisson.api.RScoredSortedSet.addAndGetRank</p>
<p>org.redisson.api.RScoredSortedSet.addAndGetRevRank</p>
<p>org.redisson.api.RScoredSortedSet.addIfAbsent</p>
<p>org.redisson.api.RScoredSortedSet.addIfExists</p>
<p>org.redisson.api.RScoredSortedSet.addIfGreater</p>
<p>org.redisson.api.RScoredSortedSet.addIfLess</p>
<p>org.redisson.api.RScoredSortedSet.addScore</p>
<p>org.redisson.api.RScoredSortedSet.addScoreAndGetRank</p>
<p>org.redisson.api.RScoredSortedSet.addScoreAndGetRevRank</p>
<p>org.redisson.api.RScoredSortedSet.tryAdd</p>
<p>org.redisson.api.RScoredSortedSet.rangeTo</p>
<p>org.redisson.api.RScoredSortedSet.revRangeTo</p>
<br/>
<p>org.redisson.api.RSortable.sortTo</p>
<br/>
<p>org.redisson.api.RList.addAfter</p>
<p>org.redisson.api.RList.addBefore</p>
<p>org.redisson.api.RList.fastSet</p>
<br/>
</details>
<details><summary>Async Commands</summary>
<p>org.redisson.api.RGeoAsync.addAsync</p>
<p>org.redisson.api.RGeoAsync.addIfExistsAsync</p>
<p>org.redisson.api.RGeoAsync.storeSearchToAsync</p>
<p>org.redisson.api.RGeoAsync.storeSortedSearchToAsync</p>
<p>org.redisson.api.RGeoAsync.tryAddAsync</p>
<p>org.redisson.api.RGeoAsync.radiusStoreSortedToAsync</p>
<p>org.redisson.api.RGeoAsync.radiusStoreToAsync</p>
<br/>
<p>org.redisson.api.RSetAsync.addAllCountedAsync</p>
<p>org.redisson.api.RSetAsync.tryAddAsync</p>
<p>org.redisson.api.RSetAsync.moveAsync</p>
<br/>
<p>org.redisson.api.RScoredSortedSetAsync.addAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addAllAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addAllIfAbsentAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addAllIfExistAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addAllIfGreaterAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addAllIfLessAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addAndGetRankAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addAndGetRevRankAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addIfAbsentAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addIfExistsAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addIfGreaterAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addIfLessAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addScoreAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addScoreAndGetRankAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.addScoreAndGetRevRankAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.tryAddAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.rangeToAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.revRangeToAsync</p>
<br/>
<p>org.redisson.api.RSortedSet.addAsync</p>
<br/>
<p>org.redisson.api.RSortableAsync.sortToAsync</p>
<br/>
<p>org.redisson.api.RListAsync.addAfterAsync</p>
<p>org.redisson.api.RListAsync.addAllAsync</p>
<p>org.redisson.api.RListAsync.addAsync</p>
<p>org.redisson.api.RListAsync.addBeforeAsync</p>
<p>org.redisson.api.RListAsync.fastSetAsync</p>
<p>org.redisson.api.RListAsync.setAsync</p>
<br/>
</details>
<details><summary>Reactive Commands</summary>
<p>org.redisson.api.RGeoReactive.add</p>
<p>org.redisson.api.RGeoReactive.addIfExists</p>
<p>org.redisson.api.RGeoReactive.storeSearchTo</p>
<p>org.redisson.api.RGeoReactive.storeSortedSearchTo</p>
<p>org.redisson.api.RGeoReactive.tryAdd</p>
<p>org.redisson.api.RGeoReactive.radiusStoreSortedTo</p>
<p>org.redisson.api.RGeoReactive.radiusStoreTo</p>
<br/>
<p>org.redisson.api.RSetReactive.addAllCounted</p>
<p>org.redisson.api.RSetReactive.tryAdd</p>
<p>org.redisson.api.RSetReactive.move</p>
<br/>
<p>org.redisson.api.RScoredSortedSetReactive.add</p>
<p>org.redisson.api.RScoredSortedSetReactive.addAll</p>
<p>org.redisson.api.RScoredSortedSetReactive.addAllIfAbsent</p>
<p>org.redisson.api.RScoredSortedSetReactive.addAllIfExist</p>
<p>org.redisson.api.RScoredSortedSetReactive.addAllIfGreater</p>
<p>org.redisson.api.RScoredSortedSetReactive.addAllIfLess</p>
<p>org.redisson.api.RScoredSortedSetReactive.addAndGetRank</p>
<p>org.redisson.api.RScoredSortedSetReactive.addAndGetRevRank</p>
<p>org.redisson.api.RScoredSortedSetReactive.addIfAbsent</p>
<p>org.redisson.api.RScoredSortedSetReactive.addIfExists</p>
<p>org.redisson.api.RScoredSortedSetReactive.addIfGreater</p>
<p>org.redisson.api.RScoredSortedSetReactive.addIfLess</p>
<p>org.redisson.api.RScoredSortedSetReactive.addScore</p>
<p>org.redisson.api.RScoredSortedSetReactive.addScoreAndGetRank</p>
<p>org.redisson.api.RScoredSortedSetReactive.addScoreAndGetRevRank</p>
<p>org.redisson.api.RScoredSortedSetReactive.tryAdd</p>
<p>org.redisson.api.RScoredSortedSetReactive.rangeTo</p>
<p>org.redisson.api.RScoredSortedSetReactive.revRangeTo</p>
<br/>
<p>org.redisson.api.RSortableReactive.sortTo</p>
<br/>
<p>org.redisson.api.RListReactive.add</p>
<p>org.redisson.api.RListReactive.addAfter</p>
<p>org.redisson.api.RListReactive.addAll</p>
<p>org.redisson.api.RListReactive.addBefore</p>
<p>org.redisson.api.RListReactive.fastSet</p>
<p>org.redisson.api.RListReactive.set</p>
<br/>
</details>
<details><summary>Rx Commands</summary>
<p>org.redisson.api.RGeoRx.add</p>
<p>org.redisson.api.RGeoRx.addIfExists</p>
<p>org.redisson.api.RGeoRx.storeSearchTo</p>
<p>org.redisson.api.RGeoRx.storeSortedSearchTo</p>
<p>org.redisson.api.RGeoRx.tryAdd</p>
<p>org.redisson.api.RGeoRx.radiusStoreSortedTo</p>
<p>org.redisson.api.RGeoRx.radiusStoreTo</p>
<br/>
<p>org.redisson.api.RSetRx.addAllCounted</p>
<p>org.redisson.api.RSetRx.tryAdd</p>
<p>org.redisson.api.RSetRx.move</p>
<br/>
<p>org.redisson.api.RScoredSortedSetRx.add</p>
<p>org.redisson.api.RScoredSortedSetRx.addAll</p>
<p>org.redisson.api.RScoredSortedSetRx.addAllIfAbsent</p>
<p>org.redisson.api.RScoredSortedSetRx.addAllIfExist</p>
<p>org.redisson.api.RScoredSortedSetRx.addAllIfGreater</p>
<p>org.redisson.api.RScoredSortedSetRx.addAllIfLess</p>
<p>org.redisson.api.RScoredSortedSetRx.addAndGetRank</p>
<p>org.redisson.api.RScoredSortedSetRx.addAndGetRevRank</p>
<p>org.redisson.api.RScoredSortedSetRx.addIfAbsent</p>
<p>org.redisson.api.RScoredSortedSetRx.addIfExists</p>
<p>org.redisson.api.RScoredSortedSetRx.addIfGreater</p>
<p>org.redisson.api.RScoredSortedSetRx.addIfLess</p>
<p>org.redisson.api.RScoredSortedSetRx.addScore</p>
<p>org.redisson.api.RScoredSortedSetRx.addScoreAndGetRank</p>
<p>org.redisson.api.RScoredSortedSetRx.addScoreAndGetRevRank</p>
<p>org.redisson.api.RScoredSortedSetRx.tryAdd</p>
<p>org.redisson.api.RScoredSortedSetRx.rangeTo</p>
<p>org.redisson.api.RScoredSortedSetRx.revRangeTo</p>
<br/>
<p>org.redisson.api.RSortableRx.sortTo</p>
<br/>
<p>org.redisson.api.RListRx.add</p>
<p>org.redisson.api.RListRx.addAfter</p>
<p>org.redisson.api.RListRx.addAll</p>
<p>org.redisson.api.RListRx.addBefore</p>
<p>org.redisson.api.RListRx.fastSet</p>
<p>org.redisson.api.RListRx.set</p>
<br/>
</details>
</details>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<details><summary>Jedis Base Commands</summary>
<p>redis.clients.jedis.Jedis.scan</p>
<p>redis.clients.jedis.Jedis.keys</p>
<p>redis.clients.jedis.Jedis.exists</p>
<p>redis.clients.jedis.Jedis.sort</p>
<p>redis.clients.jedis.Jedis.expireTime</p>
<p>redis.clients.jedis.Jedis.geodist</p>
<p>redis.clients.jedis.Jedis.geohash</p>
<p>redis.clients.jedis.Jedis.geopos</p>
<p>redis.clients.jedis.Jedis.georadius</p>
<p>redis.clients.jedis.Jedis.georadiusByMember</p>
<p>redis.clients.jedis.Jedis.georadiusByMemberReadonly</p>
<p>redis.clients.jedis.Jedis.georadiusReadonly</p>
<p>redis.clients.jedis.Jedis.geosearch</p>
<p>redis.clients.jedis.Jedis.hexists</p>
<p>redis.clients.jedis.Jedis.hget</p>
<p>redis.clients.jedis.Jedis.hgetAll</p>
<p>redis.clients.jedis.Jedis.hkeys</p>
<p>redis.clients.jedis.Jedis.hlen</p>
<p>redis.clients.jedis.Jedis.hmget</p>
<p>redis.clients.jedis.Jedis.hrandfield</p>
<p>redis.clients.jedis.Jedis.hrandfieldWithValues</p>
<p>redis.clients.jedis.Jedis.hscan</p>
<p>redis.clients.jedis.Jedis.hstrlen</p>
<p>redis.clients.jedis.Jedis.hvals</p>
<p>redis.clients.jedis.Jedis.lindex</p>
<p>redis.clients.jedis.Jedis.llen</p>
<p>redis.clients.jedis.Jedis.lpos</p>
<p>redis.clients.jedis.Jedis.lrange</p>
<p>redis.clients.jedis.Jedis.bitfield</p>
<p>redis.clients.jedis.Jedis.bitfieldReadonly</p>
<p>redis.clients.jedis.Jedis.bitpos</p>
<p>redis.clients.jedis.Jedis.bitcount</p>
<p>redis.clients.jedis.Jedis.getbit</p>
<p>redis.clients.jedis.Jedis.getSet</p>
<p>redis.clients.jedis.Jedis.getDel</p>
<p>redis.clients.jedis.Jedis.getrange</p>
<p>redis.clients.jedis.Jedis.getEx</p>
<p>redis.clients.jedis.Jedis.get</p>
<p>redis.clients.jedis.Jedis.strlen</p>
<p>redis.clients.jedis.Jedis.setGet</p>
<p>redis.clients.jedis.Jedis.scard</p>
<p>redis.clients.jedis.Jedis.sismember</p>
<p>redis.clients.jedis.Jedis.smismember</p>
<p>redis.clients.jedis.Jedis.srandmember</p>
<p>redis.clients.jedis.Jedis.sscan</p>
<p>redis.clients.jedis.Jedis.zscore</p>
<p>redis.clients.jedis.Jedis.zcount</p>
<p>redis.clients.jedis.Jedis.zlexcount</p>
<p>redis.clients.jedis.Jedis.zmscore</p>
<p>redis.clients.jedis.Jedis.zrandmember</p>
<p>redis.clients.jedis.Jedis.zrandmemberWithScores</p>
<p>redis.clients.jedis.Jedis.zrange</p>
<p>redis.clients.jedis.Jedis.zrangeByLex</p>
<p>redis.clients.jedis.Jedis.zrangeByScore</p>
<p>redis.clients.jedis.Jedis.zrangeByScoreWithScores</p>
<p>redis.clients.jedis.Jedis.zrangeWithScores</p>
<p>redis.clients.jedis.Jedis.zrank</p>
<p>redis.clients.jedis.Jedis.zrevrange</p>
<p>redis.clients.jedis.Jedis.zrevrangeByLex</p>
<p>redis.clients.jedis.Jedis.zrevrangeByScore</p>
<p>redis.clients.jedis.Jedis.zrevrangeByScoreWithScores</p>
<p>redis.clients.jedis.Jedis.zrevrangeWithScores</p>
<p>redis.clients.jedis.Jedis.zrevrank</p>
<p>redis.clients.jedis.Jedis.zscan</p>
<p>redis.clients.jedis.Jedis.sdiff</p>
<p>redis.clients.jedis.Jedis.sinter</p>
<p>redis.clients.jedis.Jedis.sunion</p>
<p>redis.clients.jedis.Jedis.zdiff</p>
<p>redis.clients.jedis.Jedis.zinter</p>
<p>redis.clients.jedis.Jedis.zunion</p>
<p>redis.clients.jedis.Jedis.sdiffstore</p>
<p>redis.clients.jedis.Jedis.sinterstore</p>
<p>redis.clients.jedis.Jedis.sunionstore</p>
<p>redis.clients.jedis.Jedis.zdiffStore</p>
<p>redis.clients.jedis.Jedis.zinterstore</p>
<p>redis.clients.jedis.Jedis.zunionstore</p>
<p>redis.clients.jedis.Jedis.smembers</p>
<p>redis.clients.jedis.Jedis.mget</p>
<p>redis.clients.jedis.Jedis.sintercard</p>
<p>redis.clients.jedis.Jedis.zcard</p>
<p>redis.clients.jedis.Jedis.zintercard</p>
<p>redis.clients.jedis.Jedis.zdiffWithScores</p>
<p>redis.clients.jedis.Jedis.zinterWithScores</p>
<p>redis.clients.jedis.Jedis.zunionWithScores</p>
<p>redis.clients.jedis.Jedis.zrangestore</p>
<p>redis.clients.jedis.Jedis.zrangestorebylex</p>
<p>redis.clients.jedis.Jedis.zrangestorebyscore</p>
<p>redis.clients.jedis.Jedis.zrevrangestore</p>
<p>redis.clients.jedis.Jedis.zrevrangestorebylex</p>
<p>redis.clients.jedis.Jedis.zrevrangestorebyscore</p>
<p>redis.clients.jedis.Jedis.xlen</p>
<p>redis.clients.jedis.Jedis.xpending</p>
<p>redis.clients.jedis.Jedis.xrange</p>
<p>redis.clients.jedis.Jedis.xrevrange</p>
</details>
<details><summary>Jedis Transaction Commands</summary>
<p>redis.clients.jedis.TransactionBase.scan</p>
<p>redis.clients.jedis.TransactionBase.keys</p>
<p>redis.clients.jedis.TransactionBase.exists</p>
<p>redis.clients.jedis.TransactionBase.sort</p>
<p>redis.clients.jedis.TransactionBase.expireTime</p>
<p>redis.clients.jedis.TransactionBase.geodist</p>
<p>redis.clients.jedis.TransactionBase.geohash</p>
<p>redis.clients.jedis.TransactionBase.geopos</p>
<p>redis.clients.jedis.TransactionBase.georadius</p>
<p>redis.clients.jedis.TransactionBase.georadiusByMember</p>
<p>redis.clients.jedis.TransactionBase.georadiusByMemberReadonly</p>
<p>redis.clients.jedis.TransactionBase.georadiusReadonly</p>
<p>redis.clients.jedis.TransactionBase.geosearch</p>
<p>redis.clients.jedis.TransactionBase.hexists</p>
<p>redis.clients.jedis.TransactionBase.hget</p>
<p>redis.clients.jedis.TransactionBase.hgetAll</p>
<p>redis.clients.jedis.TransactionBase.hkeys</p>
<p>redis.clients.jedis.TransactionBase.hlen</p>
<p>redis.clients.jedis.TransactionBase.hmget</p>
<p>redis.clients.jedis.TransactionBase.hrandfield</p>
<p>redis.clients.jedis.TransactionBase.hrandfieldWithValues</p>
<p>redis.clients.jedis.TransactionBase.hscan</p>
<p>redis.clients.jedis.TransactionBase.hstrlen</p>
<p>redis.clients.jedis.TransactionBase.hvals</p>
<p>redis.clients.jedis.TransactionBase.lindex</p>
<p>redis.clients.jedis.TransactionBase.llen</p>
<p>redis.clients.jedis.TransactionBase.lpos</p>
<p>redis.clients.jedis.TransactionBase.lrange</p>
<p>redis.clients.jedis.TransactionBase.bitfield</p>
<p>redis.clients.jedis.TransactionBase.bitfieldReadonly</p>
<p>redis.clients.jedis.TransactionBase.bitpos</p>
<p>redis.clients.jedis.TransactionBase.bitcount</p>
<p>redis.clients.jedis.TransactionBase.getbit</p>
<p>redis.clients.jedis.TransactionBase.getSet</p>
<p>redis.clients.jedis.TransactionBase.getDel</p>
<p>redis.clients.jedis.TransactionBase.getrange</p>
<p>redis.clients.jedis.TransactionBase.getEx</p>
<p>redis.clients.jedis.TransactionBase.get</p>
<p>redis.clients.jedis.TransactionBase.strlen</p>
<p>redis.clients.jedis.TransactionBase.setGet</p>
<p>redis.clients.jedis.TransactionBase.mget</p>
<p>redis.clients.jedis.TransactionBase.scard</p>
<p>redis.clients.jedis.TransactionBase.sismember</p>
<p>redis.clients.jedis.TransactionBase.smismember</p>
<p>redis.clients.jedis.TransactionBase.srandmember</p>
<p>redis.clients.jedis.TransactionBase.sscan</p>
<p>redis.clients.jedis.TransactionBase.zscore</p>
<p>redis.clients.jedis.TransactionBase.zcount</p>
<p>redis.clients.jedis.TransactionBase.zlexcount</p>
<p>redis.clients.jedis.TransactionBase.zmscore</p>
<p>redis.clients.jedis.TransactionBase.zrandmember</p>
<p>redis.clients.jedis.TransactionBase.zrandmemberWithScores</p>
<p>redis.clients.jedis.TransactionBase.zrange</p>
<p>redis.clients.jedis.TransactionBase.zrangeByLex</p>
<p>redis.clients.jedis.TransactionBase.zrangeByScore</p>
<p>redis.clients.jedis.TransactionBase.zrangeByScoreWithScores</p>
<p>redis.clients.jedis.TransactionBase.zrangeWithScores</p>
<p>redis.clients.jedis.TransactionBase.zrank</p>
<p>redis.clients.jedis.TransactionBase.zrevrange</p>
<p>redis.clients.jedis.TransactionBase.zrevrangeByLex</p>
<p>redis.clients.jedis.TransactionBase.zrevrangeByScore</p>
<p>redis.clients.jedis.TransactionBase.zrevrangeByScoreWithScores</p>
<p>redis.clients.jedis.TransactionBase.zrevrangeWithScores</p>
<p>redis.clients.jedis.TransactionBase.zrevrank</p>
<p>redis.clients.jedis.TransactionBase.zscan</p>
<p>redis.clients.jedis.TransactionBase.sdiff</p>
<p>redis.clients.jedis.TransactionBase.sinter</p>
<p>redis.clients.jedis.TransactionBase.sunion</p>
<p>redis.clients.jedis.TransactionBase.zdiff</p>
<p>redis.clients.jedis.TransactionBase.zinter</p>
<p>redis.clients.jedis.TransactionBase.zunion</p>
<p>redis.clients.jedis.TransactionBase.sdiffstore</p>
<p>redis.clients.jedis.TransactionBase.sinterstore</p>
<p>redis.clients.jedis.TransactionBase.sunionstore</p>
<p>redis.clients.jedis.TransactionBase.zdiffStore</p>
<p>redis.clients.jedis.TransactionBase.zinterstore</p>
<p>redis.clients.jedis.TransactionBase.zunionstore</p>
<p>redis.clients.jedis.TransactionBase.smembers</p>
<p>redis.clients.jedis.TransactionBase.sintercard</p>
<p>redis.clients.jedis.TransactionBase.zcard</p>
<p>redis.clients.jedis.TransactionBase.zintercard</p>
<p>redis.clients.jedis.TransactionBase.zdiffWithScores</p>
<p>redis.clients.jedis.TransactionBase.zinterWithScores</p>
<p>redis.clients.jedis.TransactionBase.zunionWithScores</p>
<p>redis.clients.jedis.TransactionBase.zrangestore</p>
<p>redis.clients.jedis.TransactionBase.zrangestorebylex</p>
<p>redis.clients.jedis.TransactionBase.zrangestorebyscore</p>
<p>redis.clients.jedis.TransactionBase.zrevrangestore</p>
<p>redis.clients.jedis.TransactionBase.zrevrangestorebylex</p>
<p>redis.clients.jedis.TransactionBase.zrevrangestorebyscore</p>
<p>redis.clients.jedis.TransactionBase.xlen</p>
<p>redis.clients.jedis.TransactionBase.xpending</p>
<p>redis.clients.jedis.TransactionBase.xrange</p>
<p>redis.clients.jedis.TransactionBase.xrevrange</p>
</details>
<details><summary>JedisCommands Commands</summary>
<p>redis.clients.jedis.commands.KeyCommands.scan</p>
<p>redis.clients.jedis.commands.KeyCommands.keys</p>
<p>redis.clients.jedis.commands.KeyCommands.exists</p>
<p>redis.clients.jedis.commands.KeyCommands.sort</p>
<p>redis.clients.jedis.commands.KeyCommands.expireTime</p>
<p>redis.clients.jedis.commands.GeoCommands.geodist</p>
<p>redis.clients.jedis.commands.GeoCommands.geohash</p>
<p>redis.clients.jedis.commands.GeoCommands.geopos</p>
<p>redis.clients.jedis.commands.GeoCommands.georadius</p>
<p>redis.clients.jedis.commands.GeoCommands.georadiusByMember</p>
<p>redis.clients.jedis.commands.GeoCommands.georadiusByMemberReadonly</p>
<p>redis.clients.jedis.commands.GeoCommands.georadiusReadonly</p>
<p>redis.clients.jedis.commands.GeoCommands.geosearch</p>
<p>redis.clients.jedis.commands.HashCommands.hexists</p>
<p>redis.clients.jedis.commands.HashCommands.hget</p>
<p>redis.clients.jedis.commands.HashCommands.hgetAll</p>
<p>redis.clients.jedis.commands.HashCommands.hkeys</p>
<p>redis.clients.jedis.commands.HashCommands.hlen</p>
<p>redis.clients.jedis.commands.HashCommands.hmget</p>
<p>redis.clients.jedis.commands.HashCommands.hrandfield</p>
<p>redis.clients.jedis.commands.HashCommands.hrandfieldWithValues</p>
<p>redis.clients.jedis.commands.HashCommands.hscan</p>
<p>redis.clients.jedis.commands.HashCommands.hstrlen</p>
<p>redis.clients.jedis.commands.HashCommands.hvals</p>
<p>redis.clients.jedis.commands.ListCommands.lindex</p>
<p>redis.clients.jedis.commands.ListCommands.llen</p>
<p>redis.clients.jedis.commands.ListCommands.lpos</p>
<p>redis.clients.jedis.commands.ListCommands.lrange</p>
<p>redis.clients.jedis.commands.StringCommands.bitfield</p>
<p>redis.clients.jedis.commands.StringCommands.bitfieldReadonly</p>
<p>redis.clients.jedis.commands.StringCommands.bitpos</p>
<p>redis.clients.jedis.commands.StringCommands.bitcount</p>
<p>redis.clients.jedis.commands.StringCommands.getbit</p>
<p>redis.clients.jedis.commands.StringCommands.getSet</p>
<p>redis.clients.jedis.commands.StringCommands.getDel</p>
<p>redis.clients.jedis.commands.StringCommands.getrange</p>
<p>redis.clients.jedis.commands.StringCommands.getEx</p>
<p>redis.clients.jedis.commands.StringCommands.get</p>
<p>redis.clients.jedis.commands.StringCommands.strlen</p>
<p>redis.clients.jedis.commands.StringCommands.setGet</p>
<p>redis.clients.jedis.commands.StringCommands.mget</p>
<p>redis.clients.jedis.commands.SetCommands.scard</p>
<p>redis.clients.jedis.commands.SetCommands.sismember</p>
<p>redis.clients.jedis.commands.SetCommands.smismember</p>
<p>redis.clients.jedis.commands.SetCommands.srandmember</p>
<p>redis.clients.jedis.commands.SetCommands.sscan</p>
<p>redis.clients.jedis.commands.SetCommands.smembers</p>
<p>redis.clients.jedis.commands.SetCommands.sdiff</p>
<p>redis.clients.jedis.commands.SetCommands.sinter</p>
<p>redis.clients.jedis.commands.SetCommands.sunion</p>
<p>redis.clients.jedis.commands.SetCommands.sdiffstore</p>
<p>redis.clients.jedis.commands.SetCommands.sinterstore</p>
<p>redis.clients.jedis.commands.SetCommands.sunionstore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zscore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zcount</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zlexcount</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zmscore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrandmember</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrandmemberWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrange</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangeByLex</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangeByScore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangeByScoreWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangeWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrank</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrange</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangeByLex</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangeByScore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangeByScoreWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangeWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrank</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zscan</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zdiff</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zinter</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zunion</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zdiffStore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zinterstore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zunionstore</p>
<p>redis.clients.jedis.commands.SetCommands.sintercard</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zcard</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zintercard</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zdiffWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zinterWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zunionWithScores</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangestore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangestorebylex</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrangestorebyscore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangestore</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangestorebylex</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrevrangestorebyscore</p>
<p>redis.clients.jedis.commands.StreamCommands.xlen</p>
<p>redis.clients.jedis.commands.StreamCommands.xpending</p>
<p>redis.clients.jedis.commands.StreamCommands.xrange</p>
<p>redis.clients.jedis.commands.StreamCommands.xrevrange</p>
</details>
<details><summary>Lettuce Commands</summary>
<details><summary>Sync Commands</summary>
<p>io.lettuce.core.api.sync.RedisListCommands.lindex</p>
<p>io.lettuce.core.api.sync.RedisListCommands.llen</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lpos</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lrange</p>
<br/>
<p>io.lettuce.core.api.sync.RedisStringCommands.getdel</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopAnd</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopNot</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopOr</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitopXor</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitpos</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.bitcount</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.get</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.getbit</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.getex</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.getrange</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.mget</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.strlen</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.setGet</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.getset</p>
<br/>
<p>io.lettuce.core.api.sync.RedisGeoCommands.geosearchstore</p>
<p>io.lettuce.core.api.sync.RedisGeoCommands.geodist</p>
<p>io.lettuce.core.api.sync.RedisGeoCommands.geohash</p>
<p>io.lettuce.core.api.sync.RedisGeoCommands.geopos</p>
<p>io.lettuce.core.api.sync.RedisGeoCommands.georadius</p>
<p>io.lettuce.core.api.sync.RedisGeoCommands.georadiusbymember</p>
<p>io.lettuce.core.api.sync.RedisGeoCommands.geosearch</p>
<br/>
<p>io.lettuce.core.api.sync.RedisHashCommands.hexists</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hget</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hgetall</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hkeys</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hvals</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hlen</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hmget</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hrandfield</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hrandfieldWithvalues</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hscan</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hstrlen</p>
<br/>
<p>io.lettuce.core.api.sync.RedisSetCommands.scard</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sismember</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sdiff</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sinter</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sunion</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.smembers</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.smismember</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.srandmember</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sscan</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sdiffstore</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sinterstore</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sunionstore</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.sintercard</p>
<br/>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zscore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zcount</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zlexcount</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zmscore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrandmember</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrandmemberWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrange</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangeByLex</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangeByScore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangeByScoreWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangeWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrank</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrange</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangeByLex</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangeByScore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangeByScoreWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangeWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrank</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zscan</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zdiff</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zinter</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zunion</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zdiffStore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zinterstore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zunionstore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zcard</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zintercard</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zdiffWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zinterWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zunionWithScores</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangestore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangestorebylex</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrangestorebyscore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangestore</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangestorebylex</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrevrangestorebyscore</p>
<br/>
<p>io.lettuce.core.api.sync.RedisStreamCommands.xlen</p>
<p>io.lettuce.core.api.sync.RedisStreamCommands.xpending</p>
<p>io.lettuce.core.api.sync.RedisStreamCommands.xrange</p>
<p>io.lettuce.core.api.sync.RedisStreamCommands.xrevrange</p>
</details>
<details><summary>Async Commands</summary>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lindex</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.llen</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lpos</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lrange</p>
<br/>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.getdel</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopAnd</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopNot</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopOr</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitopXor</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitpos</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.mget</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.bitcount</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.get</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.getbit</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.getex</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.getrange</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.strlen</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.setGet</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.getset</p>
<br/>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.geosearchstore</p>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.geodist</p>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.geohash</p>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.geopos</p>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.georadius</p>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.georadiusbymember</p>
<p>io.lettuce.core.api.async.RedisGeoAsyncCommands.geosearch</p>
<br/>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hexists</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hget</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hgetall</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hkeys</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hvals</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hlen</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hmget</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hrandfield</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hrandfieldWithvalues</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hscan</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hstrlen</p>
<br/>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.scard</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sismember</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sdiff</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sinter</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sunion</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.smembers</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.smismember</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.srandmember</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sscan</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sdiffstore</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sinterstore</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sunionstore</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.sintercard</p>
<br/>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zscore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zcount</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zlexcount</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zmscore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrandmember</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrandmemberWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrange</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangeByLex</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangeByScore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangeByScoreWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangeWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrank</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrange</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangeByLex</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangeByScore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangeByScoreWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangeWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrank</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zscan</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zdiff</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zinter</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zunion</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zdiffStore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zinterstore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zunionstore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zcard</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zintercard</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zdiffWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zinterWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zunionWithScores</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangestore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangestorebylex</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrangestorebyscore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangestore</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangestorebylex</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrevrangestorebyscore</p>
<br/>
<p>io.lettuce.core.api.async.RedisStreamAsyncCommands.xlen</p>
<p>io.lettuce.core.api.async.RedisStreamAsyncCommands.xpending</p>
<p>io.lettuce.core.api.async.RedisStreamAsyncCommands.xrange</p>
<p>io.lettuce.core.api.async.RedisStreamAsyncCommands.xrevrange</p>
</details>
<details><summary>Reactive Commands</summary>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lindex</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.llen</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lpos</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lrange</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.getdel</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopAnd</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopNot</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopOr</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitopXor</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitpos</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.mget</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.bitcount</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.get</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.getbit</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.getex</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.getrange</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.strlen</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.setGet</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.getset</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.geosearchstore</p>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.geodist</p>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.geohash</p>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.geopos</p>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.georadius</p>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.georadiusbymember</p>
<p>io.lettuce.core.api.reactive.RedisGeoReactiveCommands.geosearch</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hexists</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hget</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hgetall</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hkeys</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hvals</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hlen</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hmget</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hrandfield</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hrandfieldWithvalues</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hscan</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hstrlen</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.scard</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sismember</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sdiff</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sinter</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sunion</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.smembers</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.smismember</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.srandmember</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sscan</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sdiffstore</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sinterstore</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sunionstore</p>
<p>io.lettuce.core.api.reactive.RedisSetReactiveCommands.sintercard</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zscore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zcount</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zlexcount</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zmscore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrandmember</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrandmemberWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrange</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangeByLex</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangeByScore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangeByScoreWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangeWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrank</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrange</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangeByLex</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangeByScore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangeByScoreWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangeWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrank</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zscan</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zdiff</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zinter</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zunion</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zdiffStore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zinterstore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zunionstore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zcard</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zintercard</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zdiffWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zinterWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zunionWithScores</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangestore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangestorebylex</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrangestorebyscore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangestore</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangestorebylex</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrevrangestorebyscore</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisStreamReactiveCommands.xlen</p>
<p>io.lettuce.core.api.reactive.RedisStreamReactiveCommands.xpending</p>
<p>io.lettuce.core.api.reactive.RedisStreamReactiveCommands.xrange</p>
<p>io.lettuce.core.api.reactive.RedisStreamReactiveCommands.xrevrange</p>
</details>
</details>
<details><summary>Redisson Commands</summary>
<details><summary>Sync Commands</summary>
<p>org.redisson.api.RGeo.dist</p>
<p>org.redisson.api.RGeo.hash</p>
<p>org.redisson.api.RGeo.pos</p>
<p>org.redisson.api.RGeo.search</p>
<p>org.redisson.api.RGeo.searchWithDistance</p>
<p>org.redisson.api.RGeo.searchWithPosition</p>
<p>org.redisson.api.RGeo.storeSearchTo</p>
<p>org.redisson.api.RGeo.storeSortedSearchTo</p>
<p>org.redisson.api.RGeo.radius</p>
<p>org.redisson.api.RGeo.radiusStoreSortedTo</p>
<p>org.redisson.api.RGeo.radiusStoreTo</p>
<p>org.redisson.api.RGeo.radiusWithDistance</p>
<p>org.redisson.api.RGeo.radiusWithPosition</p>
<br/>
<p>org.redisson.api.RSet.containsEach</p>
<p>org.redisson.api.RSet.countIntersection</p>
<p>org.redisson.api.RSet.distributedIterator</p>
<p>org.redisson.api.RSet.iterator</p>
<p>org.redisson.api.RSet.mapReduce</p>
<p>org.redisson.api.RSet.random</p>
<p>org.redisson.api.RSet.readAll</p>
<p>org.redisson.api.RSet.stream</p>
<p>org.redisson.api.RSet.readDiff</p>
<p>org.redisson.api.RSet.readIntersection</p>
<p>org.redisson.api.RSet.readUnion</p>
<p>org.redisson.api.RSet.diff</p>
<p>org.redisson.api.RSet.intersection</p>
<p>org.redisson.api.RSet.union</p>
<br/>
<p>org.redisson.api.RScoredSortedSet.random</p>
<p>org.redisson.api.RScoredSortedSet.randomEntries</p>
<p>org.redisson.api.RScoredSortedSet.rank</p>
<p>org.redisson.api.RScoredSortedSet.rankEntry</p>
<p>org.redisson.api.RScoredSortedSet.revRank</p>
<p>org.redisson.api.RScoredSortedSet.revRankEntry</p>
<p>org.redisson.api.RScoredSortedSet.readAll</p>
<p>org.redisson.api.RScoredSortedSet.size</p>
<p>org.redisson.api.RScoredSortedSet.stream</p>
<p>org.redisson.api.RScoredSortedSet.toArray</p>
<p>org.redisson.api.RScoredSortedSet.valueRange</p>
<p>org.redisson.api.RScoredSortedSet.contains</p>
<p>org.redisson.api.RScoredSortedSet.containsAll</p>
<p>org.redisson.api.RScoredSortedSet.count</p>
<p>org.redisson.api.RScoredSortedSet.distributedIterator</p>
<p>org.redisson.api.RScoredSortedSet.entryIterator</p>
<p>org.redisson.api.RScoredSortedSet.entryRange</p>
<p>org.redisson.api.RScoredSortedSet.entryRangeReversed</p>
<p>org.redisson.api.RScoredSortedSet.first</p>
<p>org.redisson.api.RScoredSortedSet.firstEntry</p>
<p>org.redisson.api.RScoredSortedSet.firstScore</p>
<p>org.redisson.api.RScoredSortedSet.getScore</p>
<p>org.redisson.api.RScoredSortedSet.isEmpty</p>
<p>org.redisson.api.RScoredSortedSet.iterator</p>
<p>org.redisson.api.RScoredSortedSet.last</p>
<p>org.redisson.api.RScoredSortedSet.lastEntry</p>
<p>org.redisson.api.RScoredSortedSet.lastScore</p>
<p>org.redisson.api.RScoredSortedSet.countIntersection</p>
<p>org.redisson.api.RScoredSortedSet.readDiff</p>
<p>org.redisson.api.RScoredSortedSet.readIntersection</p>
<p>org.redisson.api.RScoredSortedSet.readUnion</p>
<p>org.redisson.api.RScoredSortedSet.rangeTo</p>
<p>org.redisson.api.RScoredSortedSet.revRangeTo</p>
<p>org.redisson.api.RScoredSortedSet.union</p>
<p>org.redisson.api.RScoredSortedSet.diff</p>
<p>org.redisson.api.RScoredSortedSet.intersection</p>
<br/>
<p>org.redisson.api.RSortedSet.distributedIterator</p>
<p>org.redisson.api.RSortedSet.readAll</p>
<br/>
<p>org.redisson.api.RSortable.readSort</p>
<p>org.redisson.api.RSortable.readSortAlpha</p>
<p>org.redisson.api.RSortable.sortTo</p>
<br/>
<p>org.redisson.api.RList.distributedIterator</p>
<p>org.redisson.api.RList.get</p>
<p>org.redisson.api.RList.mapReduce</p>
<p>org.redisson.api.RList.range</p>
<p>org.redisson.api.RList.readAll</p>
<p>org.redisson.api.RList.subList</p>
<br/>
</details>
<details><summary>Async Commands</summary>
<p>org.redisson.api.RGeoAsync.distAsync</p>
<p>org.redisson.api.RGeoAsync.hashAsync</p>
<p>org.redisson.api.RGeoAsync.posAsync</p>
<p>org.redisson.api.RGeoAsync.searchAsync</p>
<p>org.redisson.api.RGeoAsync.searchWithDistanceAsync</p>
<p>org.redisson.api.RGeoAsync.searchWithPositionAsync</p>
<p>org.redisson.api.RGeoAsync.storeSearchToAsync</p>
<p>org.redisson.api.RGeoAsync.storeSortedSearchToAsync</p>
<p>org.redisson.api.RGeoAsync.radiusAsync</p>
<p>org.redisson.api.RGeoAsync.radiusStoreSortedToAsync</p>
<p>org.redisson.api.RGeoAsync.radiusStoreToAsync</p>
<p>org.redisson.api.RGeoAsync.radiusWithDistanceAsync</p>
<p>org.redisson.api.RGeoAsync.radiusWithPositionAsync</p>
<br/>
<p>org.redisson.api.RSetAsync.containsEachAsync</p>
<p>org.redisson.api.RSetAsync.countIntersectionAsync</p>
<p>org.redisson.api.RSetAsync.iteratorAsync</p>
<p>org.redisson.api.RSetAsync.randomAsync</p>
<p>org.redisson.api.RSetAsync.readAllAsync</p>
<p>org.redisson.api.RSetAsync.readDiffAsync</p>
<p>org.redisson.api.RSetAsync.readIntersectionAsync</p>
<p>org.redisson.api.RSetAsync.readUnionAsync</p>
<p>org.redisson.api.RSetAsync.diffAsync</p>
<p>org.redisson.api.RSetAsync.intersectionAsync</p>
<p>org.redisson.api.RSetAsync.unionAsync</p>
<br/>
<p>org.redisson.api.RScoredSortedSetAsync.randomAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.randomEntriesAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.rankAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.rankEntryAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.revRankAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.revRankEntryAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.readAllAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.sizeAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.streamAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.toArrayAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.valueRangeAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.containsAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.containsAllAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.countAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.distributedIteratorAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.entryIteratorAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.entryRangeAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.entryRangeReversedAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.firstAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.firstEntryAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.firstScoreAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.getScoreAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.isEmptyAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.iteratorAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.lastAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.lastEntryAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.lastScoreAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.countIntersectionAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.readDiffAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.readIntersectionAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.readUnionAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.rangeToAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.revRangeToAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.unionAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.diffAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.intersectionAsync</p>
<br/>
<p>org.redisson.api.RSortedSet.readAllAsync</p>
<br/>
<p>org.redisson.api.RSortableAsync.readSortAlphaAsync</p>
<p>org.redisson.api.RSortableAsync.readSortAsync</p>
<p>org.redisson.api.RSortableAsync.sortToAsync</p>
<br/>
<p>org.redisson.api.RListAsync.getAsync</p>
<p>org.redisson.api.RListAsync.indexOfAsync</p>
<p>org.redisson.api.RListAsync.lastIndexOfAsync</p>
<p>org.redisson.api.RListAsync.rangeAsync</p>
<p>org.redisson.api.RListAsync.readAllAsync</p>
<br/>
</details>
<details><summary>Reactive Commands</summary>
<p>org.redisson.api.RGeoReactive.dist</p>
<p>org.redisson.api.RGeoReactive.hash</p>
<p>org.redisson.api.RGeoReactive.pos</p>
<p>org.redisson.api.RGeoReactive.search</p>
<p>org.redisson.api.RGeoReactive.searchWithDistance</p>
<p>org.redisson.api.RGeoReactive.searchWithPosition</p>
<p>org.redisson.api.RGeoReactive.storeSearchTo</p>
<p>org.redisson.api.RGeoReactive.storeSortedSearchTo</p>
<p>org.redisson.api.RGeoReactive.radius</p>
<p>org.redisson.api.RGeoReactive.radiusStoreSortedTo</p>
<p>org.redisson.api.RGeoReactive.radiusStoreTo</p>
<p>org.redisson.api.RGeoReactive.radiusWithDistance</p>
<p>org.redisson.api.RGeoReactive.radiusWithPosition</p>
<br/>
<p>org.redisson.api.RSetReactive.containsEach</p>
<p>org.redisson.api.RSetReactive.countIntersection</p>
<p>org.redisson.api.RSetReactive.iterator</p>
<p>org.redisson.api.RSetReactive.random</p>
<p>org.redisson.api.RSetReactive.readAll</p>
<p>org.redisson.api.RSetReactive.readDiff</p>
<p>org.redisson.api.RSetReactive.readIntersection</p>
<p>org.redisson.api.RSetReactive.readUnion</p>
<p>org.redisson.api.RSetReactive.diff</p>
<p>org.redisson.api.RSetReactive.intersection</p>
<p>org.redisson.api.RSetReactive.union</p>
<br/>
<p>org.redisson.api.RScoredSortedSetReactive.random</p>
<p>org.redisson.api.RScoredSortedSetReactive.randomEntries</p>
<p>org.redisson.api.RScoredSortedSetReactive.rank</p>
<p>org.redisson.api.RScoredSortedSetReactive.rankEntry</p>
<p>org.redisson.api.RScoredSortedSetReactive.revRank</p>
<p>org.redisson.api.RScoredSortedSetReactive.revRankEntry</p>
<p>org.redisson.api.RScoredSortedSetReactive.readAll</p>
<p>org.redisson.api.RScoredSortedSetReactive.size</p>
<p>org.redisson.api.RScoredSortedSetReactive.stream</p>
<p>org.redisson.api.RScoredSortedSetReactive.toArray</p>
<p>org.redisson.api.RScoredSortedSetReactive.valueRange</p>
<p>org.redisson.api.RScoredSortedSetReactive.contains</p>
<p>org.redisson.api.RScoredSortedSetReactive.containsAll</p>
<p>org.redisson.api.RScoredSortedSetReactive.count</p>
<p>org.redisson.api.RScoredSortedSetReactive.distributedIterator</p>
<p>org.redisson.api.RScoredSortedSetReactive.entryIterator</p>
<p>org.redisson.api.RScoredSortedSetReactive.entryRange</p>
<p>org.redisson.api.RScoredSortedSetReactive.entryRangeReversed</p>
<p>org.redisson.api.RScoredSortedSetReactive.first</p>
<p>org.redisson.api.RScoredSortedSetReactive.firstEntry</p>
<p>org.redisson.api.RScoredSortedSetReactive.firstScore</p>
<p>org.redisson.api.RScoredSortedSetReactive.getScore</p>
<p>org.redisson.api.RScoredSortedSetReactive.isEmpty</p>
<p>org.redisson.api.RScoredSortedSetReactive.iterator</p>
<p>org.redisson.api.RScoredSortedSetReactive.last</p>
<p>org.redisson.api.RScoredSortedSetReactive.lastEntry</p>
<p>org.redisson.api.RScoredSortedSetReactive.lastScore</p>
<p>org.redisson.api.RScoredSortedSetReactive.countIntersection</p>
<p>org.redisson.api.RScoredSortedSetReactive.readDiff</p>
<p>org.redisson.api.RScoredSortedSetReactive.readIntersection</p>
<p>org.redisson.api.RScoredSortedSetReactive.readUnion</p>
<p>org.redisson.api.RScoredSortedSetReactive.rangeTo</p>
<p>org.redisson.api.RScoredSortedSetReactive.revRangeTo</p>
<p>org.redisson.api.RScoredSortedSetReactive.union</p>
<p>org.redisson.api.RScoredSortedSetReactive.diff</p>
<p>org.redisson.api.RScoredSortedSetReactive.intersection</p>
<br/>
<p>org.redisson.api.RSortableReactive.readSorted</p>
<p>org.redisson.api.RSortableReactive.sortTo</p>
<br/>
<p>org.redisson.api.RListReactive.descendingIterator</p>
<p>org.redisson.api.RListReactive.get</p>
<p>org.redisson.api.RListReactive.indexOf</p>
<p>org.redisson.api.RListReactive.iterator</p>
<p>org.redisson.api.RListReactive.lastIndexOf</p>
<p>org.redisson.api.RListReactive.range</p>
<p>org.redisson.api.RListReactive.readAll</p>
<br/>
</details>
<details><summary>Rx Commands</summary>
<p>org.redisson.api.RGeoRx.dist</p>
<p>org.redisson.api.RGeoRx.hash</p>
<p>org.redisson.api.RGeoRx.pos</p>
<p>org.redisson.api.RGeoRx.search</p>
<p>org.redisson.api.RGeoRx.searchWithDistance</p>
<p>org.redisson.api.RGeoRx.searchWithPosition</p>
<p>org.redisson.api.RGeoRx.storeSearchTo</p>
<p>org.redisson.api.RGeoRx.storeSortedSearchTo</p>
<p>org.redisson.api.RGeoRx.radius</p>
<p>org.redisson.api.RGeoRx.radiusStoreSortedTo</p>
<p>org.redisson.api.RGeoRx.radiusStoreTo</p>
<p>org.redisson.api.RGeoRx.radiusWithDistance</p>
<p>org.redisson.api.RGeoRx.radiusWithPosition</p>
<br/>
<p>org.redisson.api.RSetRx.containsEach</p>
<p>org.redisson.api.RSetRx.countIntersection</p>
<p>org.redisson.api.RSetRx.iterator</p>
<p>org.redisson.api.RSetRx.random</p>
<p>org.redisson.api.RSetRx.readAll</p>
<p>org.redisson.api.RSetRx.readDiff</p>
<p>org.redisson.api.RSetRx.readIntersection</p>
<p>org.redisson.api.RSetRx.readUnion</p>
<p>org.redisson.api.RSetRx.diff</p>
<p>org.redisson.api.RSetRx.intersection</p>
<p>org.redisson.api.RSetRx.union</p>
<br/>
<p>org.redisson.api.RScoredSortedSetRx.random</p>
<p>org.redisson.api.RScoredSortedSetRx.randomEntries</p>
<p>org.redisson.api.RScoredSortedSetRx.rank</p>
<p>org.redisson.api.RScoredSortedSetRx.rankEntry</p>
<p>org.redisson.api.RScoredSortedSetRx.revRank</p>
<p>org.redisson.api.RScoredSortedSetRx.revRankEntry</p>
<p>org.redisson.api.RScoredSortedSetRx.readAll</p>
<p>org.redisson.api.RScoredSortedSetRx.size</p>
<p>org.redisson.api.RScoredSortedSetRx.stream</p>
<p>org.redisson.api.RScoredSortedSetRx.toArray</p>
<p>org.redisson.api.RScoredSortedSetRx.valueRange</p>
<p>org.redisson.api.RScoredSortedSetRx.contains</p>
<p>org.redisson.api.RScoredSortedSetRx.containsAll</p>
<p>org.redisson.api.RScoredSortedSetRx.count</p>
<p>org.redisson.api.RScoredSortedSetRx.distributedIterator</p>
<p>org.redisson.api.RScoredSortedSetRx.entryIterator</p>
<p>org.redisson.api.RScoredSortedSetRx.entryRange</p>
<p>org.redisson.api.RScoredSortedSetRx.entryRangeReversed</p>
<p>org.redisson.api.RScoredSortedSetRx.first</p>
<p>org.redisson.api.RScoredSortedSetRx.firstEntry</p>
<p>org.redisson.api.RScoredSortedSetRx.firstScore</p>
<p>org.redisson.api.RScoredSortedSetRx.getScore</p>
<p>org.redisson.api.RScoredSortedSetRx.isEmpty</p>
<p>org.redisson.api.RScoredSortedSetRx.iterator</p>
<p>org.redisson.api.RScoredSortedSetRx.last</p>
<p>org.redisson.api.RScoredSortedSetRx.lastEntry</p>
<p>org.redisson.api.RScoredSortedSetRx.lastScore</p>
<p>org.redisson.api.RScoredSortedSetRx.countIntersection</p>
<p>org.redisson.api.RScoredSortedSetRx.readDiff</p>
<p>org.redisson.api.RScoredSortedSetRx.readIntersection</p>
<p>org.redisson.api.RScoredSortedSetRx.readUnion</p>
<p>org.redisson.api.RScoredSortedSetRx.rangeTo</p>
<p>org.redisson.api.RScoredSortedSetRx.revRangeTo</p>
<p>org.redisson.api.RScoredSortedSetRx.union</p>
<p>org.redisson.api.RScoredSortedSetRx.diff</p>
<p>org.redisson.api.RScoredSortedSetRx.intersection</p>
<br/>
<p>org.redisson.api.RSortableRx.readSorted</p>
<p>org.redisson.api.RSortableRx.sortTo</p>
<br/>
<p>org.redisson.api.RListRx.descendingIterator</p>
<p>org.redisson.api.RListRx.get</p>
<p>org.redisson.api.RListRx.indexOf</p>
<p>org.redisson.api.RListRx.iterator</p>
<p>org.redisson.api.RListRx.lastIndexOf</p>
<p>org.redisson.api.RListRx.range</p>
<p>org.redisson.api.RListRx.readAll</p>
<br/>
</details>
</details>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<details><summary>Jedis Base Commands</summary>
<p>redis.clients.jedis.Jedis.del</p>
<p>redis.clients.jedis.Jedis.hdel</p>
<p>redis.clients.jedis.Jedis.lpop</p>
<p>redis.clients.jedis.Jedis.lrem</p>
<p>redis.clients.jedis.Jedis.ltrim</p>
<p>redis.clients.jedis.Jedis.rpop</p>
<p>redis.clients.jedis.Jedis.blpop</p>
<p>redis.clients.jedis.Jedis.brpop</p>
<p>redis.clients.jedis.Jedis.brpoplpush</p>
<p>redis.clients.jedis.Jedis.lmove</p>
<p>redis.clients.jedis.Jedis.blmove</p>
<p>redis.clients.jedis.Jedis.rpoplpush</p>
<p>redis.clients.jedis.Jedis.lpop</p>
<p>redis.clients.jedis.Jedis.rpop</p>
<p>redis.clients.jedis.Jedis.getDel</p>
<p>redis.clients.jedis.Jedis.spop</p>
<p>redis.clients.jedis.Jedis.srem</p>
<p>redis.clients.jedis.Jedis.smove</p>
<p>redis.clients.jedis.Jedis.bzpopmax</p>
<p>redis.clients.jedis.Jedis.bzpopmin</p>
<p>redis.clients.jedis.Jedis.zpopmax</p>
<p>redis.clients.jedis.Jedis.zpopmin</p>
<p>redis.clients.jedis.Jedis.zrem</p>
<p>redis.clients.jedis.Jedis.zremrangeByLex</p>
<p>redis.clients.jedis.Jedis.zremrangeByRank</p>
<p>redis.clients.jedis.Jedis.zremrangeByScore</p>
<p>redis.clients.jedis.Jedis.xtrim</p>
<p>redis.clients.jedis.Jedis.xdel</p>
</details>
<details><summary>Jedis Transaction Commands</summary>
<p>redis.clients.jedis.TransactionBase.del</p>
<p>redis.clients.jedis.TransactionBase.hdel</p>
<p>redis.clients.jedis.TransactionBase.lpop</p>
<p>redis.clients.jedis.TransactionBase.lrem</p>
<p>redis.clients.jedis.TransactionBase.ltrim</p>
<p>redis.clients.jedis.TransactionBase.rpop</p>
<p>redis.clients.jedis.TransactionBase.blpop</p>
<p>redis.clients.jedis.TransactionBase.brpop</p>
<p>redis.clients.jedis.TransactionBase.brpoplpush</p>
<p>redis.clients.jedis.TransactionBase.lmove</p>
<p>redis.clients.jedis.TransactionBase.blmove</p>
<p>redis.clients.jedis.TransactionBase.rpoplpush</p>
<p>redis.clients.jedis.TransactionBase.lpop</p>
<p>redis.clients.jedis.TransactionBase.rpop</p>
<p>redis.clients.jedis.TransactionBase.getDel</p>
<p>redis.clients.jedis.TransactionBase.spop</p>
<p>redis.clients.jedis.TransactionBase.srem</p>
<p>redis.clients.jedis.TransactionBase.smove</p>
<p>redis.clients.jedis.TransactionBase.bzpopmax</p>
<p>redis.clients.jedis.TransactionBase.bzpopmin</p>
<p>redis.clients.jedis.TransactionBase.zpopmax</p>
<p>redis.clients.jedis.TransactionBase.zpopmin</p>
<p>redis.clients.jedis.TransactionBase.zrem</p>
<p>redis.clients.jedis.TransactionBase.zremrangeByLex</p>
<p>redis.clients.jedis.TransactionBase.zremrangeByRank</p>
<p>redis.clients.jedis.TransactionBase.zremrangeByScore</p>
<p>redis.clients.jedis.TransactionBase.xtrim</p>
<p>redis.clients.jedis.TransactionBase.xdel</p>
</details>
<details><summary>JedisCommands Commands</summary>
<p>redis.clients.jedis.commands.KeyCommands.del</p>
<p>redis.clients.jedis.commands.HashCommands.hdel</p>
<p>redis.clients.jedis.commands.ListCommands.lpop</p>
<p>redis.clients.jedis.commands.ListCommands.lrem</p>
<p>redis.clients.jedis.commands.ListCommands.ltrim</p>
<p>redis.clients.jedis.commands.ListCommands.rpop</p>
<p>redis.clients.jedis.commands.ListCommands.blpop</p>
<p>redis.clients.jedis.commands.ListCommands.brpop</p>
<p>redis.clients.jedis.commands.ListCommands.brpoplpush</p>
<p>redis.clients.jedis.commands.ListCommands.lmove</p>
<p>redis.clients.jedis.commands.ListCommands.blmove</p>
<p>redis.clients.jedis.commands.ListCommands.rpoplpush</p>
<p>redis.clients.jedis.commands.ListCommands.lpop</p>
<p>redis.clients.jedis.commands.ListCommands.rpop</p>
<p>redis.clients.jedis.commands.StringCommands.getDel</p>
<p>redis.clients.jedis.commands.SetCommands.spop</p>
<p>redis.clients.jedis.commands.SetCommands.srem</p>
<p>redis.clients.jedis.commands.SetCommands.smove</p>
<p>redis.clients.jedis.commands.SortedSetCommands.bzpopmax</p>
<p>redis.clients.jedis.commands.SortedSetCommands.bzpopmin</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zpopmax</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zpopmin</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zrem</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zremrangeByLex</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zremrangeByRank</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zremrangeByScore</p>
<p>redis.clients.jedis.commands.StreamCommands.xtrim</p>
<p>redis.clients.jedis.commands.StreamCommands.xdel</p>
</details>
<details><summary>Lettuce Commands</summary>
<details><summary>Sync Commands</summary>
<p>io.lettuce.core.api.sync.RedisListCommands.blmove</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lmove</p>
<p>io.lettuce.core.api.sync.RedisListCommands.rpoplpush</p>
<p>io.lettuce.core.api.sync.RedisListCommands.brpoplpush</p>
<p>io.lettuce.core.api.sync.RedisListCommands.blmpop</p>
<p>io.lettuce.core.api.sync.RedisListCommands.blpop</p>
<p>io.lettuce.core.api.sync.RedisListCommands.brpop</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lmpop</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lpop</p>
<p>io.lettuce.core.api.sync.RedisListCommands.rpop</p>
<p>io.lettuce.core.api.sync.RedisListCommands.lrem</p>
<p>io.lettuce.core.api.sync.RedisListCommands.ltrim</p>
<br/>
<p>io.lettuce.core.api.sync.RedisStringCommands.getdel</p>
<br/>
<p>io.lettuce.core.api.sync.RedisHashCommands.hdel</p>
<br/>
<p>io.lettuce.core.api.sync.RedisSetCommands.spop</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.srem</p>
<p>io.lettuce.core.api.sync.RedisSetCommands.smove</p>
<br/>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.bzpopmax</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.bzpopmin</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zpopmax</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zpopmin</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zrem</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zremrangeByLex</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zremrangeByRank</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zremrangeByScore</p>
<br/>
<p>io.lettuce.core.api.sync.RedisStreamCommands.xtrim</p>
<p>io.lettuce.core.api.sync.RedisStreamCommands.xdel</p>
</details>
<details><summary>Async Commands</summary>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.blmove</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lmove</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.rpoplpush</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.brpoplpush</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.blmpop</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.blpop</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.brpop</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lmpop</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lpop</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.rpop</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.lrem</p>
<p>io.lettuce.core.api.async.RedisListAsyncCommands.ltrim</p>
<br/>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.getdel</p>
<br/>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hdel</p>
<br/>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.spop</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.srem</p>
<p>io.lettuce.core.api.async.RedisSetAsyncCommands.smove</p>
<br/>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.bzpopmax</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.bzpopmin</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zpopmax</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zpopmin</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zrem</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zremrangeByLex</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zremrangeByRank</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zremrangeByScore</p>
<br/>
<p>io.lettuce.core.api.async.RedisStreamAsyncCommands.xtrim</p>
<p>io.lettuce.core.api.async.RedisStreamAsyncCommands.xdel</p>
</details>
<details><summary>Reactive Commands</summary>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.blmove</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lmove</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.rpoplpush</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.brpoplpush</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.blmpop</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.blpop</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.brpop</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lmpop</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lpop</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.rpop</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.lrem</p>
<p>io.lettuce.core.api.reactive.RedisListReactiveCommands.ltrim</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.getdel</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hdel</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.spop</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.srem</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.smove</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.bzpopmax</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.bzpopmin</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zpopmax</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zpopmin</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zrem</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zremrangeByLex</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zremrangeByRank</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zremrangeByScore</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisStreamReactiveCommands.xtrim</p>
<p>io.lettuce.core.api.reactive.RedisStreamReactiveCommands.xdel</p>
</details>
</details>
<details><summary>Redisson Commands</summary>
<details><summary>Sync Commands</summary>
<p>org.redisson.api.RSet.removeAllCounted</p>
<p>org.redisson.api.RSet.removeRandom</p>
<p>org.redisson.api.RSet.move</p>
<br/>
<p>org.redisson.api.RScoredSortedSet.clear</p>
<p>org.redisson.api.RScoredSortedSet.pollFirst</p>
<p>org.redisson.api.RScoredSortedSet.pollFirstEntries</p>
<p>org.redisson.api.RScoredSortedSet.pollFirstEntriesFromAny</p>
<p>org.redisson.api.RScoredSortedSet.pollFirstEntry</p>
<p>org.redisson.api.RScoredSortedSet.pollFirstFromAny</p>
<p>org.redisson.api.RScoredSortedSet.pollLast</p>
<p>org.redisson.api.RScoredSortedSet.pollLastEntries</p>
<p>org.redisson.api.RScoredSortedSet.pollLastEntriesFromAny</p>
<p>org.redisson.api.RScoredSortedSet.pollLastEntry</p>
<p>org.redisson.api.RScoredSortedSet.pollLastFromAny</p>
<p>org.redisson.api.RScoredSortedSet.takeFirst</p>
<p>org.redisson.api.RScoredSortedSet.takeLast</p>
<p>org.redisson.api.RScoredSortedSet.remove</p>
<p>org.redisson.api.RScoredSortedSet.removeAll</p>
<p>org.redisson.api.RScoredSortedSet.removeRangeByRank</p>
<p>org.redisson.api.RScoredSortedSet.removeRangeByScore</p>
<br/>
<p>org.redisson.api.RList.fastRemove</p>
<p>org.redisson.api.RList.remove</p>
<p>org.redisson.api.RList.trim</p>
<br/>
</details>
<details><summary>Async Commands</summary>
<p>org.redisson.api.RSetAsync.removeAllCountedAsync</p>
<p>org.redisson.api.RSetAsync.removeRandomAsync</p>
<p>org.redisson.api.RSetAsync.moveAsync</p>
<br/>
<p>org.redisson.api.RScoredSortedSetAsync.pollFirstAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollFirstEntriesAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollFirstEntriesFromAnyAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollFirstEntryAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollFirstFromAnyAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollLastAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollLastEntriesAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollLastEntriesFromAnyAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollLastEntryAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.pollLastFromAnyAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.takeFirstAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.takeLastAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.removeAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.removeAllAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.removeRangeByRankAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.removeRangeByScoreAsync</p>
<br/>
<p>org.redisson.api.RSortedSet.removeAsync</p>
<br/>
<p>org.redisson.api.RListAsync.fastRemoveAsync</p>
<p>org.redisson.api.RListAsync.removeAsync</p>
<p>org.redisson.api.RListAsync.trimAsync</p>
<br/>
</details>
<details><summary>Reactive Commands</summary>
<p>org.redisson.api.RSetReactive.removeAllCounted</p>
<p>org.redisson.api.RSetReactive.removeRandom</p>
<p>org.redisson.api.RSetReactive.move</p>
<br/>
<p>org.redisson.api.RScoredSortedSetReactive.pollFirst</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollFirstEntries</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollFirstEntriesFromAny</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollFirstEntry</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollFirstFromAny</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollLast</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollLastEntries</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollLastEntriesFromAny</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollLastEntry</p>
<p>org.redisson.api.RScoredSortedSetReactive.pollLastFromAny</p>
<p>org.redisson.api.RScoredSortedSetReactive.takeFirst</p>
<p>org.redisson.api.RScoredSortedSetReactive.takeLast</p>
<p>org.redisson.api.RScoredSortedSetReactive.remove</p>
<p>org.redisson.api.RScoredSortedSetReactive.removeAll</p>
<p>org.redisson.api.RScoredSortedSetReactive.removeRangeByRank</p>
<p>org.redisson.api.RScoredSortedSetReactive.removeRangeByScore</p>
<br/>
<p>org.redisson.api.RListReactive.fastRemove</p>
<p>org.redisson.api.RListReactive.remove</p>
<p>org.redisson.api.RListReactive.trim</p>
<br/>
</details>
<details><summary>Rx Commands</summary>
<p>org.redisson.api.RSetRx.removeAllCounted</p>
<p>org.redisson.api.RSetRx.removeRandom</p>
<p>org.redisson.api.RSetRx.move</p>
<br/>
<p>org.redisson.api.RScoredSortedSetRx.pollFirst</p>
<p>org.redisson.api.RScoredSortedSetRx.pollFirstEntries</p>
<p>org.redisson.api.RScoredSortedSetRx.pollFirstEntriesFromAny</p>
<p>org.redisson.api.RScoredSortedSetRx.pollFirstEntry</p>
<p>org.redisson.api.RScoredSortedSetRx.pollFirstFromAny</p>
<p>org.redisson.api.RScoredSortedSetRx.pollLast</p>
<p>org.redisson.api.RScoredSortedSetRx.pollLastEntries</p>
<p>org.redisson.api.RScoredSortedSetRx.pollLastEntriesFromAny</p>
<p>org.redisson.api.RScoredSortedSetRx.pollLastEntry</p>
<p>org.redisson.api.RScoredSortedSetRx.pollLastFromAny</p>
<p>org.redisson.api.RScoredSortedSetRx.takeFirst</p>
<p>org.redisson.api.RScoredSortedSetRx.takeLast</p>
<p>org.redisson.api.RScoredSortedSetRx.remove</p>
<p>org.redisson.api.RScoredSortedSetRx.removeAll</p>
<p>org.redisson.api.RScoredSortedSetRx.removeRangeByRank</p>
<p>org.redisson.api.RScoredSortedSetRx.removeRangeByScore</p>
<br/>
<p>org.redisson.api.RListRx.fastRemove</p>
<p>org.redisson.api.RListRx.remove</p>
<p>org.redisson.api.RListRx.trim</p>
<br/>
</details>
</details>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<details><summary>Jedis Base Commands</summary>
<p>redis.clients.jedis.Jedis.hincrBy</p>
<p>redis.clients.jedis.Jedis.hincrByFloat</p>
<p>redis.clients.jedis.Jedis.decr</p>
<p>redis.clients.jedis.Jedis.decrBy</p>
<p>redis.clients.jedis.Jedis.incr</p>
<p>redis.clients.jedis.Jedis.incrBy</p>
<p>redis.clients.jedis.Jedis.incrByFloat</p>
<p>redis.clients.jedis.Jedis.zaddIncr</p>
<p>redis.clients.jedis.Jedis.zincrby</p>
</details>
<details><summary>Jedis Transaction Commands</summary>
<p>redis.clients.jedis.TransactionBase.hincrBy</p>
<p>redis.clients.jedis.TransactionBase.hincrByFloat</p>
<p>redis.clients.jedis.TransactionBase.decr</p>
<p>redis.clients.jedis.TransactionBase.decrBy</p>
<p>redis.clients.jedis.TransactionBase.incr</p>
<p>redis.clients.jedis.TransactionBase.incrBy</p>
<p>redis.clients.jedis.TransactionBase.incrByFloat</p>
<p>redis.clients.jedis.TransactionBase.zaddIncr</p>
<p>redis.clients.jedis.TransactionBase.zincrby</p>
</details>
<details><summary>JedisCommands Commands</summary>
<p>redis.clients.jedis.commands.HashCommands.hincrBy</p>
<p>redis.clients.jedis.commands.HashCommands.hincrByFloat</p>
<p>redis.clients.jedis.commands.StringCommands.decr</p>
<p>redis.clients.jedis.commands.StringCommands.decrBy</p>
<p>redis.clients.jedis.commands.StringCommands.incr</p>
<p>redis.clients.jedis.commands.StringCommands.incrBy</p>
<p>redis.clients.jedis.commands.StringCommands.incrByFloat</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zaddIncr</p>
<p>redis.clients.jedis.commands.SortedSetCommands.zincrby</p>
</details>
<details><summary>Lettuce Commands</summary>
<details><summary>Sync Commands</summary>
<p>io.lettuce.core.api.sync.RedisStringCommands.decr</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.decrby</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.incr</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.incrby</p>
<p>io.lettuce.core.api.sync.RedisStringCommands.incrbyfloat</p>
<br/>
<p>io.lettuce.core.api.sync.RedisHashCommands.hincrby</p>
<p>io.lettuce.core.api.sync.RedisHashCommands.hincrbyfloat</p>
<br/>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zaddIncr</p>
<p>io.lettuce.core.api.sync.RedisSortedSetCommands.zincrby</p>
</details>
<details><summary>Async Commands</summary>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.decr</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.decrby</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.incr</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.incrby</p>
<p>io.lettuce.core.api.async.RedisStringAsyncCommands.incrbyfloat</p>
<br/>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hincrby</p>
<p>io.lettuce.core.api.async.RedisHashAsyncCommands.hincrbyfloat</p>
<br/>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zaddIncr</p>
<p>io.lettuce.core.api.async.RedisSortedSetAsyncCommands.zincrby</p>
</details>
<details><summary>Reactive Commands</summary>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.decr</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.decrby</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.incr</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.incrby</p>
<p>io.lettuce.core.api.reactive.RedisStringReactiveCommands.incrbyfloat</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hincrby</p>
<p>io.lettuce.core.api.reactive.RedisHashReactiveCommands.hincrbyfloat</p>
<br/>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zaddIncr</p>
<p>io.lettuce.core.api.reactive.RedisSortedSetReactiveCommands.zincrby</p>
</details>
</details>
<details><summary>Redisson Commands</summary>
<details><summary>Sync Commands</summary>
<p>org.redisson.api.RSet.diff</p>
<p>org.redisson.api.RSet.intersection</p>
<p>org.redisson.api.RSet.union</p>
<br/>
<p>org.redisson.api.RScoredSortedSet.union</p>
<p>org.redisson.api.RScoredSortedSet.diff</p>
<p>org.redisson.api.RScoredSortedSet.intersection</p>
<br/>
</details>
<details><summary>Async Commands</summary>
<p>org.redisson.api.RSetAsync.diffAsync</p>
<p>org.redisson.api.RSetAsync.intersectionAsync</p>
<p>org.redisson.api.RSetAsync.unionAsync</p>
<br/>
<p>org.redisson.api.RScoredSortedSetAsync.unionAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.diffAsync</p>
<p>org.redisson.api.RScoredSortedSetAsync.intersectionAsync</p>
<br/>
</details>
<details><summary>Reactive Commands</summary>
<p>org.redisson.api.RSetReactive.diff</p>
<p>org.redisson.api.RSetReactive.intersection</p>
<p>org.redisson.api.RSetReactive.union</p>
<br/>
<p>org.redisson.api.RScoredSortedSetReactive.union</p>
<p>org.redisson.api.RScoredSortedSetReactive.diff</p>
<p>org.redisson.api.RScoredSortedSetReactive.intersection</p>
<br/>
</details>
<details><summary>Rx Commands</summary>
<p>org.redisson.api.RSetRx.diff</p>
<p>org.redisson.api.RSetRx.intersection</p>
<p>org.redisson.api.RSetRx.union</p>
<br/>
<p>org.redisson.api.RScoredSortedSetRx.union</p>
<p>org.redisson.api.RScoredSortedSetRx.diff</p>
<p>org.redisson.api.RScoredSortedSetRx.intersection</p>
<br/>
</details>
</details>
</div></td>
</tr>
</tbody>
</table>

## Objects

| Icon | Description |
|---|---|
| ![](666370693.png) | Java Redis Connection |
| ![](666370692.png) | Java Redis Collection |
| ![](666370691.png) | Java Unknown Redis Connection |
| ![](666370690.png) | Java Unknown Redis Collection |

## Links

| Link type | When is this created? | Methods Supported |
|---|---|---|
| belongsTo | From Redis collection object to Redis connection object  | - |
| useSelectLink | get | get<br>getbit<br>getrange<br>getex<br>hget<br>hgetAll<br>keys<br>lrange <br>mget<br>smembers<br>strlen<br>zscore<br>zrevrange |
| useInsertLink | getbit | geoadd<br>hmset<br>hset<br>hsetnx<br>lpush<br>rpush<br>sadd<br>set<br>zadd |
| useDeleteLink | getrange | del<br>hdel<br>lpop<br>lrem<br>rpop<br>spop<br>srem<br>zrem |
| useUpdateLink | getex | decr<br>decrby<br>hincrby<br>hincrbyfloat<br>incr<br>incrby<br>incrbyfloat |

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner (for example via CAST Enlighten). Some
examples are shown below.

### Redis Connection

#### Jedis Connection

``` java
public class RedisService {

public static String server = "localhost";

public Boolean addEmployee(Employee employee) {
Jedis jedis = new Jedis(server);
}

# for unknown 
public void addDeptInfo(String key, String data) {
Jedis jedis = new Jedis(RedisProperties.connection);
jedis.set(key, data);
}

}
```

![](666370689.png)

#### Lettuce Connection

``` java
public static void main(String[] args) {
        RedisClient client = RedisClient.create("redis://localhost");
        StatefulRedisConnection<String, String> connection = client.connect();
        
        RedisHashCommands<String, String> hashCommands = connection.sync().commands();
        
        KeyValueStreamingChannel<String, String> channel = new KeyValueStreamingChannel<String, String>() {
            @Override
            public void onKeyValue(String key, String value) {
                System.out.println(key + " => " + value);
            }
        };
}
```

![](666370688.png)

#### Redisson Connection

``` java
private static void initializeRedisson() {
    Config config = new Config();
    config.useSingleServer().setAddress("redis://127.0.0.1:6379");
    redissonClient = Redisson.create(config);
}
```

![](redission_connection.png)

### Insert Operation

#### Jedis Insert Operation

``` java
//adding employee in a redis list
public Boolean addEmployee(Employee employee) {
    Jedis jedis = new Jedis(server);
    Employee emp = new Employee();
    emp=getEmployee(employee.getEmployeeId());
    jedis.lpush("employeelist", employee.toString());
    return true;
}
 
 
//add departments in a set
public void addDept(String dept) {
    Jedis jedis = new Jedis(server);
    jedis.sadd("departments", dept);
}
```

![](666370687.png)

#### Lettuce Insert Operation

``` java
// Inserts field=value to myhash
public void set_operation(RedisHashCommands hashCommands){
    hashCommands.hset("myhash", "field1", "value1");
    hashCommands.hset("myhash", "field2", "value2");
    hashCommands.hset("myhash", "field3", "value3");
}
```

![](666370686.png)

#### Redisson Insert Operation

``` java
public void addElement() throws ExecutionException, InterruptedException {
        RScoredSortedSetAsync<String> scoredSortedSet = redissonClient.getScoredSortedSet("exampleSortedSet");
        scoredSortedSet.addAsync(1.0, "Item1").thenAccept(result -> {
            if (result) {
                System.out.println("Element added successfully: " + element);
            } else {
                System.out.println("Element already exists: " + element);
            }
        }).get();
    }
```

![](Redisson_insert.png)

### Select Operation

#### Jedis Select Operation

``` java
//get all employees from employee list
public List<Employee> getAllEmployee(){
    List<Employee> employeeList = new ArrayList<>();
    Jedis jedis = new Jedis(server);
    List<String> employees = jedis.lrange("employeelist", 0, jedis.llen("employeelist"));
    for (String employee: employees) {
        String[] emp1=employee.split(",");
        Employee emp = new Employee(Integer.parseInt(emp1[0]), emp1[1], emp1[2]);
        employeeList.add(emp);
    }
    return employeeList;
}
 
 
//get all departments from set
public Set<String> getDepts() {
    Jedis jedis = new Jedis(server);
    return jedis.smembers("departments");
}
```

![](666370685.png)

#### Lettuce Select Operation

``` java
// retrieves all the data from myhash
public void get_operation(RedisHashCommands hashCommands, KeyValueStreamingChannel channel){
    hashCommands.hgetall(channel, "myhash");
}
```

![](666370684.png)

#### Redisson Select Operation

``` java
public void unionSets() throws ExecutionException, InterruptedException {
    RScoredSortedSetAsync<String> scoredSortedSet1 = redissonClient.getScoredSortedSet("exampleSortedSet");
    scoredSortedSet1.unionAsync("otherSortedSet1", "otherSortedSet2").thenAccept(result -> {
        if (result != null) {
            System.out.println("Union operation completed. Result set: " + "exampleSortedSet");
        } else {
            System.out.println("Union operation failed.");
        }
    }).get();
}
```

![](Redisson_select.png)

### Delete Operation

#### Jedis Delete Operation

``` java
// delete first visitor from visitors set
public Visitor deleteVisitor() {
    Jedis jedis = new Jedis(server);
    Map<String,Integer> visitors = get_Visitors();
    Visitor vis = new Visitor();
    for (Map.Entry<String, Integer> visitor : visitors.entrySet()) {
        vis.setName(visitor.getKey());
        vis.setId(jedis.zscore("visitors", visitor.getKey()));
        break;
    }
    jedis.zrem("visitors", vis.getName());
    return vis;
}
 
//delete department information from cache
public String deleteDeptInfo(String key) {
    Jedis jedis = new Jedis(server);
    String value = jedis.get(key);
    jedis.del(key);
    return key + " = " + value + " successfully deleted";
}
```

![](666370683.png)

#### Lettuce Delete Operation

``` java
// Deletes field3 and field4 from myhash
public void del_operation(RedisHashCommands hashCommands){
    hashCommands.hdel("myhash", "field3", "field4");
}
```

![](666370682.png)

#### Redisson Delete Operation

``` java
public void removeElement() throws ExecutionException, InterruptedException {
    RScoredSortedSetAsync<String> scoredSortedSet = redissonClient.getScoredSortedSet("exampleSortedSet");
    scoredSortedSet.removeAsync("Item1").thenAccept(result -> {
        if (result) {
            System.out.println("Element removed successfully: " + element);
        } else {
            System.out.println("Element not found: " + element);
        }
    }).get();
}
```

![](Redisson_delete.png)

### Update Operation

#### Jedis Update Operation

``` java
public class Example {
    public static void main(String[] args) {
    // Create a new Jedis instance connected to Redis
    Jedis jedis = new Jedis("localhost");

    // Set the initial value of a key "mycounter" to 10
    jedis.set("mycounter", "10");

    // Decrement the value of "mycounter" by 3 using decrBy method
    Long newCounterValue = jedis.decrBy("mycounter", 3);

    // Retrieving the elements of mycounter
    System.out.println("Elements in mycounter: " + jedis.smembers("mycounter"));

    // Close the Jedis instance
    jedis.close();
    }
}
```

![](666370681.png)

#### Lettuce Update Operation

``` java
// Updates myhash field4 by 10
public void update_operation(RedisHashCommands hashCommands){
    hashCommands.hincrby("myhash", "field4", 10);
}
```

![](666370680.png)

#### Redisson Update Operation

``` java
public void unionSets() throws ExecutionException, InterruptedException {
    RScoredSortedSetAsync<String> scoredSortedSet1 = redissonClient.getScoredSortedSet("exampleSortedSet");
    scoredSortedSet1.unionAsync("otherSortedSet1", "otherSortedSet2").thenAccept(result -> {
        if (result != null) {
            System.out.println("Union operation completed. Result set: " + "exampleSortedSet");
        } else {
            System.out.println("Union operation failed.");
        }
    }).get();
}
```

![](Redisson_update.png)

## Known Limitations

- Unknown Redis connection/collection objects are created in case of unresolved names.
