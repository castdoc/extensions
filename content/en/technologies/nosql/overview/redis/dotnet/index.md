---
title: "Support of Redis for .NET"
linkTitle: "Support of Redis for .NET"
type: "docs"
---

>CAST supports Redis via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about the support provided for .NET source code is discussed below.

## Supported Client Libraries

<table>
<colgroup>
<col/>
<col/>
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a
href="https://www.nuget.org/packages/StackExchange.Redis/"
rel="nofollow">StackExchange.Redis</a></td>
<td class="confluenceTd">2.6.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><a
href="https://www.nuget.org/packages/ServiceStack.Redis"
rel="nofollow">ServiceStack.Redis</a></td>
<td class="confluenceTd">6.8.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><a
href="https://www.nuget.org/packages/Microsoft.Extensions.Caching.Redis"
rel="nofollow">Microsoft.Extensions.Caching.Redis</a></td>
<td class="confluenceTd">2.2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><a
href="https://www.nuget.org/packages/Microsoft.Extensions.Caching.StackExchangeRedis"
rel="nofollow">Microsoft.Extensions.Caching.StackExchangeRedis</a></td>
<td class="confluenceTd">7.0.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table>
<colgroup>
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Scenario</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">StackExchange.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>StackExchange.Redis.IDatabase.GeoAdd</p>
<p>StackExchange.Redis.IDatabase.HyperLogLogAdd</p>
<p>StackExchange.Redis.IDatabase.ListInsertAfter</p>
<p>StackExchange.Redis.IDatabase.ListInsertBefore</p>
<p>StackExchange.Redis.IDatabase.ListRightPush</p>
<p>StackExchange.Redis.IDatabase.SortedSetAdd</p>
<p>StackExchange.Redis.IDatabase.SetAdd</p>
<p>StackExchange.Redis.IDatabase.StreamAdd</p>
<p>StackExchange.Redis.IDatabase.StringGetSet</p>
<p>StackExchange.Redis.IDatabase.StringGetSetExpiry</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoAddAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HyperLogLogAddAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListInsertAfterAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListInsertBeforeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListRightPushAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetAddAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetAddAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamAddAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetSetAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetSetExpiryAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">ServiceStack.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>ServiceStack.Redis.RedisClient.AddItemToList</p>
<p>ServiceStack.Redis.RedisClient.AddItemToSet</p>
<p>ServiceStack.Redis.RedisClient.AddRangeToList</p>
<p>ServiceStack.Redis.RedisClient.AddRangeToSet</p>
<p>ServiceStack.Redis.RedisClient.CreateHashSet</p>
<p>ServiceStack.Redis.RedisClient.PushItemToList</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">Microsoft.Extensions.Caching.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Extensions.Caching.Redis.RedisCache.Set</p>
<p>Microsoft.Extensions.Caching.Redis.RedisCache.SetAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td
class="confluenceTd">Microsoft.Extensions.Caching.StackExchangeRedis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.Set</p>
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.SetAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">StackExchange.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>StackExchange.Redis.IDatabase.StringIncrement</p>
<p>StackExchange.Redis.IDatabaseAsync.StringIncrementAsync</p>
<p>StackExchange.Redis.IDatabase.KeyMigrate</p>
<p>StackExchange.Redis.IDatabase.KeyRename</p>
<p>StackExchange.Redis.IDatabase.SortedSetUpdate</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyRenameAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetUpdateAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">ServiceStack.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>ServiceStack.Redis.RedisClient.IncrementValueInHash</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">StackExchange.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>StackExchange.Redis.IDatabase.GeoDistance</p>
<p>StackExchange.Redis.IDatabase.GeoHash</p>
<p>StackExchange.Redis.IDatabase.GeoPosition</p>
<p>StackExchange.Redis.IDatabase.GeoRadius</p>
<p>StackExchange.Redis.IDatabase.GeoSearch</p>
<p>StackExchange.Redis.IDatabase.GeoSearchAndStore</p>
<p>StackExchange.Redis.IDatabase.HashDecrement</p>
<p>StackExchange.Redis.IDatabase.SortedSetPop</p>
<p>StackExchange.Redis.IDatabase.HashExists</p>
<p>StackExchange.Redis.IDatabase.HashGet</p>
<p>StackExchange.Redis.IDatabase.HashGetLease</p>
<p>StackExchange.Redis.IDatabase.HashGetAll</p>
<p>StackExchange.Redis.IDatabase.HashIncrement</p>
<p>StackExchange.Redis.IDatabase.HashKeys</p>
<p>StackExchange.Redis.IDatabase.HashLength</p>
<p>StackExchange.Redis.IDatabase.HashRandomField</p>
<p>StackExchange.Redis.IDatabase.HashRandomFields</p>
<p>StackExchange.Redis.IDatabase.HashRandomFieldsWithValues</p>
<p>StackExchange.Redis.IDatabase.HashScan</p>
<p>StackExchange.Redis.IDatabase.HashSet</p>
<p>StackExchange.Redis.IDatabase.HashStringLength</p>
<p>StackExchange.Redis.IDatabase.HashValues</p>
<p>StackExchange.Redis.IDatabase.HyperLogLogLength</p>
<p>StackExchange.Redis.IDatabase.HyperLogLogMerge</p>
<p>StackExchange.Redis.IDatabase.IdentifyEndpoint</p>
<p>StackExchange.Redis.IDatabase.KeyCopy</p>
<p>StackExchange.Redis.IDatabase.KeyEncoding</p>
<p>StackExchange.Redis.IDatabase.KeyExists</p>
<p>StackExchange.Redis.IDatabase.KeyExpire</p>
<p>StackExchange.Redis.IDatabase.KeyExpireTime</p>
<p>StackExchange.Redis.IDatabase.KeyFrequency</p>
<p>StackExchange.Redis.IDatabase.KeyIdleTime</p>
<p>StackExchange.Redis.IDatabase.KeyMove</p>
<p>StackExchange.Redis.IDatabase.KeyPersist</p>
<p>StackExchange.Redis.IDatabase.KeyRandom</p>
<p>StackExchange.Redis.IDatabase.KeyRefCount</p>
<p>StackExchange.Redis.IDatabase.KeyRestore</p>
<p>StackExchange.Redis.IDatabase.KeyTimeToLive</p>
<p>StackExchange.Redis.IDatabase.KeyTouch</p>
<p>StackExchange.Redis.IDatabase.KeyType</p>
<p>StackExchange.Redis.IDatabase.ListGetByIndex</p>
<p>StackExchange.Redis.IDatabase.ListPosition</p>
<p>StackExchange.Redis.IDatabase.ListPositions</p>
<p>StackExchange.Redis.IDatabase.ListLeftPush</p>
<p>StackExchange.Redis.IDatabase.ListLength</p>
<p>StackExchange.Redis.IDatabase.ListMove</p>
<p>StackExchange.Redis.IDatabase.ListRange</p>
<p>StackExchange.Redis.IDatabase.ListSetByIndex</p>
<p>StackExchange.Redis.IDatabase.ListTrim</p>
<p>StackExchange.Redis.IDatabase.LockExtend</p>
<p>StackExchange.Redis.IDatabase.LockQuery</p>
<p>StackExchange.Redis.IDatabase.LockRelease</p>
<p>StackExchange.Redis.IDatabase.LockTake</p>
<p>StackExchange.Redis.IDatabase.Publish</p>
<p>StackExchange.Redis.IDatabase.Execute</p>
<p>StackExchange.Redis.IDatabase.ScriptEvaluate</p>
<p>StackExchange.Redis.IDatabase.ScriptEvaluateReadOnly</p>
<p>StackExchange.Redis.IDatabase.SetCombine</p>
<p>StackExchange.Redis.IDatabase.SetCombineAndStore</p>
<p>StackExchange.Redis.IDatabase.SetContains</p>
<p>StackExchange.Redis.IDatabase.SetIntersectionLength</p>
<p>StackExchange.Redis.IDatabase.SetLength</p>
<p>StackExchange.Redis.IDatabase.SetMembers</p>
<p>StackExchange.Redis.IDatabase.SetMove</p>
<p>StackExchange.Redis.IDatabase.SetRandomMember</p>
<p>StackExchange.Redis.IDatabase.SetRandomMembers</p>
<p>StackExchange.Redis.IDatabase.SetScan</p>
<p>StackExchange.Redis.IDatabase.Sort</p>
<p>StackExchange.Redis.IDatabase.SortAndStore</p>
<p>StackExchange.Redis.IDatabase.SortedSetCombine</p>
<p>StackExchange.Redis.IDatabase.SortedSetCombineWithScores</p>
<p>StackExchange.Redis.IDatabase.SortedSetCombineAndStore</p>
<p>StackExchange.Redis.IDatabase.SortedSetDecrement</p>
<p>StackExchange.Redis.IDatabase.SortedSetIntersectionLength</p>
<p>StackExchange.Redis.IDatabase.SortedSetLength</p>
<p>StackExchange.Redis.IDatabase.SortedSetLengthByValue</p>
<p>StackExchange.Redis.IDatabase.SortedSetRandomMember</p>
<p>StackExchange.Redis.IDatabase.SortedSetRandomMembers</p>
<p>StackExchange.Redis.IDatabase.SortedSetRandomMembersWithScores</p>
<p>StackExchange.Redis.IDatabase.SortedSetRangeByRank</p>
<p>StackExchange.Redis.IDatabase.SortedSetRank</p>
<p>StackExchange.Redis.IDatabase.SortedSetScore</p>
<p>StackExchange.Redis.IDatabase.StreamAcknowledge</p>
<p>StackExchange.Redis.IDatabase.StreamAutoClaim</p>
<p>StackExchange.Redis.IDatabase.StreamAutoClaimIdsOnly</p>
<p>StackExchange.Redis.IDatabase.StreamClaim</p>
<p>StackExchange.Redis.IDatabase.StreamClaimIdsOnly</p>
<p>StackExchange.Redis.IDatabase.StreamConsumerGroupSetPosition</p>
<p>StackExchange.Redis.IDatabase.StreamConsumerInfo</p>
<p>StackExchange.Redis.IDatabase.StreamCreateConsumerGroup</p>
<p>StackExchange.Redis.IDatabase.StreamGroupInfo</p>
<p>StackExchange.Redis.IDatabase.StreamInfo</p>
<p>StackExchange.Redis.IDatabase.StreamLength</p>
<p>StackExchange.Redis.IDatabase.StreamPending</p>
<p>StackExchange.Redis.IDatabase.StreamPendingMessages</p>
<p>StackExchange.Redis.IDatabase.StreamRange</p>
<p>StackExchange.Redis.IDatabase.StreamReadGroup</p>
<p>StackExchange.Redis.IDatabase.StreamTrim</p>
<p>StackExchange.Redis.IDatabase.StringAppend</p>
<p>StackExchange.Redis.IDatabase.StringBitCount</p>
<p>StackExchange.Redis.IDatabase.StringBitOperation</p>
<p>StackExchange.Redis.IDatabase.StringBitPosition</p>
<p>StackExchange.Redis.IDatabase.StringDecrement</p>
<p>StackExchange.Redis.IDatabase.StringGet</p>
<p>StackExchange.Redis.IDatabase.StringGetLease</p>
<p>StackExchange.Redis.IDatabase.StringGetBit</p>
<p>StackExchange.Redis.IDatabase.StringGetRange</p>
<p>StackExchange.Redis.IDatabase.StringGetWithExpiry</p>
<p>StackExchange.Redis.IDatabase.StringIncrement</p>
<p>StackExchange.Redis.IDatabase.StringLength</p>
<p>StackExchange.Redis.IDatabase.StringLongestCommonSubsequence</p>
<p>StackExchange.Redis.IDatabase.StringLongestCommonSubsequenceLength</p>
<p>StackExchange.Redis.IDatabase.StringLongestCommonSubsequenceWithMatches</p>
<p>StackExchange.Redis.IDatabase.StringSet</p>
<p>StackExchange.Redis.IDatabase.StringSetAndGet</p>
<p>StackExchange.Redis.IDatabase.StringSetBit</p>
<p>StackExchange.Redis.IDatabase.StringSetRange</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoDistanceAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoHashAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoPositionAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoRadiusAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoSearchAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoSearchAndStoreAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashDecrementAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetPopAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashExistsAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashGetAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashGetLeaseAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashGetAllAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashIncrementAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashKeysAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashRandomFieldAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashRandomFieldsAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashRandomFieldsWithValuesAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashScanAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashSetAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetScanAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashScanAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashStringLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashValuesAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HyperLogLogLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HyperLogLogMergeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.IdentifyEndpointAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyCopyAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyEncodingAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyExistsAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyExpireAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyExpireTimeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyFrequencyAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyIdleTimeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyMoveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyPersistAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyRandomAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyRefCountAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyRestoreAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyTimeToLiveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyTouchAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyTypeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListGetByIndexAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListPositionAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListPositionsAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListLeftPushAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListMoveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListRangeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListSetByIndexAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListTrimAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.LockExtendAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.LockQueryAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.LockReleaseAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.LockTakeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.PublishAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ExecuteAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ScriptEvaluateAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ScriptEvaluateReadOnlyAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetCombineAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetCombineAndStoreAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetContainsAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetIntersectionLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetMembersAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetMoveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetRandomMemberAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetRandomMembersAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetScanAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortAndStoreAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetCombineAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetCombineWithScoresAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetCombineAndStoreAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetDecrementAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetIntersectionLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetLengthByValueAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRandomMemberAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRandomMembersAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRandomMembersWithScoresAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRangeByRankAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRankAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetScoreAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamAcknowledgeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamAutoClaimAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamAutoClaimIdsOnlyAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamClaimAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamClaimIdsOnlyAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamConsumerGroupSetPositionAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamConsumerInfoAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamCreateConsumerGroupAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamGroupInfoAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamInfoAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamPendingAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamPendingMessagesAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamRangeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamReadGroupAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamTrimAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringAppendAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringBitCountAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringBitOperationAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringBitPositionAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringDecrementAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetLeaseAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetBitAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetRangeAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetWithExpiryAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringIncrementAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringLongestCommonSubsequenceAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringLongestCommonSubsequenceLengthAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringLongestCommonSubsequenceWithMatchesAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringSetAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringSetAndGetAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringSetBitAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringSetRangeAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">ServiceStack.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>ServiceStack.Redis.RedisClient.GetHashCount</p>
<p>ServiceStack.Redis.RedisClient.GetHashKeys</p>
<p>ServiceStack.Redis.RedisClient.GetListCount</p>
<p>ServiceStack.Redis.RedisClient.GetSetCount</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><p>Microsoft.Extensions.Caching.Redis</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Extensions.Caching.Redis.RedisCache.Get</p>
<p>Microsoft.Extensions.Caching.Redis.RedisCache.GetAsync</p>
<p>Microsoft.Extensions.Caching.Redis.RedisCache.GetAndRefresh</p>
<p>Microsoft.Extensions.Caching.Redis.RedisCache.GetAndRefreshAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td
class="confluenceTd">Microsoft.Extensions.Caching.StackExchangeRedis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.Get</p>
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.GetAsync</p>
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.GetAndRefresh</p>
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.GetAndRefreshAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd">StackExchange.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>StackExchange.Redis.IDatabase.GeoRemove</p>
<p>StackExchange.Redis.IDatabase.HashDelete</p>
<p>StackExchange.Redis.IDatabase.KeyDelete</p>
<p>StackExchange.Redis.IDatabase.KeyDump</p>
<p>StackExchange.Redis.IDatabase.ListRemove</p>
<p>StackExchange.Redis.IDatabase.ListLeftPop</p>
<p>StackExchange.Redis.IDatabase.ListRightPop</p>
<p>StackExchange.Redis.IDatabase.ListRightPopLeftPush</p>
<p>StackExchange.Redis.IDatabase.SetPop</p>
<p>StackExchange.Redis.IDatabase.SetRemove</p>
<p>StackExchange.Redis.IDatabase.SortedSetRemove</p>
<p>StackExchange.Redis.IDatabase.SortedSetRemoveRangeByRank</p>
<p>StackExchange.Redis.IDatabase.SortedSetRemoveRangeByScore</p>
<p>StackExchange.Redis.IDatabase.SortedSetRemoveRangeByValue</p>
<p>StackExchange.Redis.IDatabase.StreamDelete</p>
<p>StackExchange.Redis.IDatabase.StreamDeleteConsumer</p>
<p>StackExchange.Redis.IDatabase.StreamDeleteConsumerGroup</p>
<p>StackExchange.Redis.IDatabase.StringGetDelete</p>
<p>StackExchange.Redis.IDatabaseAsync.GeoRemoveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.HashDeleteAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyDeleteAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.KeyDumpAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListRemoveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListLeftPopAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListRightPopAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.ListRightPopLeftPushAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetPopAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SetRemoveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRemoveAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRemoveRangeByRankAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRemoveRangeByScoreAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetRemoveRangeByValueAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamDeleteAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamDeleteConsumerAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StreamDeleteConsumerGroupAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.StringGetDeleteAsync</p>
<p>StackExchange.Redis.IDatabaseAsync.SortedSetPopAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd">ServiceStack.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>ServiceStack.Redis.RedisClient.RemoveItemFromSet</p>
<p>ServiceStack.Redis.RedisClient.RemoveItemFromList</p>
<p>ServiceStack.Redis.RedisClient.PopItemFromList</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd">Microsoft.Extensions.Caching.Redis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Extensions.Caching.Redis.RedisCache.Remove</p>
<p>Microsoft.Extensions.Caching.Redis.RedisCache.RemoveAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete </td>
<td
class="confluenceTd">Microsoft.Extensions.Caching.StackExchangeRedis</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.Remove</p>
<p>Microsoft.Extensions.Caching.StackExchangeRedis.RedisCache.RemoveAsync</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon </th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370647.png" draggable="false"
data-image-src="666370647.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370647"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Redis_Connection.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370638"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Redis Connection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370646.png" draggable="false"
data-image-src="666370646.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370646"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Redis_Collection.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370638"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Redis Collection</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370645.png" draggable="false"
data-image-src="666370645.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370645"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Unknown_Redis_Connection.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370638"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Redis Connection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="666370644.png" draggable="false"
data-image-src="666370644.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370644"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Unknown_Redis_Collection.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="666370638"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Redis Collection </td>
</tr>
</tbody>
</table>

## Links

Links are created for transaction and function point needs:

<table class="relative-table wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh"> Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd"><p>From DotNet Redis Collection object
to DotNet Redis Connection object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Dotnet Redis Collection objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>StringSet</p>
<p>StringSetAsync</p>
<p>Add</p>
<p>GeoAdd</p>
<p>HashSet</p>
<p>SetCombineAndStore</p>
<p>AddItemToList</p>
<p>AddItemToSet</p>
<p>AddRangeToList</p>
<p>AddRangeToSet</p>
<p>CreateHashSet</p>
<p>PushItemToList</p>
<p>GetSetCount</p>
<p>SetAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"> useDeleteLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Dotnet Redis Collection objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>KeyDelete</p>
<p>Remove</p>
<p>RemoveAll</p>
<p>KeyDump</p>
<p>RemoveItemFromSet</p>
<p>RemoveItemFromList</p>
<p>PopItemFromList</p>
<p>RemoveAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>useSelectLink</p></td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Dotnet Redis Collection objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>StringGet</p>
<p>StringGetAsync</p>
<p>GeoRadius</p>
<p>GeoRadiusAsync</p>
<p>HashGetAllAsync</p>
<p>HashGetAll</p>
<p>Sort</p>
<p>HashScan</p>
<p>SetScan</p>
<p>Get</p>
<p>GetHashCount</p>
<p>GetHashKeys</p>
<p>GetListCount</p>
<p>GetAsync</p>
<p>GetAndRefresh</p>
<p>GetAndRefreshAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and Dotnet Redis Collection objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>StringIncrement</p>
<p>StringIncrementAsync</p>
<p>IncrementValueInHash</p>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Redis Connections and Collections

``` c#
public class TestRedis 
    {

        // Configure Redis cache options
        RedisCacheOptions cacheOptions = new RedisCacheOptions("localhost");

        // Create a Redis cache instance
        public RedisCache cache = new RedisCache(cacheOptions);

        // Generate a cache key
        var cacheKey = "Company";

        public void deleteCompany()
        {   
            // Remove the cached item
            cache.Remove(cacheKey);
        }
    }
```

### Insert Operation

``` c#
public void insertCompany()
        {   
            // Store a value in the cache
            DistributedCacheEntryOptions options = new DistributedCacheEntryOptions();
            byte byteArray = Encoding.UTF8.GetBytes("1, TATA, IND");
            cache.Set(cacheKey, byteArray, options);
        }
```

![](666370642.png)

### Select Operation

``` c#
public void selectCompany()
        {   
            // Retrieve the value from the cache
            var cachedValue = cache.Get(cacheKey);
        }
       
```

![](666370641.png)

### Update Operation

``` c#
// Generate a cache key
var cacheKey = "myCounter";
public void update_()
        {   
            // Retrieve the value from the cache
            newValue = redisDb.StringIncrement(cacheKey);
        }
```

![](666370640.png)

### Delete Operation

``` c#
public void deleteCompany()
        {   
            // Remove the cached item
            cache.Remove(cacheKey);
        }
```

![](666370639.png)

## Known Limitations

-   Cases in which the name is not resolved, Unknown
    connection/collection object will get created
