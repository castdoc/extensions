---
title: "Support of Redis for Node.js"
linkTitle: "Support of Redis for Node.js"
type: "docs"
---

>CAST supports Redis via
its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
extension. Details about how this support is provided for Node.js source
code is discussed below.

## Supported Redis clients

-   redis
-   ioredis
-   redis-fast-driver
-   then-redis

## Objects

| Icon | Description                      |
|------|----------------------------------|
|  ![Alt text](354418893.png)    | Node.js Redis Connection         |
|  ![Alt text](354418889.png)    | Node.js Redis Collection         |
|  ![Alt text](354418894.png)    | Node.js unknown Redis Connection |
|  ![Alt text](354418891.png)    | Node.js Unknown Redis Collection |

## What results can you expect?

Some example scenarios are shown below:

``` js
const redis = require("redis");
var client = redis.createClient('16', '192.168.1.52');

const hash_key = "hash key"
client0.set("string key", "string val", redis.print);

client0.hset(hash_key, "hashtest 1", "some value", redis.print);
client0.hset(["hash key", "hashtest 2", "some other value"], redis.print);

client0.get("string key", redis.print);
client0.get(new Buffer("hash key"), redis.print);

client0.rpush(['frameworks', 'angularjs', 'backbone'], function(err, reply) {
    console.log(reply); //prints 2
});

client0.lrange('frameworks', 0, -1, function(err, reply) {
    console.log(reply); // ['angularjs', 'backbone']
});
```

![](354418887.png)
