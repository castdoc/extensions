---
title: "Support of Redis for Spring Data"
linkTitle: "Support of Redis for Spring Data"
type: "docs"
---

>CAST supports Redis via its
[com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest) extension.
Details about how this support is provided for Java with Spring
Data source code is discussed below.

## Supported Libraries

| Library | Version | Supported |
|---|----|:-:|
| RedisConnectionFactory   | Up to: 2.7.7 | :white_check_mark: |
| RedisTemplate            | Up to: 2.7.7 | :white_check_mark: |
| StringRedisTemplate      | Up to: 2.7.7 | :white_check_mark: |
| LettuceConnectionFactory | Up to: 2.7.7 | :white_check_mark: |
| JedisConnectionFactory   | Up to: 2.7.7 | :white_check_mark: |
| CrudRepository *         | Up to: 2.7.7 | :white_check_mark: |
| JpaRepository *          | Up to: 2.7.7 | :white_check_mark: |

>CrudRepository and JpaRepository are supported only if the repository is created using @RedisHash domain.

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><li>Operation</li></th>
<th class="confluenceTh" style="text-align: left;"><li>Methods
Supported</li></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.ValueOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.ValueOperations.set</li>
<li>org.springframework.data.redis.core.ValueOperations.multiSet</li>
<li>org.springframework.data.redis.core.ValueOperations.multiSetIfAbsent</li>
<li>org.springframework.data.redis.core.ValueOperations.setIfAbsent</li>
<li>org.springframework.data.redis.core.ValueOperations.append</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.ListOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.ListOperations.leftPush</li>
<li>org.springframework.data.redis.core.ListOperations.leftPushAll</li>
<li>org.springframework.data.redis.core.ListOperations.rightPush </li>
<li>org.springframework.data.redis.core.ListOperations.rightPushAll</li>
<li>org.springframework.data.redis.core.ListOperations.set</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.HashOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.HashOperations.putAll</li>
<li>org.springframework.data.redis.core.HashOperations.put</li>
<li>org.springframework.data.redis.core.HashOperations.putIfAbsent</li>
</ul>
</div>
</details></div>
<ul>
<li>org.springframework.data.redis.core.SetOperations.add</li>
<li>org.springframework.data.repository.CrudRepository.save</li>
<li>org.springframework.data.repository.CrudRepository.saveAll</li>
<li>org.springframework.data.jpa.repository.JpaRepository.save </li>
<li>org.springframework.data.jpa.repository.JpaRepository.saveAll</li>
<li>org.springframework.data.jpa.repository.JpaRepository.saveAndFlush</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.ValueOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.ValueOperations.get</li>
<li>org.springframework.data.redis.core.ValueOperations.multiGet</li>
<li>org.springframework.data.redis.core.ValueOperations.size</li>
<li>org.springframework.data.redis.core.ValueOperations.bitField</li>
<li>org.springframework.data.redis.core.ValueOperations.getAndDelete</li>
<li>org.springframework.data.redis.core.ValueOperations.getAndSet</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.ListOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.ListOperations.range</li>
<li>org.springframework.data.redis.core.ListOperations.size</li>
<li>org.springframework.data.redis.core.ListOperations.index</li>
<li>org.springframework.data.redis.core.ListOperations.indexOf</li>
<li>org.springframework.data.redis.core.ListOperations.lastIndexOf</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.HashOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.HashOperations.hasKey</li>
<li>org.springframework.data.redis.core.HashOperations.get</li>
<li>org.springframework.data.redis.core.HashOperations.multiGet</li>
<li>org.springframework.data.redis.core.HashOperations.keys</li>
<li>org.springframework.data.redis.core.HashOperations.values</li>
<li>org.springframework.data.redis.core.HashOperations.lengthOfValue</li>
<li>org.springframework.data.redis.core.HashOperations.size</li>
<li>org.springframework.data.redis.core.HashOperations.entries</li>
<li>org.springframework.data.redis.core.HashOperations.randomEntries</li>
<li>org.springframework.data.redis.core.HashOperations.randomEntry</li>
<li>org.springframework.data.redis.core.HashOperations.randomKey</li>
<li>org.springframework.data.redis.core.HashOperations.randomKeys</li>
<li>org.springframework.data.redis.core.HashOperations.scan</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.SetOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.SetOperations.size</li>
<li>org.springframework.data.redis.core.SetOperations.isMember</li>
<li>org.springframework.data.redis.core.SetOperations.members</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.repository.CrudRepository</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.repository.CrudRepository.findAll</li>
<li>org.springframework.data.repository.CrudRepository.count</li>
<li>org.springframework.data.repository.CrudRepository.findById</li>
<li>org.springframework.data.repository.CrudRepository.findAllById</li>
<li>org.springframework.data.repository.CrudRepository.existsById</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.jpa.repository.JpaRepository</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.jpa.repository.JpaRepository.count</li>
<li>org.springframework.data.jpa.repository.JpaRepository.findAll</li>
<li>org.springframework.data.jpa.repository.JpaRepository.findAllById</li>
<li>org.springframework.data.jpa.repository.JpaRepository.exists</li>
<li>org.springframework.data.jpa.repository.JpaRepository.findOne</li>
<li>org.springframework.data.jpa.repository.JpaRepository.findById</li>
<li>org.springframework.data.jpa.repository.JpaRepository.existsById</li>
</ul>
</div>
</details></div>
<ul>
<li>org.springframework.data.repository.PagingAndSortingRepository.findAll</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.ListOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.ListOperations.remove</li>
<li>org.springframework.data.redis.core.ListOperations.rightPop</li>
<li>org.springframework.data.redis.core.ListOperations.leftPop</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.repository.CrudRepository</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.repository.CrudRepository.deleteAll</li>
<li>org.springframework.data.repository.CrudRepository.deleteById</li>
<li>org.springframework.data.repository.CrudRepository.delete</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.jpa.repository.JpaRepository</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteInBatch</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteAllInBatch</li>
<li>org.springframework.data.jpa.repository.JpaRepository.delete</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteAll</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteAllById</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteById</li>
</ul>
</div>
</details></div>
<ul>
<li>org.springframework.data.redis.core.HashOperations.delete</li>
<li>org.springframework.data.redis.core.SetOperations.remove</li>
<li>org.springframework.data.redis.core.SetOperations.pop</li>
<li>org.springframework.data.redis.core.ValueOperations.getAndDelete</li>
<li>org.springframework.data.redis.core.RedisTemplate.delete</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.jpa.repository.JpaRepository</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteInBatch</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteAllInBatch</li>
<li>org.springframework.data.jpa.repository.JpaRepository.delete</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteAll</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteAllById</li>
<li>org.springframework.data.jpa.repository.JpaRepository.deleteById</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.ValueOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.ValueOperations.increment</li>
<li>org.springframework.data.redis.core.ValueOperations.decrement</li>
<li>org.springframework.data.redis.core.ValueOperations.getAndSet</li>
<li>org.springframework.data.redis.core.ValueOperations.setIfPresent</li>
<li>org.springframework.data.redis.core.ValueOperations.getAndSet</li>
</ul>
</div>
</details></div>
<div id="expander-1998111563" class="expand-container">
<details><summary>org.springframework.data.redis.core.ListOperations</summary>
<div id="expander-content-1998111563" class="expand-content">
<ul>
<li>org.springframework.data.redis.core.ListOperations.trim</li>
<li>org.springframework.data.redis.core.ListOperations.leftPushIfPresent</li>
<li>org.springframework.data.redis.core.ListOperations.rightPushIfPresent</li>
</ul>
</div>
</details></div>
<ul>
<li>org.springframework.data.redis.core.HashOperations.increment</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><li>Icon</li></th>
<th class="confluenceTh"
style="text-align: left;"><li>Description</li></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
&#10;</div></td>
<td class="confluenceTd" style="text-align: left;"><li>Java Redis
Connection</li></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
&#10;</div></td>
<td class="confluenceTd" style="text-align: left;">Java Redis
Collection</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
&#10;</div></td>
<td class="confluenceTd" style="text-align: left;">Java unknown Redis
Connection</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper" style="text-align: left;">
&#10;</div></td>
<td class="confluenceTd" style="text-align: left;">Java Unknown Redis
Collection</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><strong>Link
type</strong></th>
<th class="confluenceTh" style="text-align: left;">When is this
created?</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">parentLink</td>
<td class="confluenceTd" style="text-align: left;"><li>Between Redis
connection object and Redis collection object</li></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useLink</td>
<td class="confluenceTd" style="text-align: left;"><li>Between the caller
Java method object and Redis collection object</li></td>
<td class="confluenceTd"><ul>
<li>multiGet</li>
<li>get</li>
<li>keys</li>
<li>entries</li>
<li>findAllById</li>
<li>findAll</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useSelectLink</td>
<td class="confluenceTd" style="text-align: left;"><li>Between the caller
Java method object and Redis collection object</li></td>
<td class="confluenceTd" style="text-align: left;"><ul>
<li>multiGet</li>
<li>get</li>
<li>keys</li>
<li>entries</li>
<li>findAllById</li>
<li>findAll</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useInsertLink</td>
<td class="confluenceTd" style="text-align: left;"><li>Between the caller
Java method object and Redis collection object</li></td>
<td class="confluenceTd" style="text-align: left;"><ul>
<li> set</li>
<li> put</li>
<li> putAll</li>
<li> add</li>
<li> leftpush</li>
<li>save</li>
<li>saveAll</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><li>useDeleteLink</li></td>
<td class="confluenceTd" style="text-align: left;"><li>Between the caller
Java method object and Redis collection object</li></td>
<td class="confluenceTd" style="text-align: left;"><ul>
<li>delete</li>
<li>remove</li>
<li>deleteById</li>
<li>deleteAll</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useUpdateLink</td>
<td class="confluenceTd" style="text-align: left;"><li>Between the caller
Java method object and Redis collection object</li></td>
<td class="confluenceTd" style="text-align: left;"><ul>
<li>increment</li>
<li>decrement</li>
</ul></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Redis Connections and Collections

``` java
@Autowired
private ServiceConfigResource serviceConfigResource;

RedisConnectionFactory switchRedisMaster() {
         return getRedisConnectionFactory(serviceConfigResource.getSwitchRedisMaster(), serviceConfigResource.getDisRedisPort());
}
```

#### Java based configurations

``` java
@Configuration
@RefreshScope
@Getter
public class ServiceConfigResource {

      @Value("${disredis.host.disRedisHost1}")
         private String switchRedisMaster;

      @Value("${disredis.host.disRedisHost2}")
         private String switchRedisSlave1;

         @Value("${disredis.host.disRedisHost3}")
         private String switchRedisSlave2;

        @Value("${disredis.port.disRedisPort1}")
      private int disRedisPort; 

         @Value("${useRoomSharedUsedTable}")
        private int useRoomSharedUsedTable;

}
```

![](666370729.png)

#### Xml based configurations

``` java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:p="http://www.springframework.org/schema/p"
  xmlns:redis="http://www.springframework.org/schema/redis"
  xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/redis http://www.springframework.org/schema/redis/spring-redis.xsd
        ">
    
<!-- Jedis Connection -->    
<bean id="jedisConnectionFactory" class="org.springframework.data.redis.connection.jedis.JedisConnectionFactory"
   p:host-name="localhost" p:port="6379" />
  
<!-- Redis Template -->
<bean id="redisTemplate" class="org.springframework.data.redis.core.RedisTemplate">
    <property name="connectionFactory" ref="jedisConnectionFactory" />
    <property name="valueSerializer">
        <bean id="redisJsonSerializer" class="org.springframework.data.redis.serializer.JacksonJsonRedisSerializer">
            <constructor-arg type="java.lang.Class" value="redis.User"/>
        </bean>   
    </property>
</bean>

<bean class="redis.UserRepository"/>

</beans>
```

![](666370728.png)

#### Lettuce

``` java
 public ReactiveRedisConnectionFactory build() {
    LettuceClientConfiguration clientConfig;
    LettuceClientConfiguration.LettuceClientConfigurationBuilder builder =
        LettuceClientConfiguration.builder();
    if (ssl) {
      builder.commandTimeout(Duration.ofSeconds(timeout)).useSsl();
    }
    clientConfig = builder.build();

    RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration();
    configuration.setHostName(host);
    configuration.setPassword(password);
    configuration.setPort(port);
    return new LettuceConnectionFactory(configuration, clientConfig);
```

![](666370727.png)

#### RedisHash Domain 

``` java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.project.app.domain;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.redis.core.RedisHash;

/
 *
 * @author armena
 */
@RedisHash
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class SearchData implements Serializable{
    
    private String id;   //unique id
    private String key; //key for search
    private String title;
    private String content;
    
}
```

#### Redis Repo Creation using CrudRepository

``` java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.project.app.repositories;

import io.project.app.domain.SearchData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/
 *
 * @author armena
 */
@Repository
@Component
public interface SearchRepository extends CrudRepository<SearchData, String>{
    
    public SearchRepository findByTitle(String title);
    
}
```

![](666370726.png)

### Insert Operation

``` java
@Bean(name = "switch_redis_master")
    <T> RedisTemplate<String, T> switchMasterRedis() {
        RedisTemplate template = new RedisTemplate<>();
        FastJsonRedisSerializer fastJsonRedisSerializer = new FastJsonRedisSerializer(Object.class);
        template.setConnectionFactory(switchRedisMaster());
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(fastJsonRedisSerializer);
        return template;
    }

@Autowired
@Qualifier("switch_redis_master")
private RedisTemplate<String, Integer> redisTemplate;


private boolean invokeFrequencyValidate(String agentCode, Date checkInDate, Date checkOutDate) {
        int openingHotelNum = hotelService.getOpeningHotelNum();
        Date currentDate = new Date();
        // 16-90
        if (AbstractDateUtils.isAfter(checkInDate, AbstractDateUtils.getDeltaDateByDays(currentDate, 15))) {
            Integer count = redisTemplate.opsForValue().get(agentCode + "_" + SIXTEEN_TO_NINETY);
            if (count == null) {
                redisTemplate.opsForValue().set(agentCode + "_" + SIXTEEN_TO_NINETY, 1, TIME_PERIOD_THREE, TimeUnit.MINUTES);
                return true;
            }
            if (count >= maxQueryBase + openingHotelNum / FIFTEEN_OUTER_DIVIDED) {
                return false;
            }
            return true;
        }
```

![](666370725.png)

#### CrudRepository Insert Operation

``` java
  @Autowired
    private SearchRepository searchRepository;

    public SearchData save(SearchData googleSearch) {        
        return searchRepository.save(googleSearch);
    }
```

![](666370724.png)

### Select Operation

``` java
private List<RoomShareUsedCountEntity> calculateRoomShareUsedCountWithType(StandardRoomWithTypeRequest standardRoomWithTypeRequest) {
        List<String> validDateList = calculateAllKeyDate(standardRoomWithTypeRequest.getCheckInDate(), standardRoomWithTypeRequest.getCheckOutDate());
        List<String> fullKey = Lists.newArrayList();

        List<UsedShareRoomCount> allUsedShareRoomCountList = Lists.newArrayList();
        for (String key : fullKey) {
            List<UsedShareRoomCount> value = (List<UsedShareRoomCount>) usedShareRoomCountSlaveRedisTemplate.opsForValue().get(key);
        }
        if (CollectionUtils.isEmpty(allUsedShareRoomCountList)) {
            return Lists.newArrayList();
        }
        return convert2RoomShareUsedCountEntityFromUsedShareCount(allUsedShareRoomCountList);
    }
```

![](666370723.png)

#### CrudRepository Select Operation

``` java
  @Autowired
    private SearchRepository searchRepository;

    public Optional<SearchData> find(String id) {
       return searchRepository.findById(id);
    }
```

![](666370722.png)

### Update Operation

``` java
private boolean invokeFrequencyValidate(String agentCode, Date checkInDate, Date checkOutDate) {

        int openingHotelNum = hotelService.getOpeningHotelNum();
        Date currentDate = new Date();
        // 16-90
        if (AbstractDateUtils.isAfter(checkInDate, AbstractDateUtils.getDeltaDateByDays(currentDate, 15))) {
            Integer count = redisTemplate.opsForValue().get(agentCode + "_" + SIXTEEN_TO_NINETY);
            if (count == null) {
                redisTemplate.opsForValue().set(agentCode + "_" + SIXTEEN_TO_NINETY, 1, TIME_PERIOD_THREE, TimeUnit.MINUTES);
                return true;
            }
            if (count >= maxQueryBase + openingHotelNum / FIFTEEN_OUTER_DIVIDED) {
                return false;
            }
            redisTemplate.opsForValue().increment(agentCode + "_" + SIXTEEN_TO_NINETY, 1);
            return true;
        }
```

![](666370721.png)

### Delete Operation

``` java
public void delete(String key) {
        template.opsForValue().getOperations().delete(key);
    }
```

![](666370720.png)

#### CrudRepository Delete Operation

``` java
  @Autowired
    private SearchRepository searchRepository;

    public Optional<SearchData> delete(String id) {
        return searchRepository.deleteById(id);
    }
```

![](666370719.png)

## Known Limitations

-   Unknown Redis connection/collection objects are created in case of
    unresolved names.
