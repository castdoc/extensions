---
title: "Lucene.NET"
linkTitle: "Lucene.NET"
type: "docs"
no_list: true
---

>CAST supports Lucene.NET via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about how this support is provided for .NET source
code is discussed below.

## Supported Client Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Library</th>
<th class="confluenceTh">Version</th>
<th class="confluenceTh">Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><a
href="https://lucenenet.apache.org/download/version-3.html"
rel="nofollow">Lucene.NET</a></td>
<td class="confluenceTd">up to: 3.0.3</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg" alt="(tick)" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operations</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td
class="confluenceTd"><p>Lucene.Net.Index.IndexWriter.AddDocument</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td
class="confluenceTd"><p>Lucene.Net.Index.IndexWriter.GetMaxFieldLength</p>
<p>Lucene.Net.Index.IndexWriter.NumRamDocs</p>
<p>Lucene.Net.Index.IndexWriter.GetDocCount</p>
<p>Lucene.Net.Index.IndexWriter.HasDeletions</p>
<p>Lucene.Net.Index.IndexWriter.MaxDoc</p>
<p>Lucene.Net.Index.IndexWriter.NumDeletedDocs</p>
<p>Lucene.Net.Index.IndexWriter.NumDocs</p>
<p>Lucene.Net.Index.IndexReader.Document</p>
<p>Lucene.Net.Index.IndexReader.GetFieldNames</p>
<p>Lucene.Net.Index.IndexReader.IsDeleted</p>
<p>Lucene.Net.Index.IndexReader.HasDeletions</p>
<p>Lucene.Net.Index.IndexReader.ListCommits</p>
<p>Lucene.Net.Index.IndexReader.TermDocs</p>
<p>Lucene.Net.Search.Searcher.Search'</p>
<p>Lucene.Net.Search.Searcher.Doc</p>
<p>Lucene.Net.Search.Searcher.DocFreq</p>
<p>Lucene.Net.Search.Searcher.Explain</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Lucene.Net.Index.IndexWriter.DeleteDocuments</p>
<p>Lucene.Net.Index.IndexReader.DeleteDocument</p>
<p>Lucene.Net.Index.IndexWriter.DeleteAll</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"><p>Lucene.Net.Search.Searcher.Rewrite</p>
<p>Lucene.Net.Index.IndexWriter.UpdateDocument</p></td>
</tr>
</tbody>
</table>

## Objects

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370388.png" draggable="false"
data-image-src="666370388.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370388"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="642351144"
data-linked-resource-container-version="1" height="34" /></p>
</div></td>
<td class="confluenceTd">DotNet Lucene Index</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="666370387.png" draggable="false"
data-image-src="666370387.png"
data-unresolved-comment-count="0" data-linked-resource-id="666370387"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="642351144"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Lucene Index</td>
</tr>
</tbody>
</table>

## Links

All links are created between the caller .NET Method objects and DotNet
Lucene Index object:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><strong>Link type</strong></th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><p>GetMaxFieldLength</p>
<p>NumRamDocs</p>
<p>GetDocCount</p>
<p>HasDeletions</p>
<p>MaxDoc</p>
<p>NumDeletedDocs</p>
<p>NumDocs</p>
<p>Document</p>
<p>GetFieldNames</p>
<p>IsDeleted</p>
<p>ListCommits</p>
<p>TermDocs</p>
<p>Search</p>
<p>Doc</p>
<p>DocFreq</p>
<p>Explain</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><p>Rewrite</p>
<p>UpdateDocument</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><p>AddDocument</p>
<p>Commit</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><p>DeleteDocuments</p>
<p>DeleteDocument</p>
<p>DeleteAll</p></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Lucene Index

``` c#
public class MyLuceneIndex
{
    private Directory indexDirectory;
    private IndexWriterConfig indexWriterConfig;
    private IndexWriter indexWriter;

    public MyLuceneIndex()
    {
        indexDirectory = new RAMDirectory();
        indexWriterConfig = new IndexWriterConfig(LuceneVersion.LUCENE_48, new StandardAnalyzer(LuceneVersion.LUCENE_48));
        indexWriter = new IndexWriter(indexDirectory, indexWriterConfig);
    }
```

![](642351146.png)

``` c#
public class LuceneCRUDExample
{
    private string productIndexPath = "D:\ProductIndex";
    private string userIndexPath = "D:\UserIndex";
    private Directory productIndexDirectory;
    private Directory userIndexDirectory;
    private IndexWriterConfig indexWriterConfig;
    private IndexWriter productIndexWriter;
    private IndexWriter userIndexWriter;

    public LuceneCRUDExample()
    {
        productIndexDirectory = FSDirectory.Open(new DirectoryInfo(productIndexPath));
        userIndexDirectory = FSDirectory.Open(new DirectoryInfo(userIndexPath));

        indexWriterConfig = new IndexWriterConfig(Lucene.Net.Util.LuceneVersion.LUCENE_48, new StandardAnalyzer(Lucene.Net.Util.LuceneVersion.LUCENE_48));
        productIndexWriter = new IndexWriter(productIndexDirectory, indexWriterConfig);
        userIndexWriter = new IndexWriter(userIndexDirectory, indexWriterConfig);
    }
```

![](642351151.png)

![](642351150.png)

### Select Operation

``` c#
 public void SearchProductDocuments(string searchTerm)
    {
        var searcher = new IndexSearcher(productIndexDirectory, true);
        var parser = new Lucene.Net.QueryParsers.Classic.QueryParser(Lucene.Net.Util.LuceneVersion.LUCENE_48, "Description", new StandardAnalyzer(Lucene.Net.Util.LuceneVersion.LUCENE_48));
        var query = parser.Parse(searchTerm);

        var topDocs = searcher.Search(query, 10);
        foreach (var scoreDoc in topDocs.ScoreDocs)
        {
            var doc = searcher.Doc(scoreDoc.Doc);
            Console.WriteLine($"Product ID: {doc.Get("Id")}, Name: {doc.Get("Name")}, Description: {doc.Get("Description")}");
        }

        searcher.Dispose();
    }
```

![](642351145.png)

### Insert Operation

``` c#
public void AddProductDocument(int id, string name, string description)
    {
        var doc = new Document();
        doc.Add(new StringField("Id", id.ToString(), Field.Store.YES));
        doc.Add(new TextField("Name", name, Field.Store.YES));
        doc.Add(new TextField("Description", description, Field.Store.YES));

        productIndexWriter.AddDocument(doc);
        productIndexWriter.Commit();
    }
```

![](642351149.png)

### Update Operation

``` c#
 public void UpdateProductDocument(int id, string newName, string newDescription)
    {
        var term = new Term("Id", id.ToString());
        var doc = new Document();
        doc.Add(new StringField("Id", id.ToString(), Field.Store.YES));
        doc.Add(new TextField("Name", newName, Field.Store.YES));
        doc.Add(new TextField("Description", newDescription, Field.Store.YES));

        productIndexWriter.UpdateDocument(term, doc);
        productIndexWriter.Commit();
    }
```

![](642351148.png)

Delete Operation

``` c#
public void DeleteProductDocument(int id)
{
var term = new Term("Id", id.ToString());
productIndexWriter.DeleteDocuments(term);
productIndexWriter.Commit();
}
```

![](642351147.png)

## Known Limitations

-   If the index location or path is not found in the source
    application, it will result in an Unknown Lucene index object
