---
title: "Support of Azure Cosmos DB for Spring Data"
linkTitle: "Support of Azure Cosmos DB for Spring Data"
type: "docs"
---

>CAST supports Azure Cosmos DB via its [com.castsoftware.nosqljava](../../../extensions/com.castsoftware.nosqljava/) extension. Details about how this support is provided for Spring Data source code is discussed below.

## Supported Client Libraries

| Library                  | Version       | Supported          |
|--------------------------|---------------|:------------------:|
| Azure spring data cosmos | Up to: 3.10.0 | :white_check_mark: |

## Supported Operations

| Operations | Method Supported |
|---|---|
| Insert | <details><summary>org.springframework.data.repository</summary><ul><li>org.springframework.data.repository.CrudRepository.save</li><li>org.springframework.data.repository.CrudRepository.saveAll</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.save</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.saveAll</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.repository</summary><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.save</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.saveAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.save</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.saveAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.save</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.saveAll</li></ul></details><details><summary>com.azure.spring.data.cosmos.repository</summary><ul><li>com.azure.spring.data.cosmos.repository.CosmosRepository.save</li><li>com.azure.spring.data.cosmos.repository.CosmosRepository.saveAll</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.save</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.saveAll</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.core</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.insert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.insert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.insert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.insert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.insert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.insert</li></ul></details> |
| Select | <details><summary>org.springframework.data.repository</summary><ul><li>org.springframework.data.repository.CrudRepository.existsById</li><li>org.springframework.data.repository.CrudRepository.findAll</li><li>org.springframework.data.repository.CrudRepository.findById</li><li>org.springframework.data.repository.PagingAndSortingRepository.findAll</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.existsById</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.findAll</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.findAllById</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.findById</li><li>org.springframework.data.repository.reactive.ReactiveSortingRepository.findAll</li></ul></details><details><summary>com.azure.spring.data.cosmos.repository</summary><ul><li>com.azure.spring.data.cosmos.repository.CosmosRepository.findById</li><li>com.azure.spring.data.cosmos.repository.CosmosRepository.findAll</li><li>com.azure.spring.data.cosmos.repository.CosmosRepository.existsById</li><li>com.azure.spring.data.cosmos.repository.CosmosRepository.count</li><li>com.azure.spring.data.cosmos.repository.CosmosRepository.findAllById</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.findbyId</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.findAll</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.existsById</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.count</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.findAllById</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.repository</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.findById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.existsById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.findAllById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.count</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.existsById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.findAllById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.count</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveSortingRepository.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.findById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.count</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.findAllById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.existsById</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.find</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.findByIds</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.exists</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.count</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.findById</li></ul></details></ul><details><summary>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate</summary><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.findById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.find</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.findByIds</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.count</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.exists</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.find</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.findByIds</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.exists</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.existsById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.count</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.findById</li><ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.findById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.find</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.count</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.exists</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.existsById</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.count</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.exists</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.find</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.findById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.count</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.exists</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.find</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.findAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.findById</li></ul></details> |
| Delete | <details><summary>org.springframework.data.repository</summary><ul><li>org.springframework.data.repository.CrudRepository.delete</li><li>org.springframework.data.repository.CrudRepository.deleteById</li><li>org.springframework.data.repository.CrudRepository.deleteAllById</li><li>org.springframework.data.repository.CrudRepository.deleteAll</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.delete</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.deleteAll</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.deleteAllById</li><li>org.springframework.data.repository.reactive.ReactiveCrudRepository.deleteById</li></ul></details><details><summary>com.azure.spring.data.cosmos.repository</summary><ul><li>com.azure.spring.data.cosmos.repository.CosmosRepository.deleteById</li><li>com.azure.spring.data.cosmos.repository.CosmosRepository.delete</li><li>com.azure.spring.data.cosmos.repository.CosmosRepository.deleteAll</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.deletebyId</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.deleteAll</li><li>com.azure.spring.data.cosmos.repository.ReactiveCosmosRepository.delete</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.core</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.delete</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.deleteCollection</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.delete</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.deleteCollection</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.delete</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.deleteCollection</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.delete</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.deleteCollection</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.delete</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.delete</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.deleteCollection</li></ul></details><details><summary>com.microsoft.azure.spring.data.cosmosdb.repository</summary><ul><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository.delete</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.CosmosRepository.deleteById</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.deletebyId</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.deleteAll</li><li>com.microsoft.azure.spring.data.cosmosdb.repository.DocumentDbRepository.delete</li></ul></details> | 
| Upsert | <ul><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosOperations.upsert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbOperations.upsert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosOperations.upsert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.DocumentDbTemplate.upsert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.ReactiveCosmosTemplate.upsert</li><li>com.microsoft.azure.spring.data.cosmosdb.core.CosmosTemplate.upsert</li></ul> |

Above mentioned methods are supported for both Synchronous and
Asynchronous clients.

## Objects

| Icon | Description                      |
|------|----------------------------------|
| ![](383887261.png) | Java CosmosDB Database |
| ![](383887260.png) | Java CosmosDB Collection |
| ![](383887259.png) | Java Unknown CosmosDB Database |
| ![](383887258.png)  | Java Unknown CosmosDB Collection |

## Links

| Link type     | Source and destination of link                                 | Methods Supported                                         |
|---------------|----------------------------------------------------------------|-----------------------------------------------------------|
| parentLink    | Between CosmosDB Database object and CosmosDB Collection       | -                                                         |
| useInsertLink | Between the caller Java Method objects and CosmosDB Collection | save<br>saveAll                                           |
| useSelectLink | Between the caller Java Method objects and CosmosDB Collection | existsById<br>findById<br>findAll<br>findAllById<br>count |
| useDeleteLink | Between the caller Java Method objects and CosmosDB Collection | delete<br>deleteAll<br>deleteById<br>deleteAllById        |

## What results can you expect?

Some example scenarios are shown below:

### CosmosDB Database Connection from Properties File

application.properties

application.properties

``` text
azure.cosmosdb.uri=https://cosmoswithspring.documents.azure.com:443/
azure.cosmosdb.key=2VanbwRYNeJKCUjb5StsnBKaatDcKGRUckHnlSUVSJXifWL3exjwjP2o6rJpt8fY24MRmtnctzcE8hWAlBsRZw==
azure.cosmosdb.database=cosmoswithspring
```

Cosmos DB Client Creation

``` java
@Configuration
@EnableDocumentDbRepositories
public class AppConfig extends AbstractDocumentDbConfiguration {
 
    @Value("${azure.documentdb.uri}")
    private String uri;
 
    @Value("${azure.documentdb.key}")
    private String key;
 
    @Value("${azure.documentdb.database}")
    private String dbName;
 
    public DocumentClient documentClient() {
        return new DocumentClient(uri, key, ConnectionPolicy.GetDefault(), ConsistencyLevel.Session);
    }
 
    public String getDatabase() {
        return dbName;
    }
}
```

![](518029317.png)

### Select Operation

CreateDocument

``` java
   @Autowired
    private AlertRepository alertRepository;

    @GetMapping("/all")
    public Iterable getAllAlerts() {
        if (StreamSupport.stream(alertRepository.findAll().spliterator(), false).count() > 0) {
            return alertRepository.findAll();
        } else {
            throw new ValidationException("No records found.");
        }
    }

 @GetMapping("/count")
    public long countAlert() {
        if (StreamSupport.stream(alertRepository.findAll().spliterator(), false).count() > 0) {
            return alertRepository.count();
        } else {
            throw new ValidationException("No records found.");
        }
    }


```

![](518029319.png)

![](518029318.png)

### Insert Operation

QueryDocuments

``` java
 @PostMapping("/add")
    public StatusMessage addAlert(@Valid @RequestBody Alert alert) {
        Alert returnedAlert = alertRepository.save(alert);
        if (returnedAlert.getAlertId().isEmpty()) {
            throw new ValidationException("Error accessing Cosmos database.");
        }
        return new StatusMessage("200", "Alert Added");
    }
```

![](518029322.png)

### Delete Operation

DeleteDocument

``` java
@GetMapping("/deleteall")
    public StatusMessage deleteAllAlert() {
        alertRepository.deleteAll();
        
        return new StatusMessage("200", "Alert Deleted");
    }

 @DeleteMapping("/delete/{id}")
    public StatusMessage deleteAlert(@PathVariable String id) throws NotFoundException {
        //Optional<Alert> returnedAlert = alertRepository.findById(id);
        if (!alertRepository.existsById(id)) {
            throw new NotFoundException("No alert found with id: " + id);
        }
        alertRepository.deleteById(id);
        return new StatusMessage("200", "alert deleted.");
    }

```

![](518029321.png)

![](518029320.png)

### Query Method Support

Repo with query method

``` java
@Repository
public interface AlertRepository extends CosmosRepository<Alert, String> {
// repo for alert
    List<Alert> findById(String priority);
}
```

Controller

``` java
@GetMapping("find/{id}")
    public Alert findAlert(@PathVariable String id) throws NotFoundException {
        return alertRepository.findById(id).orElseThrow(
                () -> new NotFoundException("No alert found with id: " + id)
        );
}
```

![](518029316.png)

### Query Methods with @Query annotation

Repository with @query methods

``` java
@Repository
public interface AddressRepository extends CosmosRepository<Address, String> {
    void deleteByPostalCodeAndCity(String postalCode, String city);

    void deleteByCity(String city);

    Iterable<Address> findByPostalCodeAndCity(String postalCode, String city);

    Iterable<Address> findByCity(String city);

    Iterable<Address> findByCityIn(List<String> cities);

    Iterable<Address> findByPostalCode(String postalCode);

    Iterable<Address> findByPostalCodeInAndCity(List<String> postalCodes, String city);

    Iterable<Address> findByStreetOrCity(String street, String city);

    @Query("select * from a where a.city = @city")
    List<Address> annotatedFindListByCity(@Param("city") String city);

    @Query("select * from a where a.city = @city")
    Page<Address> annotatedFindByCity(@Param("city") String city, Pageable pageable);
}
```

``` java
public void testAnnotatedQuery() {
        addressRepository.saveAll(Arrays.asList(Address.TEST_ADDRESS1_PARTITION1, Address.TEST_ADDRESS1_PARTITION2));

        final List<Address> result = addressRepository.annotatedFindListByCity(Address.TEST_ADDRESS1_PARTITION1.getCity());
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0)).isEqualTo(Address.TEST_ADDRESS1_PARTITION1);
    }
```

![](541663383.png)

### Asynchronous API

All corresponding methods are represented and results are very similar
to its Synchronous counterpart. Async Client creation:

``` java
import com.microsoft.azure.spring.data.cosmosdb.repository.ReactiveCosmosRepository;

import info.hayslip.AlertHoarder.models.Alert;
import org.springframework.stereotype.Repository;

@Repository
public interface AlertReactRepository extends ReactiveCosmosRepository<Alert, String> {
// repo for alert
}
```

## Known Limitations

-   Database/Collection is created as unknown, if the name is not
    retrieved from the properties file or if the name could not be
    resolved.
