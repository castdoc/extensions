---
title: "Support of Azure Cosmos DB for .NET"
linkTitle: "Support of Azure  Cosmos DB for .NET"
type: "docs"
---

>CAST supports Azure Cosmos DB via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about how this support is provided for .NET source
code is discussed below.

## Supported Client Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><div class="content-wrapper">
<p>Library</p>
</div></th>
<th class="confluenceTh">Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><a
href="https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-sdk-dotnet"
rel="nofollow">Azure Cosmos DB .NET SDK 2.x for SQL API</a></p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><a
href="https://docs.microsoft.com/en-us/azure/cosmos-db/sql/sql-api-sdk-dotnet-standard"
rel="nofollow">Azure Cosmos DB .NET SDK 3.x for SQL API</a></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><a
href="https://cosmonaut.readthedocs.io/en/latest/CosmonautClient.html"
rel="nofollow">CosmonautClient</a></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
</tbody>
</table>

## Supported Operations

<table>
<colgroup>
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Scenario</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Create</td>
<td class="confluenceTd">Methods Supported for Create - 2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Documents.Client.DocumentClient.CreateDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.CreateDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.CreateDocumentCollectionIfNotExistsAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.CreateDocumentCollectionIfNotExistsAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Create</td>
<td class="confluenceTd">Methods Supported for Create - 3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Cosmos.Fluent.ContainerBuilder.CreateIfNotExistsAsync</p>
<p>Microsoft.Azure.Cosmos.Fluent.ContainerBuilder.CreateAsync</p>
<p>Microsoft.Azure.Cosmos.Database.CreateContainerIfNotExistsAsync</p>
<p>Microsoft.Azure.Cosmos.CosmosDatabase.CreateContainerIfNotExistsAsync</p>
<p>Microsoft.Azure.Cosmos.Database.GetContainer</p>
<p>Microsoft.Azure.CosmosDatabase.GetContainer</p>
<p>Microsoft.Azure.Cosmos.CosmosClient.GetContainer</p>
<p>Microsoft.Azure.Cosmos.Database.CreateContainerStreamAsync</p>
<p>Microsoft.Azure.CosmosDatabase.CreateContainerStreamAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Create</td>
<td class="confluenceTd">Methods Supported for Create -
CosmonautClient</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Methods Supported for Create - CosmonautClient</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">Methods Supported for Insert - 2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Documents.Client.DocumentClient.CreateDocumentAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.CreateDocumentAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.UpsertDocumentAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.UpsertDocumentAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">Methods Supported for Insert - 3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Cosmos.Container.CreateItemAsync</p>
<p>Microsoft.Azure.CosmosContainer.CreateItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.CreateItemStreamAsync</p>
<p>Microsoft.Azure.CosmosContainer.CreateItemStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.UpsertItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.UpsertItemStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.CreateItemAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">Methods Supported for Insert -
CosmonautClient</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Cosmonaut.CosmonautClient.CreateDocumentAsync</p>
<p>Cosmonaut.ICosmonautClient.CreateDocumentAsync</p>
<p>Cosmonaut.CosmonautClient.UpsertDocumentAsync</p>
<p>Cosmonaut.ICosmonautClient.UpsertDocumentAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">Methods Supported for Update - 2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Documents.Client.DocumentClient.ReplaceDocumentAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.ReplaceDocumentAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.ReplaceDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.ReplaceDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.UpsertDocumentAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.UpsertDocumentAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">Methods Supported for Update - 3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Cosmos.Container.ReplaceItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.ReplaceItemStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.ReplaceContainerAsync</p>
<p>Microsoft.Azure.CosmosContainer.ReplaceContainerStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.ReplaceContainerStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.UpsertItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.UpsertItemStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.ReplaceThroughputAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">Methods Supported for Update -
CosmonautClient</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Cosmonaut.CosmonautClient.UpdateDocumentAsync</p>
<p>Cosmonaut.ICosmonautClient.UpdateDocumentAsync</p>
<p>Cosmonaut.CosmonautClient.UpdateCollectionAsync</p>
<p>Cosmonaut.ICosmonautClient.UpdateCollectionAsync</p>
<p>Cosmonaut.CosmonautClient.UpsertDocumentAsync </p>
<p>Cosmonaut.ICosmonautClient.UpsertDocumentAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">Methods Supported for Select - 2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Documents.Client.DocumentClient.ReadDocumentAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.ReadDocumentAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.ReadDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.ReadDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.CreateDocumentQuery</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.CreateDocumentQuery</p>

</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">Methods Supported for Select - 3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Documents.Client.DocumentClient.ReadDocumentAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.ReadDocumentAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.ReadDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.ReadDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.CreateDocumentQuery</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.CreateDocumentQuery</p>
<p>Microsoft.Azure.Cosmos.Container.ReadThroughputAsync</p>
<p>Microsoft.Azure.Cosmos.Container.ReadManyItemsStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.ReadManyItemsAsync</p>
<p>Microsoft.Azure.Cosmos.Container.PatchItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.PatchItemStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.GetItemQueryStreamIterator</p>
<p>Microsoft.Azure.Cosmos.Container.GetItemQueryIterator</p>
<p>Microsoft.Azure.Cosmos.Container.GetItemLinqQueryable</p>
<p>Microsoft.Azure.Cosmos.Container.GetFeedRangesAsync</p>
<p>Microsoft.Azure.Cosmos.Container.GetChangeFeedStreamIterator</p>
<p>Microsoft.Azure.Cosmos.Container.GetChangeFeedIterator</p>
<p>Microsoft.Azure.Cosmos.Container.ChangeFeedHandlerWithManualCheckpoint</p>
<p>Microsoft.Azure.Cosmos.Container.ChangeFeedStreamHandler</p>
<p>Microsoft.Azure.Cosmos.Container.ChangeFeedStreamHandlerWithManualCheckpoint</p>
<p>Microsoft.Azure.Cosmos.Container.ChangeFeedMonitorErrorDelegate</p>
<p>Microsoft.Azure.Cosmos.Container.ChangeFeedMonitorLeaseAcquireDelegate</p>
<p>Microsoft.Azure.Cosmos.Container.ChangeFeedMonitorLeaseReleaseDelegate</p>
<p>Microsoft.Azure.Cosmos.Container.GetChangeFeedProcessorBuilder</p>
<p>Microsoft.Azure.Cosmos.Container.GetChangeFeedProcessorBuilderWithManualCheckpoint</p>
<p>Microsoft.Azure.Cosmos.Container.GetPartitionKeyRangesAsync</p>
<p>Microsoft.Azure.Cosmos.Container.GetChangeFeedProcessorBuilderWithAllVersionsAndDeletes</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd">Methods Supported for Select -
CosmonautClient</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Cosmonaut.CosmonautClient.GetDocumentAsync </p>
<p>Cosmonaut.ICosmonautClient.GetDocumentAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">Methods Supported for Delete - 2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Documents.Client.DocumentClient.DeleteDocumentAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.DeleteDocumentAsync</p>
<p>Microsoft.Azure.Documents.Client.DocumentClient.DeleteDocumentCollectionAsync</p>
<p>Microsoft.Azure.Documents.IDocumentClient.DeleteDocumentCollectionAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">Methods Supported for Delete - 3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Cosmos.Container.DeleteItemAsync</p>
<p>Microsoft.Azure.CosmosContainer.DeleteItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteItemStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteContainerAsync</p>
<p>Microsoft.Azure.CosmosContainer.DeleteContainerAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteContainerStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteAllItemsByPartitionKeyStreamAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">Methods Supported for Delete -
CosmonautClient</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Microsoft.Azure.Cosmos.Container.DeleteItemAsync</p>
<p>Microsoft.Azure.CosmosContainer.DeleteItemAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteItemStreamAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteContainerAsync</p>
<p>Microsoft.Azure.CosmosContainer.DeleteContainerAsync</p>
<p>Microsoft.Azure.Cosmos.Container.DeleteContainerStreamAsync</p>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="378513732.png" draggable="false"
data-image-src="378513732.png"
data-unresolved-comment-count="0" data-linked-resource-id="378513732"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513706"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet CosmosDB Database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="378513733.png" draggable="false"
data-image-src="378513733.png"
data-unresolved-comment-count="0" data-linked-resource-id="378513733"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513706"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet CosmosDB Collection</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="378513734.png" draggable="false"
data-image-src="378513734.png"
data-unresolved-comment-count="0" data-linked-resource-id="378513734"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513706"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet CosmosDB Unknown Database </td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="378513735.png" draggable="false"
data-image-src="378513735.png"
data-unresolved-comment-count="0" data-linked-resource-id="378513735"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513706"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet CosmosDB Unknown Collection </td>
</tr>
</tbody>
</table>

## Links

<table class="relative-table wrapped confluenceTable"
style="width: 82.6712%;">
<colgroup>
<col style="width: 11%" />
<col style="width: 53%" />
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">When is this created?</th>
<th class="confluenceTh"> Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd"><p>From DotNet CosmosDB Collection object
to DotNet CosmosDB Database object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller .NET Method objects
and DotNet CosmosDB Collection object
<p><br />
</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CreateDocumentCollectionAsync</p>
<p>CreateDocumentCollectionIfNotExistsAsync</p>
<p>GetContainer</p>
<p>CreateContainerIfNotExistsAsync</p>
<p>CreateContainerAsync</p>
<p>CreateIfNotExistsAsync</p>
<p>CreateAsync</p>
<p>CreateContainerStreamAsync</p>
<p>CreateCollectionAsync</p>
<p>GetCollectionAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>useInsertLink</p></td>
<td class="confluenceTd">Between the caller .NET Method objects
and DotNet CosmosDB Collection object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CreateDocumentAsync</p>
<p>UpsertDocumentAsync</p>
<p>CreateItemAsync</p>
<p>CreateItemStreamAsync</p>
<p>UpsertItemAsync</p>
<p>UpsertItemStreamAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller .NET Method objects
and DotNet CosmosDB Collection object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>ReplaceDocumentAsync</p>
<p>ReplaceDocumentCollectionAsync</p>
<p>UpsertDocumentAsync</p>
<p>ReplaceItemAsync</p>
<p>ReplaceItemStreamAsync</p>
<p>ReplaceContainerAsync</p>
<p>ReplaceContainerStreamAsync</p>
<p>UpsertItemAsync</p>
<p>UpsertItemStreamAsync</p>
<p>UpdateDocumentAsync</p>
<p>UpdateCollectionAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Between the caller .NET Method objects
and DotNet CosmosDB Collection object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>ReadDocumentAsync</p>
<p>ReadDocumentCollectionAsync</p>
<p>CreateDocumentQuery</p>
<p>ReadItemAsync</p>
<p>ReadItemStreamAsync</p>
<p>ReadContainerAsync</p>
<p>ReadContainerStreamAsync</p>
<p>GetDocumentAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeletetLink</td>
<td class="confluenceTd">Between the caller .NET Method objects
and DotNet CosmosDB Collection object</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>DeleteDocumentAsync</p>
<p>DeleteDocumentCollectionAsync</p>
<p>DeleteItemAsync</p>
<p>DeleteContainerAsync</p>
<p>DeleteContainerStreamAsync</p>
<p>DeleteCollectionAsync</p>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below.

### Azure Cosmos DB Database and Table Creation

#### CreateDatabase and CreateCollection - 2.x

``` c#
private static readonly string databaseName = "samples";
private static readonly string collectionName = "document-samples";

private static async Task Initialize()
        {
            await client.CreateDatabaseIfNotExistsAsync(new Database { Id = databaseName });

            // We create a partitioned collection here which needs a partition key. Partitioned collections
            // can be created with very high values of provisioned throughput (up to OfferThroughput = 250,000)
            // and used to store up to 250 GB of data. You can also skip specifying a partition key to create
            // single partition collections that store up to 10 GB of data.
            DocumentCollection collectionDefinition = new DocumentCollection();
            collectionDefinition.Id = collectionName;

            // For this demo, we create a collection to store SalesOrders. We set the partition key to the account
            // number so that we can retrieve all sales orders for an account efficiently from a single partition,
            // and perform transactions across multiple sales order for a single account number. 
            collectionDefinition.PartitionKey.Paths.Add("/AccountNumber");

            // Use the recommended indexing policy which supports range queries/sorting on strings
            collectionDefinition.IndexingPolicy = new IndexingPolicy(new RangeIndex(DataType.String) { Precision = -1 });

            // Create with a throughput of 1000 RU/s
            await client.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri(databaseName),
                collectionDefinition,
                new RequestOptions { OfferThroughput = 1000 });
        }
```

![](579109026.png)

#### Create Database and Container - 3.x

``` c#
private static async Task Initialize(CosmosClient client)
        {
            database = await client.CreateDatabaseIfNotExistsAsync(databaseId);

            ContainerProperties containerProperties = new ContainerProperties(containerId, partitionKeyPath: "/AccountNumber");

            // Create with a throughput of 1000 RU/s
            container = await database.CreateContainerIfNotExistsAsync(
                containerProperties,
                throughput: 1000);
        }
```

![](581239086.png)

### Insert Operation

#### CreateDocumentAsync - 2.x

``` c#
private static async Task CreateDocumentsAsync()
        {
            Uri collectionUri = UriFactory.CreateDocumentCollectionUri(databaseName, collectionName);
            Console.WriteLine("\n1.1 - Creating documents");
            SalesOrder salesOrder = GetSalesOrderSample("SalesOrder1");
            await client.CreateDocumentAsync(collectionUri, salesOrder);
            SalesOrder2 newSalesOrder = GetSalesOrderV2Sample("SalesOrder2");
            await client.CreateDocumentAsync(collectionUri, newSalesOrder);
        }
```

![](579109027.png)

#### CreateItemAsync - 3.x

CreateItemAsync - 3.x

``` c#
private static async Task<SalesOrder> CreateItemsAsync()
        {
            Console.WriteLine("\n1.1 - Creating items");

            // Create a SalesOrder object. This object has nested properties and various types including numbers, DateTimes and strings.
            // This can be saved as JSON as is without converting into rows/columns.
            SalesOrder salesOrder = GetSalesOrderSample("SalesOrder1");
            ItemResponse<SalesOrder> response = await container.CreateItemAsync(salesOrder, new PartitionKey(salesOrder.AccountNumber));
            SalesOrder salesOrder1 = response;
            Console.WriteLine($"\n1.1.1 - Item created {salesOrder1.Id}");
        }
```

![](581239082.png)

#### CreateDocumentAsync - CosmonautClient

``` c#
public async Task<DocumentStorageResponse<TEntity>> InsertAsync<TEntity>(TEntity entity, DocumentOptions options = null, CancellationToken cancellationToken = default)
        where TEntity : Entity
        {
            var response = await this.documentClient.CreateDocumentAsync(this.databaseName, this.collectionName, entity, options.ToRequestOptions(),     cancellationToken).ConfigureAwait(false);
            return response.ToDocumentStorageResponse();
        }
```

![](587137121.png)

### Select Operation

#### ReadDocumentAsync - 2.x

``` c#
private static async Task ReadDocumentAsync()
        {
            Console.WriteLine("\n1.2 - Reading Document by Id");

            var response = await client.ReadDocumentAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, "SalesOrder1"), 
                new RequestOptions { PartitionKey = new PartitionKey("Account1") });
        }
```

![](579109025.png)

#### ReadDocumentCollectionAsync - 2.x

``` c#
private static async Task ReadDocumentCollectionAsync()
        {
            Console.WriteLine("\n1.7 - Reading a document");
            ResourceResponse<Document> response = await client.ReadDocumentCollectionAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, "SalesOrder3"),
                new RequestOptions { PartitionKey = new PartitionKey("Account1") });
        }
```

![](579109024.png)

#### CreateDocumentAsync - 2.x

``` c#
private static SalesOrder QueryDocuments()
        {
            Console.WriteLine("\n1.4 - Querying for a document using its AccountNumber property");

            SalesOrder querySalesOrder = client.CreateDocumentQuery<SalesOrder>(
                UriFactory.CreateDocumentCollectionUri(databaseName, collectionName))
                .Where(so => so.AccountNumber == "Account1")
                .AsEnumerable()
                .First();

            Console.WriteLine(querySalesOrder.AccountNumber);
            return querySalesOrder;
        }
```

![](579109023.png)

#### ReadItemAsync - 3.x

``` c#
private static async Task ReadItemAsync()
        {
            Console.WriteLine("\n1.2 - Reading Item by Id");

            // Note that Reads require a partition key to be specified.
            ItemResponse<SalesOrder> response = await container.ReadItemAsync<SalesOrder>(
                partitionKey: new PartitionKey("Account1"),
                id: "SalesOrder1");
        }
```

![](581239084.png)

#### GetDocumentAsync - CosmonautClient

``` c#
public Task<TEntity> FindAsync<TEntity>(string id, DocumentOptions options = null, CancellationToken cancellationToken = default)
            where TEntity : Entity
        {
            return this.documentClient.GetDocumentAsync<TEntity>(this.databaseName, this.collectionName, id, options.ToRequestOptions(), cancellationToken);
        }
```

![](587137124.png)

### Update Operation

#### ReplaceDocumentAsync - 2.x

``` c#
    private static async Task ReplaceDocumentAsync(SalesOrder order)
        {
            Console.WriteLine("\n1.5 - Replacing a document using its Id");
            order.ShippedDate = DateTime.UtcNow;
            ResourceResponse<Document> response = await client.ReplaceDocumentAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, order.Id), 
                order);

            var updated = response.Resource;
            Console.WriteLine("Request charge of replace operation: {0}", response.RequestCharge);
            Console.WriteLine("Shipped date of updated document: {0}", updated.GetPropertyValue<DateTime>("ShippedDate"));
        }
```

![](579109029.png)

#### ReplaceCollectionDocumentAsync - 2.x

``` c#
private static async Task ReplaceDocumentCollectionAsync()
        {
            Console.WriteLine("\n1.7 - Updating a document");
            ResourceResponse<Document> response = await client.ReplaceDocumentCollectionAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, "SalesOrder3"),
                new RequestOptions { PartitionKey = new PartitionKey("Account1") });

            Console.WriteLine("Request charge of update operation: {0}", response.RequestCharge);
            Console.WriteLine("StatusCode of operation: {0}", response.StatusCode);
        }
```

![](579109028.png)

#### ReplaceItemAsync - 3.x

``` c#
private static async Task ReplaceItemAsync(SalesOrder order)
        {
            Console.WriteLine("\n1.6 - Replacing a item using its Id");

            order.ShippedDate = DateTime.UtcNow;
            ItemResponse<SalesOrder> response = await container.ReplaceItemAsync(
                partitionKey: new PartitionKey(order.AccountNumber),
                id: order.Id,
                item: order);
        }
```

![](581239081.png)

#### UpdateDocumentAsync - CosmonautClient

``` c#
public async Task<DocumentStorageResponse<TEntity>> UpdateAsync<TEntity>(TEntity entity, DocumentOptions options = null, CancellationToken cancellationToken = default)
            where TEntity : Entity
        {
            var requestOptions = options.ToRequestOptions();
            var document = entity.ToCosmonautDocument(this.settings.JsonSerializerSettings);
            var response = await this.documentClient.UpdateDocumentAsync(this.databaseName, this.collectionName, document, requestOptions, cancellationToken)              .ExecuteCosmosCommand(entity).ConfigureAwait(false);
            return response.ToDocumentStorageResponse();
        }
```

![](587137122.png)

#### ReplaceItemAsync Sample

``` c#
public static async Task Main(string[] args)
        {
            try
            {
                databaseId = "deviceInformation";
                containerId = "device-samples";

                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .AddJsonFile("appSettings.json")
                    .Build();

                string endpoint = configuration["EndPointUrl"];
                if (string.IsNullOrEmpty(endpoint))
                {
                    throw new ArgumentNullException("Please specify a valid endpoint in the appSettings.json");
                }

                string authKey = configuration["AuthorizationKey"];
                if (string.IsNullOrEmpty(authKey) || string.Equals(authKey, "Super secret key"))
                {
                    throw new ArgumentException("Please specify a valid AuthorizationKey in the appSettings.json");
                }

                using (CosmosClient client = new CosmosClient(endpoint, authKey))
                {
                    Database database = await client.CreateDatabaseIfNotExistsAsync(databaseId);

                    // Create the container using REST API without a partition key definition
                    //await Program.CreateNonPartitionedContainerAsync(endpoint, authKey);

                    Container container = database.GetContainer(containerId);

                    // Read back the same container and verify that partition key path is populated
                    // Partition key is returned when read from V3 SDK.
                    ContainerResponse containerResposne = await container.ReadContainerAsync();
                    if (containerResposne.Resource.PartitionKeyPath != null)
                    {
                        Console.WriteLine("Container Partition Key path {0}", containerResposne.Resource.PartitionKeyPath);
                    }
                    else
                    {
                        throw new Exception("Unexpected error : Partition Key is not populated in a migrated collection");
                    }

                    Console.WriteLine("--Demo Item operations with no partition key--");
                    await Program.ItemOperationsWithNonePartitionKeyValue(container);

                    Console.WriteLine("--Demo Item operations with valid partition key--");
                    await Program.ItemOperationsWithValidPartitionKeyValue(container);

                    Console.WriteLine("--Demo migration of items inserted with no partition key to items with a partition key--");
                    await Program.MigratedItemsFromNonePartitionKeyToValidPartitionKeyValue(container);

                    // Clean up the database -- for rerunning the sample
                    await database.DeleteAsync();
                }
            }
            catch (CosmosException cre)
            {
                Console.WriteLine(cre.ToString());
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
                Console.WriteLine("End of demo, press any key to exit.");
                Console.ReadKey();
            }
        }
private static async Task ItemOperationsWithValidPartitionKeyValue(Container container)
        {
            string itemid = Guid.NewGuid().ToString();
            string partitionKey = "a";
            DeviceInformationItem itemWithPK = GetDeviceWithPartitionKey(itemid, partitionKey);

            // Insert a new item
            ItemResponse<DeviceInformationItem> createResponse = await container.CreateItemAsync<DeviceInformationItem>(
             partitionKey: new PartitionKey(partitionKey),
             item: itemWithPK);
            Console.WriteLine("Creating Item {0} with Partition Key Status Code {1}", itemid, createResponse.StatusCode);

            // Read the item back
            ItemResponse<DeviceInformationItem> readResponse = await container.ReadItemAsync<DeviceInformationItem>(
                partitionKey: new PartitionKey(partitionKey),
                id: itemid);
            Console.WriteLine("Reading Item {0} with Partition Key Status Code {1}", itemid, readResponse.StatusCode);

            // Replace the content of the item
            itemWithPK.DeviceId = Guid.NewGuid().ToString();
            ItemResponse<DeviceInformationItem> replaceResponse = await container.ReplaceItemAsync<DeviceInformationItem>(
                 partitionKey: new PartitionKey(partitionKey),
                 id: itemWithPK.Id,
                 item: itemWithPK);
            Console.WriteLine("Replacing Item {0} with Partition Key Status Code {1}", itemid, replaceResponse.StatusCode);

            // Delete the item.
            ItemResponse<DeviceInformationItem> deleteResponse = await container.DeleteItemAsync<DeviceInformationItem>(
                partitionKey: new PartitionKey(partitionKey),
                id: itemid);
            Console.WriteLine("Deleting Item {0} with Partition Key Status Code {1}", itemid, deleteResponse.StatusCode);
        }
```

![](638255164.png)

### Upsert Operation

#### UpsertDocumentAsync - 2.x

``` c#
private static async Task UpsertDocumentAsync()
        {
            Console.WriteLine("\n1.6 - Upserting a document");

            var upsertOrder = GetSalesOrderSample("SalesOrder3");
            ResourceResponse<Document> response = await client.UpsertDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), upsertOrder);
            var upserted = response.Resource;

            Console.WriteLine("Request charge of upsert operation: {0}", response.RequestCharge);
            Console.WriteLine("StatusCode of this operation: {0}", response.StatusCode);
            Console.WriteLine("Id of upserted document: {0}", upserted.Id);
            Console.WriteLine("AccountNumber of upserted document: {0}", upserted.GetPropertyValue<string>("AccountNumber"));

            upserted.SetPropertyValue("AccountNumber", "updated account number");
            response = await client.UpsertDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), upserted);
            upserted = response.Resource;

            Console.WriteLine("Request charge of upsert operation: {0}", response.RequestCharge);
            Console.WriteLine("StatusCode of this operation: {0}", response.StatusCode);
            Console.WriteLine("Id of upserted document: {0}", upserted.Id);
            Console.WriteLine("AccountNumber of upserted document: {0}", upserted.GetPropertyValue<string>("AccountNumber"));
        }
```

![](579109030.png)

#### UpsertItemAsync - 3.x

UpsertItemAsync - 3.x

``` c#
private static async Task UpsertItemAsync()
        {
            Console.WriteLine("\n1.8 - Upserting a item");

            SalesOrder upsertOrder = GetSalesOrderSample("SalesOrder3");
            
            //creates the initial SalesOrder document. 
            //notice the response.StatusCode returned indicates a Create operation was performed
            ItemResponse<SalesOrder> response = await container.UpsertItemAsync(
                partitionKey: new PartitionKey(upsertOrder.AccountNumber),
                item: upsertOrder);
        }
```

![](581239085.png)

#### UpsertDocumentAsync - CosmonautClient

``` c#
public async Task<DocumentStorageResponse<TEntity>> UpsertAsync<TEntity>(TEntity entity, DocumentOptions options = null, CancellationToken cancellationToken = default)
        where TEntity : Entity
        {
              var requestOptions = options.ToRequestOptions();
              var document = entity.ToCosmonautDocument(this.settings.JsonSerializerSettings);
              var response = await this.documentClient.UpsertDocumentAsync(this.databaseName, this.collectionName, document, requestOptions, cancellationToken).ExecuteCosmosCommand(entity).ConfigureAwait(false);
              return response.ToDocumentStorageResponse();
        }
```

![](587137119.png)

### Delete Operation

#### DeleteDocumentAsync - 2.x

``` c#
 private static async Task DeleteDocumentAsync()
        {
            Console.WriteLine("\n1.7 - Deleting a document");
            ResourceResponse<Document> response = await client.DeleteDocumentAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, "SalesOrder3"),
                new RequestOptions { PartitionKey = new PartitionKey("Account1") });

            Console.WriteLine("Request charge of delete operation: {0}", response.RequestCharge);
            Console.WriteLine("StatusCode of operation: {0}", response.StatusCode);
        }
```

![](579109032.png)

#### DeleteDocumentCollectionAsync - 2.x

``` c#
private static async Task DeleteDocumentCollectionAsync()
        {
            Console.WriteLine("\n1.7 - Deleting a document");
            ResourceResponse<Document> response = await client.DeleteDocumentCollectionAsync(
                UriFactory.CreateDocumentUri(databaseName, collectionName, "SalesOrder3"),
                new RequestOptions { PartitionKey = new PartitionKey("Account1") });

            Console.WriteLine("Request charge of delete operation: {0}", response.RequestCharge);
            Console.WriteLine("StatusCode of operation: {0}", response.StatusCode);
        }
```

![](579109034.png)

#### DeleteItemAsync - 3.x

``` c#
private static async Task DeleteItemAsync()
        {
            Console.WriteLine("\n1.9 - Deleting a item");
            ItemResponse<SalesOrder> response = await container.DeleteItemAsync<SalesOrder>(
                partitionKey: new PartitionKey("Account1"),
                id: "SalesOrder3");

            Console.WriteLine("Request charge of delete operation: {0}", response.RequestCharge);
            Console.WriteLine("StatusCode of operation: {0}", response.StatusCode);
        }
```

![](581239083.png)

#### DeleteDocumentAsync - CosmonautClient

``` c#
public async Task<DocumentStorageResponse<TEntity>> RemoveAsync<TEntity>(string id, DocumentOptions options = null, CancellationToken cancellationToken = default)
             where TEntity : Entity
             {
                    var response = await this.documentClient.DeleteDocumentAsync(this.databaseName, this.collectionName, id, options.ToRequestOptions(), cancellationToken)
.ExecuteCosmosCommand<TEntity>(null).ConfigureAwait(false);
                    return response.ToDocumentStorageResponse();
             }
```

![](587137120.png)

#### DeleteContainerAsync Sample

``` c#
public class Program
    {
        //Assign a id for your database & collection 
        private static readonly string DatabaseId = "samples-db";
        private static readonly string ContainerId = "serversidejs-samples";

        // Async main requires c# 7.1 which is set in the csproj with the LangVersion attribute
        // <Main>
        public static async Task Main(string[] args)
        {
            try
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .AddJsonFile("appSettings.json")
                    .Build();

                string endpoint = configuration["EndPointUrl"];
                if (string.IsNullOrEmpty(endpoint))
                {
                    throw new ArgumentNullException("Please specify a valid endpoint in the appSettings.json");
                }

                string authKey = configuration["AuthorizationKey"];
                if (string.IsNullOrEmpty(authKey) || string.Equals(authKey, "Super secret key"))
                {
                    throw new ArgumentException("Please specify a valid AuthorizationKey in the appSettings.json");
                }

                //Read the Cosmos endpointUrl and authorisationKeys from configuration
                //These values are available from the Azure Management Portal on the Cosmos Account Blade under "Keys"
                //NB > Keep these values in a safe & secure location. Together they provide Administrative access to your Cosmos account
                using (CosmosClient client = new CosmosClient(endpoint, authKey))
                {
                    await Program.RunDemoAsync(client, DatabaseId, ContainerId);
                }
            }
            catch (CosmosException cre)
            {
                Console.WriteLine(cre.ToString());
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
                Console.WriteLine("End of demo, press any key to exit.");
                Console.ReadKey();
            }
        }
private static async Task RunDemoAsync(
            CosmosClient client,
            string databaseId,
            string containerId)
        {
            Database database = await client.CreateDatabaseIfNotExistsAsync(DatabaseId);

            ContainerProperties containerSettings = new ContainerProperties(containerId, "/LastName");

            // Delete the existing container to prevent create item conflicts
            using (await database.GetContainer(containerId).DeleteContainerStreamAsync())
            { }

            // Create with a throughput of 1000 RU/s
            Container container = await database.CreateContainerIfNotExistsAsync(
                containerSettings,
                throughput: 1000);

            //Run a simple script
            await Program.RunSimpleScript(container);

            // Run Bulk Import
            await Program.RunBulkImport(container);

            // Run OrderBy
            await Program.RunOrderBy(container);

            //// Uncomment to Cleanup
            //await database.DeleteAsync();
        }
             
```

![](638255165.png)

### Reading App.config

#### CreateDocumentAsync - 2.x

``` c#
private static readonly string collectionName = ConfigurationManager.AppSettings["CollectionName"];
private static readonly string database = ConfigurationManager.AppSettings["DatabaseName"];
private static void InsertDataWithDatabaseAndCollectionArgument(Family family)
{
    await this.client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(database, collectionName), family);

}
```

App.config:

``` xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
<startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.2" />
</startup>
<appSettings>
    <add key="EndPointUrl" value="https://localhost:443/" />
    <add key="AuthorizationKey" value="C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==" />
    <add key="DatabaseName" value="bulkUpdateDb" />
    <add key="CollectionName" value="bulkUpdateColl" />
    <add key="CollectionThroughput" value="100000" />
    <add key="ShouldCleanupOnStart" value="false" />
    <add key="ShouldCleanupOnFinish" value="false" />
    <add key="NumberOfDocumentsToUpdate" value="2500000" />
    <add key="NumberOfBatches" value="25" />
    <add key="CollectionPartitionKey" value="/profileid" />
</appSettings>
```

![](585826413.png)

## Known Limitations

-   Unknown database and collection objects are created when unable to
    resolve Database and Collection