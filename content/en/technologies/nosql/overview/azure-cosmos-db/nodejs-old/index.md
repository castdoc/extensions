---
title: "Support of Azure Cosmos DB for Node.js (< 2.9)"
linkTitle: "Support of Azure Cosmos DB for Node.js (< 2.9)"
type: "docs"
---

>CAST supports Azure Cosmos DB via
its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
extension. Details about how this support is provided for Node.js source
code is discussed below.

## Objects

<table>
<colgroup>
<col/>
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="373522470.png" draggable="false"
data-image-src="373522470.png"
data-unresolved-comment-count="0" data-linked-resource-id="373522470"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_Mongoose_Connection32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="373522447"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Client</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="373522455.png" draggable="false"
data-image-src="373522455.png"
data-unresolved-comment-count="0" data-linked-resource-id="373522455"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="373522447"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Database</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="373522454.png" draggable="false"
data-image-src="373522454.png"
data-unresolved-comment-count="0" data-linked-resource-id="373522454"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="373522447"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Collection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="373522450.png" draggable="false"
data-image-src="373522450.png"
data-unresolved-comment-count="0" data-linked-resource-id="373522450"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_CosmosDB_Unknown_Client .png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="373522447"
data-linked-resource-container-version="2" height="16" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Unknown Client</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="373522453.png" draggable="false"
data-image-src="373522453.png"
data-unresolved-comment-count="0" data-linked-resource-id="373522453"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="373522447"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Unknown Database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="373522452.png" draggable="false"
data-image-src="373522452.png"
data-unresolved-comment-count="0" data-linked-resource-id="373522452"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="373522447"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Node.js CosmosDB Unknown Collection</td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Cosmos DB client creation

``` js
const CosmosClient = require("@azure/cosmos");
const endpoint = '127.0.0.1:8060'

const client = new CosmosClient({ endpoint: endpoint, auth: { masterKey } });
```

### Database and collection creation

``` js
...

const databaseId = 'My database'
const collectionId = 'My collection'

// CREATE DATABASE
const dbResponse = client.databases.createIfNotExists({
  id: databaseId
});
database = dbResponse.database;

// CREATE COLLECTION
const coResponse = database.containers.createIfNotExists({
  id: collectionId
});
container = coResponse.container;
```
![](373522448.png)

### useSelectLink

These declarations create a *useSelectLink* from the source code to the
collection

``` js
// QUERY
var param = {
    query: "SELECT * FROM root r WHERE r.completed=@completed",
    parameters: [
        {
            name: '@completed',
            value: false
        }
    ]
};

const { result: results } = container.items
    .query(param)
    .toArray();
```

### useDeleteLink

This declaration create a *useDeleteLink* from the source code to the
database

``` java
client.database(databaseId).delete()
```
