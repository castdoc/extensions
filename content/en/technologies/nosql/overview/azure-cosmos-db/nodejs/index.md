---
title: "Support of Azure Cosmos DB for Node.js"
linkTitle: "Support of Azure Cosmos DB for Node.js"
type: "docs"
---

>CAST supports Azure Cosmos DB via
its [com.castsoftware.nodejs](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
extension. Details about how this support is provided for Node.js source
code is discussed below.

## Objects

<table>
<colgroup>
<col/>
<col/>
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="647463227.png" draggable="false"
data-image-src="647463227.png"
data-unresolved-comment-count="0" data-linked-resource-id="647463227"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Schema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="647463201"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="647463228.png" draggable="false"
data-image-src="647463228.png"
data-unresolved-comment-count="0" data-linked-resource-id="647463228"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Table32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="647463201"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Collection</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="647463229.png" draggable="false"
data-image-src="647463229.png"
data-unresolved-comment-count="0" data-linked-resource-id="647463229"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnknownSchema32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="647463201"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Node.js Azure CosmosDB Unknown Database</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="647463230.png" draggable="false"
data-image-src="647463230.png"
data-unresolved-comment-count="0" data-linked-resource-id="647463230"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="UnresolvedTable32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="647463201"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Node.js CosmosDB Unknown Collection</td>
</tr>
</tbody>
</table>

## Detailed support

The @azure/cosmos package is supported. Whenever a container client
use one of the method listed in the following table, a link is created
to the corresponding collection.

| LinkType  | Methods from Container | Methods from Item | Methods from Items          |
|-----------|------------------------|-------------------|-----------------------------|
| useDelete | delete                 | delete            | batch, bulk                 |
| useSelect | \-                     | read, query       | batch, bulk, query, readAll |
| useUpdate | \-                     | replace           | batch, bulk, upsert         |
| useInsert | \-                     | \-                | batch, bulk, create         |

If the evaluation of the container name fails (either due to missing
information in the source code or to limitations in the evaluation) a
link is created to an Unknown collection.

## What results can you expect?

When analyzing the following source code:

``` js
const CosmosClient = require("@azure/cosmos");
const client = new CosmosClient({ endpoint: endpoint, auth: { masterKey } });
const databaseId = 'My database'
const collectionId = 'My collection'

// CREATE DATABASE
const dbResponse = client.databases.createIfNotExists({
  id: databaseId
});
database = dbResponse.database;

// CREATE COLLECTION
const coResponse = database.containers.createIfNotExists({
  id: collectionId
});
container = coResponse.container;

// QUERY
var param = {
    query: "SELECT * FROM root r WHERE r.completed=@completed",
    parameters: [
        {
            name: '@completed',
            value: false
        }
    ]
};

const { result: results } = container.items
    .query(param)
    .toArray();


client.database(databaseId).delete()
```

you will get the following result:

![](647463202.png)

## Known Limitations

-   The link type for bulk and batch methods is not evaluated. All
    possible link types are created.
