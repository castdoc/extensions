---
title: "Support of Azure Cosmos DB for Java"
linkTitle: "Support of Azure Cosmos DB for Java"
type: "docs"
---

>CAST supports Azure Cosmos DB via its [com.castsoftware.nosqljava](../../../extensions/com.castsoftware.nosqljava/) extension. Details about how this support is provided for Java source code is discussed below.

## Supported Client Libraries

| Library                                                                                                               | Version       | Supported          |
|-----------------------------------------------------------------------------------------------------------------------|---------------|:------------------:|
| [Azure Cosmos DB Java SDK for SQL API](https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-sdk-java)             | Up to: 4.32.0 | :white_check_mark: |
| [Azure Cosmos DB Async Java SDK for SQL API](https://docs.microsoft.com/en-us/azure/cosmos-db/sql-api-sdk-async-java) | Up to: 4.32.0 | :white_check_mark: |
| [Azure Cosmos DB Java SDK v4](https://docs.microsoft.com/en-us/azure/cosmos-db/sql/sql-api-sdk-java-v4)               | Up to: 4.32.0 | :white_check_mark: |

## Supported Operations

| Operations  | Method Supported                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Insert - v2 | <ul><li>com.microsoft.azure.documentdb.DocumentClient.createDocument</li><li>com.microsoft.azure.documentdb.DocumentClient.createCollection</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.createDocument</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.createCollection</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.createCollection</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.createDocument</li></ul>                                                                                                                                                                                                                                |
| Insert - v4 | <ul><li>com.azure.cosmos.CosmosAsyncContainer.createItem</li><li>com.azure.cosmos.CosmosContainer.createItem</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                       |
| Update - v2 | <ul><li>com.microsoft.azure.documentdb.DocumentClient.replaceDocument</li><li>com.microsoft.azure.documentdb.DocumentClient.upsertDocument</li><li>com.microsoft.azure.documentdb.DocumentClient.replaceCollection</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.replaceDocument</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.replaceCollection </li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.upsertDocument </li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.replaceCollection</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.replaceDocument</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.upsertDocument</li></ul>                                                                                                                                                                                                                              |
| Update - v4 | <ul><li>com.azure.cosmos.CosmosAsyncContainer.replaceItem</li><li>com.azure.cosmos.CosmosAsyncContainer.replace</li><li>com.azure.cosmos.CosmosContainer.replace</li><li>com.azure.cosmos.CosmosContainer.replaceItem</li></ul>                                                                                                                                                                                                                                                                                                              |
| Select - v2 | <details><summary>com.microsoft.azure.documentdb.DocumentClient</summary><ul><li>com.microsoft.azure.documentdb.DocumentClient.queryDocuments</li><li>com.microsoft.azure.documentdb.DocumentClient.readDocument</li><li>com.microsoft.azure.documentdb.DocumentClient.readCollection</li><li>com.microsoft.azure.documentdb.DocumentClient.readCollections</li><li>com.microsoft.azure.documentdb.DocumentClient.queryCollections</li></ul></details><details><summary>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient</summary><ul><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.queryDocuments</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.readDocument</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.readCollection</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.queryCollections</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.readCollections</li></ul></details><details><summary>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl</summary><ul><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.queryCollections</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.queryDocuments</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.readCollection</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.readCollections</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.readDocument</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.readDocuments</li></ul></details><ul><li>com.azure.cosmos.CosmosAsyncContainer.queryItems</li></ul>                                                       |
| Select - v4 | <details><summary>com.azure.cosmos.CosmosAsyncContainer</summary><ul><li>com.azure.cosmos.CosmosAsyncContainer.readItem</li><li>com.azure.cosmos.CosmosAsyncContainer.getId</li><li>com.azure.cosmos.CosmosAsyncContainer.read</li><li>com.azure.cosmos.CosmosAsyncContainer.readAllItems</li><li>com.azure.cosmos.CosmosAsyncContainer.readMany</li></ul></details><details><summary>com.azure.cosmos.CosmosContainer</summary><ul><li>com.azure.cosmos.CosmosContainer.getId</li><li>com.azure.cosmos.CosmosContainer.queryItems</li><li>com.azure.cosmos.CosmosContainer.read</li><li>com.azure.cosmos.CosmosContainer.readItem</li><li>com.azure.cosmos.CosmosContainer.readAllItems</li><li>com.azure.cosmos.CosmosContainer.readMany</li></ul></details> |
| Delete - v2 | <ul><li>com.microsoft.azure.documentdb.DocumentClient.deleteDocument</li><li>com.microsoft.azure.documentdb.DocumentClient.deleteCollection</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.deleteDocument</li><li>com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient.deleteCollection </li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.deleteCollection</li><li>com.microsoft.azure.cosmosdb.rx.internal.RxDocumentClientImpl.deleteDocument</li></ul>                                                                                                                                                                                                                               |
| Delete - v4 | <ul><li>com.azure.cosmos.CosmosAsyncContainer.delete</li><li>com.azure.cosmos.CosmosAsyncContainer.deleteItem</li><li>com.azure.cosmos.CosmosContainer.delete</li><li>com.azure.cosmos.CosmosContainer.deleteItem </li></ul>                                                                                                                                                                                                                                                                                                                 |

Above mentioned methods are supported for both Synchronous and Asynchronous clients.

## Objects

| Icon | Description                                    |
|:----:|------------------------------------------------|
| ![](352682033.png) | Java CosmosDB Database           |
| ![](352682032.png) | Java CosmosDB Collection         |
| ![](352682031.png) | Java Unknown CosmosDB Database   |
| ![](352682030.png) | Java Unknown CosmosDB Collection |

## Links
 
| Link type     | Source and destination of link                                             | Methods Supported                                                                                                       |
|---------------|----------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------|
| belongsTo     | From Java CosmosDB Collection object to Java CosmosDB Database object      | -                                                                                                                       |
| useLink       | Between the caller Java Method objects and Java CosmosDB Collection object | CreateCollection                                                                                                        |
| useInsertLink | Between the caller Java Method objects and Java CosmosDB Collection object | CreateDocument<br>createItem                                                                                            |
| useUpdateLink | Between the caller Java Method objects and Java CosmosDB Collection object | ReplaceDocument<br>replaceItem                                                                                          |
| useSelectLink | Between the caller Java Method objects and Java CosmosDB Collection object | ReadDocument<br>ReadDocuments<br>QueryDocuments<br>ReadCollection<br>queryItems<br>readItem<br>readAllItems<br>readMany |
| useDeleteLink | Between the caller Java Method objects and Java CosmosDB Collection object | DeleteDocument<br>DeleteCollection<br>deleteItem                                                                        |

## What results can you expect?

 Some example scenarios are shown below:

### Cosmos DB Client Creation

CosmoDB client creation

``` java
private static final String HOST = "[YOUR_ENDPOINT_HERE]";
 private static final String MASTER_KEY = "[YOUR_KEY_HERE]";
 
 private static DocumentClient documentClient = new DocumentClient(HOST, MASTER_KEY,
                 ConnectionPolicy.GetDefault(), ConsistencyLevel.Session);
 
 public static DocumentClient getDocumentClient() {
     return documentClient;
 }
```

CosmoDb Async client creation

``` java
import com.microsoft.azure.cosmosdb.rx.AsyncDocumentClient;
    
public AsyncDocumentClient client = new AsyncDocumentClient.Builder()
            .withServiceEndpoint(AccountSettings.HOST)
            .withMasterKeyOrResourceToken(AccountSettings.MASTER_KEY)
            .withConnectionPolicy(ConnectionPolicy.GetDefault())
            .withConsistencyLevel(ConsistencyLevel.Eventual)
            .build();
```

### Insert Data Operation

#### v2

``` java
  public static Family andersenFamily = new Family();

    public void TestCreateDocument() throws DocumentClientException {
        // Insert your Java objects as documents
        System.out.println("Creating doc");

        andersenFamily.setId("Andersen.1");
        andersenFamily.setLastName("Andersen");

        this.client.createDocument("/dbs/familydb/colls/familycoll", andersenFamily, new RequestOptions(), true);

        System.out.println("Done Creating doc");
    }
```

![](352682019.png)

#### v4

``` java
@Override
  public Single<OutcomingAccountStatus> createStatus(IncomingCreateAccountStatus incoming) {

    Mono<YapeAccountStatus> createYapeAccountStatus =
        defer(() -> {
          YapeAccountStatus yapeAccountStatus = new YapeAccountStatus();
          yapeAccountStatus.setId(generateId());
          yapeAccountStatus.setIdYapeAccount(incoming.getIdYapeAccount());
          yapeAccountStatus.setStatus(incoming.getStatus());
          yapeAccountStatus.setCreationDate(incoming.getCreationDate());

          return cosmosAsyncDatabase.getContainer(CONTAINER_YAPE_ACCOUNT_STATUS)
              .createItem(yapeAccountStatus,
                  new PartitionKey(yapeAccountStatus.getIdYapeAccount()),
                  new CosmosItemRequestOptions())
              .retryWhen(noLongerAvailable)
              .map(itemResponse -> yapeAccountStatus);
        }
```

![](591626430.png)

### Select Data Operation

#### v2

``` java
public void TestQueryDocuments() {

        System.out.println("Querying db...");
        FeedResponse<Document> queryResults = this.client.queryDocuments("/dbs/familydb/colls/familycoll",
                "SELECT * FROM Family WHERE Family.id = 'Andersen.1'", null);

        System.out.println("Running SQL query...");
        for (Document family : queryResults.getQueryIterable()) {
            System.out.println(String.format("\tRead %s", family));
        }

    }
```

![](352682018.png)

``` java
public void TestReadDocument() throws DocumentClientException {
        this.client.readDocument("/dbs/familydb/colls/familycoll/docs/Andersen.1", null);
    }
```

![](352682017.png)

``` java
public void TestReadDocuments() throws DocumentClientException {
        this.client.readDocuments("/dbs/familydb", null);
    }
```

#### v4

``` java
private Mono<YapeAccountStatus> getLastStatusMono(String idYapeAccount) {
SqlQuerySpec sqsGetLastStatusQuery = SqlBuilder.create()
.query("select top 1 s.status from s where s.idYapeAccount = @idYapeAccount "
+ "and s.status != @statusDeleted order by s.creationDate desc")
.parameters(
new SqlParameter("@idYapeAccount", idYapeAccount),
new SqlParameter("@statusDeleted", AccountStatus.DELETED.getCode()))
.buildSqlQuerySpec();

CosmosQueryRequestOptions options = new CosmosQueryRequestOptions();
options.setPartitionKey(new PartitionKey(idYapeAccount));

return cosmosAsyncDatabase.getContainer(CONTAINER_YAPE_ACCOUNT_STATUS)
.queryItems(sqsGetLastStatusQuery, options, YapeAccountStatus.class)
.singleOrEmpty()
.retryWhen(noLongerAvailable);
}
```

![](591626429.png)

### Update Operation

#### v2

``` java
public void TestReplaceDocument() throws DocumentClientException {
        // Update a property
        andersenFamily.setLastName("Petersons");
        this.client.replaceDocument("/dbs/familydb/colls/familycoll/docs/Andersen.1", andersenFamily, null);
    }
```

![](352682015.png)

#### v4

``` java
 private Mono<BlockedResources> replaceItem(BlockedResources blockedResources, CosmosItemRequestOptions options) {

    return getBlockedResourcesContainer().replaceItem(blockedResources, blockedResources.getId(),
        new PartitionKey(blockedResources.getResource()), options)
        .retryWhen(noLongerAvailable)
        .doOnSuccess(itemResponse -> blockedResources.set_etag(itemResponse.getETag()))
        .map(itemResponse -> blockedResources);
  }
```

![](591626428.png)

### Delete Operation

``` java
public void TestDeleteDocument() throws DocumentClientException {
        this.client.deleteDocument("", null);
    }
```

![](352682014.png)

## Known Limitations

### Major limitations

No support for Cassandra API, MongoDB API, Gremlin API, Table API or
other APIs built by Microsoft for accessing an Azure Cosmos DB service.
See the API reference
[here](https://docs.microsoft.com/en-us/azure/cosmos-db/):

![](352682013.jpg)

### Minor limitations     

-   Database and collection names are resolved as known objects when a
    connection string is passed to the client directly:

    -   Example: This connection string is passed to client -
        "/dbs/familydb/colls/familycoll" 

-   Database and collection are marked as Unknown when Database
    Collection and document objects are passed as an argument in CRUD
    methods.

-   Runtime Ambiguities are not resolved:

    -   Example: Different databases are accessed in if/else cases based
        on some conditions
