---
title: "Support of Couchbase for Spring Data"
linkTitle: "Support of Couchbase for Spring Data"
type: "docs"
---

>CAST supports Couchbase via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about how this support is provided for Java with Spring Data source
code is discussed below.

## Supported Client Libraries

| Library | Versions | Supported |
|---|---|:---:|
| [Spring Data-couchbase](https://docs.spring.io/spring-data/couchbase/docs/4.0.9.RELEASE/api/) | up to: 4.0.9 | :white_check_mark: |

## Supported Operations

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Operation</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Methods
Supported</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<details><summary>springframework.data.couchbase.core</summary><ul><li>org.springframework.data.couchbase.core.CouchbaseOperations.insert</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.save</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.insert</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.save</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.insertById</li>
<li>org.springframework.data.couchbase.core.ReactiveCouchbaseTemplate.insertById</li></ul></details>
<details><summary>org.springframework.data.couchbase.repository</summary>
<ul><li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.save</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.saveAll</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.save</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.saveAll</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.save</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.saveAll</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.save</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.saveAll</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.save</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.saveAll</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.save</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.saveAll</li>
</ul></details>
<ul><li>org.springframework.data.repository.CrudRepository.save</li>
<li>org.springframework.data.repository.CrudRepository.saveAll</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.insert</li></ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<details><summary>org.springframework.data.couchbase.core.CouchbaseOperations</summary>
<ul><li>org.springframework.data.couchbase.core.CouchbaseOperations.findById</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.findByView</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.queryView</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.findByN1QL</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.findByN1QLProjection</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.findBySpatialView</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.queryN1QL</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.querySpatialView</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.queryView</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.exists</li>
<li>org.springframework.data.couchbase.core.CouchbaseOperations.execute</li></ul>
</details>
<details><summary>org.springframework.data.couchbase.core.CouchbaseTemplate</summary>
<ul><li>org.springframework.data.couchbase.core.CouchbaseTemplate.exists</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.findById</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.findByView</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.queryView</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.findByN1QL</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.findByN1QLProjection</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.findBySpatialView</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.queryN1QL</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.querySpatialView</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.queryView</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.execute</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.existsById</li>
<ul></details>
<details><summary>org.springframework.data.couchbase.repository.support</summary>
<ul><li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.findAll</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.findOne</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.existsById</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.count</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.findAllById</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.findAll</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.findOne</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.existsById</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.count</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.findAllById</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.findAll</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.existsById</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.count</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.findAllById</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.findById</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.findAll</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.existsById</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.count</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.findAllById</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.findById</li></ul></details>
<details><summary>org.springframework.data.couchbase.repository</summary>
<ul><li>org.springframework.data.couchbase.repository.CouchbaseRepository.findById</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.existsById</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.findAllById</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.count</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.findAll</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.findById</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.existsById</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.findAllById</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.count</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.findAll</li></ul></details>
<details><summary>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations</summary>
<ul><li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.exists</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.exists</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.queryN1QL</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.findById</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.queryN1QL</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.queryView</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.querySpatialView</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.findByView</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.findByN1QL</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.findBySpatialView</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.findByN1QLProjection</li></ul></details>
<details><summary>org.springframework.data.repository.CrudRepository</summary>
<ul><li>org.springframework.data.repository.CrudRepository.findById</li>
<li>org.springframework.data.repository.CrudRepository.existsById</li>
<li>org.springframework.data.repository.CrudRepository.findAllById</li>
<li>org.springframework.data.repository.CrudRepository.count</li>
<li>org.springframework.data.repository.CrudRepository.findAll</li></ul></details>
<ul><li>org.springframework.data.couchbase.core.ReactiveCouchbaseTemplate.existsById</li>
<li>org.springframework.data.couchbase.core.ReactiveCouchbaseTemplate.findById</li></ul>

</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">

<details><summary>org.springframework.data.couchbase.repository</summary>
<ul><li>org.springframework.data.couchbase.repository.CouchbaseRepository.deleteAll</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.deleteById</li>
<li>org.springframework.data.couchbase.repository.CouchbaseRepository.delete</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.deleteAll</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.deleteById</li>
<li>org.springframework.data.couchbase.repository.ReactiveCouchbaseRepository.delete</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.deleteAll</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.deleteById</li>
<li>org.springframework.data.couchbase.repository.support.N1qlCouchbaseRepository.delete</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.deleteAll</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.deleteById</li>
<li>org.springframework.data.couchbase.repository.support.ReactiveN1qlCouchbaseRepository.delete</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.deleteAll</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.deleteById</li>
<li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.delete</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.deleteById</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.deleteAll</li>
<li>org.springframework.data.couchbase.repository.support.SimpleReactiveCouchbaseRepository.delete</li></ul></details>
<details><summary>org.springframework.data.couchbase.core</summary>
<ul><li>org.springframework.data.couchbase.core.CouchbaseOperations.remove</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.remove</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.removeById</li>
<li>org.springframework.data.couchbase.core.ReactiveCouchbaseTemplate.removeById</li><li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.remove</li></ul></details>
<ul><li>org.springframework.data.couchbase.repository.support.SimpleCouchbaseRepository.delete</li>
<li>org.springframework.data.repository.CrudRepository.deleteAll</li>
<li>org.springframework.data.repository.CrudRepository.deleteById</li>
<li>org.springframework.data.repository.CrudRepository.delete</li></ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul><li>org.springframework.data.couchbase.core.CouchbaseOperations.update</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.update</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.replaceById</li>
<li>org.springframework.data.couchbase.core.CouchbaseTemplate.upsertById</li>
<li>org.springframework.data.couchbase.core.ReactiveCouchbaseTemplate.replaceById</li>
<li>org.springframework.data.couchbase.core.ReactiveCouchbaseTemplate.upsertById</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.update</li>
<li>org.springframework.data.couchbase.core.RxJavaCouchbaseOperations.save</li></ul>
</div></td>
</tr>
</tbody>
</table>

## Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh" style="text-align: center;">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813836.png" draggable="false"
data-image-src="665813836.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813836"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813815"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Java Couchbase
Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813835.png" draggable="false"
data-image-src="665813835.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813835"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Bucket.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813815"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Java Couchbase
Bucket</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813834.png"/></p></td>
<td class="confluenceTd" style="text-align: center;">Java Couchbase
Collection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813833.png" draggable="false"
data-image-src="665813833.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813833"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="665813815"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Java Couchbase
Unknown Cluster</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813832.png"/></p>
</td>
<td class="confluenceTd" style="text-align: center;">Java Couchbase
Unknown Bucket </td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813831.png"/>
</p>
</td>
<td class="confluenceTd" style="text-align: center;">Java Couchbase
Unknown Collection</td>
</tr>
</tbody>
</table>

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh">Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">parentLink</td>
<td class="confluenceTd"><p>Between Couchbase Cluster object, Couchbase
Bucket object and Couchbase Collection object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Couchbase Collection objects</td>
<td class="confluenceTd"><p>Execute</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>useSelectLink</p></td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Couchbase Collection objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>exists</p>
<p>findById</p>
<p>findall</p>
<p>count</p>
<p>findAllById</p>
<p>findByView</p>
<p>findByN1QL</p>
<p>findBySpatialView</p>
<p>queryN1QL</p>
<p>querySpatialView</p>
<p>queryView</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Couchbase Collection objects</td>
<td class="confluenceTd"><p>update</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Couchbase Collection objects</td>
<td class="confluenceTd"><p>delete</p>
<p>deleteall</p>
<p>deleteById</p>
<p>remove</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller Spring Data Java Method
objects and Couchbase Collection objects</td>
<td class="confluenceTd"><p>insert</p>
<p>save</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Cluster and Bucket Identification 

Bucket identification from properties file

``` java
spring.couchbase.bootstrap-hosts=localhost
spring.couchbase.bucket.name=test
```

![](665813830.png)

Multiple buckets

``` java
@Bean
    public Bucket campusBucket() throws Exception {
        return couchbaseCluster().openBucket("test2", "pwd");
    }
     @Bean
     public CouchbaseTemplate campusTemplate() throws Exception {
         CouchbaseTemplate template = new CouchbaseTemplate(
           couchbaseClusterInfo(), campusBucket(),
           mappingCouchbaseConverter(), translationService());
         template.setDefaultConsistency(getDefaultConsistency());
         return template;
     }

     @Override
     public void configureRepositoryOperationsMapping(
      RepositoryOperationsMapping baseMapping) {
      try {
       baseMapping.mapEntity(Campus.class, campusTemplate());
      } catch (Exception e) {
      }
     }
```

![](665813829.png)

### Cluster and Bucket Identification using Java Configuration

``` java
public class dbConfig extends AbstractCouchbaseConfiguration {
    
    @Value("${couchbase.bucketname}")
    private String bucketName;
    
    @Value("${couchbase.node}")
    private String node;
    
    @Value("${couchbase.password}")
    private String pswd;
    

    @Override
    protected String getBucketName() {
        this.bucketName = bucketName;
        return this.bucketName;
    }

    @Override
    protected String getBucketPassword() {
        this.pswd = pswd;
        return this.pswd;
    }

    @Override
    protected List<String> getBootstrapHosts(String x,String y) {
        // TODO Auto-generated method stub.
        this.node = node;
        List<String> nodes = new ArrayList<String>("one","two","three");
        nodes.add(this.node);
        return nodes;
    }

}
```

![](665813828.png)

### Marking a Cluster and Bucket Unknown

When no Cluster or Bucket Object can be identified, but there are
Couchbase Operations detected from the Source Code. Then an Unknown
Couchbase Cluster and Unknown Couchbase Bucket is Created.

![](665813827.png)

### Couchbase Insert Operation

``` java
@GetMapping("/insert")
    public void insert() {
        CouchbaseOperations cop= context.getBean(campusRepo.class).getCouchbaseOperations();
        List<Campus> campus = new ArrayList<Campus>();
        campus.add(new Campus("SP234","Chennai",new Point(67.456,78.987)));
        campus.add(new Campus("SP456","Hyderabad",new Point(56.274,108.943)));
        cop.insert(campus);
        
    }
```

![](665813826.png)

``` java
@GetMapping("/execute")
    public book executeAct() {
        CouchbaseOperations cop = context.getBean(bookRepo.class).getCouchbaseOperations();
        return cop.execute(new BucketCallback<book>() {
            public book doInBucket() throws java.util.concurrent.TimeoutException,ExecutionException, InterruptedException {
                book bk1 = new book("789UV","Spring-data","framework","Deepak Vohra");
                cop.save(bk1);
                return bk1;
            }});
```

![](665813825.png)

Insert in another bucket

insert op in another bucket

``` java
@GetMapping("/insertObj")
    public void insertObj() {
        CouchbaseOperations cop= context.getBean(campusRepo.class).getCouchbaseOperations();
        Campus campus = new Campus("SP678","Trivandrum",new Point(67.456,78.987));
        cop.insert(campus);
        
    }
```

![](665813824.png)

### Couchbase Update Operation

``` java
@PostMapping("/update")
    public void updateCustomer(@RequestBody Customer customer) {
        CouchbaseOperations cOperations = repository.getCouchbaseOperations();
        cOperations.update(customer);
    }
```

![](665813823.png)

### Couchbase Select Operation 

``` java
@GetMapping("/squery")
    public List<book> SVquery() {
        CouchbaseOperations cop = context.getBean(bookRepo.class).getCouchbaseOperations();
        return cop.findBySpatialView(SpatialViewQuery.from("book","all"),book.class );
    }
```

![](665813822.png)

``` java
@GetMapping("/Vquery")
    public List<book> Vquery() {
        CouchbaseOperations cop = context.getBean(bookRepo.class).getCouchbaseOperations();
        return cop.findByView(ViewQuery.from("book","all"),book.class);
    }
```

![](665813821.png)

Couchbase Delete Operation

``` java
@GetMapping("/removeC")
    public void remC() {
        CouchbaseOperations cop= context.getBean(campusRepo.class).getCouchbaseOperations();
        List<Campus> campus = new ArrayList<Campus>();
        campus.add(new Campus("SP89A","Mumbai",new Point(32.456,160.987)));
        campus.add(new Campus("SPABC","Kolkata",new Point(90.274,186.943)));
        cop.remove(campus);
    }
```

``` java
@GetMapping("/deletebyname/{name}")
    public void deletebyname(@PathVariable(required = true) String name) {
        List<Customer> customers = repository.findByName(name);
        repository.deleteAll(customers);
    }
```

![](665813819.png)

``` java
@GetMapping("/delete/{id}")
    public void delete(@PathVariable(required = true) int id) {
        Optional<Customer> customer = repository.findById(id);
        repository.delete(customer.get());
    }
```

![](665813818.png)

### Couchbase N1QL Query and Custom Methods

``` java
@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "building")
public interface BuildingRepository extends CouchbaseRepository<Building, String> {

    List<Building> findByCompanyId(String companyId);

    Page<Building> findByCompanyIdAndNameLikeOrderByName(String companyId, String name, Pageable pageable);
}
```

![](665813817.png)

### Query Annotations

``` java
@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "building")
public interface BuildingRepository extends CouchbaseRepository<Building, String> {

    List<Building> findByCompanyId(String companyId);

    Page<Building> findByCompanyIdAndNameLikeOrderByName(String companyId, String name, Pageable pageable);

    @Query("#{#n1ql.selectEntity} where #{#n1ql.filter} and companyId = $1 and $2 within #{#n1ql.bucket}")
    Building findByCompanyAndAreaId(String companyId, String areaId);

    @Query("#{#n1ql.selectEntity} where #{#n1ql.filter} AND ANY phone IN phoneNumbers SATISFIES phone = $1 END")
    List<Building> findByPhoneNumber(String telephoneNumber);

    @Query("SELECT COUNT(*) AS count FROM #{#n1ql.bucket} WHERE #{#n1ql.filter} and companyId = $1")
    Long countBuildings(String companyId);
}
```

![](665813816.png)

## Known Limitations

-   Resolution of Database and Collection is limited, "Unknown" is used
    when not resolved.
