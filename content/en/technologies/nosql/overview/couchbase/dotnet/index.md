---
title: "Support of Couchbase for .NET"
linkTitle: "Support of Couchbase for .NET"
type: "docs"
---

>CAST supports Couchbase via
its [com.castsoftware.nosqldotnet](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqldotnet&version=latest)
extension. Details about how this support is provided for .NET source
code is discussed below.

## Supported Client Libraries

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Library</p></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p><a
href="https://docs.couchbase.com/dotnet-sdk/2.7/sample-app-backend.html"
rel="nofollow">Couchbase .NET SDK 2.x</a></p></td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p><a
href="https://docs.couchbase.com/dotnet-sdk/3.2/hello-world/sample-application.html"
rel="nofollow">Couchbase .NET SDK 3.x</a></p></td>
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></p>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Library Version</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Create</td>
<td class="confluenceTd">2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.Cluster.OpenBucket</p>
<p>Couchbase.Core.ICluster.OpenBucket</p>
<p>Couchbase.Cluster.OpenBucketAsync</p>
<p>Couchbase.Core.ICluster.OpenBucketAsync</p>
<p>Couchbase.ClusterHelper.GetBucket</p>
<p>Couchbase.ClusterHelper.GetBucketAsync</p>
<p>Couchbase.Core.ClusterController.CreateBucket</p>
<p>Couchbase.Core.IClusterController.CreateBucket</p>
<p>Couchbase.Management.ClusterManager.CreateBucket</p>
<p>Couchbase.Management.IClusterManager.CreateBucket</p>
<p>Couchbase.Core.ClusterController.CreateBucketAsync</p>
<p>Couchbase.Core.IClusterController.CreateBucketAsync</p>
<p>Couchbase.Management.ClusterManager.CreateBucketAsync</p>
<p>Couchbase.Management.IClusterManager.CreateBucketAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Create</td>
<td class="confluenceTd">3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.IBucket.CollectionAsync</p>
<p>Couchbase.Core.BucketBase.CollectionAsync</p>
<p>Couchbase.KeyValue.IScope.CollectionAsync</p>
<p>Couchbase.KeyValue.Scope.CollectionAsync</p>
<p>Couchbase.IBucket.Collection</p>
<p>Couchbase.Core.BucketBase.Collection</p>
<p>Couchbase.KeyValue.IScope.Collection</p>
<p>Couchbase.KeyValue.Scope.Collection</p>
<p>Couchbase.IBucket.DefaultCollection</p>
<p>Couchbase.Core.BucketBase.DefaultCollection</p>
<p>Couchbase.IBucket.DefaultCollectionAsync</p>
<p>Couchbase.Core.BucketBase.DefaultCollectionAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.CouchbaseBucket.Insert</p>
<p>Couchbase.MemcachedBucket.Insert</p>
<p>Couchbase.Core.IBucket.Insert</p>
<p>Couchbase.CouchbaseBucket.InsertAsync</p>
<p>Couchbase.MemcachedBucket.InsertAsync</p>
<p>Couchbase.Core.IBucket.InsertAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd">3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.KeyValue.CollectionExtensions.InsertAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.InsertAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.InsertAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.CouchbaseBucket.Replace</p>
<p>Couchbase.MemcachedBucket.Replace</p>
<p>Couchbase.Core.IBucket.Replace</p>
<p>Couchbase.CouchbaseBucket.Upsert</p>
<p>Couchbase.MemcachedBucket.Upsert</p>
<p>Couchbase.Core.IBucket.Upsert</p>
<p>Couchbase.CouchbaseBucket.Append</p>
<p>Couchbase.MemcachedBucket.Append</p>
<p>Couchbase.Core.IBucket.Append</p>
<p>Couchbase.CouchbaseBucket.Prepend</p>
<p>Couchbase.MemcachedBucket.Prepend</p>
<p>Couchbase.Core.IBucket.Prepend</p>
<p>Couchbase.CouchbaseBucket.Increment</p>
<p>Couchbase.MemcachedBucket.Increment</p>
<p>Couchbase.Core.IBucket.Increment</p>
<p>Couchbase.CouchbaseBucket.Decrement</p>
<p>Couchbase.MemcachedBucket.Decrement</p>
<p>Couchbase.Core.IBucket.Decrement</p>
<p>Couchbase.CouchbaseBucket.MutateIn</p>
<p>Couchbase.MemcachedBucket.MutateIn</p>
<p>Couchbase.Core.IBucket.MutateIn</p>
<p>Couchbase.CouchbaseBucket.ReplaceAsync</p>
<p>Couchbase.MemcachedBucket.ReplaceAsync</p>
<p>Couchbase.Core.IBucket.ReplaceAsync</p>
<p>Couchbase.CouchbaseBucket.UpsertAsync</p>
<p>Couchbase.MemcachedBucket.UpsertAsync</p>
<p>Couchbase.Core.IBucket.UpsertAsync</p>
<p>Couchbase.CouchbaseBucket.AppendAsync</p>
<p>Couchbase.MemcachedBucket.AppendAsync</p>
<p>Couchbase.Core.IBucket.AppendAsync</p>
<p>Couchbase.CouchbaseBucket.PrependAsync</p>
<p>Couchbase.MemcachedBucket.PrependAsync</p>
<p>Couchbase.Core.IBucket.PrependAsync</p>
<p>Couchbase.CouchbaseBucket.IncrementAsync</p>
<p>Couchbase.MemcachedBucket.IncrementAsync</p>
<p>Couchbase.Core.IBucket.IncrementAsync</p>
<p>Couchbase.CouchbaseBucket.DecrementAsync</p>
<p>Couchbase.MemcachedBucket.DecrementAsync</p>
<p>Couchbase.Core.IBucket.DecrementAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd">3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.KeyValue.CollectionExtensions.ReplaceAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.ReplaceAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.ReplaceAsync</p>
<p>Couchbase.KeyValue.CollectionExtensions.UpsertAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.UpsertAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.UpsertAsync</p>
<p>Couchbase.KeyValue.CollectionExtensions.MutateInAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.MutateInAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.MutateInAsync</p>
<p>Couchbase.KeyValue.BinaryCollectionExtensions.IncrementAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.IncrementAsync</p>
<p>Couchbase.KeyValue.IBinaryCollection.IncrementAsync</p>
<p>Couchbase.KeyValue.BinaryCollectionExtensions.DecrementAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.DecrementAsync</p>
<p>Couchbase.KeyValue.IBinaryCollection.DecrementAsync</p>
<p>Couchbase.KeyValue.CollectionExtensions.TouchAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.TouchAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.TouchAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><p>2.x</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.CouchbaseBucket.Get</p>
<p>Couchbase.MemcachedBucket.Get</p>
<p>Couchbase.Core.IBucket.Get</p>
<p>Couchbase.Cluster.QueryAsync</p>
<p>Couchbase.CouchbaseBucket.QueryAsync</p>
<p>Couchbase.MemcachedBucket.QueryAsync</p>
<p>Couchbase.Core.IBucket.QueryAsync</p>
<p>Couchbase.Core.ICluster.QueryAsync</p>
<p>Couchbase.N1QL.IQueryClient.QueryAsync</p>
<p>Couchbase.N1QL.QueryClient.QueryAsync</p>
<p>Couchbase.Search.ISearchClient.QueryAsync</p>
<p>Couchbase.Search.SearchClient.QueryAsync</p>
<p>Couchbase.Cluster.Query</p>
<p>Couchbase.CouchbaseBucket.Query</p>
<p>Couchbase.MemcachedBucket.Query</p>
<p>Couchbase.Core.IBucket.Query</p>
<p>Couchbase.Core.ICluster.Query</p>
<p>Couchbase.N1QL.IQueryClient.Query</p>
<p>Couchbase.N1QL.QueryClient.Query</p>
<p>Couchbase.Search.ISearchClient.Query</p>
<p>Couchbase.Search.SearchClient.Query</p>
<p>Couchbase.CouchbaseBucket.GetAndTouch</p>
<p>Couchbase.MemcachedBucket.GetAndTouch</p>
<p>Couchbase.Core.IBucket.GetAndTouch</p>
<p>Couchbase.CouchbaseBucket.GetAndLock</p>
<p>Couchbase.MemcachedBucket.GetAndLock</p>
<p>Couchbase.Core.IBucket.GetAndLock</p>
<p>Couchbase.CouchbaseBucket.GetWithLock</p>
<p>Couchbase.MemcachedBucket.GetWithLock</p>
<p>Couchbase.Core.IBucket.GetWithLock</p>
<p>Couchbase.CouchbaseBucket.GetFromReplica</p>
<p>Couchbase.MemcachedBucket.GetFromReplica</p>
<p>Couchbase.Core.IBucket.GetFromReplica</p>
<p>Couchbase.CouchbaseBucket.GetDocument</p>
<p>Couchbase.MemcachedBucket.GetDocument</p>
<p>Couchbase.Core.IBucket.GetDocument</p>
<p>Couchbase.CouchbaseBucket.GetAndTouchDocument</p>
<p>Couchbase.MemcachedBucket.GetAndTouchDocument</p>
<p>Couchbase.Core.IBucket.GetAndTouchDocument</p>
<p>Couchbase.CouchbaseBucket.GetDocumentFromReplica</p>
<p>Couchbase.MemcachedBucket.GetDocumentFromReplica</p>
<p>Couchbase.Core.IBucket.GetDocumentFromReplica</p>
<p>Couchbase.CouchbaseBucket.LookupIn</p>
<p>Couchbase.MemcachedBucket.LookupIn</p>
<p>Couchbase.Core.IBucket.LookupIn</p>
<p>Couchbase.CouchbaseBucket.GetAsync</p>
<p>Couchbase.MemcachedBucket.GetAsync</p>
<p>Couchbase.Core.IBucket.GetAsync</p>
<p>Couchbase.CouchbaseBucket.GetAndLockAsync</p>
<p>Couchbase.MemcachedBucket.GetAndLockAsync</p>
<p>Couchbase.Core.IBucket.GetAndLockAsync</p>
<p>Couchbase.CouchbaseBucket.GetAndTouchAsync</p>
<p>Couchbase.MemcachedBucket.GetAndTouchAsync</p>
<p>Couchbase.Core.IBucket.GetAndTouchAsync</p>
<p>Couchbase.CouchbaseBucket.GetWithLockAsync</p>
<p>Couchbase.MemcachedBucket.GetWithLockAsync</p>
<p>Couchbase.Core.IBucket.GetWithLockAsync</p>
<p>Couchbase.CouchbaseBucket.GetFromReplicaAsync</p>
<p>Couchbase.MemcachedBucket.GetFromReplicaAsync</p>
<p>Couchbase.Core.IBucket.GetFromReplicaAsync</p>
<p>Couchbase.CouchbaseBucket.GetDocumentAsync</p>
<p>Couchbase.MemcachedBucket.GetDocumentAsync</p>
<p>Couchbase.Core.IBucket.GetDocumentAsync</p>
<p>Couchbase.CouchbaseBucket.GetAndTouchDocumentAsync</p>
<p>Couchbase.MemcachedBucket.GetAndTouchDocumentAsync</p>
<p>Couchbase.Core.IBucket.GetAndTouchDocumentAsync</p>
<p>Couchbase.CouchbaseBucket.GetDocumentFromReplicaAsync</p>
<p>Couchbase.MemcachedBucket.GetDocumentFromReplicaAsync</p>
<p>Couchbase.Core.IBucket.GetDocumentFromReplicaAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><p>3.x</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.KeyValue.CollectionExtensions.GetAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.GetAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.GetAsync</p>
<p>Couchbase.Cluster.QueryAsync</p>
<p>Couchbase.ICluster.QueryAsync</p>
<p>Couchbase.KeyValue.CollectionExtensions.LookupInAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.LookupInAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.LookupInAsync,</p>
<p>Couchbase.KeyValue.CollectionExtensions.GetAndTouchAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.GetAndTouchAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.GetAndTouchAsync</p>
<p>Couchbase.KeyValue.CollectionExtensions.GetAndLockAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.GetAndLockAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.GetAndLockAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete </td>
<td class="confluenceTd">2.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.CouchbaseBucket.Remove</p>
<p>Couchbase.MemcachedBucket.Remove</p>
<p>Couchbase.Core.IBucket.Remove</p>
<p>Couchbase.CouchbaseBucket.RemoveAsync</p>
<p>Couchbase.MemcachedBucket.RemoveAsync</p>
<p>Couchbase.Core.IBucket.RemoveAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd">3.x</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Couchbase.KeyValue.CollectionExtensions.RemoveAsync</p>
<p>Couchbase.KeyValue.CouchbaseCollection.RemoveAsync</p>
<p>Couchbase.KeyValue.ICouchbaseCollection.RemoveAsync</p>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon </th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813763.png" draggable="false"
data-image-src="665813763.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813763"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513778"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Couchbase Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813762.png" draggable="false"
data-image-src="665813762.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813762"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Bucket.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513778"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Couchbase Bucket</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813761.png" draggable="false"
data-image-src="665813761.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813761"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513778"
data-linked-resource-container-version="2" height="34" /></p>
</div></td>
<td class="confluenceTd">DotNet Couchbase Collection</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813760.png" draggable="false"
data-image-src="665813760.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813760"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513778"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Cluster</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="665813759.png" draggable="false"
data-image-src="665813759.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813759"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Bucket.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513778"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Couchbase Bucket </td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="665813758.png" draggable="false"
data-image-src="665813758.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813758"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="378513778"
data-linked-resource-container-version="2" height="32" /></p>
</div></td>
<td class="confluenceTd">DotNet Unknown Couchbase Collection</td>
</tr>
</tbody>
</table>

### Links

Links are created for transaction and function point needs:

<table class="relative-table wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh"> Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd"><p>From DotNet Couchbase Bucket object
to DotNet Couchbase Cluster object</p></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Couchbase Buckets/Collections objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>OpenBucket</p>
<p>OpenBucketAsync</p>
<p>GetBucket</p>
<p>GetBucketAsync</p>
<p>CreateBucket</p>
<p>CreateBucketAsync</p>
<p>CollectionAsync</p>
<p>Collection</p>
<p>DefaultCollection</p>
<p>DefaultCollectionAsync</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"> useInsertLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Couchbase Buckets/Collections objects</td>
<td class="confluenceTd"><p>Insert</p>
<p>InsertAsync</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"> useDeleteLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Couchbase Buckets/Collections objects</td>
<td class="confluenceTd"><p>Remove</p>
<p>RemoveAsync</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>useSelectLink</p></td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Couchbase Buckets/Collections objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Get</p>
<p>GetAndTouch</p>
<p>GetAndLock</p>
<p>GetWithLock</p>
<p>GetFromReplica</p>
<p>GetDocument</p>
<p>GetAndTouchDocument</p>
<p>GetDocumentFromReplica</p>
<p>LookupIn</p>
<p>GetAsync</p>
<p>LookupInAsync</p>
<p>GetAndLockAsync</p>
<p>GetAndTouchAsync</p>
<p>GetWithLockAsync</p>
<p>GetFromReplicaAsync</p>
<p>GetDocumentAsync</p>
<p>GetAndTouchDocumentAsync</p>
<p>GetDocumentFromReplicaAsync</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller .NET Class / Method objects
and DotNet Couchbase Buckets/Collections objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Replace</p>
<p>Upsert</p>
<p>Append</p>
<p>Prepend</p>
<p>Increment</p>
<p>Decrement</p>
<p>MutateIn</p>
<p>MutateInAsync</p>
<p>ReplaceAsync</p>
<p>UpsertAsync</p>
<p>AppendAsync</p>
<p>PrependAsync</p>
<p>IncrementAsync</p>
<p>DecrementAsync</p>
<p>TouchAsync</p>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

Some example scenarios are shown below:

### Cluster Creation and Bucket Opening (2.x)

``` c#
class Program
{        
    private static Cluster cluster_1 = new Cluster();
        private const string bucket_name = "GeoBucket";
        private static IBucket geobucket;
        static void Main(string[] args)
        {       
            AuthenticateCluster();
            CreateBucket();
            CouchbaseOpenBucket();
    }
        private static void AuthenticateCluster()
        {            
            Console.WriteLine("Cluster Created - Localhost");
            var authenticator = new PasswordAuthenticator("Administrator", "123456");
            cluster_1.Authenticate(authenticator);
               
        }
        private static void CreateBucket()
        {            
            var cm = cluster_1.CreateManager();
            cm.CreateBucket(bucket_name);
    }
        private static void CouchbaseOpenBucket()
        {            
            geobucket = cluster_1.OpenBucket(bucket_name);
                        return;
        }
}
```

![](378513796.jpg)

### Using ClusterHelper for cluster and bucket (2.x)

``` c#
private static void TestClusterHelper4()        
{
            var config = new ClientConfiguration {
            Servers = new List<Uri>{
            new Uri("http://192.168.56.101:8091"),
            new Uri("http://192.168.56.102:8091")
            }  };
            ClusterHelper.Initialize(config);
            var chbucket = ClusterHelper.GetBucket("defaultWayt");        
}
```

![](378513779.jpg)

### Insert Operation (2.x)

``` c#
private static void InsertDocumentInBucket()        
{
    geobucket.Insert(SetKarDoc());
    geobucket.Insert(SetGoaDoc());
    geobucket.Insert(SetBengaluruDoc());
    geobucket.Insert(SetChandigarhDoc());
}
```

![](378513798.jpg)

### Update Operation (2.x)

UpsertDocumentInBucket

``` c#
private static void UpsertDocumentInBucket()        
{
    geobucket.Upsert(MysuruDoc());        
}
```

![](378513795.jpg)

Upsert

![](378513794.jpg)

Increment

![](638255154.png)

Decrement

![](378513788.jpg)

Append and Prepend

![](378513789.jpg)

### Select Operation (2.x)

``` c#
private static void GetDocumentsFromBucket()        
{
    var result = geobucket.GetDocument<dynamic>("Goa").Content;
}
```

![](378513799.jpg)

### Delete Operation (2.x)

``` c#
private static void DeleteDocumentInBucket()        
{
    geobucket.Remove(MysuruDoc());
}        
```

![](378513800.jpg)

### Cluster, Bucket and Collection Creation (3.x)

``` c#
class Program
{        
    private const string users_collection = "collection6";

    public async Task<Employees> GetTheDoc(Employees value)
        {
            var cluster4 = await Cluster.ConnectAsync("couchbase://localhost", "adm", "jim753");
            var bucket = await cluster4.BucketAsync("NewBucket");

            // get a user-defined collection reference
            var scope = await bucket.ScopeAsync("tenant_agent_00");
            var collection = await scope.CollectionAsync(users_collection);
            
            var collection_in_default_scope = await bucket.CollectionAsync("bookings");
        }
}
```

![](581239098.png)

![](581239097.png)

### Insert Operation (3.x)

InsertDocumentInBucket

``` c#
//create
        public async Task<Employees> PostEmployee(ICluster cluster3, Employees value)
        {
            var bucket = await cluster3.BucketAsync(samplebucket);
            var collection = bucket.DefaultCollection();
            
            // You can access multiple buckets using the same Cluster object.
            var anotherBucket = await cluster3.BucketAsync("AnotherBucket");
            
            // You can access collections other than the default
            var customerC = anotherBucket.Scope("customer-c");
            var trains = customerC.Collection("trains");
            
            int idvalue = value.id;
            var collectiondataa= await trains.InsertAsync( idvalue.ToString(),value );
            if (collectiondataa == null)
            {
                logger.LogError("Error in creating");
            }
            return null;
        }
```

![](581239096.png)

![](581239095.png)

### Update Operation (3.x)

``` c#
//update
        public async Task<Employees> OnlyUpdateEmployee(ICluster cluster_new_2, Employees value)
        {
            var bucket = await cluster_new_2.BucketAsync("SmallBucket");
            var collection = bucket.DefaultCollection();
            
            // You can access multiple buckets using the same Cluster object.
            var anotherBucket = await cluster_new_2.BucketAsync("ShiningBucket");
            
            // You can access collections other than the default
            var customerD = anotherBucket.Scope("customer-D");
            var bikes = customerD.Collection("bikes");
            
            int idvalue = value.id;
            var collectiondataa= await bikes.ReplaceAsync( idvalue.ToString(),value );
            if (collectiondataa == null)
            {
                logger.LogError("Error in creating");
            }
            return null;
        }
```

![](581239094.png)

![](581239093.png)

### Select Operation (3.x)

GetDocumentFromBucket

``` c#
//get
        public async Task<Employees> GetTheDoc(ICluster cluster4, Employees value)
        {
            var bucket = await cluster4.BucketAsync(newbucket);

            // get a user-defined collection reference
            var scope = await bucket.ScopeAsync("tenant_agent_00");
            var collection = await scope.CollectionAsync(users_collection);
            
            var collection_in_default_scope = await bucket.CollectionAsync("bookings");

            // Get Document
            var getResult = await collection.GetAsync("my-document-key");
        }
```

![](581239092.png)

![](581239091.png)

### Delete Operation (3.x)

``` c#
public async Task<Employees> DeleteEmployeeById(ICluster cluster1, int id)
        {
            try
            {
                var bucket = await cluster1.BucketAsync("EmployeesBucket");
            
                // You can access collections other than the default
                var customer10 = bucket.Scope("customer-10");
                var bluecolour = customer10.Collection("bluecolour");
           
                int idvalue = value.id;
                var collectiondataa = await bluecolour.RemoveAsync( idvalue.ToString() );
                if (collectiondataa == null)
                {
                    logger.LogError("Error in creating");
                }
                return null;
            }
            catch (BucketNotFoundException)
            {
                logger.LogError("Bucket not found");
                throw;
            }
        
        }      
```

![](581239090.png)

![](581239089.png)

### N1QL Query (2.x)

``` c#
private static void QueryBucketUsingN1QL()        
{
    var create_index = "CREATE PRIMARY INDEX ON " + bucket_name;
    geobucket.Query<dynamic>(create_index);
    var queryResult = geobucket.Query<dynamic>("SELECT name, state FROM GeoBucket WHERE type = \'city\' and capital=\'Yes\'"); 
    foreach (var row in queryResult.Rows)
        Console.WriteLine(row);
 }
```

![](579109078.png)

![](579109077.png)

### N1QL Query (3.x)

``` c#
public async Task<List<Employees>> GetEmployees(ICluster cluster_query)
        {
            try
            {
                var queryResult = await cluster_query.QueryAsync<Employees>("SELECT username,name,email,age,location,srccs,phoneNumber,salary,skill,managerName,address,id FROM Employees ", new Couchbase.Query.QueryOptions());
                var queryResult2 = await cluster_query.QueryAsync<Employees>("UPDATE cars SET door = red, window = transparent, WHERE door = blue; ", new Couchbase.Query.QueryOptions());
                List<Employees> empList = new List<Employees>();

                await foreach (var row in queryResult)
                {
                    empList.Add(row);
                }

                return empList;

            }
            catch (IndexFailureException)
            {
                logger.LogError("Bucket not found");
                throw;
            }
           

        }
```

![](581239088.png)

![](581239087.png)

### Sub-Document Operations

The following methods sub-document operations are supported:

-   LookupIn
-   MutateIn
    -   ArrayInsert
    -   ArrayAppend
    -   ArrayPrepend
    -   ArrayAddUnique
    -   Remove
    -   Replace
    -   Insert
    -   Upsert

#### LookupIn

``` c#
public static void LookupInMethod2( string id)
{           var builder = geobucket.LookupIn<dynamic>(id).
                Get("type").
                Get("country").
                Exists("notfound");       
}
```

![](378513781.jpg)

#### MutateIn

``` c#
public static void SubDocMisc(string id, string path, string replace_path, object value)
{
    geobucket.MutateIn<dynamic>(id).
        Remove(path).
        Replace(replace_path, value).
        Execute();
}
```

![](579109076.png)

![](579109075.png)

Asynchronous API

-   OpenBucketAsync
-   GetBucketAsync
-   CreateBucketAsync
-   CollectionAsync
-   DefaultCollectionAsync
-   InsertAsync
-   ReplaceAsync
-   UpsertAsync
-   AppendAsync
-   PrependAsync
-   IncrementAsync
-   DecrementAsync
-   QueryAsync
-   GetAsync
-   GetAndLockAsync
-   GetAndTouchAsync
-   GetWithLockAsync
-   GetFromReplicaAsync
-   GetDocumentAsync
-   GetAndTouchDocumentAsync
-   GetDocumentFromReplicaAsync
-   TouchAsync
-   LookupInAsync
-   MutateInAsync
-   QueryAsync

### Reading App.config

Increment Operation 2.x

``` c#
static void Increment(IBucket bucket1, string key)
        {
            var result = bucket1.Increment(key);
            if (result.Success)
            {
                Console.WriteLine(result.Value);
            }
        }
```

app.config

``` xml
<?xml version="1.0" encoding="utf-8" ?>
<configuration>
    <startup> 
        <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.2" />
    </startup>
  <configSections>
    <sectionGroup name="couchbaseClients">
      <section name="couchbase" type="Couchbase.Configuration.Client.Providers.CouchbaseClientSection, Couchbase.NetClient" />
    </sectionGroup>
  </configSections>
  <couchbaseClients>
    <couchbase username="Administrator" password="123456">
      <servers>
        <add uri="http://192.168.56.101:8091"></add>
        </servers>
      <buckets>
        <add name="mybucket"></add>
        <add name="mybucket2"></add>
      </buckets>
    </couchbase>
  </couchbaseClients>
</configuration>
```

![](585826424.png)

## Known Limitations

-   If bucket/collection names are not resolved, the links are created
    between methods and unknown bucket/collection objects
