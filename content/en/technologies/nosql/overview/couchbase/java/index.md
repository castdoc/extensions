---
title: "Support of Couchbase for Java"
linkTitle: "Support of Couchbase for Java"
type: "docs"
---

>CAST supports Couchbase via
its [com.castsoftware.nosqljava](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nosqljava&version=latest)
extension. Details about how this support is provided for Java source
code is discussed below.

## Supported Libraries

| Library | Version | Supported |
|---|---|:---:|
| [Couchbase](https://doc.castsoftware.com/technologies/nosql/overview/couchbase/java/index) | 2.0.0 to 3.4.7 | :white_check_mark: |

## Supported Operations (2.x)

Support is provided for CRUD operations only when jars are configured
inside the classpath. In all other situations, CRUD operations for
Couchbase 2.x are NOT supported.

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><li>Operation</li></th>
<th class="confluenceTh" style="text-align: left;"><li>Methods
Supported</li></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li>com.couchbase.client.java.Bucket.insert</li>
<li>com.couchbase.client.java.AsyncBucket.insert</li>
<li>com.couchbase.client.java.Bucket.upsert</li>
<li>com.couchbase.client.java.Bucket.append</li>
<li>com.couchbase.client.java.Bucket.prepend</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li>com.couchbase.client.java.Bucket.get</li>
<li>com.couchbase.client.java.AsyncBucket.get</li>
<li>com.couchbase.client.java.Bucket.getAndLock</li>
<li>com.couchbase.client.java.Bucket.getAndTouch</li>
<li>com.couchbase.client.java.AsyncBucket.getAndLock</li>
<li>com.couchbase.client.java.query.N1qlQuery.parameterized</li>
<li>com.couchbase.client.java.query.N1qlQuery.simple</li>
<li>com.couchbase.client.java.AsyncBucket.exists</li>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li>com.couchbase.client.java.Bucket.remove</li>
<li>com.couchbase.client.java.AsyncBucket.remove</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li>com.couchbase.client.java.Bucket.replace</li>
<li>com.couchbase.client.java.AsyncBucket.replace</li>
</div></td>
</tr>
</tbody>
</table>

## Supported Operations (3.x)

Support is provided for CRUD operations in Couchbase 3.x in all
situations.

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Operation</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Insert</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>com.couchbase.client.java.Collection.insert</li>
<li>com.couchbase.client.java.Collection.upsert</li>
<li>com.couchbase.client.java.AsyncCollection.insert</li>
<li>com.couchbase.client.java.AsyncCollection.upsert</li>
<li>com.couchbase.client.java.ReactiveCollection.insert</li>
<li>com.couchbase.client.java.ReactiveCollection.upsert</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Select</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1998111566" class="expand-container">
<details><summary>com.couchbase.client.java.Collection</summary>
<div id="expander-content-1998111566" class="expand-content">
<ul>
<li>com.couchbase.client.java.Collection.get</li>
<li>com.couchbase.client.java.Collection.getAndTouch</li>
<li>com.couchbase.client.java.Collection.getAndLock</li>
<li>com.couchbase.client.java.Collection.exists</li>
</ul>
</div>
</details></div>
<div id="expander-1998111565" class="expand-container">
<details><summary>com.couchbase.client.java.AsyncCollection</summary>
<div id="expander-content-1998111565" class="expand-content">
<ul>
<li>com.couchbase.client.java.AsyncCollection.get</li>
<li>com.couchbase.client.java.AsyncCollection.getAndTouch</li>
<li>com.couchbase.client.java.AsyncCollection.getAndLock</li>
<li>com.couchbase.client.java.AsyncCollection.exists</li>
<li>com.couchbase.client.java.AsyncCollection.query</li>
<li>com.couchbase.client.java.AsyncCollection.query</li>
</ul>
</div>
</details></div>
<div id="expander-1998111564" class="expand-container">
<details><summary>com.couchbase.client.java.ReactiveCollection</summary>
<div id="expander-content-1998111564" class="expand-content">
<ul>
<li>com.couchbase.client.java.ReactiveCollection.get</li>
<li>com.couchbase.client.java.ReactiveCollection.getAndTouch</li>
<li>com.couchbase.client.java.ReactiveCollection.getAndLock</li>
<li>com.couchbase.client.java.ReactiveCollection.exists</li>
<li>com.couchbase.client.java.ReactiveCollection.query</li>
<li>com.couchbase.client.java.ReactiveCollection.query</li>
</ul>
</div>
</details></div>

<li>com.couchbase.client.java.Cluster.query</li>
<li>com.couchbase.client.java.Scope.query</li>


</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Delete</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>com.couchbase.client.java.Collection.remove</li>
<li>com.couchbase.client.java.AsyncCollection.remove</li>
<li>com.couchbase.client.java.ReactiveCollection.remove</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Update</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>com.couchbase.client.java.Collection.replace</li>
<li>com.couchbase.client.java.AsyncCollection.replace</li>
<li>com.couchbase.client.java.ReactiveCollection.replace</li>
</div></td>
</tr>
</tbody>
</table>

## What results can you expect?

### Objects

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><li>Icon </li></th>
<th class="confluenceTh"
style="text-align: left;"><li>Description</li></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li><img src="665813778.png" draggable="false"
data-image-src="665813778.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813778"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="client.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="388169840"
data-linked-resource-container-version="4" height="33" /></li>
</div></td>
<td class="confluenceTd">Java Couchbase Cluster</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li><img src="521109581.png" draggable="false"
data-image-src="521109581.png"
data-unresolved-comment-count="0" data-linked-resource-id="521109581"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="bucket.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="388169840"
data-linked-resource-container-version="4" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Couchbase Bucket</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li><img src="665813774.png" draggable="false"
data-image-src="665813774.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="index.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="388169840"
data-linked-resource-container-version="4" height="34" /></li>
</div></td>
<td class="confluenceTd">Java Couchbase Collection (3.x only)</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li><img src="665813776.png" draggable="false"
data-image-src="665813776.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813776"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Cluster.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="388169840"
data-linked-resource-container-version="4" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Couchbase Unknown Cluster</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li> <img src="665813775.png" draggable="false"
data-image-src="665813775.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="Unknown_Bucket.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="388169840"
data-linked-resource-container-version="4" height="32" /></li>
</div></td>
<td class="confluenceTd">Java Couchbase Unknown Bucket </td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<li><img src="665813773.png" draggable="false"
data-image-src="665813773.png"
data-unresolved-comment-count="0" data-linked-resource-id="665813773"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="unknown.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="388169840"
data-linked-resource-container-version="4" height="32" /></li>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<li>Java Couchbase Unknown Collection (3.x only)</li>
</div></td>
</tr>
</tbody>
</table>

### Links (Couchbase 2.x)

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh">Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">belongsTo</td>
<td class="confluenceTd"><li>From Java Couchbase Collection object
to Java Couchbase Bucket object and Java Couchbase Bucket object to Java
Couchbase Cluster object</li></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Java Couchbase Buckets/Collection objects</td>
<td class="confluenceTd"><li>Upsert</li>
<li>N1QL.parameterized</li>
<li>N1QL.simple</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><li>useSelectLink</li></td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Java Couchbase Buckets/Collection objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>get()</li>
<li>async().get()</li>
<li>getAndTouch()</li>
<li>async().getAndTouch()</li>
<li>getAndLock()</li>
<li>async().getAndLock()</li>
<li>exists()</li>
<li>async().exists()</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Java Couchbase Buckets/Collection objects</td>
<td class="confluenceTd"><li>replace()</li>
<li>async().replace()</li>
<li>append()</li>
<li>async().append()</li>
<li>prepend()</li>
<li>async.prepend()</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Java Couchbase Buckets/Collection objects</td>
<td class="confluenceTd"><li>remove()</li>
<li>async().remove()</li></td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Java Couchbase Buckets/Collection objects</td>
<td class="confluenceTd"><li>insert</li>
<li>async().insert()</li></td>
</tr>
</tbody>
</table>

### Links (Couchbase 3.x)

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Source and destination of link</th>
<th class="confluenceTh">Methods supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">parentLink</td>
<td class="confluenceTd"><li>Between Couchbase Connection object and
Bucket object</li>
<li>Between Couchbase bucket object and Collection object</li></td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Couchbase Buckets objects</td>
<td class="confluenceTd"><li>Upsert</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><li>useSelectLink</li></td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Couchbase Buckets objects</td>
<td class="confluenceTd"><div class="content-wrapper">
<li>get()</li>
<li>async().get()</li>
<li>getAndTouch()</li>
<li>async().getAndTouch()</li>
<li>getAndLock()</li>
<li>async().getAndLock()</li>
<li>exists()</li>
<li>async().exists()</li>
<li>Cluster().query()</li>
<li>Scope().query()</li>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Couchbase Buckets objects</td>
<td class="confluenceTd"><li>replace()</li>
<li>async().replace()</li></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Couchbase Buckets objects</td>
<td class="confluenceTd"><li>remove()</li>
<li>async().remove()</li></td>
</tr>
<tr class="even">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Between the caller Couchbase Java Method
objects and Couchbase Buckets objects</td>
<td class="confluenceTd"><li>insert</li>
<li>async().insert()</li></td>
</tr>
</tbody>
</table>

### Code examples

#### Cluster and Bucket Object  - 2.x

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
    }
}
```

![](388169845.png)

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket1 = cluster.openBucket("example1");
    }
}
```

![](397738014.png)

#### Connection, Bucket and Cluster object - 3.x

``` java
  public static void main(String... args) {

    Cluster cluster = Cluster.connect("localhost", "Administrator", "password");

    Bucket bucket = cluster.bucket("travel-sample");
    Scope scope = bucket.scope("_default");
    Collection collection = scope.collection("_default");
```

![](521109582.png)

#### Insert operations - 2.x

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
    }
}
```

![]](insertlink_insert.PNG)

#### Insert operations - 3.x

``` java
public void insert()
{
      System.out.println("\nExample: [insert]");
      // tag::insert[]
      try {
        JsonObject content = JsonObject.create().put("title", "My Blog Post 2");
        MutationResult insertResult = collection.insert("document-key2", content);
      } catch (DocumentExistsException ex) {
        System.err.println("The document already exists!");
      } catch (CouchbaseException ex) {
        System.err.println("Something else happened: " + ex);
      }
      // end::insert[]
    }

```

![](521109575.png)

#### Select operations - 2.x

get()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonDocument document1 = JsonDocument.create(id, content1);


        JsonDocument gets = myBucket.get(document);
        System.out.println(gets);

    }
}
```

getAndTouch()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonDocument document1 = JsonDocument.create(id, content1);

        JsonDocument getandtouch = myBucket.getAndTouch(document);
        System.out.println(getandtouch);
    }
}
```

getAndLock()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonDocument document1 = JsonDocument.create(id, content1);

        JsonDocument getandlock = myBucket.getAndLock(document, 10);
        System.out.println(getandlock);
    }
}
```

exists()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonDocument document1 = JsonDocument.create(id, content1);
        
        Boolean exists = myBucket.exists(document);
        System.out.println(exists);
    }
}
```

![](388169843.png)

#### Select operations - 3.x

``` java
public void get()
{
      System.out.println("\nExample: [get-simple]");
      // tag::get-simple[]
      try {
        GetResult getResult = collection.get("document-key");
        String title = getResult.contentAsObject().getString("title");
        System.out.println(title); // title == "My Blog Post"
      } catch (DocumentNotFoundException ex) {
        System.out.println("Document not found!");
      }
      // end::get-simple[]
    }
public void getAndTouch()
    {
      System.out.println("\nExample: [expiry-touch]");
      // tag::expiry-touch[]
      GetResult result = collection.getAndTouch("my-document3", Duration.ofDays(1));
      // end::expiry-touch[]

      System.out.println("cas value: " + result.cas());
    }
```

![](521109578.png)

#### Update Operations - 2.x

replace()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");

        JsonObject content3 = JsonObject.empty()
                .put("Emp_Trigram", "ABC")
                .put("Emp_id", "001");
        JsonDocument document1 = JsonDocument.create(id, content1);

        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonDocument document1 = JsonDocument.create(id, content1);
        
        JsonDocument removed = myBucket.replace(document1);
        System.out.println(myBucket);
    }
}
```

prepend()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonObject content3 = JsonObject.empty()
                .put("Emp_Trigram", "ABC")
                .put("Emp_id", "001");
        JsonDocument document1 = JsonDocument.create(id, content1);

        JsonDocument prepended = myBucket.prepend(document1);

    }
}
```

append()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonObject content3 = JsonObject.empty()
                .put("Emp_Trigram", "ABC")
                .put("Emp_id", "001");
        JsonDocument document1 = JsonDocument.create(id, content1);

        JsonDocument upserted = myBucket1.upsert(document);
        JsonDocument upserted1 = myBucket1.upsert(document, PersistTo.NONE);
        JsonDocument upserted2 = myBucket1.upsert(document, ReplicateTo.NONE);
        JsonDocument upserted3 = myBucket1.upsert(document, PersistTo.NONE, ReplicateTo.NONE);
        JsonDocument upserted4 = myBucket1.upsert(document, 100, TimeUnit.MILLISECONDS);
        JsonDocument upserted5 = myBucket1.upsert(document, PersistTo.NONE, 100, TimeUnit.MILLISECONDS);
        JsonDocument upserted6 = myBucket1.upsert(document, ReplicateTo.NONE, 100, TimeUnit.MILLISECONDS);
        JsonDocument upserted7 = myBucket1.upsert(document, PersistTo.NONE, ReplicateTo.NONE, 100, TimeUnit.MILLISECONDS);

    }
}
```

![](625705355.png)

#### Update Operations - 3.x

``` java
public void get()
{
      System.out.println("\nExample: [expiry-replace]");
      // tag::expiry-replace[]
      GetResult found = collection.get("my-document3", getOptions().withExpiry(true));

      MutationResult result = collection.replace("my-document3", json,
          replaceOptions().expiry(found.expiryTime().get()));
      // end::expiry-replace[]

      System.out.println("cas value: " + result.cas());
    }
```

![](521109573.png)

#### Delete Operations - 2.x

remove()

``` java
public class App 
{
    public static void main( String[] args )
    {
        
        System.out.println("Create connection");
        final Cluster cluster = CouchbaseCluster.create("127.0.0.1");
        cluster.authenticate("Administrator", "Password");
        Bucket myBucket = cluster.openBucket("example");
        JsonObject content = JsonObject.empty()
                  .put("name", "John Doe")
                  .put("type", "Person")
                  .put("email", "john.doe@mydomain.com")
                  .put("homeTown", "Chicago");
        String id = UUID.randomUUID().toString();
        JsonDocument document = JsonDocument.create(id, content);
        JsonDocument inserted = myBucket.insert(document);
        JsonDocument document1 = JsonDocument.create(id, content1);
        
        JsonDocument removed = myBucket.remove(document);
        System.out.println(myBucket);
    }
}
```

![](388169841.png)

#### Delete Operations - 3.x

``` java
public void remove()

{
      System.out.println("\nExample: [remove]");
      // tag::remove[]
      try {
        collection.remove("my-document");
      } catch (DocumentNotFoundException ex) {
        System.out.println("Document did not exist when trying to remove");
      }
      // end::remove[]
    }
```

![](521109579.png)

#### Batch Operations- 2.x

``` java
int docsToCreate = 100;
List<JsonDocument> documents = new ArrayList<JsonDocument>();
for (int i = 0; i < docsToCreate; i++) {
    JsonObject content = JsonObject.create()
        .put("counter", i)
        .put("name", "Foo Bar");
    documents.add(JsonDocument.create("doc-"+i, content));
}

// Insert them in one batch, waiting until the last one is done.
Observable
    .from(documents)
    .flatMap(new Func1<JsonDocument, Observable<JsonDocument>>() {
        @Override
        public Observable<JsonDocument> call(final JsonDocument docToInsert) {
            return bucket.async().insert(docToInsert);
        }
    })
    .last()
    .toBlocking()
    .single();
```

#### Queries - N1QL - 2.x

``` java
protected static N1qlQuery buildQuery(Statement statement, JsonValue queryPlaceholderValues, ScanConsistency scanConsistency) {
        Cluster cluster = CouchbaseCluster.create("localhost");  
        N1qlParams n1qlParams = N1qlParams.build().consistency(scanConsistency);
         N1qlQuery query;
         if (queryPlaceholderValues instanceof JsonObject && !((JsonObject) queryPlaceholderValues).isEmpty()) {
          query = N1qlQuery.parameterized(statement, (JsonObject) queryPlaceholderValues, n1qlParams);
         } else if (queryPlaceholderValues instanceof JsonArray && !((JsonArray) queryPlaceholderValues).isEmpty()) {
          query = N1qlQuery.parameterized(statement, (JsonArray) queryPlaceholderValues, n1qlParams);
         } else {
          query = N1qlQuery.simple(statement, n1qlParams);
         }
         return query;
        }
```

![](397738015.png)

#### Queries - Cluster - 3.x

``` java
 private void initHotel(String bucketName) {
      hotelcluster = Cluster.connect("HotelServer", "Administrator", "password");  
      hotelBucket = hotelcluster.bucket(bucketName);
      hotelScope = hotelBucket.scope("inventory");
    
  }
  
  public void clusterQuery() throws Exception {
      
      QueryResult result2 = hotelcluster.query("SELECT h.name, h.phone FROM default:`travel-sample`.inventory.hotel h\r\n"
            + "WHERE h.city=\"Manchester\" AND h.directions IS NOT MISSING ORDER BY h.name LIMIT 10",
                queryOptions().metrics(true));
      
      }

  public static void main(String[] args) throws Exception {
    App7 obj = new App7();
    obj.initHotel("hotelBucket");
    obj.scopeQuery();
    obj.clusterQuery();
  }
```

![](639336595.png)

#### Queries - Scope - 3.x

``` java
 private void init(String bucketName) {
        cluster = Cluster.connect("App5127.0.0.1", "Administrator", "password");  
        bucket = cluster.bucket(bucketName);
        scope = bucket.scope("inventory");
    
  }
  
  public void scopeQuery() throws Exception {
 
    QueryResult queryResult2 = scope.query("select *  FROM `passenger`", queryOptions().timeout(Duration.ofSeconds(3)));

  }

  public static void main(String[] args) throws Exception {
    App7 obj = new App7();
    obj.init("airlineBucket");
    obj.scopeQuery();
    obj.clusterQuery();
  }
```

![](620036157.png)

## Known Limitations

-   If the bucket or cluster name is not resolved, then "Java Couchbase
    Unknown Objects" will be created.