---
title: "Python - 1.6"
linkTitle: "1.6"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.python

## What's new?

See [Python 1.6 - Release Notes](rn).

## Description

This extension provides support for Python. If your application contains
Python source code (both .py and .jy extensions are supported) and you
want to view these object types and their links with other objects, then
you should install this extension.

## Files analyzed

| Icons | File | Extension | Note |
|---|---|---|---|
| ![](../images/668336914.png) | Python | .py | Python files - standard extension. |
| ![](../images/668336914.png) | Jython | .jy | By convention, Python files to be run in a Java implementation of the Python interpreter. |
| - | YAML (YAML Ain't Markup Language) | *.yml, *.yaml | Files related to the YAML language, commonly used for configuration purposes. Necessary to interpret Amazon Web Services deployment code. |

## Supported Python versions

| Version | Support |
|---|:-:|
| 3.x | :white_check_mark: |
| 2.x | :white_check_mark: |
| 1.x | :x: |

## Framework support

| Web Service Frameworks                                                         | Client-side  requests | Server-side operations |
| ------------------------------------------------------------------------------ | :--------------------:|:----------------------:|
| Aiohttp (https://docs.aiohttp.org/en/stable/)                                  | :white_check_mark:    | :white_check_mark:     |
| Bottle: https://bottlepy.org/docs/dev/                                         | N/A                   | :white_check_mark:     |
| CherryPy (https://docs.cherrypy.dev/en/latest/)                                | N/A                   | :white_check_mark:     |
| Django: https://www.djangoproject.com/                                         | N/A                   | :white_check_mark:     |
| Falcon (https://falconframework.org/)                                          | N/A                   | :white_check_mark:     |
| FastAPI (https://fastapi.tiangolo.com/)                                        | N/A                   | :white_check_mark:     |
| Flask (https://flask.palletsprojects.com/)                                     | N/A                   | :white_check_mark:     |
| http.client (https://docs.python.org/3/library/http.client.html)               | :white_check_mark:    | N/A                    |
| httplib (https://docs.python.org/2/library/httplib.html)                       | :white_check_mark:    | N/A                    |
| httplib2 (https://pypi.org/project/httplib2/)                                  | :white_check_mark:    | N/A                    |
| Nameko: https://github.com/nameko/nameko                                       | N/A                   | :white_check_mark:     |
| Pyramid (https://docs.pylonsproject.org/projects/pyramid/en/latest/index.html) | N/A                   | :white_check_mark:     |
| Requests (https://docs.python-requests.org/)                                   | :white_check_mark:    | N/A                    |
| Sanic: https://sanic.readthedocs.io/en/stable/                                 | N/A                   | :white_check_mark:     |
| Tornado: https://www.tornadoweb.org/en/stable/                                 | :x:                   | :white_check_mark:     |
| urllib (https://docs.python.org/3/library/urllib.html)                         | :white_check_mark:    | N/A                    |
| urllib2 (https://docs.python.org/2/library/urllib2.html)                       | :white_check_mark:    | N/A                    |
| urllib3 (https://pypi.org/project/urllib3/)                                    | :white_check_mark:    | N/A                    |
| web2py (http://www.web2py.com/)                                                | :white_check_mark:    | :x:                    |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Python extension requires that the
following other CAST extensions are also installed:

-   [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=1.7.30)
-   CAST AIP Internal extension (internal technical extension)

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST Imaging
Console. You can manage the extension using the Application -
Extensions interface.

## Source code discovery

A discoverer is provided with the extension to automatically detect
Python code: a Python project will be discovered for the package's root
folder when at least one .py or .jy (jython) file is detected in the
root folder or any sub-folders. For every Python project located, one
Universal Technology Analysis Unit will be created:

![](../images/668336913.jpg)

## Analysis - Automatic skipping of unit-test code and external libraries

The analyzer skips files that are recognized as forming part of testing
code, i.e., in principle, code not pertaining to production code. The
reason to avoid inclusion of testing code is that many Quality Rule
violations are overrepresented in test code, either because code tends
to be of poorer quality (certainly not critical) or prevalence of
particular testing patterns. Accounting for test code would negatively
impact the total score of the project.

Similarly we skip folders that contain external python libraries.
Currently we only skip the canonical
folders *site-packages* and *dist-packages* (the latter being used in
certain Linux distributions). Not only analyzing external libraries is
discouraged, but it can interfere with correct interpretation of
supported libraries and frameworks, and have a serious impact in memory
consumption and overall analysis performance.

The heuristics used by the analyzer are based on detecting (unit-test)
library imports, and file and path naming conventions as summarized in
the table below:

| Type        | Value                  | HeaderLines | MinimumCount |
|-------------|------------------------|:-----------:|:------------:|
| FilePath    | /test_*.py             |             |              |
| FilePath    | /*_test.py             |             |              |
| FilePath    | /*_test_*.py           |             |              |
| FilePath    | /test/*.py             |             |              |
| FilePath    | /tests/*.py            |             |              |
| FileContent | import unittest        | 12          |              |
| FileContent | from unittest import   | 12          |              |
| FileContent | from nose.tools import | 12          |              |
| FileContent | self.assert            |             | 2            |
| FilePath    | /site-packages/        |             |              |
| FilePath    | /dist-packages/        |             |              |
| FilePath    | /Python*/Lib/          |             |              |
| FilePath    | /Python*/Scripts/      |             |              |
| FilePath    | /Python*/Include/      |             |              |
| FilePath    | /Python*/Bin/          |             |              |

-   The \*\* symbol represents any arbitrary path string, whereas \*
    represents any string without directory slashes.
-   The heuristics above should also similarly valid for .jy (jython)
    files.
-   FilePath match is case-insensitive

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner:

![](../images/668336912.jpg)

*Python Class and method example*

*![](../images/668336911.png)*

*iOS Front-end connected to a Python Flask Back-end.*

### Objects

#### Python

| Icon | Description |
|:-:|---|
| ![](../images/668336910.png) | Python Project<br>Python External Library |
| ![](../images/668336914.png) | Python Module |
| ![](../images/668336909.png) | Python Class |
| ![](../images/668336908.png) | Python Static Initializer |
| ![](../images/668336907.png) | Python Method |
| ![](../images/668336906.png) | Python Script |
| ![](../images/668336905.png) | Python GET (urllib, urllib2, httplib, httplib2, aiohttp) service<br>Python GET service request<br>Python (Flask, aiohttp)Web Service GET operation<br>Python Web Service Get Operation |
| ![](../images/668336904.png) | Python POST (urllib, urllib2, httplib, httplib2, aiohttp) service<br>Python POST service request<br>Python (Flask, aiohttp) Web Service POST operation<br>Python Web Service Post Operation |
| ![](../images/668336903.png) | Python PUT (urllib, urllib2, httplib, httplib2, aiohttp) service<br>Python PUT service request<br>Python (Flask, aiohttp) Web Service PUT operation<br>Python Web Service Post Operation |
| ![](../images/668336902.png) | Python DELETE (urllib, urllib2, httplib, httplib2, aiohttp) service<br>Python DELETE service request<br>Python (Flask, aiohttp) Web Service DELETE operation<br>Python Web Service Delete Operation |
| ![](../images/668336901.jpg) | Python Web Service Any Operation |
| ![](../images/668336900.png) | Python Query<br>Python ORM Mapping<br>Python File Query |
| ![](../images/668336899.png) | RabbitMQ Python QueueCall<br>ActiveMQ Python QueueCall<br>IBM MQ Python QueueCall |
| ![](../images/668336898.png) | RabbitMQ Python QueueReceive<br>ActiveMQ Python QueueReceive<br>IBM MQ Python QueueReceive |
| ![](../images/668336897.png) | Python Call To Java Program |
| ![](../images/668336896.png) | Python Call To Generic Program | 

#### Amazon Web Services

| Icon | Description |
|:-:|---|
| ![](../images/668336894.png) | Python Call to AWS Lambda Function |
| ![](../images/668336893.png) | Python Call to Unknown AWS Lambda Function |
| ![](../images/668336905.png) | Python AWS Lambda GET Operation |
| ![](../images/668336904.png) | Python AWS Lambda POST Operation |
| ![](../images/668336903.png) | Python AWS Lambda PUT Operation |
| ![](../images/668336902.png) | Python AWS Lambda DELETE Operation |
| ![](../images/668336901.jpg) | Python AWS Lambda ANY Operation |
| ![](../images/668336899.png) | Python AWS SQS Publisher<br>Python AWS SNS Publisher |
| ![](../images/668336898.png) | Python AWS SQS Receiver<br>Python AWS SNS Receiver |
| ![](../images/668336892.png) | Python AWS SQS Unknown Publisher<br>Python AWS SNS Unknown Publisher |
| ![](../images/668336891.png) | Python AWS SQS Unknown Receiver<br>Python AWS SNS Unknown Receiver |
| ![](../images/668336890.png) | Python S3 Bucket |
| ![](../images/668336889.png) | Python Unknown S3 Bucket |
| ![](../images/668336888.png) | Python DynamoDB Database |
| ![](../images/668336887.png) | Python DynamoDB Table |
| ![](../images/668336886.png) | Python Unknown DynamoDB Table |
| ![](../images/668336883.png) | Python Email<br>Python SMS |

#### Python callable artifact

*Python Module*, *Python Method, and Python Static Initializer* objects
form part of Python (callable) artifacts.

#### Python static initializers

Available in ≥ 1.6.

The statements of a given class code block are represented by an
*effective Python Static Initializer *object. In this object other class
and method definitions are not included. Class docstrings are excluded
from this object. This particular code is executed at class-definition
time. To represent this in the call-graph, a link between the enclosing
Python Module (or Python Method when the class is defined inside a
method) and the Static Initializer object as shown in the example below:

``` py
# file1.py
 
from file2 import A
```

``` py
# file2.py
 
def f():
    pass
 
def f2():
    pass
 
class A:
 
    f()
 
    def m1(self):
        pass

    f2()
 
# this is code is also executed at import-time.
f2()
```
![](../images/static_initializers.png)

### Links

The following links are created:

-   call links between methods
-   inherit link between hierarchically related classes
-   refer link from methods to class (constructor call)
-   use link between modules through import
-   call links between Python callable artifacts and Python Call
    objects
-   call links between Python Call objects and external programs or
    lambda functions

The following links are created between Python ORM Mapping objects and
database table objects:

-   useSelectLink in case of SELECT operation
-   useDeleteLink in case of DELETE operation
-   useInsertLink in case of INSERT operation
-   useUpdateLink in case of UPDATE operation
-   call links in case of generic operation on S3 buckets

### Structural Rules

The following structural rules are provided: 

| Release | Link |
|---------|------|
| 1.6.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.2-funcrel) |
| 1.6.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.1-funcrel) |
| 1.6.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-funcrel) |
| 1.6.0-alpha9 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha9](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha9) |
| 1.6.0-alpha8 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha8](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha8) |
| 1.6.0-alpha7 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha7](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha7) |
| 1.6.0-alpha6 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha6](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha6) |
| 1.6.0-alpha5 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha5](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha5) |
| 1.6.0-alpha4 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha4](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha4) |
| 1.6.0-alpha3 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha3](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha3) |
| 1.6.0-alpha2 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha2](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha2) |
| 1.6.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_python&ref=\|\|1.6.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_python&ref=%7C%7C1.6.0-alpha1) |

You can also find a global list
here: [https://technologies.castsoftware.com/rules?sec=t_1021000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1021000&ref=%7C%7C)

## Expected results for supported frameworks

Refer to [Results](results) for more information about the results that you can expect for each
supported framework.

## Known Limitations

-   Not fully supported Python Decorator function.

-   The "Avoid disabling certificate check when requesting secured urls"
    for 'urllib3' is only partially supported by detecting the call to
    'urllib3.disable_warnings'.

-   Limited Python resolution that leads to missing links:
    -   No support for \_\_all\_\_
    -   No support for variable of type class, function

-   Flask:

-   -   Objects for other web service operations such as PATCH are not
        generated.
    -   The *endpoint* abstraction layer between functions and
        annotations is not considered. When using
         *add_url_rule* the endpoint argument is taken as the calling
        function name.

-   Cherrypy:
    -   Only support default request.dispatcher
        "cherrypy.dispatch.MethodDispatcher()".

-   Java-Python interoperability via Jython is not supported.
    However the files with the specific extension .jy for Jython is
    analyzed as a regular Python file.

-   Message queues
    -    To generate queue message objects the queue name has to be
        initialized explicitly in the code (dynamic naming not
        supported).

-   SQLAlchemy:
    -   Only raw queries are fully supported.
    -   The ORM api for queries is not supported (no links are created
        towards tables).
