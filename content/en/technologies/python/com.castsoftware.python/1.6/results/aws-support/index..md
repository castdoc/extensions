---
title: "Amazon Web Services support"
linkTitle: "Amazon Web Services support"
type: "docs"
---

## Amazon Web Services SDK

The Python library boto3 for the AWS SDK is supported. The Python
library aws-cdk (v1 and v2) is only supported for AWS Lambdas.

### AWS Lambda in AWS deployment frameworks

The AWS Lambda functions declared in deployment framework configuration
files are analyzed by a different extensions
(*com.castsoftware.cloudconfig*). The Python analyzer will be
responsible, however, of creating the link between Lambda Function
objects having the *runtime* property value consistent with a python
runtime (python3.5, ...) and the corresponding handler (a Python method
object) during the application-level analysis step. It is highly
recommended to add the com.castsoftware.cloudconfig extension so that
proper migration of AWS objects takes place upon upgrading
*com.castsoftware.python* extension from versions \< 1.4.0-beta7.

#### Example

In the .yml deployment file below (taken from the Serverless examples
for AWS) a Lambda function is defined (hello) and the handler's method
name is referred:

``` java
service: aws-python # NOTE: update this with your service name

frameworkVersion: '2'


provider:
  name: aws
  runtime: python3.8
  lambdaHashingVersion: 20201221

functions:
  hello:
    handler: handler.hello
```

Where the Python code of the handler:

``` py
# handler.py

def hello(event, context):
    body = {
        "message": "Go Serverless v2.0! Your function executed successfully!",
        "input": event,
    }

    return {"statusCode": 200, "body": json.dumps(body)}
```

Results:

![](../../../../../images-results/632455920.png)

### AWS Lambda (Boto3)

| Supported API methods (boto3)       | Link Type | Caller                   | Callee                             |
|:------------------------------------|-----------|--------------------------|------------------------------------|
| botocore.client.Lambda.invoke       | callLink  | Python callable artifact | Python Call to AWS Lambda Function |
| botocore.client.Lambda.invoke_async | callLink  | Python callable artifact | Python Call to AWS Lambda Function |

#### Example

A simple example showing representation of an invocation of a AWS Lambda
function:

``` py
def func():
    lambda_client.invoke(FunctionName='otherfunctionname',
                     InvocationType='RequestResponse',
                     Payload=lambda_payload)
```

### AWS SQS (Boto3)

| Supported API methods (boto3)          | Link Type | Caller                                                   | Callee                           |
|:---------------------------------------|-----------|----------------------------------------------------------|----------------------------------|
| botocore.client.SQS.send_message       | callLink  | Python callable artifact                                 | Python AWS SQS Publisher         |
| botocore.client.SQS.send_message_batch | callLink  | Python callable artifact                                 | Python AWS SQS Unknown Publisher |
| botocore.client.SQS.receive_message    | callLink  | Python AWS SQS Unknown Receiver, Python AWS SQS Receiver | Python callable artifact         |

#### Code samples

In this code, the module sqs_send_message.py publishes a message into
the "SQS_QUEUE_URL" queue and in sqs_receive_message.py is received:

``` java
# Adapted from https://boto3.amazonaws.com/v1/documentation/api/latest/guide/sqs-example-sending-receiving-msgs.html#example
# sqs_receive_message.py

import boto3

# Create SQS client
sqs = boto3.client('sqs')

queue_url = 'SQS_QUEUE_URL'

# Receive message from SQS queue
response = sqs.receive_message(QueueUrl=queue_url, ...)
```

and:

``` java
# Adapted from https://boto3.amazonaws.com/v1/documentation/api/latest/guide/sqs-example-sending-receiving-msgs.html#example
# sqs_send_message.py
 
import boto3

# Create SQS client
sqs = boto3.client('sqs')

queue_url = 'SQS_QUEUE_URL'

# Send message to SQS queue
response = sqs.send_message(QueueUrl=queue_url, ...)
```

Results:

![](../../../images-results/632455918.png)

When the name of the queue passed to the API method calls is resolvable
(either because of unavailability or because of technical limitations),
the analyzer will create *Unknown *Publisher and Receive objects.

### AWS SNS (Boto3)

There are two different APIs to manage SNS services, one based on a
low-level *client* and the higher-level one based on *resources*.

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
methods (boto3)</p></th>
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.SNS.create_topic</p></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"><p>N/A</p></td>
<td class="confluenceTd">Determines the topic</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">botocore.client.SNS.publish</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>Python callable artifact</p></td>
<td class="confluenceTd"><p>Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher, Python SMS</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">botocore.client.SNS.publish_batch</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">botocore.client.SNS.subscribe</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>Python AWS SNS Receiver,<br />
Python AWS SNS Unknown Receiver</p></td>
<td class="confluenceTd"><p>Python Call to AWS Lambda Function, <br />
Python AWS SQS Publisher, Python SMS, Python Email</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.create_topic</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">Determines the topic</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.ServiceResource.Topic</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">Determines the topic</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.Topic.publish</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher, Python SMS</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.Topic.subscribe</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>Python AWS SNS Receiver,<br />
Python AWS SNS Unknown Receiver</p></td>
<td class="confluenceTd">Python Call to AWS Lambda Function, <br />
Python AWS SQS Publisher, Python SMS, Python Email</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.PlatformEndpoint.publish</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher, Python SMS</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

The supported protocols are as follows:

| Protocol       | Object/s created                          | Name of the object                                            |
|:---------------|:------------------------------------------|:--------------------------------------------------------------|
| email      | Python AWS Email                          | *an Email*   (the email addresses are not evaluated)          |
| http/https | Python POST service request               | the url (evaluated from the endpoint)                         |
| lambda     | Python Call to AWS Lambda Function        | the name of the lambda function (evaluated from the endpoint) |
| sms        | Python AWS SMS                            | *an SMS*   (the SMS numbers are not evaluated)                |
| sqs        | Python AWS Simple Queue Service Publisher | the name of the queue (evaluated from the endpoint)           |

#### Example

The code example below shows a basic usage of the boto3 library and the
results as seen in Enlighten after analysis of the code.

``` py
import boto3

client = boto3.client('sns', region_name='eu-west-3')
topicArn1 = client.create_topic( Name = "TOPIC1")['TopicArn']

def publish(topic):
    client.publish(TopicArn=topic, Message='<your message>')

def subscribe(topic):
    client.subscribe(TopicArn=topic, Protocol="email", Endpoint="lili@lala.com")
    client.subscribe(TopicArn=topic, Protocol="sms", Endpoint="123456789")
    client.subscribe(TopicArn=topic, Protocol="sqs", Endpoint="arn:partition:service:region:account-id:queueName")
    client.subscribe(TopicArn=topic, Protocol="http", Endpoint="http://foourl")
    client.subscribe(TopicArn=topic, Protocol="lambda", Endpoint="fooarn:function:lambda_name:v2")
    
publish(topicArn1)
subscribe(topicArn1)
```

The *callLink* links between the Publisher and the respective
Subscribers are created by the Web Services Linker extension during
application level.

![](../../../images-results/632455917.png)

For each method a maximum of one subscriber per given topic will be
created as shown in the image above. In the absence of a well-resolved
topic, the analyzer will create Unknown Publishers and Subscribers.
There is no link created between unknown objects.

We can also have direct sms deliveries from calls to publish API
methods:

``` py
import boto3
AWS_REGION = "us-east-1"

def send_sms_from_resource():
    sns = boto3.resource("sns", region_name=AWS_REGION)
    platform_endpoint = sns.PlatformEndpoint('endpointArn')
    platform_endpoint.publish(PhoneNumber='123456789')

def send_sms():
    conn = boto3.client("sns", region_name=AWS_REGION)
    conn.publish(PhoneNumber='123456789')
```

Where the corresponding objects and links are:

![](../../../images-results/632455916.png)

### AWS DynamoDB (Boto3)

See [DynamoDB support for Python source
code](DynamoDB_support_for_Python_source_code).

### AWS S3 (Boto3)

Supported PI methods:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Method</p></th>
<th class="confluenceTh">Link Type (CRUD-like)</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.put_object()</p></td>
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.delete_bucket()</p></td>
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Python callable artifact<br />
<br />
</td>
<td class="confluenceTd"><p>Python S3 Bucket. Python Unknown S3
Bucket</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.delete_object()</p></td>
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Python callable artifact<br />
<br />
</td>
<td class="confluenceTd"><p>Python S3 Bucket. Python Unknown S3
Bucket</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">botocore.client.S3.delete_objects()</td>
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd">Python callable artifact<br />
<br />
</td>
<td class="confluenceTd"><p>Python S3 Bucket. Python Unknown S3
Bucket</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.get_object()</p></td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.get_object_torrent()</p></td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.list_objects()</p></td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">botocore.client.S3.list_objects_v2()</td>
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.put_bucket_logging()</p></td>
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python S3 Bucket, Python Unknown S3 Bucket</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">botocore.client.S3.put_bucket_analytics_configuration()</td>
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python S3 Bucket, Python Unknown S3 Bucket</td>
</tr>
</tbody>
</table>

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
methods<strong>()</strong> (botocore.client.S3)</p></th>
<th class="confluenceTh">Link Type (generic)</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Other effects</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.<strong>create_bucket()</strong></p></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
<td class="confluenceTd">Creation of S3 bucket</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>abort_multipart_upload,
complete_multipart_upload,<br />
copy, copy_object, create_multipart_upload,<br />
delete_bucket_analytics_configuration, delete_bucket_cors,<br />
delete_bucket_encryption,
delete_bucket_intelligent_tiering_configuration,<br />
delete_bucket_inventory_configuration, delete_bucket_lifecycle,<br />
delete_bucket_metrics_configuration,
delete_bucket_ownership_controls,<br />
delete_bucket_policy, delete_bucket_replication,
delete_bucket_tagging,<br />
delete_bucket_website, delete_object_tagging,
delete_public_access_block,<br />
download_file, download_fileobj, generate_presigned_post,<br />
get_bucket_accelerate_configuration,<br />
get_bucket_acl, get_bucket_analytics_configuration,
get_bucket_cors,<br />
get_bucket_encryption,
get_bucket_intelligent_tiering_configuration,<br />
get_bucket_inventory_configuration, get_bucket_lifecycle,<br />
get_bucket_lifecycle_configuration, get_bucket_location,<br />
get_bucket_logging, get_bucket_metrics_configuration,
get_bucket_notification,<br />
get_bucket_notification_configuration,
get_bucket_ownership_controls,<br />
get_bucket_policy, get_bucket_policy_status,
get_bucket_replication,<br />
get_bucket_request_payment, get_bucket_tagging,
get_bucket_versioning,<br />
get_bucket_website, get_object_acl, get_object_legal_hold,<br />
get_object_lock_configuration, get_object_retention,
get_object_tagging,<br />
get_object_torrent, get_public_access_block,<br />
head_bucket, head_object,<br />
list_bucket_analytics_configurations,
list_bucket_intelligent_tiering_configurations,<br />
list_bucket_inventory_configurations,
list_bucket_metrics_configurations,<br />
list_multipart_uploads, list_object_versions, list_parts,<br />
put_bucket_accelerate_configuration, put_bucket_acl,<br />
put_bucket_cors, put_bucket_encryption,
put_bucket_intelligent_tiering_configuration,<br />
put_bucket_inventory_configuration, put_bucket_lifecycle,
put_bucket_lifecycle_configuration,<br />
put_bucket_metrics_configuration, put_bucket_notification,<br />
put_bucket_notification_configuration,<br />
put_bucket_ownership_controls, put_bucket_policy,
put_bucket_replication<br />
put_bucket_request_payment, put_bucket_tagging,
put_bucket_versioning<br />
put_bucket_website, put_object_acl, put_object_legal_hold,
put_object_lock_configuration,<br />
put_object_retention, put_object_tagging, put_public_access_block,
restore_object,<br />
select_object_content, upload_file, upload_fileobj, upload_part,
upload_part_copy</p></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python S3 Bucket, Python Unknown S3 Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

In the absence of a *create_bucket* call, references to buckets in other
method calls are used to create table objects. In the case the name is
well resolved, a regular *S3 Bucket* is created, otherwise an *Unknown
S3 Bucket* is created*.* A maximum of one *Unknown S3 Bucket* per file
is created, however a maximum of one per project (as it is already the
case in analyzers for other languages such as TypeScript) is under
consideration by CAST.

The long list of methods added to the last arrow in the table above
correspond to methods that act on S3 Buckets and presumably using the
AWS SDK API behind the scenes (those few methods only acting on the
boto3 client object are not considered).

## AWS-CDK

### AWS Lambda (AWS-CDK)

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
(aws_cdk, v1 and v2)</p></th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh"><p>Creates object (caller)</p></th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh"><p>Support details</p></th>
<th class="confluenceTh" style="text-align: left;">Remarks</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Function</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python AWS Lambda Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.CfnFunction</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python AWS Lambda Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda_python.PythonFunction</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python AWS Lambda Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd">default runtime = python</td>
<td class="confluenceTd" style="text-align: left;">Only cdk v1</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda_python_alpha.PythonFunction</td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python AWS Lambda Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd">default runtime = python</td>
<td class="confluenceTd" style="text-align: left;">Only cdk v2</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Runtime</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"><p>"from_image" not supported</p></td>
<td class="confluenceTd" style="text-align: left;">Determines the
runtime</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code.from_inline</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">code argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code.inline</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">code argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler (deprecated in cdk v1)</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code.from_asset</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">path argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code.asset</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">path argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler (deprecated in cdk v1)</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.InlineCode</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">code argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.AssetCode</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">path argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.AssetCode.from_asset</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">path argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
</tbody>
</table>

## Known Limitations

-   Monolithic pattern for lambda functions is not properly
    supported
