---
title: "Message Queues support"
linkTitle: "Message Queues support"
type: "docs"
---

## Introduction

Message queues are software-engineering components used for
inter-process communication, or for inter-thread communication within
the same process. They use a queue for messaging. A producer posts
messages to a queue. At the appointed time, the receivers are started up
and process the messages in the queue. A queued message can be stored
and forwarded, and the message can be redelivered until the message is
processed. Message queues enable asynchronous processing, which allows
messages to be queued without the need to process them immediately.

## Message Queues currently handled by the Python analyzer

### ActiveMQ

Apache ActiveMQ is an open source message broker written in Java
together with a full Java Message Service (JMS) client. The goal of
ActiveMQ is to provide standards-based, message-oriented application
integration across as many languages and platforms as possible. ActiveMQ
acts as the middleman allowing heterogeneous integration and interaction
in an asynchronous manner.

### IBM MQ

IBM MQ is a family of network message-oriented middle ware products
that IBM launched. It was originally called MQSeries (for "Message
Queue"), and was renamed *WebSphere MQ* to join the suite
of WebSphere products. IBM MQ allows independent and potentially
non-concurrent applications on a distributed system to securely
communicate with each other. IBM MQ is available on a large number of
platforms (both IBM and non-IBM),
including z/OS (mainframe), OS/400 (IBM System i or AS/400), Transaction
Processing Facility, UNIX, Linux, and Microsoft Windows.

### RabbitMQ

RabbitMQ is an open source message-queueing software called a message
broker or queue manager RabbitMQ implements AMQP. It supports
multiple messaging protocols. RabbitMQ can be deployed in distributed
and federated configurations to meet high-scale, high-availability
requirements.

Message queue applications using the below mentioned frameworks/clients
are handled:

-   Library interface with STOMP protocol for ActiveMQ

-   Pika client with AMQP protocol for RabbitMQ

-   MQ-Light client with TCP/IP for IBM MQ

-   Pymqi python extension for IBM MQ

## Results

When a message queue application is analyzed by the Python analyzer, the
following transactions can be found at the end of analysis:

### Example of ActiveMQ Producer

``` py
import stomp

conn = stomp.Connection10()
conn.start()
conn.connect()
conn.send('SampleQueue', 'Its working!!')
conn.disconnect()
```

### Example of ActiveMQ Consumer

``` py
import stomp

queue = 'SampleQueue'
conn = stomp.Connection10()
conn.start()
conn.connect()
conn.subscribe(queue)
conn.disconnect()
```

![Alt text](../../../images-results/632455911.jpg)

### Example of RabbitMQ Producer

``` py
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
 
channel.queue_declare(queue = "sample_queue")
channel.basic_publish(exchange = '', routing_key = "sample_queue", body = "Hello world!" )
connection.close()
```

### Example of RabbitMQ Consumer

``` py
import pika

def callback(ch, method, properties, body):
    print("[x] Received % r" % body)
 
connectionconnection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.queue_declare(queue = "sample_queue")
channel.basic_consume(callback, queue = "sample_queue", no_ack = True)
channel.start_consuming()
```

![](../../../images-results/632455910.jpg) 

### Example of IBM MQ Producer

``` py
import pymqi

def send_message(self):
    queue_manager = "QM01"
    channel = "SVRCONN.1"
    host = "192.168.1.135"
    port = "1434"
    queue_name = "TEST.QUEUE1"
    message = "Hello from Python!"

    qmgr = pymqi.connect(queue_manager, channel, conn_info)
    queue = pymqi.Queue(qmgr, queue_name)
    queue.put(message)
    queue.close()
    qmgr.disconnect()
```

### Example of IBM MQ Consumer

``` py
import pymqi

def on_message(self,headers, msg):
    queue_manager = "QM01"
    channel = "SVRCONN.1"
    host = "192.168.1.135"
    port = "1434"
    queue_name = "TEST.QUEUE1"

    qmgr = pymqi.connect(queue_manager, channel, conn_info)
    queue = pymqi.Queue(qmgr, queue_name)
    message = queue.get()
    queue.close()
    qmgr.disconnect()
```

![](../../../images-results/632455909.png)

  
