---
title: "Links handled by command line parsers"
linkTitle: "Links handled by command line parsers"
type: "docs"
---

The Python analyzer fulfills the call-links handled by the plac
framework that facilitates the manipulation of command line arguments of
Python scripts.

## Example of call from plac module

``` py
class Interface():
    commands = ['Method2']

    def Method1(self):
        pass

    def Method2(self):
        pass

if __name__ == '__main__':
    plac.Interpreter.call(Interface)
```

In this example, the "script" character of the source file is followed
by the presence of the "if \_\_name\_\_ == ..." structure. This
structure is represented by the analyzer with a *Python main* object
that serves as an entry point for receiving (external) calls. The call
handled by *plac* between *plac.Interpreter.call()* and *Method2* will
be modelized as call-link by the Python analyzer as shown below.

## CAST Enlighten screenshot of call handled by plac

![](574783871.png)