---
title: "Calls to external program from Python"
linkTitle: "Calls to external program from Python"
type: "docs"
---

## Calls to external program from Python

### Introduction

Python, often used to glue together different components of an
application, provides various mechanisms to call external programs. By
supporting these calls the analyzer can provide the linkage between
different technology layers.

| Supported API methods     | Link Type  | Caller   | Callee   |
|:-------------------------|---------|---------------|-----------------------|
| os.system, os.popen, subprocess.call, subprocess.check_call, subprocess.run, subprocess.Popen  | callLink | Python callable artifact | Python Call to Java Program, Python Call to Generic Program |

### Technologies currently handled by the Python analyzer

The Python analyzer currently supports calls to the following
technologies

-   Cobol
-   Java: classes and .jar
-   Python
-   Shell

The Java technology is specific and has its own object because links are
made using the fullname of the class, package and class name.

Furthermore, the link is not made to the class object but directly to
its main method. Indeed, Java program can only be called if they contain
a main method.

### CAST Enlighten screenshots

When a call to an external program is analyzed by the Python analyzer,
the following transactions can be found at the end of analysis:

#### Example of call to an external program

``` py
import subprocess
from subprocess import Popen

subprocess.call('/bin/java com.cast.Classe')
cmd = './hello.sh'
popen = Popen(cmd)
```

#### CAST Enlighten screenshot of call to an external program

![](574783866.png)

Python code can also call a different Python program via the *python*
(or *jython*) executable. Then the analyzer will create, as shown
before, "Python Call to Generic Program" objects and they will be linked
to the corresponding "Python Main" objects during application level
analysis via web service linker extension. For example *launch.py* will
invoke the *run.py* script in the code below

``` py
# launch.py

import subprocess
from subprocess import Popen

cmd = 'python run.py'
popen = Popen(cmd)
```

where the target code contains a code block in the top-level script
environment (signaled by the "*if \_\_name\_\_ ...*" structure).

``` py
# run.py

def run():
    print("running...")

if __name__=="__main__":
    run()
```

so as a results we would have

![](574783867.png)
