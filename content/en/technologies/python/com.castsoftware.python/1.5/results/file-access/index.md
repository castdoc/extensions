---
title: "File system access functions"
linkTitle: "File system access functions"
type: "docs"
---

## File system access functions

Representing end points based on file system access is important to
automatically configure transactions. Towards this goal we aim at
providing automatic recognition of standard library input/output
functions. Currently we provide support for the built-in open
function and the most common methods associated to file-like objects
write, read, writelines, readline, readlines, and
close,  as shown in the example below.

``` py
# file1.py
 
data = """<html>
<header><title>This is title</title></header>
<body>
Hello world
</body>
</html>
"""
 
f = open('page.html', 'w')
f.write(data)
f.close()
```

The objects corresponding to the code of the *file1.py *file are inside
the *Universal Directory* root object. Additionally the analyzer will
automatically generate a *Python external library* object representing
the Python *Standard Library*. Within this, the Python built-in library
is abstracted as a *Python source code* object named *builtins.py,* with
its corresponding *open* function and *file* class (abstracting general
file-like objects) that contains the above mentioned methods*.* No
differences are considered between Python2 and Python3 built-in
functions. Notice the external character of these
objects denoted by gray-shaded icons in the left Object Browser panel.

![](563413408.png)

Due to implementation constraints in CAIP versions \[7.3.6, 8.1\] a
spurious link is generated between the *Python external library*
object and a *PY File* object.