---
title: "Amazon Web Services support"
linkTitle: "Amazon Web Services support"
type: "docs"
---

## Amazon Web Services SDK

The Python library boto3 for the AWS SDK is supported. The Python
library aws-cdk (v1 and v2) is only supported for AWS Lambdas.

### AWS Lambda in AWS deployment frameworks

The AWS Lambda functions declared in deployment framework configuration
files are analyzed by a different extensions
(*com.castsoftware.cloudconfig*). The Python analyzer will be
responsible, however, of creating the link between Lambda Function
objects having the *runtime* property value consistent with a python
runtime (python3.5, ...) and the corresponding handler (a Python method
object) during the application-level analysis step. It is highly
recommended to add the com.castsoftware.cloudconfig extension so that
proper migration of AWS objects takes place upon upgrading
*com.castsoftware.python* extension from versions \< 1.4.0-beta7.

Available in 1.4.0-beta6 and below (deprecated)

Serverless framework, Serverless Application Model (SAM), and
Cloudformation are supported. These are frameworks using \*.yml and
\*.yaml (or \*.json, currently not supported in this extension) file to
set up AWS environment.   
Whenever the runtime set in these files is pythonX.Y,
the com.castsoftware.python extension is responsible for creating the
corresponding Python AWS Lambda Function, Python AWS Lambda Operation
(which represent AWS APIGateway events), and Python AWS Simple Queue
objects. 

#### Example

In the .yml deployment file below (taken from the Serverless examples
for AWS) a Lambda function is defined (hello) and the handler's method
name is referred:

``` java
service: aws-python # NOTE: update this with your service name

frameworkVersion: '2'


provider:
  name: aws
  runtime: python3.8
  lambdaHashingVersion: 20201221

functions:
  hello:
    handler: handler.hello
```

Where the Python code of the handler:

``` py
## handler.py

def hello(event, context):
    body = {
        "message": "Go Serverless v2.0! Your function executed successfully!",
        "input": event,
    }

    return {"statusCode": 200, "body": json.dumps(body)}
```

The results in Enlighten:

![](574783857.png)

### AWS Lambda (Boto3)

<table class="relative-table wrapped confluenceTable"
style="width: 93.3077%;">
<colgroup>
<col style="width: 17%" />
<col style="width: 8%" />
<col style="width: 25%" />
<col style="width: 48%" />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
methods (boto3)</p></th>
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><ul>
<li><p>botocore.client.Lambda.<strong>invoke</strong></p></li>
</ul></td>
<td rowspan="2" class="confluenceTd">callLink</td>
<td rowspan="2" class="confluenceTd">Python callable artifact</td>
<td rowspan="2" class="confluenceTd"><p>Python Call to AWS Lambda
Function</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><ul>
<li>botocore.client.Lambda.<strong>invoke_async</strong></li>
</ul></td>
</tr>
</tbody>
</table>

#### Example

A simple example showing representation of an invocation of a AWS Lambda
function:

``` py
def func():
    lambda_client.invoke(FunctionName='otherfunctionname',
                     InvocationType='RequestResponse',
                     Payload=lambda_payload)
```

### AWS SQS (Boto3)

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
methods (boto3)</p></th>
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><ul>
<li><p>botocore.client.SQS.<strong>send_message</strong></p></li>
<li>botocore.client.SQS.<strong>send_message_batch</strong></li>
</ul></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python AWS SQS Publisher<br />
Python AWS SQS Unknown Publisher</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><ul>
<li>botocore.client.SQS.<strong>receive_message</strong></li>
</ul></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>Python AWS SQS Unknown Receiver<br />
Python AWS SQS Receiver</p></td>
<td class="confluenceTd">Python callable artifact</td>
</tr>
</tbody>
</table>

##### Code samples

In this code, the module sqs_send_message.py publishes a message into
the "SQS_QUEUE_URL" queue and in sqs_receive_message.py is received:

``` java
## Adapted from https://boto3.amazonaws.com/v1/documentation/api/latest/guide/sqs-example-sending-receiving-msgs.html#example
## sqs_receive_message.py

import boto3

## Create SQS client
sqs = boto3.client('sqs')

queue_url = 'SQS_QUEUE_URL'

## Receive message from SQS queue
response = sqs.receive_message(QueueUrl=queue_url, ...)
```

and

``` java
## Adapted from https://boto3.amazonaws.com/v1/documentation/api/latest/guide/sqs-example-sending-receiving-msgs.html#example
## sqs_send_message.py
 
import boto3

## Create SQS client
sqs = boto3.client('sqs')

queue_url = 'SQS_QUEUE_URL'

## Send message to SQS queue
response = sqs.send_message(QueueUrl=queue_url, ...)
```

The results derived from the analysis of the above code can be seen
Enlighten:

*Click to enlarge*

![](574783859.png)

When the name of the queue passed to the API method calls is resolvable
(either because of unavailability or because of technical limitations),
the analyzer will create *Unknown *Publisher and Receive objects.

### AWS SNS (Boto3)

There are two different APIs to manage SNS services, one based on a
low-level *client* and the higher-level one based on *resources*.

<table>
<colgroup>
<col />
<col />
<col />
<col />
<col />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
methods (boto3)</p></th>
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Remarks</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.SNS.<strong>create_topic</strong></p></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"><p>N/A</p></td>
<td class="confluenceTd">Determines the topic</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">botocore.client.SNS.<strong>publish</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>Python callable artifact</p></td>
<td class="confluenceTd"><p>Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher, Python SMS</p></td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">botocore.client.SNS.<strong>publish_batch</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">botocore.client.SNS.<strong>subscribe</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>Python AWS SNS Receiver,<br />
Python AWS SNS Unknown Receiver</p></td>
<td class="confluenceTd"><p>Python Call to AWS Lambda Function, Python
AWS SQS Publisher, Python SMS, Python Email</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.<strong>create_topic</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">Determines the topic</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.ServiceResource.<strong>Topic</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd">Determines the topic</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.Topic.<strong>publish</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher, Python SMS</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.Topic.<strong>subscribe</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>Python AWS SNS Receiver,<br />
Python AWS SNS Unknown Receiver</p></td>
<td class="confluenceTd">Python Call to AWS Lambda Function, Python AWS
SQS Publisher, Python SMS, Python Email</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">boto3.resources.factory.sns.PlatformEndpoint.<strong>publish</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python AWS SNS Publisher,<br />
Python AWS SNS Unknown Publisher, Python SMS</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

The supported protocols are the following:

| Protocol       | Object/s created                          | Name of the object                                            |
|:---------------|:------------------------------------------|:--------------------------------------------------------------|
| email      | Python AWS Email                          | *an Email*   (the email addresses are not evaluated)          |
| http/https | Python POST service request               | the url (evaluated from the endpoint)                         |
| lambda     | Python Call to AWS Lambda Function        | the name of the lambda function (evaluated from the endpoint) |
| sms        | Python AWS SMS                            | *an SMS*   (the SMS numbers are not evaluated)                |
| sqs        | Python AWS Simple Queue Service Publisher | the name of the queue (evaluated from the endpoint)           |

#### Example

The code example below shows a basic usage of the boto3 library and the
results as seen in Enlighten after analysis of the code.

``` py
import boto3

client = boto3.client('sns', region_name='eu-west-3')
topicArn1 = client.create_topic( Name = "TOPIC1")['TopicArn']

def publish(topic):
    client.publish(TopicArn=topic, Message='<your message>')

def subscribe(topic):
    client.subscribe(TopicArn=topic, Protocol="email", Endpoint="lili@lala.com")
    client.subscribe(TopicArn=topic, Protocol="sms", Endpoint="123456789")
    client.subscribe(TopicArn=topic, Protocol="sqs", Endpoint="arn:partition:service:region:account-id:queueName")
    client.subscribe(TopicArn=topic, Protocol="http", Endpoint="http://foourl")
    client.subscribe(TopicArn=topic, Protocol="lambda", Endpoint="fooarn:function:lambda_name:v2")
    
publish(topicArn1)
subscribe(topicArn1)
```

The *callLink* links between the Publisher and the respective
Subscribers are created by the Web Services Linker extension during
application level.

![](574783860.png)

For each method a maximum of one subscriber per given topic will be
created as shown in the image above. In the absence of a well-resolved
topic, the analyzer will create Unknown Publishers and Subscribers.
There is no link created between unknown objects.

We can also have direct sms deliveries from calls to publish API
methods:

``` py
import boto3
AWS_REGION = "us-east-1"

def send_sms_from_resource():
    sns = boto3.resource("sns", region_name=AWS_REGION)
    platform_endpoint = sns.PlatformEndpoint('endpointArn')
    platform_endpoint.publish(PhoneNumber='123456789')

def send_sms():
    conn = boto3.client("sns", region_name=AWS_REGION)
    conn.publish(PhoneNumber='123456789')
```

Where the corresponding objects and links are:

![](574783861.png)

### AWS DynamoDB (Boto3)

See [DynamoDB support for Python source code](../../../../../nosql/overview/dynamodb/python).

### AWS S3 (Boto3)

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 12%" />
<col style="width: 13%" />
<col style="width: 26%" />
<col style="width: 22%" />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
methods</p></th>
<th class="confluenceTh">Link Type (CRUD-like)</th>
<th class="confluenceTh">Caller</th>
<th class="confluenceTh">Callee</th>
<th class="confluenceTh">Other effects</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td
class="confluenceTd"><p>botocore.client.S3.<strong>put_object()</strong></p></td>
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>botocore.client.S3.<strong>delete_bucket()</strong></p></td>
<td rowspan="3" class="confluenceTd">useDeleteLink</td>
<td rowspan="3" class="confluenceTd">Python callable artifact<br />
<br />
</td>
<td rowspan="3" class="confluenceTd"><p>Python S3 Bucket. Python Unknown
S3 Bucket</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>botocore.client.S3.<strong>delete_object()</strong></p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">botocore.client.S3.<strong>delete_objects()</strong></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>botocore.client.S3.<strong>get_object()</strong></p></td>
<td rowspan="4" class="confluenceTd">useSelectLink</td>
<td rowspan="4" class="confluenceTd">Python callable artifact</td>
<td rowspan="4" class="confluenceTd"><p>Python S3 Bucket, Python Unknown
S3 Bucket<br />
<br />
</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>botocore.client.S3.<strong>get_object_torrent()</strong></p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>botocore.client.S3.<strong>list_objects()</strong></p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">botocore.client.S3.<strong>list_objects_v2()</strong></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>botocore.client.S3.<strong>put_bucket_logging()</strong></p></td>
<td rowspan="2" class="confluenceTd">useUpdateLink</td>
<td rowspan="2" class="confluenceTd">Python callable artifact</td>
<td rowspan="2" class="confluenceTd">Python S3 Bucket, Python Unknown S3
Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">botocore.client.S3.<strong>put_bucket_analytics_configuration()</strong></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTh" style="text-align: left;"><p>Supported API
methods<strong>()</strong> (botocore.client.S3)</p></td>
<td class="confluenceTh">Link Type (generic)</td>
<td class="confluenceTh">Caller</td>
<td class="confluenceTh">Callee</td>
<td class="confluenceTh">Other effects</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>botocore.client.S3.<strong>create_bucket()</strong></p></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd"><p>Python S3 Bucket, Python Unknown S3
Bucket</p></td>
<td class="confluenceTd">Creation of S3 bucket</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>abort_multipart_upload,
complete_multipart_upload,<br />
copy, copy_object, create_multipart_upload,<br />
delete_bucket_analytics_configuration, delete_bucket_cors,<br />
delete_bucket_encryption,
delete_bucket_intelligent_tiering_configuration,<br />
delete_bucket_inventory_configuration, delete_bucket_lifecycle,<br />
delete_bucket_metrics_configuration,
delete_bucket_ownership_controls,<br />
delete_bucket_policy, delete_bucket_replication,
delete_bucket_tagging,<br />
delete_bucket_website, delete_object_tagging,
delete_public_access_block,<br />
download_file, download_fileobj, generate_presigned_post,<br />
get_bucket_accelerate_configuration,<br />
get_bucket_acl, get_bucket_analytics_configuration,
get_bucket_cors,<br />
get_bucket_encryption,
get_bucket_intelligent_tiering_configuration,<br />
get_bucket_inventory_configuration, get_bucket_lifecycle,<br />
get_bucket_lifecycle_configuration, get_bucket_location,<br />
get_bucket_logging, get_bucket_metrics_configuration,
get_bucket_notification,<br />
get_bucket_notification_configuration,
get_bucket_ownership_controls,<br />
get_bucket_policy, get_bucket_policy_status,
get_bucket_replication,<br />
get_bucket_request_payment, get_bucket_tagging,
get_bucket_versioning,<br />
get_bucket_website, get_object_acl, get_object_legal_hold,<br />
get_object_lock_configuration, get_object_retention,
get_object_tagging,<br />
get_object_torrent, get_public_access_block,<br />
head_bucket, head_object,<br />
list_bucket_analytics_configurations,
list_bucket_intelligent_tiering_configurations,<br />
list_bucket_inventory_configurations,
list_bucket_metrics_configurations,<br />
list_multipart_uploads, list_object_versions, list_parts,<br />
put_bucket_accelerate_configuration, put_bucket_acl,<br />
put_bucket_cors, put_bucket_encryption,
put_bucket_intelligent_tiering_configuration,<br />
put_bucket_inventory_configuration, put_bucket_lifecycle,
put_bucket_lifecycle_configuration,<br />
put_bucket_metrics_configuration, put_bucket_notification,<br />
put_bucket_notification_configuration,<br />
put_bucket_ownership_controls, put_bucket_policy,
put_bucket_replication<br />
put_bucket_request_payment, put_bucket_tagging,
put_bucket_versioning<br />
put_bucket_website, put_object_acl, put_object_legal_hold,
put_object_lock_configuration,<br />
put_object_retention, put_object_tagging, put_public_access_block,
restore_object,<br />
select_object_content, upload_file, upload_fileobj, upload_part,
upload_part_copy</p></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Python callable artifact</td>
<td class="confluenceTd">Python S3 Bucket, Python Unknown S3 Bucket</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

In the absence of a *create_bucket* call, references to buckets in other
method calls are used to create table objects. In the case the name is
well resolved, a regular *S3 Bucket* is created, otherwise an *Unknown
S3 Bucket* is created*.* A maximum of one *Unknown S3 Bucket* per file
is created, however a maximum of one per project (as it is already the
case in analyzers for other languages such as TypeScript) is under
consideration by CAST.

The long list of methods added to the last arrow in the table above
correspond to methods that act on S3 Buckets and presumably using the
AWS SDK API behind the scenes (those few methods only acting on the
boto3 client object are not considered).

## AWS-CDK

### AWS Lambda (AWS-CDK)

<table>
<colgroup>
<col />
<col />
<col />
<col />
<col />
<col />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Supported API
(aws_cdk, v1 and v2)</p></th>
<th class="confluenceTh" style="text-align: center;">Link type</th>
<th class="confluenceTh" style="text-align: left;"><p>Creates object
(caller)</p></th>
<th class="confluenceTh" style="text-align: center;">Callee</th>
<th class="confluenceTh" style="text-align: left;"><p>Support
details</p></th>
<th class="confluenceTh" style="text-align: center;">Remarks</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.<strong>Function</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd" style="text-align: left;">Python AWS Lambda
Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.<strong>CfnFunction</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd" style="text-align: left;">Python AWS Lambda
Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda_python.<strong>PythonFunction</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd" style="text-align: left;">Python AWS Lambda
Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd" style="text-align: left;">default runtime =
<em>python</em></td>
<td class="confluenceTd" style="text-align: left;">Only cdk v1</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda_python_alpha.<strong>PythonFunction</strong></td>
<td class="confluenceTd">callLink</td>
<td class="confluenceTd" style="text-align: left;">Python AWS Lambda
Function</td>
<td class="confluenceTd">Python Method</td>
<td class="confluenceTd" style="text-align: left;">default runtime =
<em>python</em></td>
<td class="confluenceTd" style="text-align: left;">Only cdk v2</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.<strong>Runtime</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;"><p>"from_image" not
supported</p></td>
<td class="confluenceTd" style="text-align: left;">Determines the
runtime</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code<strong>.from_inline</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"
style="text-align: left;"><em>code</em> argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code<strong>.inline</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"
style="text-align: left;"><em>code</em> argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler (deprecated in cdk v1)</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code<strong>.from_asset</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"
style="text-align: left;"><em>path</em> argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.Code<strong>.asset</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"
style="text-align: left;"><em>path</em> argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler (deprecated in cdk v1)</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.<strong>InlineCode</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"
style="text-align: left;"><em>code</em> argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.<strong>AssetCode</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"
style="text-align: left;"><em>path</em> argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">aws_cdk.aws_lambda.AssetCode<strong>.from_asset</strong></td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
<td class="confluenceTd">N/A</td>
<td class="confluenceTd"
style="text-align: left;"><em>path</em> argument supported</td>
<td class="confluenceTd" style="text-align: left;">Determines the
handler</td>
</tr>
</tbody>
</table>

## Limitations

-   Monolithic pattern for lambda functions is not properly
    supported
