---
title: "Release Notes - 1.5"
linkTitle: "Release Notes"
type: "docs"
---

## 1.5.4-funcrel

### Performance Improvements

| Summary |
| ------- |
| Fixed CASTONCAST analysis performance issue |

## 1.5.3-funcrel

### Resolved Issues

| Customer Ticket Id | Details |
| ------------------ | ------- |
| 42086 | Fixes the traceback error "TypeError: sequence item 4: expected str instance, NoneType found" with certain type hint annotations. |

### New Support

| Summary | Details |
| ------- | ------- |
| Support parenthesized context managers | Support provided for the use of parentheses in "with" statements. |

## 1.5.2-funcrel

### Resolved Issues

| Customer Ticket Id | Details |
| ------------------ | ------- |
| 38882 | Fix stalled analysis in files with complex string reconstructions by introducing a timeout. |

### Other Updates

| Details |
| ------- |
| Fix a false detection of http queries (requests library). This fixes false positive in rule (1021004) "Avoid using a web service with Python requests inside a loop". |

## 1.5.1-funcrel

### Other Updates

| Details |
| ------- |
| Fixed an issue with implicit string concatenation. Now multiline strings are managed correctly. |
| Declared AWS S3 bucket and DynamoDB tables as Data Entities (instead of end points). This is relevant for function point counting. |

### New Support

| Summary | Details |
| ------- | ------- |
| Support "PURL" library | URLs manipulated with the "PURL" library are now correctly evaluated. This can impact positively in the linking of Web Service objects. |

## 1.5.0-funcrel

### Note

Improvements to the lexer are made in this release, to correctly handle nested quotation marks in Python strings. These improvements introduce meaningful whitespace characters which were previously missing in the respective AST nodes. This in turn affects the checksum values of objects that contain these nodes, hence upgrading to this release may impact the existing analysis results.
### Other Updates

| Details |
| ------- |
| Correct interpretation of strings with nested quotes, including escape characters. |
| Add default entry/exit points for AWS objects. |

## 1.5.0-alpha3

### Other Updates

| Details |
| ------- |
| Implement naming of web frameworks specifically for CAST Imaging. |
| Update type description for AWS related objects. |

### New Support

| Summary | Details |
| ------- | ------- |
| Support Pyramid web service operations | Creation of REST operations and links to the Python handler methods. |
| Support "string.Template" for string interpolation | Add support string.Template.__init__, string.Template.substitute and string.Template.safe_substitute |

## 1.5.0-alpha2

### Resolved Issues

| Customer Ticket Id | Details |
| ------------------ | ------- |
| 34909 | Correct false violation for the rule 'Avoid string interpolations to prevent SQL injections(Python)' when using f-strings. |

### Other Updates

| Details |
| ------- |
| Enhance support of resolutions (dotaccess). |

### Rules

| Rule Id | New Rule | Details |
| ------- | -------- | ------- |
| 1021044 | FALSE | Avoid string interpolations to prevent SQL injections (Python). |

### New Support

| Summary | Details |
| ------- | ------- |
| Support for Django and Django Rest Frameworks | Creation of REST operations and links to the Python handler methods. |
| Support Tornado Framework for server-side | Creation of REST operations and links to the Python handler methods. |

## 1.5.0-alpha1

### Other Updates

| Details |
| ------- |
| Enhance support of string evaluations (raw string supported). |
| Add new common protocol category inheritance to PYTHON AWS DynamoDB Table objects to improve application level code handling. |

### New Support

| Summary | Details |
| ------- | ------- |
| Support Nameko web framework | Creation of REST operations and links to the Python handler methods. |
| Support of Dictionary Merge & Update Operations | Support for new syntax in Python 3.9. |
| Support removeprefix and removesuffix built in methods | Support for new syntax in Python 3.9. |
