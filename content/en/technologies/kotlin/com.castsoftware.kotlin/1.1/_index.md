---
title: "Kotlin - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.kotlin

## What's new?

See [KRelease Notes](rn/).

## Description

If your application contains Kotlin source code and you want to view
these object types and their links with other objects, then you should
install this extension. All files with the .kt extension are analyzed.
Kotlin code and Java code may coexist in the same application, and a
Kotlin method may call a Java method, and inversely. If a JEE analysis
is defined for the Application, call links from Kotlin methods to Java
methods should be created. But links from Java methods to Kotlin methods
are not supported yet: this is a limitation.

## Technology support

- Kotlin 1.x

## Function Point, Quality and Sizing support

- Function Points (transactions): A green tick indicates that OMG Function Point
counting and Transaction Risk Index are supported.
- Quality and Sizing: A green tick indicates that CAST can measure
size and that a minimum set of Quality Rules exist.

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically installed by CAST Imaging Console when at least one .kt file is delivered for analysis.

## Source code discovery

A discoverer is provided with the extension to automatically detect
Kotlin code: one Kotlin project will be discovered for the package's
root folder when at least one .kt file is detected in the root folder or
any sub-folders, resulting in a corresponding Universal Technology
Analysis Unit.

## Supported frameworks

The following libraries are supported for Web Service HTTP calls:

- Retrofit
- Android Volley

Once the Kotlin extension analysis is finished, the analyzer will output
the final number of web service calls created.

### Retrofit

``` js
import retrofit2.http.GET
import retrofit2.http.POST
interface ApiInterfaces {
@GET("v2/Contacts")
fun getContacts(): Call<CrmContacts>

@POST("v2/Contacts")
fun addContact(): Call<AddResponse>
}
```

This code produces Web Service HTTP calls

### Android Volley

``` js
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley

class MainActivity
{
    private fun apiHit() {
        val textView : AppCompatTextView = findViewById(R.id.textView)
        val url = "https://jsonplaceholder.typicode.com/users"
        val queue : RequestQueue = Volley.newRequestQueue(this)
        val request =  JsonObjectRequest(Request.Method.POST ,  url, null , {
            response: JSONObject? ->
            textView.text = response.toString()
        } , {
            error: VolleyError? ->
            textView.text =  error.toString()
        })
        queue.add(request)
    }
}
```

JsonObjectRequest, StringRequest are supported

This code produces Web Service HTTP calls

### Spring http operation (since version 1.0.3-funcrel)

``` js
import org.springframework.stereotype.Controller
import org.springframework.validation.BindingResult
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*

interface ApiInterfaces {
@Controller
class VisitController(val visits: VisitRepository, val pets: PetRepository) {
    // Spring MVC calls method loadPetWithVisit(...) before initNewVisitForm is called
    @GetMapping("/owners/*/pets/{petId}/visits/new")
    fun initNewVisitForm(@PathVariable("petId") petId: Int, model: Map<String, Any>): String
            = "pets/createOrUpdateVisitForm"
}
```

This code produces Web Service HTTP operations. Supported annotations
are:

-   @GetMapping
-   @PostMapping
-   @PutMapping
-   @DeleteMapping

### Spring beans (since version 1.0.3-funcrel)

The following code:

``` js
import pkg.queue.StandaloneBxCleanup
import org.springframework.context.annotation.AnnotationConfigApplicationContext
        
fun main(args: Array<String>) {
    val standaloneBxCleanup = app.getBean(StandaloneBxCleanup::class.java)
    standaloneBxCleanup.cleanupBxData()
}
```

``` js
package pkg.queue
        
@Component
class StandaloneBxCleanup(@Autowired val parseProcApiFactory: ProcApiFactory,
                          @Autowired val bxCleanerFactory: BxCleanerFactory,
                          @Autowired val cleanupExecutorService: ExecutorService) {
    fun cleanupBxData() {
    }
}
```

will generate the following links when analyzed:

![](../images/668336204.png)

The following code:

``` js
import pkg.queue.BillQueueProcessor
import org.springframework.context.annotation.AnnotationConfigApplicationContext
        
fun main(args: Array<String>) {
    val billQueueProcessor= app.getBean(BillQueueProcessor::class.java)
    Thread(billQueueProcessor).start()
}
```

``` js
package pkg.queue
        
@Component
class BillQueueProcessor(@Autowired val parseProcApiFactory: ProcApiFactory,
                          @Autowired val bxCleanerFactory: BxCleanerFactory,
                          @Autowired val cleanupExecutorService: ExecutorService) {
    override fun run() {
    }
}
```

will generate the following links when analyzed:

![](../images/668336203.png)

### Spring JDBC (since version 1.0.3-funcrel)

Only links to database procedures are supported. The following code:

``` js
package pkg.db.proc

import org.springframework.jdbc.`object`.StoredProcedure

class BxCleanupBillList(jdbcTemplate: JdbcTemplate) :
        StoredProcedure(jdbcTemplate, "best.pkg_bb_bill_load.bbl_get_bx_cleanup_bills") {
        
    fun getBxCleanupBills(): List<Long> {
        val outputParams = execute()
        return outputParams[cv1Param] as List<Long>
    }
}
```

will generate the following links when analyzed:

![](../images/668336202.png)

Same for following code where "REPLACE_FORBEARANCE" is a procedure.

``` java
interface ForbearanceRepository : CrudRepository<Client, String> {

    @Query("CALL FORBEARANCE_P.REPLACE_FORBEARANCE(:clientId, :eventId, :category, :type, :subtype, :note, :reasons, :accountNumber, :productSource, :expectedEndDate, :monitoringPeriodStartDate, :forbearanceChangeRequestAt, :source)")
    fun replaceForbearance(clientId: String,
                           eventId: String,
                           category: String,
                           type: String,
                           subtype: String?,
                           note: String?,
                           reasons: String?,
                           accountNumber: String?,
                           productSource: String?,
                           expectedEndDate: LocalDate?,
                           monitoringPeriodStartDate: LocalDate?,
                           forbearanceChangeRequestAt: LocalDate?,
                           source: String)
}
```

A link is created to the "FORBEARANCE_CV" table for code:

``` java
interface ForbearanceRepository : CrudRepository<Client, String> {

    @Query("select * from FORBEARANCE_CV f where f.CLIENT_ID = :clientId", rowMapperClass = ForbearanceAdapter.ForbearanceMapper::class)
    fun replaceForbearance(clientId: String,
                           eventId: String,
                           category: String,
                           type: String,
                           subtype: String?,
                           note: String?,
                           reasons: String?,
                           accountNumber: String?,
                           productSource: String?,
                           expectedEndDate: LocalDate?,
                           monitoringPeriodStartDate: LocalDate?,
                           forbearanceChangeRequestAt: LocalDate?,
                           source: String)
}
```

A link to the "ENROLL_CORPORATE_CLIENT" procedure is created for code:

``` java
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

....
            namedParameterJdbcTemplate.update("""
                    CALL ENROLLMENT_P.ENROLL_CORPORATE_CLIENT(
                        :enrollmentDraftId
                    )""".trimIndent(),...)
 
```

## What results can you expect?

### Example application

![](../images/668336201.png)

Kotlin code which calls java code:

![](../images/668336200.png)

Properties, getters, setters

![Alt text](../images/668336199.png)

``` java
class class1 {
    val counter = 0
        get() = field
        set(value) {
            if (value >= 0)
                field = value
        }
}
```

### Objects

| Icon | Description |
|---|---|
| ![](../images/668336198.png) | Kotlin anonymous class |
| ![](../images/668336197.png) | Kotlin class / Kotlin generic class / Kotlin JVM class / Kotlin JVM generic class |
| ![](../images/668336196.png) | Kotlin function / Kotlin JVM method |
| ![](../images/668336195.png) | Kotlin generic interface / Kotlin interface |
| ![](../images/668336194.png) | Kotlin instantiated class |
| ![](../images/668336193.png) | Kotlin lambda |
| ![](../images/668336192.png) | Kotlin main |
| ![](../images/668336191.png) | Kotlin method |
| ![](../images/668336190.png) | Kotlin object |
| ![](../images/668336189.png) | Kotlin property |
| ![](../images/668336188.png) | Kotlin getter |
| ![](../images/668336187.png) | Kotlin setter |
| ![](../images/668336186.png) | Kotlin source code  |
| ![](../images/668336185.png) | Kotlin Get HttpRequest Service<br> <br>Kotlin Web service Get Operation |
| ![](../images/668336184.png) | Kotlin Post HttpRequest Service<br> <br>Kotlin Web service Post Operation |
| ![](../images/668336183.png) | Kotlin Delete HttpRequest Service<br> <br>Kotlin Web service Delete Operation |
| ![](../images/668336182.png) | Kotlin Put HttpRequest Service<br> <br>Kotlin Web service Put Operation |

### Structural rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 1.1.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.7-funcrel) |
| 1.1.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.6-funcrel) |
| 1.1.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.5-funcrel) |
| 1.1.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.4-funcrel) |
| 1.1.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.3-funcrel) |
| 1.1.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.2-funcrel) |
| 1.1.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.1-funcrel) |
| 1.1.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=\|\|1.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_kotlin&ref=%7C%7C1.1.0-funcrel) |

You can also find a global list
here: [https://technologies.castsoftware.com/rules?sec=t_1030000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1030000&ref=%7C%7C).

## Known Limitations

- Links from Java methods to Kotlin methods are not supported.
