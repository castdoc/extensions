---
title: "PeopleSoft - 5.2"
linkTitle: "5.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.peoplesoft

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for PeopleSoft.

## Supported Versions of PeopleSoft

The qualification of the PeopleSoft application does not depend on the
functional release of PeopleSoft (8.4, 9.0, 9.1, 9.2) nor on the
functional module (CRM, HRMS, FSCM, Portal etc.). Indeed it depends
only on the versions of PeopleTools used to develop the source
code.  
This extension provides support for the following PeopleTools
versions:

| PeopleTools version | Supported  |
|---|:-:|
| 8.45 | :white_check_mark: |
| 8.46 | :white_check_mark: |
| 8.47 | :white_check_mark: |
| 8.48 | :white_check_mark: |
| 8.49 | :white_check_mark: |
| 8.50 | :white_check_mark: |
| 8.51 | :white_check_mark: |
| 8.52 | :white_check_mark: |
| 8.53 | :white_check_mark: |
| 8.54 | :white_check_mark: |
| 8.55 | :white_check_mark: |
| 8.56 | :white_check_mark: |
| 8.57 | likely\* |
| 8.58 | likely\* |

- PeopleTools versions above 8.48 that are listed in the table above are supported, but new functionalities or syntax introduced in these versions are not supported.
- likely\*: the PeopleTools repository tables (mostly PSxxx) have been very stable for many years, therefore the extractor will likely extract the objects and links as expected, and the rest of analysis will run fine. As soon as it is confirmed, a green tick will be added instead.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Supported DBMS servers for PeopleSoft applications

The extension supports PeopleSoft applications installed on the
following DBMS:

| DBMS | Supported  |
|---|:-:|
| Oracle Server | :white_check_mark: |
| DB2 UDB       | :white_check_mark: |

## Prerequisites

| Item | Required? | Description |
|---|:-:|---|
| AIP Core | :white_check_mark: | An installation of any compatible release of AIP Core (see table above). If you are using AIP Console to manage the analysis, you must use AIP Core ≥ 8.3.39. |
| AIP Console | :white_check_mark: | If you would like to use AIP Console to manage the analysis, please ensure that you are using v. ≥ 1.27 |
| Legacy scan mode | :white_check_mark: | Fast scan mode is NOT supported - therefore ensure you are using legacy scan mode in AIP Console. |
| Vanilla repository availability | :white_check_mark: |  Ensure that the Vanilla repository is online and available before starting.  |
| On PeopleSoft server hosting the Oracle/DB2 repository (for Vanilla and Project) | :white_check_mark: |  The server can be any OS, however Unix/Linux OS may require some .ksh script if you want to run the extractor on the server machine itself. <br><br>The version of the Oracle/DB2 server must match a version supported by the JDBC driver embedded in the [CAST Database Extractor](https://extend.castsoftware.com/:white_check_mark:/extension?id=com.castsoftware.aip.extractor.sqldatabase&version=latest) (used to extract the SQL data). This is different and wider than the supported versions for a participating database. Note that versions supported by AIP Core for a participating databases can be found in the official CAST AIP documentation (e.g. [SQL - Covered technologies](../../../../sql/technos)). |
| On Workstation used for extraction (where the CAST Database Extractor / CAST Delivery Manager Tool is deployed) | :white_check_mark: | <ul><li>Windows or Linux (Unix/Linux OS may require some additional `.ksh` scripts).</li><li>JRE 1.7 (ideally 64 bit version to allow for a JVM large heap size).</li><li>Minimum 1.2 GB RAM memory free, more if available.</li><li>DMT/extractor location: <ul><li>Ideally, install the DMT/extractor on the RDBMS server (Oracle or DB2) itself.</li><li>Alternatively on a machine with:<ul><li>very good bandwidth (1 GB/s) to the Oracle / DB2 server (some remote extractions fail due to poor VPN bandwidth)</li><li>connectivity to the Oracle / DB2 server (required ports open)</li></ul></ul><li>4 GB free disk space (for temporary files before compression)</li><li>250 MB free disk space (to store the two extraction archives) included in above requirement.</li><li>When using the standalone CAST Database Extractor to extract the SQL data (instead of using the extractors built into the CAST Delivery Manager Tool) you should download the extractor here: https://extend.castsoftware.com/:white_check_mark:/extension?id=com.castsoftware.aip.extractor.sqldatabase&version=latest</li></ul> |
| On AIP Node used for analysis | :white_check_mark: | <ul><li>Windows (uses AIP).</li><li>2 GB RAM memory free (for analysis/snapshot)</li><li>Connectivity to the CAST Storage Service/PostgreSQL instance server hosting the application schemas (port open)</li><li>5-10 GB free disk space (to store the source code, once expanded by the Deploy phase)</li><li>Disable anti-virus software. Check that no anti-virus is running on the workstation or disable real-time scan for the work folders (delivery & deploy folders + temp folders). Failure to do so will multiply the injection runtime.  |
| Tables required to grant access to the PeopleSoft repository | :white_check_mark: | The following database tables are accessed during the PeopleSoft extraction process: `PEOPLESOFT_TABLES.txt`. |

## FAQs

Please see [PeopleSoft & Siebel FAQ](../../faq).

## Architecture and analysis principles

This section discusses the way CAST analyzes PeopleSoft projects and
describes the specific metrics that form part of the standard CAST
Quality Model.

The image below describes the entire process of computing quality
assessments of PeopleSoft projects, from the extraction of the
PeopleSoft source code and meta-data to the integration of the quality
results into the CAST AIP dashboards.

![](../images/557023927.png)

### Discrimination process

The purpose of the discrimination process is to identify differences
between the standard installation (also called Vanilla) and the Project
environment.

![](../images/557023928.png)

In order to identify these modifications, we require the source code
delivery for the Project and the Vanilla. To integrate them, we create
two Analysis schemas:

-   Project Analysis schema: contains the results of the technical
    analysis of the current version of the PeopleSoft project
-   Vanilla Analysis schema: contains the results of the technical
    analysis of the baseline of a PeopleSoft implementation. This
    Analysis Service should not be undergo snapshot generation each time
    and for each project. It should instead undergo snapshot generation
    each time the Vanilla changes (patch, upgrade etc.). This Analysis
    schema can be used by multiple projects.

## PeopleSoft technology coverage

The objects listed below are all the object types that a PeopleSoft
analysis will deliver in the Analysis schema. With these objects, CAST
is capable of supporting a set of quality rules to provide a customer
with a good overview of the current technical status of a PeopleSoft
application. It is not the intention of Cast to capture all details and
syntaxes; a coupling between requirement and detection is made.

![](../images/557023929.jpg)

### PeopleSoft artifacts and their relations

The PeopleSoft extension is capable of detecting a large number of
objects, properties and links. All of these artifacts are mentioned in
the image above, but in essence, all PeopleSoft objects are supported:

-   User
-   Role
-   Permission List
-   Component Interface
-   Menu – We distinguish different types:
    -   Menu
    -   Popup
-   Component
-   Message Channel
-   Message Node
-   Message
-   Page – We distinguish different types:
    -   Page
    -   Sub
    -   Secondary
-   Application Engine – We distinguish different types:
    -   Standard
    -   Upgrade only
    -   Import only
    -   Daemon only
-   Transform only
-   Style Sheet
-   Record – We distinguish different types:
    -   Table
    -   View
    -   Derived
    -   Sub
    -   Dynamic
    -   Query
    -   Temp
-   SQL – We distinguish different types:
    -   View
    -   SQL
    -   AE step
-   DBField
-   Application Package
-   Peoplecode – We distinguish different types:
    -   Event
    -   Function
    -   Class
    -   Method
-   Field format
-   Mobile Page
-   HTML
-   Query
-   File Layout
-   Business Process
-   Activity
-   Process group
-   Job
-   Process 

## Download, installation, configuration and operating instructions

See [Onboarding with CAST Imaging Console](console).

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
|  ![](../images/557023708.png)  | PeopleSoft Activity |
|  ![](../images/557023709.png)  | PeopleSoft ActivityEmail |
|  ![](../images/557023710.png)  | PeopleSoft ActivityEvent |
|  ![](../images/557023711.png)  | PeopleSoft ActivityReport |
|  ![](../images/557023712.png)  | PeopleSoft ActivitySchedule |
|  ![](../images/557023713.png)  | PeopleSoft ActivityStep |
|  ![](../images/557023714.png)  | PeopleSoft ActivityWorklist |
|  ![](../images/557023715.png)  | PeopleSoft Application Engine Action Call |
|  ![](../images/557023716.png)  | PeopleSoft Application Engine Action Message |
|  ![](../images/557023717.png)  | PeopleSoft Application Engine Action Peoplecode |
|  ![](../images/557023718.png)  | PeopleSoft Application Engine Action SQL |
|  ![](../images/557023719.png)  | PeopleSoft Application Engine Action XSLT |
|  ![](../images/557023720.png)  | PeopleSoft Application Engine Action Deamon Only |
|  ![](../images/557023721.png)  | PeopleSoft Application Engine Action Import Only |
|  ![](../images/557023722.png)  | PeopleSoft Application Engine Action Section |
|  ![](../images/557023723.png)  | PeopleSoft Application Engine Action Section Market |
|  ![](../images/557023724.png)  | PeopleSoft Application Engine Action Standard |
|  ![](../images/557023725.png)  | PeopleSoft Application Engine Action Step |
|  ![](../images/557023726.png)  | PeopleSoft Application Engine Action Transform Only |
|  ![](../images/557023727.png)  | PeopleSoft Application Engine Action Upgrade Only |
|  ![](../images/557023728.png)  | PeopleSoft Application Class |
|  ![](../images/557023729.png)  | PeopleSoft Application Class Execute |
|  ![](../images/557023730.png)  | PeopleSoft Application Method |
|  ![](../images/557023731.png)  | PeopleSoft Application Package |
|  ![](../images/557023732.png)  | PeopleSoft Permission Component Interface |
|  ![](../images/557023733.png)  | PeopleSoft Permission CI Method |
|  ![](../images/557023734.png)  | PeopleSoft Authentication CnhlMon |
|  ![](../images/557023735.png)  | PeopleSoft Authentication Cube<br><br> PeopleSoft Authentication Item<br><br> PeopleSoft Authentication Mobile Page<br><br> PeopleSoft Authentication Option<br><br> PeopleSoft Authentication Process<br><br> PeopleSoft Authentication Queue Monitor<br><br> PeopleSoft Authentication WebService |
|  ![](../images/557023736.png)  | PeopleSoft BusinessProcess |
|  ![](../images/557023737.png)  | PeopleSoft BusinessProcessItem |
|  ![](../images/557023738.png)  | PeopleSoft CompInterface Event |
|  ![](../images/557023739.png)  | PeopleSoft CI Property Level 0  Event<br><br> PeopleSoft CI Property Level 1  Event<br><br> PeopleSoft CI Property Level 2  Event |
|  ![](../images/557023740.png)  | PeopleSoft Component |
|  ![](../images/557023741.png)  | PeopleSoft Component Event |
|  ![](../images/557023742.png)  | PeopleSoft Component Interface |
|  ![](../images/557023743.png)  | PeopleSoft CI Collection Level 1<br><br> PeopleSoft CI Collection Level 2<br><br> PeopleSoft CI Collection Level 3 |
|  ![](../images/557023744.png)  | PeopleSoft CI CreateKey |
|  ![](../images/557023745.png)  | PeopleSoft CI FindKey |
|  ![](../images/557023746.png)  | PeopleSoft CI GetKey |
|  ![](../images/557023747.png)  | PeopleSoft CI Property Level 0 |
|  ![](../images/557023748.png)  | PeopleSoft CI Property Level 1<br><br> PeopleSoft CI Property Level 2<br><br> PeopleSoft CI Property Level 3 |
|  ![](../images/557023749.png)  | PeopleSoft CI User Property Level 0 |
|  ![](../images/557023750.png)  | PeopleSoft CI User Property Level 1<br><br> PeopleSoft CI User Property Level 2<br><br> PeopleSoft CI User Property Level 3 |
|  ![](../images/557023751.png)  | PeopleSoft Component Item |
|  ![](../images/557023752.png)  | PeopleSoft Component Record |
|  ![](../images/557023753.png)  | PeopleSoft ComponentRecord Event |
|  ![](../images/557023754.png)  | PeopleSoft ComponentRecord Field |
|  ![](../images/557023755.png)  | PeopleSoft ComponentRecordField Event |
|  ![](../images/557023756.png)  | PeopleSoft DBField |
|  ![](../images/557023757.png)  | PeopleSoft Environment |
|  ![](../images/557023758.png)  | PeopleSoft Field Format |
|  ![](../images/557023759.png)  | PeopleSoft Field Label Item |
|  ![](../images/557023760.png)  | PeopleSoft Field XLAT Item |
|  ![](../images/557023761.png)  | PeopleSoft File Layout |
|  ![](../images/557023762.png)  | PeopleSoft HTML |
|  ![](../images/557023763.png)  | PeopleSoft Menu |
|  ![](../images/557023764.png)  | PeopleSoft Menu Bar |
|  ![](../images/557023765.png)  | PeopleSoft Menu Event |
| ![](../images/557023766.png)  | PeopleSoft Menu Item |
|  ![](../images/557023767.png)  | PeopleSoft Menu Popup |
|  ![](../images/557023768.png)  | PeopleSoft Message |
|  ![](../images/557023769.png)  | PeopleSoft Message Catalog |
|  ![](../images/557023770.png)  | PeopleSoft Message Event |
|  ![](../images/557023771.png)  | PeopleSoft Message Node |
|  ![](../images/557023772.png)  | PeopleSoft Message Sent |
|  ![](../images/557023773.png)  | PeopleSoft Mobile Page |
|  ![](../images/557023774.png)  | PeopleSoft Message Channel |
|  ![](../images/557023775.png)  | PeopleSoft Message Subscription |
|  ![](../images/557023776.png)  | PeopleSoft Package |
|  ![](../images/557023777.png)  | PeopleSoft Page |
|  ![](../images/557023778.png)  | PeopleSoft SubPage |
|  ![](../images/557023779.png)  | PeopleSoft SecondaryPage |
|  ![](../images/557023780.png)  | PeopleSoft Page Event |
|  ![](../images/557023781.png)  | PeopleSoft Page Field0 |
|  ![](../images/557023782.png)  | PeopleSoft Page Field1 |
|  ![](../images/557023783.png)  | PeopleSoft Page Field2 |
|  ![](../images/557023784.png)  | PeopleSoft Page Field3 |
|  ![](../images/557023785.png)  | PeopleSoft Page Field Level1<br><br> PeopleSoft Page Field Level2<br><br> PeopleSoft Page Field Level3 |
|  ![](../images/557023786.png)  | PeopleSoft PeopleCodeFunction |
|  ![](../images/557023787.png)  | PeopleSoft Permission List |
|  ![](../images/557023788.png)  | PeopleSoft Portal Content Reference |
|  ![](../images/557023789.png)  | PeopleSoft Portal Folder |
|  ![](../images/557023790.png)  | PeopleSoft Portal Registry |
|  ![](../images/557023791.png)  | PeopleSoft Process |
|  ![](../images/557023792.png)  | PeopleSoft ProcessGroup |
|  ![](../images/557023793.png)  | PeopleSoft ProcessJob<br><br> PeopleSoft Query |
|  ![](../images/557023794.png)  | PeopleSoft RecordDerived |
|  ![](../images/557023795.png)  | PeopleSoft RecordDynamic |
|  ![](../images/557023796.png)  | PeopleSoft RecordFieldDB |
|  ![](../images/557023797.png)  | PeopleSoft RecordFieldEvent |
|  ![](../images/557023798.png)  | PeopleSoft RecordField |
|  ![](../images/557023799.png)  | PeopleSoft RecordIndex |
|  ![](../images/557023800.png)  | PeopleSoft RecordQuery |
|  ![](../images/557023801.png)  | PeopleSoft RecordSub |
|  ![](../images/557023802.png)  | PeopleSoft RecordTable |
|  ![](../images/557023803.png)  | PeopleSoft RecordTemp |
|  ![](../images/557023804.png)  | PeopleSoft RecordView |
|  ![](../images/557023805.png)  | PeopleSoft Role |
|  ![](../images/557023806.png)  | PeopleSoft SQL |
|  ![](../images/557023807.png)  | PeopleSoft StyleSheet |
|  ![](../images/557023808.png)  | PeopleSoft SubPackage |
|  ![](../images/557023809.png)  | PeopleSoft Subscription Event |
|  ![](../images/557023810.png)  | PeopleSoft URL |
|  ![](../images/557023811.png)  | PeopleSoft User |

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 5.2.1 | [https://technologies.castsoftware.com/rules?sec=srs_peoplesoft&ref=\|\|5.2.1](https://technologies.castsoftware.com/rules?sec=srs_peoplesoft&ref=%7C%7C5.2.1) |
| 5.2.0 | [https://technologies.castsoftware.com/rules?sec=srs_peoplesoft&ref=\|\|5.2.0](https://technologies.castsoftware.com/rules?sec=srs_peoplesoft&ref=%7C%7C5.2.0) |

You can also find a global list here:

[https://technologies.castsoftware.com/rules?sec=t_1600000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1600000&ref=%7C%7C)

## Performance issues

A Peoplesoft analysis almost always takes longer then other technologies
supported by CAST and is dependent on the fact that both the Vanilla,
Project and the delta between them should be extracted and analyzed.
Some tips listed below can help to improve total extraction and analysis
time:

-   Ensure that the infrastructure on which the extraction and analysis
    takes place is of high quality using fast disks such as SSD or SCSI
    with ample free RAM.
-   For the extraction phase:
    -   consider skipping the database extraction if you do not need to
        see the result of quality rules run on these extractions -
        see PeopleSoft specific - Why do we need to extract also the
        Oracle or DB2 database (PL/SQL structure)? in [Siebel and
        PeopleSoft -
        FAQs](https://doc.castsoftware.com/display/TECHNOS/Siebel+and+PeopleSoft+-+FAQs).
    -   ensure that the extraction is actioned on the server hosting the
        database itself - if a remote server is used, then network
        performance will impact the extraction time.
    -   the Peoplesoft schema (on Oracle or DB2) stats should be up to
        date:
        -   If not up-to-date for Oracle, update it with the
            following SQL statement (requires some privileges : GRANT
            execute ON dbms_stats TO XXX);

``` java
Execute dbms_stats.gather_schema_stats(ownname => 'MY_PSOFT_SCHEMA', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size auto', cascade => true);
```

-   -   -   If not up-to-date for DB2, check if DB2 automatic
            runstats and DB2 real time statistics are enabled.
            If not, ask the DBA to run the following or similar:

``` java
 db2 reorgchk update statistics on table all
```

## Known issues/limitations

The following section lists all known issues/limitations in this release
of the extension.

### Error when displaying source code in CAST dashboards

Most PeopleSoft objects don't have associated source code as they
correspond to a configuration. In the CAST dashboards, an error will
therefore be displayed when trying to display the object source code
(example error message from the legacy CAST Engineering Dashboard):

 ![](../images/557023815.png)

### No bookmarks are displayed for violations in source code in CAST dashboards

When using the CAST dashboards, no "bookmarks" will be displayed to
indicate where violations occur in source code. This is a limitation of
the extension.

### Warnings in the analysis log

The following warnings may appear in the analysis log:

-   end of string ''' not found
-   end of string '\['\]' not found
-   File skipped

This is a limitation of the extension: when the end of a string
pattern is not found inside an object's source code, an error is raised
as the syntax is not understood. In the context of PeopleSoft, SQL
objects can be created and configured to contain the sub-part of an SQL
statement that can be re-used in other objects. As such these SQL
statements may be incomplete which will cause the warnings described
above to appear. To avoid these type of warnings, this partial SQL
should not be analyzed individually, but rather in the context of where
it has been included.
