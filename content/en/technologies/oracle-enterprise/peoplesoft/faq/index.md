---
title: "PeopleSoft & Siebel FAQ"
linkTitle: "PeopleSoft & Siebel FAQ"
type: "docs"
---

## Addition of other technos to a Siebel or PeopleSoft analysis

Is it possible to analyze other technologies along with a Siebel or PeopleSoft extraction? The technology extensions for Siebel / PeopleSoft do not consider other technologies. Since 2006, most analyses (99%) have been performed with a single technology. With the rise of web technologies, especially to build front ends, the question arises more and more. The answer is Yes, you can. It will provide value for quality-oriented analysis, thanks to the HTML5 / Javascript quality model.

However, the deliver mode is an open question: the HTML pages are not part of the scope extracted by current Siebel/Peoplesoft extractors, so they have to be delivered as standalone files.

A specific DMT package, of type File on file system, should be created and custom Analysis Unit created to analyze these .html file. See HTML5 and JavaScript Packaging, delivering and analyzing your source code

## Vanilla and Project extraction
### Is it possible to perform an offline extraction?
Yes, this is available since Siebel extension v5.1.0 and since PeopleSoft extension v5.1.0.

### PeopleSoft specific - Why do we need to extract also the Oracle or DB2 database (PL/SQL structure)?
The Oracle or DB2 UDB schema needs to be extracted to enable a series of 7 quality rules on Dead Code (PeopleSoft: Avoid unused Record XXX).

With the FSCM module, the volume is quite large: the extraction will pass, but the analysis job will be a risk.

This step can be skipped for performance reasons and a "degraded mode" Dashboard can be delivered with the remaining Quality Rules. The only consequence in analysis results is to jeopardize 7 Quality Rules relying on these object types (and links to them). All the rest will be a regular analysis. In order to present a coherent Dashboard, the following 7 QRs must be disabled:

- 1601500	PeopleSoft: Avoid unused Record Table
- 1601502	PeopleSoft: Avoid unused Record View
- 1601504	PeopleSoft: Avoid unused Record Derived
- 1601506	PeopleSoft: Avoid unused Record Sub
- 1601508	PeopleSoft: Avoid unused Record Dynamic
- 1601510	PeopleSoft: Avoid unused Record Query
- 1601512	PeopleSoft: Avoid unused Record Temp

>Note: a light extraction for Oracle was provided with extension v4.0.4 (outside of DMT). This extraction is not yet possible within DMT. A workaround, unpackaged, is possible for Oracle:
- use the 4.0.4 .config file within the Standalone DB extractor 2.0  <.config of 4.0.4 adapted for 7.3+
- run Standalone DB extractor.
- configure to use alternate .config
- extract the Oracle schema (it will extract only Tables and Views)
- generate a .castextraction
- deliver this archive through the DMT entry "Oracle / Reuse existing CAST extractor output" : 
- extraction and analysis will be streamlined, and results of the 7 QRs above, preserved.

## Vanilla and Project repositories - discrimination
### Why do we need to extract and analyze both the Vanilla and the Project repositories?
This is required for two purposes:

1. We need the standard objects extracted and then analyzed in the Analysis Service schema, in order to get a full picture of the dependencies. Custom objects may have dependencies to standard objects.
1. This is the only reliable means to distinguish between standard, modified, added and deleted objects. This process is called "discrimination".
### This is not the case for an ABAP analysis, why?
With SAP development rules, the developer can just add Z* or Y* objects. He is not supposed to modify SAP core components. So the extrator can rely safely on the program naming convention to target only the added components (.Z* and Y* programs) and modified components (a list of User Exits).

### How does the analyzer decide if an object has been Added, Modified or Deleted?
This is the aim of the Discrimination job (embedded into the analyzer). It compares the content of the Project and the Vanilla Analysis Service schemas. For each high-level object (Applet, Business Component, Business Service, ...):

- If the object is unchanged (inspecting all the object structure's property values) it is a flagged as Standard.
- If it exists in Vanilla, but has an added, modified or deleted property or a modified, added or deleted child object, it is flagged as Modified.
- If it does not exist in Vanilla, it is flagged as Added.
### Does discrimination filter the violations on Modified module ?
Since we have all information in both Analysis Service schemas (V and P), one could expect that the reported violations are due to custom modification on the code, and violations have therefore been introduced by the developer.

Violations due to native issues may be ignored (customer) or reported to Oracle. Violations due to customer customization actions could be part of an Action Plan for remediation and risk reduction.

All information required to decide this root cause are available in the Analysis Service schemas.

This feature (providing a filtered report for Modified module, or reporting the root cause of violation) is now available since Siebel extension v5.1.0, and PeopleSoft extension v5.1.0: the violations originating from Vanilla are now excluded and not considered in the Project results: if the violations are detected only in the Vanilla results, they will not be reported in the Project results.

### Why do we need to definition the various User Defined Modules?
For Siebel and PeopleSoft analysis, the below pair of modules is mandatory. This is the design of the package. The REPOSITORY modules contain are the raw results of the discrimination job:

- REPOSITORY – ADDED is the sum of Added objects, meaning objects found in Project but not present in the Vanilla.
- REPOSITORY – MODIFIED is the sum of Modified objects, meaning objects present in the Vanilla (V) but with different properties or children to the corresponding object in Project (P): same GUID in V and P, but different checksum.
For Siebel, an additional pair of modules is required for each Siebel application customized by the dev team. The <Application> modules are the result of an Impact Analysis, starting from the Siebel Application object (the name(s) you need to collect from the dev team using the SCDF Excel sheet): all linked objects starting from this customized application:

- <APPNAME> ADDED : is the sum of linked objects, flagged as Added by the Discrimination process.
- <APPNAME> MODIFED: is the sum of linked objects, flagged as Added by the Discrimination process.

In conclusion, the standard objects are analyzed and present in the Analysis Service schema (P_LOCAL), but they are not reported in the Dashboard, because they are not present in any User Defined Module. 

### Siebel specific - How to list the modified Applets (in case of discussion with the application team)?
This question could be raised in case of an abnormal count result, that one could see especially in invasive customization ratio.

An abnomally high number of Applets (or other object types) could come from an invalid Vanilla, i.e. a Vanilla repository that does not have the same level of patches, nor the same history of migration as the Project repository. As a consequence the discrimination will tag objects as modified, whereas the modification is actually due to a patch and not to development. In order to check this, the development team may ask for the list of modified (or added) Applest (or other object types).

Solution: use the following SQL query on the Analsyis Service schema and submit the list to the application team review: they will tell you quickly if these are their Modified / Added objects. If there are many objects that are supposed to be standard (unmodified) it means the Vanilla is out of sync with the project (missing patches, or different migration path).

List modfied applets
```
set search_path = siebel_local;
 
select T2.keynam
from objinf T1 join keys T2 on (T2.idkey = T1.idobj)
where inftyp=1520010 and infsubtyp=10                                    -- property holding the object status, as set by discrimination
and objtyp in (select idtyp from typcat where idcatparent = 1520018)     -- several types of applet, so using the parent category to have them all in one go
and infval > 2                                                        -- Modified of all kind
order by 1
-- How to decode the status values :
0 Standard
1 Unknown
2 Added
3 Modified
4,5,6 Modified by sub-objects
```
### PeopleSoft specific - How to list the modified or added Application Engines (in case of discussion with the application team)?
This question could be raised in case of an abnormal count result, that one could see especially in invasive customization ratio.

Solution : use the following SQL query on the local schema, from pgAdmin III and submit the list to application team review, they will tell you quickly if these are their Modified / Added objects.

Solution: use the following SQL query on the Analsyis Service schema and submit the list to the application team review: they will tell you quickly if these are their Modified / Added objects. If there are many objects that are supposed to be standard (unmodified) it means the Vanilla is out of sync with the project (missing patches, or different migration path).

List modfied PeopleSoft Application Engines
```
set search_path = peoplesoft_local;
 
select T2.keynam
from objinf T1 join keys T2 on (T2.idkey = T1.idobj)
where inftyp=1600010 and infsubtyp=3                                   --  <property name="DiscriminationStatus" type="integer" rid="103">    <attribute name="INF_TYPE" intValue="1600010"/> <attribute name="INF_SUB_TYPE" intValue="3"/>
and objtyp in (select idtyp from typcat where idcatparent = 1600024)   -- <category name="PSoft_ApplicationEngines" rid="24">
and infval > 2                                                          -- Modified of all kind
order by 1
 
values used for infval:
-----------------------
0 Standard
1 Unknown
2 Added
3 Modified
4,5,6 Modified by sub-objects
```
### Siebel specific - How to explain the gap between REPOSITORY and application modules (at the inventory level)?
The <APPNAME> ADDED module is a subset of  the REPOSITORY – ADDED module by definition. The gap consists of unused objects, which are usually objects developed and then replaced by others and no longer used, or potentially made up of linked objects but the link is not detected by the analyzer. This may occur on the newest versions of Siebel.

### PeopleSoft specific - Team wants the Dashboard Results (modules) Project wise, is it possible?
The package defines two modules, ADDED and MODIFIED. This is the default and only possible split for now.

PeopleSoft projects are workspaces in the PeopleTools IDE that can be used for delivering modifications. We can compare that to a transport order in SAP. Potentially it could be possible to define a module to match the scope of projects. This requires a change both in the CAST PeopleSoft Extractor and in the CAST PeopleSoft metamodel.

## Analysis results consumption
### How to measure the customization ratio?
There are 2 series of 7 measures (Siebel) or 19 measures (PeopleSoft), located in the new (since extensions v5.0), custom Technical Criteria, named ERP customization.

- Invasive customization rate measures the customization on objects delivered by Oracle. The ratio is the number of modified objects compared to the total number of objects of the same type (total excluding added objects).
- Extensive customization rate measures the ratio of added objects on top of those delivered by Oracle. The ratio is the number of added objects and the total number of objects of the same type.
To collect these measures there are two options:

1. In the legacy CAST Engineering Dashboard, for each measure, look at the Computing Details tab, and note the measure value which reads at the right of the formula "grade=measure" (from 1% to 70% usually).
2. With the REST-API using a Measurement Service, the following URIs will retrieve all measures in one go:

URI for PeopleSoft:
```
AAD/results?quality-indicators=(1601313,1601316,1601312,1601307,1601317,1601315,1601314,1601308,1601318,1601302,1601304,1601305,1601303,1601300,1601306,1601301,1601310,1601311,1601309,1601213,1601216,1601212,1601207,1601217,1601215,1601214,1601208,1601218,1601202,1601204,1601205,1601203,1601200,1601206,1601201,1601210,1601211,1601209)&snapshots=-1
```
URI for Siebel:
```
AAD/results?quality-indicators=(<metric_ids to be completed)&snapshots=-1
```
### Why is the LoC metric so low ?
A Siebel analysis takes into account Applets, Business Components, Business Objects, Business Services, ... which are mostly structures, with properties and values. A PeopleSoft analysis takes into account Application Engines, Panel definitions, Record definitions,... which are mostly structures, with properties and values.

These structures are extracted into a CAST proprietary format, and imported through Universal Import. This part leads to no LoC count. Only Siebel eScript and VBScript hold procedural code. So only these scripts have a metric for LoC (Line of Code). By design, AIP does not count LoC for structures extracted as .uax files (a specific XML format).

As a consequence, LoC is not a fully representative measure of a Siebel project size. Most of the customization is done by pure configuration, not holding any LoC.

For PeopleSoft, the LoC contributors are methods, functions, events and SQL (metamodel types : PSoft_AppMethod, PSoft_PeopleCodeFunction, several types for event and SQL , see below exact list of types):

- events types : PSoft_RecordFieldEvent, PSoft_MenuEvent, PSoft_ActivityEvent, PSoft_MessageEvent, PSoft_SubscriptionEvent, PSoft_CompInterfaceEvent, PSoft_PageEvent, PSoft_ComponentEvent, PSoft_ComponentRecordEvent, PSoft_ComponentRecordFieldEvent.

- SQL types, inheriting from PSoft_SQLDefinitions category: PSoft_RecordView, PSoft_RecordDynamic, PSoft_SQL, PSoft_AEActionSQL

### How does LoC represent a risk ?
With Siebel 8.0.0 and above, the Vanilla is delivered with 0 LoC. So the LoC metric represents pure customization, which is the "last chance" means to adapt Siebel to the organization's needs. This way to adapt Siebel introduces a real technical debt for the customer. It is important to measure and monitor this debt, with the aim to keep it as low as possible.

You should regard coding as a last resort. Siebel Tools provides many ways to configure your Siebel application without coding, and these methods should be exhausted before you attempt to write your own code, for the following reasons:

- Using Siebel Tools is easier than writing code.
- More importantly, your code may not survive an upgrade. Customizations created directly in Siebel Tools are upgraded automatically when you upgrade your Siebel application, but code is not touched, and it may need to be reviewed following the automated part of the upgrade.
- Finally, declarative configuration through Siebel Tools results in better performance than implementing the same functionality through code.
### Is the Dashboard able to display the source code for objects in violation?
It depends on the scope of the rule (scope = total) and the type of object reported by the rule matters: Only objects with procedural code (having associated .src source code referenced by the .uax, and inserted by UA inside UI) will display in the Code Viewer. See the list of object types in the previous answer for LoC counting.

- For Siebel, these are mostly the case of Quality Rules starting with: Siebel eScript: xxxx  and SiebelScriptingReview: xxxx. On the other hand, objects imported from .uax files (inserted by pure UI) are structures / containers, and thus do not display in Code Viewer (this is by Universal Importer design : .uax source code is not stored in Analysis/Dashboard Service schemas).
- For PeopleSoft, you'll see the source code for quality rules on Events, Functions, Methods and SQL.
### Can I measure the productivity using standard Estimated Effort and Estimated Cost ?
Since Estimated Effort and Estimated Cost are based on an artifact's size (based on LoC) these features are not pertinent for Siebel nor PeopleSoft, as is.

A full study should be conducted to design what should be a Simple, Average, Complex and Very Complex Siebel Applet (resp. Application Engine for PeopleSoft), and the same with other non-procedural object types. Since there is no known industry standard for measuring the complexity on Siebel/PeopleSoft projects, the outcome of this study would be purely conventional and specific to CAST.

### What are the critical Quality Rules ?
- Siebel: there are 8 Quality Rules with the critical contribution flag by default.
- PeopleSoft : there are 5 Quality Rules with the critical contribution flag by default.
### What if I have a violation on a Modified object - who is responsible: Oracle or the developer?
This can be an endless debate and you must be prepared to this question. If the Project Manager and architect (SME) consider the violation can hurt the production, you must fix it yourself first. At the same time you should open a ticket with Oracle support to ask for a fix.

### Why do some rules show on ADDED modules only, some others on MODIFIED modules only and some on both?
 Some rules like naming convention make sense only for ADDED objects. Other rules make sense only for MODIFIED objects. Lastly, many rules apply on both kinds of objects.

 So, in all, rules apply on different scopes, depending on the need.

### Siebel specific - which module should be elected for remediation plan ?
Normally <APPNAME> ADDED is the priority, and then <APPNAME> MODIFED, because the development team has full control over Added objects, whereas violations on Modified objects may be due to inherited Vanilla violations. Since extension v5.1.0, we do filter violations that are also present in Vanilla.

On the other hand, since Siebel 8.0, you may notice a large gap (in object inventory) between an application's modules and the REPOSITORY's corresponding modules. In order to avoid missing objects (not linked), you could stick to the REPOSITORY – ADDED and then REPOSITORY – MODIFIED modules. Potentially you will have a few old and no longer used objects in these modules. Not a big deal. Ask the developer to skip them when working on the remediation plan.

That said, they are all in the same unique application , named "Project" and the Dashboard's Automated Action Plan works at the application level, so you will end up with duplicated rows in the export file. It is quite easy to eliminated the duplicate rows with Excel.