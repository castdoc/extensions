---
title: "Analysis configuration"
linkTitle: "Analysis configuration"
type: "docs"
weight: 5
---

AIP Console exposes the Technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Forms to display the available options:

![](414482622.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same as at Application level) that allow you to
        make fine-grained configuration changes.

You should check that settings are as required and that at least one
Analysis Unit exists for the specific technology.

![](414482626.jpg)

<table>
<tbody>
<tr>
<th>Dependencies</th>
<td><p>Dependencies are configured
at Application level for each
technology and are configured between individual
Analysis Units/groups of Analysis Units (dependencies between
technologies are not available as is the case in the CAST Management
Studio). You can find out more detailed information about how to ensure
dependencies are set up correctly, in Validate dependency
configuration.</p></td>
</tr>
<tr>
<th>Analysis Units</th>
<td><div class="content-wrapper">
<p>For Oracle Forms and Reports, there are no Analysis Unit
level settings available. You can however:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr>
<th><div class="content-wrapper">
<img src="414941210.jpg" />
</div></th>
<td><p>Analyze a specific Analysis Unit (and all
Analysis Units within the Execution Unit), specifically for testing
purposes.</p></td>
</tr>
<tr>
<th><div class="content-wrapper">
<img src="414941211.jpg" />
</div></th>
<td>Disable or enable the analysis of a specific
Analysis Unit, when the next Application level analysis/snapshot is run.
By default all Analysis Units are always set to
enable. When an Analysis Unit is set to disable, this
is usually a temporary action to prevent an Analysis Unit from being
analyzed.</td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
</tbody>
</table>