---
title: "Analysis results"
linkTitle: "Analysis results"
type: "docs"
weight: 6
---

## Oracle Forms objects

| Icon |          Meta Model Description            |         Meta Model Name         |
|------|--------------------------------------------|---------------------------------|
| ![Alt text](668336384.png)     |            Oracle Forms Window             |        CAST_Forms_Window        |
| ![Alt text](668336385.png)     |          Oracle Forms Window Tab           |      CAST_Forms_Window_Tab      |
| ![Alt text](668336386.png)     |             Oracle Forms Alert             |        CAST_Forms_Alert         |
| ![Alt text](668336387.png)     |           Oracle Forms Alert Tab           |      CAST_Forms_Alert_Tab       |
| ![Alt text](668336388.png)     |            Oracle Forms Canvas             |        CAST_Forms_Canvas        |
| ![Alt text](668336389.png)     |          Oracle Forms Canvas Tab           |      CAST_Forms_Canvas_Tab      |
| ![Alt text](668336390.png)     |           Oracle Forms Datablock           |      CAST_Forms_DataBlock       |
| ![Alt text](668336391.png)     |      Oracle Forms Data Block Relation      |  CAST_Forms_DataBlock_Relation  |
| ![Alt text](668336392.png)     |     Oracle Forms Library Tab DataBlock     |    CAST_Forms_DataBlock_Tab     |
| ![Alt text](668336393.png)     |        Oracle Forms DataBlock Item         |    CAST_Forms_DataBlockItem     |
| ![Alt text](668336394.png)     |  Oracle Forms Library Tab DataBlock Item   |  CAST_Forms_DataBlockItem_Tab   |
| ![Alt text](668336395.png)     |           Oracle Forms Function            |       CAST_Forms_Function       |
| ![Alt text](668336396.png)    |     Oracle Forms Library Tab Function      |     CAST_Forms_Function_Tab     |
| ![Alt text](668336397.png)     |              Oracle Forms LOV              |         CAST_Forms_LOV          |
| ![Alt text](668336398.png)     |        Oracle Forms Library Tab LOV        |       CAST_Forms_LOV_Tab        |
| ![Alt text](668336399.png)     |             Oracle Forms Menu              |         CAST_Forms_Menu         |
| ![Alt text](668336400.png)     |       Oracle Forms Library Tab Menu        |       CAST_Forms_Menu_Tab       |
| ![Alt text](668336401.png)     |           Oracle Forms Menu Item           |       CAST_Forms_MenuItem       |
| ![Alt text](668336402.png)     |     Oracle Forms Library Tab Menu Item     |     CAST_Forms_MenuItem_Tab     |
| ![Alt text](668336403.png)     |    Oracle Forms Menu Module (.MMB file)    |      CAST_Forms_MenuModule      |
| ![Alt text](668336404.png)     |      Oracle Forms Module (.FMB file)       |        CAST_Forms_Module        |
| ![Alt text](668336405.png)     |     Oracle Forms Menu Module Parameter     |   CAST_Forms_ModuleParameter    |
| ![Alt text](668336406.png)     |  Oracle Forms Object Library (.OLB file)   |    CAST_Forms_ObjectLibrary     |
| ![Alt text](668336407.png)     |          Oracle Forms Library Tab          |   CAST_Forms_ObjectLibraryTab   |
| ![Alt text](668336408.png)     |            Oracle Forms Package            |       CAST_Forms_Package        |
| ![Alt text](668336409.png)     |      Oracle Forms Library Tab Package      |     CAST_Forms_Package_Tab      |
| ![Alt text](668336408.png)     |                Package Body                |     CAST_Forms_PackageBody      |
| ![Alt text](668336409.png)       |      Oracle Forms Library Tab Package      |   CAST_Forms_PackageBody_Tab    |
| ![Alt text](668336410.png)     |  Oracle Forms PL/SQL Library (.PLL file)   |     CAST_Forms_PlsqlModule      |
| ![Alt text](668336399.png)    |          Oracle Forms Popup Menu           |      CAST_Forms_PopupMenu       |
| ![Alt text](668336411.png)     |        Oracle Forms Popup Menu Item        |    CAST_Forms_PopupMenuItem     |
| ![Alt text](668336412.png)     |           Oracle Forms Procedure           |      CAST_Forms_Procedure       |
| ![Alt text](668336413.png)     |     Oracle Forms Library Tab Procedure     |    CAST_Forms_Procedure_Tab     |
| ![Alt text](668336414.png)     |           Oracle Forms Procedure           |   CAST_Forms_PrivateProcedure   |
| ![Alt text](668336415.png)     |        Oracle Forms Property Class         |    CAST_Forms_PropertyClass     |
| ![Alt text](668336416.png)     |  Oracle Forms Library Tab Property Class   |  CAST_Forms_PropertyClass_Tab   |
| ![Alt text](668336417.png)     |         Oracle Forms Record Group          |     CAST_Forms_RecordGroup      |
| ![Alt text](668336418.png)     |   Oracle Forms Library Tab Record group    |   CAST_Forms_RecordGroup_Tab    |
| ![Alt text](668336419.png)     |            Oracle Forms Trigger            |       CAST_Forms_Trigger        |
| ![Alt text](668336420.png)     |      Oracle Forms Library Tab Trigger      |     CAST_Forms_Trigger_Tab      |
| ![Alt text](668336421.png)     |       Oracle Forms Visual Attribute        |   CAST_Forms_VisualAttribute    |
| ![Alt text](668336422.png)     |     Oracle Forms Visual Attribute Tab      | CAST_Forms_VisualAttribute_Tab  |
| ![Alt text](668336423.png)     |       Oracle Forms Visual Attributes       |   CAST_Forms_VisualAttributes   |
| ![Alt text](668336424.png)     | Oracle Forms Library Tab Visual Attributes | CAST_Forms_VisualAttributes_Tab |
| ![Alt text](668336425.png)     |         Oracle Forms Package Type          |     FORMS_TYPEPACKAGEHEADER     |
| ![Alt text](668336426.png)     |         Oracle Forms Sub Procedure         |       FORMS_SUBPROCHEADER       |
| ![Alt text](668336427.png)     |         Oracle Forms Sub Function          |       FORMS_SUBFUNCHEADER       |
| ![Alt text](668336428.png)     |            Oracle Forms Cursor             |      FORMS_SUBCURSORHEADER      |
| ![Alt text](668336429.png)     |           Oracle Forms Variable            |     FORMS_SUBVARIABLEHEADER     |
| ![Alt text](668336430.png)     |         Oracle Forms Sub Procedure         |        FORMS_SUBPROCBODY        |
| ![Alt text](668336431.png)     |         Oracle Forms Sub Function          |        FORMS_SUBFUNCBODY        |
| ![Alt text](668336432.png)     |            Oracle Forms Cursor             |       FORMS_SUBCURSORBODY       |
| ![Alt text](668336429.png)     |           Oracle Forms Variable            |      FORMS_SUBVARIABLEBODY      |

## Oracle Reports objects

| Icon |       Meta Model Description       |        Meta Model Name         |
|------|------------------------------------|--------------------------------|
| ![Alt text](668336433.png)    |      Oracle Report Data Link       |      FORMS_RDF_DATA_LINK       |
| ![Alt text](668336434.png)    |   Oracle Report Database Column    |      FORMS_RDF_DB_COLUMN       |
| ![Alt text](668336435.png)     |    Oracle Report Summary Column    |    FORMS_RDF_SUMMARY_COLUMN    |
| ![Alt text](668336435.png)     |    Oracle Report Formula Column    |    FORMS_RDF_FORMULA_COLUMN    |
| ![Alt text](668336435.png)     |  Oracle Report Placeholder column  |  FORMS_RDF_PLACEHOLDER_COLUMN  |
| ![Alt text](668336436.png)     |      Oracle Report File Query      |    FORMS_RDF_EXT_FILE_QUERY    |
| ![Alt text](668336437.png)     |        Oracle Report Group         |        FORMS_RDF_GROUP         |
| ![Alt text](668336438.png)     |        Oracle Report Module        |        FORMS_RDF_MODULE        |
| ![Alt text](668336439.png)     |      Oracle Report Data Model      |      FORMS_RDF_DATA_MODEL      |
| ![Alt text](668336440.png)     |  Oracle Report Predefined Source   |  FORMS_RDF_PREDEFINED_SOURCE   |
| ![Alt text](668336441.png)     |   Oracle Report System Parameter   |   FORMS_RDF_SYSTEM_PARAMETER   |
| ![Alt text](668336441.png)    |    Oracle Report User Parameter    |    FORMS_RDF_USER_PARAMETER    |
| ![Alt text](668336442.png)     |       Oracle Report Element        |       FORMS_RDF_ELEMENT        |
| ![Alt text](668336436.png)     |   Oracle Report Non OraDB Query    |   FORMS_RDF_NON_ORADB_QUERY    |
| ![Alt text](668336436.png)       |        Oracle Report Query         |        FORMS_RDF_QUERY         |
| ![Alt text](668336436.png)       |   Oracle Report Ref Cursor Query   |   FORMS_RDF_REF_CURSOR_QUERY   |
| ![Alt text](668336436.png)       |    Oracle Report Unknown Query     |    FORMS_RDF_UNKNOWN_QUERY     |
| ![Alt text](668336443.png)     |      Oracle Report BP Button       |        FORMS_RDF_BUTTON        |
| ![Alt text](668336444.png)     |       Oracle Report BP Chart       |        FORMS_RDF_CHART         |
| ![Alt text](668336445.png)     |    Oracle Report Form Parameter    |    FORMS_RDF_PARAMETER_FORM    |
| ![Alt text](668336435.png)     |     Oracle Report user column      | FORMS_RDF_USER_UNKNOWN_COLUMN  |
| ![Alt text](668336446.png)     |     Oracle Report Layout Model     |     FORMS_RDF_LAYOUT_MODEL     |
| ![Alt text](668336447.png)     |        Oracle Report Header        |        FORMS_RDF_HEADER        |
| ![Alt text](668336448.png)     |       Oracle Report Trailer        |       FORMS_RDF_TRAILER        |
| ![Alt text](668336449.png)     |         Oracle Report Body         |         FORMS_RDF_BODY         |
| ![Alt text](668336450.png)     |        Oracle Report Margin        |        FORMS_RDF_MARGIN        |
| ![Alt text](668336451.png)     |   Oracle Report Repeating Frame    |   FORMS_RDF_REPEATING_FRAME    |
| ![Alt text](668336452.png)     |        Oracle Report Frame         |        FORMS_RDF_FRAME         |
| ![Alt text](668336453.png)     |        Oracle Report Field         |        FORMS_RDF_FIELD         |
| ![Alt text](668336454.png)     |       Oracle Report BP Text        |       FORMS_RDF_BP_TEXT        |
| ![Alt text](668336455.png)     |     Oracle Report BP Polygone      |     FORMS_RDF_BP_POLYGONE      |
| ![Alt text](668336456.png)     |     Oracle Report BP Polyline      |     FORMS_RDF_BP_POLYLINE      |
| ![Alt text](668336457.png)     |     Oracle Report BP Freehand      |     FORMS_RDF_BP_FREEHAND      |
| ![Alt text](668336458.png)     |       Oracle Report BP Line        |       FORMS_RDF_BP_LINE        |
| ![Alt text](668336459.png)     |     Oracle Report BP Rectangle     |     FORMS_RDF_BP_RECTANGLE     |
| ![Alt text](668336460.png)     | Oracle Report BP Rounded Rectangle | FORMS_RDF_BP_ROUNDED_RECTANGLE |
| ![Alt text](668336461.png)     |      Oracle Report BP Ellipse      |      FORMS_RDF_BP_ELLIPSE      |
| ![Alt text](668336462.png)     |        Oracle Report BP Arc        |        FORMS_RDF_BP_ARC        |
| ![Alt text](668336463.png)     |       Oracle Report BP Image       |       FORMS_RDF_BP_IMAGE       |
| ![Alt text](668336464.png)     |      Oracle Report BP Unknown      |      FORMS_RDF_BP_UNKNOWN      |
| ![Alt text](668336465.png)     |     Oracle Report Program Unit     |     FORMS_RDF_PROGRAM_UNIT     |


## Links

|  Link Type  |   When is this type of link created?   |
|-------------|----------------------------------------|
|   Use (U)   | This type is generically used to describe an interaction between two objects.  For example:  between a Form Module and its Menu Module                                                                   |
|  Call (C)   | A link of this type is created from a (popup) menu item invoking a menu to the cascading (popup) menu.  For example:  Between a data block item and the sequence specified in its Initial Value property. Between a Form Module and the Report Modules via Report Objects. |
| Inherit (I) |                             A link of this type is created from an object created by subclassing to the source object. It is also created between an object and its property class and between a subclassed Module and its reference modules.                              |
|  Match (X)  |                                                                                               This type of link indicates a link you have created using a Reference Pattern.                                                                                               |
|   Rely On   |                                                                                       This type of link is created between a data block and the server objects used to construct it.                                                                                       |
|   Access    |                                                                This type of link is created when there is a reference in the code to a data block item through host variable syntax (:block.item or :item).                                                                |
|   Monitor   |                                                                                               This type of link is created between a data block relation and the data block.                                                                                               |