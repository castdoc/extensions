---
title: "Oracle Forms and Reports Discoverer"
linkTitle: "Oracle Forms and Reports Discoverer"
type: "docs"
no_list: false
---

Configures a project/Analysis Unit for each of the following items delivered as source code:

Oracle Forms:

- .fmb
- .olb
- .mmb
- .pll

Oracle Reports:

- .rex

Displayed as follows:

![Alt text](console_formsAU.jpg)