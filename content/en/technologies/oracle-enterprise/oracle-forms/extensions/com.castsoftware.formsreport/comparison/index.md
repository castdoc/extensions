---
title: "Comparison with legacy Oracle Forms and Reports analyzer provided with 8.3.x"
linkTitle: "Comparison"
type: "docs"
---

## Overview

The support for Oracle Forms and Reports provided by the **com.castsoftware.formsreport** extension is similar to the supported provided by the Oracle Forms and Reports analyzer embedded in AIP Core 8.3.x, however it is not identical. These differences are explained below. This comparison was made using the latest Oracle Forms and Reports analyzer embedded in AIP Core 8.3.x and the 1.0.0-alpha1 release of the **com.castsoftware.formsreport** extension.

## Objects

The following table provides information about equivalence for objects:

- Most objects have equivalents in both
- Some objects created by the AIP 8.3.x Oracle Forms and Reports analyzer have no equivalent object in the Oracle Forms and Reports extension. These objects are therefore not listed below.

| Object family | AIP 8.3.x Oracle Forms and Reports | Oracle Forms and Reports as extension | Impact | Remarks |
|:---:|---|---|---|---|
| Structure | | <p>(1000003) Universal Directory (1000001)</p><p>Universal Project</p>(141813) The result project of a plugin| No impact, just structure |
| Structure | (126) Oracle Forms Application | (1107004) Oracle Forms and Report Analyzer Project | |
| Structure | <p>(817) Oracle Report Module</p><p>(107) Oracle Forms Module</p> | <p>(1107007) Oracle Forms Module</p><p>(1107017) Oracle Forms PL/SQL Library</p><p>(1107018) Oracle Forms Menu Module</p><p>(1107021) Oracle Forms Object Library</p><p>(1107022) Oracle Report</p>| No impact, just structure |
| Structure | (107510) Forms Subset | (1107008) Oracle Forms Report Analyzer Subset | No impact, just structure |
| Package | <p>(124) Oracle Forms Package</p><p>(125) Oracle Forms Package Body</p><p>(321) Oracle Forms Library Tab Package</p><p>(322) Oracle Forms Library Tab Package</p> | (1107016) Oracle Forms Package | No impact, merged types |
| Procedure alike | <p>(319) Oracle Forms Library Tab Procedure</p><p>(141428) Oracle Forms Library Tab Private Procedure</p><p>(141426) Oracle Forms Procedure</p><p>(122) Oracle Forms Procedure</p> | (1107014) Oracle Forms Procedure | No impact, merged types |
| Procedure alike | <p>(123) Oracle Forms Function</p><p>(320) Oracle Forms Library Tab Function</p><p>(141427) Oracle Forms Library Tab private Function</p><p>(141425) Oracle Forms private stored function</p> | (1107015) Oracle Forms Function | No impact, merged types |
| Procedure alike | <p>(106) Oracle Forms Trigger</p><p>(314) Oracle Forms Library Tab Trigger</p> | (1107009) Oracle Forms Trigger | No impact, merged types |
| Window alike | <p>(141242)Oracle Forms Window</p><p>(141457)Oracle Forms Window Tab</p><p>(141367)Oracle Forms Alert Tab</p><p>(141226)Oracle Forms Alert</p><p>(324)Oracle Forms Library Tab LOV</p><p>(223) Oracle Forms LOV</p> | (1107011) Oracle Forms Window | No impact, merged types |
| Data Block alike | <p>(108) Oracle Forms DataBlock</p><p>(315) Oracle Forms Library Tab DataBlock</p> | (1107012) Oracle Forms DataBlock | No impact, merged types |
| Data Block alike | (316) Oracle Forms Library Tab DataBlock Item | (1107013) Oracle Forms DataBlock Item| No impact, merged types |
| Menu alike | <p>(111) Oracle Forms Menu</p><p>(224) Oracle Forms Popup Menu</p> | (1107019) Oracle Forms Menu | No impact, merged types |
| Menu Item alike | <p>(112) Oracle Forms Menu Item</p><p>(318) Oracle Forms Library Tab Menu Item</p><p>(225) Oracle Forms Popup Menu Item</p> | (1107020) Oracle Forms Menu Item | No impact, merged types |
| Report Query | (822) Oracle Report Query | (1107023) Oracle Report Query | |

## Links

Links are identical between the two.

## Structural rules

The following table provides information about equivalence for structural rules:

- Most structural rules have equivalents in both
- Some structural rules exist only in the AIP 8.3.x Oracle Forms and Reports analyzer and therefore have with no equivalent - and vice versa.

| AIP 8.3.x Oracle Forms and Reports | Oracle Forms and Reports as extension | Impact | Remarks |
|:---:|---|---|---|
| (1574) Use at most one statement per line | (1101062) Avoid using multiple statements per line (SQL) | | |
| (1580) Avoid using execute immediate | (1101024) Avoid using dynamic SQL in SQL Artifacts | | |
| (1588) Use WHEN OTHERS in exception management | (1101052) Use WHEN OTHERS in exception management (SQL) | | |
| (1598) Avoid Rule HINT /*+ rule */ or --+ rule in PL/SQL code | | | Not implemented |
| (1606) Triggers should not directly modify tables, a procedure or function should be used instead | (1606) Triggers should not directly modify tables, a procedure or function should be used instead | | |
| (1608) Avoid cascading Triggers | (1101064) Avoid cascading Triggers (SQL) | | |
| (5550) Avoid having joins with more than 4 Tables | (1101030) Avoid Artifacts with queries on too many Tables and/or Views | | |
| (5554) Avoid having SQL code in Triggers named "pre-record" | | | Not implemented |
| (5558) Based Data Blocks naming convention - represented table | (1107004) Oracle Forms Table Based Data Blocks naming convention | | |
| (5560) Based Data Block Items naming convention - represented column | (1107006) Oracle Forms Column Based Data Block Items naming convention | | |
| (5562) Not Based Data Blocks naming convention - prefix control | (1107008) Oracle Forms Control Blocks naming convention - prefix control | | |
| (5564) Not Based Data Block Items naming convention - prefix control | (1107010) Oracle Forms Control Block Items naming convention - prefix control | | |
| (5566) Use of call_form built-in Function from a centralized location | (1107000) Use of call_form built-in Oracle Forms Function from a centralized locatio | | |
| | (1101074) Avoid Tables aliases ending with a numeric suffix | | |
| | (1101072) Avoid not aliased Tables | | |
| | (1101076) Avoid unqualified column references | | |
| | (1101012) Avoid specifying column numbers instead of column names in ORDER BY clauses | | |
| | (1101028) Use MINUS or EXCEPT operator instead of NOT EXISTS and NOT IN subqueries | | |
| | (1101008) Avoid non-SARGable queries | | |
| | (1101086) Always use VARCHAR2 and NVARCHAR2 instead of CHAR, VARCHAR or NCHAR | | |
| (5572) Avoid objects without COMMENT property | (1107002) Avoid Oracle Forms objects without comments | | |
| (7420) Avoid SQL queries with implicit conversions in the WHERE clause | | | Not implemented |
| (7660) Never use SQL queries with a cartesian product on XXL Tables | (1101002) Never use SQL queries with a cartesian product on XXL Tables (SQL) | | |
| (7662) Avoid SQL queries on XXL Tables with implicit conversions in the WHERE clause | | | Not implemented |
| (7760) Avoid triggers, functions and procedures with a very low comment/code ratio | | | |
| (7762) Avoid undocumented Triggers, Functions and Procedures | | | |
| (7766) Avoid Artifacts with High Cyclomatic Complexity | | | |
| (7768) Avoid Artifacts with High Depth of Code | | | |
| (7770) Avoid Artifacts with too many parameters | (1101016) Avoid Artifacts with too many parameters (SQL) | | |
| (7772) Avoid Artifacts with High Essential Complexity | (7772) Avoid Artifacts with High Essential Complexity | | |
| (7774) Avoid Artifacts with High Integration Complexity | (7774) Avoid Artifacts with High Integration Complexity | | |
| (7776) Avoid Artifacts with High Fan-In | (7776) Avoid Artifacts with High Fan-In | | |
| (7778) Avoid Artifacts with High Fan-Out | (7778) Avoid Artifacts with High Fan-Out | | |
| (7784) Avoid Artifacts with lines longer than X characters | (7784) Avoid Artifacts with lines longer than X characters | | |
| (7790) Avoid Cursors inside a loop | (1101084) Avoid Cursors inside a loop (SQL) | | |
| (7806) Avoid Artifacts with Group By | (1101018) Avoid using the GROUP BY clause | | |
| (7808) Avoid Artifacts with SQL statement including subqueries | (7808) Avoid Artifacts with SQL statement including subqueries | | |
| (7810) Avoid Artifacts with a Complex SELECT Clause | (1101098) Avoid Artifacts with a Complex SELECT Clause (SQL) | | |
| | (1101034) Avoid using DISTINCT in SQL SELECT statements | | |
| (7820) Never use SQL queries with a cartesian product | (1101000) Never use SQL queries with a cartesian product (SQL) | | |
| (7822) Avoid Artifacts with queries on more than 4 Tables  | (1101030) Avoid Artifacts with queries on too many Tables and/or Views | | |
| (7826) Forms naming convention - prefix control | (7826) Forms naming convention - prefix control | | |
| (7828) Avoid Artifacts with High RAW SQL Complexity | (7828) Avoid Artifacts with High RAW SQL Complexity | | |
| (7840) Trigger naming convention - prefix control | (7840) Trigger naming convention - prefix control | | |
| (7842) Avoid large Artifacts - too many Lines of Code | (7842) Avoid large Artifacts - too many Lines of Code | | |
| (7860) Avoid unreferenced Functions and Procedures | (7860) Avoid unreferenced Functions and Procedures | | |
| (7900) Stored Procedure naming convention - prefix control | (7900) Stored Procedure naming convention - prefix control | | |
| <p>(7902) Avoid SQL queries that no index can support</p><p>(7418) Avoid SQL queries using functions on indexed columns in the WHERE clause</p><p>(7428) Avoid SQL queries not using the first column of a composite index in the WHERE clause</p> | (1101004) Avoid non-indexed SQL queries | | |
| <p>(7642) Avoid SQL queries on XXL tables not using the first column of a composite index in the WHERE clause</p><p>(7658) Avoid SQL queries on XXL Tables using Functions on indexed Columns in the WHERE clause</p> | (1101006) Avoid non-indexed XXL SQL queries | | |
| (7946) Avoid queries using old style join convention  instead of ANSI-Standard joins | (1101014) Avoid queries using old style join convention instead of ANSI-Standard joins (SQL) | | |
| (7344) Avoid "SELECT *" queries | (1101114) Avoid "SELECT *" queries (SQL) | | |
| (7948) Do not mix Ansi joins syntax  with Oracle proprietary joins syntax in the same query | (1101058) Avoid mixing ANSI and non-ANSI JOIN syntax in the same query | | |
| (7960) Avoid looping chain of synonyms in PL/SQL context | (1101082) Avoid looping chain of synonyms | | Synonyms are created by SQL Analyzer. |
| (8036) Avoid improperly written triangular joins with XXL tables in PL/SQL code  | (1101066) Avoid improperly written triangular joins with XXL tables | | |
| (7126) Avoid Artifacts with high Commented-out Code Lines/Code Lines ratio | (7126) Avoid Artifacts with high Commented-out Code Lines/Code Lines ratio | | |
| (7130) Avoid Artifacts with High Depth of Nested Subqueries | (7130) Avoid Artifacts with High Depth of Nested Subqueries | | |
| (7156) Avoid Too Many Copy Pasted Artifacts | (7156) Avoid Too Many Copy Pasted Artifacts | | |
| (7424) Avoid using SQL queries inside a loop | (1101116) Avoid using SQL queries inside a loop (SQL) | | |
| (7436) Prefer UNION ALL to UNION | (7436) Prefer UNION ALL to UNION | | |