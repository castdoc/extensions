---
title: "Oracle Forms and Reports - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.formsreport

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for the analysis of Oracle Forms and Reports applications, specifically for CAST Imaging v3.

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :x: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## What about the legacy Oracle Forms and Reports analyzer?

The **com.castsoftware.formsreport** extension is specific to CAST Imaging v3 and does not function with v2/8.3.x. The extension should not be considered as an exact replacement for the Oracle Forms and Reports analyzer embedded in AIP Core 8.3.x and you should note that there is no supported migration path between the two.

The support for Oracle Forms and Reports provided by the **com.castsoftware.formsreport** extension is similar to the supported provided by the analyzer embedded in AIP Core 8.3.x, however it is not identical. If you would like to know more about the differences, please see [Comparison with legacy Oracle Forms and Reports analyzer provided with 8.3.x](../comparison/).

## Technical information

### Supported releases of Oracle Forms and Reports

If you can succesfully convert your Oracle Forms and Reports files to the list of supported and accepted files (see section [below](#supported-files-for-analysis)), then the extension will be able to analyze them.

### Supported files for analysis

This extension supports the following files for analysis:

- `.rex` files for `.rdf` files
- `.xml` files for `.fmb`, `.mmb`, `.rdf` and `.olb` files.
- `.pld` files for `.pll` files

In otherwords, you will need to convert your Forms/Reports files before you can deliver them for analyis. This conversion process is described in the table below:

| Original file | Accepted file type | Conversion methods |
|:--:|---|---|
| `.fmb`<br><br>(Oracle Forms binary File / Module File) | `.xml` | To convert `.fmb` files to `.xml`, use either of the following methods:<ul><li>Oracle Forms Builder, see : https://blogs.oracle.com/apex/post/forms-to-apex-converting-fmbs-to-xml</li><li>The Oracle command line utility `Forms2XMLfrmf2xml` (for Microsoft Windows) or `frmf2xml.sh` (for Linux), see : http://docs.oracle.com/cd/E16764_01/bi.1111/b32121/pbr_cla002.htm</li></ul> |
| `.mmb`<br><br>Oracle Forms Menu File | `.xml` | Use the same methods as described above for `.fmb` files. |
| `.olb`<br><br>Oracle Object Library File | `.xml` | Use the same methods as described above for `.fmb` files. |
| `.pll`<br><br>PL/SQL Library File | `.pld` | To convert `.pll` files to `.pld`, use the **File > Convert** option in Oracle Forms Builder, see : https://docs.oracle.com/cd/E14373_01/migrate.32/e13368/appmgr_forms.htm#CHDEIIHC |
| `.rdf`<br><br>Oracle Reports File | `.xml` or `.rex` | To convert `.rdf` to `.xml`, see: https://docs.oracle.com/en/database/oracle/application-express/19.2/aemig/converting-oracle-forms-to-XML.html#GUID-6862BD3A-84B6-4684-B64F-481142802D40, https://docs.oracle.com/html/B10314_01/pbr_cla.htm#634712.<br><br>To convert `.rdf` to `.rex`, see: https://doc.castsoftware.com/technologies/oracle-enterprise/oracle-forms/requirements/#note-about-oracle-reports. |

### Source code discovery

The extension will "discover" one project (and create one "Analysis Unit") if at least one of the following files is delivered for analysis:

- `.rex`
- `.xml`
- `.pld`

And when when at least one of the files contains content that matches the following regular expression:

```text
MenuModule|FormModule|ObjectLibrary|report
```

## What results can you expect?

### Objects

| Icon | Object Type | Description |
|:---:|---|---|
| ![](../images/fmb.png) | Oracle Forms Module | Object created from a FormModule tag.  |
| ![](../images/function.png) | Oracle Forms Function | Object created with FUNCTION statements from a ProgramUnit in a xml file, DEFINE TOOL_PLSQL tag in a rex file or function tag in a Oracle Reports xml file. The same type cover Package's function. |
| ![](../images/procedure.png) | Oracle Forms Procedure | Object created with PROCEDURE statements from a ProgramUnit in a xml file, DEFINE TOOL_PLSQL tag in a rex file or function tag in a Oracle Reports xml file. The same type cover Package's procedure. |
| ![](../images/package.png) | Oracle Forms Package | Object created with PACKAGE statement from a ProgramUnit in a xml file, DEFINE TOOL_PLSQL tag in a rex file or function tag in a Oracle Reports xml file. |
| ![](../images/trigger.png) | Oracle Forms Trigger | Object created from a ProgramUnit, Trigger, DEFINE TOOL_PLSQL or a function tag. |
| ![](../images/window.png) | Oracle Forms Window | Object created from a Window, Alert or a LOV tag. |
| ![](../images/datablock.png) | Oracle Forms DataBlock | Object created from a Block tag. |
| ![](../images/item.png) | Oracle Forms DataBlock Item | Object created from an Item tag. |
| ![](../images/olb.png) | Oracle Forms Object Library | Object created from an ObjectLibrary tag. |
| ![](../images/pll.png) | Oracle Forms PL/SQL Library | Object created for a pld file. |
| ![](../images/mmb.png) | Oracle Forms Menu Module | Object created from a MenuModule tag. |
| ![](../images/menu.png) | Oracle Forms Menu | Object created from a Menu tag. |
| ![](../images/menuitem.png) | Oracle Forms Menu Item | Object created from a MenuItem tag. |
| ![](../images/rex.png) | Oracle Report | Object created for a rex/xml file. |
| ![](../images/query.png) | Oracle Report SQL Query | Object created from a DEFINE SRW2_QUERY tag from a rzx file or a dataSource tag from a xml file. |

### Links

| Link Type | Caller type | Callee type | Details / Comments |
|---|---|---|---|
| useSelect, useUpdate, useDelete, useInsert | Oracle Forms Procedure, Oracle Forms Function, Oracle Forms Trigger, Oracle Report SQL Query, Oracle Forms Menu, Oracle Forms Menu Item, Oracle Forms DataBlock, Oracle Forms DataBlock Item, Oracle Forms Window | Table, View, Synonym |  |
| call | Oracle Forms Procedure, Oracle Forms Function, Oracle Forms Trigger, Oracle SQL Query, Oracle Forms Menu, Oracle Forms Menu Item, Oracle Forms DataBlock, Oracle Forms DataBlock Item, Oracle Forms Window | Oracle Forms Procedure, Oracle Forms Function, Procedure, Function |  |
| call | Oracle Forms Procedure, Oracle Forms Function, Oracle Forms Trigger, Oracle Report SQL Query, Oracle Forms Menu, Oracle Forms Menu Item, Oracle Forms DataBlock, Oracle Forms DataBlock Item, Oracle Forms Window | Oracle Forms Module | Via the built-in function call_form. |
| call | Oracle Report | Oracle Forms Function, Oracle Report SQL Query  | Each function and SQL query defined in a Oracle Report it belongs from the Oracle Report and it is seen as called by the Oracle Report |
| call | Oracle Forms Menu Module | Oracle Forms Menu | When the MainMenu is specified, link the menu module with the menu. |
| call | Oracle Forms Module | Oracle Forms PL/SQL Library | For the AttachedLibrary tag values. |
| monitor | Oracle Forms Module, Oracle Forms DataBlock, Oracle Forms DataBlock Item | Oracle Forms Trigger |  |
| monitorInsert, monitorUpdate, monitorDelete | Oracle Forms Module, Oracle Forms DataBlock, Oracle Forms DataBlock Item | Oracle Forms Trigger | For the PRE/POST/ON-INSERT/UPDATE/DELETE triggers.  |
| inherit | Oracle Forms Module object | Oracle Forms Object Library object | Link created based on the ParentModule and ParentName attributs. |

## Structural rules

The following rules are provided in the extension:

- [1.0.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_formsreport&ref=%7C%7C1.0.0-alpha1)

## Log messages

{{% fetch-remote-md url="https://extend.castsoftware.com/api/delta/export/log-messages/format/markdown?id=com.castsoftware.formsreport&renew=true" %}}