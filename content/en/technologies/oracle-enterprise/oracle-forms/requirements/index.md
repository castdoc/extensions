---
title: "Requirements"
linkTitle: "Requirements"
type: "docs"
weight: 3
---

<table>
<thead>
<th>Technology</th>
<th>Pre-extraction?</th>
<th>Version supported</th>
<th>Install on server running the CAST Imaging Console</th>
<th>Install on the Node</th>
</thead>
<tbody>
&#10;<tr>
<td>Oracle Forms</td>
<td>Pre-extraction via the standalone <a
href="https://doc.castsoftware.com/display/DOCCOM/CAST+Oracle+Forms+Extractor">CAST
Oracle Forms Extractor</a> must be performed.</td>
<td>32bit and 64bit</td>
<td>Nothing required</td>
<td>Nothing required</td>
</tr>
<tr>
<td>Oracle Reports</td>
<td>Pre-extraction not required. See notes
below.</td>
<td>32bit and 64bit</td>
<td>Nothing required</td>
<td>Nothing required</td>
</tr>
</tbody>
</table>

## Note about Oracle Reports

When you need to deliver Oracle Reports, the CAST can only
accept .rex files. If you have Oracle Reports .rdf files and
would like to include them in the delivery, you can convert them to .rex
files as follows:

-   Using Oracle Reports Builder
-   Using the Oracle command line utility rwconverter.exe (for
    Windows) or rwconverter.sh (for Linux) -
    see [http://docs.oracle.com/cd/E16764_01/bi.1111/b32121/pbr_cla002.htm](http://docs.oracle.com/cd/E16764_01/bi.1111/b32121/pbr_cla002.htm) -
    see example script below:

``` java
@@Echo on
REM set the REPORTS_PATH Oracle Environment Variable to point to the reports and templates required by rwconverter.exe. This environment variable is
REM used to locate reports and external objects that you use in your reports, such as PL/SQL libraries, external queries, and external boilerplate.
REM see https://docs.oracle.com/cd/B14099_19/bi.1012/b14048/pbr_rfap.htm#i648209 for more information.
set REPORTS_PATH=<path_to_reports>;<path_to_dependencies>
 
REM change directories and move to the location of your *.rdf files
cd D:\path_to\some_folder\
 
REM loop through all .rdf files in the current folder and convert into .rex files
for %%f in (*.rdf) do C:\path_to\rwconverter.exe source=%%f userid=<userid> stype=rdffile dtype=rexfile dest=..\rex1%%f.rex batch=yes
pause
```