---
title: "Covered Technologies"
linkTitle: "Covered Technologies"
type: "docs"
weight: 1
---

| Versions supported | CAST Imaging | CAST Dashboards (Quality, Security and Sizing) | Function Points (transactions) | Supported by |
|----|----|----|----|----|
| Forms/Reports 10g: 9.0.4 | ![](/images/icons/emoticons/check.svg) | ![](/images/icons/emoticons/check.svg) | ![](/images/icons/emoticons/check.svg) | Oracle Forms and Reports Analyzer (embedded in CAST Imaging Core). |
| Forms/Reports 10g R2: 10.1.2.0.2 | ![](/images/icons/emoticons/check.svg) | ![](/images/icons/emoticons/check.svg) | ![](/images/icons/emoticons/check.svg) |  |
| Forms/Reports 10g R2: 10.1.2.3.0 | ![](/images/icons/emoticons/check.svg) | ![](/images/icons/emoticons/check.svg) | ![](/images/icons/emoticons/check.svg) |  |
| Forms/Reports 11g  | ![](/images/icons/emoticons/check.svg)  | ![](/images/icons/emoticons/check.svg) | ![](/images/icons/emoticons/check.svg) |  |