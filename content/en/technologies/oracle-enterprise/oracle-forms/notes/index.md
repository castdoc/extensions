---
title: "Technical notes"
linkTitle: "Technical notes"
type: "docs"
weight: 2
---

## Inter-Application links

Please note that currently inter-Application links (i.e. links
between source code in separate Applications) between the following
combinations of technologies is not supported:

-   Between Oracle Forms Analysis Units in different Applications
-   Between Oracle Forms Analysis Units and Oracle Server PL/SQL
    Analysis Units in different Applications

If you do require link resolution, then all Analysis Units must belong
to the same Application.

## Objects containing non printable ASCII characters

Oracle Forms objects containing non printable ASCII character such as
DC2 (Device Control2) are skipped during the analysis and a warning
message will be outputted to the log file.

## go_block built-in not supported

CAST does not support the use of the "go_block" statement to call data
blocks. Only "call_forms" is supported.