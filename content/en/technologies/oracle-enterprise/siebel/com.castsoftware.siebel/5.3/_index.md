---
title: "Siebel - 5.3"
linkTitle: "5.3"
type: "docs"
no_list: true
---

***

>Please ensure that you strictly follow all instructions exactly as
described in this documentation. Failure to do so may result in a failed
installation or erroneous results. For example:
>-   Do not use these instructions with previous releases of the Siebel extension, and vice-versa.
>-   Avoid creating your own User Defined Modules - please follow the User Defined Modules configuration exactly as described in [Onboarding with CAST Imaging Console](console).

## Extension ID

com.castsoftware.siebel

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for Siebel.

### In what situation should you install this extension?

The Siebel Analyzer has been designed mostly for its quality model
inspired by customer best practices as well as Siebel review checklists.
Hence it fulfils the use cases using the CAST Dashboard.

## Supported Versions of Siebel

This extension provides support for the following Siebel versions:

| Siebel version | Supported | Comments |
|---|:-:|---|
| 7.5 | :white_check_mark: |   |
| 7.7 | :white_check_mark: |   |
| 7.8 | :white_check_mark: |   |
| 8.0 | :white_check_mark: |   |
| 8.1.x.x | :white_check_mark: |   |
| 15.x (8.1.1.15/8.2.2.15, IP 2015) | :white_check_mark: | New functionalities or syntax introduced in these versions are NOT supported, but this does not affect the results. |
| 16.x (IP 2016) | :white_check_mark: |  New functionalities or syntax introduced in these versions are NOT supported, but this does not affect the results. |
| IP 2017<br> IP 2018<br> IP 2019<br> 20.x<br> 21.x<br> 22.x | :white_check_mark: |  New functionalities or syntax introduced in these versions are NOT supported, but this does not affect the results.<br><br>Limitation starting from 17.x (IP 2017): The Siebel Analyzer can ONLY extract from Siebel repositories that do NOT have the Workspace option enabled. If you attempt to run an extraction on a repository with the Workspace option enabled, the following error will be logged in the extractor log when using Siebel ≥ 5.3.2:<br><br>`Validation error #-5: Repository ID <ID> contains multiple workspaces in the schema <schema>`<br><br>If the Workspace option is enabled and you wish to perform an extraction, then you must apply the flattening process before you run the extraction, as per the following third-party documentation: https://docs.oracle.com/cd/E88140_01/books/UsingTools/using_workspaces39.html#wp1013245.  |
| 23.x and above | likely* | New functionalities or syntax introduced in these versions are NOT supported, but this does not affect the results.<br><br>In addition to the Workspace limitation described previously: * The Siebel repository tables "S_" are stable and have not changed for quite some time. As such, the extractor will likely extract the objects and links as expected, and the rest of the analysis process will complete without issue. |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Supported DBMS servers for Siebel applications

The extension supports Siebel applications installed on the
following DBMS:

| DBMS | Supported  |
|---|:-:|
| Oracle Server | :white_check_mark: |
| DB2 UDB       | :white_check_mark: |
| Microsoft SQL Server (supported from v 5.2.6) | :white_check_mark: |

## Prerequisites

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">AIP Core</th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">An installation of any compatible release of
AIP Core (see table above). If you are using AIP Console to manage the
analysis, you must use AIP Core <strong>≥ 8.3.39</strong>.</td>
</tr>
<tr class="even">
<th class="confluenceTh">AIP Console</th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">If you would like to use AIP Console to manage
the analysis, please ensure that you are using <strong>v. ≥
1.27.</strong></td>
</tr>
<tr class="even">
<th class="confluenceTh">Legacy scan mode</th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">Fast scan mode is NOT supported - therefore ensure you are using legacy scan mode in AIP Console.</td>
</tr>
<tr class="odd">
<th class="confluenceTh"><p>Vanilla repository availability</p></th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Please ensure that the Vanilla repository is online and available
before starting.</p>
<div>
<div>
Note that the Vanilla repository must have the same Siebel version as
the Project repository, and same series of Siebel patches.
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh"><p>On Siebel server hosting the
Oracle/DB2/Microsoft SQL Server repository (for Vanilla and
Project)</p></th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>The server can be any OS, however Unix/Linux OS may require some
.ksh script if you want to run the extractor on the server machine
itself. </li>
<li>The version of the Oracle/DB2/Microsoft SQL Server server must match
a version supported by the JDBC driver embedded in the <strong><a
href="https://doc.castsoftware.com/display/DOCCOM/CAST+Database+Extractor">CAST
Database Extractor</a></strong> (used to extract the SQL data). This is
different and wider than the supported versions for a participating
database. Note that versions supported by AIP Core for a participating
databases can be found in the official CAST AIP documentation (e.g.
<strong><a href="SQL_-_Covered_technologies">SQL - Covered
technologies</a></strong>)</li>
<li>Make sure that the <strong>Siebel schema statistics</strong> are
up-to-date (if not up-to-date, some queries in the extractor may not
finish in reasonable times (especially the first one)). To update them,
run the following:</li>
</ul>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<th class="confluenceTh">Oracle</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>Update the schema with the following SQL statement (requires some
privileges : GRANT execute ON dbms_stats TO XXX):</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: sql; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: sql; gutter: false; theme: Confluence"><pre
class="sourceCode sql"><code class="sourceCode sql"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="kw">Execute</span> dbms_stats.gather_schema_stats(ownname <span class="op">=&gt;</span> <span class="st">&#39;MY_SIEBEL_SCHEMA&#39;</span>, estimate_percent <span class="op">=&gt;</span> dbms_stats.auto_sample_size, method_opt <span class="op">=&gt;</span> <span class="st">&#39;for all columns size auto&#39;</span>, <span class="kw">cascade</span> <span class="op">=&gt;</span> <span class="kw">true</span>); </span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">DB2 UDB</th>
<td class="confluenceTd">Check if the DB2 automatic runstats and DB2
real time statistics are enabled. If not, ask the DBA to
run "<strong>db2 reorgchk update statistics on table all</strong>" or
similar.</td>
</tr>
<tr class="odd">
<th class="confluenceTh">Microsoft SQL Server</th>
<td class="confluenceTd">Not required.</td>
</tr>
</tbody>
</table>
</div>
<ul>
<li>Identify the USER that will be used for extraction (in the CAST
Delivery Manager Tool):
<ul>
<li>On Oracle, the required privileges for the USER used to perform the
extraction are: <strong>GRANT SELECT</strong> on all <strong>S_
tables</strong>. </li>
<li>On DB2 UDB, the minimum roles and permissions for an extract is as
follow : READ access on all S_ tables, <strong>connect to
database</strong> permission.</li>
<li>On Microsoft SQL Server, the required privileges for the USER used
to perform the extraction are: <strong>GRANT SELECT</strong> on all
<strong>S_ tables</strong>.</li>
</ul></li>
</ul>
<div>
<div>
For the list of <strong>S_* tables</strong>, see <strong><a
href="../images/557023523.txt"
data-linked-resource-id="557023523" data-linked-resource-version="1"
data-linked-resource-type="attachment"
data-linked-resource-default-alias="SIEBEL_TABLES.txt"
data-nice-type="Text File"
data-linked-resource-content-type="text/plain"
data-linked-resource-container-id="557023504"
data-linked-resource-container-version="24">Tables required to grant
access to the Siebel repository</a></strong>.
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh"><p>On Workstation used for extraction (where
the CAST Database Extractor / CAST Delivery Manager Tool is
deployed)</p></th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><ul>
<li>Windows or Linux (Unix/Linux OS may require some additional .ksh
scripts). </li>
<li><strong>JRE 1.7</strong> (ideally 64 bit version to allow for a JVM
large heap size).</li>
<li>Minimum <strong>1.2 GB RAM</strong> memory free, more if
available.</li>
<li><strong>DMT/extractor location</strong>:
<ul>
<li>Ideally, install the DMT/extractor on the RDBMS server (Oracle or
DB2) itself.</li>
<li>Alternatively on a machine with:<br />
&#10;<ul>
<li>very good <strong>bandwidth</strong> (1 GB/s) to the Oracle / DB2
server (some remote extractions fail due to poor VPN bandwidth)</li>
<li><strong>connectivity</strong> to the Oracle / DB2 server (required
ports open)</li>
</ul></li>
</ul></li>
<li><strong>4 GB free disk space </strong>(for temporary files before
compression)</li>
<li><strong>250 MB free disk space</strong> (to store the two extraction
archives) included in above requirement.</li>
<li>When using the standalone <strong><a
href="https://doc.castsoftware.com/display/DOCCOM/CAST+Database+Extractor">CAST
Database Extractor</a></strong> to extract the SQL data (instead of
using the extractors built into the CAST Delivery Manager Tool) you
should download the extractor here: <strong><a
href="https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extractor.sqldatabase&amp;version=latest"
rel="nofollow">https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extractor.sqldatabase&amp;version=latest</a></strong></li>
</ul></td>
</tr>
<tr class="even">
<th class="confluenceTh"><p>On AIP Node used for analysis</p></th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><ul>
<li>Windows (uses AIP).</li>
<li><strong>2 GB RAM</strong> memory free (for analysis/snapshot)</li>
<li>Connectivity to the CAST Storage Service/PostgreSQL instance server
hosting the application schemas (port open)</li>
<li>5-10 GB free disk space (to store the source code, once expanded by
the <strong>Deploy</strong> phase)</li>
<li><p>Disable <strong>anti-virus</strong> software. Check
that <strong>no anti-virus is running </strong>on the workstation or
disable real-time scan for the work folders (delivery &amp; deploy
folders + temp folders). Failure to do so will multiply the injection
runtime.</p></li>
</ul></td>
</tr>
<tr class="odd">
<th class="confluenceTh"><p>On CAST Storage Service/PostgreSQL instance
used for analysis</p></th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><strong>Disk space requirements</strong></p>
<p>Disk space depends on the Siebel version implemented by the
customer:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Siebel version</th>
<th class="confluenceTh">Vanilla _LOCAL</th>
<th class="confluenceTh">Project _LOCAL</th>
<th class="confluenceTh">Project _CENTRAL</th>
<th class="confluenceTh">_MNGT</th>
<th class="confluenceTh">UNDO and TEMP</th>
<th class="confluenceTh">Source Code</th>
<th class="confluenceTh">Total consumed</th>
<th class="confluenceTh">Total required at peak time</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">8.x</td>
<td class="confluenceTd">5 GB</td>
<td class="confluenceTd">7 GB</td>
<td class="confluenceTd">2 GB</td>
<td class="confluenceTd">0.256 GB</td>
<td class="confluenceTd">23 GB + 10 GB (maybe due to other
analyses)</td>
<td class="confluenceTd">5.98 GB</td>
<td class="confluenceTd">49 GB</td>
<td class="confluenceTd">100 GB</td>
</tr>
<tr class="even">
<td class="confluenceTd">7.8</td>
<td class="confluenceTd">2.5 GB</td>
<td class="confluenceTd">3 GB</td>
<td class="confluenceTd">0.8 GB</td>
<td class="confluenceTd">0.256 GB</td>
<td class="confluenceTd">TBD or see above.</td>
<td class="confluenceTd">5.21 GB</td>
<td class="confluenceTd">30-35 GB</td>
<td class="confluenceTd">50 GB</td>
</tr>
</tbody>
</table>
</div>
<p><strong>Disk speed requirements</strong></p>
<p>Siebel analysis incurs heavy disk usage. <strong>Disk I/O
throughput</strong> (not necessarily speed) is possibly the most
important factor to determine the analysis time. So far we have tested
three environments with the tool <strong>iometer</strong>,
following <strong><a href="https://greg.porter.name/wordpress/?p=666"
rel="nofollow">this how-to</a></strong>. The rough results are
these:</p>
<ul>
<li>DELL laptop, 1 disk 7500rpm. Iometer reports around 3 MB/s. Analysis
times +20 hours.</li>
<li>HP desktop, 2 disks 10000rpm. Iometer reports around 5 MB/s in one
of the disks. Analysis time 10 hours (sources in one disk, oracle
datafiles in another disk).</li>
<li>DELL workstation, 4 disks 10000rpm in a RAID5 configuration. Iometer
reports around 150MB/s. Analysis time 4 hours (since this is a very
performant disk configuration, CPU time is possibly part of the
bottleneck. This means it would be hard to further reduce analysis time,
unless switching to other costlier technology, like SSD drive).</li>
</ul></td>
</tr>
<tr class="even">
<th class="confluenceTh"><p>Tables required to grant access to the
Siebel repository</p></th>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>The following database tables are accessed
during the Siebel extraction process:<strong> <a
href="../images/557023523.txt"
data-linked-resource-id="557023523" data-linked-resource-version="1"
data-linked-resource-type="attachment"
data-linked-resource-default-alias="SIEBEL_TABLES.txt"
data-nice-type="Text File"
data-linked-resource-content-type="text/plain"
data-linked-resource-container-id="557023504"
data-linked-resource-container-version="24">SIEBEL_TABLES.txt</a></strong>.</p></td>
</tr>
</tbody>
</table>

## FAQs

Please see [PeopleSoft & Siebel FAQ](../../../peoplesoft/faq) for more information.

## Download, installation, configuration and operating instructions
See [Onboarding with CAST Imaging Console](console).

## What results can you expect?

### Objects

The objects listed below are all the object types that a Siebel analysis
will deliver in the Analysis Service. With these objects, CAST is
capable of supporting a set of quality rules to provide a customer with
a good overview of the current technical status of a Siebel application.
It is not the intention of CAST to capture all details and syntaxes; a
coupling between requirement and detection is made.

Click to enlarge or download it
[here](../images/557023519.png):

![](../images/557023519.png)

The Siebel extension is capable of detecting a large number of objects,
properties and links. All of these artifacts are mentioned in the
picture above, but in essence, all Siebel objects are supported:

| Icon | Metamodel description |
|---|---|
| ![](../images/557023518.png) | Siebel Application |
| ![](../images/557023517.png) | Siebel Screen |
| ![](../images/557023516.png) | Siebel View |
| ![](../images/557023515.png) | Siebel Business Objects |

Applets

| Icon | Metamodel description |
|---|---|
| ![](../images/557023514.png) | Siebel Applet - Association List<br>Siebel Applet - Detail<br>Siebel Applet - MVG<br>Siebel Applet - NULL<br>Siebel Applet - Pick List<br>Siebel Applet - Playbar<br>Siebel Applet - Standard<br>Siebel Applet - Task |

Business Component

| Icon | Metamodel description |
|---|---|
| ![](../images/557023512.png) | Siebel Business Component |

Table

| Icon | Metamodel description |
|---|---|
| ![](../images/557023511.png) | Siebel Table Data (Intersection)<br>Siebel Table Data (Private)<br>Siebel Table Data (Public)<br>Siebel Table Dictionary<br>Siebel Table Extension<br>Siebel Table Extension (Siebel)<br>Siebel Table External<br>Siebel Table External View<br>Siebel Table Interface<br>Siebel Table Log<br>Siebel Table Repository<br>Siebel Table Virtual Table<br>Siebel Table Warehouse<br>Siebel Table Unkown Type |

Web Template

| Icon | Metamodel description |
|---|---|
| ![](../images/557023507.png) | Siebel Web Template - Applet Template |
| ![](../images/557023506.png) | Siebel Web Template - Applet Template - Chart<br>Siebel Web Template - Applet Template - Form<br>Siebel Web Template - Applet Template - Grid Layout<br>Siebel Web Template - Applet Template - List<br>Siebel Web Template - Applet Template - Specialized<br>Siebel Web Template - Applet Template - Tree<br>Siebel Web Template - View Template<br>Siebel Web Template - Web Page Template |

Class

| Icon | Metamodel description |
|---|---|
| ![](../images/557023505.png) | Siebel Class - ActiveX Control<br>Siebel Class - Applet<br>Siebel Class - Business Component<br>Siebel Class - Document<br>Siebel Class - Java Applet<br>Siebel Class - Report<br>Siebel Class - Search<br>Siebel Class - Service |

Misc

| Icon | Metamodel description |
|---|---|
| ![](../images/557023510.png) | Siebel Link |
| ![](../images/557023509.png) | Siebel Picklist |
| ![](../images/557023508.png) | Siebel Web Page |

### Structural rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 5.3.2 | [https://technologies.castsoftware.com/rules?sec=srs_siebel&ref=\|\|5.3.2](https://technologies.castsoftware.com/rules?sec=srs_siebel&ref=%7C%7C5.3.2) |
| 5.3.1 | [https://technologies.castsoftware.com/rules?sec=srs_siebel&ref=\|\|5.3.1](https://technologies.castsoftware.com/rules?sec=srs_siebel&ref=%7C%7C5.3.1) |
| 5.3.0 | [https://technologies.castsoftware.com/rules?sec=srs_siebel&ref=\|\|5.3.0](https://technologies.castsoftware.com/rules?sec=srs_siebel&ref=%7C%7C5.3.0) |

You can find a global list
here: [https://technologies.castsoftware.com/rules?sec=t_1520000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1520000&ref=%7C%7C)

## Known issues/limitations

The following section lists all known issues/limitations in this release
of the extension.

### Unable to establish a connection while performing the database repository extraction

#### Situation

When attempting to extract the Siebel database repositories (Vanilla or
Project), when using:

-   The standalone [CAST Database
    Extractor](https://doc.castsoftware.com/display/DOCCOM/CAST+Database+Extractor)
    using the Siebel-Extract-CLI.bat batch file
-   The CAST Delivery Manager Tool (DMT) launched via the CAST
    Management Studio

Note that the exact situation in which the issue occurs is currently
unknown.

#### Symptoms

The following error is logged, stopping the extraction process:

    Unable to establish a connection to: jdbc:oracle:thin:@<ip>:1521:<instance> => The Network Adapter could not establish the connection

#### Workaround

The workaround to this issue involves manually adding a specific Java
runtime option (-Djava.net.preferIPv4Stack=true) and then attempting
to rerun the database extraction:

##### Standalone CAST Database Extractor using the Siebel-Extract-CLI.bat batch file

If you are using the standalone [CAST Database
Extractor](http://doc.castsoftware.com/display/DOCCOM/CAST+Database+Extractor)
with the Siebel-Extract-CLI.bat batch file to perform the database
extraction, you must add the specific Java option to the batch file
at line 163, immediately after %JAVA_EXE%:

![](../images/557023521.jpg)

##### CAST Delivery Manager Tool (DMT) launched via the CAST Management Studio

If you are using the CAST Delivery Manager Tool (DMT) launched via the
CAST Management Studio to perform the database extraction, you must add
the specific Java option to the shortcut that runs the CAST
Management Studio executable on your workstation. Add the option to the
Target field, immediately after CAST-MS.exe" :
-Djava.net.preferIPv4Stack=true

![](../images/557023522.jpg)

Note that you will need to close the CAST Management Studio and re-open
it if it was running when you made the change to the shortcut.

### Error when displaying source code in the CAST dashboards

Most Siebel objects don't have associated source code as they correspond
to a configuration (example: an applet). In the CAST dashboards an error
will therefore be displayed when trying to display the object source
code (example error message from the legacy CAST Engineering Dashboard):

### No bookmarks are displayed for violations in source code in CAST dashboards

When using the CAST dashboards, no "bookmarks" will be displayed to
indicate where violations occur in source code. This is a limitation of
the extension.

## Miscellaneous information

This section explains various miscellaneous information that is
important to understand.

### Running a CAST AIP upgrade or an Extension upgrade

This note is valid in the following situations:

-   Running a CAST AIP upgrade (major/minor or Service Pack)
-   Running an Extension upgrade (not limited to an upgrade of the
    Siebel extension)
-   Using the Component Update/Component Reinstall options in CAST
    Server Manager

When any of the above actions are performed, the Siebel custom tables
located in the VANILLA_SCHEMA will be recreated and all data will be
overwritten and lost. In this situation, you MUST use the Run Analysis
only option in the CAST Management Studio (on the Vanilla application)
to re-populate the custom tables (used for the discrimination step
during the Project snapshot generation) before you run the Project
snapshot.
