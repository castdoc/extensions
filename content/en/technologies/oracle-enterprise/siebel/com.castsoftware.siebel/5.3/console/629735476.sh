#!/bin/bash

# PLEASE Update all 14 <PARAM> from this batch with your parameters
# PLEASE check the 2 <CHOOSE>
 
EXECUTION_FOLDER=`pwd`

# ****************************************************************************
# Platform parameters
# ****************************************************************************
# By default, you can use the execution folder (${EXECUTION_FOLDER}) for the Log folder and the 01_Results folder.
# If you want to use a specific location, enter a path like "~/data/test/" (please do not forget double quotes) 
ROOT_FOLDER=${EXECUTION_FOLDER}

if [[ -n $(type -p java) ]]
then
    echo Found java executable in PATH
    java_cmd=java
elif [[ (-n "$JAVA_HOME") &&  (-f "$JAVA_HOME/bin/java") ]]
then
    echo Found java executable in JAVA_HOME
    java_cmd="$JAVA_HOME/bin/java"  
else
    echo "JAVA_HOME is not defined. Using the local setting in the batch."
    java_cmd=/etc/alternatives/java_sdk_1.8.0/bin/java
    if [! -f "${java_cmd}"]; then
        END_WITH_ERROR="Bad Java Location. Check parameter JAVA_HOME."
        echo "Error: ${END_WITH_ERROR}"
	exit 1
    fi
fi

# path of the CASTDBGUI.jar file
# extractor can be downloaded from https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extractor.sqldatabase&version=latest
# for ex: "~/com.castsoftware.aip.extractor.sqldatabase.2.3.33-funcrel/CASTDBGUI.jar" (please do not forget double quotes)
SQLExtractor=/var/lib/CAST/Extensions/com.castsoftware.aip.extractor.sqldatabase.2.3.42-funcrel/CASTDBGUI.jar

# ****************************************************************************
# Database parameters = Access to the database hosting the Siebel repository
# ****************************************************************************
# for Oracle : oracle
# for SQL Server : mssql
# for DB2 : db2
DBTYPE=oracle

# either a host or an IP
SERVER_NAME=qa_o11g_part9  
PORTNUMBER=1521
DATABASE_NAME=ORA11GR2

# URL for Oracle = jdbc:oracle:thin:@<SERVER_NAME or IP>:<PORTNUMBER>:<SID>
JDBC_URL_ORACLE_SID=jdbc:oracle:thin:@${SERVER_NAME}:${PORTNUMBER}:${DATABASE_NAME}
# URL for Oracle = jdbc:oracle:thin:@<SERVER_NAME or IP>:<PORTNUMBER>/<SERVICE>
JDBC_URL_ORACLE_SERVICE=jdbc:oracle:thin:@${SERVER_NAME}:${PORTNUMBER}/${DATABASE_NAME}

JDBC_URL_SQLSERVER_DEFAULT=jdbc:jtds:sqlserver://${SERVER_NAME}:${PORTNUMBER}
JDBC_URL_SQLSERVER_INSTANCE=jdbc:jtds:sqlserver://${SERVER_NAME};instance=${DATABASE_NAME}:${PORTNUMBER}

# <CHOOSE> ${JDBC_URL_ORACLE_SID} or ${JDBC_URL_ORACLE_SERVICE}
JDBC_URL_ORACLE=${JDBC_URL_ORACLE_SID}

# <CHOOSE> ${JDBC_URL_SQLSERVER_DEFAULT} or ${JDBC_URL_SQLSERVER_INSTANCE}
JDBC_URL_SQLSERVER=${JDBC_URL_SQLSERVER_DEFAULT}

# user used for the SQL connection
# SET /p DBUSER="CONNECTION USER: "
DBUSER=system
# SET /p DBPWD="CONNECTION PASSWORD: "
DBPWD=cast

# ****************************************************************************
# Siebel parameters = Access to the Siebel repository
# ****************************************************************************
# user that contains the tables
SCHEMA=AMADEUS

# Repository type = Vanilla or Project
PROJECT_NAME=Vanilla

# Repository ID
REPOSITORY_ID=1-2096-1

# Repository version. Either 7.5, 7.6, 7.7, 7.8, 8.0, 8.1
REPOSITORY_VERSION=7.8

# List of application names separated by a comma
APPLICATION_LIST="Siebel CRA"


# ******************************************************************
# Step1: Checks
# ******************************************************************
echo "Step1: Checks..."

PARAM_LOG_FOLDER=${ROOT_FOLDER}/Log/
TARGET_FOLDER=${ROOT_FOLDER}/01_Results/

mkdir -p "${PARAM_LOG_FOLDER}"
if [ $? -ne 0 ] ; then
   echo "Unable to create log folder ${PARAM_LOG_FOLDER}. Fix the configuration."
   exit 1
fi

# Init log
EXECUTION_LOG_FILE=${PARAM_LOG_FOLDER}${PROJECT_NAME}.log

if [ -f "${EXECUTION_LOG_FILE}" ] ; then
    rm -f "${EXECUTION_LOG_FILE}"
    if [ $? -ne 0 ] ; then
        echo "Error while deleting the existing log file ${EXECUTION_LOG_FILE}. Fix the configuration."
        exit 1
    fi
fi

echo Step1... >> ${EXECUTION_LOG_FILE}

# Init target extraction folder
mkdir -p "${TARGET_FOLDER}"
if [ $? -ne 0 ] ; then
    END_WITH_ERROR="Unable to create target extraction folder ${TARGET_FOLDER}"
    echo "Error: ${END_WITH_ERROR}"
fi

if [ -z ${END_WITH_ERROR}]; then
    if (
        shopt -s nocasematch;
        [[ "${PROJECT_NAME}" =~ ^(Project|Vanilla)$ ]]
        ); then
        echo "Project Name: ${PROJECT_NAME}"
    else
        END_WITH_ERROR="Parameter PROJECT_NAME must be either Vanilla or Project."
        echo "Error : ${END_WITH_ERROR}"
    fi
fi


# ******************************************************************
if [ "${DBTYPE}" == "oracle" ]; then
    JDBC_DRIVER=oracle.jdbc.OracleDriver
    JDBC_URL=${JDBC_URL_ORACLE}
    CONFIG_FILE=${EXECUTION_FOLDER}/CASTSiebel_orcl.config
    CONFIG_FILE_PATH=TOOLS/Plugins/dmtcastsiebeloracleextractor-1.1.1
elif [ "${DBTYPE%}" == "mssql" ]; then
    JDBC_DRIVER=net.sourceforge.jtds.jdbc.Driver
    JDBC_URL=${JDBC_URL_SQLSERVER}
    CONFIG_FILE=${EXECUTION_FOLDER}/CASTSiebel_mssql.config
    CONFIG_FILE_PATH=TOOLS/Plugins/dmtcastsiebelmssqlextractor-1.0.0
elif [ "${DBTYPE}" == "db2" ]; then
    JDBC_DRIVER=com.ibm.db2.jcc.DB2Driver
    # URL for DB2 = jdbc:db2://<SERVER_NAME or IP>:<PORTNUMBER>/<DATABASE_NAME>
    JDBC_URL=jdbc:db2://${SERVER_NAME}:${PORTNUMBER}/${DATABASE_NAME}
    CONFIG_FILE=${EXECUTION_FOLDER}i/CASTSiebel_db2.config
    CONFIG_FILE_PATH=TOOLS/Plugins/dmtcastsiebeldb2extractor-1.1.0
else
    END_WITH_ERROR="Parameter DBTYPE is invalid : ${DBTYPE}. It should be either 'oracle', 'mssql' or 'db2'"
    echo "Error : ${END_WITH_ERROR}"
fi

if [[( -z ${END_WITH_ERROR}) && (! -f "${CONFIG_FILE}") ]] ; then
  END_WITH_ERROR="Config file is missing: [${CONFIG_FILE}]. Copy it from [${CONFIG_FILE_PATH}]"
  echo "Error: ${END_WITH_ERROR}"
fi

EXTRACTOR_PARAMETERS="idProject=${PROJECT_NAME};repositorySchema=${SCHEMA};repositoryID=${REPOSITORY_ID};schema=REPOSITORY,${APPLICATION_LIST}"


# ******************************************************************
# Technical space where to generate files =
# outputs of 01_SourceExtraction used by 02_SourcePreparation
# ******************************************************************

TARGET_FILE=${TARGET_FOLDER}/SiebelExtract_${PROJECT_NAME}
EXTRACTOR_ERRORFILE=${PARAM_LOG_FOLDER}/ExtractorLog_${PROJECT_NAME}.log

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# Step2: extract data from Siebel repository
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

if [ -z ${END_WITH_ERROR}]; then
    echo Step2: extract data from Siebel repository...                          
    echo Step2: extract data from Siebel repository... >> ${EXECUTION_LOG_FILE}

    PARAMETER_FILE=${EXECUTION_FOLDER}/parameters.txt
    echo vendor=Siebel > ${PARAMETER_FILE}
    echo platform=${REPOSITORY_VERSION} >> ${PARAMETER_FILE}
    echo driver=${JDBC_DRIVER} >> ${PARAMETER_FILE}
    echo url=${JDBC_URL} >> ${PARAMETER_FILE}
    echo user=${DBUSER} >> ${PARAMETER_FILE}
    echo password=${DBPWD} >> ${PARAMETER_FILE}
    echo format=text >> ${PARAMETER_FILE}
    echo l=${EXTRACTOR_ERRORFILE} >> ${PARAMETER_FILE}
    echo target=${TARGET_FILE} >> ${PARAMETER_FILE}
    echo config=${CONFIG_FILE} >> ${PARAMETER_FILE}
    echo zip=true >> ${PARAMETER_FILE}
    echo multithreaded=true >> ${PARAMETER_FILE}
    echo "parameters=${EXTRACTOR_PARAMETERS}" >> ${PARAMETER_FILE}

    # This step must be done on a server that have access to the Siebel database repository 
    "${java_cmd}" -Xmx1200M -cp "${SQLExtractor}" com.castsoftware.extractor.cli.DatabaseExtractor \@"${PARAMETER_FILE}"

    errorLevel=$?
    # Validation of the parameters - exit with code -1
    if [ $errorLevel -eq -1 ] ; then
        END_WITH_ERROR="Invalid parameters. Check log file ${EXECUTION_LOG_FILE}"
        echo "Error : ${END_WITH_ERROR}" 
    elif [ $errorLevel -eq 1000 ] ; then
    # when help is explicitly asked (-h option) or when a mandatory option is missing
        END_WITH_ERROR="Missing configuration file ${CONFIG_FILE}"
        echo "Error : ${END_WITH_ERROR}" 
    elif [ $errorLevel -eq 1001 ] ; then
    # undefined functional error (check the log file or the output console)
        END_WITH_ERROR="Check the log file ${EXECUTION_LOG_FILE}."
        echo "Error : ${END_WITH_ERROR}" 
    elif [ $errorLevel -eq 2000 ] ; then
    # connection lost, ...
        END_WITH_ERROR="Unable to establish a connection. Check the log file ${EXECUTION_LOG_FILE}"
        echo "Error : ${END_WITH_ERROR}" 
    elif [ $errorLevel -eq 2001 ] ; then
        END_WITH_ERROR="Error during the extraction. Check the log file ${EXECUTION_LOG_FILE}"
        echo "Error : ${END_WITH_ERROR}" 
    elif [ $errorLevel -lt 0 ] ; then
    # Current implementation does not stop the extraction
    # However, instead of returning 0, it returns the number of failed mandatory type extractions
    # So that, in the GUI, it appears as a failed extraction. - exit with code 1
        END_WITH_ERROR="Help is activated or a mandatory option is missing"
        echo "Error : ${END_WITH_ERROR}" 
    elif [ $errorLevel -gt 0 ] ; then
        END_WITH_ERROR="Java error. Contact CAST Support"
        echo "Error : ${END_WITH_ERROR}" 
    fi
fi

if [ -z "${END_WITH_ERROR}" ]; then
    echo
    echo >> ${EXECUTION_LOG_FILE}
    echo "-------------------------------------------------------------------------------"
    echo "-------------------------------------------------------------------------------" >> ${EXECUTION_LOG_FILE}
    echo "Extraction was successful !"
    echo "Extraction was successful !" >> ${EXECUTION_LOG_FILE}
    echo "-------------------------------------------------------------------------------"
    echo "-------------------------------------------------------------------------------" >> ${EXECUTION_LOG_FILE}
    echo
else
    echo
    echo >> ${EXECUTION_LOG_FILE}
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> ${EXECUTION_LOG_FILE}
    echo "Error while extracting Siebel : ${END_WITH_ERROR}" 
    echo "Error while extracting Siebel : ${END_WITH_ERROR}" >> ${EXECUTION_LOG_FILE}
    echo "Extraction log : ${EXTRACTOR_ERRORFILE}"
    echo "Extraction log : ${EXTRACTOR_ERRORFILE}" >> ${EXECUTION_LOG_FILE}
    echo "- Please check the logs, fix the configuration and retry the extraction"
    echo "- Please check the logs, fix the configuration and retry the extraction"  >> ${EXECUTION_LOG_FILE}
    echo "- If the issue is not a configuration issue, please contact the CAST Support"
    echo "- If the issue is not a configuration issue, please contact the CAST Support" >> ${EXECUTION_LOG_FILE}
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" >> ${EXECUTION_LOG_FILE}
    echo ""
fi
