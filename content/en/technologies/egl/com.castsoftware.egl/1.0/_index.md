---
title: "EGL - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.egl

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications written using EGL
languages.

{{% alert color="info" %}}Although this extension is officially supported by CAST, please note
that it has been developed within the technical constraints of the CAST
Universal Analyzer technology and to some extent adapted to meet
specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other analyzers.{{% /alert %}}

## In what situation should you install this extension?

If your application contains source code written using EGL and you want
to view these object types and their links with other objects, then you
should install this extension.

## Supported EGL versions

This version of the extension provides support for:

| Version                             | Supported          |
|-------------------------------------|:------------------:|
| Rational Business Developer V7 R5.1 | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Supported file types

Only files with following extensions will be analyzed:

- `*.egl`
- `*.egldd`
- `*.eglbld`
- `*.project`

## Download and installation instructions

The extension will be automatically downloaded and installed when at least one of the supported file types is delivered for analysis.

## Source code discovery

A discoverer is provided with the extension to automatically detect EGL
code: when one of the supported file types is found in a folder, one
"EGL" project will be discovered, resulting in a corresponding
Analysis Unit.

## EGL preprocessor

EGL source code needs to be preprocessed so that CAST can understand it
and analyze it correctly. This code preprocessing is actioned
automatically when an analysis is launched or a snapshot is generated
(the code is preprocessed before the analysis starts). EGL Preprocessor
log file is stored in the following location:

``` text
%PROGRAMDATA%\CAST\CAST\Logs\<application_name\Execute_Analysis_<guid>\com.castsoftware.egl.<_extension_version>.prepro_YYYYMMDDHHMMSS.log
```

## What results can you expect?

### Objects

| Icon Image | ID | Description | Concept |
|:---:|---|---|---|
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageOrange.svg) | EGLPackage | EGL Package |  |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FolderDarkBlue.svg) | EGLProjectDeclaration | EGL Project Declaration |  |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg) | EGLRecord | EGL Record |  |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataBlock.svg) | EGLRecordColumn | EGL Record Column |  |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FolderRed.svg) | EGLRecordDeclaration | EGL Record Declaration |  |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FolderLightBlue.svg) | EGLReferencedProjects | EGL Referenced Projects |  |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Module.svg) | EGLProgram | EGL Program | Class |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg) | EGLFunction | EGL Function | Function |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Table.svg) | EGLDataTable | EGL Data Table | Table |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Form.svg) | EGLForm | EGL Form | UI |
| ![icon](https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Form.svg) | EGLFormGroup | EGL Form Group | UI |

### Structural Rules

The following structural rules are provided:

| Release | URL |
|---------|:---:|
| 1.0.7   | [https://technologies.castsoftware.com/rules?sec=srs_egl&ref=\|\|1.0.7](https://technologies.castsoftware.com/rules?sec=srs_egl&ref=%7C%7C1.0.7) |
| 1.0.6   | [https://technologies.castsoftware.com/rules?sec=srs_egl&ref=\|\|1.0.6](https://technologies.castsoftware.com/rules?sec=srs_egl&ref=%7C%7C1.0.6) |
| 1.0.5   | [https://technologies.castsoftware.com/rules?sec=srs_egl&ref=\|\|1.0.5](https://technologies.castsoftware.com/rules?sec=srs_egl&ref=%7C%7C1.0.5) |
| 1.0.4   | [https://technologies.castsoftware.com/rules?sec=srs_egl&ref=\|\|1.0.4](https://technologies.castsoftware.com/rules?sec=srs_egl&ref=%7C%7C1.0.4) |
| 1.0.3   | [https://technologies.castsoftware.com/rules?sec=srs_egl&ref=\|\|1.0.3](https://technologies.castsoftware.com/rules?sec=srs_egl&ref=%7C%7C1.0.3) |
| 1.0.2   | [https://technologies.castsoftware.com/rules?sec=srs_egl&ref=\|\|1.0.2](https://technologies.castsoftware.com/rules?sec=srs_egl&ref=%7C%7C1.0.2) |

You can also find a global list here: [https://technologies.castsoftware.com/rules?sec=t_1015000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1015000&ref=%7C%7C)
