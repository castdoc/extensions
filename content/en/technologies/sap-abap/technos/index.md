---
title: "Covered Technologies"
linkTitle: "Covered Technologies"
type: "docs"
weight: 3
---

## Language

| Icon                                                                                                                                                  | Name        | Versions   | Quality                                                                                | Extension                                                                               |
|:------------------------------------------------------------------------------------------------------------------------------------------------------|:------------|:-----------|:---------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/abap/abap-original.svg" alt="icon" style="width: 32px; height: 32px;"> | ABAP        | 4.6 - 7.5  | ISO 5055 Security, ISO 5055 Efficiency, ISO 5055 Reliability, ISO 5055 Maintainability | core, [com.castsoftware.sap](../sap/extensions/com.castsoftware.sap/)                   |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/abap/abap-original.svg" alt="icon" style="width: 32px; height: 32px;"> | BAPI        | 4.6 - 7.5  | ISO 5055 Security, ISO 5055 Efficiency, ISO 5055 Reliability, ISO 5055 Maintainability | core, [com.castsoftware.sap](../sap/extensions/com.castsoftware.sap/)                   |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/abap/abap-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Web Dynpro  | 4.6 - 7.6  |                                                                                        | core, [com.castsoftware.sap](../sap/extensions/com.castsoftware.sap/)                   |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/abap/abap-original.svg" alt="icon" style="width: 32px; height: 32px;"> | ABAP Script | 4.6 - 7.5  |                                                                                        | [com.castsoftware.sqlanalyzer](../sqlanalyzer/extensions/com.castsoftware.sqlanalyzer/) |

## Communication

| Icon                                                                                                                                                                                  | Name       | Versions   | Quality   | Extension                                                                            |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------|:-----------|:----------|:-------------------------------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/defaultjavaframework/defaultjavaframework-original.svg" alt="icon" style="width: 32px; height: 32px;"> | SAP Hybris |            |           | [com.castsoftware.sap.hybris](../sap.hybris/extensions/com.castsoftware.sap.hybris/) |

## Products

The ABAP Analyzer supports all SAP ABAP products, running with SAP modules Basis and ABA of NetWeaver v7.

**Supported SAP products:**
- ECC5, ECC6, S/4HANA
- CRM v7
- SRM v7
- SCM v7
- ISU v7

**Unsupported SAP products:**
- CE
- XI/PI
- BW/BI
- Mobile

**Supported SAP technologies:**
- ABAP
- ABAP Object
- BAPI
- Web Dynpro for ABAP

**Unsupported SAP technologies:**
- BSP
- iDoc
- SAPScript
- Search Help
- Smart Forms
- Workflow
- Web Dynpro for Java
