---
title: "SAP ABAP Analyzer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.sap

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for SAP ABAP. If your application
contains SAP ABAP source code and you want to view these object
types and their links with other objects, then you should install this
extension.

## Technical information

When installed, this extension replaces the SAP ABAP Analyzer
embedded in CAST Imaging Core:

-   The SAP ABAP Analyzer embedded in AIP Core will continue to exist
    and will be shipped "out of the box" with CAST Imaging Core,
-   Critical bugs will continue to be fixed in the Mainframe Analyzer
    embedded in CAST Imaging Core but no new features or functionality
    will be added.
-   5 new quality rules will be added in the first release of the
    "standalone" SAP ABAP Analyzer extension, but otherwise the
    extension will have the same features and functionality on release
    as the SAP ABAP Analyzer embedded in CAST Imaging Core.
-   The SAP ABAP Analyzer is compatible with AIP Core ≥ 8.3.43.
-   All future development of the SAP ABAP Analyzer (bug fixes, new
    features, functionality etc.) will be completed in the SAP ABAP
    Analyzer extension only.
-   The behaviour is as follows:
    -   Nothing is automatic - for both CAST Console and "legacy" CAST
        deployments, the SAP ABAP Analyzer extension must be manually
        downloaded and installed in order to use it
    -   If the standalone extension is installed, CAST Console will
        automatically detect that it exists and will use the extension
        rather than the analyzer embedded in CAST Imaging Core.
    -   Once the extension has been installed and used to produce
        analysis results, it is not possible to reverse this choice by
        removing the extension and re-analyzing the source code again.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :<white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Requirements

See [Requirements](../../../requirements/).

## Dependencies with other extensions

None.

## Download and installation instructions

The extension is not downloaded and installed automatically.

## Prepare and deliver the source code

There are two available methods for collecting source code for the SAP
ABAP Analyzer:

-   Use
    the [com.castsoftware.aip.extractor.sap](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extractor.sap&version=latest)
    to extract the source code directly from your SAP instance and into
    a format that can be packaged for delivery to CAST Console.
-   If you are storing your source code in
    [abapGit](https://abapgit.org/) and you are
    using the SAP ABAP Analyzer ≥ 1.0.8-funcrel, you can package the
    source code directly from this source code repository. The following
    abapGit file storage types are supported and the resulting objects
    are identical to those generated from a
    [com.castsoftware.aip.extractor.sap](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extractor.sap&version=latest) extraction:

| Name | Programming Language | Supported | Description |
| --- | -------------------- | --------- | ----------- |
| *.abap | ABAP | True | objects, programs, classes |
| *.sap.xml | ABAP | True | SAP Tables, transactions, views, CDS |
| *.tabl.xml | ABAP | True | SAP Tables |
| *.tran.xml | ABAP | True | SAP transactions |
| *.view.xml | ABAP | True | SAP views |
| *.wdya.xml | ABAP | True | web dynpros |
| *.wdyn.xml | ABAP | True | web dynpros |
| package.devc.xml | ABAP | True | packages |
| *.asddls | ABAP CDS | True | CDS |
| *.ddls.asddls | ABAP CDS | True | CDS |

### Information about discovery

Discovery is a process that is actioned during the delivery process.
CAST will attempt to automatically identify "projects" within your
application using a set of predefined rules. Discoverers are
currently embedded in CAST Imaging Core: [SAP ABAP
Discoverer](SAP_ABAP_Discoverer). You should read the relevant
documentation for each discoverer (provided in the link above) to
understand how the source code will be handled.

### Source code delivery using CAST Imaging Console

CAST Imaging Console expects either a ZIP/archive file or source
code located in a folder configured in CAST Console. You should
include in the ZIP/source code folder all extracted SAP/ABAP source
code:

-   results of a [com.castsoftware.aip.extractor.sap](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extractor.sap&version=latest) extraction
-   directly from an [abapGit](https://doc.castsoftware.com/export/DOCCOM/CAST+SAP+Extractor+NG)
    repository (supported for analysis with the SAP ABAP Analyzer ≥
    1.0.8-funcrel)

CAST highly recommends placing the files in a folder dedicated to
SAP/ABAP. If you are using a ZIP/archive file, zip the folders in the
"temp" folder - but do not zip the "temp" folder itself, nor create any
intermediary folders:

``` java
D:\temp
    |-----SAP/ABAP
    |-----OtherTechno1
    |-----OtherTechno2
```

### .SQLTABLESIZE files

XXL table rules (see XXL and XXS tables rules enablement) are
performance related rules that help detect incorrect or poorly
performing SQL queries running on XXL tables. XXL tables can be defined
as extremely large tables, containing a large amount of data. The goal
is to use table size from production systems because development /
integration systems may not feature really large tables and would not
help detect real threat on application performance levels.

Note that:

-   The com.castsoftware.aip.extractor.sap will automatically generate
    the .SQLTABLESIZE file when you extract the SAP/ABAP source code.
-   You will need to manually create this file if you are packaging code
    directly from an
    [abapGit](https://doc.castsoftware.com/export/DOCCOM/CAST+SAP+Extractor+NG)
    repository.

You should ensure that you deliver the .SQLTABLESIZE file with your
extracted SAP table code.

## What analysis results can you expect?

### Objects

#### Main
| Icon | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Alias.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapAlias | ABAP Alias |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapBookmarkFile | ABAP File used for bookmark (invisible) |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Frame.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapFrame | ABAP Frame |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Class.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapClass | ABAP Class | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Frame.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapInterface | ABAP Interface | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Constructor.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapConstructor | ABAP Constructor | Class Constructor |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Application_SAP.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapBadi | ABAP BADI | Class Grouping |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Class.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapClassPool | ABAP Class Pool | Class Grouping |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Frame.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapInterfacePool | ABAP Interface Pool | Class Grouping |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapModulePool | ABAP Module Pool | Class Grouping |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/EventHandler.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapEvent | ABAP Event | Event Handling |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/EventHandler.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapEventBlock | ABAP Event Block | Event Handling |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapFlowLogicFile | ABAP Flow Logic File | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Namespace.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapInclude | ABAP Include | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapTypePool | ABAP Type Pool | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapUserExit | ABAP User-Exit | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapEventMethod | ABAP Event Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Form.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapForm | ABAP Form | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapFunction | ABAP Function | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapMethod | ABAP Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapModule | ABAP Module | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageGrey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapUnresolvedObject | ABAP Unresolved Object | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapFunctionPool | ABAP Function Pool | Function Grouping |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Macro.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapMacro | ABAP Macro | Macro |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Application_SAP.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapProgram | ABAP Program | Program |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FolderGrey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapUnresolvedFolder | ABAP Unresolved Folder | Project |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UIView.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapProcessingScreen | ABAP Processing Screen | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UIView.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | abapSelectionScreen | ABAP Selection Screen | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Synonym.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | AbapMember | ABAP Member | Variable |


>(\*RFC): When using the [com.castsoftware.aip.extractor.sap](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.extractor.sap&version=latest) (v. ≥ 8.2.0), the remote attribute is managed at ABAP function level. Please see [Can the extractor extract remote function modules?](https://doc.castsoftware.com/export/DOCCOM/CAST+SAP+Extractor+NG+-+FAQ#CASTSAPExtractorNGFAQ-remote) for more details.

#### SAP objects

| Icon | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FolderGrey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SAPUnresolvedFolder | SAP Unresolved Folder | Project |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/CursorBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SAPTransaction | SAP Transaction | Transaction |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SAPTransactionFile | SAP Transaction File | Transaction |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UIView.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SAPView | SAP View | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SAPCDSView(**) | SAP CDS View | View |

> **: SAP CDS View <ul><li>Analyzed in the SAP Analyzer embedded in AIP Core 8.3.38 - 8.3.43 and all SAP Analyzer extension releases.</li><li>In SAP Analyzer extension ≥ 1.0.6-funcrel, only CDS Views starting with /, Y or Z are analyzed. All other CDS Views are ignored.</li></ul>


#### SAP OpenSQL Objects

| Icon | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Index.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ABAP_SAPTableIndex | SAP table index | Index |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ForeignKey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ABAP_SAPTableForeignKey | SAP table foreign key | SQL Key |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ConstraintPrimary.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ABAP_SAPTablePrimaryKey | SAP table primary key | SQL Key |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UnresolvedTable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ABAP_SAPUnresolvedTable | SAP unresolved Table | Table |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Table.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | SAPTable | SAP Table | Table |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/TableColumn.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_ABAP_SAPTableColumn | SAP table column | Table Column |

>SAP Tables and SAP Views are only saved in the Analysis schema when they are called from the analysis source code.


#### BAPI objects

| Icon | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/EventHandler.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_BAPI_EVENT | SAP BAPI Event | Event Handling |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_BAPI_File | SAP BAPI File | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_BAPI | SAP BAPI | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_BAPI_METHOD | SAP BAPI Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UITextField.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_BAPI_KEY_FIELD | SAP BAPI Key Field | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PropertyOutlet.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_BAPI_ATTRIBUTE | SAP BAPI Attribute | Variable |


#### Web Dynpro for ABAP

| Icon | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_Application | R3 WebDynpro Application | Application |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/EventHandler.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_EventHandler | R3 WebDynpro Event Handler | Event Handling |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_File | SAP Web Dynpro File | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_Action | R3 WebDynpro Action | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_Method | R3 WebDynpro Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_SupplyFunction | R3 WebDynpro Supply Function | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Synonym.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_Attribute | R3 WebDynpro Attribute | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_Component | R3 WebDynpro Component | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionAction.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_ComponentController | R3 WebDynpro Component Controller | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlueService.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_InboundPlugin | SAP R3WDYN inbound plugin | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionAction.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_InterfaceController | R3 WebDynpro Interface Controller | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UIView.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_InterfaceView | R3 WebDynpro Interface View | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlueService.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_OutboundPlugin | SAP R3WDYN outbound plugin | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UIView.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_View | R3 WebDynpro View | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UIView.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SAP_R3WDYN_Window | R3 WebDynpro Window | UI |



### Links

<table>
<tbody>
<tr>
<th>Link type</th>
<th>When is this type of link created?</th>
<th>Example</th>
</tr>
&#10;<tr>
<td>ACCESS EXEC</td>
<td>This link is created when a piece of code calls
a method.</td>
<td><pre><code>CALL METHOD oref-&gt;method1...</code></pre></td>
</tr>
<tr>
<td>ACCESS MEMBER</td>
<td>This link is created when an object make an
access to a member of another object.</td>
<td><pre><code>METHOD mymethod.
   WRITE ref-&gt;attr
ENDMETHOD.</code></pre></td>
</tr>
<tr>
<td>ACCESS READ</td>
<td>This link is created when an object reads a
value of another object.</td>
<td><pre><code>METHOD mymethod.
   X = ref-&gt;attr
ENDMETHOD.</code></pre></td>
</tr>
<tr>
<td>ACCESS WRITE</td>
<td>This link is created when an object modifies a
value of another object.</td>
<td><pre><code>METHOD mymethod.
   ref-&gt;attr = 10
ENDMETHOD.</code></pre></td>
</tr>
<tr>
<td>BELONGS TO</td>
<td>This link is created when one object belongs to
another object.</td>
<td><pre><code>report abapReport
Function f
Endfunction</code></pre>
<p>(f belongs to abapReport).</p></td>
</tr>
<tr>
<td>CALL</td>
<td>This link is used to describe a function that
is called in a code block.</td>
<td><pre><code>Function f
Perform g
Endfunction</code></pre>
<p>(f calls g)</p></td>
</tr>
<tr>
<td>CALL</td>
<td>This link is created when a SAP transaction
calls another SAP transaction.</td>
<td>This information is extracted from the SAP
system.</td>
</tr>
<tr>
<td>CALL</td>
<td>This link is created when a SAP transaction
calls the processing screen or the selection screen of an ABAP
program.</td>
<td>This information is extracted from the SAP
system.</td>
</tr>
<tr>
<td>CALL</td>
<td>This link is created when an ABAP program calls
a SAP transaction.</td>
<td><pre><code>CALL TRANSACTION SA10 AND SKIP FIRST SCREEN.
...
LEAVE TO TRANSACTION SA10.</code></pre></td>
</tr>
<tr>
<td>CALL</td>
<td>This link is created when an event block of a
screen calls an ABAP module.</td>
<td><pre><code>PROCESS BEFORE OUTPUT.
MODULE STATUS_0100.
*
PROCESS AFTER INPUT.
CHAIN.
FIELD: SDYN_CONN-CITY, SDYN_CONN-COUNTRY,
SDYN_CONN-CARID, SDYN_CONN-MARK.
MODULE USER_COMMAND.
ENDCHAIN.</code></pre></td>
</tr>
<tr>
<td>CALL</td>
<td>This link is created when an ABAP component
calls a screen.</td>
<td>This information is extracted from the SAP
system.</td>
</tr>
<tr>
<td>INCLUDE</td>
<td>This link is used when a file includes another
file.</td>
<td><pre><code>Report abapReport
Include include_name</code></pre></td>
</tr>
<tr>
<td>INHERIT EXTEND</td>
<td>This link is created from a class A to a class
B if A inherits B.</td>
<td><pre><code>CLASS subclass DEFINITION
   INHERITING FROM superclass.
ENDCLASS.</code></pre></td>
</tr>
<tr>
<td>INHERIT OVERRIDE</td>
<td>If a method of a class overrides a method
belonging to a parent class, then this link is created between the two
methods.</td>
<td><pre><code>METHODS mymethod REDEFINITION.</code></pre></td>
</tr>
<tr>
<td>INHERIT IMPLEMENT</td>
<td>This link is created from a class to an
interface when the class implements this interface.</td>
<td><pre><code>CLASS myclass DEFINITION.
PUBLIC SECTION.
INTERFACES: interf1, interf2 ... 
ENDCLASS.</code></pre></td>
</tr>
<tr>
<td>INHERIT IMPLEMENT</td>
<td>If a class implements an interface, this link
is also created between the method belonging to the class and the
corresponding method belonging to the interface.</td>
<td><pre><code>INTTERFACE status.
METHODS write.
ENDINTERFACE.
&#10;CLASS counter DEFINITION.
PUBLIC SECTION.
INTERFACE status.
...
ENDCLASS.
&#10;CLASS counter IMPLEMENTATION.
METHOD status~write.
...
ENDMETHOD.
...
ENDCLASS.</code></pre></td>
</tr>
<tr>
<td>RAISE</td>
<td>This link is created when a method activates an
event.</td>
<td><pre><code>RAISE EVENT evt EXPORTING ...</code></pre></td>
</tr>
<tr>
<td>RELY ON</td>
<td>This link is created when a method is a handler
for an event.</td>
<td><pre><code>METHODS handler FOR EVENT evt OF {class|interf}
IMPORTING ... ei ...[sender].</code></pre></td>
</tr>
<tr>
<td>USE</td>
<td>This link is created when a processing screen
or a selection screen is used in conjuncton with an ABAP program.</td>
<td>This information is extracted from the SAP
system.</td>
</tr>
<tr>
<td>USE</td>
<td>This link is created when a SAP transaction
calls an ABAP program.</td>
<td>This information is extracted from the SAP
system.</td>
</tr>
<tr>
<td>USE</td>
<td>This link is created when an ABAP component
calls a screen.</td>
<td>This information is extracted from the SAP
system.</td>
</tr>
<tr>
<td>USE</td>
<td>This link is created when a processing screen
or a selection screen is used in conjuncton with an ABAP program.</td>
<td>This information is extracted from the SAP
system.</td>
</tr>
<tr>
<td>USE SELECT</td>
<td>This link is used when a code block contains an
SQL request with a select statement (in the case of embedded SQL or
native SQL).</td>
<td><pre><code>Report abapReport
Select col from table</code></pre></td>
</tr>
<tr>
<td>USE UPDATE</td>
<td>This link is used when a code block contains an
SQL request with an update statement (in the case of embedded SQL or
native SQL).</td>
<td><pre><code>Report abapReport.
Update table set col=’a’</code></pre></td>
</tr>
<tr>
<td>USE DELETE</td>
<td>This link is used when a code block contains an
SQL request with a delete statement (in the case of embedded SQL or
native SQL).</td>
<td><pre><code>Report abapReport
Delete from table</code></pre></td>
</tr>
<tr>
<td>USE INSERT</td>
<td>This link is used when a code block contains an
SQL request with an insert (in the case of embedded SQL or native
SQL).</td>
<td><pre><code>Report abapReport
Insert into table values vals</code></pre></td>
</tr>
</tbody>
</table>

### Structural Rules

| Release | Link |
|---|---|
| 1.0.16-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.16-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.16-funcrel) |
| 1.0.15-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.15-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.15-funcrel) |
| 1.0.14-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.14-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.14-funcrel) |
| 1.0.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.13-funcrel) |
| 1.0.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.12-funcrel) |
| 1.0.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.11-funcrel) |
| 1.0.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.10-funcrel) |
| 1.0.9-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.9-funcrel)   |
| 1.0.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.8-funcrel)   |
| 1.0.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.7-funcrel)   |
| 1.0.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.6-funcrel)   |
| 1.0.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.5-funcrel)   |
| 1.0.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.4-funcrel)   |
| 1.0.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.3-funcrel)   |
| 1.0.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.2-funcrel)   |
| 1.0.1-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.1-funcrel)   |
| 1.0.0-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sap&ref=\|\|1.0.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sap&ref=%7C%7C1.0.0-funcrel)   |

A global list is also available
here: [https://technologies.castsoftware.com/rules?sec=t\_-15&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_-15&ref=%7C%7C).
