---
title: "SAP ABAP Discoverer"
linkTitle: "SAP ABAP Discoverer"
type: "docs"
no_list: false
---

The SAP ABAP Discoverer is not provided as an extension: instead it is embedded in CAST Imaging Core and is therefore present "out of the box". It configures one project for the highest level folder (within the root folder) containing files that use the .ABAP file extension. Starting ≥ 8.3.35, if multiple SAP extractions are delivered, one project will be created for each SAP extraction within the delivered source code.

> The following are discovered:
> - Root path
> - SAP Table extraction folder
> - No discovery scan will take place in sub-folders of a SAP Table extraction folder