---
title: "Log messages"
linkTitle: "Log messages"
type: "docs"
weight: 6
---

## ABAP.01

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Info</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Identifier</td>
<td class="confluenceTd">ABAP.01</td>
</tr>
<tr class="even">
<td class="confluenceTd">Message</td>
<td class="confluenceTd">Unresolved Reference to '&lt;CALLEE_NAME&gt;'
found in object '&lt;CALLER_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="confluenceTd">Explanation</td>
<td class="confluenceTd">The analyzer found a reference to an object
called &lt;CALLEE_NAME&gt; and was not able to find out that object in
the source code to be analyzed. </td>
</tr>
<tr class="odd">
<td class="confluenceTd">User Action</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Select the object &lt;CALLEE_NAME&gt; in the list of objects in
the Unresolved folder of the technical browser in CAST Enlighten. Check
the source code with the application team. If this is an application
specific component, then it is recommended to add it to the analysis
scope. Missing components could affect reference resolution and
potentially quality rules.</p>
<p><img src="270008719.png" class="image-left"
draggable="false" data-image-src="270008719.png"
data-unresolved-comment-count="0" data-linked-resource-id="270008719"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav3b84918abee80adf102b08048e50332c.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="270008717"
data-linked-resource-container-version="2" width="591"
height="338" /></p>
<p><br />
</p>
</div></td>
</tr>
</tbody>
</table>

## ABAP.02

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Info</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Identifier</td>
<td class="confluenceTd">ABAP.02</td>
</tr>
<tr class="even">
<td class="confluenceTd">Message</td>
<td class="confluenceTd">Unresolved Include '&lt;INCLUDE_NAME&gt;' found
in object '&lt;CALLER_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="confluenceTd">Explanation</td>
<td class="confluenceTd">The analyzer found a reference to an include
file called &lt;INCLUDE_NAME&gt; and was not able to find out the
corresponding file in the source code to be analyzed. </td>
</tr>
<tr class="odd">
<td class="confluenceTd">User Action</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Select the object &lt;INCLUDE_NAME&gt; in the list of objects in
the Unresolved folder of the technical browser in CAST Enlighten. Check
the source code with the application team. If this is an application
specific include, then it is recommended to add it to the analysis
scope. Missing components could affect reference resolution and
potentially quality rules.</p>
</div></td>
</tr>
</tbody>
</table>

## ABAP.03

|             |                                                                                                 |
|-------------|-------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                     |
| Identifier  | ABAP.03                                                                                         |
| Message     | Include resolution completed.                                                                   |
| Severity    | Information                                                                                     |
| Explanation | All the include file references have been processed. The analyzer will now start the analysis.  |
| User Action | Nothing special to do.                                                                          |

ABAP.04

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Info</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Identifier</td>
<td class="confluenceTd">ABAP.04</td>
</tr>
<tr class="even">
<td class="confluenceTd">Message</td>
<td class="confluenceTd">Unsupported syntax found at line &lt;LINE&gt;
and column &lt;COLUMN&gt; in file '&lt;FILENAME&gt;'
(&lt;CODE&gt;).</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="confluenceTd">Explanation</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>The analyzer is not able to parse the piece of ABAP source code
&lt;CODE&gt; correctly. The parsing of the source file &lt;FILENAME&gt;
will not complete and only the corresponding object will be created
without any properties and children objects.</p>
<p>Example:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="op">-</span> Unsupported syntax found at line <span class="dv">230</span> and column <span class="dv">28</span> in file </span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>  <span class="er">&#39;</span>C<span class="op">:</span>\ABAP\CASTPUBS\ZPACKAGES\ZPACKAGESZMMRST\ZMMZ00X4_MAT_CHNG_DEL_RPT<span class="op">.</span><span class="fu">ABAP_CAST</span><span class="er">&#39;</span> </span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="op">(</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a>SELECT changenr tabname tabkey fname chngind INTO TABLE icdocs</span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a>      FROM cdpos</span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a>      FOR ALL ENTRIES IN icdhdr</span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a>      WHERE objectclas <span class="op">=</span> <span class="er">&#39;</span>MATERIAL<span class="er">&#39;</span></span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a>        AND objectid   <span class="op">=</span> icdhdr<span class="op">-</span>objectid</span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a>        AND changenr   <span class="op">=</span> icdhdr<span class="op">-</span>changenr</span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a>        AND tabname    <span class="fu">in</span> <span class="op">(</span><span class="er">&#39;</span>MARA<span class="er">&#39;</span><span class="op">,</span></span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a>                           <span class="er">&#39;</span></span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a>        AND chngind    <span class="op">&lt;&gt;</span> <span class="ch">&#39;I&#39;</span><span class="op">.</span></span>
<span id="cb1-13"><a href="#cb1-13" aria-hidden="true" tabindex="-1"></a><span class="op">---------------------------^</span></span>
<span id="cb1-14"><a href="#cb1-14" aria-hidden="true" tabindex="-1"></a><span class="op">)</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">User Action</td>
<td class="confluenceTd">Identify with the application team whether it
is an incorrect syntax on the customer side (the source code should
compile correctly). If not, then contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Support</a>.</td>
</tr>
</tbody>
</table>

## ABAP.05

|             |                                                                                                                                                                                                                                                           |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info    | Explanation                                                                                                                                                                                                                                               |
| Identifier  | ABAP.05                                                                                                                                                                                                                                                   |
| Message     | Duplicated object '\<OBJECT_NAME\>' found in file '\<FILENAME\>'.                                                                                                                                                                                         |
| Severity    | Warning                                                                                                                                                                                                                                                   |
| Explanation | The analyzer found 2 objects with the name \<OBJECT_NAME\> and same type in the source file \<FILENAME\>. Only the first one is taken in account.                                                                                                         |
| User Action | Identify with the application team the ABAP element and check if the source code compile correctly. If yes, then contact [CAST Support](https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support). |

## ABAP.06

|             |                                                                          |
|-------------|--------------------------------------------------------------------------|
| Info        | Explanation                                                              |
| Identifier  | ABAP.06                                                                  |
| Message     | Starting macros resolution. This action may take a while...              |
| Severity    | Information                                                              |
| Explanation | The analyzer starts to process the macro directives in the source code.  |
| User Action | Nothing special to do.                                                   |

## ABAP.07

|             |                                                                         |
|-------------|-------------------------------------------------------------------------|
| Info        | Explanation                                                             |
| Identifier  | ABAP.07                                                                 |
| Message     | Macros resolution completed.                                            |
| Severity    | Information                                                             |
| Explanation | All the macro directives found in the source code have been processed.  |
| User Action | Nothing special to do.                                                  |

## ABAP.08

|             |                                                                                                                                                                                                                                                                                              |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                                                                  |
| Identifier  | ABAP.08                                                                                                                                                                                                                                                                                      |
| Message     | The following file will not be processed '\<FILENAME\>': duplicated program or report '\<PROGRAM_NAME\>'.                                                                                                                                                                                    |
| Severity    | Warning                                                                                                                                                                                                                                                                                      |
| Explanation | The analyzer found a source file containing a program called \<PROGRAM_NAME\> that has been already analyzed in another source file. In order to avoid duplicates, the second source file is not analyzed. This situation can occur when 2 versions of the same program have been delivered. |
| User Action | Identify the duplicated object with the application team. Exclude the file from the next analysis.                                                                                                                                                                                           |

## ABAP.09

|             |                                                                                                                                                                                                                                                                                              |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                                                                  |
| Identifier  | ABAP.09                                                                                                                                                                                                                                                                                      |
| Message     | The following file will not be processed '\<FILENAME\>': duplicated include '\<INCLUDE_NAME\>'.                                                                                                                                                                                              |
| Severity    | Warning                                                                                                                                                                                                                                                                                      |
| Explanation | The analyzer found a source file containing a include called \<INCLUDE_NAME\> that has been already analyzed in another source file. In order to avoid duplicates, the second source file is not analyzed. This situation can occur when 2 versions of the same include have been delivered. |
| User Action | Identify the duplicated object with the application team. Exclude the file from the next analysis.                                                                                                                                                                                           |

## ABAP.10

|             |                                                                                                                                                                                                                                                                                                                |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                                                                                    |
| Identifier  | ABAP.10                                                                                                                                                                                                                                                                                                        |
| Message     | The following file will not be processed '\<FILENAME\>': duplicated function-pool '\<FUNCTION_POOL_NAME\>'.                                                                                                                                                                                                    |
| Severity    | Warning                                                                                                                                                                                                                                                                                                        |
| Explanation | The analyzer found a source file containing a function pool called \<FUNCTION_POOL_NAME\> that has been already analyzed in another source file. In order to avoid duplicates, the second source file is not analyzed. This situation can occur when 2 versions of the same function pool have been delivered. |
| User Action | Identify the duplicated object with the application team. Exclude the file from the next analysis.                                                                                                                                                                                                             |

## ABAP.11

|             |                                                                                                                                                                                                                                                                                                    |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                                                                        |
| Identifier  | ABAP.11                                                                                                                                                                                                                                                                                            |
| Message     | The following file will not be processed '\<FILENAME\>': duplicated type-pool '\<TYPE_POOL_NAME\>'.                                                                                                                                                                                                |
| Severity    | Warning                                                                                                                                                                                                                                                                                            |
| Explanation | The analyzer found a source file containing a type pool called \<TYPE_POOL_NAME\> that has been already analyzed in another source file. In order to avoid duplicates, the second source file is not analyzed. This situation can occur when 2 versions of the same type pool have been delivered. |
| User Action | Identify the duplicated object with the application team. Exclude the file from the next analysis.                                                                                                                                                                                                 |

## ABAP.12

|             |                                                                                                                                                                                                                                                                                                    |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                                                                        |
| Identifier  | ABAP.12                                                                                                                                                                                                                                                                                            |
| Message     | The following file will not be processed '\<FILENAME\>': duplicated user-exit '\<USER_EXIT_NAME\>'.                                                                                                                                                                                                |
| Severity    | Warning                                                                                                                                                                                                                                                                                            |
| Explanation | The analyzer found a source file containing a user-exit called \<USER_EXIT_NAME\> that has been already analyzed in another source file. In order to avoid duplicates, the second source file is not analyzed. This situation can occur when 2 versions of the same user-exit have been delivered. |
| User Action | Identify the duplicated object with the application team. Exclude the file from the next analysis.                                                                                                                                                                                                 |

## ABAP.13

|             |                                                                                                                                                        |
|-------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                            |
| Identifier  | ABAP.13                                                                                                                                                |
| Message     | Loading SAP tables . . .                                                                                                                               |
| Severity    | Information                                                                                                                                            |
| Explanation | The analyzer is loading the extraction files containing the information on database tables. The number of files depends on the size of the extraction. |
| User Action | Nothing special to do.                                                                                                                                 |

ABAP.14

|             |                                                                                                                                                                                                                                                                                                       |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                                                                           |
| Identifier  | ABAP.14                                                                                                                                                                                                                                                                                               |
| Message     | \<DB_TABLE_NUMBER\> tables \<DB_VIEW_NUMBER\> views have been loaded.                                                                                                                                                                                                                                 |
| Severity    | Information                                                                                                                                                                                                                                                                                           |
| Explanation | This message presents the number of database tables and database views that have been identified in extraction files. This information can be used to validate the number of database tables that have been created in the Local base compared to the database tables that are used by the ABAP code. |
| User Action | Nothing special to do.                                                                                                                                                                                                                                                                                |

## ABAP.15

|             |                                                                                                       |
|-------------|-------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                           |
| Identifier  | ABAP.15                                                                                               |
| Message     | No file to parse.                                                                                     |
| Severity    | Error                                                                                                 |
| Explanation | The analysis settings are not correct and no ABAP source files has been selected for being analyzed.  |
| User Action | Review the Analysis Unit configuration and the source file settings.                                  |

## ABAP.16

<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Info</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Identifier</td>
<td class="confluenceTd">ABAP.16</td>
</tr>
<tr class="even">
<td class="confluenceTd">Message</td>
<td class="confluenceTd">Unresolved SAP database table or view:
'&lt;DB_TABLE_NAME&gt;' called in object '&lt;CALLER_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="confluenceTd">Explanation</td>
<td class="confluenceTd">The analyzer found an Open SQL query refering
to a database table or a database view called &lt;DB_TABLE_NAME&gt; that
has not been identified in the extraction files. </td>
</tr>
<tr class="odd">
<td class="confluenceTd">User Action</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Select the object &lt;DB_TABLE_NAME&gt; in the list of objects in
the Unresolved folder of the technical browser in CAST Enlighten. Check
the SAP table extracted files with the application team and investigate
why these tables or views have not been extracted. Missing components
could affect reference resolution and potentially quality rules.</p>
<p><img src="270008723.png" class="image-left"
draggable="false" data-image-src="270008723.png"
data-unresolved-comment-count="0" data-linked-resource-id="270008723"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="worddav9906793042b1ab5c35a2812e46de2037.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="270008717"
data-linked-resource-container-version="2" width="555"
height="304" /></p>
</div></td>
</tr>
</tbody>
</table>

## ABAP.17

  

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Info</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Identifier</td>
<td class="confluenceTd">ABAP.17</td>
</tr>
<tr class="even">
<td class="confluenceTd">Message</td>
<td class="confluenceTd">Unresolved Class or Interface: '&lt;NAME&gt;'
called in object '&lt;CALLER_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="confluenceTd">Explanation</td>
<td class="confluenceTd">The analyzer found a reference to a class or an
interface called &lt;NAME&gt; that is not defined in any ABAP source
file belonging to the analysis scope.  </td>
</tr>
<tr class="odd">
<td class="confluenceTd">User Action</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Select the object &lt;NAME&gt; in the list of objects in
the Unresolved folder of the technical browser in CAST Enlighten. Check
the source code with the application team. Missing components could
affect reference resolution and potentially quality rules.</p>
</div></td>
</tr>
</tbody>
</table>

## ABAP.18

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Info</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Identifier</td>
<td class="confluenceTd">ABAP.18</td>
</tr>
<tr class="even">
<td class="confluenceTd">Message</td>
<td class="confluenceTd">Unresolved Method: '&lt;METHOD_NAME&gt;' in
class '&lt;CLASS_NAME&gt;' called in object '&lt;CALLER_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="confluenceTd">Explanation</td>
<td class="confluenceTd">The analyzer found a reference to a class
method called &lt;METHOD_NAME&gt; for which no source code has been
identified in the analysis scope. This could also occur when a parent
class in which the method is defined has not been delivered.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">User Action</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Select the object &lt;METHOD_NAME&gt; in the list of objects in
the Unresolved folder of the technical browser in CAST Enlighten. Check
the source code with the application team. Missing components could
affect reference resolution and potentially quality rules.</p>
</div></td>
</tr>
</tbody>
</table>

## ABAP.19

|             |                                                                                                                                                                                                                                                    |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                        |
| Identifier  | ABAP.19                                                                                                                                                                                                                                            |
| Message     | Unresolved Member: '\<MEMBER_NAME\>' in class '\<CLASS_NAME\>' called in object '\<CALLER_NAME\>'.                                                                                                                                                 |
| Severity    | Warning                                                                                                                                                                                                                                            |
| Explanation | The analyzer found a reference to a class member called \<MEMBER_NAME\> for which no source code has been identified in the analysis scope. This could also occur when a parent class in which the member is defined has not been delivered.       |
| User Action | Select the object in the list of objects in the Unresolved folder of the technical browser in CAST Enlighten. Check the source code with the application team. Missing components could affect reference resolution and potentially quality rules. |

## ABAP.20

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Info</th>
<th class="confluenceTh">Explanation</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Identifier</td>
<td class="confluenceTd">ABAP.20</td>
</tr>
<tr class="even">
<td class="confluenceTd">Message</td>
<td class="confluenceTd">Unresolved Events: '&lt;EVENT_NAME&gt;' in
class '&lt;CLASS_NAME&gt;' called in object '&lt;CALLER_NAME&gt;'.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="confluenceTd">Explanation</td>
<td class="confluenceTd">The analyzer found a reference to a class event
called &lt;EVENT_NAME&gt; for which no source code has been identified
in the analysis scope. This could also occur when a parent class in
which the event is defined has not been delivered.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">User Action</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Select the object in the list of objects in the Unresolved folder of
the technical browser in CAST Enlighten. Check the source code with the
application team. Missing components could affect reference resolution
and potentially quality rules.</p>
</div></td>
</tr>
</tbody>
</table>

## ABAP.21

|             |                                                                                                                                                                                                                                    |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                        |
| Identifier  | ABAP.21                                                                                                                                                                                                                            |
| Message     | \<MESSAGE\>                                                                                                                                                                                                                        |
| Severity    | Warning                                                                                                                                                                                                                            |
| Explanation | The analyzer is not able to read a LIST file that contains the information on SAP transactions and various cases can be described by this message. This occurs when the file is not correctly generated by the CAST SAP Extractor. |
| User Action | Identify the LIST file for which the message has been produced and contact [CAST Support](https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support).                        |

## ABAP.22

|             |                                                                                                                                                                                                                                              |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                  |
| Identifier  | ABAP.22                                                                                                                                                                                                                                      |
| Message     | Light processing file '\<FILENAME\>'.                                                                                                                                                                                                        |
| Severity    | Information                                                                                                                                                                                                                                  |
| Explanation | The analyzer processes the source files in 2 steps. The first one aims to identify the different objects across the source files. The second one parses the code and resolves references. This message appears when the first step starts.   |
| User Action | Nothing special to do.                                                                                                                                                                                                                       |

## ABAP.23

|             |                                                                                                                                                                                                                                               |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                   |
| Identifier  | ABAP.23                                                                                                                                                                                                                                       |
| Message     | Processing file '\<FILENAME\>'.                                                                                                                                                                                                               |
| Severity    | Information                                                                                                                                                                                                                                   |
| Explanation | The analyzer processes the source files in 2 steps. The first one aims to identify the different objects across the source files. The second one parses the code and resolves references. This message appears when the second step starts.   |
| User Action | Nothing special to do.                                                                                                                                                                                                                        |

## ABAP.24

|             |                                                                                                                                                                                                                                                                   |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                                                                                                                                                                                       |
| Identifier  | ABAP.24                                                                                                                                                                                                                                                           |
| Message     | Unresolved Transaction: '\<TRANSACTION_NAME\>' called in object '\<CALLER_NAME\>'.                                                                                                                                                                                |
| Severity    | Warning                                                                                                                                                                                                                                                           |
| Explanation | The analyzer found a reference to a transaction called \<TRANSACTION_NAME\> that has not been identified in any extraction file. This can occur when the package in which the transaction is defined has not been delivered or is not part of the analysis scope. |
| User Action | Select the object in the list of objects in the Unresolved folder of the technical browser in CAST Enlighten. Check the source code with the application team. Missing components could affect reference resolution and potentially quality rules.                |

## ABAP.25

|             |                                                                                                    |
|-------------|----------------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                        |
| Identifier  | ABAP.25                                                                                            |
| Message     | Loading SAP database tables file '\<FILENAME\>'. . .                                               |
| Severity    | Information                                                                                        |
| Explanation | The analyzer is loading one of the extraction files containing the information on database tables. |
| User Action | Nothing special to do.                                                                             |

## ABAP.26

|             |                                                                                              |
|-------------|----------------------------------------------------------------------------------------------|
| Info        | Explanation                                                                                  |
| Identifier  | ABAP.26                                                                                      |
| Message     | Resolving SAP database table Foreign Keys. . .                                               |
| Severity    | Information                                                                                  |
| Explanation | The analyzer creates links between database tables by following the Foreign Keys relations.  |
| User Action | Nothing special to do.                                                                       |

## ABAP.27

|             |                                                                        |
|-------------|------------------------------------------------------------------------|
| Info        | Explanation                                                            |
| Identifier  | ABAP.27                                                                |
| Message     | SAP database table Foreign Keys resolved.                              |
| Severity    | Information                                                            |
| Explanation | The analyzer completed the creation of links between database tables.  |
| User Action | Nothing special to do.                                                 |

