---
title: "Qualification specifics"
linkTitle: "Qualification specifics"
type: "docs"
weight: 1
---

## What you need to know

### Types of ABAP code

There are two types of ABAP code: "Procedural ABAP" and "ABAP Object". A
significant part of the development of custom code around SAP is done
using Procedural ABAP. However, in the last version of SAP specific
standard modules are implemented in ABAP Objects and as a result, the
custom code has been adapted to ABAP Objects.

Please also see [ABAP and SAP terminology](../terminology/) for more background information.

### Access to the ABAP source code

The code developed within the SAP instance can be fed into CAST for
analysis in two ways:

#### CAST SAP Extractor NG

Source code can be extracted from the SAP repository by using an ABAP
program (i.e. the extraction tool) - see [CAST SAP Extractor
NG](https://doc.castsoftware.com/display/DOCCOM/CAST+SAP+Extractor+NG)
for more information. The extractor will handle:

-   all ABAP programs, including source code, belonging to a SAP package
-   all SAP tables and views belonging to SAP object names

![](270008683.jpg)

#### Directly extracted from an abapGit repository

If you are storing your source code
in [abapGit](https://abapgit.org/) and you are using the SAP ABAP Analyzer ≥
1.0.8-funcrel, you can package the source code directly from this
source code repository. The following abapGit file storage types are
supported and the resulting objects are identical to those generated
from a [CAST SAP Extractor
NG](https://doc.castsoftware.com/display/DOCCOM/CAST+SAP+Extractor+NG) extraction:

-   package.devc.xml files (packages)
-   \*.abap files (abap objects, programs, classes...)
-   \*.tabl.xml files (sap tables)
-   \*.view.xml files (sap views)
-   \*.ddls.asddls files (sap CDS views)
-   \*.tran.xml files (transactions)
-   \*.wdya.xml files (web dynpros)
-   \*.wdyn.xml files (web dynpros)

### Objects created by the ABAP Analyzer

The ABAP Analyzer provides the ability to see
the objects around two programming models:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><p><strong>Macroscopic
objects</strong></p></td>
<td class="confluenceTd"><p><strong>Microscopic
objects</strong></p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>ABAP Package<br />
ABAP Program<br />
ABAP Include<br />
ABAP Function Pool<br />
ABAP Module Pool<br />
ABAP Type Pool<br />
ABAP Class Pool<br />
ABAP Interface Pool<br />
R3 WebDynpro component<br />
R3 WebDynpro Application<br />
R3 WebDynpro Window<br />
R3 WebDynpro View<br />
R3 WebDynpro Component Controller</p></td>
<td class="confluenceTd"><p>ABAP Form<br />
ABAP Function<br />
ABAP Type<br />
ABAP Event Block<br />
ABAP Macro<br />
ABAP Module<br />
ABAP Class<br />
ABAP Constructor<br />
ABAP Member<br />
ABAP Constant<br />
ABAP Method<br />
ABAP Event<br />
ABAP Event Method<br />
ABAP Interface<br />
SAP Transaction<br />
ABAP Processing Screen<br />
ABAP Selection-Screen<br />
ABAP BAPI<br />
ABAP BAPI Method<br />
ABAP BAPI Key Field<br />
ABAP BAPI Attribute<br />
ABAP BAPI Event<br />
SAP R3WDYN inbound plugin<br />
SAP R3WDYN outbound plugin<br />
R3 WebDynpro Event Handler<br />
R3 WebDynpro Action<br />
R3 WebDynpro Method<br />
R3 WebDynpro Supply Function<br />
R3 WebDynpro Attribute</p></td>
</tr>
</tbody>
</table>

### Technical inner structure

The ABAP Analyzer detects the technical structure regarding:

-   Dependencies between programs and includes
-   Calls between transactions, programs, includes, function-pools,
    user-exits and functions, forms, modules, processing &
    selection-screens and methods
-   Include links between reports, function-pools, user-exits, BADIs and
    Includes
-   Use of function pools
-   Definition and usage of Macros
-   Inheritance between classes
-   Implementation between classes and interfaces
-   Access links between methods, members and objects
-   Raise links and Rely On links between methods and events

The following "Belongs To" relationships are managed by the ABAP
Analyzer (object types located in the second level belong to those which
are located in the first level): 

Project:

-   Package
-   Processing Screen

SAP Project:

-   Table
-   View
-   CDS View
-   Transaction

Package:

-   Program
-   Include
-   Function Pool
-   Type Pool
-   Class Pool
-   Interface Pool
-   Transaction
-   BAPI
-   R3 WebDynpro Component

Program:

-   File Level Code
-   Event Block
-   Module
-   Function
-   Form
-   Selection Screen
-   Class
-   Interface

Include:

-   File Level Code
-   Event Block
-   Module
-   Function
-   Form
-   Selection Screen
-   Class
-   Interface

File Level Code:

-   Selection Screen

Function Pool:

-   Function
-   Form

Class Pool:

-   Class

Interface Pool:

-   Interface

Class:

-   Method
-   Constructor
-   Event Method
-   Member
-   Constant

Interface:

-   Method
-   Constructor
-   Event Method
-   Member

BAPI:

-   BAPI Method
-   Key Field
-   Attribute
-   Event

R3 WebDynpro Component

-   R3 WebDynpro Application
-   R3 WebDynpro Window
-   R3 WebDynpro View
-   R3 WebDynpro Component Controller

R3 WebDynpro Window

-   SAP R3WDYN inbound plugin

R3 WebDynpro View

-   SAP R3WDYN inbound plugin
-   SAP R3WDYN outbound plugin
-   R3 WebDynpro Action
-   R3 WebDynpro Event Handler
-   R3 WebDynpro Method

R3 WebDynpro Component Controller

-   R3 WebDynpro Method
-   R3 WebDynpro Supply function
-   R3 WebDynpro Attribute

In addition to the dependency analysis, a comprehensive set of metrics
is calculated on the ABAP code, such as:

-   Cyclomatic complexity (which gives the number of logical paths
    within a given module or function)
-   SQL Complexity (returning queries with complex SQL statements, which
    can therefore potentially affect performance).

Quality rules are calculated on ABAP code (this includes Web Dynpro
object types such as Method, Event Handlers and Supply functions) and on
Open SQL queries.

## Questions to ask the customer

The following list provides some questions that should be discussed with
the customer before extracting any source code from the SAP repository:

-   What version of SAP is used (4.0B, 5.0, ...)?
    -   Notes:  
        -   Cast AIP supports SAP R/3 (4.0 to 6.0)
        -   For versions ECC5 and ECC6, the version numbers are 5.0 and
            6.0 respectively
-   Do you use Open SQL or ABAP Native SQL?
    -   The Metric Assistant only calculates metrics on Open SQL. ABAP
        Native SQL is detected for Quality rule purposes.
-   What is the naming convention used to identify the custom code?
    -   The name of customized objects must start with a Z - although
        SAP does allow certified partners that deliver SAP complements
        to use other letters for the transactions (example: Y\*, LZ\*,
        LY\*, SAPLY\*, SAPLZ\*)
-   Do you have other SAP packages outside of the Z package that you
    need to analyze?
-   Are applications implemented on several SAP instances?
-   Can you quantify the number of ABAP objects per package this
    represent?
-   Can you provide access to SAP on the CAST analysis machine or other
    workstation accessible by the CAST administrator?
    -   SAP access must be set up for a SAP user that has development
        authority
-   Which network or local hard drive can be used to store the results
    of the extraction?

## Collecting the source code using the SAP Extractor NG

The ABAP code is extracted from the SAP repository via the an ABAP
program (extractor) as mentioned before. The ABAP program queries the
system catalog of the SAP repository and use built-in functions to
access the source code and then save it as text files on disk. The
results are saved in a special file format on a network, a local hard
drive or on the SAP server home directory. The result files are then
specified in the configuration of the ABAP analysis settings.

The extractor allows you to extract the number of rows for each
extracted table. It saves this information in a XML format in
file: SAP_tables_volume_0001.SQLTABLESIZE among those that are
already generated. This file must be used to configure XXL diagnostics
in the CAST Management Studio.

So, if you want to use these specific diagnostics, then you must select
the option Extract number of rows in the SAP Extractor (this is the
default value for this option). In addition, the extractor extracts the
number of rows for client-dependent tables. Client-dependent tables can
contain merged data related to different clients. By default, this
option is activated. You must know that this operation can take a long
time especially if the volumes of tables are huge.

In batch mode, the extractors cannot create subdirectories on the
server. They will then use ZIP files to save source files and
corresponding sub-directories related to packages. Note that this
behavior is available for SAP 4.7, ECC5 and ECC6 only. For other
versions, result files will be all saved in the same directory. This
does not impact the analyzer (it finds information about package in
source files) but if you change the extraction mode, then objects full
name will change and this will impact dashboard in terms of deleted and
new created objects.  
See related documentation for more information on how to use this type
of files.

Please see [CAST SAP Extractor
NG](https://doc.castsoftware.com/display/DOCCOM/CAST+SAP+Extractor+NG)
for more information about this.

It may be very useful to extract additional information that could be
used to filter rules or in an upgrade context.Extracting additional
information

This information is provided by the transactions ST03: Performance, SAP
Statistics, Workload. The extraction of the information related to the
transaction ST03 may be integrated in to a future version of the Cast
extractors. However, for the moment, it is still necessary to extract
this information manually.

### ABAP Source Code

#### ABAP files

It is likely that the ABAP source code will not be extracted from the
CAST AIP analysis machine. Therefore the ABAP files will be moved to the
CAST AIP analysis machine following extraction. The CAST AIP
administrator should note the number of ABAP files present in each
Package / folder that is a candidate for the analysis. A header is
inserted at the beginning of the extracted files. This header contains
specific information such as the version of the extractor, the version
of SAP, the package and the extraction date.

Example: File "Z_CREATE_EXAMPLE_DATA.abap":

    * extractor_prog_Version 6.4 for SAP ECC5, ECC6.
    * DATE 20100323 . DESCR Example of code to create data
    * SAP_Version 701 .
    * Dev_Class Z_ABAP_BOOK .

    REPORT z_create_example_data 

It is important to use the files that are created by the CAST extractor
because the ABAP Analyzer uses the information written in the ABAP file
header.

#### Backup

The administrator will make sure that a backup of the current version of
the ABAP source code is kept in a location clearly identified by the
project team. This procedure will include a clear identification of the
different versions and the date the files were extracted from the SAP
repository.