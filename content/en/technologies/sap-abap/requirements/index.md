---
title: "Requirements"
linkTitle: "Requirements"
type: "docs"
weight: 4
---

| Technology | Pre-extraction? | Install on workstation running CAST Console | Install on workstation running Node |
|---|---|---|---|
| ABAP | Not required. | Nothing required | Nothing required |
| SAP | A specific extractor is delivered by CAST (see below) and this must be installed and run on the SAP instance BEFORE the packaging/extraction is actioned in the CAST Delivery Manager Tool. | Nothing required | Nothing required |

## SAP Extractor

CAST supports the analysis of ABAP client code and can also identify
links from this client code to SAP tables/programs. To do so,
information about the SAP tables and programs needs to be extracted from
the SAP system and analyzed at the same time as the ABAP client code. To
perform the extraction, a specific SAP extractor must be installed and
run on the SAP instance BEFORE the extraction is actioned in the CAST
Delivery Manager Tool. This extractor is run from the SAP instance and
extracts information into flat files: these files are then manually or
automatically transferred onto a Windows workstation for
packaging/extraction. You can find
out more about this process in the [CAST SAP Extractor NG](https://doc.castsoftware.com/export/DOCCOM/CAST+SAP+Extractor+NG).

Note that in order for CAST to save SAP tables and programs in the
CAST Analysis Service schema during an analysis, the following must be
true: 

-   The SAP tables and programs must have been successfully extracted
    from the SAP system and analyzed with the ABAP client code
-   AND the SAP tables and programs must be referenced in the ABAP
    client code

If only one of these things is true then the tables and programs will
not be saved in the CAST Analysis Service schema. 
