---
title: "Coverage overview"
linkTitle: "Coverage overview"
type: "docs"
weight: 1
no_list: false
---

***

The following technologies are supported in **CAST Imaging v3 on-premises**:

- [Java/JEE](details/#jee)
- [Microsoft .NET](details/#microsoft-net)
- [Web](details/#web)
- [Node.js](details/#nodejs)
- [RDBMS](details/#rdbms)
- [NoSQL](details/#nosql)
- [IBM Mainframe zOS](details/#ibm-mainframe-zos) (except Enterprise Generation Language (EGL))
- [IBM i (formerly System i, iSeries or AS/400)](details/#ibm-i) (except Enterprise Generation Language (EGL))
- [Cobol (not IBM)](details/#cobol-not-ibm)
- [Microsoft Classic](details/#microsoft-classic) (except Microsoft Visual Basic (VB))
- [Message Queues](details/#message-queues)
- [Scripting Languages](details/#scripting-languages)
- [Go](details/#go)

{{% alert color="info" %}}Looking for information about **CAST Console v1/v2 / Core 8.3**, see [Covered technologies for v2](../../export/TECHNOS/Covered+Technologies) or for information about **CAST Imaging on Cloud**, see [Covered technologies for Imaging Cloud](../../imagingoncloud/covered-technos/).{{% /alert %}}