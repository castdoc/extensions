---
title: "Covered Technologies"
linkTitle: "Covered Technologies"
type: "docs"
weight: 1
---

> Support for Microsoft Visual Basic is deprecated. This means that no further development nor bug fixes will be provided for this component. In addition, whilst analyses of small/medium sized Visual Basic will usually proceed without issue, larger applications may fail due to analyzer memory limitations. Please see the note [here](../requirements) about 32bit/64bit compatibility.
 
| Versions supported | Supported by |
|-----|-----|
| VB 4 32 bits | VB Analyzer (embedded in CAST Imaging Core) |
| VB 5  | VB Analyzer (embedded in CAST Imaging Core) |
| VB 6  | VB Analyzer (embedded in CAST Imaging Core  |