---
title: "Requirements"
linkTitle: "Requirements"
type: "docs"
weight: 3
---

> Support for Microsoft Visual Basic is deprecated. This means that no further development nor bug fixes will be provided to support this technology.

## Visual Basic analyzer and 64-bit compatibility

CAST Imaging Core is 64-bit compliant and therefore requires a 64-bit compatible machine for installation purposes, however, the Visual Basic analyzer contains components that cannot be re-factored to 64bit and therefore CAST has had to deliver these components as 32-bit modules. This has been achieved by placing the required components in the Vb6 folder located at the root of the CAST Imaging Core installation location.

Analysis of VB source code will proceed without issue, however, please note the following:

- Because the Visual Basic analyzer is delivered as 32-bit, it is subject to the same memory limitations as any other 32-bit application.
- If you manually modify any configuration files (such as the default CAST Imaging Core environment profiles or other .INI files), you must ensure that you modify them in the `<CAST_Imaging_Core_install_folder>\Vb6\configuration` folder, rather than in the identical file located in the standard `<CAST_Imaging_Core_install_folder>\configuration` folder.
- You can still manually create Execution Units if you find that you are receiving out of memory errors when attempting to run a large Visual Basic analysis.