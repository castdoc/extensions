---
title: "Analysis results"
linkTitle: "Analysis results"
type: "docs"
weight: 6
---

## What results can you expect?

### Objects

| Icon | Description                                                                    |
|------|--------------------------------------------------------------------------------|
| ![Alt text](270008600.png)  | Class Module                                                                   |
| ![Alt text](270008599.png)  | Constant                                                                       |
| ![Alt text](270008602.png)     | Control                                                                        |
| ![Alt text](270008601.png)     | Designer                                                                       |
| ![Alt text](270008603.png)     | Event                                                                          |
| ![Alt text](270008605.png)     | External Function                                                              |
| ![Alt text](270008605.png)     | External Sub                                                                   |
| ![Alt text](270008604.png)     | Form                                                                           |
| ![Alt text](270008607.png)     | Function                                                                       |
| ![Alt text](270008606.png)     | MDI Form                                                                       |
| ![Alt text](270008608.png)     | Module                                                                         |
| ![Alt text](270008610.png)     | Control Project                                                                |
| ![Alt text](270008609.png)     | EXE Project                                                                    |
| ![Alt text](270008613.png)     | Group Project                                                                  |
| ![Alt text](270008612.png)     | OLE DLL Project                                                                |
| ![Alt text](270008611.png)     | OLE Exe Project                                                                |
| ![Alt text](270008614.png)    | Get Property                                                                   |
| ![Alt text](270008614.png)     | Let Property                                                                   |
| ![Alt text](270008616.png)     | Page Property                                                                  |
| ![Alt text](270008614.png)     | Set property                                                                   |
| ![Alt text](270008607.png)     | Sub                                                                            |
| ![Alt text](270008615.png)     | VB Subset                                                                      |
| ![Alt text](270008618.png)     | User Control                                                                   |
| ![Alt text](270008617.png)     | User Document                                                                  |
| ![Alt text](270008614.png)     | Variable                                                                       |
| ![Alt text](270008619.png)     | Com CoCLass                                                                    |
| ![Alt text](270008621.png)     | Com Disp Interface Com Interface                                               |
| ![Alt text](270008620.png)     | Com Method Com Property Com Get Property Com Put Property Com Put Ref Property |
| ![Alt text](270008622.png)     | Com Type Lib Com External Type Lib                                             |

### Links

<table>
<tbody>
<tr>
<td><strong>Link Type</strong></td>
<td><strong>When is this type of link
created?</strong></td>
</tr>
<tr>
<td><strong>Call</strong></td>
<td><div class="content-wrapper">
- <strong>When calling a VB or a COM function (sub, function, property
get, let set, com method, com prop put, prop putref, prop get) except
system functions (functions from VB, VBA or VBRUN DLLs)</strong>
<p>- <strong>When calling a VB or a COM event</strong></p>
<p>- <strong>When the return type of a function is not a standard
one</strong></p>
<p>Here is a sample that shows one of these links.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: vb; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: vb; gutter: false; theme: Confluence"><pre
class="sourceCode vb"><code class="sourceCode monobasic"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="kw">Private</span> <span class="kw">Sub </span>MySub(i <span class="kw">As</span> <span class="dt">Integer</span>)</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>  <span class="co">&#39;Do Whatever</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a>  <span class="kw">End Sub</span></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a><span class="kw">Private</span> <span class="kw">Sub </span>Command1_Click()</span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a> </span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a>  <span class="kw">Dim</span> j <span class="kw">As</span> <span class="dt">Integer</span></span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a>  MySub (j)</span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-13"><a href="#cb1-13" aria-hidden="true" tabindex="-1"></a><span class="kw">End Sub</span></span></code></pre></div>
</div>
</div>
<p>This will result in the following view:</p>
<p><img src="270008624.jpg" /></p>
</div></td>
</tr>
<tr>
<td><strong>Raise</strong></td>
<td><div class="content-wrapper">
- <strong>When raising an event</strong>
<p>In the following sample, we have a class (<em>Class1</em>) that
defines an event (<em>Gong</em>) and a method (<em>MyMethod</em>). When
<em>MyMethod</em> is called, we raise an event.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: vb; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: vb; gutter: false; theme: Confluence"><pre
class="sourceCode vb"><code class="sourceCode monobasic"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="kw">Event</span> Gong()</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="kw">Public</span> <span class="kw">Sub </span>MyMethod()</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="kw">RaiseEvent</span> Gong</span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a><span class="kw">End Sub</span></span></code></pre></div>
</div>
</div>
<p>This will result in the following view (includes only event relevant
links):</p>
<p><img src="270008623.jpg" /></p>
</div></td>
</tr>
<tr>
<td><strong>Use</strong></td>
<td><div class="content-wrapper">
<p>There is also a Form (<em>Form1</em>) that has the button
(<em>Command1</em>). Here’s the code of this Form:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: vb; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: vb; gutter: false; theme: Confluence"><pre
class="sourceCode vb"><code class="sourceCode monobasic"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="kw">Private</span> <span class="kw">WithEvents</span> mHandler <span class="kw">As</span> Class1</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="kw">Private</span> <span class="kw">Sub </span>Command1_Click()</span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>   <span class="kw">Set </span>mHandler = <span class="kw">New</span> Class1</span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>   mHandler.MyMethod</span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a><span class="kw">End Sub</span></span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a><span class="kw">Private</span> <span class="kw">Sub </span>mHandler_Gong()</span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a>MsgBox <span class="st">&quot;Gong !&quot;</span></span>
<span id="cb3-14"><a href="#cb3-14" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-15"><a href="#cb3-15" aria-hidden="true" tabindex="-1"></a><span class="kw">End Sub</span></span></code></pre></div>
</div>
</div>
<p>- <strong>When declaring a user data-type, link with the non standard
members of this user data-type</strong></p>
<p>- <strong>When referencing a non standard type member of a user
data-type variable</strong></p>
<p><img src="270008625.jpg" /></p>
<p><strong>- When accessing a variable</strong></p>
<p><img src="270008626.jpg" /></p>
</div></td>
</tr>
<tr>
<td><strong>Rely On</strong></td>
<td><div class="content-wrapper">
- <strong>When declaring a variable of a non standard type</strong>
<p><img src="270008628.jpg" /></p>
<p>- <strong>When declaring a function return value of a non standard
type</strong></p>
</div></td>
</tr>
<tr>
<td><strong>Inherit</strong></td>
<td><div class="content-wrapper">
- <strong>When a user control inherits from a non standard type</strong>
<p>In this sample, the user control “UserControl1” is used in the form
“Form1”</p>
<p><img src="270008627.jpg" /></p>
<p>- <strong>When implementing an event using WithEvents
statement</strong></p>
<p><img src="270008629.jpg" /></p>
</div></td>
</tr>
</tbody>
</table>
