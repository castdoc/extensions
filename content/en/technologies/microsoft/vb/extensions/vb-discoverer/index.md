---
title: "Visual Basic Discoverer"
linkTitle: "Visual Basic Discoverer"
type: "docs"
no_list: false
---

The Visual Basic Discoverer is not provided as an extension: instead it is embedded in CAST Imaging Core and is therefore present "out of the box". It Configures one project for each VB project (.VBP file) discovered and is displayed as follows in CAST Imaging Console:

![Alt text](images/VB_AU.jpg)