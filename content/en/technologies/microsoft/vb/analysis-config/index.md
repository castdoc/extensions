---
title: "Analysis configuration"
linkTitle: "Analysis configuration"
type: "docs"
weight: 5
---

## Using CAST Imaging Console

CAST Imaging Console exposes the technology configuration options once an initial analysis has been run. Click
Visual Basic Technology to display the available options:

![](492011820.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same as at Application level) that allow you to
        make fine-grained configuration changes.

Settings are initially set according the information discovered during
the [source code discovery
process](../prepare/) when
creating a version. You should check that these auto-determined settings
are as required and that at least one Analysis Unit exists for the
specific technology.

![](492011821.jpg)

| Item                           | Description                                                                                                                                                                                         |
| ------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Default Handling               | During the analysis, when this option is selected, the analyzer will take into account predefined constants and any #Const directives contained in the project.                                     |
| Compare references by filename | If you select this option, the analyzer will compare ActiveX references in the .VBP file using the filename only. If the option is left blank, references are compared using the full path instead. |
