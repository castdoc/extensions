---
title: "Prepare and deliver the source code"
linkTitle: "Prepare and deliver the source code"
type: "docs"
weight: 4
---

## Information about discovery

Discovery is a process that is actioned during the delivery process.
CAST Imaging will attempt to automatically identify "projects" within your
application using a set of predefined rules. This discovery process also
allows CAST Imaging to set the initial analysis configuration settings. Discoverers are
currently embedded in CAST Imaging Core:

- [Visual Basic Discoverer](../extensions/vb-discoverer)

You should read the relevant documentation for each discoverer (provided
in the link above) to understand how the source code will be handled.

## Source code delivery

> Supported in CAST Imaging Console v. ≥ 1.23.0.

CAST Imaging Console expects either a ZIP/archive file or source code
located in a folder configured in CAST Imaging Console. You should include in
the ZIP/source code folder all Visual Basic source code:

-   .VBP
-   Any associated files.

CAST highly recommends placing the files in a folder dedicated to Visual
Basic. If you are using a ZIP/archive file, zip the folders in the
"temp" folder - but do not zip the "temp" folder itself, nor create any
intermediary folders:

```
D:\temp
    |-----Visual Basic
    |-----OtherTechno1
    |-----OtherTechno2
```