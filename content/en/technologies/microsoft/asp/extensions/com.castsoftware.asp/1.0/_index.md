---
title: "ASP Rules - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.asp

## What's new?

See [Release Notes](rn/).

## Description

This extension provides a correction for one specific ASP related rule: [Always validate user input with Request variables - 7448](https://technologies.castsoftware.com/rules?s=7448|qualityrules|7448). If an analysis is run without this extension, any true violations of the rule are not detected when the rule is triggered. Installing the extension ensures that the rule in question is correctly applied and any true violations are detected, if they exist.

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: - no longer required with v3/8.4 |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |
