---
title: "Technical notes"
linkTitle: "Technical notes"
type: "docs"
weight: 2
---
## ASP

### \<SCRIPT\> tag

Files contained within the \<SCRIPT\> tag without the "runat = server"
attribute will be considered as containing client code only. This can
cause syntax errors during the analysis if you have an ASP file
contained within the \<SCRIPT\> tag. One possible solution is to ignore
this type of coding by replacing the expression \<SCRIPT
src="XXX/XXX.asp" \> with \<SCRIPT\> via the Text Replacement feature.

### Source code and comment line counts

The number of lines of code (LOC) of a Web file is calculated according
to the formula:

1. #LOC in file = #lines in file - #empty lines - #comment lines
2. When a scripting files is included for both the server side (using
   the tag \<script runat=server\> in one page) and the client side
   (using the tag \<script\> in another page), file level comments are
   counted twice. This affects also the #LOC of that file as it is
   calculated using the formula given in 1. However, the count for the
   class's sub-objects is correct.
3. VBScript classes are not handled and therefore comments inside these
   classes are not seen. This affects the line count for code and
   comments in these files as it is calculated using the formula given
   in 1.
4. Variable declarations are not handled. Therefore there are no
   comments associated with them.
5. If a file contains two distinct comment blocks on the same line and
   if there is part of a statement between the two blocks, CAST AIP
   counts two comment lines instead of one. In the following example,
   two lines are counted:

```xml
return /* Comment 1 */ 0; /* Comment 2 */
```

### Implicit declaration of events

Implicit declaration of events is not supported. Thus such events are
not displayed and no link to the procedure is found.

## Jscript 3.0 - 5.6

- Precompilation is not supported.
- Jscript classes are represented as Jscript methods and there are no
  calling links between Jscript class members.

## XHTML 1.x

- Unexpected syntax error: unescaped script content.

## JavaScript

- Object Literals are not supported.
- WindChill script is not supported (\<script language=windchill\>).
  Usage of this script language can cause syntax errors during an
  analysis. As a consequence, no JavaScript methods are created nor
  links to JavaScript methods.
- JavaScript function calls from included ASPs are not detected by the
  analyzer. For example:
  - asp1.asp includes asp2.asp and myscript.js
  - asp2.asp calls functions defined in myscript.js
  - CAST AIP cannot detect Call links from asp2.asp to
    the js functions
- JavaScript files delivered in minified format will not be
  analyzed (the analyze will skip them). A syntax error will be
  recorded in the analysis log file, similar to this:

```xml
2015-11-05 22:07:32.477     Warning    SYNTAX ; Body    Syntax error : [W]=function(X){V=X;I();L();l[W]=g;try{delete l[W]}catch(Y){}if(H){H.removeChild(T)}}}
```

- JavaScript files delivered with the extension .javascript will
  be skipped by the analyzer. Only files with the
  extension .js will be analyzed.

## Additional notes

The following section lists technical and functional aspects with regard
to analysis of ASP source code:

### Support for classic ASP via the HTML5/JavaScript extension

Although CAST AIP handles classic ASP (Active Server
Pages) applications "out of the box" with the ASP analyzer, the [com.castsoftware.html5](../../../web/html5-js/com.castsoftware.html5/) is also capable of
analyzing classic ASP applications. Below is a comparison of the
analysis of the same classic ASP application with results from
the [com.castsoftware.html5](../../../web/html5-js/com.castsoftware.html5/) extension on the left and results with the ASP analyzer embedded in Core on the right:

Some additional links are provided in certain situations:

![](asp1.jpg)

But less information in other situations:

![](asp2.jpg)

### URI-like Strings in JavaScript Functions

Situation

- Analyzing a web application using javascript
- and the applicaction contains a javascript function having a
  "URI-like" string. E.g. "http://jira/secure/EditIssue"
- and the URI-like string does not match a file name.

See
[http://tools.ietf.org/html/rfc2396](http://tools.ietf.org/html/rfc2396)
for a definition of what is considered as an URI

Symptoms

Following warning is displayed during analysis:

```xml
Cannot find file whose path is "<URI-like string>".
```

This warning has no impact on analysis results and can be safely
ignored. Any "URI-like" string present in a javascript function is
considered as a filename and an attempt is made to resolve it.

### Local variables having the same name as script language methods or functions

If a local variable has the same name as a function or a method (in
JavaScript, Jscript, vbscript), the analyser will create a false link
from the function (or method) containing the local variable to the
function (or method) having the same name as the variable.

Example 1:

The following function will create a recursive link between the function
falseLink and itself:

```xml
function falseLink(...){
    var falseLink = a("newElement");
    ...
    return falseLink;
}
```

Example 2:

The following two functions will create a link between the function ""a
and the function "falseLink":

```xml
function falseLink(...){
    ...
}
 
function a(...){
    var falseLink = a("newElement");
    ...
    return falseLink;
}
```

### Interdependencies between client-side code and server-side code

Because of the way in which the analyzer handles server-side code (i.e.
code running on the web server) and client-side code (i.e. code executed
in the web browser), an analysis may fail if the web file contains both
types of code. The analyzer functions as follows:

- A page is first analyzed as server-side file and what is not
  recognized as server code is collected (preserving line and column
  numbers) in a buffer.
- The collected code is then considered and analyzed as client-side
  code.

For example, the following line of code containing both Java and
JavaScript causes an analysis to fail (bold text is collected as
client-side code):

```xml
String Var = <% If(Test) { %> val1 <% } else {%> Val2 <% } %>
```

This is because the analyzer will first analyze the server-side code (in
this case the Java code) and then the client-side code (in this case
JavaScript). This results in the following client code being analyzed:

```xml
String Var = val1 Val2
```

This is syntactically incorrect. The error occurs on "Val2" and the
analysis of the object containing the code fails. This limitation also
applies even if server-side and client-side code appear on different
lines in the same web file.

#### Syntax error on attribute value without opening double-quote

The analyzer produces a syntax error when encountering an HTML attribute
inside a tag where the attribute value is closed by a double-quote but
has no opening double-quote like in the following example:

```xml
<table border=0">
```

Please note that:

- there is no double-quote before the 0
- this is incorrect syntax, however, it is tolerated by Internet
  Explorer

#### Restrictions on text length when using Text Replacements

When specifying regular expressions in the Text Replacement feature in
the .NET analyzer, you must ensure that the new text has the same length
as the replaced text. Otherwise, an analysis may fail. Note that the
issue will occur during the Metrics Assistant phase of the analysis,
although Text Replacement is configured in the relevant .NET Analysis
Unit or at Application or Technology level.

#### Methods with the same name

If a web file includes directly or indirectly two js files and if
several js or vbs script methods with the same name exist, a link will
be drawn to all of the methods, when the methods are referenced in the
code.