---
title: "Covered Technologies"
linkTitle: "Covered Technologies"
type: "docs"
weight: 1
---

| Sub technology / framework | Versions supported |
|----------------------------|--------------------|
| ASP (ASP Classic)          | -                  |
| JavaScript                 | 1.2 - 1.5          |
| JScript                    | 1.0 - 5.6          |
| VBScript                   | Up to 5.5          |
| HTML                       | 2.0 - 4.0.1       |