---
title: "Delta Tool - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.deltatool

## What's new?

See [Release Notes](rn/).

## Description

This extension provides the automated generation of a report for the
consistency snapshot executed after the upgrade. It identifies any
variations that may exist in the post-upgrade snapshot and provides a
link to an explanation for this variation in the documentation when
available.

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |
## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, should manually install the extension.
