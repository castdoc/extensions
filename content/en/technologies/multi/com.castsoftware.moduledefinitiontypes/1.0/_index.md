---
title: "Module Definition Types - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.moduledefinitiontypes

## What's new?

See [Release Notes](rn/).

## Description

This extension categorizes all modules defined (whether manually or
automatically) in CAST Imaging and gives them a value
as follows:

| Item                      | Value |
|---------------------------|-------|
| Unassigned                | -1    |
| Full Content              | 1     |
| Union Content             | 2     |
| IFPUG                     | 3     |
| Per Analysis Unit         | 4     |
| User Defined              | 5     |
| Per Technology            | 6     |
| Per Folder Path (≥ 1.0.2) | 7     |

This value is stored for each Application (in the Analysis schema) and
the information is used by other CAST processes and tools, as listed
below:

## CAST Imaging Viewer ≥ 2.11

During the export/import process of Application data into CAST Imaging ≥
2.11, this information is used to ensure that Per technology modules are
never included in the export and therefore never appear in CAST Imaging.
These type of modules are automatically created in CAST Imaging Console (unless a
different strategy is chosen manually) and are very often very large
(i.e. contain a lot of data). Because these modules are large, they
often do not provide much value during investigation.

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The Module Definition Types extension will be installed automatically
only when using CAST Imaging Console ≥ 2.4. In any other situation you must download
and install it "manually". A new analysis/snapshot must be performed
after installation to ensure the extension is executed. In addition, a
new import into CAST Imaging Viewer using ≥ 2.11 must be actioned so that the
categorization provided by the extension can be exploited in CAST
Imaging Viewer.

## Configuration instructions

None.