---
title: "Internal Platform - 0.9"
linkTitle: "0.9"
type: "docs"
no_list: true
---

***

This extension is delivered as a dependency to specifically provide
additional features common to all extensions. It will be automatically
downloaded with other extensions, so there is no need to download the
extension manually.