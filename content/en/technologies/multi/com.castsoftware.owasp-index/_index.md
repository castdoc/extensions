---
title: "Security Standards"
linkTitle: "Security Standards"
type: "docs"
---

***

## Extension ID

com.castsoftware.owasp-index

## Description

This extension will compute:

- [OWASP-2021](https://owasp.org/www-project-top-ten/), [OWASP-2017](https://owasp.org/www-project-top-ten/), [OWASP-2013](https://owasp.org/www-project-top-ten/) top 10 application security risks as technical criteria grades,
- [CWE TOP 25 2024](https://cwe.mitre.org/top25/archive/2024/2024_cwe_top25.html), [CWE TOP 25 2023](https://cwe.mitre.org/top25/archive/2023/2023_top25_list.html), [CWE TOP 25 2022](https://cwe.mitre.org/top25/archive/2022/2022_cwe_top25.html), [CWE TOP 25 2021](https://cwe.mitre.org/top25/archive/2021/2021_cwe_top25.html), [CWE TOP 25 2020](https://cwe.mitre.org/top25/archive/2020/2020_cwe_top25.html), [CWE TOP 25 2019](https://cwe.mitre.org/top25/archive/2019/2019_cwe_top25.html), [CWE TOP 25 2011](https://cwe.mitre.org/top25/archive/2011/2011_cwe_sans_top25.html) top 25 application security risks as technical criteria grades, and [CWE](https://cwe.mitre.org/data/index.html),
- PCI-DSS-V4.0, PCI-DSS-V3.2.1, PCI-DSS-V3.1 security risks as technical criteria grades

All CAST rules that are tagged with a related tag will contribute to the various technical criteria provided by the extension, thereby allowing specific grades and rule violations to be reported.

## Compatibility

| Product | Release | Supported |
|---|---|:-:|
| CAST Imaging Core | ≥ 8.3.24, ≥ 8.4.x | :white_check_mark: |
| CAST Engineering Dashboard | ≥ 1.5 | :white_check_mark: |
| CAST Health/Management Dashboard | ≥ 1.17 | :white_check_mark: |
| CAST Security Dashboard | ≥ 1.20 | :white_check_mark: |

## Supported indexes/standards

- OWASP 2021
- OWASP 2017
- OWASP 2013
- CWE 
- CWE TOP 25 2024
- CWE TOP 25 2023
- CWE TOP 25 2022
- CWE TOP 25 2021
- CWE TOP 25 2020
- CWE TOP 25 2019
- CWE TOP 25 2011
- PCI DSS 4.0
- PCI DSS 3.2.1
- PCI DSS 3.1

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, should manually install the extension.

## Configuration requirements

### Generate a snapshot

A new snapshot must be generated (after the extension is installed)
before results can be viewed. If you do not immediately see changes in
the dashboard, please consider restarting the dashboard service and/or
emptying your browser cache.

### Engineering Dashboard

#### Tiles

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to display
Violation data directly from this extension using the Industry
Standard/s tile plugin in v. ≥ 1.18 of the Engineering Dashboard.
See [Engineering Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+tile+management)
for more information.

Clicking on the tile navigates to Risk investigation view and the
specified Industry Standard will be selected in the Health Factor
table. 

## Health Dashboard

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to display
Grade, Compliance, and Violation data directly from this extension using
the Industry Standard/s tile plugin in v. ≥ 1.17 of the Health
Dashboard. See [Health Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Health+Dashboard+tile+management#HealthDashboardtilemanagement-industry)
for more information. Clicking on any of these tiles will display a list
of the rules that have been tagged with the specified standard as
provided by the extension. Compliance percentage is also displayed in a
"bubble".

Example for cmp.json

Configuration to create a "gauge" tile at portfolio level (multi-app
level) to show an OWASP-2017 A1-2017 tile:

``` xml
{
  "id": 1234,
  "plugin": "IndustryStandards",
  "color": "black",
  "parameters": {
    "type": "OWASP-2017",
    "title": "OWASP-2017 A1-2017",
    "widget": "gauge",
    "industryStandard": {
        "id": "1062321",
        "indexID": "1062320",
        "mode": "grade",
        "format": "0.00",
        "description": "OWASP-2017 A1-2017, in grade format"
    }
  }
}
```

Example for app.json

Configuration to create a "number of violations" tile at application
level (single app level) to show an OWASP-2017 A1-2017 tile:

``` xml
{
  "id": 1236,
  "plugin": "IndustryStandard",
  "color": "orange",
  "parameters": {
    "type": "OWASP-2017",    
    "title": "OWASP-2017 A1-2017",
    "industryStandard": {
        "id": "1062321",
        "indexID": "1062320",
        "mode": "violations",
        "format": "0,000",
        "description": "OWASP-2017 A1-2017, in number of violations format" 
    }
  }
}
```

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the dashboards:

### Assessment Model

Various Business and Technical Criteria will be added by the extension:

#### OWASP

| ID      | Name       | Type                |
|---------|------------|---------------------|
| 1062340 | OWASP-2021 | Business Criterion  |
| 1062320 | OWASP-2017 | Business Criterion  |
| 1062300 | OWASP-2013 | Business Criterion  |
#### CWE

| ID | Name | Type |
|---|---|---|
| 1066007 | CWE-Index | Business Criterion |
| 1066006 | CWE-Top25-2024 | Business Criterion |
| 1066005 | CWE-Top25-2023 | Business Criterion |
| 1066004 | CWE-Top25-2022 | Business Criterion |
| 1066003 | CWE-Top25-2021 | Business Criterion |
| 1066002 | CWE-Top25-2020 | Business Criterion |
| 1066001 | CWE-Top25-2019 | Business Criterion |
| 1066000 | CWE-Top25-2011 | Business Criterion |

#### PCI DSS

| ID | Name | Type |
|---|---|---|
| 1063002 | PCI-DSS-V4 | Business Criterion |
| 1063001 | PCI-DSS-V3.2.1 | Business Criterion |
| 1063000 | PCI-DSS-V3.1 | Business Criterion |

### Engineering Dashboard

Out of the box, results are displayed in a specific interface - click
the relevant Assessment Model option to view the results:

![](images/616595493.jpg)

For example, for OWASP 2013 and 2017:

![](images/616595494.jpg)

![](images/616595495.jpg)

### Health Dashboard

Out of the box, no results are provided. Tiles can be configured
manually as described above.

### Security Dashboard

Out of the box, OWASP results are displayed in a specific interface -
click either the OWASP-2013 or OWASP-2017 Assessment Model options
(after clicking the Risk Investigation tile in the Application home
page) to view the results:

![](images/616595498.jpg)

### RestAPI

The RestAPI can be used to query both the Dashboard (AED) and
Measurement (AAD) schemas for results, for example for OWASP results:

![](images/616595499.jpg)
