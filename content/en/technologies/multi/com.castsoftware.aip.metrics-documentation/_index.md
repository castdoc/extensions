---
title: "Metrics Documentation"
linkTitle: "Metrics Documentation"
type: "docs"
---

***

## Extension ID

com.castsoftware.aip.metrics-documentation

## Description

This extension provides a set of .json files containing the
documentation for all metrics/rules provided in the most recent release
of CAST AIP core and in the most recent release of all published CAST
AIP extensions. The extension is published automatically on a weekly
basis (and therefore does not have a set of release notes) and therefore
contains the metrics/rules documentation at the point in time when the
extension is generated.

# In what situation should you install this extension?

This extension exists to resolve a specific issue and therefore CAST
recommends installing this extension only if you are facing this issue.
The issue is as follows: when using the CAST Health, Engineering and
Security Dashboards, metrics/rules documentation is provided within the
dashboard to help understand why a specific grade has been given or why
a specific violation exists. In addition, you can query the RestAPI to
retrieve metrics/rules documentation.

The source of the metrics/rules documentation is as follows:

| Dashboard type | Source |
|-----|-----|
| Health Dashboard                  | Measurement Schema (xxx_MEASURE)                                   |
| Engineering or Security Dashboard | Dashboard schema (xxx_CENTRAL)                                     |
| RestAPI standalone WAR            | Measurement Schema (xxx_MEASURE) or Dashboard schema (xxx_CENTRAL) |

Due to the way in which CAST Imaging Core has been designed, the rule
documentation is updated as follows:

| Schema | Update method |
| ----- | ----- |
| Dashboard schema (xxx_CENTRAL)   | Every time CAST Imaging Core is upgraded to a new release or a new extension is installed, the rule documentation is refreshed, i.e. any changes to the rule documentation will be visible in the Engineering and Security Dashboards or when directly querying the Dashboard schema using the RestAPI.                                                           |
| Measurement Schema (xxx_MEASURE) | The first time a snapshot is uploaded into the Measurement schema, the rule documentation is "pushed" directly from the Dashboard schema into the Measurement schema. Then on each subsequent snapshot upload to the Measurement schema, only documentation for NEW metrics/rules is pushed. Any changes to documentation for existing metrics/rules are ignored. |


This situation is by design, however, it does mean that rule
documentation in the Measurement schema may not match the rule
documentation available in the Dashboard schema and therefore the
display in the Dashboards/RestAPI may be out of sync. If you are facing
this issue and need to resolve it, then you should install this
extension.

## How does it work?

The .json files provided in the extension contain the most recent
version (at the point in time when the extension is generated) of the
metrics/rules documentation for CAST AIP core and all published CAST AIP
extensions. These .json files should be placed in a specific location
within the Dashboard/RestAPI web application file hierarchy on the
Apache Tomcat server. When these .json files exist in the designated
location on the Apache Tomcat server, they will be used to display
metrics/rules documentation, rather than the documentation available in
the Measurement schema, therefore ensuring that a more recent release of
the metrics/rules documentation is provided.

## Compatibility

|  Product | Release | Supported |
|-----|-----|:---:|
| CAST Health Dashboard                  | ≥ 1.6   | :white_check_mark: |
| Combined Health/Engineering Dashboards | ≥ 1.6   | :white_check_mark: |
| CAST Engineering Dashboard             | ≥ 1.6   | :white_check_mark: |
| CAST Security Dashboard                | ≥ 1.6   | :white_check_mark: |
| RestAPI standalone WAR                 | ≥ 1.6   | :white_check_mark: |

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download

You can download the extension from here:
https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.metrics-documentation&version=latest.

## Installation

To install the extension, unpack the downloaded extension and locate the
metrics-documentation.zip file within. Unpack this ZIP file and copy
everything within the ZIP file ("en" folder and the .json files) to the
following location:

``` java
%CATALINA_HOME%\webapps\<war_file>\WEB-INF\metrics-documentation
```

Finally, restart your Apache Tomcat server to ensure the updates are
taken into account.

To ensure that the same metrics/rules documentation is used in all
Dashboard/RestAPI deployments, you should repeat the process described
below for all your deployed WAR files.
