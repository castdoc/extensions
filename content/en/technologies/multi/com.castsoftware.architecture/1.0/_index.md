---
title: "Architecture Discovery - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.architecture

## What's new?

See [Release Notes](rn/).

## Description

The extension provides specific functionality for applications displayed in CAST Imaging, see below for more information:

- Component and Connector view
- Project Structure view

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be handled as follows:

- Onboarding with Fast Scan mode (v2/v3): always automatically downloaded and installed for each application provided that you change your extension strategy to include beta releases.
- Onboarding without Fast Scan mode (v2): always automatically downloaded and installed for each application if:
    - the "Blue Print objective" is enabled
    - and you change your extension strategy to include beta releases.

![](../images/632455621.jpg)

## What results can you expect?

### Component and Connector view

This extension calculates the components for a Component and Connector
view of an application. Those components will then be displayed in the
Service scope in CAST Imaging Viewer. A Component and Connector view
allows you to answer questions, such as:

-   What are the system's principal executing components, and how dothey interact with each other?
-   How does data progress through a system as it is executed?

A Component is a part of the code that can be executed autonomously
and is complete in that regard. Examples:

-   an executable (xxx.exe)
-   a web application on the "client side"
-   a mobile application written in Java+Android or in Swift+Objective-C
-   a database
-   a classic JEE web server
-   a micro service

For more information about component and connector views, see:

-   [https://sites.google.com/site/softwarearchitectureinpractice/9-documenting-software-architecture/c-component-and-connector-c-c-views](https://sites.google.com/site/softwarearchitectureinpractice/9-documenting-software-architecture/c-component-and-connector-c-c-views)
-   [https://www.oreilly.com/library/view/documenting-software-architectures/9780132488617/](https://www.oreilly.com/library/view/documenting-software-architectures/9780132488617/)

Component and Connector views function for all kinds of architecture
types, including microservices architectures and older architecture
styles.

#### Example

We have analyzed the project
[PiggyMetrics](https://github.com/sqshq/piggymetrics/tree/6bb2cf9ddbca980b664d3edbb6ff775d75369278),
whose Component view is provided as part of the project documentation
and we compare this with the Component view calculated by this
extension:

| Component view given by documentation | Component view calculated automatically by CAST |
|---|---|
| ![](../images/666371450.png)  | ![](../images/666371451.png)  |

#### What is extracted?

-   Name: Name of the components are extracted from the source code.
-   Labels: Each component has a list of Labels. Each of those labels
    provides some information about the component. Possible values of
    labels are:

| Labels | Details/Values of labels |
|---|---|
| Technical                                                                              | The component is considered as a non functional one               |
| AWS Lambda                                                                             | The component contains one lambda                                 |
| AWS Cognito                                                                            | The component uses AWS Cognito                                    |
| AWS Event Source XXX, with XXX one of 'S3', 'SNS', 'DynamoDB', 'SQS', 'Api', 'Cognito' | The component has an event source (in triggering) of a given type |
| AWS API Gateway                                                                        | The component uses AWS API Gateway                                |
| Discovery service                                                                      | The component provides a discovery service                        |
| Eureka client                                                                          | The component is an Eureka client                                 |
| Consul client                                                                          | The component is a Consul client                                  |
| Zookeeper client                                                                       | The component is an Zookeeper client                              |
| Configuration service                                                                  | The component is a configuration service                          |
| Admin service                                                                          | The component is an Admin service                                 |
| API Gateway service                                                                    | The component is an API Gateway service                           |
| Zipkin Tracing service                                                                 | The component is a Zipkin Tracing service                         |
| Hystrix (CircuitBreaker)                                                               | \-                                                                |
| Monitoring service                                                                     | The component is a Zipkin Tracing service                         |
| Turbine service                                                                        | The component is a Turbine service                                |
| Oauth Authorization service                                                            | \-                                                                |
| Oauth Resource service                                                                 | \-                                                                |

-   Business words: Each component has a list of business words that
    describes the functionalities in it.
-   Shared objects: The same object, (for example: Java class), may
    belong to several components. This is the case for example of
    utility classes, shared libraries etc.

#### Technical details

This information is extracted from the source code using :

-   analysis of compilation projects (pom.xml, etc...) and folder
    structure
-   techniques from [Static Microservice Architecture Recovery Using
    Model-driven
    Engineering](https://scholar.google.com/citations?view_op=view_citation&hl=nl&user=HN3ekd4AAAAJ&citation_for_view=HN3ekd4AAAAJ:2osOgNQ5qMEC)
    for labels and names
-   business words are extracted from the source code file paths using a
    series of heuristics

#### Current known issues for Component and Connector view

Due to the complex nature of this computation there are potentially many
unknown issues. However, current known limitations are as follows:

-   Test projects will be taken into account leading to components,
    which is not really useful
-   SQL code will be seen as part of its server code, when located
    inside the server, whereas it should be seen as a separate component

### Project Structure view

-   Available in ≥ 1.0.0-beta3.
-   Requires CAST Imaging ≥ 2.17.

This extension extracts projects from an application, i.e. project
files such as Visual Studio .csproj/.vsproj/.vcxproj project files,
or Maven pom.xml files (for example). Projects that are identified
by the extension are displayed in a dedicated Project Structure
scope in CAST Imaging Viewer ≥ 2.17.

#### Example

Using the Shopizer application.

Project Structure scope with selection of projects:

Application scope + Aggregated by Project Structure:

![](../images/632455664.jpg)

Properties panel for a selected node (i.e. a project), showing the
project type (Maven):

![](../images/632455615.png)

The Welcome page in CAST Imaging also includes a dedicated tile
which will open the Project Structure scope:

![](../images/632455626.png)

#### Technical details

Projects taken into account and recognised by this extension are as
follows:

-   Visual Studio projects

    -   \*.vcxproj files
    -   \*.vcproj files
    -   \*.csproj files

-   Node.js

    -   package.json files

-   Maven

    -   pom.xml files

-   Bower

    -   bower.json files

-   Eclipse

    -   .project files

-   Gradle

    -   build.gradle

-   Xcode

    -   \*.pbxproj files

-   IntelliJ

    -   workspace.xml files

-   Python
    -   setup.py files (Setup for Python)
    -   requirements.txt files (Requirements file)

#### Current known issues for Project Structure view

-   When drilling down to Object level from a "project" node, all
    objects are currently displayed (classes/methods). A future release
    of this extension will change this behaviour to ensure only classes
    are displayed.
-   Links correspond to call links between the objects grouped together
    in each "project" node
-   No display of source code on objects