---
title: "Manual configuration of Dashboard tiles using ISO-5055 data"
linkTitle: "Manual configuration of Dashboard tiles using ISO-5055 data"
type: "docs"
---

## Introduction

This documentation explains how to modify the default tiles in the
ISO-5055 view provided out of the box in the CAST Health Dashboard to
use different ISO-5055 metrics. The ISO-5055 view is accessed using the
drop down available at single or multi-application level:

![](images/667320618.jpg)

By default, the tiles in this view use the root ISO-5055 Business
Criterion (ISO-5055), but you can also use any of the ISO-5055
business/technical criterion for the "qualityIndicator" parameter:

- ISO-5055
- ISO-5055-SECURITY
- ISO-5055-RELIABILITY
- ISO-5055-PERFORMANCE-EFFICIENCY
- ISO-5055-MAINTAINABILITY

The customizations explained in this documentation page can be used in
the following Dashboard releases:

-   ≥ 1.27.0-funcrel
-   ≥ 2.1.0-funcrel

## Which tiles can be changed to use alternative ISO-5055 metrics?

### Single-application

| Scenario | Tile |
|---|---|
| Modules Mapping | ![](images/667320617.png) |
| Top 5 Riskiest Modules | ![](images/667320613.png) |
| Technologies Overview | ![](images/667320615.png) |

### Multi-application

| Scenario | Tile |
|---|---|
| Applications map | ![](images/667320614.png) |
| Top 5 Riskiest Modules | ![](images/667320613.png) |


### Single and Multi-Application

| Scenario | Tile |
|---|---|
| Industry Standards tiles | ![](images/667320612.jpg) |
| Trends tiles | ![](images/667320611.png) |

## What about changing other aspects of the tiles?

Other aspects of the tiles can be changed, just as with any other
predefined or custom tile used in the Health Dashboard, for example,
colour, size, title etc. (as described in [Health Dashboard tile
management](../../../../export/DASHBOARDS/Health+Dashboard+tile+management)).
In addition it is also possible to change;

-   parameters \> industryStandard \> mode:
    -   compliance \> At the rules level, the compliance score
        represents 1 minus the ratio between the number of times a rule
        has been violated compared to the number of opportunities that
        the rule could have been violated. The compliance score at
        Technical criteria level is the average compliance score of all
        Rules for the given Technical Criteria.
    -   violations \> number of violations
    -   grade \> grade for the selected criteria
-   parameters \> widget:
    -   gauge
    -   number
    -   donut

## Changing the tiles

To change any of the tiles listed above, locate the following files:

``` java
WAR 1.x
Single application level: CATALINA_HOME\webapps\CAST-Health\portal\resources\app-ISO.json
Multi application level: CATALINA_HOME\webapps\CAST-Health\portal\resources\cmp-ISO.json

WAR ≥ 2.x
Single application level: CATALINA_HOME\webapps\CAST-Health\WEB-INF\classes\config\hd\app-ISO.json
Multi application level: CATALINA_HOME\webapps\CAST-Health\WEB-INF\classes\config\hd\cmp-ISO.json

ZIP ≥ 2.x
Single application level: <unpacked_zip>\configurations\config\hd\app-ISO.json
Multi application level: <unpacked_zip>\configurations\config\hd\cmp-ISO.json
```

Locate the tile configuration in the file, for example, the Modules
Mapping tile at single application level:

``` java
{
    "id": 101,
    "plugin": "QualityIndicatorBySizingMeasureResult",
    "size":"large",
    "color": "purple",
    "parameters": {
        "type":"treemap",
        "title": "Modules Mapping",
        "order": "descending",
        "qualityIndicator": {"id": "ISO-5055", "format": "0.00", "tooltipFormat":"0.00", "description": "ISO-5055"},
        "sizingMeasure": {"id": "LINES_OF_CODE", "format": "0,000.0a", "tooltipFormat":"'0,000", "description": "Lines of Code"}
    }
},
```

Change:

-   the `"qualityIndicator": {"id": "ISO-5055"` entry to the required
    ISO-5055 criterion, for example, ISO-5055-SECURITY
-   and optionally the `"description": "ISO-5055"` entry to match the
    criterion you are using, for example, ISO-5055-SECURITY

The process is the same for all tiles that can be modified:

``` java
{
    "id": 101,
    "plugin": "QualityIndicatorBySizingMeasureResult",
    "size":"large",
    "color": "purple",
    "parameters": {
        "type":"treemap",
        "title": "Modules Mapping",
        "order": "descending",
        "qualityIndicator": {"id": "ISO-5055-SECURITY", "format": "0.00", "tooltipFormat":"0.00", "description": "ISO-5055-SECURITY"},
        "sizingMeasure": {"id": "LINES_OF_CODE", "format": "0,000.0a", "tooltipFormat":"'0,000", "description": "Lines of Code"}
    }
},
```

Save the file and restart the web application to view the changes. For
example:

![](images/667320610.jpg)
