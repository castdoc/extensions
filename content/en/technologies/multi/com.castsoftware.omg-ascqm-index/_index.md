---
title: "ISO-5055 Index"
linkTitle: "ISO-5055 Index"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.omg-ascqm-index

## Description

ISO-5055 is a standard that is published by
ISO: [https://www.iso.org/standard/80623.html](https://www.iso.org/standard/80623.html).

This extension will compute Quality Software Characteristic Measures as
Business Criteria and Quality Measure Elements as Technical Criteria.
All CAST rules that are tagged with a ISO-50555 related tag will
contribute to the various Quality Measure Elements / Quality Software
Characteristic Measures thereby allowing specific compliance ratios,
grades and rule violations to be reported.

## Compatibility

| Product                    | Release  | Supported          |
|----------------------------|----------|:------------------:|
| CAST Imaging Core          | ≥ 8.3.24 / 8.4.x | :white_check_mark: |
| CAST Engineering Dashboard | ≥ 1.5    | :white_check_mark: |
| CAST Health Dashboard      | ≥ 1.17   | :white_check_mark: |
| CAST Security Dashboard    | ≥ 1.18   | :white_check_mark: |

## ISO-5055 version

| Version    | Supported          |
|------------|:------------------:|
| April 2021 | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Imaging Console. If you need to use it, should manually install the
extension.

## Configuration requirements

### Generate a snapshot

A new snapshot must be generated (after the extension is installed)
before results can be viewed. If you do not immediately see changes in
the dashboard, please consider restarting Apache Tomcat/the web
application and/or emptying your browser cache.

## Optional Health Dashboard configuration options

It is possible to modify some of the default tiles provided out of the
box and also add additional tiles in the CAST Health Dashboard for use
with ISO-5055 data. See [Manual configuration of Dashboard tiles using ISO-5055 data](manual-config/) for
more information.

These customizations can be used in the following Health Dashboard
releases:

-   ≥ 1.27.0-funcrel
-   ≥ 2.1.0-funcrel

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the dashboards:

### Assessment Model

Various Business and Technical Criteria will be added by the extension:

#### ISO-5055-Index

| ID      | Name           | Type               |
|---------|----------------|--------------------|
| 1061000 | ISO-5055-Index | Business Criterion |

Note that the ISO-5055-Index Business Criterion has all Technical
Criteria listed below as contributors.

#### ISO-5055-Maintainability

| ID      | Name                     |
|---------|--------------------------|
| 1061001 | ISO-5055-Maintainability |
| 1061046 | CWE-407                  |
| 1061056 | CWE-478                  |
| 1061057 | CWE-480                  |
| 1061058 | CWE-484                  |
| 1061061 | CWE-561                  |
| 1061065 | CWE-570                  |
| 1061066 | CWE-571                  |
| 1061088 | CWE-783                  |
| 1061105 | CWE-1041                 |
| 1061108 | CWE-1045                 |
| 1061110 | CWE-1047                 |
| 1061111 | CWE-1048                 |
| 1061114 | CWE-1051                 |
| 1061115 | CWE-1052                 |
| 1061116 | CWE-1054                 |
| 1061117 | CWE-1055                 |
| 1061121 | CWE-1062                 |
| 1061122 | CWE-1064                 |
| 1061128 | CWE-1074                 |
| 1061129 | CWE-1075                 |
| 1061131 | CWE-1079                 |
| 1061132 | CWE-1080                 |
| 1061135 | CWE-1084                 |
| 1061136 | CWE-1085                 |
| 1061137 | CWE-1086                 |
| 1061138 | CWE-1087                 |
| 1061141 | CWE-1090                 |
| 1061143 | CWE-1093                 |
| 1061145 | CWE-1095                 |

#### ISO-5055-Performance-Efficiency

| ID      | Name                            |
|---------|---------------------------------|
| 1061002 | ISO-5055-Performance-Efficiency |
| 1061044 | CWE-401                         |
| 1061045 | CWE-404                         |
| 1061049 | CWE-424                         |
| 1061085 | CWE-772                         |
| 1061086 | CWE-775                         |
| 1061106 | CWE-1042                        |
| 1061107 | CWE-1043                        |
| 1061109 | CWE-1046                        |
| 1061112 | CWE-1049                        |
| 1061113 | CWE-1050                        |
| 1061118 | CWE-1057                        |
| 1061120 | CWE-1060                        |
| 1061124 | CWE-1067                        |
| 1061126 | CWE-1072                        |
| 1061127 | CWE-1073                        |
| 1061140 | CWE-1089                        |
| 1061142 | CWE-1091                        |
| 1061144 | CWE-1094                        |

#### ISO-5055-Reliability

| ID      | Name                 |
|---------|----------------------|
| 1061003 | ISO-5055-Reliability |
| 1061021 | CWE-119              |
| 1061022 | CWE-120              |
| 1061023 | CWE-123              |
| 1061024 | CWE-125              |
| 1061026 | CWE-130              |
| 1061027 | CWE-131              |
| 1061029 | CWE-170              |
| 1061030 | CWE-194              |
| 1061031 | CWE-195              |
| 1061032 | CWE-196              |
| 1061033 | CWE-197              |
| 1061034 | CWE-248              |
| 1061035 | CWE-252              |
| 1061038 | CWE-366              |
| 1061039 | CWE-369              |
| 1061040 | CWE-390              |
| 1061041 | CWE-391              |
| 1061042 | CWE-392              |
| 1061043 | CWE-394              |
| 1061044 | CWE-401              |
| 1061045 | CWE-404              |
| 1061047 | CWE-415              |
| 1061048 | CWE-416              |
| 1061049 | CWE-424              |
| 1061051 | CWE-456              |
| 1061053 | CWE-459              |
| 1061054 | CWE-476              |
| 1061057 | CWE-480              |
| 1061058 | CWE-484              |
| 1061060 | CWE-543              |
| 1061062 | CWE-562              |
| 1061064 | CWE-567              |
| 1061067 | CWE-595              |
| 1061068 | CWE-597              |
| 1061074 | CWE-662              |
| 1061076 | CWE-667              |
| 1061077 | CWE-672              |
| 1061078 | CWE-681              |
| 1061079 | CWE-682              |
| 1061080 | CWE-703              |
| 1061081 | CWE-704              |
| 1061083 | CWE-758              |
| 1061084 | CWE-764              |
| 1061085 | CWE-772              |
| 1061086 | CWE-775              |
| 1061089 | CWE-786              |
| 1061090 | CWE-787              |
| 1061091 | CWE-788              |
| 1061094 | CWE-805              |
| 1061095 | CWE-820              |
| 1061096 | CWE-821              |
| 1061097 | CWE-822              |
| 1061098 | CWE-823              |
| 1061099 | CWE-824              |
| 1061100 | CWE-825              |
| 1061101 | CWE-833              |
| 1061102 | CWE-835              |
| 1061103 | CWE-908              |
| 1061108 | CWE-1045             |
| 1061114 | CWE-1051             |
| 1061119 | CWE-1058             |
| 1061123 | CWE-1066             |
| 1061125 | CWE-1070             |
| 1061130 | CWE-1077             |
| 1061131 | CWE-1079             |
| 1061133 | CWE-1082             |
| 1061134 | CWE-1083             |
| 1061138 | CWE-1087             |
| 1061139 | CWE-1088             |
| 1061146 | CWE-1096             |
| 1061147 | CWE-1097             |
| 1061148 | CWE-1098             |

#### ISO-5055-Security

| ID      | Name              |
|---------|-------------------|
| 1061004 | ISO-5055-Security |
| 1061010 | CWE-22            |
| 1061011 | CWE-23            |
| 1061012 | CWE-36            |
| 1061013 | CWE-77            |
| 1061014 | CWE-78            |
| 1061015 | CWE-79            |
| 1061016 | CWE-88            |
| 1061017 | CWE-89            |
| 1061018 | CWE-90            |
| 1061019 | CWE-91            |
| 1061020 | CWE-99            |
| 1061021 | CWE-119           |
| 1061022 | CWE-120           |
| 1061023 | CWE-123           |
| 1061024 | CWE-125           |
| 1061025 | CWE-129           |
| 1061026 | CWE-130           |
| 1061027 | CWE-131           |
| 1061028 | CWE-134           |
| 1061030 | CWE-194           |
| 1061031 | CWE-195           |
| 1061032 | CWE-196           |
| 1061033 | CWE-197           |
| 1061035 | CWE-252           |
| 1061036 | CWE-259           |
| 1061037 | CWE-321           |
| 1061038 | CWE-366           |
| 1061039 | CWE-369           |
| 1061044 | CWE-401           |
| 1061045 | CWE-404           |
| 1061049 | CWE-424           |
| 1061050 | CWE-434           |
| 1061051 | CWE-456           |
| 1061052 | CWE-457           |
| 1061055 | CWE-477           |
| 1061057 | CWE-480           |
| 1061059 | CWE-502           |
| 1061060 | CWE-543           |
| 1061063 | CWE-564           |
| 1061064 | CWE-567           |
| 1061065 | CWE-570           |
| 1061066 | CWE-571           |
| 1061069 | CWE-606           |
| 1061070 | CWE-611           |
| 1061071 | CWE-624           |
| 1061072 | CWE-643           |
| 1061073 | CWE-652           |
| 1061074 | CWE-662           |
| 1061075 | CWE-665           |
| 1061076 | CWE-667           |
| 1061077 | CWE-672           |
| 1061078 | CWE-681           |
| 1061079 | CWE-682           |
| 1061082 | CWE-732           |
| 1061085 | CWE-772           |
| 1061086 | CWE-775           |
| 1061087 | CWE-778           |
| 1061088 | CWE-783           |
| 1061089 | CWE-786           |
| 1061090 | CWE-787           |
| 1061091 | CWE-788           |
| 1061092 | CWE-789           |
| 1061093 | CWE-798           |
| 1061094 | CWE-805           |
| 1061095 | CWE-820           |
| 1061096 | CWE-821           |
| 1061097 | CWE-822           |
| 1061098 | CWE-823           |
| 1061099 | CWE-824           |
| 1061100 | CWE-825           |
| 1061102 | CWE-835           |
| 1061104 | CWE-917           |

### Health Dashboard

#### v. ≥ 1.27 / ≥ 2.1.0

A dedicated ISO-5055 view is available allowing you to swap between
tiles showing ISO-5055 data and tiles showing standard data. The
ISO-5055 view is accessed using the drop down available at single or
multi-application level as shown in the screenshots below. By default,
the tiles in this view use the root ISO-5055 Business
Criterion (ISO-5055), and show either of the following data by default:

| Release                    | Data displayed by default | Description               |
|----------------------------|---------------------------|---------------------------|
| ≥ 2.5.x                    | Compliance ratio          | ![](images/667320575.jpg) |
| 2.1.x - 2.4.x and ≥ 1.27.x | Grades                    | ![](images/667320576.jpg) |

The tiles can be modified as follows (see [Manual configuration of Dashboard tiles using ISO-5055 data](manual-config/)) for
more information::
\extensions\content\en\technologies\multi\com.castsoftware.omg-ascqm-index\manual-config
-   to use any of the ISO-5055 business/technical criterion for the "qualityIndicator" parameter:
    -   ISO-5055
    -   ISO-5055-SECURITY
    -   ISO-5055-RELIABILITY
    -   ISO-5055-PERFORMANCE-EFFICIENCY
    -   ISO-5055-MAINTAINABILITY
-   the "mode" can be changed manually to show any of the following, instead of the default:
    -   Grade
    -   Compliance ratio
    -   Number of violations

#### v. 1.26 / 2.0 only

Five ISO-5055 specific tiles will be available at both single and
multi-application level - one for the Business Criteria ISO-5055 Index
and the rest for the four Technical Criteria. In these releases, grade
information is displayed by default.

#### v. ≤ 1.25

No tiles will be provided to display data for this extension, however it
is possible to create tiles manually to display Grade, Compliance, and
Violation data directly from this extension using the Industry
Standard/s tile plugin available in v. ≥ 1.17. Clicking on any of
these tiles will display a list of the rules that have been tagged with
the specified standard as provided by the extension. Compliance
percentage is also displayed in a "bubble".

Examples for cmp.json and app.json:

##### cmp.json

Configuration to create a "gauge" tile at portfolio level (multi-app
level) to show a ISO-5055-Index Business Criterion tile:

``` xml
{
  "id": 1234,
  "plugin": "IndustryStandards",
  "color": "black",
  "parameters": {
    "type": "ISO-5055",
    "title": "ISO-5055-Index",
    "widget": "gauge",
    "industryStandard": {
        "id": "1061000",
        "mode": "grade",
        "format": "0.00",
        "description": "ISO-5055-Index in grade format"
    }
  }
}
```

##### app.json

Configuration to create a "number of violations" tile at application
level (single app level) to show a ISO-5055-Maintainability Business
Criterion tile:

``` xml
{
  "id": 1236,
  "plugin": "IndustryStandard",
  "color": "orange",
  "parameters": {
    "type": "ISO-5055",    
    "title": "ISO-5055-Maintainability",
    "industryStandard": {
        "id": "1061001",
        "mode": "violations",
        "format": "0,000",
        "description": "ISO-5055-Maintainability in number of violations format" 
    }
  }
}
```

### Engineering/Security Dashboard

#### v. ≥ 1.26

An ISO-5055 tile is displayed by default taking data from the Business
Criterion ISO-5055 Index for the Engineering Dashboard and the Technical
Criterion ISO-5055 Security for the Security Dashboard. The number of
violations is shown in both cases:

Drilling down through this tile will take you to the Risk Investigation
view, where the focus will be set to the ISO-5055 Assessment Model (1)
showing only the ISO-5055 metrics (2):

![](images/667320581.jpg)

![](images/667320582.jpg)

#### v. ≤ 1.25

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to
display Violation data directly from this extension using the Industry
Standard/s tile plugin in v. ≥ 1.18 of the Engineering Dashboard.
See [Engineering Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+tile+management) for
more information. Clicking on the tile navigates to Risk investigation
view and the specified Industry Standard will be selected in the Health
Factor table.

In addition, as above, the ISO-5055 Assessment Model will be made
available in the Risk Investigation view.

### RestAPI

The RestAPI can be used to query both the Dashboard (AED) and
Measurement (AAD) schemas for results, for example:

![](images/667320583.jpg)