---
title: "Console Information for Imaging - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.consoleinformation

## What's new?

See [Release Notes](rn/).

## Description

This extension transfers specific application properties such as the LOC
(lines of code) from a file called console_information.json (generated
by CAST Console and stored in the %PROGRAMDATA%/CAST/CAST/CASTMS/LISA
folder on each Node managed by CAST Console) into the application's
"analysis" (or "local") schema during the analysis process.
Subsequently, the information in the "analysis" schema is used by CAST
Imaging following an ETL import, as described below.

### CAST Imaging Viewer

Information is made available in CAST Imaging ≥ 2.16 in the Welcome
Page. Lines of Code (LOC) values are displayed for each Language found
in the delivered source code:

![](../images/620888136.jpg)

-   A new import into CAST Imaging using ≥ 2.16 must be actioned so that
    the properties made available by the extension can be exploited in
    CAST Imaging.
-   If the extension is not installed, CAST Imaging will revert to
    displaying LOC values for each Technology found in the
    delivered source code ("Technologies" are a custom nomenclature and
    can include multiple "Languages").

## Prerequisites

| Product | Release |
|---|---|
| CAST Imaging Console (using the onboarding with Fast Scan mode only) | ≥ 2.7   |
| CAST Imaging Viewer                                                  | ≥ 2.16  |
| CAST Imaging Core                                                    | ≥ 8.3.x / 8.4.x |

## Download and installation instructions

This extension is automatically installed by CAST Imaging Console in ≥
2.8 via a Force Install configuration:

![](../images/620888148.jpg)

## Configuration instructions

None.
