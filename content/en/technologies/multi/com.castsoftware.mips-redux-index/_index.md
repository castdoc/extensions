---
title: "MIPS Reduction Index"
linkTitle: "MIPS Reduction Index"
type: "docs"
---

***

## Extension ID

com.castsoftware.mips-redux-index

## Description

This extension will compute the MIPS (Million Instructions Per
Second) Reduction Index as a Business Criterion with dedicated
supporting Technical Criteria. All CAST rules that are tagged with a
MIPS related tag will contribute to these MIPS specific Business and
Technical Criteria, thereby allowing specific grades and rule violations
to be reported.

## Compatibility

| Product                    | Release  | Supported             |
|----------------------------|----------|:---------------------:|
| CAST Imaging Core          | ≥ 8.3.24 / 8.4.x | :white_check_mark:    |
| CAST Engineering Dashboard | ≥ 1.5    | :white_check_mark:    |
| CAST Health Dashboard      | ≥ 1.17   | :white_check_mark:    |
| CAST Security Dashboard    | \-       | :x:                   |

## Technology compatibility

This extension supports only Mainframe related technologies (Cobol, JCL,
CICS, IMS/DB) and SQL related technologies.

## Download and installation instructions

The MIPS Reduction Index extension will NOT be installed automatically
in any situations, therefore, if you want to use it, you must download
and install it "manually".

## Configuration requirements

### Generate a snapshot

A new snapshot must be generated (after the extension is installed)
before results can be viewed. If you do not immediately see changes in
the dashboard, please consider restarting Apache Tomcat and/or emptying
your browser cache.

### Engineering Dashboard

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to display
Violation data directly from this extension using the Industry
Standard/s tile plugin in v. ≥ 1.18 of the Engineering Dashboard.
See [Engineering Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+tile+management)
for more information.

Clicking on the tile navigates to Risk investigation view and the
specified Industry Standard will be selected in the Health Factor
table. 

### Health Dashboard

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to display
Grade, Compliance, and Violation data directly from this extension using
the Industry Standard/s tile plugin in v. ≥ 1.17 of the Health
Dashboard. See [Health Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Health+Dashboard+tile+management#HealthDashboardtilemanagement-industry)
for more information. Clicking on any of these tiles will display a list
of the rules that have been tagged with the specified standard as
provided by the extension. Compliance percentage is also displayed in a
"bubble".

Example for cmp.json

Configuration to create a "gauge" tile at portfolio level (multi-app
level) to show a "MIPS Reduction - focus on algorithmic costs" tile:

``` xml
{
  "id": 1234,
  "plugin": "IndustryStandards",
  "color": "black",
  "parameters": {
    "type": "MIPS",
    "title": "MIPS Reduction",
    "widget": "gauge",
    "industryStandard": {
        "id": "1062201",
        "indexID": "1062200",
        "mode": "grade",
        "format": "0.00",
        "description": "MIPS Reduction - focus on algorithmic costs, in grade format"
    }
  }
}
```

Example for app.json

Configuration to create a "number of violations" tile at application
level (single app level) to show a "MIPS Reduction - focus on
algorithmic costs" tile:

``` xml
{
  "id": 1236,
  "plugin": "IndustryStandard",
  "color": "orange",
  "parameters": {
    "type": "MIPS",    
    "title": "MIPS Reduction",
    "industryStandard": {
        "id": "1062201",
        "indexID": "1062200",
        "mode": "violations",
        "format": "0,000",
        "description": "MIPS Reduction - focus on algorithmic costs, in number of violations format" 
    }
  }
}
```

## What results can you expect?

### Assessment Model

#### Business Criterion

| ID      | Name           |
|---------|----------------|
| 1062200 | MIPS Reduction |

#### Technical Criteria

| ID      | Name                                                   |
|---------|--------------------------------------------------------|
| 1062201 | MIPS Reduction - focus on algorithmic costs            |
| 1062202 | MIPS Reduction - focus on data access efficiency       |
| 1062203 | MIPS Reduction - focus on avoiding transaction failure |

These Technical Criteria have been populated with rules with an
aggregate weight greater or equal to 5 from the following existing
Technical Criteria:

-   Efficiency - Expensive Calls in Loops
-   Efficiency - SQL and Data Handling Performance
-   Programming Practices - Error and Exception Handling
-   Programming Practices - Unexpected Behavior

Engineering Dashboard

#### ≥ 1.18.0

Results are displayed in a specific interface - click the
"MIPS-Reduction Assessment Model" option to view the results:

![](images/423264463.jpg)

![](images/423264464.jpg)

### Health Dashboard

Out of the box, no results are provided. Tiles can be configured
manually as described above.

### RestAPI

The RestAPI can be used to query both the Dashboard (AED) and
Measurement (AAD) schemas for results, for example:

![](images/435486780.jpg)
