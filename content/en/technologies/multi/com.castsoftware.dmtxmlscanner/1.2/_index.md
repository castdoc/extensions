---
title: "DMT Framework Scanner - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

## Extension ID

com.castsoftware.dmtxmlscanner

## What's new?

See [Release Notes - 1.2](rn).

## Description

The DMT Framework Scanner (com.castsoftware.dmtxmlscanner) extension
automatically generates information for the "Source Code Delivery"
report that is available in the CAST Delivery Manager Tool. The
extension can be downloaded on its own as a standalone extension and is
also shipped and automatically installed with CAST 8.3.x where x ≥ 5.

## What does it do

The extension will scan all source code it encounters during the
packaging action (scan phase) in the CAST Delivery Manager Tool and will
attempt to identify a framework/technology/analyzer based on predefined
patterns provided with the extension. Information about any
framework/technology/analyzer that is identified in the source code will
be provided in the Source Code Delivery report that is available in
the CAST Delivery Manager Tool - this information can then be used, for
example, to install any additional extensions that may be required:

![](../images/382624784.jpg)

### Out of the box framework pattern support

The extension is shipped with a set of predefined pattern configuration
files that are set up to identify the most common frameworks. When the
extension is installed, the "active" pattern configuration files can be
found in the following location:

``` text
<delivery_folder>\dmtxmlscanner-<version>\*.config
```

The following patterns are provided "out of the box" - please check each
.config file to see which frameworks are supported:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Technology/Item</th>
<th class="confluenceTh">Pattern file</th>
<th class="confluenceTh">Notes</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Technology</td>
<td class="confluenceTd">analyzer.config</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Identifies technology types based on file extensions. For
example:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: xml; gutter: false; theme: Confluence"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a>Analyzer_Name=file extensions seperated by &#39;,&#39;(coma) without dot</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>Python=py,pyx,pxd,pxi,pyt,pyw,wsgi,xpy,wscript,pytb,numpy,numpyw,numsc</span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>Cpp=c,cpp,c++,w,pc,cxx,h++,hh,hpp,hxx,tcc,tpp,upc</span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a>JEE=java</span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a>ABAP=abap</span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a>ANTLR=g4</span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a>ASP=aspx,aspx-vb,asp,asax,,ascx,ashx,asmx,axd</span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a>ActionScript=as</span></code></pre></div>
</div>
</div>
<p>This will give the following results, which can be used, for example,
to identify which CAST AIP extensions may need to be installed:</p>
<p><img src="../images/382624785.png" draggable="false"
data-image-src="../images/382624785.png"
data-unresolved-comment-count="0" data-linked-resource-id="382624785"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="AnalyzerForFileExtension.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="382624780"
data-linked-resource-container-version="4" height="150" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">C / C++</td>
<td class="confluenceTd"><p>ccpp_language.config</p></td>
<td class="confluenceTd">Identifies frameworks based on file extensions
and other methods.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">.NET</td>
<td class="confluenceTd"><p>dotnet_language.config</p></td>
<td class="confluenceTd">Identifies frameworks based on file extensions
and other methods.</td>
</tr>
<tr class="even">
<td class="confluenceTd">JEE XML file</td>
<td class="confluenceTd"><p>framework_doctypes.config</p></td>
<td class="confluenceTd">Identifies frameworks based on the URL
referenced in the DOCTYPE element in the XML file.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">JEE XML file</td>
<td class="confluenceTd"><p>framework_xmlnamespaces.config</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Identifies frameworks based on the XMLNS attribute of the first node
in the XML file, or the schema location. For example, the following is
taken from a Spring Framework 3.1 for Spring Beans:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: xml; gutter: false; theme: Confluence"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a>&lt;<span class="kw">beans</span><span class="ot"> xmlns=</span><span class="st">&quot;http://www.springframework.org/schema/beans&quot;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="ot">        xmlns:xsi=</span><span class="st">&quot;http://www.w3.org/2001/XMLSchema-instance&quot;</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="ot">        xmlns:util=</span><span class="st">&quot;http://www.springframework.org/schema/util&quot;</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="ot">        xmlns:jee=</span><span class="st">&quot;http://www.springframework.org/schema/jee&quot;</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a><span class="ot">        xsi:schemaLocation=</span><span class="st">&quot;http://www.springframework.org/schema/beans</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true" tabindex="-1"></a><span class="st">            http://www.springframework.org/schema/beans/spring-beans-3.1.xsd</span></span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true" tabindex="-1"></a><span class="st">            http://www.springframework.org/schema/util</span></span>
<span id="cb2-8"><a href="#cb2-8" aria-hidden="true" tabindex="-1"></a><span class="st">            http://www.springframework.org/schema/util/spring-util-3.1.xsd</span></span>
<span id="cb2-9"><a href="#cb2-9" aria-hidden="true" tabindex="-1"></a><span class="st">            http://www.springframework.org/schema/jee</span></span>
<span id="cb2-10"><a href="#cb2-10" aria-hidden="true" tabindex="-1"></a><span class="st">            http://www.springframework.org/schema/jee/spring-jee-3.1.xsd&quot;</span></span>
<span id="cb2-11"><a href="#cb2-11" aria-hidden="true" tabindex="-1"></a><span class="ot">        default-lazy-init=</span><span class="st">&quot;true&quot;</span>&gt;</span></code></pre></div>
</div>
</div>
<p>The above would be matched by the following predefined pattern:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: xml; gutter: false; theme: Confluence"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a>springbeans;3.1;http://www.springframework.org/schema/beans/spring-beans-3.1.xsd</span></code></pre></div>
</div>
</div>
<div>
<div>
Note that if a version number attribute is found in the XMLNS, then this
number will be recorded, rather than the version number specified in the
*.config file.
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">JEE XML file</td>
<td class="confluenceTd"><p>framework_xmlnodes.config</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Identifies frameworks based on the first XML element in the XML file
if it doesn't have any attributes.</p>
<p>If it is not possible to identify the framework with the first
element in the XML file, then the next child element will be used: this
is targeted at build systems such as Maven and Ant which use an initial
&lt;project&gt; element with attributes and then a child element called
&lt;target&gt;, take for example this Ant XML file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: xml; gutter: false; theme: Confluence"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a>&lt;<span class="kw">project</span><span class="ot"> name=</span><span class="st">&quot;simpleCompile&quot;</span><span class="ot"> default=</span><span class="st">&quot;deploy&quot;</span><span class="ot"> basedir=</span><span class="st">&quot;.&quot;</span>&gt;</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>   &lt;<span class="kw">target</span><span class="ot"> name=</span><span class="st">&quot;init&quot;</span>&gt;</span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>      &lt;<span class="kw">property</span><span class="ot"> name=</span><span class="st">&quot;sourceDir&quot;</span><span class="ot"> value=</span><span class="st">&quot;src&quot;</span><span class="er">/</span> &gt;</span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a>      &lt;<span class="kw">property</span><span class="ot"> name=</span><span class="st">&quot;outputDir&quot;</span><span class="ot"> value=</span><span class="st">&quot;classes&quot;</span> /&gt;</span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a>      &lt;<span class="kw">property</span><span class="ot"> name=</span><span class="st">&quot;deployJSP&quot;</span><span class="ot"> value=</span><span class="st">&quot;/web/deploy/jsp&quot;</span> /&gt;</span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a>      &lt;<span class="kw">property</span><span class="ot"> name=</span><span class="st">&quot;deployProperties&quot;</span><span class="ot"> value=</span><span class="st">&quot;/web/deploy/conf&quot;</span> /&gt;</span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a>   &lt;/<span class="kw">target</span>&gt;</span></code></pre></div>
</div>
</div>
<p>To match the above, use the following configuration in the .config
file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb5"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: xml; gutter: false; theme: Confluence"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a>ant;;project/target</span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Java</td>
<td class="confluenceTd">java_language.config</td>
<td class="confluenceTd">Identifies frameworks based on file extensions
and other methods.</td>
</tr>
<tr class="even">
<td class="confluenceTd">PHP</td>
<td class="confluenceTd">php_language.config</td>
<td class="confluenceTd">Identifies frameworks based on file extensions
and other methods.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Python</td>
<td class="confluenceTd">python_language.config</td>
<td class="confluenceTd">Identifies frameworks based on file extensions
and other methods.</td>
</tr>
<tr class="even">
<td class="confluenceTd">Web related (AngularJS, jQuery etc.)</td>
<td class="confluenceTd">web_language.config</td>
<td class="confluenceTd">Identifies frameworks based on file extensions
and other methods.</td>
</tr>
</tbody>
</table>

### Customizing framework support

If you would like to customize the list of frameworks identified by the
extension, you can do so by modifying the appropriate \*.config file
located here:

``` text
<delivery_folder>\dmtxmlscanner-<version>\*.config
```

This will modify the extension in the current Delivery folder for all
Applications managed in that Delivery folder. The majority of \*.config
files use the following format:

``` xml
fileExtensions=<semi-colon_separated_list>
<framework_name1>;<version>;<framework_identifier_pattern>
<framework_name2>;<version>;<framework_identifier_pattern>
etc.
```

## framework_xmlnamespaces.config

When using the framework_xmlnamespaces.config to match XML namespaces,
you can use a "wildcard" pattern when you want to match multiple
different frameworks in the same XML file, as follows:

``` xml
<framework>;x.y.z;<framework_identifier_pattern>
```

For example, to avoid entering the following in the .config file to
match the specific versions of the ActiveMQ framework:

``` xml
activemq;4.1.1;http://activemq.apache.org/schema/core/activemq-core-4.1.1.xsd
activemq;4.1.2;http://activemq.apache.org/schema/core/activemq-core-4.1.2.xsd
```

you can use the following to match all versions of the framework:

``` xml
activemq;x.y.z;http://activemq.apache.org/schema/core/activemq-core-x.y.z.xsd
```

### XML file size limit

All XML files over 1MB in size will be ignored - this is to
avoid performance issues when very large XML files are present in the
source code.

## Compatibility

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><div>
<div>
CAST Imaging Core
</div>
</div></th>
<th class="confluenceTh" style="text-align: center;"><div>
<div>
Supported
</div>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd">8.3.x where x ≥ 5</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
</tbody>
</table>

## Download and installation instructions

-   The extension will not be automatically downloaded and installed in
    CAST Imaging Console. If you need to use it, should manually
    install the extension.

## CAST Delivery Manager Tool GUI

The extension provides two CAST Delivery Manager Tool "discoverers",
these can be seen below:

![](../images/382624783.png)

|                     |                                                                                                                      |
|---------------------|----------------------------------------------------------------------------------------------------------------------|
| Discoverer          | Description                                                                                                          |
| Source file scanner | Scans file extensions only as defined in the .config files named \<technology\>\_language.config and analyzer.config |
| XML scanner         | Scans only XML files as defined in the .config files named framework\_\<type\>.config                                |

  

-   If you have existing packages:
    -   and this extension (nor its predecessor the DMT XML Scanner 1.0)
        have been installed before, then both discoverers will not be
        automatically activated (i.e. ticked). If you would like to
        benefit from the information provided by the extension in the
        Source Code Delivery report, please enable them before the next
        package action.
    -   and the extension's predecessor (DMT XML Scanner 1.0) has been
        installed before, then the XML scanner discoverer will be
        ticked, but the Source file scanner will not.
-   For all packages created after the installation of the extension,
    both discoverers will be automatically activated (i.e. ticked).

## What results can you expect?

The results of the source code scan can be seen in the Source Code
Delivery report in the CAST Delivery Manager Tool:

![](../images/382624784.jpg)

This report is an XML file and a specific section is dedicated to
reporting any frameworks/versions that have been identified in the
source code, for example:

![](../images/382624782.png)

![](../images/382624781.png)
