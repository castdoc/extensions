---
title: "Release Notes - 1.2"
linkTitle: "Release Notes"
type: "docs"
---

***

## 1.2.3-funcrel

<table>
  <tbody>
    <tr>
      <th>Internal ID</th>
      <th>Call ID</th>
      <th>Description</th>
    </tr>
    <tr>
      <td>SCANFWK-112</td>
      <td>-</td>
      <td>
        <p>When a framework that was not shipped "out of the box" with the extension was detected during an analysis, a warning was added to the log stating:</p>
        <pre>The xmlns "&lt;example&gt;" has been found in "example.xml" but it's not recognized. You can add it to the configuration in the extension as explained in the documentation.Please report this case to CAST so that it could be integrated in next release of the extension.</pre>
        <p>This warning is no longer visible in the analysis log.</p>
      </td>
    </tr>
  </tbody>
</table>

## 1.2.2-funcrel

### Updates

By default, the XMLS that are within the first node of the xml file are
scanned. If you want to scan a particular framework from all the nodes
in the XML file you need to add another parameter at the end of that
framework in the framework_xmlnamespaces.config file.

The 4th parameter can be added in the framework_xmlnamespaces.config
file, to avail the below functions:

-   rootNodeOnly (Default): Scan frameworks only in the first XML node
-   allNodes: Scan the full XML file for a particular framework

![](../382624787.png)

## 1.2.1-funcrel

### Resolved issues

|             |         |                                                                                                                                            |
|:------------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------------|
| Internal ID | Call ID | Description                                                                                                                                |
| SCANFWK-101 | 15915   | Error during Packaging- Scan Step- DMT Framework Scanner 1.2.0 - "java.lang.StringIndexOutOfBoundsException:String index out of range: -1" |

## 1.2.0-funcrel

### Updates

One new config file has been added (analyzer.config) to detect specific
technologies based on file extensions:

``` xml
Analyzer_Name=file extensions seperated by ','(coma) without dot
Python=py,pyx,pxd,pxi,pyt,pyw,wsgi,xpy,wscript,pytb,numpy,numpyw,numsc
Cpp=c,cpp,c++,w,pc,cxx,h++,hh,hpp,hxx,tcc,tpp,upc
JEE=java
ABAP=abap
ANTLR=g4
ASP=aspx,aspx-vb,asp,asax,,ascx,ashx,asmx,axd
ActionScript=as
```

This will give the following results, which can be used, for example, to
identify which CAST AIP extensions may need to be installed:

![](../382624788.png)

### Resolved issues

None.