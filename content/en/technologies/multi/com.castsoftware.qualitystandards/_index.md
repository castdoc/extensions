---
title: "Quality Standards Mapping"
linkTitle: "Quality Standards Mapping"
type: "docs"
---

***

## Exension ID

com.castsoftware.qualitystandards

## In what situation should you install this extension?

This extension is required in the following situations:

-   If you would like to view CISQ, OWASP, CWE and other quality
    standards data in the CAST dashboards (Health, Engineering and
    Security) in dedicated custom tiles, you should install this
    extension.
-   If you would like to use the official CISQ, OWASP and CWE templates
    provided with the CAST Report Generator.
-   If you would like to create custom templates that uses CISQ, OWASP,
    CWE and other quality standards data in the CAST Report Generator.

## How does it work?

The extension provides a set of tags that identify the relevant quality
standards data in the snapshot data:

-   When using the CAST Report Generator, these tags can be used in
    report templates (supported templates are delivered with tags
    pre-defined, however you can also use the tags in your own custom
    templates)
-   When using the CAST dashboards, custom tiles can be configured using
    these tags - note that some tiles using some tags are provided
    "out-of-the-box" in the CAST Security Dashboard.

## Available tags

Tags provided by this extension are listed in the Standards section
of [https://technologies.castsoftware.com/](https://technologies.castsoftware.com/).

## Compatibility

| Product               | Release | Supported          | Notes                                                                                           |
| --------------------- | ------- |:------------------:| ----------------------------------------------------------------------------------------------- |
| CAST Imaging Core     | ≥ 8.3.x / 8.4.x | :white_check_mark: | -                                                                                               |
| CAST AIP for Security | ≥ 1.0   | :white_check_mark: | -                                                                                               |
| CAST Dashboards       | ≥ 1.6.0 | :white_check_mark: | Supported dashboards: CAST Health Dashboard, CAST Engineering Dashboard,CAST Security Dashboard |
| CAST Report Generator | ≥ 1.7.0 | :white_check_mark: | -                                                                                               |

## Download and installation instructions

### Step 1 - Download and installation

This extension is automatically installed by CAST Imaging Console in ≥
2.8 via a Force Install configuration.

### Step 2 - Add the tile configuration to your deployed Dashboard

-   This step is not required when using the extension with CAST Report
    Generator templates.
-   Some tiles using some tags are provided "out-of-the-box" in the CAST
    Security Dashboard.

#### CAST Health Dashboard

See [Health Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Health+Dashboard+tile+management) -
specifically the section Custom Tiles.

#### CAST Engineering / Security Dashboard

See [Engineering Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+tile+management) -
specifically the section Custom Tiles.

### Step 3 - Upload snapshot to Measurement schema

If you are configuring custom tiles using Quality Standards tags in the
Health Dashboard, you will need to upload the most recent snapshot to
the Measurement schema in order that the new tags are transferred (the
Quality Standards Mapping extension (i.e. extensions in general) are
only installed to the Analysis and Dashboard schemas). To do so, run the
Consolidate Snapshot action on the most recent snapshot:

![](images/667320713.jpg)

### Step 4 - Reload the cache or log out / log in

To ensure that the most up-to-date information is available in the
dashboards, ensure you either [reload the
cache](https://doc.castsoftware.com/display/DASHBOARDS/Reload+the+cache) or
alternatively, log out and log back in.

## What results can you expect?

### CAST dashboards

To view the new tiles, refresh your browser. If you do not see the
tiles, you may need to empty your browser cache. The tiles will be
displayed as follows (example for the CISQ-Reliability tag in the CAST
Health Dashboard):

![](images/667320714.png)

Clicking the tile will drill down as follows:

*Application level (click to enlarge):*

![](images/667320715.png)

*Portfolio level (click to enlarge):*

![](images/667320716.png)

### CAST Report Generator

You can use any [predefined standards
templates](https://doc.castsoftware.com/display/DOCCOM/CAST+Report+Generator+-+Templates+and+output+options)
requiring the Quality Mapping extension to generate reports or use any
of the available tags in your own custom templates.