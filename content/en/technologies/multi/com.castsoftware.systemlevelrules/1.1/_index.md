---
title: "System Level Rules - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.systemlevelrules

## What's new

See [Release Notes](rn/).

## Description

This extension provides "system level rules" for several of the main technologies supported in CAST Imaging. These rules are provided as an
addition to the rules already present for each of these technologies.

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-:|:--:|
| :x: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in order to function correctly. The System Level Rules extension requires
that the following other CAST extensions are also installed:

-   [com.castsoftware.sqlanalyzer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.sqlanalyzer&version=latest)
-   Internal component (com.castsoftware.xset)

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, should manually install the extension.

## What results can you expect?

### Rules provided by the extension

| Release | Rules |
|---|---|
| 1.1.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=\|\|1.1.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=%7C%7C1.1.5-funcrel) |
| 1.1.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=\|\|1.1.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=%7C%7C1.1.4-funcrel) |
| 1.1.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=\|\|1.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=%7C%7C1.1.3-funcrel) |
| 1.1.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=\|\|1.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=%7C%7C1.1.2-funcrel) |
| 1.1.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=\|\|1.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=%7C%7C1.1.1-funcrel) |
| 1.1.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=\|\|1.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=%7C%7C1.1.0-funcrel) |
| 1.1.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=\|\|1.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_systemlevelrules&ref=%7C%7C1.1.1-funcrel) |
