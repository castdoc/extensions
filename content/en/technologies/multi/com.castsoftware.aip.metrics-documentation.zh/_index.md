---
title: "Chinese Metrics Documentation"
linkTitle: "Chinese Metrics Documentation"
type: "docs"
---

***

## Extension ID

com.castsoftware.aip.metrics-documentation.zh

## Description

This extension provides a set of .json files containing the Chinese
(Simplified Chinese) language version of the documentation for
metrics/rules provided in the most recent release of CAST Imaging Core
and in the most recent release of all published CAST extensions. This
documentation has been translated from the original English with the
[DeepL API](https://www.deepl.com/home) an automated
translator based on deep learning.

This initial release of the extension provides Chinese language
documentation for a subset of the CAST rules:  the current scope is
targeted at Applications developed with JEE, HTML5 and SQL technologies,
and includes Chinese language rules and other metrics (sizing measures,
business criteria. etc.).

## In what situation should you install this extension?

This extension displays the metrics/rules documentation in Chinese
language for the CAST Health, Engineering and Security Dashboards, REST
API, and Report Generator.

## How does it work?

The .json files provided in the extension contain the most recent
version (at the point in time when the extension is generated) of the
metrics/rules documentation for  CAST Imaging Core and all published
CAST AIP extensions. These .json files should be placed in a specific
location within the Dashboard/RestAPI web application file hierarchy on
the Apache Tomcat server. When these .json files exist in the designated
location on the Apache Tomcat server, they will be used to display
metrics/rules documentation, rather than the documentation available in
the Measurement schema, therefore ensuring that a more recent release of
the metrics/rules documentation is provided.

## Compatibility

| Product  | Release |  Supported |
|-----|:---:|:---:|
| CAST Health Dashboard                  | ≥ 1.6   | :white_check_mark: |
| Combined Health/Engineering Dashboards | ≥ 1.6   | :white_check_mark: |
| CAST Engineering Dashboard             | ≥ 1.6   | :white_check_mark: |
| CAST Security Dashboard                | ≥ 1.6   | :white_check_mark: |
| RestAPI standalone WAR                 | ≥ 1.6   | :white_check_mark: |

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download

You can download the extension here:
[https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.metrics-documentation.zh&version=latest](https://extend.castsoftware.com/#/extension?id=com.castsoftware.aip.metrics-documentation.zh&version=latest).

## Installation

To install the extension, unpack the downloaded extension and locate the
metrics-documentation.zip file within. Unpack this ZIP file and copy
everything within the ZIP file ("zh" folder and the .json files) to the
following location:

``` java
%CATALINA_HOME%\webapps\<war_file>\WEB-INF\metrics-documentation
```

Finally, restart your Apache Tomcat server to ensure the updates are
taken into account.

To ensure that the same metrics/rules documentation is used in all
Dashboard/RestAPI deployments, you should repeat the process described
above for all your deployed WAR files.
