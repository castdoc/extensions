---
title: "Security Analyzer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.securityanalyzer

## What's new?

See [Release Notes](rn/).

## Technical information

When installed, this extension replaces the Security Analyzer embedded
in CAST Imaging Core:

-   The Security Analyzer embedded in CAST Imaging Core will continue to
    exist and will be shipped "out of the box" with CAST Imaging Core.
-   Critical bugs will continue to be fixed in the Security Analyzer
    embedded in CAST Imaging Core but no new features or functionality
    will be added.
-   The Security Analyzer extension will have exactly the same features
    and functionality on release as the Security Analyzer embedded in
    CAST Imaging Core, therefore analysis results will be identical.
-   The Security Analyzer is compatible with CAST Imaging Core ≥ 8.3.44.
-   All future development of the Security Analyzer (new features,
    functionality etc.) will be completed in the Security Analyzer
    extension only. Critical bug fixes will be fixed in the Security
    Analyzer extension (as well as the analyzer embedded in CAST Imaging
    Core).
-   The behaviour is as follows:
    -   Nothing is automatic - for both CAST Imaging Console and
        "legacy" CAST deployments, the Security Analyzer extension must
        be manually downloaded and installed in order to use it.
    -   If the extension is installed, CAST Imaging Console will
        automatically detect that it exists and will use the extension
        rather than the analyzer embedded in CAST Imaging Core.
    -   Once the extension has been installed and used to produce
        analysis results, it is not possible to reverse this choice by
        removing the extension and re-analyzing the source code again.

## In what situation should you install this extension?

You should install this extension when you want to detect improper user
input validation, API calls (REST, JMS, etc.), second order injections,
hard-coded elements, correct values for encryption APIs, and more in
your application source code, which can lead to the following security
vulnerabilities:

-   SQL Injection (CWE-89)
-   Cross-Site Scripting (CWE-79)
-   LDAP Injection (CWE-90)
-   OS Command Injection (CWE-78)
-   XPath Injection (CWE-91)
-   Path Manipulation (CWE-99)
-   Avoid Log forging vulnerabilities (CWE-117)
-   Avoid uncontrolled format string (CWE-134)
-   Trust Boundary Violation (CWE-501)
-   Sensitive Cookie in HTTPS Session Without 'Secure' Attribute
    (CWE-614)
-   Use of hard-coded credential (java, C#, VB.Net languages) (CWE-798)

In addition, the extension provides additional rules, and it computes
(for JEE and .NET only) a large set of security rules, requiring
dataflow technology. Detailed information about how the Security
Analyzer functions can be found in [Application - Security
Dataflow](https://doc.castsoftware.com/display/AIPCONSOLE/Application+-+Security+Dataflow).

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| ≥ v2/8.3.44 | Microsoft Windows | :white_check_mark: |

## Supported technologies

| Technology | Supported |
|------------|:---------:|
| JEE        | :white_check_mark: |
| .NET       | :white_check_mark: |

## Prerequisites

User Input Security analyses require a significant of free RAM memory on the target node - see [CAST AIP for Dashboards - Hardware
requirements](https://doc.castsoftware.com/display/SIZING/CAST+AIP+for+Dashboards+-+Hardware+requirements).

## Download and install the extension

The Security Analyzer extension must be downloaded manually using the Available Extensions interface in CAST Imaging Console.

## Rules provided by the extension

- [1.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_securityanalyzer&ref=%7C%7C1.1.1-funcrel)
- [1.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_securityanalyzer&ref=%7C%7C1.1.0-funcrel)
- [1.1.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_securityanalyzer&ref=%7C%7C1.1.0-beta1)

Other rules calculated by the Security Analyzer are provided in CAST Imaging Core.
