---
title: "OMG Technical Debt - an explanation"
linkTitle: "OMG Technical Debt - an explanation"
type: "docs"
---

## Introduction

Technical Debt (OMG) is calculated as the amount of effort required to
fix the structural quality problems in an application that, if left
unfixed, put the business at serious risk. Like financial debt,
Technical Debt incurs interest in the form of the extra effort it takes
to maintain and enhance an application due to the structural quality
flaws in the code.

Technical Debt (OMG) follows the OMG specification. For more information
on the specification, please visit
- [https://www.omg.org/spec/ATDM/About-ATDM/](https://www.omg.org/spec/ATDM/About-ATDM/).

## Background of Technical Debt

The Technical Debt metaphor was introduced by Ward Cunningham to
describe how sub-optimal design decisions, often made to meet schedules,
accumulated a debt that had to be repaid through corrective maintenance
during future releases.

The violations of choosing "debt" as a metaphor engages a set of
financial concepts that help executives think about software quality in
business terms. The components that comprise Technical Debt provide a
foundation for the economics of software quality. The metaphor can be
partitioned into the following elements:

-   Technical Debt - Future costs attributable to known structural
    weaknesses in production code that must be fixed. Technical Debt
    includes both the debt’s principal and interest. A weakness in
    production code is only included in Technical Debt calculations if
    those responsible for the application believe it is a ‘must-fix’
    problem, therefore incurring corrective maintenance costs in a
    future release. Technical Debt is a primary component of the cost of
    application ownership.
-   Principal - The cost of remediating must-fix problems in production
    code. At a minimum, the principal is calculated from the number of
    hours required to remediate these problems, multiplied by the fully
    burdened hourly cost of those involved in designing, implementing,
    and unit testing these fixes.
-   Interest - Continuing costs, primarily in IT, attributable to
    must-fix problems so long as they remain in production code. These
    ongoing costs can result from the excessive effort to modify
    unnecessarily complex code, greater resource usage by inefficient
    code, etc.
-   Business Risk – Potential costs to the business if must-fix problems
    in production code cause damaging operational events such as
    outages, data corruption, performance degradation, and security
    breaches.
-   Liability - Costs to the business resulting from operational
    problems caused by flaws in production code. These flaws include
    both must-fix problems included in the calculation of Technical Debt
    as well as problems not listed as must-fix because their risk was
    underestimated.
-   Opportunity Cost - Benefits such as revenue from new features that
    could have been achieved had resources been committed to developing
    new capability rather than being assigned to retire Technical Debt.
    Opportunity costs represent the trade-off that application managers
    and executives must weigh when deciding how much effort to devote to
    retiring Technical Debt.

## CISQ Quality Characteristic Measures

[CISQ](https://www.it-cisq.org/) has developed a
specification for Technical Debt that is designed to predict corrective
maintenance costs and related factors to guide IT decisions and resource
allocations. The CISQ measure of Technical Debt builds on the existing
four OMG standards CISQ has developed for measuring the structural
quality of software.

The Automated Technical Debt Measure (ATDM) is calculated from
occurrences of the 86 weaknesses that are included in the 4 CISQ Quality
Characteristic measures. Detecting and counting these weaknesses is the
starting point for calculating ATDM. The CISQ Quality Characteristic
measures consist of the following approved specifications of the OMG:

-   Automated Source Code Reliability Measure (ASCRM) - violations of
    good architectural and coding practice that can cause outages,
    delayed recovery, data corruption, and unpredictable operational
    behavior.
-   Automated Source Code Security Measure (ASCSM) - violations of good
    architectural and coding practice in an application that allow
    unauthorized intrusion into the application’s source code, data
    store, operations, or connections.
-   Automated Source Code Performance Efficiency Measure (ASCPEM) -
    violations of good architectural and coding practice that can result
    in slow response, degraded performance, or excessive use of
    computational resources.
-   Automated Source Code Maintainability Measure (ASCMM) - violations
    of good architectural and coding practice that make an application’s
    source code difficult to understand or modify

For more information about CISQ patterns, please visit
- [https://www.it-cisq.org/standards/code-quality-standards/](https://www.it-cisq.org/standards/code-quality-standards/)

## How OMG specified Technical Debt measure is calculated

The calculation of OMG specified Technical Debt requires the
installation of an extension [OMG Automated or Contextual Technical Debt Measure](_index/)
and the generation of a new snapshot with this extension installed.

Release ≥ 2.x of this extension uses CTDM (Contextual Technical Debt
Measure) to calculate Technical Debt values. CTDM is a recognized
nomenclature for a customized ATDM (Automated Technical Debt Measure) as
described in chapter 6.3 of the OMG ATDM specification 1.0,
see: [https://www.omg.org/spec/ATDM/1.0/PDF](https://www.omg.org/spec/ATDM/1.0/PDF).
For this extension, CTDM uses a combination of remediation effort
produced by the ISO-5055 measure and remediation effort produced by CAST
Imaging Core.

Technical Debt measure is calculated as the total effort required to
remediate all CISQ patterns. A Pattern (or Source Code Pattern)
designates a set of elements and their relationships that can be
detected through automated matching of the pattern description with
structures in the source code.

A pattern remediated effort is derived by summing up all the occurrence
remediation effort. An occurrence (or Pattern Occurrence) designates a
single instance of a Source Code Pattern (or Pattern) representing a
weakness that has been implemented in the measured software (ASCMM,
ASCRM, ASCPEM, ASCSM):

![](../images/404488246.jpg) 

The occurrence remediation effort is the multiplication of occurrence
raw remediation effort and adjustment factor. The occurrence raw
remediation effort has been estimated based on survey results and is
fixed input to the calculation engine. The adjustment factor
contextualizes the occurrence remediation effort based on various
criteria as described below.

-   Complexity - or Effort Complexity – of the code elements
    implementing an Occurrence is qualification information that is
    measured according to the Effort Complexity definition from the
    Automated Enhancement Points (AEP) specification. (AEP)
-   Exposure - of code elements propagating effects of the occurrence to
    the rest of the software. Based on the extent of propagation,
    remediating the occurrence could involve direct references to code
    elements (measured as the code elements' number of distinct direct
    callers), or indirect references (measured as the number of distinct
    call paths leading to the code elements).
-   Technological diversity - the number of the languages in which
    elements in the source code pattern of a specific occurrence are
    instantiated.
-   Concentration - Concentration is qualification information that
    measures the number of Occurrences within any Code Element in the
    software. Total number of occurrences of any source code patterns
    within a single code element (e.g., class, module, component,
    subroutine, etc.).
-   Evolution status - The Evolution Status of an Occurrence and of code
    elements implementing an Occurrence is qualification information
    which indicates if the Occurrence or the code elements implementing
    an Occurrence have been added, updated, or deleted between measured
    revisions of the software. Changes and evolution both of code
    elements in the occurrence and of code elements constituting the
    immediate software environment within which the occurrence is
    embedded.
-   Occurrence Gap Size - In the context of patterns which rely on roles
    that model values and threshold values that are not to be exceeded,
    the gap between these values must be closed to remediate this
    weakness; the Occurrence Gap Size is the extent of the gap, measured
    as the difference between the values and the thresholds.
