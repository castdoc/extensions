---
title: "OMG Automated or Contextual Technical Debt Measure - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.omg-atdm

## What's new?

See [Release Notes](rn/).

## Description

Release ≥ 2.x of this extension uses CTDM (Contextual Technical Debt
Measure) to calculate Technical Debt values. CTDM is a recognized
nomenclature for a customized ATDM (Automated Technical Debt Measure) as
described in chapter 6.3 of the OMG ATDM specification 1.0,
see: [https://www.omg.org/spec/ATDM/1.0/PDF](https://www.omg.org/spec/ATDM/1.0/PDF).
For this extension, CTDM uses a combination of remediation effort
produced by the ISO-5055 measure and remediation effort produced by CAST
Imaging Core. This means that the number of rules taken in to account by
this release of the extension is higher than in release 1.x of the
extension (ATDM), which only considered "CISQ" Business Criteria. As a
direct result, Technical Debt values will be higher for release ≥ 2.x of
the extension and are therefore not comparable with release 1.x. You can
see a list of Technical Criteria used by the extension below.

The technical debt values are computed for all violations which have an
entry with a remediation effort at Business Criteria or Rule
level. Effort is taken in to account in the following order:

-   Rule 
-   CISQ index (if
    the [com.castsoftware.cisq-index](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cisq-index&version=latest)
    extension is installed) 
-   ISO-5055 index
-   CAST Imaging Core (TQI)

Therefore:

-   If an effort exists at rule level, then this effort is applied for
    the rule 
-   If an effort exists at CISQ level then this effort is applied 
-   if an effort exists for the rule at ISO-5055 level then this effort
    is applied
-   otherwise effort from CAST Imaging Core is applied.

Technical Debt is calculated as follows: at the object/rule level and
then aggregated at Rule, Technical Criterion, Business Criterion, Module
and Application level. The details at rule level are computed on demand
by default, this means the results are not stored at the object and rule
level - this is to reduce the volume of generated data and to improve
performance.

Note that this extension is not a replacement for the built-in Technical
Debt measures included in CAST Imaging Core "out-of-the box". Indeed
both the existing Technical Debt measures and this extension can be used
at the same time. In other words, the installation of this extension
does not mean that the calculation of the existing Technical Debt
measures will be disabled.

## Compatibility and requirements

| Product | Release | Required | Notes |
|---|---|:-:|---|
| CAST Imaging Core | ≥ 8.3.34 / 8.4.x | :white_check_mark: | For 8.3.34 - 8.3.36, ISO remediation efforts are not available, however, default ISO remediation efforts (as provided in 8.3.37) will be used instead. |
| Dashboard/RestAPI | ≥ 2.3.1 | :white_check_mark: | \- |
| [com.castsoftware.omg-ascqm-index](https://extend.castsoftware.com/#/extension?id=com.castsoftware.omg-ascqm-index&version=latest) | Any | Recommended | If the ISO-5055 extension is not installed, only the Technical Criteria from CAST Imaging Core will be used to generate the Technical Debt values. |
| [com.castsoftware.cisq-index](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cisq-index&version=latest) | Any | Optional | Only required if you want to ALSO view ATDM values (which are based on CISQ Index) as in version 1.x. |

## OMG-ATDM version

| Version | Supported |
|---|:-:|
| 2.0 (October 2021) | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Imaging Console. If you need to use it, should manually install the
extension.

## Assessment Model

The extension calculates the following metrics as Sizing Measures:

| Metric ID | Name | Description |
|---|---|---|
| 1062010 | OMG-ATDM: Number of occurrences | An occurrence (or Pattern Occurrence) designates a single instance of a Source Code Pattern (or Pattern) representing a weakness that has been implemented in the measured software.  This sizing measure keeps, per snapshot, the number of occurrences per object, rule, Technical Criterion and Business Criterion.<br><br>ATDM is calculated as defined with CISQ patterns:<br><br><ul><li>ASCMM</li><li>ASCRM</li> <li>ASCPEM</li><li>ASCSM</li></ul>CTDM is calculated as defined with CAST TQI patterns and ISO-5055 patterns  |
| 1062011 | OMG-ATDM: Complexity | The Complexity - or Effort Complexity - of the code elements implementing an Occurrence is qualification information that is measured according to the Effort Complexity definition from the Automated Enhancement Points (AEP) specification. (AEP). |
| 1062012 | OMG-ATDM: Exposure | The Exposure of an Occurrence is qualification information that measures the level of connectedness of the Occurrence with the rest of the software, both directly and indirectly through call paths. |
| 1062013 | OMG-ATDM: Concentration | Concentration is qualification information that measures the number of Occurrences within any Code Element in the software. |
| 1062014 | OMG-ATDM: Technological Diversity | The Technological Diversity of an Occurrence is qualification information that measures the number of distinct programming languages in which the code elements included in a single occurrence of a source code pattern are written. |
| 1062015 | OMG-ATDM: Gap Size | In the context of patterns which rely on roles that model values and threshold values that are not to be exceeded, the gap between these values must be closed to remediate this weakness; the Occurrence Gap Size is the extent of the gap, measured as the difference between the values and the thresholds. |
| 1062016 | OMG-ATDM: Adjustment Factor | Adjustment Factor is computed based on qualification measures. |
| 1062020 | OMG-ATDM: Adjusted Remediation Effort | Remediation Effort designates the time required to remove an occurrence – or a set of occurrences – of a Technical Debt Item from the software. It covers the coding activity as well as unit/non-regression testing activities. |
| 1062030 | OMG-ATDM: Remediation Effort ADDED | - |
| 1062032 | OMG-ATDM: Remediation Effort DELETED | - |

## How are results calculated?

### Predefined Un-Adjusted Remediation Effort

Configuration data is loaded to have the remediation effort for each
pattern ( CISQ, ISO-5055, CAST TQI ). This is called Un-Adjusted
Remediation Effort. The unit of effort is minute. The effort taken in to
account by CTDM for each pattern is EFFORT_DEFAULT. E.g.:

```text
Un-Adjusted Remediation Effort
STANDARD : CISQ
PATTERN : ASCPEM-PRF-15
EFFORT_DEFAULT: 90
EFFORT_MIN: 30
EFFORT_MAX: 210
EFFORT_UNIT: MIN 

Un-Adjusted Remediation Effort
STANDARD : ISO-5055
PATTERN : CWE-125
EFFORT : 30
```

For all violations of pattern ASCPEM-PRF-15, the Un-Adjusted Remediation
Effort is equal to 90 minutes. 

For all violations of pattern CWE-125 , the Un-Adjusted Remediation
Effort is equal to 30 minutes. 

### Scope

-   For each of the violations, the number of occurrences and related objects are collected, with related technologies.
-   For a violation of type Bookmark, the number of occurrences corresponds to the number of bookmarks and the related object is the main object of the violation.
-   For a violation of type Path, the number of occurrences corresponds to the number of paths, and the related objects are the main object of the violation, plus all related objects in the path(s)

The table below lists the Technical Criteria used by this extension:

| Standard | Tag | ID | Name | Effort value |
|---|---|---|---|---|
| AIP | NULL | 61001 | Architecture - Multi-Layers and Data Access | 60 |
| AIP | NULL | 61013 | Architecture - Object-level Dependencies | 180 |
| AIP | NULL | 61004 | Architecture - OS and Platform Independence | 60 |
| AIP | NULL | 66009 | Architecture - Reuse | 180 |
| AIP | NULL | 61009 | Complexity - Algorithmic and Control Structure Complexity | 60 |
| AIP | NULL | 61029 | Complexity - Dynamic Instantiation | 30 |
| AIP | NULL | 61029 | Complexity - Dynamic Instantiation | 60 |
| AIP | NULL | 61031 | Complexity - Empty Code | 12 |
| AIP | NULL | 66008 | Complexity - Functional Evolvability | 120 |
| AIP | NULL | 61010 | Complexity - OO Inheritance and Polymorphism | 180 |
| AIP | NULL | 61011 | Complexity - SQL Queries | 120 |
| AIP | NULL | 61026 | Complexity - Technical Complexity | 60 |
| AIP | NULL | 61027 | Dead code (static) | 24 |
| AIP | NULL | 61008 | Documentation - Automated Documentation | 12 |
| AIP | NULL | 61007 | Documentation - Bad Comments | 12 |
| AIP | NULL | 61017 | Documentation - Naming Convention Conformity | 12 |
| AIP | NULL | 61006 | Documentation - Style Conformity | 12 |
| AIP | NULL | 61028 | Documentation - Volume of Comments | 24 |
| AIP | NULL | 66068 | Efficiency - Expensive Calls in Loops | 120 |
| AIP | NULL | 61018 | Efficiency - Memory, Network and Disk Space Management | 60 |
| AIP | NULL | 61019 | Efficiency - SQL and Data Handling Performance | 120 |
| AIP | NULL | 61014 | Programming Practices - Error and Exception Handling | 60 |
| AIP | NULL | 61015 | Programming Practices - File Organization Conformity | 60 |
| AIP | NULL | 61020 | Programming Practices - Modularity and OO Encapsulation Conformity | 60 |
| AIP | NULL | 61003 | Programming Practices - OO Inheritance and Polymorphism | 60 |
| AIP | NULL | 61024 | Programming Practices - Structuredness | 24 |
| AIP | NULL | 66069 | Programming Practices - Unexpected Behavior | 60 |
| AIP | NULL | 66063 | Secure Coding - API Abuse | 30 |
| AIP | NULL | 66066 | Secure Coding - Encapsulation | 60 |
| AIP | NULL | 66062 | Secure Coding - Input Validation | 60 |
| AIP | NULL | 66065 | Secure Coding - Time and State | 60 |
| AIP | NULL | 66064 | Secure Coding - Weak Security Features | 30 |
| AIP | NULL | 61022 | Volume - Number of Components | 60 |
| AIP | NULL | 61023 | Volume - Number of LOC | 60 |
| ISO-5055 | CWE-1041 | 1061105 | CWE-1041 - Use of Redundant Code | 40 |
| ISO-5055 | CWE-1042 | 1061106 | CWE-1042 - Static Member Data Element outside of a Singleton Class Element | 120 |
| ISO-5055 | CWE-1043 | 1061107 | CWE-1043 - Data Element Aggregating an Excessively Large Number of Non-Primitive Elements | 120 |
| ISO-5055 | CWE-1045 | 1061108 | CWE-1045 - Parent Class with a Virtual Destructor and a Child Class without a Virtual Destructor | 90 |
| ISO-5055 | CWE-1046 | 1061109 | CWE-1046 - Creation of Immutable Text Using String Concatenation | 30 |
| ISO-5055 | CWE-1047 | 1061110 | CWE-1047 - Modules with Circular Dependencies | 300 |
| ISO-5055 | CWE-1048 | 1061111 | CWE-1048 - Invokable Control Element with Large Number of Outward Calls | 360 |
| ISO-5055 | CWE-1049 | 1061112 | CWE-1049 - Excessive Data Query Operations in a Large Data Table | 360 |
| ISO-5055 | CWE-1050 | 1061113 | CWE-1050 - Excessive Platform Resource Consumption within a Loop | 180 |
| ISO-5055 | CWE-1051 | 1061114 | CWE-1051 - Initialization with Hard-Coded Network Resource Configuration Data | 120 |
| ISO-5055 | CWE-1052 | 1061115 | CWE-1052 - Excessive Use of Hard-Coded Literals in Initialization | 30 |
| ISO-5055 | CWE-1054 | 1061116 | CWE-1054 - Invocation of a Control Element at an Unnecessarily Deep Horizontal Layer | 120 |
| ISO-5055 | CWE-1055 | 1061117 | CWE-1055 - Multiple Inheritance from Concrete Classes | 180 |
| ISO-5055 | CWE-1057 | 1061118 | CWE-1057 - Data Access Operations Outside of Expected Data Manager Component | 180 |
| ISO-5055 | CWE-1058 | 1061119 | CWE-1058 - Invokable Control Element in Multi-Thread Context with non-Final Static Storable or Member Element | 120 |
| ISO-5055 | CWE-1060 | 1061120 | CWE-1060 - Excessive Number of Inefficient Server-Side Data Accesses | 240 |
| ISO-5055 | CWE-1062 | 1061121 | CWE-1062 - Parent Class with References to Child Class | 120 |
| ISO-5055 | CWE-1064 | 1061122 | CWE-1064 - Invokable Control Element with Signature Containing an Excessive Number of Parameters | 180 |
| ISO-5055 | CWE-1066 | 1061123 | CWE-1066 - Missing Serialization Control Element | 40 |
| ISO-5055 | CWE-1067 | 1061124 | CWE-1067 - Excessive Execution of Sequential Searches of Data Resource | 150 |
| ISO-5055 | CWE-1070 | 1061125 | CWE-1070 - Serializable Data Element Containing non-Serializable Item Elements | 90 |
| ISO-5055 | CWE-1072 | 1061126 | CWE-1072 - Data Resource Access without Use of Connection Pooling | 300 |
| ISO-5055 | CWE-1073 | 1061127 | CWE-1073 - Non-SQL Invokable Control Element with Excessive Number of Data Resource Accesses | 300 |
| ISO-5055 | CWE-1074 | 1061128 | CWE-1074 - Class with Excessively Deep Inheritance | 300 |
| ISO-5055 | CWE-1075 | 1061129 | CWE-1075 - Unconditional Control Flow Transfer outside of Switch Block | 90 |
| ISO-5055 | CWE-1077 | 1061130 | CWE-1077 - Floating Point Comparison with Incorrect Operator | 40 |
| ISO-5055 | CWE-1079 | 1061131 | CWE-1079 - Parent Class without Virtual Destructor Method | 90 |
| ISO-5055 | CWE-1080 | 1061132 | CWE-1080 - Source Code File with Excessive Number of Lines of Code | 180 |
| ISO-5055 | CWE-1082 | 1061133 | CWE-1082 - Class Instance Self Destruction Control Element | 60 |
| ISO-5055 | CWE-1083 | 1061134 | CWE-1083 - Data Access from Outside Expected Data Manager Component | 90 |
| ISO-5055 | CWE-1084 | 1061135 | CWE-1084 - Invokable Control Element with Excessive File or Data Access Operations | 180 |
| ISO-5055 | CWE-1085 | 1061136 | CWE-1085 - Invokable Control Element with Excessive Volume of Commented-out Code | 30 |
| ISO-5055 | CWE-1086 | 1061137 | CWE-1086 - Class with Excessive Number of Child Classes | 300 |
| ISO-5055 | CWE-1087 | 1061138 | CWE-1087 - Class with Virtual Method without a Virtual Destructor | 50 |
| ISO-5055 | CWE-1088 | 1061139 | CWE-1088 - Synchronous Access of Remote Resource without Timeout | 90 |
| ISO-5055 | CWE-1089 | 1061140 | CWE-1089 - Large Data Table with Excessive Number of Indices | 240 |
| ISO-5055 | CWE-1090 | 1061141 | CWE-1090 - Method Containing Access of a Member Element from Another Class | 40 |
| ISO-5055 | CWE-1091 | 1061142 | CWE-1091 - Use of Object without Invoking Destructor Method | 30 |
| ISO-5055 | CWE-1094 | 1061144 | CWE-1094 - Excessive Index Range Scan for a Data Resource | 360 |
| ISO-5055 | CWE-1095 | 1061145 | CWE-1095 - Loop Condition Value Update within the Loop | 60 |
| ISO-5055 | CWE-1096 | 1061146 | CWE-1096 - Singleton Class Instance Creation without Proper Locking or Synchronization | 60 |
| ISO-5055 | CWE-1097 | 1061147 | CWE-1097 - Persistent Storable Data Element without Associated Comparison Control Element | 90 |
| ISO-5055 | CWE-1098 | 1061148 | CWE-1098 - Data Element containing Pointer Item without Proper Copy Control Element | 40 |
| ISO-5055 | CWE-1121 | 1061149 | CWE-1121 - Excessive McCabe Cyclomatic Complexity | 120 |
| ISO-5055 | CWE-119 | 1061021 | CWE-119 - Improper Restriction of Operations within the Bounds of a Memory Buffer | 40 |
| ISO-5055 | CWE-120 | 1061022 | CWE-120 - Buffer Copy without Checking Size of Input ('Classic Buffer Overflow') | 30 |
| ISO-5055 | CWE-123 | 1061023 | CWE-123 - Write-what-where Condition | 30 |
| ISO-5055 | CWE-125 | 1061024 | CWE-125 - Out-of-bounds Read | 30 |
| ISO-5055 | CWE-129 | 1061025 | CWE-129 - Improper Validation of Array Index | 50 |
| ISO-5055 | CWE-130 | 1061026 | CWE-130 - Improper Handling of Length Parameter Inconsistency | 30 |
| ISO-5055 | CWE-131 | 1061027 | CWE-131 - Incorrect Calculation of Buffer Size | 60 |
| ISO-5055 | CWE-134 | 1061028 | CWE-134 - Use of Externally-Controlled Format String | 60 |
| ISO-5055 | CWE-170 | 1061029 | CWE-170 - Improper Null Termination | 50 |
| ISO-5055 | CWE-194 | 1061030 | CWE-194 - Unexpected Sign Extension | 60 |
| ISO-5055 | CWE-195 | 1061031 | CWE-195 - Signed to Unsigned Conversion Error | 60 |
| ISO-5055 | CWE-196 | 1061032 | CWE-196 - Unsigned to Signed Conversion Error | 60 |
| ISO-5055 | CWE-197 | 1061033 | CWE-197 - Numeric Truncation Error | 60 |
| ISO-5055 | CWE-22 | 1061010 | CWE-22 - Improper Limitation of a Pathname to a Restricted Directory ('Path Traversal') | 60 |
| ISO-5055 | CWE-23 | 1061011 | CWE-23 - Relative Path Traversal | 60 |
| ISO-5055 | CWE-248 | 1061034 | CWE-248 - Uncaught Exception | 50 |
| ISO-5055 | CWE-252 | 1061035 | CWE-252 - Unchecked Return Value | 50 |
| ISO-5055 | CWE-259 | 1061036 | CWE-259 - Use of Hard-coded Password | 90 |
| ISO-5055 | CWE-321 | 1061037 | CWE-321 - Use of Hard-coded Cryptographic Key | 120 |
| ISO-5055 | CWE-36 | 1061012 | CWE-36 - Absolute Path Traversal | 60 |
| ISO-5055 | CWE-366 | 1061038 | CWE-366 - Race Condition within a Thread | 120 |
| ISO-5055 | CWE-369 | 1061039 | CWE-369 - Divide By Zero | 60 |
| ISO-5055 | CWE-390 | 1061040 | CWE-390 - Detection of Error Condition Without Action | 50 |
| ISO-5055 | CWE-391 | 1061041 | CWE-391 - Unchecked Error Condition | 50 |
| ISO-5055 | CWE-392 | 1061042 | CWE-392 - Missing Report of Error Condition | 50 |
| ISO-5055 | CWE-394 | 1061043 | CWE-394 - Unexpected Status Code or Return Value | 50 |
| ISO-5055 | CWE-401 | 1061044 | CWE-401 - Missing Release of Memory after Effective Lifetime | 180 |
| ISO-5055 | CWE-404 | 1061045 | CWE-404 - Improper Resource Shutdown or Release | 180 |
| ISO-5055 | CWE-407 | 1061046 | CWE-407 - Inefficient Algorithmic Complexity | 120 |
| ISO-5055 | CWE-415 | 1061047 | CWE-415 - Double Free | 90 |
| ISO-5055 | CWE-416 | 1061048 | CWE-416 - Use After Free | 90 |
| ISO-5055 | CWE-424 | 1061049 | CWE-424 - Improper Protection of Alternate Path | 120 |
| ISO-5055 | CWE-434 | 1061050 | CWE-434 - Unrestricted Upload of File with Dangerous Type | 90 |
| ISO-5055 | CWE-456 | 1061051 | CWE-456 - Missing Initialization of a Variable | 30 |
| ISO-5055 | CWE-457 | 1061052 | CWE-457 - Use of Uninitialized Variable | 30 |
| ISO-5055 | CWE-459 | 1061053 | CWE-459 - Incomplete Cleanup | 120 |
| ISO-5055 | CWE-476 | 1061054 | CWE-476 - NULL Pointer Dereference | 50 |
| ISO-5055 | CWE-477 | 1061055 | CWE-477 - Use of Obsolete Function | 30 |
| ISO-5055 | CWE-478 | 1061056 | CWE-478 - Missing Default Case in Switch Statement | 90 |
| ISO-5055 | CWE-480 | 1061057 | CWE-480 - Use of Incorrect Operator | 30 |
| ISO-5055 | CWE-484 | 1061058 | CWE-484 - Omitted Break Statement in Switch | 90 |
| ISO-5055 | CWE-502 | 1061059 | CWE-502 - Deserialization of Untrusted Data | 40 |
| ISO-5055 | CWE-543 | 1061060 | CWE-543 - Use of Singleton Pattern Without Synchronization in a Multithreaded Context | 60 |
| ISO-5055 | CWE-561 | 1061061 | CWE-561 - Dead Code | 30 |
| ISO-5055 | CWE-562 | 1061062 | CWE-562 - Return of Stack Variable Address | 50 |
| ISO-5055 | CWE-564 | 1061063 | CWE-564 - SQL Injection: Hibernate | 90 |
| ISO-5055 | CWE-567 | 1061064 | CWE-567 - Unsynchronized Access to Shared Data in a Multithreaded Context | 120 |
| ISO-5055 | CWE-570 | 1061065 | CWE-570 - Expression is Always False | 30 |
| ISO-5055 | CWE-571 | 1061066 | CWE-571 - Expression is Always True | 30 |
| ISO-5055 | CWE-595 | 1061067 | CWE-595 - Comparison of Object References Instead of Object Contents | 30 |
| ISO-5055 | CWE-597 | 1061068 | CWE-597 - Use of Wrong Operator in String Comparison | 30 |
| ISO-5055 | CWE-606 | 1061069 | CWE-606 - Unchecked Input for Loop Condition | 60 |
| ISO-5055 | CWE-611 | 1061070 | CWE-611 - Improper Restriction of XML External Entity Reference | 60 |
| ISO-5055 | CWE-643 | 1061072 | CWE-643 - Improper Neutralization of Data within XPath Expressions ('XPath Injection') | 60 |
| ISO-5055 | CWE-652 | 1061073 | CWE-652 - Improper Neutralization of Data within XQuery Expressions ('XQuery Injection') | 60 |
| ISO-5055 | CWE-662 | 1061074 | CWE-662 - Improper Synchronization | 120 |
| ISO-5055 | CWE-665 | 1061075 | CWE-665 - Improper Initialization | 30 |
| ISO-5055 | CWE-667 | 1061076 | CWE-667 - Improper Locking | 120 |
| ISO-5055 | CWE-672 | 1061077 | CWE-672 - Operation on a Resource after Expiration or Release | 90 |
| ISO-5055 | CWE-681 | 1061078 | CWE-681 - Incorrect Conversion between Numeric Types | 60 |
| ISO-5055 | CWE-682 | 1061079 | CWE-682 - Incorrect Calculation | 60 |
| ISO-5055 | CWE-703 | 1061080 | CWE-703 - Improper Check or Handling of Exceptional Conditions | 50 |
| ISO-5055 | CWE-704 | 1061081 | CWE-704 - Incorrect Type Conversion or Cast | 60 |
| ISO-5055 | CWE-732 | 1061082 | CWE-732 - Incorrect Permission Assignment for Critical Resource | 60 |
| ISO-5055 | CWE-758 | 1061083 | CWE-758 - Reliance on Undefined, Unspecified, or Implementation-Defined Behavior | 30 |
| ISO-5055 | CWE-764 | 1061084 | CWE-764 - Multiple Locks of a Critical Resource | 120 |
| ISO-5055 | CWE-77 | 1061013 | CWE-77 - Improper Neutralization of Special Elements used in a Command ('Command Injection') | 90 |
| ISO-5055 | CWE-772 | 1061085 | CWE-772 - Missing Release of Resource after Effective Lifetime | 120 |
| ISO-5055 | CWE-775 | 1061086 | CWE-775 - Missing Release of File Descriptor or Handle after Effective Lifetime | 120 |
| ISO-5055 | CWE-778 | 1061087 | CWE-778 - Insufficient Logging | 90 |
| ISO-5055 | CWE-78 | 1061014 | CWE-78 - Improper Neutralization of Special Elements used in an OS Command ('OS Command Injection') | 90 |
| ISO-5055 | CWE-783 | 1061088 | CWE-783 - Operator Precedence Logic Error | 15 |
| ISO-5055 | CWE-786 | 1061089 | CWE-786 - Access of Memory Location Before Start of Buffer | 50 |
| ISO-5055 | CWE-787 | 1061090 | CWE-787 - Out-of-bounds Write | 30 |
| ISO-5055 | CWE-788 | 1061091 | CWE-788 - Access of Memory Location After End of Buffer | 50 |
| ISO-5055 | CWE-789 | 1061092 | CWE-789 - Uncontrolled Memory Allocation | 50 |
| ISO-5055 | CWE-79 | 1061015 | CWE-79 - Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting') | 120 |
| ISO-5055 | CWE-798 | 1061093 | CWE-798 - Use of Hard-coded Credentials | 90 |
| ISO-5055 | CWE-805 | 1061094 | CWE-805 - Buffer Access with Incorrect Length Value | 30 |
| ISO-5055 | CWE-820 | 1061095 | CWE-820 - Missing Synchronization | 90 |
| ISO-5055 | CWE-821 | 1061096 | CWE-821 - Incorrect Synchronization | 90 |
| ISO-5055 | CWE-822 | 1061097 | CWE-822 - Untrusted Pointer Dereference | 50 |
| ISO-5055 | CWE-823 | 1061098 | CWE-823 - Use of Out-of-range Pointer Offset | 50 |
| ISO-5055 | CWE-824 | 1061099 | CWE-824 - Access of Uninitialized Pointer | 50 |
| ISO-5055 | CWE-825 | 1061100 | CWE-825 - Expired Pointer Dereference | 50 |
| ISO-5055 | CWE-833 | 1061101 | CWE-833 - Deadlock | 240 |
| ISO-5055 | CWE-835 | 1061102 | CWE-835 - Loop with Unreachable Exit Condition ('Infinite Loop') | 90 |
| ISO-5055 | CWE-88 | 1061016 | CWE-88 - Improper Neutralization of Argument Delimiters in a Command ('Argument Injection') | 90 |
| ISO-5055 | CWE-89 | 1061017 | CWE-89 - Improper Neutralization of Special Elements used in an SQL Command ('SQL Injection') | 90 |
| ISO-5055 | CWE-90 | 1061018 | CWE-90 - Improper Neutralization of Special Elements used in an LDAP Query ('LDAP Injection') | 50 |
| ISO-5055 | CWE-908 | 1061103 | CWE-908 - Use of Uninitialized Resource | 30 |
| ISO-5055 | CWE-91 | 1061019 | CWE-91 - XML Injection (aka Blind XPath Injection) | 50 |
| ISO-5055 | CWE-917 | 1061104 | CWE-917 - Improper Neutralization of Special Elements used in an Expression Language Statement ('Expression Language Injection') | 60 |
| ISO-5055 | CWE-99 | 1061020 | CWE-99 - Improper Control of Resource Identifiers ('Resource Injection') | 50 |

### Qualification information

#### Complexity

The Complexity - or Effort Complexity - of the code elements
implementing an Occurrence is qualification information that is measured
according to the Effort Complexity definition from Automated Enhancement
Points:

``` text
EC/LowEffortComplexity
```

EC is computed by the following metrics:

-   10351: EC ADDED
-   10353: EC UPDATED
-   10354: EC UNCHANGED

#### LowEffortComplexity

The technology related Low Complexity column for ADDED artifacts ( from
COST_CONFIG ). When the violation has related objects, then the average
EC of all objects is taken in to account. The complexity is computed for
the main object violating a rule:

``` text
Complexity = AVG ( Effort Complexity all objects of the violation )/ Low Complexity for the related technology
```

#### Concentration

Concentration is qualification information that measures the number of
Occurrences within any Code Element in the software:

``` text
1/nb of time the object violates any rule
```

#### Exposure

The Exposure of an Occurrence is qualification information that measures
the level of contentedness of the Occurrence with the rest of the
software, both directly and indirectly through call paths:

``` java
1+log(nb paths)
```

#### Technical diversity

The Technological Diversity of an Occurrence measures the number of
distinct technologies in which the code elements included in a single
occurrence of a source code pattern are written. This is set to 1. 

#### Gap size

In the context of patterns which rely on roles that model values
and threshold values that are not to be exceeded, the gap between
these values must be closed to re-mediate this weakness. This is set to
1. 

### Adjustment Factor

The adjustment factor is computed based on qualification information, as
follows:

``` text
AVG(Complexity) X AVG(Exposure) X Count(Technological diversity) X AVG(Concentration) X Sum(Gap size)
```

### Technical debt

Finally the technical debt is computed by:

``` java
Nb Of Occurrences X Adjustment Factor X Un-adjusted Remediation Effort
```

## Result storage

### Default result storage method - high level data only

By default only data aggregated at the following level is available and
is stored in the Dashboard schema table DSS_METRIC_RESULTS:

-   Application
-   Module
-   Technical Criteria
-   Business Criteria

Detailed information at Object and Rule level can be generated and made
available to the dashboard on demand by executing the following query
against the Dashboard schema:

``` sql
select OMG_ATDM_COMPUTE_DETAILS (SNAPSHOT_ID, OBJECT_ID, RULE_ID) 
```

Where:

| Item | Description |
|---|---|
| `SNAPSHOT_ID` | Is the ID of the snapshot you want to generate detailed object and rule level information for. |
| `RULE_ID` | The ID of the rule you want to generate detailed information for. Note that if `RULE_ID = -1`, detailed information will be generated for all rules. |
| `OBJECT_ID` | The ID of the object you want to generate detailed information for. Note that if `OBJECT_ID = -1`, detailed information will be generated for all objects. |

When the next snapshot is generated, detailed object/rule level
information will be saved in the table OMGTD_RESULTS.

### Alternative result storage method - all data

This method is not recommended for very large Applications, since the
impact on performance of generating all data for every snapshot will be
significant.

It is possible to change the behaviour and choose to always save all
results (including Object and Rule level information) for every snapshot
that is generated. To do so, execute the following query against the
Dashboard schema:

``` sql
select OMG_ATDM_DETAILSALL();
```

This option will be taken in account when a new snapshot is generated
and as a result all information is saved as follows:

-   Application, Module, Technical Criteria, Business Criteria - aggregated in the table DSS_METRIC_RESULTS
-   Details for Object and Rule levels stored in the table OMGTD_RESULTS 

To disable the storage of all details, execute the following query
against the Dashboard schema:

``` sql
select OMG_ATDM_DETAILSONDEMAND();
```

## What results can you expect?

### Health Dashboard

When the ISO-5055 view is selected, two tiles are available out of the
box in the Overview and Trends sections respectively,
at Multi-Application level (the tiles will display no value if the OMG
Technical Debt extension is not installed and no snapshot has been
generated):

![](../images/667320662.jpg)

At single Application level, one tile is available out of the box in the
Overview section when the ISO-5055 view is selected:

![](../images/667320663.jpg)

Switch between the AIP and ISO-5055 views using the drop down in
the home page:

![](../images/667320664.jpg)

Clicking the tiles will provide more detailed information:

*Multi Application level*

![](../images/667320665.jpg)

*Single Application level*

![](../images/667320666.jpg)

In addition, at single Application level drill down, for each of the
Source Code Pattern Names, it is possible to navigate to the details of
the violations in the Engineering Dashboard (where this has been setup):

By default, these tiles are configured to display ISO-5055 index data
(using the OMG_TECHNICAL_DEBT_ISO ID). If you want to display either TQI
(CAST Imaging Core) or CISQ Index values (if the extension is
installed), you will need to manually edit the configuration in the
cmp-ISO.json or app-ISO.json file and change the ID used in the tile
(i.e. for SizingMeasureEvolution and/or SizingMeasureResult /
SizingMeasureResults) - see [Health Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Health+Dashboard+tile+management) for
more information:

-   OMG_TECHNICAL_DEBT_ISO - based on ISO-5055 index data, installed by
    default in CAST Imaging Console
-   OMG_TECHNICAL_DEBT_CISQ - based on CISQ Index data, requires that
    the
    [com.castsoftware.cisq-index](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cisq-index&version=latest) is
    installed
-   OMG_TECHNICAL_DEBT - based on TQI from CAST Imaging Core.

For example:

Tile drill down is tailored to the data chosen in the tile, for example:

Tile configuration examples:

``` java
SizingMeasureResult (app-ISO.json) / SizingMeasureResults (cmp-ISO.json):

        {
          "id": 208,
          "plugin": "SizingMeasureResult",
          "color": "yellow",
          "parameters": {
            "title": "Technical Debt (OMG) by ISO-5055",
            "sizingMeasure": {"id": "OMG_TECHNICAL_DEBT_ISO", "format": "0,0", "description": "Days"}
          }
        },
        {
          "id": 998,
          "plugin": "SizingMeasureResult",
          "color": "yellow",
          "parameters": {
            "title": "Technical Debt (OMG) by CISQ",
            "sizingMeasure": {"id": "OMG_TECHNICAL_DEBT_CISQ", "format": "0,0", "description": "Days"}
          }
        },
        {
          "id": 999,
          "plugin": "SizingMeasureResult",
          "color": "yellow",
          "parameters": {
            "title": "Technical Debt (OMG) by TQI",
            "sizingMeasure": {"id": "OMG_TECHNICAL_DEBT", "format": "0,0", "description": "Days"}
          }
        },  

SizingMeasureEvolution (app-ISO.json and cmp-ISO.json):

        {
          "id": 2103,
          "plugin": "SizingMeasureEvolution",
          "color": "yellow",
          "parameters": {
            "widget":"line",
            "title": "Technical Debt (OMG) by ISO-5055",
            "sizingMeasure": {"id": "OMG_TECHNICAL_DEBT_ISO", "format": "0,0"}
          }
        },
        {
          "id": 9998,
          "plugin": "SizingMeasureEvolution",
          "color": "yellow",
          "parameters": {
            "widget":"line",
            "title": "Technical Debt (OMG) by CISQ",
            "sizingMeasure": {"id": "OMG_TECHNICAL_DEBT_CISQ", "format": "0,0"}
          }
        },
        {
          "id": 9999,
          "plugin": "SizingMeasureEvolution",
          "color": "yellow",
          "parameters": {
            "widget":"line",
            "title": "Technical Debt (OMG) by TQI",
            "sizingMeasure": {"id": "OMG_TECHNICAL_DEBT", "format": "0,0"}
          }
        },
```

### Engineering Dashboard

A tile is available out of the box displaying the Total Technical Debt
(OMG) in days for the current Application, as well as Added and Removed
OMG Technical Debt in days for the current snapshot:

![](../images/667320670.jpg)

You can change the specific Technical Criteria used to display the
values in the tile, using the drop down option:

![](../images/667320671.jpg)

By default, this tile is configured to display ISO-5055 index data
(using the 1061000 metric ID). If you want to display either TQI (CAST
Imaging Core) or CISQ Index values (if the extension is installed), you
will need to manually edit the configuration in the ed.json file and
change the ID used - see [Engineering Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+tile+management) for
more information:

-   1061000 - based on ISO-5055 index data, installed by default in CAST
    Imaging Console
-   1062100 - based on CISQ Index data, requires that the
    [com.castsoftware.cisq-index](https://extend.castsoftware.com/#/extension?id=com.castsoftware.cisq-index&version=latest) is
    installed
-   60017 (based on TQI from CAST Imaging Core)

Clicking this tile will drill down to the [Risk investigation
view](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+-+Risk+Investigation) with Technical
Debt (OMG) with the ISO-5055 Assessment Model selected. If the tile has
been manually edited and re-configured to show either TQI (CAST Imaging
Core) or CISQ Index data, the drill down will also change as shown
below:

| Data | Example  |
|---|---|
| ISO-5055 data (default) | ![](../images/667320672.jpg) |
| TQI data (AIP Core) | ![](../images/667320673.jpg) |
| CISQ Index data | ![](../images/667320674.jpg) |

You can also switch to showing Violations instead of Technical Debt
(OMG) values from the drop-down list:

![](../images/667320675.png) 

In the rule details section, there is a dedicated section called
"Technical Debt (OMG)", which will show the details (Total, Added,
Removed, and No. of occurrences):

In the source code view, a Technical Debt (OMG) section is displayed,
showing object level details on the Adjustment Factor, Unadjusted Effort
(in mins) and Adjusted Efforts (in mins):

![](../images/667320677.png)

### Using the RestAPI to obtain results

#### Total Technical Debt by Application, Module

Results can be obtained using a RestAPI query. For example, to
obtain technical debt as a remediation effort use the metric #1062020
(you can replace this ID with other supported Sizing Measure IDs):

``` text
AAD/results?metrics=1062020&modules=$all&technologies=$all
```

Example showing the technical debt for all applications, with a
breakdown by technology and by module for an example Application called
"shopizer8321":

``` bash
C:>curl -H "Accept: text/csv" -u admin:cast "http://localhost:9190/CAST-RESTAPI/rest/AAD/results?metrics=1062020&modules=$all&technologies=$all"
```

Results:

``` text
Application Name;Module Name;Technology;Metric Name;Metric Id;Metric Type;Critical;Snapshot Date #1;Result #1
shopizer8321;null;null;OMG-ATDM: Remediation Effort;1062020;technical-debt-statistics;N/A;2020-03-27;387350.0
shopizer8321;null;HTML5;OMG-ATDM: Remediation Effort;1062020;technical-debt-statistics;N/A;2020-03-27;770.0
shopizer8321;null;JEE;OMG-ATDM: Remediation Effort;1062020;technical-debt-statistics;N/A;2020-03-27;386580.0
shopizer8321;shopizer8321 full content;null;OMG-ATDM: Remediation Effort;1062020;technical-debt-statistics;N/A;2020-03-27;387350.0
shopizer8321;shopizer8321 full content;HTML5;OMG-ATDM: Remediation Effort;1062020;technical-debt-statistics;N/A;2020-03-27;770.0
shopizer8321;shopizer8321 full content;JEE;OMG-ATDM: Remediation Effort;1062020;technical-debt-statistics;N/A;2020-03-27;386580.0
```

In other words, the "shopizer8321" application has a technical debt of
387350 minutes, which is equivalent to 387350 ÷ 60 ÷ 8 = 806 workload
days. The remediation effort is dispatched between HTML5 code and Java
Code as follows:

| Technology | Remediation effort |
|---|---|
| HTML       |    770 minutes     |
| JEE        |  386,580 minutes   |

#### Total Technical Debt by Business Criterion, Technical Criterion, Rule

With a CISQ Business Criterion ID, you can obtain the technical debt for
this Quality Indicator and all related indicators (ie CISQ Measure
Elements):

| CISQ Business Criterion ID | Name                        |
|----------------------------|-----------------------------|
| 1062100                    | CISQ-Index                  |
| 1062101                    | CISQ-Maintainability        |
| 1062102                    | CISQ-Performance-Efficiency |
| 1062103                    | CISQ-Reliability            |
| 1062104                    | CISQ-Security               |

Example:

``` bash
C:>curl -H "Accept: text/csv" -u admin:cast "http://localhost:9190/CAST-RESTAPI/rest/SHOPIZER/applications/3/results?metrics=c:1062100&select=omgTechnicalDebt"

Application Name;Technical Criterion;Metric Id;Metric Type;Critical;Snapshot Date #1;Result #1;OMG Technical Debt (Result #1);OMG Occurrences (Result #1);OMG Added Technical Debt (Result #1);OMG Removed Technical Debt (Result #1)
shopizer8321;ASCMM-MNT-1 - Control Flow Transfer Control Element outside Switch Block;1062110;technical-criteria;false;2020-05-15;4.0;0;2;0;0
shopizer8321;ASCMM-MNT-11 - Callable and Method Control Element Excessive Cyclomatic Complexity Value;1062112;technical-criteria;false;2020-05-15;3.58017346587814;null;null;null;null
shopizer8321;ASCMM-MNT-12 - Named Callable and Method Control Element with Layer-skipping Call;1062113;technical-criteria;false;2020-05-15;4.0;null;null;null;null
shopizer8321;ASCMM-MNT-13 - Callable and Method Control Element Excessive Number of Parameters;1062114;technical-criteria;false;2020-05-15;4.0;null;null;null;null
shopizer8321;ASCMM-MNT-15 - Public Member Element;1062116;technical-criteria;false;2020-05-15;4.0;40;1;0;0
...
```

You can get also the technical debt for a single rule, as long as this
rule is identified as a CISQ rule by the CISQ Index:

``` bash
C:>curl -H "Accept: application/json" -u admin:cast "http://localhost:9190/CAST-RESTAPI/rest/SHOPIZER/applications/3/results?metrics=8216&select=omgTechnicalDebt,violationRatio"


...
                "result": {
                    "grade": 4,
                    "omgTechnicalDebt": {
                        "total": 11040,
                        "numberOccurrences": 176,
                        "added": 0,
                        "removed": 0
                    },
                    "violationRatio": {
                        "totalChecks": 7411,
                        "failedChecks": 33,
                        "successfulChecks": 7378,
                        "ratio": 0.9955471596275807
                    }
                },
```

#### Detailed Technical Debt for a violation

As we refer to findings from a snapshot ID, and object ID and a rule ID
with URI such as:

``` java
TINY/components/568/snapshots/8/findings/8216
```

We can refer technical debt details in a similar Web Service:

``` java
TINY/components/568/snapshots/8/omg-technical-debt/8216
```

E.g.:

``` bash
C:>curl -H "Accept: application/json" -u admin:cast "http://localhost:9190/CAST-RESTAPI/rest/TINY/components/568/snapshots/8/omg-technical-debt/8216"
{
    "total": 180,
    "numberOccurrences": 3,
    "complexity": 1,
    "exposure": 1,
    "concentration": 0,
    "technologicalDiversity": 1,
    "gapSize": 1,
    "unadjustedEffort": 60,
    "added": 0,
    "removed": 0,
    "adjustmentFactor": 3
}
```

### Querying the Dashboard schema for results

The Dashboard schema contains views and tables that provide information
about the results generated by this extension:

| View/Table | Description  | Type |
|---|---|---|
| `OMG_ATDM_RESULTS_OBJ_APP` | Remediation effort and adjusted Factor aggregated at Application, Module, PATTERN level.<br>   | VIEW |
| `OMG_ATDM_RESULTS_OBJ_RULE_APP` | Remediation effort and adjusted Factor for Object Rule, aggregated at Application, Module, PATTERN level. | VIEW |
| `OMG_ATDM_DETAILS_OBJ_RULE` | Details all metrics computed for all violations. | VIEW |
| `ATDM_SCOPE_OCCURENCES` | Last scope taken in to account. | TABLE |

#### Obtaining results at Application level for a snapshot

``` sql
SET search_path=xxx_central;
SELECT * 
FROM   omg_atdm_results_obj_app 
WHERE  snapshot_id = <snapshot_id>
```

![](../images/667320678.png)

#### Obtaining results per CAST Rule / CISQ Pattern at the application level for a snapshot

Possible STANDARD values are 'CISQ' , 'ISO-5055', 'AIP'. CISQ Total:

``` sql
SET search_path=xxx_central;
SELECT TD.SNAPSHOT_ID,  TD.OBJECT_NAME as APPLICATION_NAME,
  sum(case when TD.METRIC_ID = 1062020 then TD.METRIC_NUM_VALUE else 0 end) tech_debt,
  sum(case when TD.METRIC_ID = 1062030 then TD.METRIC_NUM_VALUE else 0 end) tech_debt_added,
  sum(case when TD.METRIC_ID = 1062032 then TD.METRIC_NUM_VALUE else 0 end) tech_debt_removed
FROM OMG_ATDM_RESULTS_OBJ_RULE_APP TD
join DSS_SNAPSHOTS S on S.SNAPSHOT_ID = TD.SNAPSHOT_ID and TD.OBJECT_ID = S.APPLICATION_ID
Where TD.METRIC_VALUE_INDEX in ( select METRIC_ID from CTDM_REMEDIATION_EFFORTS where STANDARD = 'ISO-5055')
And TD.SNAPSHOT_ID = 2
And TD.METRIC_ID in ( 1062020,1062030, 1062032 ) -- Remediation Effort Total, added, deleted
Group by TD.SNAPSHOT_ID, TD.OBJECT_NAME
```

CISQ Technical Criteria:

``` sql
SET search_path=xxx_central;
SELECT TD.SNAPSHOT_ID, TD.METRIC_VALUE_INDEX , TD.AGGREGATELEVEL , TD.OBJECT_NAME as APPLICATION_NAME, 
      sum(case when TD.METRIC_ID = 1062020 then TD.METRIC_NUM_VALUE else 0 end) tech_debt,
      sum(case when TD.METRIC_ID = 1062030 then TD.METRIC_NUM_VALUE else 0 end) tech_debt_added,
      sum(case when TD.METRIC_ID = 1062032 then TD.METRIC_NUM_VALUE else 0 end) tech_debt_removed 
FROM  OMG_ATDM_RESULTS_OBJ_RULE_APP TD
join DSS_SNAPSHOTS S on S.SNAPSHOT_ID = TD.SNAPSHOT_ID and TD.OBJECT_ID = S.APPLICATION_ID
Where TD.METRIC_VALUE_INDEX in ( select METRIC_ID from CTDM_REMEDIATION_EFFORTS where STANDARD = 'CISQ')
And TD.SNAPSHOT_ID = <SNAPSHOT_ID>
And TD.METRIC_ID in ( 1062020,1062030, 1062032 ) -- Remediation Effort Total, added, deleted
Group by  TD.SNAPSHOT_ID, TD.METRIC_VALUE_INDEX , TD.AGGREGATELEVEL , TD.OBJECT_NAME
Order by TD.METRIC_VALUE_INDEX 
```

CISQ Business Criterion:

``` sql
SELECT TD.SNAPSHOT_ID, TD.METRIC_VALUE_INDEX , TD.AGGREGATELEVEL , TD.OBJECT_NAME as APPLICATION_NAME,
      sum(case when TD.METRIC_ID = 1062020 then TD.METRIC_NUM_VALUE else 0 end) tech_debt,
      sum(case when TD.METRIC_ID = 1062030 then TD.METRIC_NUM_VALUE else 0 end) tech_debt_added,
      sum(case when TD.METRIC_ID = 1062032 then TD.METRIC_NUM_VALUE else 0 end) tech_debt_removed
FROM  OMG_ATDM_RESULTS_OBJ_RULE_APP TD
join DSS_SNAPSHOTS S on S.SNAPSHOT_ID = TD.SNAPSHOT_ID and TD.OBJECT_ID = S.APPLICATION_ID
Where TD.METRIC_VALUE_INDEX in ( select BC_METRIC_ID from OMG_BC_METRIC_VIEW BC join  CTDM_REMEDIATION_EFFORTS TC on TC.STANDARD = 'CISQ' and TC.METRIC_ID =BC.PATTERN_METRIC_ID )
And TD.SNAPSHOT_ID = <SNAPSHOT_ID>
And TD.METRIC_ID in ( 1062020,1062030, 1062032 ) -- Remediation Effort Total, added, deleted
Group by  TD.SNAPSHOT_ID, TD.METRIC_VALUE_INDEX , TD.AGGREGATELEVEL , TD.OBJECT_NAME
Order by TD.METRIC_VALUE_INDEX
```

For example to get the results for all metrics of pattern
'ASCPEM-PRF-8':

``` sql
SELECT * 
FROM   omg_atdm_results_obj_rule_app 
WHERE  snapshot_id = 4 
       AND ( metric_value_index IN (SELECT T.metric_id + 1 
                                    FROM   aed_metric_quality_tags T 
                                    WHERE  T.tag = 'ASCPEM-PRF-8') 
              OR aggregatelevel = 'ASCPEM-PRF-8' ) 
```

![](../images/667320679.png)

Note that to obtain the CISQ Pattern of a given CAST rule, you can adapt
the following query. For example, to find the CISQ Pattern for the rule
ID = 7201, the following query will return ASCPEM-PRF-8:

``` sql
SELECT C.tag 
FROM   aed_quality_tags_doc C 
       join aed_metric_quality_tags T 
         ON T.tag = C.tag 
WHERE  C.standard = 'CISQ' 
       AND T.metric_id + 1 = 7201 
```

## Working out why Technical Debt values increase when you have removed violations?

Sometimes it can be difficult to fully work out why Technical Debit values increase between successive versions of an application when you have performed remedial action to fix violations. This is because the calculation of Technical Debt takes inputs from many different sources, for example, the number of violations to certain rules, the complexity of items in your application and the remediation effort required to fix these violations (and takes into account how many occurences of the violation exist).

For example, even if you reduce the number of lines of code in your application while reducing the number of violations reported by CAST, you may find that you have, at the same time, increased complexity due to the way in which the violations have been fixed. The changes you have made may impact objects which have a large number of connections to other objects (i.e. high complexity objects) and which still return violations. Therefore the Technical Debt value reflects this: it is more difficult to fix violations reported on complex objects.

One place to start is to take the rule that shows the highest increase in technical debt compared with the previous analysis run. In this example you can see that while Technical Debt has decreased it has also increased:

![](tech_debt1.jpg)

You should then look at the number of occurrences (i.e. the number of objects where the specific violation occurs) and compare this with a previous analysis result:

![](tech_debt2.jpg)

In a majority of cases, the violation **7778 Avoid Artifacts with High Fan-Out** may well be the violation with the highest increase in technical debt: this violation lists objects that have a high "fan-out" value: the higher the "fan-out" value, the more connections the object has, therefore indicating that the objects are complex and that the cost to remediate violations in these objects will be high. Compare the total "fan-out" value for all the objects that violate this rule with a previous analysis: you can do so by downloading the list of objects to `.csv` file and then summing the **Fan-out** column:

![](tech_debt3.jpg)

![](tech_debt4.jpg)

Even if the number of violations has decreased, the **Fan-Out** value may have increased, therefore also increasing Technical Debt values.