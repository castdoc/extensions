---
title: "Perl runtime - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.perl.runtime

## What's new?

See [Release Notes](rn/).

## Description

This extension provides **Perl runtime binaries** from https://github.com/StrawberryPerl/Perl-Dist-Strawberry/releases for any other CAST extension that requires them.

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

This extension is a dependency of any CAST extension that requires the Perl runtime binaries and is therefore automatically downloaded and installed whenever required.
