---
title: "gRPC"
linkTitle: "gRPC"
description: "Technical information about how CAST handles gRPC..."
type: "docs"
---

## What is gRPC ?

gRPC is an open source Remote Procedure Calls framework. As in many RPC
systems, gRPC is based around the idea of defining a service and then
specifying the methods that can be called remotely with their parameters
and return types. On the server side, the server implements this
interface and runs a gRPC server to handle client calls. On the client
side, the client has a stub (referred to as just a client in some
languages) that provides the same methods as the server. that can run in
any environment. It can be used with several programming languages. It
can connect services in and across data centers with pluggable support
for load balancing, tracing, health checking and authentication. It is
also applicable in last mile of distributed computing to connect
devices, mobile applications and browsers to backend services.

gRPC clients and servers can run and talk to each other in a variety of
environments - from servers inside Google to your own desktop - and can
be written in many
[languages](https://www.grpc.io/docs/languages/).

## Supported languages

We have extensions for analyzing gRPC as follows:

-   [gRPC-.NET](https://extend.castsoftware.com/#/extension?id=com.castsoftware.dotnet.grpc&version=latest)
-   [gRPC-java](https://extend.castsoftware.com/#/extension?id=com.castsoftware.grpcjava&version=latest)
-   [Node.js (JavaScript)](https://extend.castsoftware.com/#/extension?id=com.castsoftware.nodejs&version=latest)
-   [Node.js (TypeScript)](https://extend.castsoftware.com/#/extension?id=com.castsoftware.typescript&version=latest)