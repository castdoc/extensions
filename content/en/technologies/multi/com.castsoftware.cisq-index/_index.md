---
title: "CISQ Index"
linkTitle: "CISQ Index"
type: "docs"
---

***

## Extension ID

com.castsoftware.cisq-index

## Description

This extension will compute CISQ Quality Software Characteristic
Measures as Business Criteria and CISQ Measure Elements as Technical
Criteria. All CAST rules that are tagged with a CISQ related tag will
contribute to the various CISQ Measure Elements / CISQ Quality Software
Characteristic Measures thereby allowing specific grades and rule
violations to be reported.

## Compatibility

| Product                    | Release  | Supported          |
|----------------------------|----------|:------------------:|
| CAST Engineering Dashboard | ≥ 1.5    | :white_check_mark: |
| CAST Health Dashboard      | ≥ 1.17   | :white_check_mark: |
| CAST Security Dashboard    | ≥ 1.18   | :white_check_mark: |

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## CISQ version

| Version             |  Supported         |
|---------------------|:------------------:|
| 1.0 (December 2016) | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Imaging Console. If you need to use it, should manually install the
extension.

## Configuration requirements

### Generate a snapshot

A new snapshot must be generated (after the extension is installed)
before results can be viewed. If you do not immediately see changes in
the dashboard, please consider restarting Apache Tomcat and/or emptying
your browser cache.

### Engineering Dashboard

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to display
Violation data using the Industry Standard/s tile plugin in v. ≥ 1.18 of
the Engineering Dashboard. See [Engineering Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+tile+management)
for more information:

Clicking on the tile navigates to Risk investigation view and the
specified Industry Standard will be selected in the Health Factor table.

### Health Dashboard

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to display
Grade, Compliance, and Violation data directly from this extension using
the Industry Standard/s tile plugin in v. ≥ 1.17 of the Health
Dashboard. See Health Dashboard tile management for more information:

![](images/406814971.jpg)

Clicking on any of these tiles will display a list of the rules that
have been tagged with the specified standard as provided by the
extension. Compliance percentage is also displayed in a "bubble":

*Compliance grade at Application level with compliance percentage
displayed in a "bubble"*

![](images/406814972.jpg)

*Compliance grade at Portfolio level*

![](images/406814973.png)

Example for cmp.json / cmp-ISO.json

Configuration to create a "gauge" tile at portfolio level (multi-app
level) to show a "parent" CISQ-Security Business Criterion tile:

``` xml
{
  "id": 1234,
  "plugin": "IndustryStandards",
  "color": "black",
  "parameters": {
    "type": "CISQ",
    "title": "CISQ-Security",
    "widget": "gauge",
    "industryStandard": {
        "id": "1062166",
        "indexID": "1062100",
        "mode": "grade",
        "format": "0.00",
        "description": "CISQ-Security in grade format"
    }
  }
}
```

Example for app.json

Configuration to create a "number of violations" tile at application
level (single app level) to show a CISQ-Security Business Criterion
tile:

``` xml
{
  "id": 1236,
  "plugin": "IndustryStandard",
  "color": "orange",
  "parameters": {
    "type": "CISQ",    
    "title": "CISQ-Security",
    "industryStandard": {
        "id": "1062166",
        "indexID": "1062100",
        "mode": "violations",
        "format": "0,000",
        "description": "CISQ-Security in number of violations format" 
    }
  }
}
```

### Security Dashboard

Out of the box, no tiles will be provided to display data for this
extension, however it is possible to create tiles manually to display
Violation data using the Industry Standard/s tile plugin in v. ≥ 1.18 of
the Security Dashboard. The tile configuration is identical to that
described for the Engineering Dashboard, see [Engineering Dashboard tile
management](https://doc.castsoftware.com/display/DASHBOARDS/Engineering+Dashboard+tile+management)
for more information:

Clicking on the tile navigates to Security Dashboard - Risk
Investigation and the specified Industry Standard will be selected in
the Health Factor table.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results:

### Assessment Model

Various Business and Technical Criteria will be added by the extension:

#### CISQ-Index

| ID      | Name       | Type               |
|---------|------------|--------------------|
| 1062100 | CISQ-Index | Business Criterion |

Note that the CISQ-Index Business Criterion has all Technical Criteria
listed below as contributors.

#### CISQ-Maintainability

| ID      | Name                 | Type                |
|---------|----------------------|---------------------|
| 1062101 | CISQ-Maintainability | Business Criterion  |
| 1062110 | ASCMM-MNT-1          | Technical Criterion |
| 1062121 | ASCMM-MNT-2          | Technical Criterion |
| 1062123 | ASCMM-MNT-3          | Technical Criterion |
| 1062124 | ASCMM-MNT-4          | Technical Criterion |
| 1062125 | ASCMM-MNT-5          | Technical Criterion |
| 1062126 | ASCMM-MNT-6          | Technical Criterion |
| 1062127 | ASCMM-MNT-7          | Technical Criterion |
| 1062128 | ASCMM-MNT-8          | Technical Criterion |
| 1062129 | ASCMM-MNT-9          | Technical Criterion |
| 1062111 | ASCMM-MNT-10         | Technical Criterion |
| 1062112 | ASCMM-MNT-11         | Technical Criterion |
| 1062113 | ASCMM-MNT-12         | Technical Criterion |
| 1062114 | ASCMM-MNT-13         | Technical Criterion |
| 1062115 | ASCMM-MNT-14         | Technical Criterion |
| 1062116 | ASCMM-MNT-15         | Technical Criterion |
| 1062117 | ASCMM-MNT-16         | Technical Criterion |
| 1062118 | ASCMM-MNT-17         | Technical Criterion |
| 1062119 | ASCMM-MNT-18         | Technical Criterion |
| 1062120 | ASCMM-MNT-19         | Technical Criterion |
| 1062122 | ASCMM-MNT-20         | Technical Criterion |

#### CISQ-Performance-Efficiency

| ID      | Name                        | Type                |
|---------|-----------------------------|---------------------|
| 1062102 | CISQ-Performance-Efficiency | Business Criterion  |
| 1062130 | ASCPEM-PRF-1                | Technical Criterion |
| 1062137 | ASCPEM-PRF-2                | Technical Criterion |
| 1062138 | ASCPEM-PRF-3                | Technical Criterion |
| 1062139 | ASCPEM-PRF-4                | Technical Criterion |
| 1062140 | ASCPEM-PRF-5                | Technical Criterion |
| 1062141 | ASCPEM-PRF-6                | Technical Criterion |
| 1062142 | ASCPEM-PRF-7                | Technical Criterion |
| 1062143 | ASCPEM-PRF-8                | Technical Criterion |
| 1062144 | ASCPEM-PRF-9                | Technical Criterion |
| 1062131 | ASCPEM-PRF-10               | Technical Criterion |
| 1062132 | ASCPEM-PRF-11               | Technical Criterion |
| 1062133 | ASCPEM-PRF-12               | Technical Criterion |
| 1062134 | ASCPEM-PRF-13               | Technical Criterion |
| 1062135 | ASCPEM-PRF-14               | Technical Criterion |
| 1062136 | ASCPEM-PRF-15               | Technical Criterion |

#### CISQ-Reliability

| ID      | Name                   | Type                |
|---------|------------------------|---------------------|
| 1062103 | CISQ-Reliability       | Business Criterion  |
| 1062145 | ASCRM-CWE-120          | Technical Criterion |
| 1062146 | ASCRM-CWE-252-data     | Technical Criterion |
| 1062147 | ASCRM-CWE-252-resource | Technical Criterion |
| 1062148 | ASCRM-CWE-396          | Technical Criterion |
| 1062149 | ASCRM-CWE-397          | Technical Criterion |
| 1062150 | ASCRM-CWE-456          | Technical Criterion |
| 1062151 | ASCRM-CWE-674          | Technical Criterion |
| 1062152 | ASCRM-CWE-704          | Technical Criterion |
| 1062153 | ASCRM-CWE-772          | Technical Criterion |
| 1062154 | ASCRM-CWE-788          | Technical Criterion |
| 1062155 | ASCRM-RLB-1            | Technical Criterion |
| 1062166 | ASCRM-RLB-2            | Technical Criterion |
| 1062167 | ASCRM-RLB-3            | Technical Criterion |
| 1062168 | ASCRM-RLB-4            | Technical Criterion |
| 1062169 | ASCRM-RLB-5            | Technical Criterion |
| 1062170 | ASCRM-RLB-6            | Technical Criterion |
| 1062171 | ASCRM-RLB-7            | Technical Criterion |
| 1062172 | ASCRM-RLB-8            | Technical Criterion |
| 1062173 | ASCRM-RLB-9            | Technical Criterion |
| 1062156 | ASCRM-RLB-10           | Technical Criterion |
| 1062157 | ASCRM-RLB-11           | Technical Criterion |
| 1062158 | ASCRM-RLB-12           | Technical Criterion |
| 1062159 | ASCRM-RLB-13           | Technical Criterion |
| 1062160 | ASCRM-RLB-14           | Technical Criterion |
| 1062161 | ASCRM-RLB-15           | Technical Criterion |
| 1062162 | ASCRM-RLB-16           | Technical Criterion |
| 1062163 | ASCRM-RLB-17           | Technical Criterion |
| 1062164 | ASCRM-RLB-18           | Technical Criterion |
| 1062165 | ASCRM-RLB-19           | Technical Criterion |

#### CISQ-Security

| ID      | Name                   | Type                |
|---------|------------------------|---------------------|
| 1062166 | CISQ-Security          | Business Criterion  |
| 1062177 | ASCSM-CWE-22           | Technical Criterion |
| 1062189 | ASCSM-CWE-78           | Technical Criterion |
| 1062191 | ASCSM-CWE-79           | Technical Criterion |
| 1062194 | ASCSM-CWE-89           | Technical Criterion |
| 1062195 | ASCSM-CWE-99           | Technical Criterion |
| 1062174 | ASCSM-CWE-120          | Technical Criterion |
| 1062175 | ASCSM-CWE-129          | Technical Criterion |
| 1062176 | ASCSM-CWE-134          | Technical Criterion |
| 1062178 | ASCSM-CWE-252-resource | Technical Criterion |
| 1062179 | ASCSM-CWE-327          | Technical Criterion |
| 1062180 | ASCSM-CWE-396          | Technical Criterion |
| 1062181 | ASCSM-CWE-397          | Technical Criterion |
| 1062182 | ASCSM-CWE-434          | Technical Criterion |
| 1062183 | ASCSM-CWE-456          | Technical Criterion |
| 1062184 | ASCSM-CWE-606          | Technical Criterion |
| 1062185 | ASCSM-CWE-667          | Technical Criterion |
| 1062186 | ASCSM-CWE-672          | Technical Criterion |
| 1062187 | ASCSM-CWE-681          | Technical Criterion |
| 1062188 | ASCSM-CWE-772          | Technical Criterion |
| 1062190 | ASCSM-CWE-789          | Technical Criterion |
| 1062192 | ASCSM-CWE-798          | Technical Criterion |
| 1062193 | ASCSM-CWE-835          | Technical Criterion |

### Engineering Dashboard

In ≥ 1.18.0 out of the box, results are displayed in a specific
interface - click the CISQ Assessment Model option to view the results:

![](images/435486765.jpg)

![](images/423264451.jpg)

### Health Dashboard

Out of the box, no results are provided. Tiles can be configured
manually as described above.

### Security Dashboard

Out of the box, results are displayed in a specific interface - click
the CISQ Assessment Model option (after clicking the Risk Investigation
tile in the Application home page) to view the results:

![](images/435486759.jpg)

Only the CISQ-Security Business Criterion will be available (due to the
default filtering implemented in the Security Dashboard):

![](images/423264450.jpg)

### RestAPI

The RestAPI can be used to query both the Dashboard (AED) and
Measurement (AAD) schemas for results, for example:

![](images/435486769.jpg)
