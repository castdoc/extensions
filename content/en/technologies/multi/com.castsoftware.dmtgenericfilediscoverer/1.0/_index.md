---
title: "Generic File Discoverer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtgenericfilediscoverer

## What's new?

See [Release Notes](rn/).

## Extension description

This extension is a dependency used by other extensions for file discovery purposes: it provides the "core" discovery mechanism. Other extensions (whether official CAST Product extensions, or custom extensions) can then use **com.castsoftware.dmtgenericfilediscoverer** with dedicated configuration files for their own specific file discovery purposes. E.g. a custom extension analyzing a language not supported by other official CAST Product extensions can include a custom discoverer (to ensure the correct custom language files are included in the analysis) - this custom extension must also declare a dependency to **com.castsoftware.dmtgenericfilediscoverer** in its `.nuspec` file.

### In what situation should you install this extension?

This extension will be installed automatically as a dependency of any other extension that requires it. This dependency must be defined in the `.nuspec` file of the other extension.

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## How do I use this extension for my own purposes?

In the following hypothetical example, we will build a discoverer that can discover files with the extension `*.test` or `*.tst` in your application source code, and then create appropriate Analysis Units so that this source code is correctly analyzed. This discoverer should then be packaged within your extension. To achieve this, you need to do the following:

- Create the discoverer configuration files (where `xxx` is specific to your discoverer):
  - `DMT.configuration.xml`
  - `xxx.model.xml`
  - `xxx.configuration.xml`
- Create the `.mda` archive for your extension
- Define the dependency to **com.castsoftware.dmtgenericfilediscoverer** in the extension's `.nuspec` file

### Step 1 - Create discoverer configuration files

#### DMT.configuration.xml

This required file declares the name of the `xxx.model.xml` file. In this example our `xxx.model.xml` will be called `TestFiles.model.xml`. It should therefore only contain the following, respecting the exact case of the corresponding file name:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<configuration>
	<models>
		<model path="TestFiles.model.xml"/>
	</models>
</configuration>
```

#### xxx.model.xml

This file declares the discoverers model, i.e. which file extensions will be identified by the discoverer and the associated Universal Analyzer language(s). The file should be named to match the exact case of the `label=` attribute (defined in the file) without any white space: therefore if you define `label="Test Files"`  then the file should be named `TestFiles.model.xml`. 

The following example shows the definition of the file extensions `*.test;*.tst` in the symbol `<entity symbol="TestMainLanguage">`, i.e. corresponding to one single Universal Analyzer language called `TestMainLanguage`:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<document>
	<library symbol="testfilediscoverer" imageRootPath="icons" product="DMT" version="1.0" dependencies="delivery:2.1;dmtcoetechnoua:3.0;dmtgenericfilediscoverer:1.0">
		<entity symbol="TestFileDiscoverer" baseEntity="dmtgenericfilediscoverer.GenericFileDiscoverer" label="Test Files" viewForm="subForm">
			<field symbol="languageSettings" override="true">
				<value formula="true">testMainLanguage</value>
			</field>
			<field symbol="testMainLanguage" valueEntity="TestMainLanguage" columnLabel="" composite="true" multiplicity="1"/>
		</entity>
		<entity symbol="TestMainLanguage" baseEntity="dmtcoetechnoua.UALanguage">
			<field symbol="key" value="TestMainLanguage" override="true"/>
			<field symbol="fileExtensions" value="*.test;*.tst" label="File extensions" override="true"/>
		</entity>
	</library>
	<conversion requires="" provides="testfilediscoverer:1.0"></conversion>
</document>
```

#### xxx.configuration.xml

This file declares the Universal Analyzer language categories, the suffix of the project name, the project generation mode, and optional validation via regular expressions for some of the declared source languages. The file should be named to match the exact case of the `label=` attribute (defined in the `xxx.model.xml` file) without any white space: therefore if you define `label="Test Files"`  then the file should be named `TestFiles.configuration.xml`. 

The following example shows the definition of the `TestMainLanguage`, found in files of type `*.test`, and then an optional validation of the content in those file types based on regular expressions (i.e. the files must contain either `test` or `TEST`) in order to be considered as a project (and therefore Analysis Unit). This content validation ensures that only files that match the validation will be used to determine project references (and therefore Analysis Units) - `*.test` files that do not contain the required content will not cause the creation of a project/Analysis Unit.

Note that in our example, we are building a discoverer for both `*.test` and `*.tst`, however we have chosen not to enforce content validation for `*.tst` file types - if these file types are found they will therefore **always** cause the creation of a project (and Analysis Unit). If you need to validate content for `*.tst` file types a well as `*.test`, add them to the `fileExtensions` attribute, e.g.: `<fileValidation languageName="TestMainLanguage" fileExtensions="*.test;*.tst">`.

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<configuration languageCategories="TEST" languageSuffix="Test" projectGenerationMode="StandardProjects">
    <fileValidation languageName="TestMainLanguage" fileExtensions="*.test">
        <![CDATA[^test$|^TEST$]]>
    </fileValidation>
</configuration>
```

The `projectGenerationMode` attribute accepts two values that define how "projects" are declared: one "project" is equal to one "Analysis Unit":

- `StandardProjects`: generates one project per parent of the source code folders; this is the default value, if omitted.
- `SingleProject`: generates one unique project, in the parent of all the source code folders (or in the root of the source directory tree).

### Step 2 - Create the .mda archive for your extension

The three XML configuration files need to be packaged (without any parent folder) into a zipped archive file with the extension `.mda` and this file should be named to match the `label=` attribute (defined in the `xxx.model.xml` file) without any white space, in lower case and with the version number, in our example `testfilediscoverer-1.0.0.mda`.

This `.mda` file should be placed in the following folder location within the extension, e.g.:

```text
\TOOLS\Plugins\testfilediscoverer-1.0.0\testfilediscoverer-1.0.0.mda
```

### Step 3 - Define the dependency to com.castsoftware.dmtgenericfilediscoverer

Your extension (in which your discoverer will be added) will have its own `.nuspec` file (present at the root of the extension) which declares the ID, name, description, licensing etc. for your extension. You can find out more about this in [Extension description file](../../../../export/EXTEND/Extension+description+file).

You must declare the dependency to the **com.castsoftware.dmtgenericfilediscoverer** extension here, in the tag `<dependencies>`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2013/05/nuspec.xsd">
  <metadata>
    <id>com.castsoftware.uc.testfiles-1.0.0</id>
    <version>1.0.0</version>
    <title>Test File Analyzer</title>
    <authors>User Community</authors>
    <owners>Jimmy H</owners>
    <requireLicenseAcceptance>false</requireLicenseAcceptance>
    <licenseUrl>...</licenseUrl>
    <projectUrl>...</projectUrl>
    <iconUrl>...</iconUrl>
    <description>Support for analyzing test language and creating appropriate analysis units.</description>
    <releaseNotes>...</releaseNotes>
    <tags>ANALYZER DMT DISCOVER TEST FILES</tags>
    <dependencies>
      <dependency id="com.castsoftware.dmtgenericfilediscoverer" version="1.0.4" />
    </dependencies>
  </metadata>
</package>
```
