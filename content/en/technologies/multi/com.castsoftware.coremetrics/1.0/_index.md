---
title: "Core Metrics - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.coremetrics

## Release Notes

See [Release Notes](rn/).

## Description

This extension provides an algorithm for generating Line of Code (LOC)
counts. The algorithm is an improved and more precise version of the
Line of Code count mechanism which is built in to CAST Imaging Core.
Installing the extension will mean that any Line of Code values reported
by CAST products (for example in CAST Imaging) will use the extension's
algorithm and not the mechanism built into CAST Imaging Core.

The extension is designed to resolve a recurring issue found when
looking at the results of CAST analyses when the built-in Line of Code
count mechanism is used: when building user-defined modules (UDM) to
group together specific objects, some objects may be filtered/excluded.
The Line of Count counts produced for these UDMs can therefore be
misleading because the algorithm uses source code LOCs which may include
the specific objects that have been filtered/excluded from the UDM. The
algorithm provided in this extension uses artifact based LOC counts and
is able to distinguish when an object has been excluded from a UDM and
will therefore not include it in the LOC count.

It is possible to revert to the default Line of Code count mechanism
provided in CAST Imaging Core by uninstalling the extension.

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

If you have previously used the deprecated `com.castsoftware.codelines` extension, you must ensure that this extension is removed
from `%PROGRAMDATA%\CAST\CAST\Extensions` on your Node BEFORE you download and install the Core Metrics extension.

The Core Metrics extension will be installed automatically only when using CAST Console / CAST Imaging ≥ 2.4. In any other situation you must download and install it manually.

## What results can you expect?

When compared with LOC counts produced by the mechanism built-in to CAST Imaging Core, there is expected to be a +/- 5% difference in values.

### CAST Imaging Viewer

The LOC values displayed in the Welcome Page specifically for "Technologies" are produced by this extension:

![](../images/620036255.jpg)

### CAST Dashboards

The extension will alter the results produced by the following built-in metrics:

- #10151: Number of Code Lines
- #10107: Number of Comment Lines