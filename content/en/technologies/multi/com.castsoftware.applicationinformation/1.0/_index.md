---
title: "Application Information - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.applicationinformation

## What's new?

See [Release Notes](rn/).

## Description

This extension transfers specific application properties such as the
current version name, current release date etc from the Management
schema to the Analysis schema and the information is used by other CAST
processes and tools, as listed below:

## CAST Imaging Viewer

Information such as current version name will be made available in the
CAST Imaging Viewer ≥ 2.13:

![](../images/620036249.jpg)

A new import into CAST Imaging Viewer using ≥ 2.12 must be actioned so
that the properties made available by the extension can be exploited.

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The Application Information extension will be installed automatically
only when using CAST Imaging Console ≥ 2.5. In any other situation you
must download and install it "manually". A new analysis/snapshot
must be performed after installation to ensure the extension is
executed.

## Configuration instructions

None.
