---
title: "Structural rules"
linkTitle: "Structural rules"
type: "docs"
weight: 7
---

See [https://technologies.castsoftware.com/rules?sec=t_140029&ref=||](https://technologies.castsoftware.com/rules?sec=t_140029&ref=||)

> Note that rules for JEE are provided in the default CAST Assessment Model shipped with CAST Imaging Core and are installed automatically with new CAST schemas. The following extensions also provide JEE specific rules:
> - [JEE Analyzer](../extensions/com.castsoftware.jee)
> - [Jee Rules](../extensions/com.castsoftware.jeerules)
>- [Spring Security](../extensions/com.castsoftware.springsecurity)