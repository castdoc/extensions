---
title: "Log messages"
linkTitle: "Log messages"
type: "docs"
weight: 6
---

## JAVA010

|             |                                                                                                        |
|-------------|--------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA010                                                                                            |
| Message     | Missing home interface for bean '%BEAN_NAME%'                                                          |
| Severity    | Warning                                                                                                |
| Explanation | When this is an EJB2 bean, the java class defined as home interface of the bean has not been resolved. |
| User Action | Please review the xml configuration file.                                                              |

## JAVA011

|             |                                                                                                         |
|-------------|---------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA011                                                                                             |
| Message     | Missing remote interface for bean '%BEAN_NAME%'                                                         |
| Severity    | Warning                                                                                                 |
| Explanation | When this is an EJB2 bean, the java class defined as remote interface of the bean has not been resolved |
| User Action | Please review the xml configuration file.                                                               |

## JAVA012

|             |                                                                                                                                                                                                                             |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA012                                                                                                                                                                                                                 |
| Message     | Missing primary key class for bean '%BEAN_NAME%''                                                                                                                                                                           |
| Severity    | Warning                                                                                                                                                                                                                     |
| Explanation |  An EJB2 entity bean does not define a primary key class (see [https://docs.oracle.com/cd/E16439_01/doc.1013/e13981/cmp21cfg001.htm](https://docs.oracle.com/cd/E16439_01/doc.1013/e13981/cmp21cfg001.htm)) |
| User Action | Please review the xml configuration file.                                                                                                                                                                                   |

## JAVA014

|             |                                                                                                                                                                                   |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA014                                                                                                                                                                       |
| Message     | No Home Interface for bean '%BEAN_NAME%' is registered in Knowledge Base                                                                                                          |
| Severity    | Warning                                                                                                                                                                           |
| Explanation | The JEE analyzer cannot find any object created in the Analysis Service schema (aka Knowledge Base) matching the interface home name. Some links will not be created as expected. |
| User Action | Some source files are missing  in the delivery folders. You can look for files referring to the '%BEAN_NAME%'.                                                                    |

## JAVA015

|             |                                                                                                                                                                                                |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA015                                                                                                                                                                                    |
| Message     | No Remote interface for bean '%BEAN_NAME%' is registered in Knowledge Base                                                                                                                     |
| Severity    | Warning                                                                                                                                                                                        |
| Explanation | The JEE analyzer cannot find any object created in the Analysis Service schema (aka Knowledge Base) matching the interface for bean '%BEAN_NAME%'. Some links will not be created as expected  |
| User Action | Some source files are missing in the delivery folders. You can look for files referring to the '%BEAN_NAME%'.                                                                                  |

## JAVA016

|             |                                                                                                                                                                                         |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA016                                                                                                                                                                             |
| Message     | No Bean class '%BEAN_NAME%' is registered in Knowledge Base                                                                                                                             |
| Severity    | Warning                                                                                                                                                                                 |
| Explanation | TThe JEE analyzer cannot find any object created in the Analysis Service schema (aka Knowledge Base) matching the bean class '%BEAN_NAME%'. Some links will not be created as expected. |
| User Action | Some source files are missing in the delivery folders. You can look for files referring to the '%BEAN_NAME%'.                                                                           |

## JAVA017

|             |                                                                                                                                                   |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA017                                                                                                                                       |
| Message     | No Primary key class '%BEAN_NAME%' is registered in Knowledge Base                                                                                |
| Severity    | Warning                                                                                                                                           |
| Explanation | The JEE analyzer cannot find any object created in the Analysis Service schema (aka Knowledge Base) matching the primary key class '%BEAN_NAME%'. |
| User Action | Some source files are missing in the delivery folders. You can look for files referring to the '%BEAN_NAME%'.                                     |

## JAVA021

|             |                                                                                              |
|:------------|:---------------------------------------------------------------------------------------------|
| Identifier  | JAVA021                                                                                  |
| Message     | Invalid or missing transaction type for bean '%BEAN_NAME%'                                   |
| Severity    | Warning                                                                                      |
| Explanation | This warning is displayed when a bean's attribute values are found to be missing or invalid. |
| User Action | Verify the bean's attribute values and set them as required.                                 |

## JAVA024

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Identifier</td>
<td class="confluenceTd"><strong>JAVA024</strong></td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Message</td>
<td class="confluenceTd"><p>Duplicated %ELEMENT_DESCRIPTION% :
'%ELEMENT_NAME%'</p></td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Explanation</td>
<td class="confluenceTd"><p>Duplicate declaration can be found in one of
the follwing cases:</p>
<ul>
<li>More than one parameter type found when scanning, method or
constructor declaration. Example, declare a type parameter &lt;T&gt; in
a method itself declared in a class with a type parameter whose name is
T</li>
<li>Two fields with the same name</li>
<li>In a method, constructor or lambda declaration, two variables or
more have the same name.</li>
<li>Two method declarations with same signature on the same context
(class declaration for instance)</li>
<li>Two types declaration/definitions with same name in same
package.</li>
<li>SQLJ variable has the same name as a field in the current scope</li>
</ul></td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">User Action</td>
<td class="confluenceTd"><p>Most of these errors can be seen as
ill-formed java code, and would require a change in the code for the
warnings to disappear.</p></td>
</tr>
</tbody>
</table>

## JAVA025

|             |                                                                                                                                      |
|-------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA025                                                                                                                          |
| Message     | Files "%FILE1%" and "%FILE2%" have the same type. The first one will be used. Make sure you have choosen the right module directory. |
| Severity    | Warning                                                                                                                              |
| Explanation | The JEE analyzer has identified two files with same type: "%FILE1%" and "%FILE2%" have the same type.                                |
| User Action | You need to make sure you have choosen the right module directory.                                                                   |

## JAVA026

|             |                                                                                                                                 |
|-------------|---------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JEE026                                                                                                                      |
| Message     | You should configure the tld file with uri '%URI%' so that tags attributes values can be properly resolved.                     |
| Severity    | Warning                                                                                                                         |
| Explanation | A TLD is not taken into account by the analyzer resolution mechanism. Many false links will be created.                         |
| User Action | Please update the file cast-tag.extensions.xml file with the uri '%URI%' so that tag attribute values can be properly resolved. |

## JAVA027

|             |                                                                             |
|-------------|-----------------------------------------------------------------------------|
| Identifier  | JAVA027                                                                 |
| Message     | Unable to find file '%FILE_NAME%'                                           |
| Severity    | Warning                                                                     |
| Explanation | The JEE analyzer cannot find the file '%FILE_NAME%' in the delivery folder. |
| User Action | Please check if the file '%FILE_NAME%' can be found in the delivery folder. |

## JAVA029

|             |                                                                    |
|-------------|--------------------------------------------------------------------|
| Identifier  | JAVA029                                                        |
| Message     | No persistence information files found using xml as search pattern |
| Severity    | Warning                                                            |
| Explanation | Please ignore these warnings, they are not relevant.               |
| User Action | Please ignore these warnings, they are not relevant.               |

## JEE027

|             |                                                                                                                                                                                                                         |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JEE027                                                                                                                                                                                                              |
| Message     | You should use uri '%URI%' to configure tags defined by tag files within sub-folder '%SUBFOLDER%' so that those tags attributes values can be properly resolved. Refer to online help on CAST tags extension mechanism. |
| Severity    | Warning                                                                                                                                                                                                                 |
| Explanation | Some Tags are not taken into account by the analyzer resolution mechanism. Many false links will be created.                                                                                                            |
| User Action | Please update the tag files within the sub-folder '%SUBFOLDER%' with the uri '%URI%' so that the tag attribute values can be properly resolved. You can refer to the online help on CAST tags extension mechanism.      |

## JAVA044

|             |                                                                                                                                                                                                                                                                        |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA044                                                                                                                                                                                                                                                            |
| Message     | Syntax not  recognized                                                                                                                                                                                                                                                 |
| Severity    | Warning                                                                                                                                                                                                                                                                |
| Explanation | The JEE analyzer does not know how to handle the source file. This may occur if the Java version in the analysis configuration is incorrect. Otherwise, this warning may be related to new syntax that is not handled by the analyzer or a bug in the analyzer itself. |
| User Action | Please check that the correct Java version is set in the analysis configuration. Otherwise, please contact CAST Support if the syntax is valid.                                                                                                                        |

## JAVA045

|             |                                                                                                                                                                                  |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA045                                                                                                                                                                      |
| Message     | Unable to read file %CAUSE%                                                                                                                                                      |
| Severity    | Warning                                                                                                                                                                          |
| Explanation | This warning is displayed if a file could not be read because it does not exist, is empty or if a file system error was encountered. The cause will be specified in the message. |
| User Action | Take action to resolve this warning based on the cause mentioned in the warning.                                                                                                 |

## JAVA048

|             |                                                                                                                                                                  |
|:------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA048                                                                                                                                                      |
| Message     | %CAUSE%                                                                                                                                                          |
| Severity    | Warning                                                                                                                                                          |
| Explanation | This message is displayed when the analyzer is unable to resolve a classpath specified in an analysis unit. The exact warning message is displayed as the cause. |
| User Action | Update the classpath setting according to the details shown in the warning.                                                                                      |

## JAVA056

|             |                                                                                                                                                                 |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA056                                                                                                                                                     |
| Message     | The persistence storage(table/view) '%TABLE_NAME%' could not be found among selected server objects.                                                            |
| Severity    | Warning                                                                                                                                                         |
| Explanation | The persistence storage (table/view) '%TABLE_NAME%' could not be found among selected server objects.                                                           |
| User Action | Please check if all server objects are created as expected in the Analysis Servvice schema. Otherwise, the selected Analysis Service schema may not be correct. |

## JAVA065

|             |                                                                                                                                                                                 |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JEE065                                                                                                                                                                      |
| Message     | Unable to process included file '%FILE%' (%MESSAGE%)                                                                                                                            |
| Severity    | Warning                                                                                                                                                                         |
| Explanation | This message is reported where a file included in a JSP could not be found by the analyzer.                                                                                     |
| User Action | This included file may not have been delivered (missing in configuration) or there may be a mismatch in relative locations. If so, you need to check the delivered source code. |

## JAVA068

|             |                                                                                        |
|:------------|:---------------------------------------------------------------------------------------|
| Identifier  | JAVA068                                                                            |
| Message     | JAVA068: Duplicate %ELEMENT% declaration : %NAME%                                      |
| Severity    | Warning                                                                                |
| Explanation | This warning is shown when a duplicate declaration of a Java type is found             |
| User Action | The duplicate declaration should be corrected in the source code to avoid this warning |

## JEE072

|             |                                                                                                                               |
|-------------|-------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JEE072                                                                                                                    |
| Message     | Custom tag '%NAME%' not found in tag library '%LIBRARY%' imported with prefix '%PREFIX%'                                      |
| Severity    | Warning                                                                                                                       |
| Explanation | This message is reported when a custom Tag lib is not found in the analysis configuration.                                    |
| User Action | You should add the relevant taglib to resolve the warning. You can refer to the online help on CAST tags extension mechanism. |

## JEE080

|             |                                                                                                                                                                                                          |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JEE080                                                                                                                                                                                               |
| Message     | No taglib declaration found that uses '%PREFIX%' as prefix. Processing of all tags within file '%FILE%' that have that prefix may be incomplete                                                          |
| Severity    | Warning                                                                                                                                                                                                  |
| Explanation | This message may appear for JSP files that are included in other JSP files.                                                                                                                              |
| User Action | The taglib declaration is in the file that includes the other file. If this is the case, you can ignore this warning. First verify that this file is not included in another JSP file via a grep search. |

## JEE088

|             |                                                                                                                                                                                                                                                                                                                                    |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JEE088                                                                                                                                                                                                                                                                                                                         |
| Message     | The uri '%URI%' could not be resolved within the file web.xml or among the jar files deployed with your application                                                                                                                                                                                                                |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                            |
| Explanation | This error message appears when the TLD that contains the uri cannot be found. As consequence the TLD will not be managed, so if an attribute value with the same name as a database table is identified and this value also represents the name of a JSP Bean, an incorrect link will be created with the table and the JSP bean. |
| User Action | You should identify the TLD that is being used and add the associated \*.tld or jar file to the analysis environment.                                                                                                                                                                                                              |

## JAVA089

|             |                                                                                                                                                                                                                                   |
|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA089                                                                                                                                                                                                                       |
| Message     | Class '%CLASS_NAME%' is not visible - one dependency is missing                                                                                                                                                                   |
| Severity    | Warning                                                                                                                                                                                                                           |
| Explanation | This message is issued whenever the analyzer tries to resolve a class name but finds a candidate in an Analysis Unit that is not in the dependency set of the context in which it is being resolved.                              |
| User Action | The symbol would have been found without any warning where a dependency existed. As such, you need to check the dependency between the Analysis Unit or ignore the warning if there is no issue with the dependecy configuration. |

## JAVA090

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Identifier</td>
<td class="confluenceTd"><strong>JAVA090</strong></td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Message</td>
<td class="confluenceTd"><p>Class '%CLASS_NAME%' not found in file as
expected under classpath</p></td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Severity</td>
<td class="confluenceTd">Warning</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Explanation</td>
<td class="confluenceTd">This message is reported when a class
definition could not be found in the source code files nor in the class
path of all Analysis Units, with or without any dependency.</td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">User Action</td>
<td class="confluenceTd"><p>To avoid this warning, you should
either:</p>
<ul>
<li>find the source file in which this class is defined and then add the
appropriate JAR to the class path</li>
<li>or identify the framework in which the type is defined and select
the appropriate Environment Profile</li>
</ul></td>
</tr>
</tbody>
</table>

## JAVA113

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Identifier</td>
<td class="confluenceTd"><strong>JAVA113</strong></td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Message</td>
<td class="confluenceTd"><p>Unable to load %ELEMENT% '%NAME ( Unable to
load class 'java.lang.Class" - Unable to load class
'java.lang.String" )</p></td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Severity</td>
<td class="confluenceTd">Error</td>
</tr>
<tr class="even">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Explanation</td>
<td class="confluenceTd"><p>An attempt is made to resolve basic Java
classes such as "java.lang.Class", "java.lang.Object",
"java.lang.String" during an analysis. These classes are deliverered
within the CAST AIP installation folder.</p>
<p>The error is reported when these classes are not found. In the vast
majority of cases, the error occurs due to a corrupt installation of
CAST AIP that revents the analyzer from resolving the classes.</p></td>
</tr>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">User Action</td>
<td class="confluenceTd"><p>Ensure that the CAST AIP installation folder
is correct and that all expected files are present. In some cases, it
may be necessary to re-install CAST AIP to resolve the error.</p></td>
</tr>
</tbody>
</table>

## JAVA114

|             |                                                                                                                                                                                  |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA114                                                                                                                                                                      |
| Message     | Please make sure that classpath includes core API libraries.                                                                                                                     |
| Severity    | Fatal Error                                                                                                                                                                      |
| Explanation | This error is the direct consequence of the JAVA113 error.                                                                                                                   |
| User Action | Ensure that the CAST AIP installation folder is correct and that all expected files are present. In some cases, it may be necessary to re-install CAST AIP to resolve the error. |

## JAVA121

|             |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA121                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| Message     | No parametrization trigger found for %METHOD_NAME%                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Explanation | A method '%METHOD_NAME%' has been found by the analyzer in Java code specified in your Analysis Unit and and it's not triggering an existing Parametrization rule from the selected Environment Profiles. Consequence: The method's arguments will be sent to a grep mechanism in order to create all possible links. Some of these links could be false links that may need to be ignored by the user through the Dynamic Link Manager.                                                        |
| User Action | You should create a custom Environment Profile in order to define Method Parametrization so that the analyzer can correctly identify links that are defined via custom methods. This will allow you to control the behavior of the JEE analyzer in order to match your custom dynamic programming methods. The Parametrization enables you to tell the JEE analyzer about your custom object creation methods so that it can correctly detect all dynamic and late binding calls automatically. |

## JAVA124

|             |                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA124                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| Message     | Cannot resolve '%NAME%' as %TYPE%%IN%%FROM%                                                                                                                                                                                                                                                                                                                                                                                                                               |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| Explanation | This warning is prompted when a source file or archive is missing in the delivery (missing jar in the class path, or when the selected Java version is not correct). This warning can be also related to missing dependencies between Analysis Units.                                                                                                                                                                                                                     |
| User Action | As consequence of the missing components, some artifacts will not be present in the Analysis Service schema and so metrics and rules based on these artifacts will be impacted. To avoid this issue, you need to identify which library contains the unresolved component and add it in a new Environment Profile (when it is shared by several Applications) or through the class path in the archive list. The dependencies of Analysis Units should be also validated. |

## JAVA127

|             |                                                                                                             |
|-------------|-------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA127                                                                                                 |
| Message     | The target type used for the resolution of the parametrized trigger named "%METHOD_NAME%" is not supported. |
| Severity    | Warning                                                                                                     |
| Explanation | The target type used for the resolution of the parametrized trigger named "%METHOD_NAME%" is not supported. |
| User Action | You should review the parametrization defined in the custom Environment Profile.                            |

## JAVA128

|             |                                                                                                                                                                                                                                                                                                                                                                                                          |
|-------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA128                                                                                                                                                                                                                                                                                                                                                                                              |
| Message     | Annotation '%ANNOTATION_NAME%' is not managed in Environment Profiles                                                                                                                                                                                                                                                                                                                                    |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                                                                                                  |
| Explanation | The annotation whose fully qualified name is ANNOTATION_NAME, is not defined in any Environment Profiles used in the analysis (selected in the Analysis Unit or in the Application).                                                                                                                                                                                                                     |
| User Action | If this annotation is referenced in an existing Environment Profile, then you need to select it. In other cases (for instance for frameworks not yet supported by CAST AIP), you can make a custom Environment Profiles to handle annotation or ignore them: when this annotation is meaningless, you can ignore it or if it has a meaning, you will need to update an Environment Profile to handle it. |

## JAVA131

|             |                                                                                                                                                                                                                                            |
|-------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA131                                                                                                                                                                                                                                |
| Message     | The classes of archive 'C:\ProgramData\CAST\CAST\Extensions\\JEE_EXTENSION\>\EnvProf\J2EE\XXX.jar' are compiled with a different version of JSE than the version specified in this Job. This may lead to incorrect results or syntax error |
| Severity    | Warning                                                                                                                                                                                                                                    |
| Explanation | The JSE version specified in this analysis configuration does not match the version used for the Jar Compilation. This may lead to incorrect results or syntax error (skipped files).                                                      |
| User Action | You should review the analysis configuration and specify the correct JSE version used by your Application. Otherwise, some files will be ignored and skipped during the analysis.                                                          |

## JAVA291

|             |                                                                                                                                                                             |
|:------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | JAVA291                                                                                                                                                                 |
| Message     | JAVA291: %INFORMATION%                                                                                                                                                      |
| Severity    | Info                                                                                                                                                                        |
| Explanation | This message displays information about type resolution when either a type is not found in the current context, or, when candidates were found outside the current context. |
| User Action | This is an informational message about type resolution.                                                                                                                     |

## Too many search paths for partial match ( \>10 )

This message is a warning and will appear during the analysis when the
analyzer tries to find exact matches when creating links. If no exact
match is found, the analyzer uses heuristics to filter and detect
partial matches, and then creates links for all such matches - in this
situation this warning message is displayed in the log.