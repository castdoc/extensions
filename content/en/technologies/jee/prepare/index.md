---
title: "Prepare and deliver the source code"
linkTitle: "Prepare and deliver the source code"
type: "docs"
weight: 4
---

## Information about discovery

Discovery is a process that is actioned during the delivery process.
CAST will attempt to automatically identify "projects" within your
application using a set of predefined rules. This discovery process also
allows CAST Imaging to set the initial analysis configuration settings. Discoverers are:

-   either embedded in CAST Imaging Core:
    - [Eclipse no nature Project Discoverer](../extensions/eclipse-no-nature)
    - [Web JSP Discoverer](../extensions/web-jsp-discoverer)
   
-   or are provided as installable extensions:
    - [Eclipse Project Discoverer](../extensions/com.castsoftware.dmtjeeeclipsediscoverer)
    - [Maven Project Discoverer](../extensions/com.castsoftware.dmtjeemavendiscoverer)
    - [JEE Manifest Discoverer](../extensions/com.castsoftware.dmtjeemanifestdiscoverer)
    - [Gradle Project Discoverer](../extensions/com.castsoftware.dmtjeegradlediscoverer)
    - [JEE Netbeans Discoverer](../extensions/com.castsoftware.dmtjeenetbeansdiscoverer)
    - [JEE File Discoverer](../extensions/com.castsoftware.dmtjeefilediscoverer)

You should read the relevant documentation for each discoverer (provided
in the link above) to understand how the source code will be handled.

CAST Imaging Console will automatically install discoverers based on the
presence of specific source code files in the delivered source code,
these are as follows:

-   [Eclipse Project Discoverer](../extensions/com.castsoftware.dmtjeeeclipsediscoverer) \>\>\>
    if the project contains .project files "with Java nature"
-   [Maven Project Discoverer](../extensions/com.castsoftware.dmtjeemavendiscoverer) \>\>\> if
    the project contains .pom files (except when the type of
    packaging declared in the pom.xml is "pom")
-   [Gradle Project Discoverer](../extensions/com.castsoftware.dmtjeegradlediscoverer) \>\>\> if
    the project contains .gradle or .gradle.kts files
-   [JEE File Discoverer](../extensions/com.castsoftware.dmtjeefilediscoverer) \>\>\> if the project
    contains .java files (only in Console ≥ 2.3)

## Source code delivery

### Prepare the application source code

CAST Imaging Console expects either a ZIP/archive file or source code
located in a folder configured in CAST Imaging Console. You should include in
the ZIP/source code folder all JEE source code, including JAR files if
necessary. CAST highly recommends placing the files in a folder
dedicated to JEE and using sub-folders where necessary. If you are using
a ZIP/archive file, zip the folders in the "temp" folder - but do not
zip the "temp" folder itself, nor create any intermediary folders:

``` java
D:\temp
    |-----JEE-Java
    |-----OtherTechno1
    |-----OtherTechno2
```

### Maven based source code

When analyzing an Application that includes Maven based source code,
you have several choices with regard to specifying where the required
Maven repositories are located. The location of the repository is
crucial to ensure that any associated JAR files can be automatically
discovered and that POM dependencies can also be located. You can do as
follows:

-   You can include the Maven repository when you deliver the source
    code (i.e. in the ZIP or in the designated source code folder).
    Place the contents of the Maven repository (using the same file
    structure) at the root of the ZIP, for example:

``` java
D:\temp
    |-----JEE-Java
    |-----MavenRepo
    |-----OtherTechno1
```

-   You can define a local Maven repository for the target AIP Node
-   You can define a remote HTTP Maven repository for the target AIP
    Node.

AIP Console will also use the above order to prioritise the various
repositories. In other words, if you include a repository in the ZIP or
in the designated source code folder this will be used instead of any
local or remote repositories that have been defined.

See Configuring source code delivery for Maven for more information.

### No .pom or .project file

When analyzing an Application that includes JEE based source code and
this source code does not contain a .pom or .project file Console will
behave as follows (see also Configuring source code delivery for JEE
without .pom file or .project file for more information):

#### Behaviour in Console ≥ 2.3

Console will automatically install the JEE File Discoverer whenever
a .java file is encountered in your delivered source code. This
extension will ensure that the required Analysis Units are created for
your source code and that an analysis can proceed without issue.

#### Behaviour in Console ≤ 2.2

Console is unable to "discover" this code as JEE (Console relies on the
presence of the .pom / .project files). In this situation, the source
code delivery will end in failure. In order to resolve this issue, you
can manually configure Console to install an extension called the JEE
File Discoverer whenever a .java file is encountered in your delivered
source code. This extension will ensure that the required Analysis Units
are created for your source code and that an analysis can proceed
without issue.