---
title: "Analysis configuration"
linkTitle: "Analysis configuration"
type: "docs"
weight: 5
---

## Introduction

As explained in [Prepare and deliver the source code](../prepare), CAST Imaging extracts
relevant information used to create the automated analysis configuration
from the JEE project files. Currently the JEE Analayzer extension
supports these build project files:

-   Eclipse - all version greater than 3
-   Maven - version 2 and 3

For any other build project format (e.g. Apache -ant) CAST Imaging will
not be able to automatically retrieve build information: no Analysis
Unit will be created and no analysis configuration will be provided. To
address this situation, the Analysis Unit and the analysis
configuration should be created manually. This case falls outside of
the out-of-the-box support and is out of scope of a standard analysis.

## Using CAST Imaging Console

CAST Imaging Console exposes the Technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
JEE Technology to display the available options:

![](432472141.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application (i.e. all Analysis Units)
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same or similar as at Application level) that
        allow you to make fine-grained configuration changes.

Settings are initially set according to the information discovered
during the [source code discovery
process](JEE_-_Prepare_and_deliver_the_source_code) when creating a
version. You should check that these auto-determined settings are as
required and that at least one Analysis Unit exists for the specific
technology.

Technology and Analysis Unit settings

Notes on Technology and Analysis Unit settings:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><strong>Technology</strong></td>
<td class="confluenceTd" style="text-align: left;"><ul>
<li>These are the default options that will be used to populate the same
fields at <strong>Analysis Unit </strong>level when the source code
discovery is not able to find a particular setting in the project files.
If you need to define specific settings at Technology level then you can
override them. These settings will be <strong>replicated</strong> at
Analysis Unit level (except as described in the point below).</li>
<li>If you make a change to a specific option at <strong>Analysis Unit
level</strong>, and then subsequently change the same option
at <strong>Technology level</strong>, this setting will NOT be mirrored
back to the Analysis Unit - this is because specific settings at
Analysis Unit level have precedence if they have been changed from the
default setting available at Technology level.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><strong>Analysis
Unit</strong></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>Options available at this level are valid only for the specific
Analysis Unit.</li>
<li>When the Analysis Unit has been created <strong>automatically during
the source code delivery process</strong>, options will "inherit" their
initial configuration settings from the discovery process (i.e.
"project" settings). Where an option could not be defined automatically,
it will "inherit" its initial configuration settings from those defined
at <strong>Technology level</strong>.</li>
<li>Analysis Units that are<strong> manually</strong> defined will
"inherit" their initial configuration settings from the settings defined
at <strong>Technology level</strong></li>
<li>Modifying an identical option at <strong>Technology
level </strong>will automatically update the <strong>same
option</strong> in the <strong>Analysis Unit editor</strong> unless that
specific option has already been modified independently in the Analysis
Unit editor</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

### Technology level settings

![](432472142.jpg)

#### Classpath

Click here to expand...

Classpaths are automatically discovered by the AIP Console through the
inspection of the project files. If the project files are missing or if
you receive , Classpaths can be added manually at Technology level
(i.e. for all Analysis Units) and can be discovered via manual
inspection of the delivered source code or by inquiring with the
Application Team.

The analyzer uses the Classpath information to search for external
classes that are referenced in project code but not defined in
your source code files. Classpath entries can be either a
file (archive file such as a JAR) or a folder. When analyzing a
folder, the analyzer assumes that classes are correctly deployed on
disk and that they respect the naming convention where a package maps to
a folder and a class maps to a file.

If you need to add additional Classpaths, use the Add button to
define your class path files (typically an archive file) or
folders. You will be prompted to select the type you want to add:

![](432472143.jpg)

|                          |                                                                                                                                                                                                                                                                                                                                           |
|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Add New Archive File | If the project uses external classes packaged in a single JAR file, use this option to define the location of a single JAR file. You can define multiple JAR files as separate entries, although consider using the Archive Folder option if the project uses multiple JAR files.                                                 |
| Class Root Folder    | If the project uses external classes defined in .class files, use this option to define the location of the root folder. You can define multiple Class Root Folders. All .class files in the folder and any sub folders will be taken into account (if the Recursive option is enabled) .java and .XML files are ignored. |
| Archive Folder       | If the project uses external classes packaged in multiple JAR files, use this option to define the location of the folder containing the JAR files. You can also define multiple Archive Folders. All JAR files in the folder and any sub folders (if the Recursive option is enabled) will be taken into account.                    |

All three options will open the web interface on the following root
folder location, unless the location has been changed during the [AIP
Console setup and
installation](https://doc.castsoftware.com/display/AIPCONSOLE/Configure+AIP+Node+storage+folder+locations+-+optional+-+v.+1.x):

``` text
<AIP_Node_install_folder>/AipNode/data/upload/java
```

![](432472145.jpg)

Use the green buttons in the bottom right corner to add the required
items - doing so will add the items to the storage location and they can
be re-used for other Applications if necessary:

![](432472144.jpg)

-   use the Upload a folder icon to create a folder in the root
    location into which you can then add your .class or .jar files using
    the Upload a file icon:

![](432472146.jpg)

-   use the Upload a file icon to upload a specific file into the
    current folder:

![](432472147.jpg)

-   Alternatively, if the .jar/.class file/folders have been delivered
    already but are not found (for example you have an alert stating a
    missing file) then you can select the files from the delivered
    source code using the drop down. Then drill down to the delivered
    source code:

![](432472155.jpg)

-   Alternatively, you can upload the required files direct on the AIP
    Node using Remote Desktop/File Explorer into:

``` text
<AIP_Node_install_folder>/AipNode/data/upload/java
```

When you have either uploaded the required files/folders or located them
in the delivered source code, you need to select the required
files/folder by placing a tick in the tick box and then clicking
Save:

![](432472156.jpg)

Selected class paths will be listed:

![](432472158.jpg)

-   If you define any JAR archives (individual archives or archives
    located in a folder), during the analysis the archive(s) will be
    unpacked in memory and analyzed.
-   When defining a class path, instead of listing all the
    subdirectories of package, it is more convenient to add the folder
    that contains the package. For example: if a Java file contains:
    "import com.Pkg1.Pkg2.\*" and these packages are located in the
    folder "\<DIR\>/com/Pkg1/Pkg2", simply add the folder \<DIR\> to the
    class path.

#### Java / JEE versions

Click here to expand...

![](432472162.jpg)

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Java version</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>The analyzer will assume that that <strong>latest version</strong> of
the JDK has been used to compile the application source code. If your
application source code does not use the latest JDK, you can override
the selection by manually choosing a version in the drop down.</p>
<div>
<div>
<ul>
<li>Selecting a JDK version will automatically apply the corresponding
default Environment Profile.</li>
<li>The Java version selection is used for syntax and semantic
checking.</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">JEE (Servlets/JSP) versions</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>The analyzer will assume that that <strong>latest</strong>
Servlet/JSP version has been used to compile the application source
code.</p>
<p>Use the drop down list box to choose the correct option if your
application source code uses an older version. It is possible to obtain
the JSP version in use by opening the application's web.xml file and
locating the DTD version used to define the servlet - this is the
servlet version. The servlet version is 2.3 in the example below:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>&lt;!DOCTYPE web-app PUBLIC
   &quot;-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN&quot;
   &quot;http://java.sun.com/dtd/web-app_2_3.dtd&quot;&gt;</code></pre>
</div>
</div>
<p>Using this information, see the table below for equivalence:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">J2EE version</th>
<th class="confluenceTh">Servlet version</th>
<th class="confluenceTh">JSP version</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">1.2</td>
<td class="confluenceTd">2.1 or 2.2</td>
<td class="confluenceTd">1.1</td>
</tr>
<tr class="even">
<td class="confluenceTd">1.3</td>
<td class="confluenceTd">2.3</td>
<td class="confluenceTd">1.2</td>
</tr>
<tr class="odd">
<td class="confluenceTd">1.4</td>
<td class="confluenceTd">2.4</td>
<td class="confluenceTd">2.0</td>
</tr>
<tr class="even">
<td class="confluenceTd">5</td>
<td class="confluenceTd">2.5</td>
<td class="confluenceTd">2.1</td>
</tr>
<tr class="odd">
<td class="confluenceTd">6</td>
<td class="confluenceTd">3.0</td>
<td class="confluenceTd">2.2</td>
</tr>
<tr class="even">
<td class="confluenceTd">7</td>
<td class="confluenceTd">3.1</td>
<td class="confluenceTd">2.3</td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh">Enable JEE Web Profile Analysis</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This section governs the settings that will be applied if your JEE
project is defined as a web application:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh"><strong>YES</strong></th>
<td class="confluenceTd">When set to the default <strong>YES</strong>
position, the analyzer is capable of analyzing web files (.JSP, .XHTML
etc) even if no <strong>Web Application Descriptor (web.xml)</strong> is
present in each Analysis Unit in the Application. These web files are
instead identified for analysis by exploring each Analysis Unit's
Analysis Unit's <strong>project</strong> path (i.e. the location of the
<strong>.project</strong> file).</td>
</tr>
<tr class="even">
<th class="confluenceTh"><strong>NO</strong></th>
<td class="confluenceTd"><p>When set to the <strong>NO</strong>
position, the behaviour will revert to pre-CAST AIP 8.2.x. In other
words a <strong>Web Application Descriptor (web.xml)</strong> must be
present in the Application and defined in the relevant field in each
Analysis Unit in order that web files (.JSP, .XHTML etc.) are
analyzed.</p></td>
</tr>
</tbody>
</table>
</div>
<div>
<div>
<p><strong>Web Client Files</strong></p>
<p>The JEE Analyzer <strong>does not support</strong> the analysis of
web client files such as *.HTML, *.CSS, *.JS. These files are handled by
the <strong><a
href="../../web/com.castsoftware.webfilesdiscoverer">HTML5
and JavaScript</a></strong> extension.</p>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

#### Java Frameworks

Click here to expand...

For more information about frameworks see [JEE - Framework analysis
tips](JEE_-_Framework_analysis_tips).

The Java Framework section governs the analysis settings used when a
specific Java framework is present in your source code. These
pre-defined settings are designed to address specific recurrent issues
during analysis and correspond to default [Environment
Profiles](JEE_-_Environment_Profiles). Each framework has an
associated drop down list box with various options:

##### JPA (Hibernate), Struts and Spring

By default, the option for these Frameworks will be set to "Latest".
In other words, the analyzer always assumes that you are using the
latest available release supported by the JEE analyzer extension. You
can override the default setting if required:

![](432472164.jpg)

Note that when using JEE Analyzer ≥ 1.1.0, the Struts
Version combo box has no impact: by default, all supported versions of
Apache Struts are handled by default. The combo box will be removed in a
future release.

##### Web Service (WBS) Version and EJB2 (Enterprise Java Bean) Version

Auto discovery and configuration of Web Services (WBS Services) is
supported and Enterprise Java Bean (EJB) is not supported. The
AIP Super Operator will need to gather the required information from the
application team or inspect the delivered source code to determine the
proper configuration of these parameters. Then a decision will need to
be made to select the correct version of each:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh"><div class="content-wrapper">
<p>Web Services</p>
<p><br />
</p>
</div></th>
<td class="confluenceTd"><div class="content-wrapper">
<img src="432472165.jpg" />
<p><strong>CAST AIP ≥ 8.3.6</strong></p>
<p>Auto discovery and configuration of Web Services (WBS Services) is
supported as follows:</p>
<ul>
<li>For <strong>.wsdl</strong> file extension, the default version
selected will be <strong>WSDL 1.1 (Tibco)</strong></li>
<li>For <strong>.wsdd</strong> file extension, the default version
selected will be <strong>Axis WSDD 1.0 (Java)</strong></li>
<li>For <strong>web-services.xml</strong> files, the default version
selected will be <strong>BEA Weblogic 7.0</strong></li>
<li>For <strong>webservices.xml</strong> files the default version
selected will be <strong>JAX-RPC (Java)</strong></li>
</ul>
<p><strong>CAST AIP ≤ 8.3.6</strong></p>
<p>Auto discovery and configuration of Enterprise Java Bean (EJB) is not
supported. The AIP Super Operator will need to gather the required
information from the application team or inspect the delivered source
code to determine the proper configuration of these parameters. Then a
decision will need to be made to select the correct
version.<strong><br />
</strong></p>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh"><div class="content-wrapper">
<p>EJB</p>
<p><strong><br />
</strong></p>
</div></th>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="432472166.jpg" /></p>
<p>Auto discovery and configuration of Enterprise Java Bean (EJB) is not
supported. The AIP Super Operator will need to gather the required
information from the application team or inspect the delivered source
code to determine the proper configuration of these parameters. Then a
decision will need to be made to select the correct version.</p>
</div></td>
</tr>
</tbody>
</table>

Note:

-   To find out if your application implements web services, simply
    search for files with the extension .wsdl or.wsd, or the
    files web-services.xml or webservices.xml in the source file
    directories. Usually, these files are located in the META-INF
    directory, but as the source folders do not always match the
    production folder tree, we recommend searching all the source files.
-   If you explicitly select an EJB2 profile (see above) then all
    configuration regarding EJB 3.x will be automatically
    deactivated.

##### Other frameworks

Other frameworks, such as those listed below are always handled
"out-of-the box":

-   EJB3, 3.1, 3.2
-   JSF
-   CDI
-   Common-logging
-   Dom4
-   JUnit
-   Log4J
-   MX4J
-   CICS ECI/CTG

Note:

-   versions for EJB 3.x, JSF, CDI will follow the Java
    Version (i.e. the JDK) that is set for the source code.
-   if you explicitly select an EJB2 profile (see above) then all
    configuration regarding EJB 3.x will be automatically
    deactivated.

When unsupported frameworks (i.e. not supported out-of the box) are part
of the delivery, a Custom Environment Profile needs to be created to
enable correct processing. To identify unknown frameworks or custom
frameworks, there are various methods:

-   Find all the XML files and look at the DTD they are referring to.
    This allows you to find frameworks like Spring, Hibernate... (You
    can use UltraEdit or a grep command to find the pattern DTD inside
    XML files in one go). Examples:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Framework</p></th>
<th class="confluenceTh"><p>DTD</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p><strong>Hibernate</strong></p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: DJango"
data-theme="DJango"
style="brush: xml; gutter: false; theme: DJango"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="dt">&lt;!DOCTYPE</span> <span class="dt">hibernate-mapping</span> PUBLIC </span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>&quot;-//Hibernate/Hibernate Mapping DTD 2.0//EN&quot; </span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>&quot;http://hibernate.sourceforge.net/hibernate-mapping-2.0.dtd&quot;<span class="dt">&gt;</span> </span></code></pre></div>
</div>
</div>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: DJango"
data-theme="DJango"
style="brush: xml; gutter: false; theme: DJango"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="dt">&lt;!DOCTYPE</span> <span class="dt">hibernate-configuration</span> PUBLIC </span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>&quot;-//Hibernate/Hibernate Configuration DTD 2.0//EN&quot; </span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>&quot;http://hibernate.sourceforge.net/hibernate-configuration-2.0.dtd&quot;<span class="dt">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p><strong>iBATIS</strong></p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;!</span>DOCTYPE daoConfigPUBLIC <span class="st">&quot;-//iBATIS.com//DTD DAO Configuration 2.0//EN&quot;&quot;http://www.ibatis.com/dtd/dao-2.dtd&quot;</span><span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p><strong>Spring</strong></p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: xml; gutter: false; theme: DJango"
data-theme="DJango"
style="brush: xml; gutter: false; theme: DJango"><pre
class="sourceCode xml"><code class="sourceCode xml"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="dt">&lt;!DOCTYPE</span> <span class="dt">beans</span> PUBLIC &quot;-//SPRING//DTD BEAN//EN&quot; &quot;http://www.springframework.org/dtd/spring-beans.dtd&quot;<span class="dt">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

-   Look at the libraries: the names of the libraries can give you an
    indication. Examples:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Framework</p></th>
<th class="confluenceTh"><p>Libraries</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p><strong>Hibernate</strong></p></td>
<td class="confluenceTd"><p>hibernate3.jar</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p><strong>iBATIS</strong></p></td>
<td class="confluenceTd"><p>ibatis-common-2.jar <br />
ibatis-dao-2.jar <br />
ibatis-sqlmap-2.jar</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p><strong>Spring</strong></p></td>
<td class="confluenceTd"><p>spring-1.2.7.jar</p></td>
</tr>
</tbody>
</table>

-   Look at the different extensions used throughout the entire
    application. This information will also allow you to verify if the
    default extension list used by the analyzer to parse client and
    server source code is complete.Examples:
    -   .jhtml: for Java HTML (See
        [http://en.wikipedia.org/wiki/JHTML](http://en.wikipedia.org/wiki/JHTML)).
        This is used with Dynamo, a Java-based Web application server
    -   .jspx: JSPX pages are a special type of JSP page: They are
        pre-processed by the JSPXServlet, and converted into regular JSP
        pages (See:
        [http://stackoverflow.com/questions/28235/should-i-be-doing-jspx-instead-of-jsp](http://stackoverflow.com/questions/28235/should-i-be-doing-jspx-instead-of-jsp)
    -   .jspf: for JSP Page Fragment
    -   .jrxml: for Jasper Report
        ([http://en.wikipedia.org/wiki/JasperReports#JRXML](http://en.wikipedia.org/wiki/JasperReports#JRXML))

If you have detected this framework through an XML file, you can start
to customize the support of this XML (see Manage XML configuration
files), but in any case, CAST recommend that you read up how it works
and how it is configured.

#### Files to analyze

Click here to expand...

![](432865299.jpg)

This section displays a list of the file extensions for the various
parts of your JEE application. These file extensions are the default
included in the current file filter and only files that match these file
extensions will be taken into account during the analysis. You can
manually update these file extensions if necessary.

#### Dependencies

Click here to expand...

Dependencies are configured at Application level for each
technology and are configured between individual Analysis
Units/groups of Analysis Units (dependencies between technologies are
not available as is the case in the CAST Management Studio). You can
find out more detailed information about how to ensure dependencies are
set up correctly, in Advanced onboarding - review analysis configuration
and execution for AIP Console.

### Analysis Units

This section lists all Analysis Units created either automatically based
on the source code discovery process or manually. You should check that
at least one Analysis Unit exists for the specific technology.
Settings at Analysis Unit level can be modified independently of the
parent Technology settings and any other Analysis Units if required -
click the Analysis Unit to view its technology settings:

![](432865365.jpg)

See [Application - Config - Analysis - Working with Execution
Groups](https://doc.castsoftware.com/display/AIPCONSOLEDRAFT/Application+-+Config+-+Analysis+-+Working+with+Execution+Groups)
for more information about the options available here. Clicking an
Analysis Unit will take you directly to the available Analysis Unit
level settings:

![](449314910.jpg)

#### Technology settings at Analysis Unit level

Click here to expand...

Technology settings at Analysis Unit level are for the most part
identical to those explained above. Some differences exist:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Main Files</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This section lists the location of the files for analysis. This is
usually a location in the designated Deployment folder. You can
<strong>include</strong> additional folder or <strong>exclude</strong>
existing folders if required using the <strong>Add</strong> button. All
three options will open the web interface on the following root folder
location, unless the location has been changed during the <strong><a
href="https://doc.castsoftware.com/display/AIPCONSOLE/Configure+AIP+Node+storage+folder+locations+-+optional+-+v.+1.x">AIP
Console setup and installation</a></strong>:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>%PROGRAMDATA%/CAST/AipConsole/AipNode/upload/</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">Resource Files</th>
<td rowspan="2" class="confluenceTd">These two options are read-only.
They list any files that will be taken into account during the analysis,
such as .XML, .properties and any files that correspond to
the <strong>Enable JEE Web Profile Analysis</strong> option described at
Technology level.</td>
</tr>
<tr class="odd">
<th class="confluenceTh">Web Application Files</th>
</tr>
</tbody>
</table>

