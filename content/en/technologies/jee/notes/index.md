---
title: "Technical notes"
linkTitle: "Technical notes"
type: "docs"
weight: 3
---

## Java JDK

CAST supports the analysis of Java JDK with some limitations. These
limitations are listed below, but are not exhaustive:

-   Compilation errors are not always noticed by the JEE Analyzer.
-   Compilation errors due to generics are not detected. To avoid
    analysis interpretation issues you must ensure that the source code
    that references generics does actually compile.
-   The Java interface related to the implementation of an annotation is
    not stored in the CAST Analysis Service database. Note that this
    doesn't mean that annotation properties of classes, interfaces and
    ... are not stored in the CAST Analysis Service database.
-   Java source code may contain Unicode escapes in Literals or
    Identifiers but not in Keywords, Operators or Separators. The JEE
    Analyzer would produce a syntax warning for unsupported cases.

## JPA / Hibernate

### JPA 2.0 / Hibernate 4

#### Basic or Embeddable types

Collections of Basic or Embeddable types are supported so that
links between these entities and the data table is identified.

#### @org.hibernate.annotations.Entity

In Hibernate 4, @org.hibernate.annotations.Entity has been
deprecated and the advice is to replace its individual attributes/values
with annotations. For example, the following:

``` java
@Entity(dynamicUpdate = true)
Class MyEntity {
}
```

should be replaced with:

``` java
@Entity
@SelectBeforeUpdate
Class MyEntity {
}
```

CAST supports this change and in particular the following Quality Rules
will still function correctly:

-   Avoid select-before-update when the Table is not associated to an
    UPDATE Trigger (7634)

-   Avoid UPDATE trigger firing when not necessary (7490)

### JPA 2.1 / Hibernate 4.3 - 5.x

Access to database stored procedures using one of the below mechanisms
is now supported:

-   Via the annotation @NamedStoredProcedureQuery
-   Or via the tag named-stored-procedure-query when configuring via
    XML in orm.xml

## JEE Web Apps

### Support of Java Servlet annotations

The Java Servlet annotation @WebServlet introduced in JEE 1.6
(Java Servlet 3.0) is now supported by CAST AIP and links between JSP
and Java files made using this annotation will be correctly resolved by
CAST AIP. Important notes:

-   CAST AIP will:  
    -   create prototype links from the servlet to the annotated class
    -   create a servlet mapping for each urlPatterns whose parent is
        the servlet and create an access page forward link to the
        servlet
    -   handle any URL matching the urlPatterns to create the
        correctlink (on Struts - the action mapping, JSF - the managed
        bean), when not defined on the servlet mapping
-   Other annotations provided in the javax.servlet.annotations
    package are not supported, however, their presence in
    Application source code will not cause warnings in the analysis log.

Note that by default, the analyzer will assume that the source code
always uses the latest JEE Servlet release, however, this can be
overridden and a specific release can be selected:

![](383881237.jpg)

## Spring Framework

### Spring 1.2.8

While using the Spring Framework 1.2.8 environment profile, the J2EE
analyzer does not create objects for bean properties. As a consequence,
links are created directly between beans, no matter if the reference to
another bean occurs inside a property or inside an anonymous bean.

### Spring 3.x

@bean and @configuration annotations are supported. For example
with the following source code:

``` java
package com.sample;

@Configuration @Lazy("false")
public class AppConfig {
@Bean(autowire=NO) @Scope("prototype")
public MyService myService()

{ toto(); return new MyServiceImpl(); }

}
```

A Spring bean myService will be created with a RelyOn link to
the MyServiceImpl class with the following properties:

-   singleton: false
-   lazy-init: true
-   autowire: false
-   ImplementationClass: initialized com.sample.MyServiceImpl

In addition, a RelyOn link will be created to the class
com.sample.MyServiceImpl.

### Spring 4.x

Includes support for @Query annotation.

## Spring Batch

Spring Batch is supported as standard by the JEE Analyzer: by
default, Spring frameworks present in the delivered source code will be
analyzed and objects / links relating to Spring Batch will be detected
and stored in the results:

Objects detected:

-   Spring Batch Job
-   Spring Batch Step (belonging to Spring Batch Jobs)

Links resolved:

-   Call links from the Spring Batch Steps to the Spring beans
    implementing the operation (read/process/write)
-   accessExecLink links between the java method (or initializer) that
    calls "run", to the Spring Batch Job

A [Spring Batch extension](Batch_Analyzer) also exists which
provides dedicated support for the Spring Batch framework. CAST
highly recommends that you install and use this extension if your
application contains Spring Batch source code:

-   No further development of Spring Batch support will be added in any
    future release of the JEE Analyzer - all future development will be
    done in the [Spring Batch extension](Batch_Analyzer)
-   Any Spring Batch related objects created by the JEE Analyzer will be
    deleted and replaced with corresponding objects created by
    the [Spring Batch extension](Batch_Analyzer). You should
    therefore expect a small impact on existing analysis results in
    terms of the number of added/deleted objects.

## Struts Framework

### 2.x

Support includes the following annotations:

-   @Action, @Actions
-   @Result
-   @Namespace, @Namespaces
-   @ParentPackage

### 1.0/1.1

#### Configuration types

The following Struts configuration types are not supported:

-   By naming convention
-   By annotation

This means that the analyzer must have the struts.xml file to deduce the
relations between Struts actions and Java.

### OGNL

OGNL: the language that permits to query objects properties from the JSP
or from the XML is not supported and have the following consequences:

-   We have no links from JSP pages to the Action getter/setter methods
    used to fill the JSP page
-   We have no links from a Struts Action to another Struts Action when
    the action is specified with OGNL in the struts.xml fi

### Scope Attributes

No scope attributes for Struts 2.x are created since Struts 2.x use the
value stack to get these values.

## JSP

-   JSP Documents are not supported. This may lead to syntax errors
    during server-side and/or client-side code analysis.
-   Tag Files can be packaged in the /META-INF/tags/ directory in a JAR
    file installed in the /WEB-INF/lib/ directory of the web
    application. In this context, you should extract the Tag Files to an
    equivalent /META-INF/tags/ folder under your application root path
    (at the same level as the WEB-INF folder) before the JEE Analyzer
    can process them.

### Support for classic JSP via the HTML5/JavaScript extension

CAST recommends using the [HTML5 and JavaScript
extension](../../web/html5-js) to
analyze classic JSP applications (JSP versions 1.1 - 2.3) - this is
because the JEE Analyzer is not capable of analyzing the JavaScript
elements of the JSP application. Below is a screenshot of the analysis
of a classic JSP application using the HTML5/JavaScript extension -
click to enlarge:

### EL Expressions

EL expressions for JSP 2.0 that appear in tag attribute values and
template data (client-side code) are supported.

Note that EL 2.1 is not supported.

#### Enabling/ Disabling EL expressions

With regard to the JSP 2.0 specifications, EL expressions can be
disabled in three ways (these three methods of disabling EL expressions
are all supported by CAST):

-   Using the page or tag directive as follows (note that the J2EE
    analyzer applies this setting sequentially: true is used for the
    remaining of the translation unit (main .jsp page plus included
    files) until it finds the same directive with another value for the
    isELIgnored attribute).

    <%@ page isELIgnored="true" %>
    <%@ tag isELIgnored=" true" %> 

-   Escaping any string starting with "${" as show below:

    \${customer.name}

-   Using a jsp-config element within web.xml:

    <jsp-config>
        <jsp-property-group>
            <url-pattern>/jsp/cal/cal1.jsp</url-pattern>
            <el-ignored>true</el-ignored>
        </jsp-property-group>
    </jsp-config>

#### What does the JEE Analyzer do with an EL expression?

An EL expression has the form ${ expression }, where expression
corresponds to the expression to interpret. The expression may have one
of the following forms:

    ${ term1 operator term2 }
    ${ unaire-oprator term }
    ${ term1 operator term2 operator term3 [operator term*]... }

Each term can be:

1) Primary Type

Since everything is an object in EL, the result of the evaluation of a
primary type is its corresponding "wrapper class" (i.e.
java.lang.Integer for int).

2) Implicit Objects

The following objects are implicitly created and stored in the Knowledge
Base (even thought they do not actually host a child object).

-   sessionScope (hosts all session scope attributes/beans)
-   requestScope (hosts all request scope attributes/beans)
-   applicationScope (hosts all application scope attributes/beans)

They have the JSP Attributes Scope type. Other EL implicit objects deal
with HTTP headers/request parameters and cookies. Since no object is
created elsewhere for these elements (not supported yet), they are also
not taken into account while analyzing EL expressions (resolution would
never succeed).

With the example:

-   ${ sessionScope\["book"\].title}

...the analyzer will look for a bean named book within the session scope
(as child of the sessionScope Object) and only within that scope. If the
lookup is successful then:

-   A Bean Property object named title is created as a child of the book
    bean.
-   Let T be the java Type of the book bean and F a method we will look
    for within T.

If T is a List (implements java.util.List) or a Map (implements
java.util.Map) then F is get (java.lang.Object). Otherwise F is
getTitle() (Title is the property name). If F could be resolved within
T, let TF be the return type of F. The following links are created:

<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Caller</strong></td>
<td class="confluenceTd"><strong>Called</strong></td>
<td class="confluenceTd"><strong>Link Type</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd">Page</td>
<td class="confluenceTd">book</td>
<td class="confluenceTd">use</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Tile</td>
<td class="confluenceTd">TF</td>
<td class="confluenceTd">Prototype</td>
</tr>
<tr class="even">
<td class="confluenceTd">Page</td>
<td class="confluenceTd"><ul>
<li>( getTitle()</li>
<li>get(…))</li>
</ul></td>
<td class="confluenceTd">Call</td>
</tr>
</tbody>
</table>

This process is applied recursively for each part of a qualified bean
property name. Note that for expressions within braces (\[expression\])
that use operators, only the "+" is interpreted if its operands are
literals. Hence, ${sessionScope\["book" + 1\].title} will work fine if
there is a "book1" bean within the session scope.

In other cases, evaluation results to an empty string. This is because
there is not yet a reliable way to get actual attributes values.

3) Scope attributes

If a term within an expression does not correspond to an implicit object
or a primitive type then it is resolved as a scoped attribute (a bean
within a scope) in the following scopes: page, session, request then
application.

If the term is a qualified bean property name then it is handled as
described in the previous paragraph (2) Implicit Objects).

Example:

    <tr>
        <td>${book.title}</td>
        <td>${my:caps(book['title'])}</td>
    </tr>

In this example, book is resolved as a scoped attribute (bean). If
resolution succeeds, then a bean property is created.

![](383881223.jpg)

EL Functions

EL functions are Tag Library Descriptor elements. Hence, they are
created by the CAST J2EE Analyzer when parsing the containing .tld file.
These objects are stored in the Analysis Service and have the JSP EL
Function type. An EL Function is used as follows:

    ${ prefix:FunctionName ( [parameters...] ) }

When a reference to an EL Function is made in an expression, the
analyzer resolves the function using the same mechanism it uses for
custom tags:

-   The prefix is used to find the tld
-   The function is then searched for within the found tld.

The table and image below summarize the links/objects that are created:

|            |             |               |                               |
|------------|-------------|---------------|-------------------------------|
| Caller | Called  | Link Type | When                      |
| JSP Page   | Function    | Use           | While analyzing EL expression |
| Function   | Java Method | Prototype     | While analyzing .tld          |

![](383881222.jpg)

Using EL Function in a JSP page:

![](383881224.jpg)

## JSE Generics

The JEE Analyzer supports JSE Generics in Java . This section
provides detailed information about the various objects that are created
during an analysis and how the information is displayed in CAST
Enlighten (both in the Object Manager and in the Graphical View):

### Objects

The following "Generics" specific objects are created by the analyzer:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: center;">Icon</th>
<th class="confluenceTh">Object type</th>
<th class="confluenceTh">Object example</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881229.png"/></p>
</div></td>
<td class="confluenceTd">Generic Java Class</td>
<td
class="confluenceTd"><strong>java.util.HashSet&lt;E&gt;</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881230.png"/></p>
</div></td>
<td class="confluenceTd">Generic Java Constructor</td>
<td class="confluenceTd"><strong>static &lt;T&gt; void
fromArrayToCollection(T[] a, Collection&lt;T&gt; c)</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881231.png"/></p>
</div></td>
<td class="confluenceTd">Generic Java Interface</td>
<td class="confluenceTd"><strong>java.util.Set&lt;T&gt;</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881232.png"/></p>
</div></td>
<td class="confluenceTd">Generic Java Method</td>
<td class="confluenceTd"><strong>static &lt;T&gt; void
fromArrayToCollection(T[] a, Collection&lt;T&gt; c)</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881238.png"/></p>
</div></td>
<td class="confluenceTd">Generic Java Type Parameter</td>
<td class="confluenceTd"><strong>java.util.Set&lt;T&gt;</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881233.png" /></p>
</div></td>
<td class="confluenceTd">Java Instantiated Class</td>
<td
class="confluenceTd"><strong>java.util.Set&lt;String&gt;</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881235.png"/></p>
</div></td>
<td class="confluenceTd">Java Instantiated Constructor</td>
<td
class="confluenceTd"><strong>fromArrayToCollection&lt;String&gt;(String[]
a, Collection&lt;String&gt; c)</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881234.png"/></p>
</div></td>
<td class="confluenceTd">Java Instantiated Interface</td>
<td
class="confluenceTd"><strong>java.util.Set&lt;String&gt;</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="383881236.png" /></p>
</div></td>
<td class="confluenceTd">Java Instantiated Method</td>
<td
class="confluenceTd"><strong>fromArrayToCollection&lt;String&gt;(String[]
a, Collection&lt;String&gt; c)</strong></td>
</tr>
</tbody>
</table>

### Display in CAST Enlighten Object Manager

Java Generics are displayed in the CAST Enlighten Object Manager as
shown in the image below:

![](383881225.jpg)

The following information also applies:

-   The Generic Java classes and interfaces appear under the Java file
    that defines them
-   The Generic Java methods, constructors and type parameters appear
    under the class or interface that defines them like a normal class
-   The Java instantiated classes and interfaces appear in a subfolder
    called Instantiation. So, even if the declaration of an
    instantiated generic class or interface is in a specific file, this
    class will not appear in the folder related to this file. This is
    because: it is possible to have the same instantiation in various
    different files (not just one), and secondly, all the code is in the
    Generic class and not the instantiated generic class. In addition,
    it is possible to see whether a Generic class is used or not.
-   The Java instantiated constructors and methods also appear as a
    child of the folder named with the generic constructor or method.
-   The generic Java type parameter appears under the Generic class,
    interface, constructor or method that defines it. Note that its name
    has been prefixed with the name of the component that defines it to
    avoid a situation where many Java type parameters appear with the
    same name T, U etc.

Note that the methods and fields of instantiated classes and interfaces
are not represented since their actual implementation is located in the
generic class or interface.

### Display in the CAST Enlighten Graphical View

Java Generics are displayed in the CAST Enlighten Graphical View as
follows:

![](383881226.jpg)

### Impacts on Quality Rules

Generic classes, methods and constructors are counted as a standard Java
classes, methods and constructors because all the code is inside these
objects. Quality Rules will apply to these Generic object types exactly
as with any other Java objects. Instantiated classes, methods and
constructors are not counted as classes, methods and constructors
because they do not handle any code (there is no effort to maintain
them), this is why they appear in a specific category.

## CDI support

The JEE Analyzer supports CDI (Contexts and Dependency Injection)
out of the box. No configuration is necessary.

-   Support will follow the Java Version that is set for the source
    code
-   The following annotations/scopes are supported:
    -   @Inject
    -   @Named
    -   @Produces
    -   @Qualifier
    -   All scopes, including the following standard scopes
        -   @SessionScoped
        -   @RequestScoped
        -   @Dependent
    -   Custom scopes built from:
        -   @Scope
        -   @NormalScope
-   @Named CDI beans used in place of JSF specific injection features
    (@ManagedBean annotated classes) are supported
-   CDI Beans are resolved and saved in the CAST Analysis Service
    schema whenever a bean qualifier or scope is used to annotate a
    class.
-   Links:
    -   A Rely On link will be created from the annotated class
        (with a scope and/or a qualifier) to the bean.

    -   When there is an injection of a class on a field using the
        @Inject annotation, a Fire link is created from the
        object the field is injected into, to the constructor(s) of the
        injected type(s). Though CDI only allows one type to match the
        injection constraints, many links can be drawn in case of
        ambiguity, for instance if projects embed more objects than the
        development environment.

## Web Services

If your JEE application contains Web Services defined in .WSDL or .WSDD
files, the CAST Delivery Manager Tool will identify and package these
files correctly according to their extension:

![](383881221.jpg)

### Creation of objects / links

The extension will store (in the CAST Analysis Service schema) objects
that represent the description of Web Services using only the contents
of WSDL files

These objects include:

-   A "WSDL File" object that represents the selected WSDL file and that
    corresponds to the root node of the file tree of other Web Services
    objects stored in the CAST Analysis Service schema.
-   One or more Web Services objects
-   One of more Port objects - children of the Web Service objects
-   One or more Port Type objects - children of the WSDL file
-   One or more Operation objects - children of the Port Type objects 

There is no correlation between the language used to implement the Web
Services and the objects stored in the CAST Analysis Service schema.

Belongs To links and those that follow (Internal links) are created
during this initial phase:

1.  Port == Hi ==\> Port Type  (i)
2.  Port == U ==\> Operation  (ii)

The extension will resolve and create implementation links (External
links) for the Web Services components stored in the Analysis Service

The link type created is the same for all implementations and should be
as follows:

1.  Port Type == P ==\> Object1 (iii)
2.  Operation == P ==\> Object2 (iv)

### How are these objects and links constructed?

Objects (Phase 1): Objects result from the analysis of the WSDL
files. During this analysis, only the "local names" are taken into
account and the files must conform to WSDL 1.1 specifications. The
internal link *Inherit-Implement* (i) is created via a Binding object
(represented in the Graphical View but not stored in the Analysis
Service schema) that associates the port with a Port Type as follows:

    Port > Binding > Port Type

The association Port \> Binding is specified in the value of the
*binding* attribute in the tag \<port\> that declares the Port. This
value is a reference in the form *prefix:name* - the part in bold
(prefix) is optional and variable. Only the name is taken into account
for the resolution of the corresponding Binding.

The second association is also specified in a similar way, but instead
via the value of the *type* attribute in the \<binding\> element that
declares the Binding. The format is identical and only the *name* is
taken into account in the resolution of the Port Type.

The internal link *Use* (ii) results from the Belongs To link between
the Operations and the Port Type. However, for each Operation under the
Port Type that was found in the first phase, a Prototype link will be
created from the Port.

External Links (Phase 2): The external links go from the web service
objects to the objects of the implementation language (VB, Java etc.)

These links are created on the basis of the correspondence files. These
files are, generally, XML files but their format differs from one
language to another. As a result, the extension needs to already know
the format of these files in their different versions for all the
supported languages and thus support of new languages would normally
require the revision of the Web Services Asst. code. In order to work
around this constraint, a format that is unique to the extension has
been developed: an XSLT file is used to transform these files from their
proprietary format into the format used by the extension. In order to do
this, the extension requires the mapping files to actually transform and
the XSLT files to do the transforming. Together these elements are known
as Mapping File Profiles to which two labels can be associated (language
name and a succinct description of the format of the mapping files) -
these two labels are used for identification and selection of the
Profile.

### How is the support provided?

Support for Web Services is provided through the Environment Profile
feature. Assign a Web Services Environment Profile via the Web
Service Version drop down list in the [JEE - Analysis
configuration](JEE_-_Analysis_configuration) (Analysis tab), under
Frameworks:

  

### What about Mapping Files

In the case of VB the mapping files coincide with the WSDL files.
However, their format does not specify a PortType \> Language object
mapping. The only mappings actually specified are:

-   Operation \> VB Function

These elements can be found in the value of the *soapAction*
attribute in the tag binding/operation/XXX:operation where XXX: is a
variable and optional prefix (soap, wsdlSoap etc.).

-   For VB, this value is a URL whose last component is a VB
    function. During the transformation process, only this part is taken
    into account.
-   For Java under Tibco (Axis in reality), the mapping information
    is stored in the WSDD files (Web Services Deployment Descriptor).
    These files can store all the information from a group of web
    services implementation modules or they can unique to each module.
    In the first case, the transformation is applied to one individual
    file and all required information must be present. One file with an
    absolute path is therefore to be specified as the file to be
    transformed (this can be done via a WSDL Environment Profile).
    This results in the following:
    -   Port Type \> Class Java
    -   Operation \> Method Java

In order to function correctly, the Web Services Asst. requires the
following information:

-   Name of the WSDL file containing the components to be mapped.
-   For each correspondence:
    -   WSDL component name (Port Type, Operation)
    -   Name (qualified name for classes) of the object that
        implements the component (Java class/method, VB function, TIBCO
        process etc.).
    -   Object type (*KeysClass* value within the *Keys* table in
        the knowledge base). A list of VB and Java object types and
        their object type number is shown below:

|                               |       |
|-------------------------------|-------|
| Applet                        | 5140  |
| Entity Java Bean              | 5150  |
| Generic Java Class            | 5040  |
| Generic Java Constructor      | 5050  |
| Generic Java Interface        | 5030  |
| Generic Java Method           | 5070  |
| Generic Java Type Parameter   | 5045  |
| JDO                           | 5179  |
| Java Class                    | 5040  |
| Java Constructor              | 5050  |
| Java Data Object              | 5180  |
| Java Enum                     | 5040  |
| Java Enum Item                | 5060  |
| Java Field                    | 5060  |
| Java File                     | 5170  |
| Java Import                   | 5171  |
| Java Initializer              | 5080  |
| Java Instantiated Class       | 5042  |
| Java Instantiated Constructor | 5055  |
| Java Instantiated Interface   | 5035  |
| Java Instantiated Method      | 5075  |
| Java Interface                | 5030  |
| Java Method                   | 5070  |
| Java Package                  | 5020  |
| Java Project                  | 5010  |
| JavaBeans                     | 5149  |
| Message Driven Java Bean      | 5165  |
| Servlet                       | 11112 |
| Session Java Bean             | 5160  |

This information is thus extracted from the mapping files during the
transformation process. For Java, this name must be present in the
WSDD - if no name exists, the resolution process will stop on the first
web component of the type being searched with the name being searched.
Please note that the Port Type names appear under the element
\<service\> with the attribute *name*.

The search for external objects (those specific to each language)
follows the Dependency Rules defined in the Dependencies tab in the
CAST Management Studio - only those objects associated with the project
currently being analyzed must be selected.

Please note that the analyses that create the external objects must have
already been executed.

## EJB

CAST can support Enterprise Java Beans (EJB) deployment files
to EJB 2.0, 2.1 and 3.x specifications. Support is achieved
by creating the EJB object and map it with the Java classes
constituting the bean (bean class, local and remote interfaces and, for
entity beans, primary key class) based on their fully qualified
name. Beans are registered in the Analysis Service with their
logical name independently of their constituting classes.

### How is support provided?

Support for EJB is provided through predefined Environment Profiles:

-   By default, the analyzer will assume that EJB 3.x is used in the
    application source code and will automatically apply this
    environment profile.
-   If your application source code contains EJB 2.x, then you must
    specifically choose the correct predefined environment profile via
    the EJB2 Version drop down list in the [J2EE Technology
    options](JEE_-_Analysis_configuration) (Analysis tab), under
    Frameworks. Doing so will deactivate support for EJB 3.x:

![](383881240.jpg)

### EJB 2.x

The JEE Analyzer supports EJB 2.x specifications and the Web Services
support they provide via a varierty of Environment Profiles.

Each EJB 2.x environment profile:

-   is nearly identical, with only very slight differences due to
    configuration for each specific Web Application Server.
-   supports EJB 2.x session beans defined in ejb-jar.xml
    files and in EJB .jar files.
-   supports database mapping mechanisms for the selected Web
    Application Server - if you are not interested in database mapping
    you can use the "2.x - No Entity" profile since it will only
    create EJB 2.x session beans and corresponding links.

### EJB 3.x

Includes support for JSR 330 @Inject and @Named annotations.

## Discovery

### Maven

#### In the maven dependency, the version should be defined or inherited from the parent. Check the packaging of the source code

When packaging a J2EE Maven resource package, if a pom file has a
parent, the parent file is not scanned and we may have the following
warning message in the discoverer log file:

*In the maven dependency, the version should be defined or inherited
from the parent. Check the packaging of the source code.*

This is a limitation, the parent pom files are not scanned.

Note that this limitation has been removed in CAST AIP ≥ 8.3.8.

## Additional notes

The following section lists technical and functional aspects with regard
to analysis of JEE source code:

### Funnel programming and Strongly Connected Components

Where "funnel programming" causes Strong Connected Components (SCC -
see [Transaction Configuration Center - Information - Definition and
impacts of the large Strongly Connected
Component](https://doc.castsoftware.com/display/TG/Transaction+Configuration+Center+-+Information+-+Definition+and+impacts+of+the+large+Strongly+Connected+Component))
a number of ambiguous links may be generated during an analysis. This
can in turn cause erroneous Function Point values for Transactions.

Example of "funnel programming":

-   An abstract class called "ABS" exists.
-   All of its methods are abstract, except "processFlow", which is
    implemented. 
-   "ABS.processFlow" calls methods "ABS.method1", "ABS.method2" and
    "ABS.method3" (which are abstract)
-   Classes "B", "C" and "D" inherit class "A"
-   All their methods are overriden but "processFlow" ("processFlow" is
    already implemented in "A")
-   A method "methodFromZ" from class "Z" does the following:

``` java
public methodFromZ(){
  B myB = new B();
  myB.processFlow();
}
```

-   The analyzer will draw a link from "methodFromZ" to
    "ABS.processFlow", and then "ABS.processFlow" will have a link to
    "B.method1", "C.method1", "D.Method1", "B.Method2", "C.Method2",
    "D.method1", "D.Method2" which will resemble a "funnel" when viewed
    in CAST Enlighten.
-   CAST recommends creating an "Update CAST Knowledge Base" tool job
    (Content Enrichment editor in the CAST Management Studio) to
    remediate situations where Strongly Connected Components exist.

### Anonymous classes

Objects are not saved in the CAST Analysis Service schema for anonymous
classes and their sub-objects. For example, for the following code where
an anonymous class implementing interface "MyInterface" is dynamically
created. There will be no object saved in the CAST Analysis Service for
this anonymous class, nor for the overriding method "doSomething":

``` java
post( new MyInterface(){
    @Override
    public void doSomething(){
        ....
    }
    );
```

### Java Identifiers containing the '$' character

Identifiers containing the '$' character: '$' is used by java
compilers as a class name delimiter in inner class file generation. It
is also a legal java character authorized in class names. When reversing
class files containing a reference to a class name with a '$', the
analyzer can not determine if the '$' should be kept as is or
interpreted as a '.' delimiter. It always takes the '.' alternative,
meaning that "C$D" will always be interpreted as inner class "D" of
class "C". The impact on the analyzer is that the '$' character is not
supported in class names reversed from class files. However it is
supported for code defined in java source files.

### Overloaded Java methods

J2EE Analyzer cannot distinguish two overloaded methods if the signature
(i.e. the name + parameters) of one of them exceeds 1024 characters.
Both methods will be considered as one single method and links will be
created for only one of them.

### JSP unsupported syntax

#### Comments within the body of an action

JSP comments are not supported within the body of an action (custom and
standard).

#### Use of conditional \<c:if\> in a \<div\> element

The use of a conditional \<c:if\> in a \<div\> element is not supported
and will cause a syntax error when analyzed. For example:

``` xml
<div <c:if test="${not empty currentNode.properties.classNames}">
        class="${currentNode.properties.classNames.string}"
     </c:if> 
     <c:if test="${not empty currentNode.properties.classId}"> 
        id="${currentNode.properties.classId.string}" 
    </c:if> >
    <template:area path="content_${currentNode.name}"/>
</div>
```

### Incorrect JSE registry key in the environment profile

When the registry key specified in a J2EE Environment Profile does not
correspond to the real registry key used by the JSE installed on the
analysis PC, you should duplicate the Environment Profile of the JSE
version that you want to use and reference there the rt.jar of the JSE
installed on your machine. Note that duplication is required to ensure
that the parameterization related to JSE will be effective for this
profile.

### Java code in a JSP tag

When analyzing JSP expressions that are the value of attributes in a
tag, the analyzer may try to identify a bean with the same name as the
java code. The analyzer will not find this bean in the scope as it does
not exist. Take the following JSP example:

  

``` xml
<logic:equal name="<%=Colors.KEY_COLOR%>" value="<%=Colors.RED%>">
```

When this code is analyzed by the J2EE Analyzer, the log will contain
the following warning:

``` xml
Bean 'KEY_COLOR%>' not found in scope
```

### Tag Libraries

J2EE Analyzer is unable to take into account attributes that are
IMPLIED. This occurs when the TLD references a DTD that contains IMPLIED
attributes. When the analyzer encounters such attributes in a JSP page,
it gives the following error message in the log: "Attribute 'xxx' is not
valid for custom tag 'yyy'". Workaround: Modify the TLD and add all
IMPLIED attributes with their values defined in the DTD. This allows
J2EE Analyzer to correctly parse the TLD.

### Supported path resolution for links between JSP files

The JEE analyzer supports the following path configuration rules in JSP
source files:

-   If the path starts with a slash ("/"), then this refers to the Web
    Application Root Path (as configured in the CAST Management Studio)
-   If not, the path is a relative to the folder containing the current
    file

If the above rules are adhered to, then the analyzer will correctly
resolve links between JSP files and other JSP files.

If any other path resolution rule is used, links will not be created.

### Path Builder limitations for rules

Some JEE rules rely on the use of an internal tool called "Path Builder"
to discover recursive paths between objects. The results of this tool
are used for example by the following rule: Avoid direct or indirect
remote calls inside a loop (7962). By default this tool uses the
following limits after which recursive path searching is stopped:

|                                          |       |
|------------------------------------------|-------|
| Parameter                                | Value |
| Maximum Depth of explored Paths          | 50    |
| Maximum Total Number of explored Objects | 10000 |
