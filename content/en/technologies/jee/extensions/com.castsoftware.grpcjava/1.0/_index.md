---
title: "gRPC-java - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.grpcjava

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for gRPC when used inside Java
source code. 

## In what situation should you install this extension?

If your JEE application source code uses the gRPC framework you
should install this extension  (see [gRPC](../../../../multi/grpc/) for more
information about this framework).

## Features

### Object structure and links

-   Creates gRPC service method objects which represent entry-points
    to the server.
-   Creates calls to gRPC service method objects which represent
    exit-points from the client.
-   The dependent [Web Services Linker](../../../../multi/com.castsoftware.wbslinker) is
    responsible for detecting and creating links between
    matching calls to gRPC Method objects and the gRPC
    Method objects.

### Support

#### Server side

When a class inherits from *FooService*Grpc.FooServiceImplBase,
for any overiden method `FooMethod` the analyzer creates a gRPC Method
object named `FooService.FooMethod`. For instance when analyzing the
following source code, the extension creates a Greeter.sayHello gRPC
Method and a callLink to the Java Method `sayHello`.

``` java
public class GreeterService extends GreeterGrpc.GreeterImplBase {
    @Override
    public void sayHello(GreeterOuterClass.HelloRequest request, StreamObserver<GreeterOuterClass.HelloReply> responseObserver) {
        String message = "Hello " + request.getName();
        log.info("Returning " +message);
    }
}
```

![](../images/439222389.png)

#### Client side

The gRPC frameworks provide stubs for the client side. A gRPC call can
be carried out from that stub. When analyzing the following source code,
this extension will generate a call to gRPC Method object
name` Greeter.sayHello`. 

``` java
import io.grpc.examples.GreeterGrpc;

public class DemoAPpTest {

  private static final GreeterGrpc.GreeterStub stub = GreeterGrpc.newStub(channel);


  protected void init(VaadinRequest vaadinRequest) {
    try {
      stub.sayHello(GreeterOuterClass.HelloRequest.newBuilder().setName("name").build());
    } catch (StatusRuntimeException e) {
    }      
```

 [Web Services Linker](Web_Services_Linker) is then responsible for
linking that call to gRPC Method to any matching gRPC service
method.

![](../images/439222388.png)

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## What source code is needed by our analyzer?

In order to develop an application which uses gRPC one needs to go
through the following steps: 

-   write at least one \*.proto file. This file is used to define
    the name of the services and their methods which will be available
    for remote procedure calls. 
-   use protoc to generate source code (in a chosen language)
    containing the client and server. This code is generated based on
    the \*.proto files. 
-   define the services and methods available through gRPC by extending
    the generated server.
-   calls to these methods are carried out using stubs defined in the
    generated source code.

Our analyzers rely on the naming convention which is used by gRPC for
generating the source code based on the \*.proto files. Our
analyzers do not need the \*.proto file nor the generated source
code.

If the generated source code has been modified, analyzers will probably
not be able to connect gRPC calls from clients to servers.

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The extension requires that the
following other CAST extensions are also installed:

-   [JEE Analyzer](../../com.castsoftware.jee/)
-   [Web Services Linker](../../../../multi/com.castsoftware.wbslinker/)
-   com.castsoftware.internal.platform (internal extension)

Note that:

-   any dependent extensions are automatically downloaded and
    installed for you. You do not need to do anything.
-   the [JEE Analyzer](../../com.castsoftware.jee/) is not a dependency, but this
    extension will be automatically installed when JEE/Java source code
    is delivered for analysis.

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, should manually install the
extension:

![](../images/632455213.jpg)

## What results can you expect?

### Objects

| Icon | Description |
|------|-------------|
| ![](../images/439222387.png) | Java gRPC Service Method     |
| ![](../images/439222387.png) | call to gRPC Service Method  |

## Current known limitations

- Only the files which contain an import containing at least one"grpc" string are currently analyzed with this extension. 
- Cloud endpoints with gRPC is not supported
- gRPC Cloud Pub/Sub is not supported
- Multi-line method call are not supported for the creation of calls to gRPC service method. No call to gRPC service can be created from the following code:

``` java
stub
   .foo()
```
