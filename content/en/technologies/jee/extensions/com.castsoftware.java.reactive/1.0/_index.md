---
title: "Reactive Programming for Java - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.reactive 

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Java Reactive web operation
calls made from Java code through
packet org.springframework.web.reactive.function.server

The chosen approach for modelization is about web operation object on
the server-side which is shown in more detail in the following table.

## Supported API

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Method</th>
<th class="confluenceTh">Support</th>
<th class="confluenceTh">Parameter(s)</th>
<th class="confluenceTh">Notes</th>
</tr>
&#10;<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.route</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd"><p>Normally used to pair with
RouterFunctions.Builder.X methods</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate  </p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> A<span class="op">)</span></span></code></pre></div>
</div>
</div>
1 web operation object:
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts</li>
<li><strong>Handler:</strong> A</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.nest</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate </p>
<p>RouterFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="fu">nest</span><span class="op">(</span>RequestPredicates<span class="op">.</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> <span class="fu">route</span><span class="op">(</span>RequestPredicates<span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/&quot;</span><span class="op">),</span> A<span class="op">)</span></span></code></pre></div>
</div>
</div>
<p>1 web operation object:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/</li>
<li><strong>Handler:</strong> A</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.route</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate  </p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">().</span><span class="fu">route</span><span class="op">(</span>RequestPredicates<span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> B<span class="op">).</span><span class="fu">build</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p>1 web operation object:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts</li>
<li><strong>Handler:</strong> B</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.add</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RouterFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">().</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/main&quot;</span><span class="op">,</span> builder <span class="op">-&gt;</span> builder<span class="op">.</span><span class="fu">add</span><span class="op">(</span><span class="fu">route</span><span class="op">().</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">,</span> B<span class="op">).</span><span class="fu">build</span><span class="op">())</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>                                        <span class="op">.</span><span class="fu">add</span><span class="op">(</span><span class="fu">route</span><span class="op">().</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/home&quot;</span><span class="op">,</span> B<span class="op">).</span><span class="fu">build</span><span class="op">()))</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>                        <span class="op">.</span><span class="fu">build</span><span class="op">();</span></span></code></pre></div>
</div>
</div>
<p>2 web operation objects:</p>
<p><u>First:</u></p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /main/posts</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p><u>Second:</u></p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /main/home</li>
<li><strong>Handler:</strong> B</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.nest</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>Supplier&gt;</p></td>
<td rowspan="2" class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb5"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">().</span><span class="fu">nest</span><span class="op">(</span>RequestPredicates<span class="op">.</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/main&quot;</span><span class="op">),</span> </span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>builder <span class="op">-&gt;</span> builder<span class="op">.</span><span class="fu">add</span><span class="op">(</span><span class="fu">route</span><span class="op">().</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">,</span> B<span class="op">).</span><span class="fu">build</span><span class="op">())</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a>                  <span class="op">.</span><span class="fu">add</span><span class="op">(</span><span class="fu">route</span><span class="op">().</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/home&quot;</span><span class="op">,</span> B<span class="op">).</span><span class="fu">build</span><span class="op">()))</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a>                        <span class="op">.</span><span class="fu">build</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p>2 web operation objects:</p>
<p><u>First:</u></p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /main/posts</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p><u>Second:</u></p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /main/home</li>
<li><strong>Handler:</strong> B</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>Consumer</p></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.path</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>Supplier&gt;</p></td>
<td rowspan="2" class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb6"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">().</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">,</span> builder <span class="op">-&gt;</span> builder<span class="op">.</span><span class="fu">GET</span><span class="op">(</span>B<span class="op">)).</span><span class="fu">build</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p>1 web operation object:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/{}</li>
<li><strong>Handler:</strong> B</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>Consumer</p></td>
</tr>
<tr class="even">
<td rowspan="4"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.GET</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb7"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">().</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">,</span> <span class="op">()</span> <span class="op">-&gt;</span> <span class="op">{</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a>                                <span class="cf">return</span> RouterFunctions<span class="op">.</span><span class="fu">route</span><span class="op">()</span></span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a>                                        <span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/&quot;</span><span class="op">,</span> B<span class="op">)</span></span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a>                                        <span class="op">.</span><span class="fu">POST</span><span class="op">(</span><span class="st">&quot;/&quot;</span><span class="op">,</span> C<span class="op">)</span></span>
<span id="cb7-5"><a href="#cb7-5" aria-hidden="true" tabindex="-1"></a>                                        <span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/{id}&quot;</span><span class="op">,</span> D<span class="op">)</span></span>
<span id="cb7-6"><a href="#cb7-6" aria-hidden="true" tabindex="-1"></a>                                        <span class="op">.</span><span class="fu">build</span><span class="op">();</span></span>
<span id="cb7-7"><a href="#cb7-7" aria-hidden="true" tabindex="-1"></a>                            <span class="op">})</span></span>
<span id="cb7-8"><a href="#cb7-8" aria-hidden="true" tabindex="-1"></a>                    <span class="op">.</span><span class="fu">build</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p>3 web operation objects:</p>
<p>First:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p>Second:</p>
<ul>
<li><strong>HTTP method:</strong> POST</li>
<li><strong>URL:</strong> /posts/</li>
<li><strong>Handler:</strong> C</li>
</ul>
<p>Third:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/{id}</li>
<li><strong>Handler:</strong> D</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb8"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">().</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">,</span> RequestPredicates<span class="op">.</span><span class="fu">queryParam</span><span class="op">(</span><span class="st">&quot;c&quot;</span><span class="op">,</span> <span class="st">&quot;d&quot;</span><span class="op">),</span> B<span class="op">)</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a>       <span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">,</span> RequestPredicates<span class="op">.</span><span class="fu">queryParam</span><span class="op">(</span><span class="st">&quot;a&quot;</span><span class="op">,</span> <span class="st">&quot;b&quot;</span><span class="op">),</span> B<span class="op">)</span></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a>                    <span class="op">.</span><span class="fu">build</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p>2 web operation objects:</p>
<p>First:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts?c=d</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p>Second:</p>
<ul>
<li><strong>HTTP method:</strong> POST</li>
<li><strong>URL:</strong> /posts?a=b</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p>Normal <strong>url</strong> <em>/posts</em> is no longer valid based
on real test.</p>
</div></td>
</tr>
<tr class="even">
<td rowspan="4"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.POST</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td rowspan="4"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.PUT</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td rowspan="4"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.DELETE</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td rowspan="4"
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.HEAD</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p>
<p>RequestPredicate</p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.before</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">Function</td>
<td rowspan="2" class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb9"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a>RouterFunctions<span class="op">.</span><span class="fu">route</span><span class="op">().</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/hello&quot;</span><span class="op">,</span> </span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a>                        b1 <span class="op">-&gt;</span> b1<span class="op">.</span><span class="fu">nest</span><span class="op">(</span><span class="fu">accept</span><span class="op">(</span>MediaType<span class="op">.</span><span class="fu">APPLICATION_JSON</span><span class="op">),</span> </span>
<span id="cb9-3"><a href="#cb9-3" aria-hidden="true" tabindex="-1"></a>                                 b2 <span class="op">-&gt;</span> b2<span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/get&quot;</span><span class="op">,</span> <span class="kw">this</span><span class="op">::</span>getHello<span class="op">)</span></span>
<span id="cb9-4"><a href="#cb9-4" aria-hidden="true" tabindex="-1"></a>                                         <span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="kw">this</span><span class="op">::</span>handleHello<span class="op">)</span>                                                                                                     </span>
<span id="cb9-5"><a href="#cb9-5" aria-hidden="true" tabindex="-1"></a>                                         <span class="op">.</span><span class="fu">before</span><span class="op">(</span>FilterHandler1<span class="op">))</span></span>
<span id="cb9-6"><a href="#cb9-6" aria-hidden="true" tabindex="-1"></a>                                  <span class="op">.</span><span class="fu">POST</span><span class="op">(</span><span class="kw">this</span><span class="op">::</span>sayHello<span class="op">))</span></span>
<span id="cb9-7"><a href="#cb9-7" aria-hidden="true" tabindex="-1"></a>                                  <span class="op">.</span><span class="fu">after</span><span class="op">(</span>FilterHandler2<span class="op">)</span></span>
<span id="cb9-8"><a href="#cb9-8" aria-hidden="true" tabindex="-1"></a>                        <span class="op">.</span><span class="fu">build</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p>3 web operation objects:</p>
<p>First:</p>
<ul>
<li>HTTP method: GET</li>
<li>URL: /hello/get</li>
<li>Handlers: 
<ol>
<li>getHello</li>
<li>FilterHandler1</li>
<li>FilterHandler2</li>
</ol></li>
</ul>
<p>Second:</p>
<ul>
<li>HTTP method: GET</li>
<li>URL: /hello/{}</li>
<li>Handlers:
<ol>
<li>handleHello</li>
<li>FilterHandler1</li>
<li>FilterHandler2</li>
</ol></li>
</ul>
<p>Third:</p>
<ul>
<li>HTTP method: POST</li>
<li>URL: /hello/{}</li>
<li>Handlers:
<ol>
<li>sayHello</li>
<li>FilterHandler2</li>
</ol></li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.after</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">BiFunction</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunctions.Builder.filter</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd">HandlerFilterFunction</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb10"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a>RouterFunctions<span class="op">.</span><span class="fu">route</span><span class="op">()</span></span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true" tabindex="-1"></a>                <span class="op">.</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/hello&quot;</span><span class="op">,</span> <span class="kw">this</span><span class="op">::</span>handleHello<span class="op">)</span></span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true" tabindex="-1"></a>                <span class="op">.</span><span class="fu">filter</span><span class="op">(</span><span class="kw">this</span><span class="op">::</span>filterLogic<span class="op">)</span></span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true" tabindex="-1"></a>                <span class="op">.</span><span class="fu">build</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p>1 web operation object:</p>
<ul>
<li>HTTP method: GET</li>
<li>URL: /hello</li>
<li>Handlers: 
<ol>
<li>handleHello</li>
<li>filterLogic</li>
</ol></li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunction.andRoute</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate </p>
<p>HandlerFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb11"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> B<span class="op">)</span></span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true" tabindex="-1"></a>       <span class="op">.</span><span class="fu">andRoute</span><span class="op">(</span><span class="fu">POST</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> C<span class="op">)</span></span></code></pre></div>
</div>
</div>
<p>2 web operation objects:</p>
<p>First:</p>
<ul>
<li>HTTP method: GET</li>
<li>URL: /posts</li>
<li>Handler: B</li>
</ul>
<p>Second:</p>
<ul>
<li>HTTP method: POST</li>
<li>URL: /posts</li>
<li>Handler: C</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunction.andNest</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate </p>
<p>RouterFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb12"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="fu">nest</span><span class="op">(</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span></span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true" tabindex="-1"></a>                    <span class="fu">nest</span><span class="op">(</span><span class="fu">accept</span><span class="op">(</span>APPLICATION_JSON<span class="op">),</span> <span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/{id}&quot;</span><span class="op">),</span> D<span class="op">))</span></span>
<span id="cb12-3"><a href="#cb12-3" aria-hidden="true" tabindex="-1"></a>                    <span class="op">.</span><span class="fu">andNest</span><span class="op">(</span><span class="fu">accept</span><span class="op">(</span>APPLICATION_JSON<span class="op">),</span> <span class="fu">route</span><span class="op">(</span><span class="fu">POST</span><span class="op">(</span><span class="st">&quot;/&quot;</span><span class="op">),</span> C<span class="op">)))</span></span></code></pre></div>
</div>
</div>
<p>2 web operation objects:</p>
<p>First:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/{id}</li>
<li><strong>Handler:</strong> D</li>
</ul>
<p>Second:</p>
<ul>
<li><strong>HTTP method:</strong> POST</li>
<li><strong>URL:</strong> /posts/</li>
<li><strong>Handler:</strong> C</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunction.andOther</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RouterFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb13"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="fu">nest</span><span class="op">(</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span></span>
<span id="cb13-2"><a href="#cb13-2" aria-hidden="true" tabindex="-1"></a>                    <span class="fu">nest</span><span class="op">(</span><span class="fu">accept</span><span class="op">(</span>APPLICATION_JSON<span class="op">),</span> <span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/{id}&quot;</span><span class="op">),</span> D<span class="op">))</span></span>
<span id="cb13-3"><a href="#cb13-3" aria-hidden="true" tabindex="-1"></a>                    <span class="op">.</span><span class="fu">andNest</span><span class="op">(</span><span class="fu">accept</span><span class="op">(</span>APPLICATION_JSON<span class="op">),</span> <span class="fu">route</span><span class="op">(</span><span class="fu">POST</span><span class="op">(</span><span class="st">&quot;/&quot;</span><span class="op">),</span> C<span class="op">))</span></span>
<span id="cb13-4"><a href="#cb13-4" aria-hidden="true" tabindex="-1"></a>                <span class="op">).</span><span class="fu">andOther</span><span class="op">(</span><span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/&quot;</span><span class="op">),</span> B<span class="op">))</span></span></code></pre></div>
</div>
</div>
<p>3 web operation objects:</p>
<p>First:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/{id}</li>
<li><strong>Handler:</strong> D</li>
</ul>
<p>Second:</p>
<ul>
<li><strong>HTTP method:</strong> POST</li>
<li><strong>URL:</strong> /posts/</li>
<li><strong>Handler:</strong> C</li>
</ul>
<p>Third:</p>
<ul>
<li><strong>HTTP method:</strong> POST</li>
<li><strong>URL:</strong> /{}</li>
<li><strong>Handler:</strong> B</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RouterFunction.and</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RouterFunction</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb14"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> B<span class="op">)</span></span>
<span id="cb14-2"><a href="#cb14-2" aria-hidden="true" tabindex="-1"></a>                <span class="op">.</span><span class="fu">and</span><span class="op">(</span><span class="fu">route</span><span class="op">(</span><span class="fu">POST</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> C<span class="op">))</span></span>
<span id="cb14-3"><a href="#cb14-3" aria-hidden="true" tabindex="-1"></a>                <span class="op">.</span><span class="fu">and</span><span class="op">(</span><span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts/{id}&quot;</span><span class="op">),</span> D<span class="op">))</span></span></code></pre></div>
</div>
</div>
<p>3 web operation objects:</p>
<p>First:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p>Second:</p>
<ul>
<li><strong>HTTP method:</strong> POST</li>
<li><strong>URL:</strong> /posts</li>
<li><strong>Handler:</strong> C</li>
</ul>
<p>Third:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/{id}</li>
<li><strong>Handler:</strong> D</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.GET</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.POST</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.PUT</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.DELETE</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.HEAD</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.all</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb15"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb15-1"><a href="#cb15-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">(</span>RequestPredicates<span class="op">.</span><span class="fu">all</span><span class="op">(),</span> A<span class="op">)</span></span></code></pre></div>
</div>
</div>
<p>1 web operation object:</p>
<ul>
<li><strong>HTTP method:</strong> ANY</li>
<li><strong>URL:</strong> /{}</li>
<li><strong>Handler:</strong> A</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.path</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>String</p></td>
<td class="confluenceTd"><p>//</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.method</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>HttpMethod</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb16"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true" tabindex="-1"></a><span class="fu">nest</span><span class="op">(</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> <span class="fu">route</span><span class="op">(</span><span class="fu">method</span><span class="op">(</span>HttpMethod<span class="op">.</span><span class="fu">GET</span><span class="op">),</span> B<span class="op">))</span></span></code></pre></div>
</div>
</div>
<p>1 web operation object:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts</li>
<li><strong>Handler:</strong> B</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicates.methods</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>HttpMethod...</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb17"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb17-1"><a href="#cb17-1" aria-hidden="true" tabindex="-1"></a><span class="fu">nest</span><span class="op">(</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">),</span> <span class="fu">route</span><span class="op">(</span>RequestPredicates<span class="op">.</span><span class="fu">method</span><span class="op">(</span>HttpMethod<span class="op">.</span><span class="fu">GET</span><span class="op">),</span> B<span class="op">)</span></span>
<span id="cb17-2"><a href="#cb17-2" aria-hidden="true" tabindex="-1"></a>                   <span class="op">.</span><span class="fu">andRoute</span><span class="op">(</span>RequestPredicates<span class="op">.</span><span class="fu">methods</span><span class="op">(</span>HttpMethod<span class="op">.</span><span class="fu">POST</span><span class="op">,</span> HttpMethod<span class="op">.</span><span class="fu">PUT</span><span class="op">),</span> C<span class="op">))</span></span></code></pre></div>
</div>
</div>
<p>3 web operation objects:</p>
<p>First:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts/{}</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p>Second:</p>
<ul>
<li><strong>HTTP method:</strong> POST</li>
<li><strong>URL:</strong> /posts/{}</li>
<li><strong>Handler:</strong> C</li>
</ul>
<p>Third:</p>
<ul>
<li><strong>HTTP method:</strong> PUT</li>
<li><strong>URL:</strong> /posts/{}</li>
<li><strong>Handler:</strong> C</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicate.and</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb18"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb18-1"><a href="#cb18-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">(</span><span class="fu">path</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">).</span><span class="fu">and</span><span class="op">(</span><span class="fu">method</span><span class="op">(</span>GET<span class="op">)),</span> B<span class="op">)</span></span></code></pre></div>
</div>
</div>
<p>1 web operation object:</p>
<ul>
<li>HTTP method: GET</li>
<li>URL: /posts</li>
<li>Handlers: B</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>org.springframework.web.reactive.function.server.RequestPredicate.or</p></td>
<td class="confluenceTd"><img src="/images/icons/emoticons/check.svg"
class="emoticon emoticon-tick" data-emoticon-name="tick"
alt="(tick)" /></td>
<td class="confluenceTd"><p>RequestPredicate</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Quick explanation:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb19"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb19-1"><a href="#cb19-1" aria-hidden="true" tabindex="-1"></a><span class="fu">route</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/posts&quot;</span><span class="op">).</span><span class="fu">or</span><span class="op">(</span><span class="fu">GET</span><span class="op">(</span><span class="st">&quot;/content&quot;</span><span class="op">)),</span> B<span class="op">)</span></span></code></pre></div>
</div>
</div>
<p>2 web operation objects:</p>
<p>First:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /posts</li>
<li><strong>Handler:</strong> B</li>
</ul>
<p>Second:</p>
<ul>
<li><strong>HTTP method:</strong> GET</li>
<li><strong>URL:</strong> /content</li>
<li><strong>Handler:</strong> B</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

## Supported handler binding

### Lambda

##### Lambda to internal MethodCall

``` java
route(GET("/posts"), A->A.B())
```

Handler is passed as a lambda A-\>A.B() pointing to an internal
MethodCall A.B() which creates a routing value of GET request and
URL value of /posts

Both A (class) and B (method) are declared and defined by user.

##### Lambda to external MethodCall

``` java
route().path("/posts", builder -> builder.GET(ClassC::D)).build()
```

Handler is passed as MethodReference ClassC::D inside an external
MethodCall builder.GET() which creates a routing value of GET
request and URL value of /posts/{}

GET is an API method found in instance of builder of an dedicated
API class

Both ClassC (class) and D (method) are declared and defined by user

### MethodCall

``` java
route(GET("/posts"), E())
```

Handler is passed as a call of an internal method E() which creates a
routing value of GET request and URL value of /posts

### MethodReference

``` java
route(GET("/posts"), this::all)
```

Handler is passed as a reference to an internal method all which creates
a routing value of GET request and URL value of /posts

### Identifier

``` java
route(GET("/posts"), all)
```

Handler is passed as a variable -  an identifier all used to referencing
a method which creates a routing value of GET request and URL value
of /posts 

The referenced method is determined based following this order:

@Bean method specified by @Qualifier \>\> @Bean method annotated with
@Primary \>\> @Bean method with matching declared Bean name \>\> @Bean
method with matching normal name \>\> Unique @Bean method found
(regardless of name).

##### @Bean method specified by @Qualifier

``` java
@Bean
public RouterFunction<ServerResponse> A (@Qualifier("C") HandlerFunction<ServerResponse> B) {
    return route(GET("/posts/{id}"), B);
}
```

 C is the name declared by the @Bean that annotates the method or the
name of the method itself.

##### @Bean method annotated with @Primary

``` java
@Primary
@Bean()
public HandlerFunction<ServerResponse> D() {
    return E;
}
```

##### @Bean method with declared Bean name

``` java
@Bean("F")
public HandlerFunction<ServerResponse> G() {
    return G;
}
```

F is the Bean name of method with G as its normal name, in this case the
name F takes precedence over the name G.

##### Unique @Bean method found

``` java
@Bean("B")
public HandlerFunction<ServerResponse> Y() {
    return Z;
}

@Bean
public RouterFunction<ServerResponse> newRoute(HandlerFunction<ServerResponse> X) {
    return route(GET("/posts/{id}"), X);
}
```

newRoute will automatically bind Y to its argument X, in
other word X will be replaced with Y since Y is the only
present @Bean method in the current configuration.

### Multiple handlers

``` java
RouterFunctions.route()
                .GET("/hello", A)
                .filter(B)
                .build()
```

One web operation object is created with 2 binding handlers: A and B

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |
| com.castsoftware.jee.1.3.5-funcrel  | - | :white_check_mark: |
| com.castsoftware.internal.platform.0.9.15 | - | :white_check_mark: |
| com.castsoftware.wbslinker.1.7.23-funcrel | - | :white_check_mark: |

## Dependencies with other extensions

The Reactive Programming for Java extension requires
that ≥ 1.3.5-funcrel of the [JEE Analyzer](../../com.castsoftware.jee/) is also
installed and used in order to ensure the most complete set of results.
This dependency with the JEE Analyzer is not automatically
handled when installing the Reactive Programming for Java
extension, therefore you must ensure that the [JEE
Analyzer](JEE_Analyzer) is already installed before starting an
analysis.

## Download and installation instructions

The extension will not be automatically installed by CAST Console,
therefore you should ensure that the extension is manually installed
using the Application -
Extensions interface:

![](../images/637370512.png)

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/637370518.png) | Spring WebFlux Get Operation |
| ![](../images/637370515.png) | Spring WebFlux Put Operation |
| ![](../images/637370514.png) | Spring WebFlux Post Operation |
| ![](../images/637370516.png) | Spring WebFlux Delete Operation |
| ![](../images/637370517.png) | Spring WebFlux Any Operation |

### Examples

#### [Spring 5 examples](https://github.com/daggerok/spring-5-examples)

##### Axon lock application

*From axon-lock\axon-app\src\main\java\daggerok\AxonApplication.java*

``` java
 @Bean RouterFunction<ServerResponse> routes() {

      return

          route(POST("/api/v1/register-guest"), request -> {

            final String uuid = UUID.randomUUID().toString();

            final URI uri = request.uriBuilder()
                                   .path(uuid)
                                   .build();

            return created(uri).body(request.bodyToMono(Map.class)
                                            .map(map -> map.get("name"))
                                            .map(name -> {

                                              log.info("create user {}", name);
                                              return "";

                                            }).subscribeOn(Schedulers.elastic()), String.class);

          })

          ;
    }
```

One Java Reactive POST object:

``` java
POST("/api/v1/register-guest")
```

And its associated handler:

``` java
request -> {

            final String uuid = UUID.randomUUID().toString();

            final URI uri = request.uriBuilder()
                                   .path(uuid)
                                   .build();

            return created(uri).body(request.bodyToMono(Map.class)
                                            .map(map -> map.get("name"))
                                            .map(name -> {

                                              log.info("create user {}", name);
                                              return "";

                                            }).subscribeOn(Schedulers.elastic()), String.class);

          }
```

Result:

![](../images/647463266.png)

##### Spring data count query

*From spring-data-jpa-count-query-fix\src\main\java\daggerok\App.java*

``` java
@Configuration
@RequiredArgsConstructor
class WebfluxRoutesConfig {

  private final OrderRepository orderRepository;

  @Bean
  HandlerFunction<ServerResponse> getCountQueryHandler() {
    return request ->
        ok().contentType(APPLICATION_JSON_UTF8)
            .body(Flux.fromIterable(orderRepository.findAllPrices()), Price.class);
  }

  @Bean
  HandlerFunction<ServerResponse> getOrdersHandler() {
    return request ->
        ok().contentType(APPLICATION_JSON_UTF8)
            .body(Flux.fromIterable(orderRepository.findAll()), Order.class);
  }

  @Bean
  HandlerFunction<ServerResponse> fallbackHandler() {
    return request -> {
      final URL url = Try.of(() -> request.uri().toURL())
                         .getOrElseThrow(() -> new RuntimeException("=/"));
      final String protocol = url.getProtocol();
      final int defaultPort = "https".equals(protocol) ? 443 : 80;
      final int currentPort = url.getPort();
      final int port = currentPort == -1 ? defaultPort : currentPort;
      final String baseUrl = format("%s://%s:%d", protocol, url.getHost(), port);
      return ok().body(Flux.just(
          format("GET orders -> %s/api/orders/", baseUrl),
          format("GET pages -> %s/api/", baseUrl),
          format("GET  -> %s/", baseUrl)
      ), String.class);
    };
  }

  @Bean
  RouterFunction routes(final HandlerFunction<ServerResponse> fallbackHandler) {
    return
        nest(
            path("/"),
            nest(
                accept(APPLICATION_JSON),
                route(
                    GET("/api/orders"),
                    getOrdersHandler()
                )
            ).andNest(
                accept(APPLICATION_JSON),
                route(
                    GET("/api"),
                    getCountQueryHandler()
                )
            )
        ).andOther(
            route(
                GET("/home"),
                fallbackHandler
            )
        )
        ;
  }
}
```

Three Java Reactive objects:

First GET object:

``` java
path("/") ... GET("/api/orders")
```

And its corresponding handler:

``` java
request ->
        ok().contentType(APPLICATION_JSON_UTF8)
            .body(Flux.fromIterable(orderRepository.findAll()), Order.class)
```

Result:

![](../images/647463265.png)

Second GET object:

``` java
path("/") ... GET("/api")
```

And its corresponding handler:

``` java
request ->
        ok().contentType(APPLICATION_JSON_UTF8)
            .body(Flux.fromIterable(orderRepository.findAll()), Order.class)
```

Result:

![](../images/647463264.png)

Third GET object:

``` java
GET("/home")
```

And its corresponding handler:

``` java
fallbackHandler
```

Result:

![](../images/665813449.png)

## About Project Reactor

Although it is primarily a library for reactive programming and focusing
on handling asynchronous data streams using reactive types like Mono and
Flux, it does not inherently provide routing or HTTP method creation
functionalities. 

It can be used within the context of an HTTP server framework to handle
incoming requests reactively and execute corresponding logic based on
the requested URL paths and HTTP methods.

## Limitation

Not enough use of functional programming pattern in client applications
to validate the current support performance.
