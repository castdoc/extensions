---
title: "Security for Java - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.securityforjava

## What's new

See [Release Notes](rn/).

## Description

The Security for Java extension automatically generates JEE specific bytecode (also known as "CASTIL") for the User Input Security feature that is part of CAST Imaging. The bytecode is generated during an analysis and is then stored in the Large Intermediate Storage Area (LISA).

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :x: |

## Prerequisites

- RAM: CAST highly recommends that the node is configured with at least **32GB of RAM** to ensure best performance during an analysis. In addition, you should ensure that the node is not running any other unrelated processes when running the Security Analyzer analysis.

## Configuration

There is nothing to configure - if the extension is installed then it will be used to generate the bytecode required by the User Input Security feature.
