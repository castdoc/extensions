---
title: "Apache Commons DbUtils - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.commondbutils

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for SQL query execution calls under Apache Commons DbUtils using Java language.

## Supported Apache Commons DbUtils methods

All supported methods/properties below are used to execute SQL queries on their corresponding server. Other API methods are considered
internally so that the query statements are correctly evaluated.

<table class="wrapped confluenceTable" style="white-space:nowrap;width:100%;">
<tbody>
<tr class="header">
<th class="confluenceTh">Method</th>
<th class="confluenceTh">Parameter(s)</th>
</tr>
&#10;
<tr class="odd">
<td rowspan="2" class="confluenceTd"><p>org.apache.commons.dbutils.QueryRunner.batch<p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p>Object[][] params</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>String sql</strong></p>
                <p>Object[][] params</p>
</td>
</tr>

<tr class="odd">
<td rowspan="4" class="confluenceTd"><p>org.apache.commons.dbutils.QueryRunner.execute</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p><strong>Object... params</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>

<tr class="odd">
<td rowspan="4" class="confluenceTd"><p>org.apache.commons.dbutils.QueryRunner.insert</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p>ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>

<tr class="odd">
<td rowspan="2" class="confluenceTd"><p>org.apache.commons.dbutils.QueryRunner.insertBatch</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p>ResultSetHandler<T> rsh</p>
                <p>Object[][] params</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p>final Object[][] params</p>
</td>
</tr>

<tr class="odd">
<td rowspan="8" class="confluenceTd"><p>org.apache.commons.dbutils.QueryRunner.query</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p><strong>Object param</strong></p>
                <p>ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p><strong>final Object[] params</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p><strong>final Object param</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p><strong>final Object[] params</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>

<tr class="odd">
<td rowspan="6" class="confluenceTd"><p>org.apache.commons.dbutils.QueryRunner.update</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p><strong>final Object param</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p><strong>final Object param</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>

<tr class="odd">
<td rowspan="2" class="confluenceTd"><p>org.apache.commons.dbutils.AsyncQueryRunner.batch</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p>Object[][] params</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>String sql</strong></p>
                <p>Object[][] params</p>
</td>
</tr>

<tr class="odd">
<td rowspan="4" class="confluenceTd"><p>org.apache.commons.dbutils.AsyncQueryRunner.insert</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p>ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>

<tr class="odd">
<td rowspan="2" class="confluenceTd"><p>org.apache.commons.dbutils.AsyncQueryRunner.insertBatch</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
                <p>ResultSetHandler<T> rsh</p>
                <p>Object[][] params</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p>final Object[][] params</p>
</td>
</tr>

<tr class="odd">
<td rowspan="4" class="confluenceTd"><p>org.apache.commons.dbutils.AsyncQueryRunner.query</p></td>
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p>final ResultSetHandler<T> rsh</p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>

<tr class="odd">
<td rowspan="6" class="confluenceTd"><p>org.apache.commons.dbutils.AsyncQueryRunner.update</p></td>
<td class="confluenceTd">
                <p>Connection conn</p>
                <p><strong>String sql</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p><strong>final Object param</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p>final Connection conn</p>
                <p><strong>final String sql</strong></p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p><strong>final Object param</strong></p>
</td>
</tr>
<tr class="even">
<td class="confluenceTd">
                <p><strong>final String sql</strong></p>
                <p><strong>final Object... params</strong></p>
</td>
</tr>

</tbody>
</table>

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## What results can you expect?

### Objects

| Icon | Object type Description | When is this object created ? |
|:----:|-------------------------|-------------------------------|
| ![](../images/query_icon.png)  | Apache Commons DbUtils Query | An object is created for each SQL query found and resolved in an Apache Commons DbUtils CRUD method call  |
| ![](../images/unknown_query_icon.png)  | Apache Unknown Commons DbUtils Query | An object is created for each SQL query or supported callback detected that could not be resolved correctly.  |

### Links

The following links will be created by the Apache Commons DbUtils extension, or by other extensions:

| Link type | Caller type | Callee type | Methods Supported |
|-----------|-------------|:-----------:|-------------------|
| callLink  | Java Method | ![](../images/query_icon.png) | All CRUD methods supported from Apache Commons DbUtils package (see above)  |
| callLink  | Java Method | ![](../images/unknown_query_icon.png) | All CRUD methods supported from Apache Commons DbUtils package (see above)  |
| useLink   | Apache Commons DbUtils Query | ![](../images/table_icon.png) | Created by SQL Analyzer when DDL source files are analyzed  |
| callLink  | Apache Commons DbUtils Query | ![](../images/procedure_icon.png) |   |
| useLink   | Apache Commons DbUtils Query |   ![](../images/missing_table.png) | Created by [Missing tables and procedures for JEE](../../../../sql/extensions/missing-tables/com.castsoftware.jee.missingtable) when the object is not analyzed |
| callLink  | Apache Commons DbUtils Query |   ![](../images/missing_procedure.png) |   |


### Samples

#### [txlcn-tc](https://github.com/codingapi/tx-lcn)
From `txlcn-tc\src\main\java\com\codingapi\txlcn\tc\jdbc\log\TransactionLogExecutor.java`

``` java
public class TransactionLogExecutor {

    private JdbcTransactionDataSource jdbcTransactionDataSource;

    private LogExecutor logExecutor;

    private QueryRunner queryRunner = new QueryRunner();

    public TransactionLogExecutor(LogExecutor logExecutor,JdbcTransactionDataSource jdbcTransactionDataSource) {
        this.logExecutor = logExecutor;
        this.jdbcTransactionDataSource = jdbcTransactionDataSource;
    }

    public void saveLog() throws SQLException {
        JdbcTransaction jdbcTransaction = JdbcTransaction.current();
        //todo insert all
        for(TransactionLog transactionLog:jdbcTransaction.getTransactionLogs()) {
            String sql = logExecutor.insert(transactionLog);
            Connection connection = jdbcTransactionDataSource.getConnection();
            int row = queryRunner.execute(connection, sql, transactionLog.params());
            log.debug("insert-sql=>{},row:{},connection:{}", sql, row,connection);
        }
    }

    public void init(Connection connection) throws SQLException{
        try {
            String createSql = logExecutor.create();
            queryRunner.execute(connection, createSql);
            log.info("transaction log init success .");
        }catch (SQLException exception){
            log.warn("init transaction-log");
        }
    }

    public void delete(Connection connection)throws SQLException{
        List<Long> ids =  JdbcTransaction.current().logIds();
        String sql = logExecutor.delete(ids);
        int row = queryRunner.execute(connection,sql);
        log.debug("delete-sql=>{},row:{}",sql,row);
    }
}

```
and from `txlcn-tc\src\main\java\com\codingapi\txlcn\tc\jdbc\log\MysqlLogExecutor.java`
``` java
public class MysqlLogExecutor implements LogExecutor {

    @Override
    public String insert(TransactionLog transactionLog) {
        return "insert into `transaction_log`(`id`,`group_id`,`sql`,`time`,`flag`) values(?,?,?,?,?)";
    }

    @Override
    public String create() {
        return "\n" +
                "CREATE TABLE `transaction_log` (\n" +
                "  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' ',\n" +
                "  `group_id` varchar(40) DEFAULT NULL,\n" +
                "  `sql` varchar(255) DEFAULT NULL,\n" +
                "  `time` bigint(20) DEFAULT NULL,\n" +
                "  `flag` int(1) DEFAULT NULL,\n" +
                "  PRIMARY KEY (`id`)\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=latin1;\n" +
                "\n" ;
    }

    @Override
    public String delete(List<Long> ids) {
        String id = Joiner.on(",").join(ids);
        return "delete from `transaction_log` where id in ("+id+")";
    }
}
```
Three Apache Commons DbUtils objects are created:

![](../images/3_objects_txlcn.png)

## Limitations

-   Most of tracebacks in evaluating string with common exception
    AttributeError, TypeError, etc. and evaluation that returns an empty value are currently marked to create an unknown querty object.
-   Java Property Mapping is still not perfect.

