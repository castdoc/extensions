---
title: "JAX-RS - 1.5"
linkTitle: "1.5"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.jaxrs

## What's new?

See [Release Notes](rn/).

{{% alert color="info" %}}Note that support for calls to web services
through WebTarget has been removed from 1.5.x (and above) because it was error prone. Please use [REST Service Calls for Java](../../com.castsoftware.java.service/)(com.castsoftware.java.service) instead.{{% /alert %}}

## Description

This extension provides support for JAX-RS.

### In what situation should you install this extension?

The main purpose of this extension is to create HTTP API entry
points, to enable linking from a Web App front end. Therefore if your
Web application contains source code which uses JAX-RS (1.0 (JSR 311) and 2.0 (JSR 339)) and you want to view these object types and
their links with other objects, then you should install this extension. 

<table>
<tbody>
<tr>
<td><img src="../images/669253983.png" /></td>
<td><img src="../images/669253982.png" /></td>
</tr>
<tr>
<td><strong>iOS Front-End Example</strong></td>
<td><strong>AngularJS Front-End Example</strong></td>
</tr>
</tbody>
</table>

### Features

This extension handles JAX-RS Web Services used in J2EE applications,
for example:

``` java
@Singleton
@Path("/printers")
public class PrintersResource {
     @GET
     @Produces({"application/json", "application/xml"})
     public WebResourceList getMyResources() { ... }
      
     @GET @Path("/list")
     @Produces({"application/json", "application/xml"})
     public WebResourceList getListOfPrinters() { ... }
   
     @PUT @Path("/ids/{printerid}")
     @Consumes({"application/json", "application/xml"})
     public void putPrinter(@PathParam("printerid") String printerId,  Printer printer) { ... }
    
     @DELETE @Path("/ids/{printerid}")
     public void deletePrinter(@PathParam("printerid") String printerId) { ... }
 }
```

For each class annotated with javax.ws.rs.Path (@Path):

-   A Web Service object will be created whose name is deduced from the
    value of @Path on the class
-   A Web Service Port object will be created that is a child of the web
    service with the same name and has with a prototype link to the
    class
-   For each method annotated with @GET, @PUT, @DELETE or @POST, the
    following will be created:  
    -   a Web Service operation child of the Web Service Port whose name
        is the concatenation of the @Path specified at the class level
        and the @Path specified at the method level. If no @Path on the
        method, then method path = /
    -   a fire link from the Web Service operation to the method

#### Annotations considered

-   for the URLs:  
    -   javax.ws.rs.Path
    -   jakarta.ws.rs.Path

-   for the access type:
    -   javax.ws.rs.GET
    -   javax.ws.rs.PUT
    -   javax.ws.rs.DELETE
    -   javax.ws.rs.POST

    -   jakarta.ws.rs.GET
    -   jakarta.ws.rs.PUT
    -   jakarta.ws.rs.DELETE
    -   jakarta.ws.rs.POST

#### Basic case

The following Java code:

``` java
import javax.ws.rs.Path;
import javax.ws.rs.GET;


@Path("Service")
public class MyClass
{
    @Path("/getter")
    @GET
    public void getter() {}
}
```

will generate :

![](../images/669253981.png)

#### Inheritance case

The annotations may also be in the parent class or interface:

``` java
import javax.ws.rs.Path;


@Path("Service")
public class MyClass implements MyInterface
{
    public void getter() {}
}
```

Here the interface defines the exposed methods:

``` java
import javax.ws.rs.Path;
import javax.ws.rs.GET;


public interface MyInterface
{
    @Path("/getter")
    @GET
    public void getter();
}
```

and will generate :

![](../images/669253981.png)

Here the interface exposes the main path and the implementation defines
the exposed methods:

``` java
import javax.ws.rs.Path;


public class MyClass implements MyInterface
{
    @Path("/getter")
    @GET
    public void getter() {}
}
```

``` java
import javax.ws.rs.Path;
import javax.ws.rs.GET;

@Path("Service")
public interface MyInterface
{

    public void getter();
}
```

and will generate :

![](../images/669253981.png)

#### Support for User Input Security

Service entry points are created automatically for applications that
have a presentation layer based on JAX-RS with @Path (and associated
annotations) usage. This can be seen in the analysis log file as
follows:

``` text
2018-07-04 20:27:29,227 INFO  SecurityAnalyzer.Processor LoadBlackboxesForApplication cast#spec cast#lib JAXRSServiceEntryPoints
```

This corresponds to the generation of a file in the following location:

``` text
<BytecodeFolder>\com.castsoftware.jaxrs\ServiceEntryPoints.blackbox.xml
```

Note that while the ServiceEntryPoints.blackbox.xml file is generated
when the extension is used with any release of CAST AIP, it will only be
exploited by the CAST User Input Security feature in CAST AIP ≥ 8.3.3.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The JAX-RS extension requires that the
following other CAST extensions are also installed:

-   Web services linker service (internal technical extension)

Note that when using the CAST Extension Downloader to download the
extension and the Manage Extensions interface in CAST Server
Manager to install the extension, any dependent extensions are
automatically downloaded and installed for you. You do not need to
do anything.

### CAST Transaction Configuration Center (TCC) Entry Points

In JAX-RS ≥ 1.3.x, if you are using the extension with CAST
AIP ≥ 8.3.x, a set of JAX-RS specific Transaction Entry Points are
now automatically imported when the extension is installed.
These Transaction Entry Points will be available in the CAST
Transaction Configuration Center:

![](../images/669253980.jpg)

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/669253974.png) | JAX-RS Delete Operation Service |
| ![](../images/669253973.png) | JAX-RS Get Operation Service |
| ![](../images/669253972.png) | JAX-RS Post Operation Service |
| ![](../images/669253971.png) | JAX-RS Put Operation Service |
| ![](../images/669253970.png) | JAX-RS Port |
| ![](../images/669253969.png) | JAX-RS Service |
| ![](../images/669253969.png) | JAX-RS Get Resource Service |

### Rules

None.
