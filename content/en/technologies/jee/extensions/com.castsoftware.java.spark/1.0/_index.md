---
title: "Apache Spark JDBC - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.spark

## What's new?

See [Release Notes](rn/).

## Description

Apache Spark is an open-source unified analytics engine for large-scale
data processing. This extension provides support for Apache Spark JDBC
connections in Java. Spark SQL is a component on top of Spark Core where
data source options of JDBC can be set using read and write methods.

## In what situation should you install this extension?

If your Java application
contains [Apache Spark](https://spark.apache.org/docs/latest/index.html) JDBC API related
source code and you want to view these object types and their links,
then you should install this extension. More specifically the extension
will identify:

-   "callLinks" from Java methods to Apache Spark JDBC Objects

## Technology support

### Apache Spark

The following Apache Spark releases are supported by this extension:

| Release | Supported | Supported Technology |
|---|:-:|:-:|
| 2.0 and above | :white_check_mark: | Java |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For JEE applications using Apache Spark JDBC, the extension will
be automatically. This is in place since
October 2023.

For upgrade, if the Extension Strategy is not set to Auto update, you
can manually install the extension.

## What results can you expect?

### Objects

| Icon | Type Description | When is this object created ? |
|---|---|---|
| ![](../images/669614117.png) | Spark JDBC Read | an object is created for each database table name found in DataFrameReader api and resolved in a JDBC method call |
| ![](../images/669614116.png) | Spark JDBC Write | an object is created for each database table name found in DataFrameWriter api and resolved in a JDBC method call |
| ![](../images/669614115.png) | Spark JDBC SQL Query | an object is created for each SQL query found and resolved in a JDBC method call  |
| ![](../images/unknown_sql_query_icon.png) | Spark JDBC Unknown SQL Query | an object is created for SQL query found and the exact query cannot be resolved  |

### Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller type</th>
<th class="confluenceTh">Callee type</th>
<th class="confluenceTh">API's supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Java method</td>
<td class="confluenceTd"><p>Apache Spark JDBC object</p>
<p>(Spark JDBC Read or</p>
<p>Spark JDBC Write or</p>
<p>Spark JDBC SQL Query or</p>
<p>Spark JDBC Unknown SQL Query)</p></td>
<td
class="confluenceTd">
<details><summary>Apache Spark APIs</summary>
<p>org.apache.spark.sql.DataFrameReader.jdbc</p>
<p>org.apache.spark.sql.DataFrameWriter.jdbc</p>
<p>org.apache.spark.sql.DataFrameReader.load</p>
<p>org.apache.spark.sql.DataFrameWriter.save</p>
<p>org.apache.spark.sql.SQLContext.jdbc</p>
<p>org.apache.spark.sql.SQLContext.load</p></details></td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Spark JDBC SQL Query</td>
<td class="confluenceTd">Table, View</td>
<td rowspan="2" class="confluenceTd">Created by <strong>SQL
Analyzer</strong> when DDL source files are analyzed</td>
</tr>
<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Spark JDBC SQL Query</td>
<td class="confluenceTd">Procedure</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">Spark JDBC SQL Query</td>
<td class="confluenceTd">Missing Table</td>
<td rowspan="2" class="confluenceTd">Created by <strong>Missing tables
and procedures for JEE</strong> extension when the object is not
analyzed</td>
</tr>
<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Spark JDBC SQL Query</td>
<td class="confluenceTd">Missing Procedure</td>
</tr>
</tbody>
</table>

## Code Examples

### Read Operation

Read operation through .jdbc() API

``` java
public static void main(String[] args) {
    SparkSession spark = SparkSession
      .builder()
      .appName("Java Spark SQL data sources example")
      .config("spark.some.config.option", "some-value")
      .getOrCreate();

    String table_name = "CAR";
    sparkRead(spark, table_name);
    spark.stop();
  }
  
  private static void sparkRead(SparkSession spark, String table_name) {
    Properties connectionProperties = new Properties();
    connectionProperties.put("user", "username");
    connectionProperties.put("password", "password");
    
    Dataset<Row> jdbcDF = spark.read()
      .jdbc("jdbc:postgresql:dbserver", table_name, connectionProperties);    
  }
```

![](../images/669614114.png)

Read operation through .load() API

using dbtable

providing table name as dbtable

``` java
private static void sparkReadLoad(SparkSession spark) {
    String tab = "BIKE";
    String opt = "dbtable";   
    Dataset<Row> jdbcDF = spark.read()
      .format("jdbc")
      .option("url", "jdbc:postgresql:dbserver")
      .option(opt, tab)
      .option("user", "username")
      .option("password", "password")
      .load();  
  }
```

![](../images/669614113.png)

providing query as dbtable

``` java
public static void main(String[] args) {
    SparkSession spark = SparkSession
      .builder()
      .appName("Java Spark SQL data sources example")
      .config("spark.some.config.option", "some-value")
      .getOrCreate();

    String sql_query = "INSERT INTO EMPLOYEE (NAME, AGE, DEPT) VALUES ('Har', 23, 'SDE') ";
    sparkReadQueryLoad(spark, sql_query);
    spark.stop();
  }

private static void sparkReadQueryLoad(SparkSession spark, String sql_query) {
    Dataset<Row> jdbcDF = spark.read()
      .format("jdbc")
      .option("url", "jdbc:postgresql:dbserver")
      .option("dbtable", sql_query)
      .option("user", "username")
      .option("password", "password")
      .load();  
  }
```

![](../images/669614112.png)

using query

``` java
private static void sparkQuery(SparkSession spark) {
    String opt = "query";
    String query = "select * from MANAGER where rownum<=20";
    Dataset<Row> jdbcDF = spark.read()
      .format("jdbc")
      .option("url", "jdbc:postgresql:dbserver")
      .option(opt, query)
      .option("user", "username")
      .option("password", "password")
      .load();  
  }
```

![](../images/669614111.png)

Read operation using Map

``` java
public List<String> getTableNames(String JDBC_URI,String JDBC_username,String JDBC_password) {
    List<String> TablesList =  mysql_getTablesName("URL", "username", "password");
    return TablesList;
}
public List<String> mysql_getTablesName(String query, String uri, String username, String password) {
    SparkConf sc = new SparkConf().setAppName("MySQL");
    JavaSparkContext ctx = new JavaSparkContext(sc);
    SQLContext sqlContext = new SQLContext(ctx);
            
    String dbt = "CAR";
    Map<String, String> optionsReadMap = new HashMap<String, String>();
    optionsReadMap.put("url", "MYSQL_CONNECTION_URL");
    optionsReadMap.put("driver", "MYSQL_DRIVER");
    optionsReadMap.put("dbtable", dbt);
    optionsReadMap.put("user", "MYSQL_USERNAME");
    optionsReadMap.put("password", "MYSQL_PWD");
    Dataset<Row> jdbcDF = sqlContext.read().format("jdbc").options(optionsReadMap).load();
}
```

![](../images/669614110.png)

Read operation through SQLContext.load() API using Map

``` java
public static void main(String[] args) {
        //Data source options
        Map<String, String> options = new HashMap<>();
        options.put("driver", MYSQL_DRIVER);
        options.put("url", MYSQL_CONNECTION_URL);
        options.put("dbtable",
                    "(select emp_no, concat_ws(' ', first_name, last_name) as full_name from employees) as employees_name");
        options.put("partitionColumn", "emp_no");
        options.put("lowerBound", "10001");
        options.put("upperBound", "499999");
        options.put("numPartitions", "10");

        //Load MySQL query result as DataFrame
        DataFrame jdbcDF = sqlContext.load("jdbc", options);

        List<Row> employeeFullNameRows = jdbcDF.collectAsList();

        for (Row employeeFullNameRow : employeeFullNameRows) {
            LOGGER.info(employeeFullNameRow);
        }
    }
```

![](../images/669614109.png)

### Write Operation

Write operation through .jdbc() API

``` java
private static void sparkWrite() {
    Properties connectionProperties = new Properties();
    connectionProperties.put("user", "username");
    connectionProperties.put("password", "password");
    
    Dataset<Row> jdbcDF;
    jdbcDF.write()
      .jdbc("jdbc:postgresql:dbserver", "STUDENT", connectionProperties);   
  }
```

![](../images/669614108.png)

Write operation through .save() API

``` java
private static void sparkWriteSave() {
    Dataset<Row> jdbcDF;
    jdbcDF.write()
      .format("jdbc")
      .option("url", "jdbc:postgresql:dbserver")
      .option("dbtable", "FACULTY")
      .option("user", "username")
      .option("password", "password")
      .save();  
  }
```

![](../images/669614107.png)

Write operation using Map

``` java
public List<String> getTableNames(String JDBC_URI,String JDBC_username,String JDBC_password) {
    List<String> TablesList =  mysql_getTablesName("SELECT * FROM AUTO", "RED" , "username", "password");
    return TablesList;
}
public List<String> mysql_getTablesName(String query, String uri, String username, String password) {
    Map<String, String> optionsWriteMap = new HashMap<String, String>();
    optionsWriteMap.put("url", "MYSQL_CONNECTION_URL");
    optionsWriteMap.put("driver", "MYSQL_DRIVER");
    optionsWriteMap.put("dbtable", query);
    optionsWriteMap.put("user", "MYSQL_USERNAME");
    optionsWriteMap.put("password", "MYSQL_PWD");
            
    Dataset<Row> DF;
    DF.write().format("jdbc").options(optionsWriteMap).save();
}
```

![](../images/669614106.png)

### Unknown SQL Query
``` java
private static void sparkQuery(SparkSession spark) {
	String opt = "query";
	String query = getQuery();
	Dataset<Row> jdbcDF = spark.read()
      .format("jdbc")
      .option("url", "jdbc:postgresql:dbserver")
      .option(opt, query)
      .option("user", "username")
      .option("password", "password")
      .load();	
}
```

![](../images/spark_unknown.png)

