---
title: "Eclipse Project Discoverer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtjeeeclipsediscoverer

## What's new ?

See [Release Notes](rn/).

## Extension description

This discoverer detects a project for each Eclipse Java project (.project) identified.

### In what situation should you install this extension?

This extension should be used when you are analyzing Eclipse based JEE applications. The discoverer will automatically detect one project for every Eclipse Java .project file configured as follows:

-   with the org.eclipse.jdt.core.javanature nature.
-   with no nature at all, or with one of the following natures:
    -   com.ibm.etools.j2ee.EAR13Nature
    -   com.ibm.wtp.j2ee.EARNature
    -   org.eclipse.wst.common.modulecore.ModuleCoreNature

One corresponding Analysis Unit (per project) will then be created for analysis purposes.

-   project inheritance and project aggregation is supported
-   the pom.xml must be valid XML and not contain any erroneous characters or spaces - you can check the consistency of an XML file by opening it in a browser - errors will be shown.

### Resulting source package configuration

-   Application root path:
    -   using WEB-INF/web.xml grand parent folder if it exists
-   Source folders configured in .classpath
-   Class path configured in .classpath
-   Java language & environment (1.1 to 8.0) according to .classpath and project settings
-   XML files found in project folder (excluding files in Eclipse .settings folders and output class folders)
-   PROPERTIES files found in project folder (excluding files in output class folders)
-   Libraries according to jar names in the project classpath: JUnit, Log4J, Commons logging, DOM4J, JSF, MX4J, Hibernate, Struts, Spring

### Technical information

The discoverer is already embedded in AIP Core. This embedded version of the extension will not undergo any further updates and instead all functional changes/customer bug fixes will be actioned in the extension.

## Eclipse support

| Eclipse project support |  Supported |
|:------------------------|:----------:|
| ≥ 3.x                   | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

This extension will not be automatically installed when Eclipse related .project files are delivered - the embedded discoverer will be used. Instead if it is required, the extension should be downloaded and installed "manually".

## Packaging messages

The following messages emitted by the discoverer may appear during the
packaging action:

| Format  | Message ID                                                        | Message                                                                                                                                                                     | Remediation                                                                                                                                                                                               |
| ------- | ----------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Error   | cast.dmt.discover.eclipse.jee.createXMLReaderFailure              | Internal error while creating an XML reader                                                                                                                                 | Please contact Support.                                                                                                                                                                                   |
| Error   | cast.dmt.discover.eclipse.jee.ioExceptionInClasspathParsing       | Error during .classpath file parsing. The format may not be supported, the file may be corrupted or it may not be an eclipse at all. The project is ignored: %PROJECT_PATH% | Please contact Support.                                                                                                                                                                                   |
| Error   | cast.dmt.discover.eclipse.jee.saxExceptionInClasspath             | Error during .classpath file parsing. The format may not be supported, the file may be corrupted or it may not be an eclipse at all. The project is ignored: %PROJECT_PATH% | Please contact Support.                                                                                                                                                                                   |
| Error   | cast.dmt.discover.eclipse.jee.createXMLReaderFailure              | Internal error while creating an XML reader                                                                                                                                 | Please contact Support.                                                                                                                                                                                   |
| Error   | cast.dmt.discover.eclipse.jee.ioExceptionInClasspathParsing       | Error during .classpath file parsing. The format may not be supported, the file may be corrupted or it may not be an eclipse at all. The project is ignored: %PROJECT_PATH% | Please contact Support.                                                                                                                                                                                   |
| Error   | cast.dmt.discover.eclipse.jee.saxExceptionInClasspath             | Error during .classpath file parsing. The format may not be supported, the file may be corrupted or it may not be an eclipse at all. The project is ignored: %PROJECT_PATH% | Please contact Support.                                                                                                                                                                                   |
| Warning | cast.dmt.discover.eclipse.jee.userDefinedJREContainerNotSupported | User defined JRE (%PATH%) are not supported. This information will not be used for the project: %PROJECT_PATH%                                                              | Nothing to do in the delivery. Before running the analysis in CMS, you must define the value for the corresponding Analysis Unit or define a default value for the application.                           |
| Warning | cast.dmt.discover.eclipse.jee.unsupportedJVMType                  | The JVM name &quot;%NAME%&quot; is unknown or not supported. This information is ignored for the project: %PROJECT_PATH%                                                    | Nothing to do in the delivery. Before running the analysis in CMS, you must define the value for the corresponding Analysis Unit or define a default value for the application.                           |
| Warning | cast.dmt.discover.eclipse.jee.unsupportedClassPathEntryKind       | Classpath file entry kind &quot;%KIND%&quot; with path &quot;%PATH%&quot; is not supported. This information is ignored for the project: %PROJECT_PATH%                     | Nothing to do in the delivery. Before running the analysis in CMS, you must define the value for the corresponding Analysis Unit or define a default value for the application. This message is expected. |
| Warning | cast.dmt.discover.eclipse.jee.readSettingsFileFailure             | Error during reading of eclipse settings file. The content is ignored for the project: %PROJECT_PATH%                                                                       | Check the content of this file. Before running the analysis in CMS, you must define the java version for the corresponding Analysis Unit or define a default value for the application.                   |
| Warning | cast.dmt.discover.eclipse.jee.noSourceLevelInfoInSettingsFile     | No source level information in eclipse settings file. Maybe the file is corrupted: %PROJECT_PATH%                                                                           | Before running the analysis in CMS, you must define the java version for the corresponding Analysis Unit or define a default value for the application.                                                   |
| Warning | cast.dmt.discover.eclipse.jee.project.multipleWebInfWebXmlFound   | Multiple web application descriptor files has been found. Only the first one will be used in the project configuration: %PROJECT_PATH%                                      | Check the folder structure under this project. You might have duplicated code.                                                                                                                            |
| Warning | cast.dmt.discover.eclipse.jee.project.manifestFileNotFound        | META-INF/MANIFEST.MF is required but was not found at the expected location: %PROJECT_PATH%.                                                                                | The eclipse project is based on OSGi. Check why the file is not provided.                                                                                                                                 |

## Notes

### Missing source folder alerts

When packaging Eclipse based Java code, you may receive Missing source folder alerts even though the source folder that is recorded as being missing is actually present in the source code. This alert is usually generated when no .project file has been found in the folder that is recorded as being missing. To resolve this alert, you can manually add the missing .project file and then re-run the source code delivery.
