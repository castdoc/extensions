---
title: "Program Calls for Java - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java2program

## What's new?

See [Release Notes](rn/).

## Description

This extension is responsible of creating objects representing calls
from a java application to external programs. It is also responsible of
creating objects representing JAR applications. Here *programs* is
understood in a broad sense: it comprises scripts, operating system
commands, and general purpose applications. The link between these
objects and the objects representing the programs themselves (if
present) is performed by a different com.castsoftware.wbslinker
extension during the application analysis level.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## What results can you expect?

### Objects

|  <br>Icon  | Description |
|---|---|
| ![](../images/366968879.png) | Java Call to Java Program |
| ![](../images/366968878.png) | Java Call to Generic Program |
| ![](../images/366968878.png) | Java Call to Mainframe Program |
| ![](../images/366968878.png) | Java Call to Midrange Program |
| ![](../images/670138583.png) | Java JAR Program |

### Supported program types

The primary target are customer programs possibly contained in the
application under analysis. The only exception to this are operating
system commands which can be identified by the property "Operating
system call" (and filter if necessary). The list of supported program
types is given in the table below.

| Program type | Support | Program name (extensions) | Special property |
|---|:---:|---|---|
| Script-like programs | :white_check_mark: | *.py, *.pl, *.sh, *.bash, *.bat, *.jy |  |
| Calls to operating system commands  | :white_check_mark: | cp, chmod, gunzip, ... | Operating system call (=1) |
| Java programs | :white_check_mark: | *.java |  |
| Jar programs | :white_check_mark: | *.jar |  |

### Supported program invocation mechanisms

Common classes:
``` java
java.lang.ProcessBuilder
java.lang.Runtime
```
IBM classes:
``` java
com.ibm.ctg.client.JavaGateway
com.ibm.connector2.cics.ECIInteraction
com.ibm.as400.access.CommandCall
com.ibm.as400.access.ProgramCall
```

#### java.lang.ProcessBuilder

In the example below the call to the shell script *myCommand.sh* is
represented by a *Java Call to General Program* object of the same name.

``` java
 ProcessBuilder pb = new ProcessBuilder("myCommand.sh", "myArg1", "myArg2");
 Process p = pb.start();
```

The *ProcessBuilder.command* method used to construct the command line
string is also supported.

#### java.lang.Runtime

In the example below, the results are similar to those in the example
above for 'java.lang.ProcessBuilder'.

``` java
Runtime runtime = Runtime.getRuntime();
Process p = runtime.exec("CMD /C myCommand.sh myArg1 myArg2");
```

#### com.ibm.as400.access.ProgramCall

In the example below the call to the the cobol program *QSYS* is
represented by a *Java Call to Midrange Program* object of the same name.

``` java
ProgramCall pgm = new ProgramCall(system);
String programName = "QSYS";
pgm.run(programName, paramList)
```

### Command options and arguments

The arguments passed to the ProcessBuilder class or the Runtime.exe
method might be parameterized. There are two different main cases that
are treated differently. In the first one, it is the executed program
name itself being parameterized, in which case the analyzer will create
different objects for each different program name. 

The second case is when the command line options and arguments are being
parameterized. In that case, a single *Java Call to Program* object is
created. However the receiving call links of this object will reflect
the the origin of the value passed to the parameterized argument. 

``` java
    public void executeCommand(String option) throws IOException {
        String[] command = {"CMD", "/C", "myprogram.sh", option};        
        Runtime runtime = Runtime.getRuntime();
        Process p = runtime.exec(command);
    }
    
    public void callA() throws IOException {
        String myvar = "option1";
        executeCommand(myvar);
    }
    
    public void callB() throws IOException {
        String myvar = "option2";
        executeCommand(myvar);
    }
```

![](../images/670138582.png)

The *Java Call to Program* object belongs to the method containing the
expression invoking the command (*ProcessBuilder.start(...)* or
*Runtime.exec(...)*), in this case the method *executeCommand*. The
callers to the *Java Call to Program* represent the methods from where
the parameters of the (usually parameterized) invocation have been
determined. In the figure above we can seen the two different call links
(C) from methods *callA* and *callB *that correspond to two different
values of the argument passed to the *myprogram.sh* script (*option1*
and *option2*). The list of all unique full command lines associated to
a given *Java Call to Program* object can be found in the "Full command
lines" property:

![](../images/670138581.png)

The correspondence between the calling object and a particular command
line can be only verified by manual inspection. Undefined (or hard to
determine) parameters are represented by question marks "?".

### Specifics in calls to Java programs

Two different ways of calling Java programs is supported. The first one,
directly invokes the main class:

``` java
ProcessBuilder pb = new ProcessBuilder("java", "mycode.Hello");     // This corresponds to the command: "java mycode.Hello"
Process p = pb.start();
```

which is represented by a *Java Call To Java Program* with the main
class name. The class fullname information (namely mycode.Hello) is
saved in the property "Fullname of the class, ...":

![](../images/670138580.png)

The second approach, is to invoke a JAR application:

``` java
ProcessBuilder pb = new ProcessBuilder("java", "-jar", "myProgram.jar");
Process p = pb.start();
```

Following this code a common *Java Call To Generic Program* object will
be created. In order to link this call to the main java class, the
analyzer will search for jar compilation instructions in maven files (we
only support for "maven-assembly-plugin" artifacts). In case it is
found, the analyzer will create a *Java JAR Program* together with the
callLink to the main method. The link between the call to the JAR and
the JAR program will be done during the application level analysis by
the web service linker extension. 

Below is an example of both ways of invoking the same Java program
*mycode.Hello:*

![](../images/670138579.png)

### Specifics in calls to Cobol programs

In communicating with Midrange application
``` java
public class IBMiExample {
    public static void main(String[] args) {
        AS400 system = null;
        system = new AS400("systemName", "userId", "password");
        system.connectService(AS400.COMMAND);

        // ProgramCall.run() with parameters
        ProgramCall pgm = new ProgramCall(system);
        String programName = "QSYS";
        pgm.run(programName, null)
    }
}
```
The code snippet above is expected to produce a new object called *Java Call to Midrange Program* which has *QSYS* and *JTOpen for IBM i* for name and vendor respectively. This object will also link to the mentioned cobol program if its source code presents in the analysis project. 

![](../images/midrange_object.png)

![](../images/midrange_object_properties.png)

## Limitations

-   Command lines using pipelines and more complex constructs are not
    fully supported yet (the quality of the results might vary).
-   The Java JAR Program and the link to the main method is created when
    the jar-defining pom.xml file and the main class file share the same
    analysis unit. This restriction will be relaxed in future releases.
-   The reconstruction of the invoked full command lines is a complex
    task which is under constant improvement. This means that when
    upgrading the version of the extension there might be notable
    changes in the number of commands retrieved.
-   The name of the program called by ProgramCall can sometimes not be found if 
    the ProgramCall instance is declacred outside of the current physical caller which execute the callback.
  
