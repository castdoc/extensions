---
title: "Java Function Point Configuration - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.config.tcc

## Description

This extension defines entry/end points to create Structural Transactions for CAST Imaging and compute Automated Function Points (ISO/IEC 19515).

## What's new?

This release focuses on improvements to end-point definitions:

- to increase the number of "complete" transactions identified during an analysis by adding new end-points definitions (an incomplete transaction does not reach an end-point (or a data entity) and as such has Function Point value of 0).
- to split the single `.tccsetup` file delivered in previous releases of this extension into multiple `.tccsetup` files - one file per specific end-point "role", for example in this release, the following roles each have their own dedicated file:
    - FTP
    - GraphQL
    - LDAP
    - Mail
- for definitions that have been moved to dedicated "role" `.tccsetup` files, the number of object types that are classed as end-points has been reduced to only Java Methods (Java Classes, Java Interfaces and Java Functions are no longer classed as end-points). This simplifies the transaction call graph by removing objects that are not actual end-points.

See [Release Notes](rn/).

### List of updated end-point definitions

| End-point definition | Existing package<br>(previous release) | New package |
|---|---|---|
| com.enterprisedt.net.ftp | Base_Java_ConfigTcc | Base_Java_Communication_FTP |
| org.apache.commons.net.ftp | Base_Java_ConfigTcc | Base_Java_Communication_FTP |
| com.sun.jndi.ldap | Base_Java_ConfigTcc | Base_Java_Communication_LDAP |
| com.sun.mail.smtp | Base_Java_ConfigTcc | Base_Java_Communication_Mail |
| javax.mail | Base_Java_ConfigTcc | Base_Java_Communication_Mail |
| org.apache.commons.mail | Base_Java_ConfigTcc | Base_Java_Communication_Mail |
| org.springframework.mail | Base_Java_ConfigTcc | Base_Java_Communication_Mail |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

| JEE Analyzer | Supported |
|---|:-:|
|  ≥ 1.3.18-funcrel | :white_check_mark: |

## Download and installation instructions

This extension will be downloaded and installed automatically when JEE Analyzer version ≥ 1.3.18-funcrel is installed.
