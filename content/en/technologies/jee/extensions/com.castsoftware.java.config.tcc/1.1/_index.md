---
title: "Java Function Point Configuration - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.config.tcc

## What's new?

Introducing a brand-new extension dedicated to entry/exit definitions. In order to get accurate transactions with specific definition of entry/exit points and to avoid duplication across extensions.

This simplifies some transactions' call graph, by removing:

*   Entry/end points property on objects
*   classes, methods, functions, interfaces objects that are not actual entry/end points
*   

See also [Release Notes](rn/).

### Example of simplification in a transaction

#### Entry point

On the right, the newly calculated transaction, with 2 nodes removed. The entry point is identified more precisely:

![deleted-entry-points-before-after](../images/deleted-entry-points-before-after.png)

#### End point

On the right, the newly calculated transaction, with 2 nodes removed, and 3 methods whose end points have been deleted. Only the actual end point remains:

![deleted-end-points-before-after](../images/deleted-end-points-before-after.png)

### Names of deleted packages and objects

#### Entry points
 
  | Deleted definition | Extension with new modelisation |
  |---|---|
  | AWT Screens (Definition based on Classes, interfaces and methods)                     | Java Presentation Frameworks  (UI and View objects)                                  |
  | javax.swing  (Definition based on Classes, interfaces and methods)                    | Java Presentation Frameworks  (UI and View objects)                                  |
  | com.sun.java.swing.plaf.windows (Definition based on Classes, interfaces and methods) | Java Presentation Frameworks  (UI and View objects)                                  |
  | com.google.gwt.user.client (Definition based on Classes, interfaces and methods)      | Google Web Toolkit Framework  (UI and Webservice Call via GWT resource services)     |
  | Servlet (Definition based on Classes, interfaces and methods)                         | Universal Linker : Exposed Web service via Servelet Opertaions Exposed Web service)  |
  | org.eclipse.birt.report (Definition based on Classes, interfaces and methods)         | Business Intelligence and Reporting Tools : Birt UI                                  |
  | freemarker (Definition based on Classes, interfaces and methods)                      | Apache Freemarker : Service Invocation via Java Freemarker FTL Call                  |
  | com.sun.jersey.api.client (Definition based on Classes, interfaces and methods)       | REST Service Calls for Java : Webservice Call via Java resource services             |
  | org.apache.camel                                                                      | Apache Camel  : Message Queue Publishers & Subscibers                                |

#### End points
  
  | Deleted definition | Extension with new modelisation |
  |---|---|
  | com.ibm.mq  (Definition based on Classes, interfaces and methods)                   | Message Queues Extension:   Message Queue Publishers & Subscibers                            |
  | com.ibm.msg (Definition based on Classes, interfaces and methods)                   | Message Queues Extension:Message Queue Publishers & Subscibers                               |
  | com.ibm.jms (Definition based on Classes, interfaces and methods)                   | Message Queues Extension :Message Queue Publishers & Subscibers                              |
  | javax.jms   (Definition based on Classes, interfaces and methods)                   | Message Queues Extension :Message Queue Publishers & Subscibers                              |
  | org.apache.activemq  (Definition based on Classes, interfaces and methods)          | Message Queues Extension :Message Queue Publishers & Subscibers                              |
  | org.springframework.jms (Definition based on Classes, interfaces and methods)       | Message Queues Extension :Message Queue Publishers & Subscibers                              |
  | org.springframework.web (Definition based on Classes, interfaces and methods)       | Spring MVC Framework: Exposed Web service via Spring MVC Opertaions                          |
  | org.apache.camel    (Definition based on Classes, interfaces and methods)           | Apache Camel : Message Queue Publishers & Subscibers                                         |
  | org.apache.commons.httpclient (Definition based on Classes, interfaces and methods) | REST Service Calls for Java: Webservice Calls via Java Services                              |
  | java.sql         (Definition based on Classes, interfaces and methods)              | JDBC : SQL queries                                                                           |
  | org.hibernate   (Definition based on Classes, interfaces and methods)               | Java Persistence Frameworks : SQL queries and Entity operations calling DB tables/Procedures |
  | org.springframework.jdbc   (Definition based on Classes, interfaces and methods)    | Spring Data/JDBC Frameworks: SQL queries and Entity operations calling DB tables/Procedures  |
  | Query    (Definition based on Classes, interfaces and methods)                      | Java Persistence Frameworks SQL queries and Entity operations calling DB tables/Procedures   |
  | com.informix.jdbc      (Definition based on Classes, interfaces and methods)        | JDBC: SQL queries and Entity operations calling DB tables/Procedures                         |
  | net.sourceforge.jtds.jdbc  (Definition based on Classes, interfaces and methods)    | JDBC: SQL queries and Entity operations calling DB tables/Procedures                         |
  | javax.xml.rpc  (Definition based on Classes, interfaces and methods)                | JAX-WS : Exposed Web service via JAX-WS Opertaions                                           |
  | org.springframework.ws   (Definition based on Classes, interfaces and methods)      | JAX-WS: Exposed Web service via JAX-WS Opertaions                                            |

## Description

This extension defines entry/end points to compute Automated Function Points (ISO/IEC 19515). This is also used to create CAST Imaging Blueprint.

It will be installed automatically as a dependency of JEE Analyzer version ≥ 1.3.18-funcrel.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

| JEE Analyzer | Supported |
|---|:-:|
|  ≥ 1.3.18-funcrel | :white_check_mark: |

## Download and installation instructions

This extension will be downloaded and installed automatically when JEE Analyzer version ≥ 1.3.18-funcrel is installed.
