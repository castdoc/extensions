---
title: "Java Function Point Configuration - 1.3"
linkTitle: "1.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.config.tcc

## Description

This extension defines entry/end points to create Structural Transactions for CAST Imaging and compute Automated Function Points (ISO/IEC 19515).

## What's new?

This release focuses on improvements to end-point definitions and the on-going process of grouping definitions by "role":

- addition of new end-point definitions to increase the number of "complete" transactions identified during an analysis (an incomplete transaction does not reach an end-point (or a data entity) and as such has Function Point value of 0)
- creation of the following "roles" and assigning existing end-point definitions to these new "roles" (see the table below):
    - RPC
    - SSH
    - I/O

See also [Release Notes](rn/).

### List of updated end-point definitions

{{% alert color="info" %}}As part of the process of moving end-point definitions, only Java Method object types are now defined as end-points.{{% /alert %}}

| End-point definition | Existing package<br>(previous release) | New package |
|---|---|--|
| com.google.common.io | Base_Java_ConfigTcc | Base_Java_Communication_io |
| com.sshtools.j2ssh | Base_Java_ConfigTcc | Base_Java_communication_SSH |
| javax.xml.rpc | Base_Java_ConfigTcc | Base_Java_Communication_RPC |
| org.apache.commons.io | Base_Java_ConfigTcc | Base_Java_Communication_io |
| org.dom4j.io | Base_Java_ConfigTcc | Base_Java_Communication_io |
| org.drools.io | Base_Java_ConfigTcc | Base_Java_Communication_io |
| org.opensaml.xml.io | Base_Java_ConfigTcc | Base_Java_Communication_io |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

| JEE Analyzer | Supported |
|---|:-:|
|  ≥ 1.3.18-funcrel | :white_check_mark: |

## Download and installation instructions

This extension will be downloaded and installed automatically when JEE Analyzer version ≥ 1.3.18-funcrel is installed.
