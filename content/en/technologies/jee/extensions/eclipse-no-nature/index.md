---
title: "Eclipse no nature Project Discoverer"
linkTitle: "Eclipse no nature Project Discoverer"
type: "docs"
---

The Eclipse no nature Project Discoverer is not currently provided as an
extension: instead it is embedded in CAST Imaging Core and is therefore
present "out of the box".

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Supported project type</th>
<th class="confluenceTh">CAST Delivery Manager Tool interface</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Eclipse no nature project</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Configures a project for each Eclipse Java project (.project without
a Java nature) identified:<em><br />
</em></p>
<p><em>Click to enlarge</em></p>
<p><em><img src="images/248833757.jpg" /><br />
</em></p>
</div></td>
</tr>
</tbody>
</table>