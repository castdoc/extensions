---
title: "DMT extractor for Maven local repository"
linkTitle: "DMT extractor for Maven local repository"
type: "docs"
---

>The DMT extractor for Maven local repository is not currently
provided as an extension: instead it is embedded in CAST Imaging Core and is
therefore present "out of the box".

## What is it?

The DMT extractor for Maven local repository provides the means to
extract (using the CAST Delivery Manager Tool) JAR based source code
from a local Maven repository. In other words, JAR based source code
that resides in a simple local or network folder. This extractor should
be used when you want to extract JAR based source code that is stored in
a local Maven repository. For example, when your JEE application
relies on JAR files and the initial extraction of this application in
the CAST Delivery Manager Tool throws "missing library file" alerts,
you can use this extractor to extract the missing files from a local
Maven repository and resolve the alerts.

## Accessing the extractor

The extractor can be accessed as follows:

![](images/270731509.jpg)

![](images/270731508.jpg)

## Extractor interface

![](images/270731507.jpg)

<table class="confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: center;"><p>Option</p></th>
<th class="confluenceTh"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;">1</td>
<td class="confluenceTd">Enter the path to your local/network location
where the Maven repository resides.<strong><br />
</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;">2</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>This option enables you to specify specific artifacts that you know
need to be extracted in order to resolve a packaging alert. You can
specify the artifact using:</p>
<ul>
<li><strong>Group ID</strong> (usually a reverse domain name like
com.example.foo) - this option is mandatory</li>
<li><strong>Artifact ID</strong> (usually the name) - this option is
mandatory</li>
<li><strong>Classifier</strong> (an arbitrary string that distinguishes
artifacts that were built from the same POM but differ in their content)
- this option is mandatory</li>
<li><strong>Version</strong> (the artifact's version string) - this
option is mandatory</li>
</ul>
<div>
<div>
<p><strong>Notes</strong></p>
<ul>
<li>If you do not specify an element, the CAST Delivery Manager Tool
will automatically populate the list of elements when you run the
Package action based on what it finds in the repository path you have
entered.</li>
<li><strong>&lt;groupID&gt;</strong> or
<strong>&lt;artifactID&gt;</strong> configured in
<strong>&lt;relocation&gt;</strong> tags are supported.</li>
<li><strong>maven-metadata.xml</strong> will be used to determine a best
version for the &lt;versionID&gt; if no pom.xml can be found.</li>
</ul>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>
