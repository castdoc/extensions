---
title: "Apache Struts - 1.5"
linkTitle: "1.5"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.struts

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Apache Struts - this support is
in addition to the basic support provided for Apache Struts in
the [JEE Analyzer](../../com.castsoftware.jee/). This extension's main role is to
improve the detection of links and transaction computations where Apache
Struts is implicated.

Note that:

-   At the current time, analysis results will contain duplicated
    objects: the "Struts Operation object"
    (created by the extension) corresponds to the Struts Action
    Mapping Operation object created by the [JEE Analyzer](../../com.castsoftware.jee/) - this duplication will be fixed in a future
    release.
-   All Transaction results involving Apache Struts code are generated
    using the "Struts Operation object"
    (created by the extension) and not the Struts Action Mapping
    Operation object created by the [JEE Analyzer](../../com.castsoftware.jee/).

## In what situation should you install this extension?

If your JEE application source code uses the Apache Struts framework
you should install this extension to benefit from improved support for
Apache Struts.

## Supported Apache Struts versions

| Apache Struts | Supported |
|---------------|:---------:|
| 1.x           | :white_check_mark: |
| 2.x           | :white_check_mark: |

## Supported client frameworks

| Client framework | Supported |
|------------------|:---------:|
| jQuery           | :white_check_mark: |
| AngularJS        | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Apache Struts extension requires
that the following other CAST extensions are also installed:

-   [JEE Analyzer](../../com.castsoftware.jee/)
-   [HTML5/JavaScript extension](../../../../web/html5-js)
-   [Universal Linker](../../../../multi/com.castsoftware.wbslinker/)
-   com.castsoftware.internal.platform (internal extension)

Note that:
-   any dependent extensions are automatically downloaded and installed for you. You do not need to do anything.

>For support of `SelectAction`, use HTML5/JavaScript ≥ 2.0.7 and Web Services Linker ≥ 1.6.7.

### Transaction setup

An Apache Struts specific setup is automatically imported when the extension
is installed:

![](../images/670138611.jpg)

### What results can you expect?

#### Objects

| Icon | Description      |
|------|------------------|
| ![](../images/670138605.png) | Struts Operation |

## Features

### Object structure and links

-   Creates Struts Operations which represent entry-points for web
    services. For each operation, one Struts Operation object is
    created.
-   The dependent [Universal Linker](../../../../multi/com.castsoftware.wbslinker/) is responsible
    for detecting and creating links between the UI (JSP/HTML etc) to
    the "Struts Operation object" created by
    the extension.

### Struts 1.1 support

#### Application configuration

In the "web.xml" file, the extension will detect the servlet pointing to
"org.apache.struts.action.ActionServlet" class, indicating that Struts
1.1 is being used. The extension will detect the servlet-mapping (i.e.
"\*.perform" in the example below):

``` xml
<web-app id="WebApp">
...
    <servlet>
        <servlet-name>action</servlet-name>
        <display-name>Struts Action</display-name>
        <servlet-class>org.apache.struts.action.ActionServlet</servlet-class>
....
    </servlet>
...
    <servlet-mapping>
        <servlet-name>action</servlet-name>
        <url-pattern>*.perform</url-pattern>
    </servlet-mapping>
....
</web-app>
```

In the struts config xml files (struts-XXX.xml), the extension will
detect the correspondence between an URL and a java class
(i.e. /logout corresponds to the
"com.company.lightspeed.struts.logon.StrutsActionLogout" java
class): 

``` xml
<struts-config>
...
  <action-mappings>
    <action
        path="/logout"
        type="com.company.lightspeed.struts.logon.StrutsActionLogout">
        <forward name="loggedOut" path="lightspeed/LoggedOut.jsp" />
    </action>
....
  </action-mappings>
</struts-config>
```

The extension will search for the "execute" method in the
corresponding java class (here StrutsActionLogout class) and create
a StrutsOperation named with the corresponding URL (here /logout). A
call link between the StrutsOperation and the "execute" method will
be created.

Note that the "execute" method may be defined in a parent class
of StrutsActionLogout. This specific case which is common to
both struts 1 and struts 2 is described in section "Modelling when the
execute method is defined in a parent Struts
class".

#### Classes inheriting from org.apache.struts.actions.DispatchAction

The extension will search for all java classes implementing
"org.apache.struts.actions.DispatchAction".

For example:

``` java
import org.apache.struts.actions.DispatchAction;

    public class BookEditAction extends DispatchAction {


    public ActionForward editBook(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
            BookEditForm bookEditForm = (BookEditForm) form;
            
            /* lalinuna.de 04.11.2004
             * get id of the book from request
             */
            Integer id = Integer.valueOf(request.getParameter("id"));
                    // [laliluna] 28.11.2004  get business logic
            LibraryManager libraryManager = new LibraryManager();
            bookEditForm.setBook(libraryManager.getBookByPrimaryKey(id));
            return mapping.findForward("showEdit");
    }

...
}
```

We create a Struts Operation with the name
"/BookEdit?do=editBook" (this name is found in the struts web xml
file or, by default, is the class name without the Action part), and a
call link to the "editBook" method. The "do" after the "?" is found
in the struts web xml file in the "parameter" field of the "action" tag.
The same is done for every method of the class.

Example of a struts-config.xml:

``` xml
<!DOCTYPE struts-config PUBLIC "-//Apache Software Foundation//DTD Struts Configuration 1.2//EN" "http://struts.apache.org/dtds/struts-config_1_2.dtd">
<struts-config>
   <action-mappings >
      <action
         attribute="bookEditForm"
         input="/jsp/bookEdit.jsp"
         name="bookEditForm"
         parameter="do"
         path="/bookEdit"
         scope="request"
         type="com.library.struts.action.BookEditAction">
         <forward name="showBorrow" path="/jsp/borrowBook.jsp" />
         <forward name="showEdit" path="/jsp/bookEdit.jsp" />
         <forward
            name="showList"
            path="/bookList.do"
            redirect="true" />
         <forward name="showAdd" path="/jsp/bookAdd.jsp" />
      </action>

   </action-mappings>
</struts-config>}
```

Results:

![](../images/670138604.jpg)

#### Classes inheriting from net.jspcontrols.dialogs.actions.SelectAction

The extension will search for all java classes implementing
"net.jspcontrols.dialogs.actions.SelectAction".

For example:

``` java
public class MaintainMyAccountAction extends SelectAction{
    
    
     protected Map getKeyMethodMap() {
        Map map = new HashMap();
        map.put(getInitKey() + "-GET", "getfoo");
        map.put(getInitKey() + "-SAVE", "save");
        map.put(getInitKey() + "-CANCEL", "cancel");
        return map;
    }
    
    public ActionForward getfoo(ActionMapping mapping, 
        ActionForm form,
        HttpServletRequest request, 
        HttpServletResponse response)
        throws IOException, ServletException {
        ActionForward forward = null;
        UserForm userForm = (UserForm)form;

        UserVO userVO = (UserVO) UserSecurityManager.getUserContainer(request);
        if(userVO != null) CopyUtils.copyProperties(userForm,userVO);
        
        forward = mapping.findForward(Constant.SUCCESS);
        return(forward);
     }


...
}
```

In a SelectAction class, the method getKeyMethodMap provides a mapping
between the methods and a parameter name. In the previous example, the
method "getfoo" is mapped with a
parameter DIALOG-EVENT-GET (DIALOG-EVENT is the output of
getInitKey()).

We create a Struts Operation with the name
"/MaintainMyAccount?DIALOG-EVENT-GET" (this name is found in the
struts web xml file or, when no reference to this class is found in the
web xml file, this name is set to the class name without the Action
part), and a call link to the "getfoo" method. 

#### Struts 1.x + Spring

##### DelegatingActionProxy

Spring provides a special action
class org.springframework.web.struts.DelegatingActionProxy that acts
as a proxy for struts actions, allowing more indirections:

``` xml
<struts-config>
...
  <action-mappings>
    <action path="/user" type="org.springframework.web.struts.DelegatingActionProxy">
....
  </action-mappings>
</struts-config>
```

The class name is then found in a bean:

``` xml
<bean name="/user" scope="prototype" autowire="byName" class="org.example.web.UserAction"/>
```

or in a bean alias.

``` xml
<bean name="/someBean" scope="prototype" autowire="byName" class="org.example.web.UserAction"/>
<alias alias="/user" name="/someBean"/>
```

### Struts 2.x support

#### Application configuration

In the "web.xml" file, the extension will detect the filter pointing to
"org.apache.struts2.dispatcher.FilterDispatcher" class, indicating that
Struts 2.x is being used. The extension will detect the servlet-mapping
(i.e. "\*.action;/\*" in the example below). The extension will also
obtain the filter "struts2" and "BaseStrutsFilter"
because "BaseStrutsFilter" points to a class that exists in the
analysis.

``` xml
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://java.sun.com/xml/ns/javaee" xmlns:jsp="http://java.sun.com/xml/ns/javaee/jsp" xmlns:web="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd" id="WebApp_ID" version="2.5">
...
    <filter>
        <filter-name>struts2</filter-name>
        <filter-class>org.apache.struts2.dispatcher.FilterDispatcher</filter-class>
        <init-param>
            <param-name>actionPackages</param-name>
            <param-value>com.mapa.sipe.apresentacao.action, br.gov.mapa.arquitetura.apresentacao.struts</param-value>
        </init-param>
    </filter> 

    <filter>
        <display-name>BaseStrutsFilter</display-name>
        <filter-name>BaseStrutsFilter</filter-name>
        <filter-class>com.mapa.sipe.apresentacao.filter.BaseStrutsSipeFilter</filter-class>
        <init-param>
            <param-name>public-url</param-name>
            <param-value>/sipe/imprimirGruPublica.action</param-value>
        </init-param>
    </filter>
    
    <filter-mapping>
        <filter-name>BaseStrutsFilter</filter-name>
        <url-pattern>*.action</url-pattern>
        <dispatcher>REQUEST</dispatcher>
        <dispatcher>FORWARD</dispatcher>
    </filter-mapping>

    <filter-mapping>
        <filter-name>struts2</filter-name>
        <url-pattern>/*</url-pattern>
        <dispatcher>REQUEST</dispatcher>
        <dispatcher>ERROR</dispatcher> 
    </filter-mapping>
....
</web-app>
```

If a struts config file exists as shown below, the extension can obtain
a correspondence between the path and a java class: in the example
below, name="Login" gives the name of the operation that will be
created:

``` xml
<struts>
...
    <package name="chapterSixPublic" namespace="/chapterSix" extends="struts-default">

        <action name="Login" class="manning.chapterSix.Login">
                    <result type="redirect">/chapterSix/secure/AdminPortfolio.action</result>
                    <result name="input">/chapterSix/Login.jsp</result>
        </action>

....
    </package>

</struts>
```

The extension will search for the "execute" method in the
corresponding java class (here manning.chapterSix.Login class) and
create a StrutsOperation named with the corresponding URL
(here /Login). A call link between the StrutsOperation and
the "execute" method will be created.

The "execute" method may be defined in a parent class
of StrutsActionLogout. This specific case which is common to
both struts 1 and struts 2 is described in section "Modelling when the
execute method is defined in a parent Struts
class".

#### Classes with @Results annotation

The extension will search for all java classes using the "@Results"
annotation which is resolved to "org.apache.struts2.config.Results".

For example:

``` java
@Results({@Result(name = "popup", value = "popUpRestringe", type = TilesResult.class),
        @Result(name = "popupRegistro", value = "popUpRestringeRegistro", type = TilesResult.class),
        @Result(name = "base", value = "base", type = ActionChainResult.class, params = {"method", "execute"})})
public class RestringeAction extends SipeBaseAction {

   public String carregarRegistro() throws Exception {
        VwPessoaVinculadaEstab vw = montarVwPessoaVinculadaEstab();
        if (vw != null) {
            listaDadosRegistro =
                getSipeComumBD().consultarRegistroCadastro(vw.getIdPessoaEstabelecimento(), getJAASPrincipal().getIdPessoaFisica(), getIdAreaSelecionada(),
                    getPreCadastroAtivo(), getStPerfil());
            for (RegistroEstabCadDTO dados : listaDadosRegistro) {
                if (dados.getCdRegistroCadastro().equals(getRegistroSelecionado())) {
                    vw.setDadosRegistro(dados);
                    recuperarInformacoesUsuarioLogado(vw);
                    break;
                }
            }
            adicionarEstabelecimentoSessao(vw);
        }
        return FW_BASE;
    }

}
```

The extension will then search for all methods which are public and have
"String" as the return type. The extension will create a Struts
Operation with the name "/Restringe!carregarRegistro" (Restringe
is the class name without the ending "Action"), and a call link to the
method.

For example:

![](../images/670138603.jpg)

#### Classes implementing SessionAware

The extension will search for all java classes implementing
"org.apache.struts2.interceptor.SessionAware".

For example:

``` java
import org.apache.struts2.interceptor.SessionAware;

public class Login extends ActionSupport implements SessionAware {
    

    public String execute(){
        
        User user = getPortfolioService().authenticateUser( getUsername(), getPassword() ); 
        if ( user == null )
        {
            /* User not valid, return to input page. */
            return INPUT;
        }
        else{
            session.put( Struts2PortfolioConstants.USER, user );
        }
        
        return SUCCESS;
    }

}
```

The extension will create a Struts Operation with the name
"/Login" (this name is found in a struts XML config file or the
class name by default), and a call link to the "execute" method.

For example:

![](../images/670138602.jpg)

#### Classes inheriting from com.opensymphony.xwork2.ActionSupport

The extension will search for all java classes implementing
"com.opensymphony.xwork2.ActionSupport".

For example:

``` java
import com.opensymphony.xwork2.ActionSupport;

public class ManageFieldsPVAction extends ActionSupport{
    private static final long serialVersionUID = -8126525052448102790L;
    
    private String first_name;
    private String last_name;
    
    public String execute() {
        //if (first_name==null)
        //  return "login";
        //setFirst_name(first_name+=" with added thingies from your favorite Action!!!");
        return "success";
        
    }
...
}
```

The extension will create a Struts Operation with the name
"/ManageFieldsPV" (this name is found in the class name without the
Action part), and a call link to the "execute" method.

#### Class and methods using @Action annotation

[Annotations](https://struts.apache.org/getting-started/annotations.html)
of the "struts2 convention plugin" can be used instead of struts.xml
files.

For exemple :

``` java
import org.apache.struts2.convention.annotation.Action;

@Action(value = "login", results = {
        @Result(name = "SUCCESS", location = "/welcome.jsp"),
        @Result(name = "ERROR", location = "/error.jsp") })
public class LoginAction {

    public String execute() throws Exception {
        if("pankaj".equals(getName()) && "admin".equals(getPwd()))
        return "SUCCESS";
        else return "ERROR";
    }
    
    @Action(value = "foopath", results = {
            @Result(name = "SUCCESS", location = "/has_been_in_method.jsp"),
            @Result(name = "ERROR", location = "/error.jsp") })
    public String a_method() throws Exception {
        this.name = "single_annotation";
        return "SUCCESS";
    }
}
```

The extension will create two Struts Operations :

-   one named "/login" (which corresponds to the string assigned to
    value). A callLink from that operation to the execute method
    is created.
-   one named "/foopath". A callLink from that operation to the
    a_method method is created.

![](../images/670138601.png)

#### Struts 2.x + Spring

##### StrutsSpringObjectFactor

Spring offers additional indirection for Struts 2
using org.apache.struts2.spring.StrutsSpringObjectFactory.

``` xml
<struts>
...
    <constant name="struts.objectFactory" value="org.apache.struts2.spring.StrutsSpringObjectFactory" />
...
    <package name="chapterSixPublic" namespace="/chapterSix" extends="struts-default">

        <action name="Login" class="myBean">

....
    </package>

</struts>
```

Again the real class will be indicated in a bean.

``` xml
<bean name="myBean" class="manning.chapterSix.Login"/>
```

### Modeling when the execute method is defined in a parent Struts class

In both struts 1 and struts 2 it is common that the "execute" method
is not defined within the referred java class but in a parent of that
class. This "execute" method then calls a method that is overridden
in the referred java class. In that case, a call link is created between
the Struts Operation and this method.

Let's for instance consider the struts 1 application using the following
config xml file ( which defines that a /logout URL refers to the
"com.company.lightspeed.struts.logon.StrutsActionLogout" java
class): 

``` xml
<struts-config>
...
  <action-mappings>
    <action
        path="/logout"
        type="com.company.lightspeed.struts.logon.StrutsActionLogout">
        <forward name="loggedOut" path="lightspeed/LoggedOut.jsp" />
    </action>
....
  </action-mappings>
</struts-config>
```

If no "execute" method is found in
"com.company.lightspeed.struts.logon.StrutsActionLogout" java class,
the extension searches for the "execute" method in the class
hierarchy.

For example:

![](../images/670138600.jpg)

In this example, the "execute" method is found in the parent java
class ActionBaseCommon. The extension will analyze
this "execute" method to see if a method of the "referred" java
class (here StrutsActionLogout) is called from "execute" (here
the "execute" method calls "executeExtended" which is in the
"referred" java class StrutsActionLogout). The extension will create
a Struts Operation named "/logout" (name found in the Struts xml
file) and a call link between the operation and the
"executeExtended" method.

#### Current known limitations

-   The Class name to URL naming convention of the convention plugin is
    not supported
-   When Dynamic Method Invocation is used, the method foo of
    action hello can be accessed with both the URLs: hello!foo.action
    and hello.action?method: foo.  As the recommended approach is to use
    the first URL and in order to limit the number of objects created,
    the analyzer supports only the first approach.

## Structural Rules

The following structural rules are provided:

| Release | Link  |
|---------|-------|
| 1.5.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.7-funcrel) |
| 1.5.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.6-funcrel) |
| 1.5.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.5-funcrel) |
| 1.5.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.4-funcrel) |
| 1.5.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.3-funcrel) |
| 1.5.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.2-funcrel) |
| 1.5.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.1-funcrel) |
| 1.5.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_struts&ref=\|\|1.5.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_struts&ref=%7C%7C1.5.0-funcrel) |
