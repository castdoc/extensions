---
title: "JEE Maven Build Extractor - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtmavenbuildextractor

## What's new ?

See [Release Notes](rn/).

## Extension description

This extension detects projects based on the \*.jar and
\*.war files described in a Maven .pom file. You should install
this extension when you want to detect all the dependencies (.jar and
\*.war) referenced in the \*.pom file. The extension will create
projects for each of these \*.jar / \*.war files based on their
respective pom file.

With this extension a option is added under Files on your file
system option called Compiled files with Maven Source code.

## Technical information

A project is created for each \*.jar and \*.war file based on the
following criteria:

-   the \*.jar and \*.war file provided with the source
-   the \*.pom file for the respective \*.jar / \*.war describing the
    groupId, artifactId and version.

For example folder structure shown below, the following details will be
discovered:

Folder structure:

![](../images/293208088.png)  

Package content:

![](../images/293208090.png)  


The \<projectName\> is the name of the \*.jar / \*.war file without
version and path is the relative path of the \*.jar / .war. Analysis
Units will be created as per the projects discovered:


![](../images/293208091.png)  

## Supported Maven releases

| Maven release | Supported |
|---|:-:|
| 3.x.x | :white_check_mark: |
| 2.x.x | :white_check_mark: |
| 1.x.x | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Extension interface

The following screen shot shows the differences in the product when the
extension is installed:

![](../images/250382260.png)
