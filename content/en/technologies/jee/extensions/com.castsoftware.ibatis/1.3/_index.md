---
title: "MyBatis - 1.3"
linkTitle: "1.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.ibatis

## What's new ?

See [Release Notes](rn/).

## Description

This extension provides support for the MyBatis/iBatis persistence framework on Java and .NET.     

## In what situation should you install this extension?

This extension should be installed when analyzing a Java/.NET project that uses a MyBatis framework, and you want to
view a transaction consisting of MyBatis objects with their corresponding links. Links to corresponding database tables can also be resolved, provided that the SQL database has been extracted and DDL has been created.

## Technology support

The following libraries are supported by this extension:

| Language | Library name | Namespace | Version | Supported |
| -------- | -------- | ------------------------------------------------------------------------- | :--------: | :----------------: |
| Java     | [iBatis](https://ibatis.apache.org/) |com.ibatis.sqlmap.client                                 | 1.0.0 to 2.5.0| :white_check_mark: |
| Java     | [MyBatis](https://mybatis.org/mybatis-3/)| org.apache.ibatis.session                      | 3.0.0 to 3.5.16 | :white_check_mark: |
| Java     |[MyBatis Spring](https://mybatis.org/spring/)| org.mybatis.spring    | 1.0.0 to 3.0.3 | :white_check_mark: |
|Java | [Spring Framework: iBATIS](https://docs.spring.io/spring-framework/docs/3.2.x/javadoc-api/org/springframework/orm/ibatis/package-summary.html) | org.springframework.orm.ibatis | 1.0.0 to 3.2.12 | :white_check_mark: |
| Java     | [tk.mybatis](https://mybatis.io/)|tk.mybatis.mapper.common                                        | 3.1.0 to 4.1.5 | :white_check_mark: |
| .NET C#|[iBATIS.NET - DataMapper Application Framework](https://ibatis.apache.org/docs/dotnet/datamapper/)     | IBatisNet.DataMapper| 1.0.0 upto 2.1.0 | :white_check_mark: |
| .NET C#    |[SqlBatis DataMapper](https://www.nuget.org/packages/SqlBatis.DataMapper) | SqlBatis.DataMapper | 3.0.0 to 5.1.0 | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For applications using any of the above mentionned libraries, this extension will be automatically installed by CAST Imaging Console.
For upgrade, if the Extension Strategy is not set to Auto update, you can manually upgrade the extension using the Application - Extensions interface.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the below objects and links created.

### Objects

| Icon                                            | Description              |                                           Comment                                            |
| ----------------------------------------------- | ------------------------ | :------------------------------------------------------------------------------------------: |
| ![sql_query_icon](../images/sql_query_icon.png) | Java MyBatis SQL Query   | An object is created for each native SQL query found in java project and resolved in a  method call/xml file |
| ![entity_icon](../images/entity_icon.png) | Java MyBatis Entity   | An object is created for each tk.MyBatis entity |
| ![entity_operation](../images/entity_operation.png) | Java MyBatis Entity Operation   | An object is created for each tk.MyBatis Mapper operation|
| ![unknown_sql_query_icon](../images/unknown_sql_query_icon.png) | Java MyBatis Unknown SQL Query   | An object is created for each native SQL query found in java project and the exact query cannot be resolved |
| ![unknown_entity_icon](../images/Unknown_Entity.png) | Java MyBatis Unknown Entity   | An object is created for when tk.MyBatis entity cannot be resolved |
| ![unknown_entity_operation](../images/UnknownEntity_Operation.png) | Java MyBatis Unknown Entity Operation   | An object is created for each tk.MyBatis Mapper operation and respective Entity cannot be resolved|
| ![sql_query_icon](../images/sql_query_icon.png) | DotNet MyBatis SQL Query | An object is created for each native SQL query found in .Net project and resolved in a  method call/xml file |
| ![unknown_sql_query_icon](../images/unknown_sql_query_icon.png) | DotNet MyBatis Unknown SQL Query | An object is created for each native SQL query found in .NET project and the exact query cannot be resolved |

### Links
| Link Type | Caller | Callee | APIs Supported |
| --- | --- | --- | --- |
| callLink | Java Method | Java MyBatis SQL Query | <details><summary>SqlMapClient APIs</summary>com.ibatis.sqlmap.client.SqlMapExecutor.update<br>com.ibatis.sqlmap.client.SqlMapExecutor.delete<br>com.ibatis.sqlmap.client.SqlMapExecutor.insert<br>com.ibatis.sqlmap.client.SqlMapExecutor.queryForObject<br>com.ibatis.sqlmap.client.SqlMapExecutor.queryForList<br>com.ibatis.sqlmap.client.SqlMapExecutor.queryWithRowHandler<br>com.ibatis.sqlmap.client.SqlMapExecutor.queryForMap<br>com.ibatis.sqlmap.client.SqlMapExecutor.queryForPaginatedList<br>com.ibatis.sqlmap.client.SqlMapClient.update<br>com.ibatis.sqlmap.client.SqlMapClient.delete<br>com.ibatis.sqlmap.client.SqlMapClient.insert<br>com.ibatis.sqlmap.client.SqlMapClient.queryForObject<br>com.ibatis.sqlmap.client.SqlMapClient.queryForList<br>com.ibatis.sqlmap.client.SqlMapClient.queryWithRowHandler<br>com.ibatis.sqlmap.client.SqlMapClient.queryForMap<br>com.ibatis.sqlmap.client.SqlMapClient.queryForPaginatedList</details> <details><summary>SqlSession APIs</summary>org.apache.ibatis.session.SqlSession.selectOne<br>org.apache.ibatis.session.SqlSession.selectList<br>org.apache.ibatis.session.SqlSession.selectCursor<br>org.apache.ibatis.session.SqlSession.selectMap<br>org.apache.ibatis.session.SqlSession.select<br>org.apache.ibatis.session.SqlSession.insert<br>org.apache.ibatis.session.SqlSession.update<br>org.apache.ibatis.session.SqlSession.delete</details> <details><summary>Annotation APIs</summary>org.apache.ibatis.annotations.Insert<br>org.apache.ibatis.annotations.Update<br>org.apache.ibatis.annotations.Delete<br>org.apache.ibatis.annotations.Select<br>org.apache.ibatis.annotations.InsertProvider<br>org.apache.ibatis.annotations.UpdateProvider<br>org.apache.ibatis.annotations.DeleteProvider<br>org.apache.ibatis.annotations.SelectProvider</details><details><summary>SqlSessionTemplate APIs</summary>org.mybatis.spring.SqlSessionTemplate.selectOne<br>org.mybatis.spring.SqlSessionTemplate.selectList<br>org.mybatis.spring.SqlSessionTemplate.selectCursor<br>org.mybatis.spring.SqlSessionTemplate.selectMap<br>org.mybatis.spring.SqlSessionTemplate.select<br>org.mybatis.spring.SqlSessionTemplate.insert<br>org.mybatis.spring.SqlSessionTemplate.update<br>org.mybatis.spring.SqlSessionTemplate.delete</details><details><summary>SpringFramework APIs</summary>org.springframework.orm.ibatis.SqlMapClientTemplate.update<br>org.springframework.orm.ibatis.SqlMapClientTemplate.delete<br>org.springframework.orm.ibatis.SqlMapClientTemplate.insert<br>org.springframework.orm.ibatis.SqlMapClientTemplate.queryForObject<br>org.springframework.orm.ibatis.SqlMapClientTemplate.queryForList<br>org.springframework.orm.ibatis.SqlMapClientTemplate.queryWithRowHandler<br>org.springframework.orm.ibatis.SqlMapClientTemplate.queryForMap<br>org.springframework.orm.ibatis.SqlMapClientTemplate.queryForPaginatedList<br>org.springframework.orm.ibatis.SqlMapClientTemplate.execute<br>org.springframework.orm.ibatis.SqlMapClientTemplate.executeWithListResult<br>org.springframework.orm.ibatis.SqlMapClientTemplate.executeWithMapResult<br>org.springframework.orm.ibatis.SqlMapClientOperations.update<br>org.springframework.orm.ibatis.SqlMapClientOperations.delete<br>org.springframework.orm.ibatis.SqlMapClientOperations.insert<br>org.springframework.orm.ibatis.SqlMapClientOperations.queryForObject<br>org.springframework.orm.ibatis.SqlMapClientOperations.queryForList<br>org.springframework.orm.ibatis.SqlMapClientOperations.queryWithRowHandler<br>org.springframework.orm.ibatis.SqlMapClientOperations.queryForMap<br>org.springframework.orm.ibatis.SqlMapClientOperations.queryForPaginatedList<br>org.springframework.orm.ibatis.SqlMapTemplate.execute<br>org.springframework.orm.ibatis.SqlMapTemplate.executeQueryForList<br>org.springframework.orm.ibatis.SqlMapTemplate.executeQueryForMap<br>org.springframework.orm.ibatis.SqlMapTemplate.executeQueryForObject<br>org.springframework.orm.ibatis.SqlMapTemplate.executeUpdate<br>org.springframework.orm.ibatis.SqlMapTemplate.executeWithListResult<br>org.springframework.orm.ibatis.SqlMapTemplate.executeWithMapResult<br>org.springframework.orm.ibatis.SqlMapOperations.executeQueryForList<br>org.springframework.orm.ibatis.SqlMapOperations.executeQueryForMap<br>org.springframework.orm.ibatis.SqlMapOperations.executeQueryForObject<br>org.springframework.orm.ibatis.SqlMapOperations.executeQueryWithRowHandler<br>org.springframework.orm.ibatis.SqlMapOperations.executeUpdate</details>|
| callLink | Java Method | Java MyBatis Entity Operation | <details><summary>tk.mybatis Mapper APIs</summary>tk.mybatis.mapper.common.condition.UpdateByConditionMapper.updateByCondition<br>tk.mybatis.mapper.common.condition.UpdateByConditionSelectiveMapper.updateByConditionSelective<br>tk.mybatis.mapper.common.base.delete.DeleteByPrimaryKeyMapper.deleteByPrimaryKey<br>tk.mybatis.mapper.common.base.delete.DeleteMapper.delete<br>tk.mybatis.mapper.common.base.insert.InsertMapper.insert<br>tk.mybatis.mapper.common.base.insert.InsertSelectiveMapper.insertSelective<br>tk.mybatis.mapper.common.base.select.ExistsWithPrimaryKeyMapper.existsWithPrimaryKey<br>tk.mybatis.mapper.common.base.select.SelectAllMapper.selectAll<br>tk.mybatis.mapper.common.base.select.SelectByPrimaryKeyMapper.selectByPrimaryKey<br>tk.mybatis.mapper.common.base.select.SelectCountMapper.selectCount<br>tk.mybatis.mapper.common.base.select.SelectMapper.select<br>tk.mybatis.mapper.common.base.select.SelectOneMapper.selectOne<brtk.mybatis.mapper.common.base.update.UpdateByPrimaryKeyMapper.updateByPrimaryKey<br>tk.mybatis.mapper.common.base.update.UpdateByPrimaryKeySelectiveMapper.updateByPrimaryKeySelective<br>tk.mybatis.mapper.common.example.DeleteByExampleMapper.deleteByExample<br>tk.mybatis.mapper.common.example.SelectByExampleMapper.selectByExample<br>tk.mybatis.mapper.common.example.SelectCountByExampleMapper.selectCountByExample<br>tk.mybatis.mapper.common.example.SelectOneByExampleMapper.selectOneByExample<br>tk.mybatis.mapper.common.example.UpdateByExampleMapper.updateByExample<br>tk.mybatis.mapper.common.example.UpdateByExampleSelectiveMapper.updateByExampleSelective<br>tk.mybatis.mapper.common.ids.DeleteByIdsMapper.deleteByIds<br>tk.mybatis.mapper.common.ids.SelectByIdsMapper.selectByIds<br>tk.mybatis.mapper.common.rowbounds.SelectByConditionRowBoundsMapper.selectByConditionAndRowBounds<br>tk.mybatis.mapper.common.rowbounds.SelectByExampleRowBoundsMapper.selectByExampleAndRowBounds<br>tk.mybatis.mapper.common.rowbounds.SelectRowBoundsMapper.selectByRowBounds<br>tk.mybatis.mapper.common.special.InsertListMapper.insertList<br>tk.mybatis.mapper.common.special.InsertUseGeneratedKeysMapper.insertUseGeneratedKeys<br>tk.mybatis.mapper.common.sqlserver.InsertMapper.insert<br>tk.mybatis.mapper.common.sqlserver.InsertSelectiveMapper.insertSelective<br> </details>
| useLink | Java MyBatis SQL Query | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | Java MyBatis SQL Query | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|
| useLink | Java MyBatis SQL Query | Missing Table | Created by Missing tables and procedures for JEE extension when the table object cannot be resolved|
| callLink | Java MyBatis SQL Query | Missing Procedure | Created by Missing tables and procedures for JEE extension when the procedure object cannot be resolved|
| useLink | Java MyBatis Entity Operation | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | Java MyBatis Entity Operation | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | C# Method | DotNet MyBatis SQL Query | <details><summary>IBatisNet APIs</summary>IBatisNet.DataMapper.ISqlMapper.QueryForList<br>IBatisNet.DataMapper.ISqlMapper.QueryForObject<br>IBatisNet.DataMapper.ISqlMapper.QueryForDictionary<br>IBatisNet.DataMapper.ISqlMapper.QueryForMap<br>IBatisNet.DataMapper.ISqlMapper.QueryWithRowDelegate<br>IBatisNet.DataMapper.ISqlMapper.Insert<br>IBatisNet.DataMapper.ISqlMapper.Delete<br>IBatisNet.DataMapper.ISqlMapper.Update</details> <details><summary>SqlBatis APIs</summary>SqlBatis.DataMapper.ISqlMapper.QueryForList<br>SqlBatis.DataMapper.ISqlMapper.QueryForObject<br>SqlBatis.DataMapper.ISqlMapper.QueryForDictionary<br>SqlBatis.DataMapper.ISqlMapper.QueryForMap<br>SqlBatis.DataMapper.ISqlMapper.QueryWithRowDelegate<br>SqlBatis.DataMapper.ISqlMapper.Insert<br>SqlBatis.DataMapper.ISqlMapper.Delete<br>SqlBatis.DataMapper.ISqlMapper.Update</details> |
| useLink | DotNet MyBatis SQL Query| Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | DotNet MyBatis SQL Query | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|
| useLink | DotNet MyBatis SQL Query | Missing Table | Created by Missing tables and procedures for .NET extension when the table object cannot be resolved|
| callLink | DotNet MyBatis SQL Query | Missing Procedure | Created by Missing tables and procedures for .NET extension when the procedure object cannot be resolved|

### Java Examples

#### SqlMapExecutor CRUD APIs

```java
 package com.alibaba.cobar.client;

import java.sql.SQLException;
import java.util.Collection;

import com.ibatis.sqlmap.client.SqlMapExecutor;


public class CobarSqlMapClientDaoSupport {

public int batchUpdate(final String statementName, final Collection<?> entities)
            throws DataAccessException {
         {
            return (Integer) getSqlMapClientTemplate().execute(new SqlMapClientCallback() {
                public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
                    executor.startBatch();
                    for (Object parameterObject : entities) {
                        executor.update("com.alibaba.cobar.client.entities.Offer.update", parameterObject);
                    }
                    return executor.executeBatch();
                }
            });
        }
    }
}

```
```xml
<sqlMap namespace="com.alibaba.cobar.client.entities.Offer">
     
     <update id="update">
		UPDATE offers SET subject=#subject#, gmtUpdated=#gmtUpdated# WHERE id=#id#
	</update>
     
</sqlMap>

```
![](../images/executor.png)

#### SqlSession CRUD APIs

```java
public void update(Village village)
   {
	   SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
	   session.update("com.rlab.mappers.VillageMapper.updatevillage",village);
	   session.commit();
	   session.close();
   }

```
```xml
<mapper namespace = "com.rlab.mappers.VillageMapper">
     
    <update id="updatevillage" parameterType = "Village">
        UPDATE village SET name=#{name},district=#{district}WHERE id= #{id}
    </update>
     
</mapper>

```
![](../images/jee_session_example.png)

#### SpringFramework SqlMapClientTemplate CRUD APIs

```java
package com.alibaba.doris.admin.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.alibaba.doris.admin.dao.ConsistentReportDao;


public class ConsistentReportDaoImpl extends SqlMapClientDaoSupport implements ConsistentReportDao {

    public Integer deleteByGmtCreate(String gmtCreateFrom, String gmtCreateTo) {
        Map<String, String> params = new HashMap<String, String>(2);
        params.put("gmtCreateFrom", gmtCreateFrom);
        params.put("gmtCreateTo", gmtCreateTo);
        return getSqlMapClientTemplate().delete("CONSISTENT_REPORT.deleteByGmtCreate", params);
    }

}

```
```xml
<sqlMap namespace = "CONSISTENT_REPORT">

    <delete id="deleteByGmtCreate" parameterClass="map" >
        DELETE FROM CONSISTENT_REPORT 
        WHERE <![CDATA[ gmt_create >= #gmtCreateFrom:VARCHAR# and gmt_create <= #gmtCreateTo:VARCHAR# ]]>
    </delete>

</sqlMap>
```
![](../images/spring_framework.png)

#### Mapper Interface

```java

public interface PersonMapper {

	Person insertPerson(Person person);
	Person findPersonById(Integer id);
	List<Person> findAllPersons();
	Person updatePerson(Person person);
	void removePerson(Integer idPerson);

}

```

``` xml
<!DOCTYPE mapper
    PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="br.com.erudio.mappers.PersonMapper">

    <update id="updatePerson" parameterType="Person">
      update person set
      firstName = #{firstName}
      ,lastName = #{lastName}
      ,address = #{address}
      where id = #{idPerson}
    </update>
    
</mapper>

```

![](../images/PersonMapperxml.png)

#### Annotations

```java

public interface UserAnnotationMapper {
	
	@Insert("insert into users(name,email) values(#{name},#{email})")
	@SelectKey(statement = "call identity()", keyProperty = "id", before = false, resultType = Integer.class)
	void insertUser(User user);

	@Select("select id, name, email from users WHERE id=#{id}")
	User findUserById(Integer id);

	@Select("select id, name, email from users")
	List<User> findAllUsers();

}

```
![](../images/AnnotationExam.png)

#### Unknown SQL Query - Annonation Provider APIs
```java
public interface AddressMapper {
	
	@InsertProvider(type = AddressServiceImpl.class, method = "dynamicSQL")
	int insertProvider(String record_);
}

```
![](../images/providerUnknown.png)

#### tk.MyBatis Mapper
##### selectCount API
```java
import com.yzx.shop.user.entity.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}
```
```java
import javax.persistence.Table;

@Table(name = "tb_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}

```
```java
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public Boolean checkUserData(String data, Integer type) {
        User record=new User();
        if(type==1){
            record.setUsername(data);
        }else if(type==2){
            record.setPhone(data);
        }else {
            return null;
        }
        return userMapper.selectCount(record)==0;
    }

}
```
![](../images/tkMyBatisUser.png)

##### deleteByPrimaryKey API

```java
import com.yzx.shop.item.entity.Stock;
import tk.mybatis.mapper.common.Mapper;

public interface StockMapper extends Mapper<Stock> {
}

```
```java
import javax.persistence.Table;

@Table(name = "tb_stock")
public class Stock {
    @Id
    private Long skuId;

    public Long getSkuId() {
        return skuId;
    }
}
```
```java
@Service
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private StockMapper stockMapper;

    @Override
    @Transactional
    public void updateGoods(SpuBo spuBo) {
        Sku record=new Sku();
        record.setSpuId(spuBo.getId());
        List<Sku> skuList=skuMapper.select(record);

        skuList.forEach(s->{
            stockMapper.deleteByPrimaryKey(s.getId());
            skuMapper.deleteByPrimaryKey(s.getId());
        });

        spuBo.setLastUpdateTime(new Date());
        spuMapper.updateByPrimaryKeySelective(spuBo);
        spuDetailMapper.updateByPrimaryKey(spuBo.getSpuDetail());

        insertSkusBySpuBo(spuBo);
        sendMsg(MSG_UPDATA,spuBo.getId());
    }
}
```
![](../images/tkMyBatisStock.png)

#### Unknown Entity

```
import tk.mybatis.mapper.common.Mapper;

public abstract class BaseService<T> implements IService<T> {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected Mapper<T> mapper;

	public Mapper<T> getMapper() {
		return mapper;
	}

    @Override
	public int batchSave(List<T> list) {
		int result = 0;
		for (T record : list) {
			int count = mapper.insertSelective(record);
			result += count;
		}
		return result;
	}
}
```
![](../images/UnknownEntity.png)
### .NET Examples

#### ISQLMapper APIs

``` c#
public static string FindDepartment(int deptId){

        ISqlMapper mapper = EntityMapper;
        string str = mapper.QueryForObject<string>("FindDepartment", deptId);
        return str;


        }

```

```xml
<?xml version="1.0" encoding="utf-8" ?>
<sqlMap namespace="MyBatisApp" xmlns="http://ibatis.apache.org/mapping" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
  
  <statements>
    <statement id="FindDepartment" parameterClass="System.Int32" resultClass="System.String" >
      SELECT Name
      FROM Department
      WHERE Id =  #value#
    </statement>

</sqlMap>
```
![](../images/DotnetXMLMapper.png)

## Structural Rules

The following structural rules are provided with MyBatis extension:

| Release | Link |
|---------|------|
| 1.3.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.3.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.3.2-funcrel) |
| 1.3.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.3.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.3.1-funcrel) |
| 1.3.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.3.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.3.0-funcrel) |

## Limitations
- SQL Query from Annotation Provider APIs cannot be resolved, therefore  Unknown MyBatis SQL Query objects will be created for them
- Unknown Entity and Unknown Entity Operation objects are created for tk.Mybatis in case the entity of the CRUD transaction cannot be resolved

