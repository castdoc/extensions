---
title: "MyBatis - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.ibatis

## What's new ?

See [Release Notes](rn/).

## Description

This extension provides support for:

-   MyBatis for Java using:
    -   Mapper XML file containing SQL queries
    -   MyBatis Annotations for JAVA
    -   MyBatis-Spring integration
-   MyBatis for .NET using:
    -   Mapper XML file containing SQL queries only  
-   TkMyBatis using:
    -   Methods provided by mapper class

## In what situation should you install this extension?

This extension should be installed when analyzing a Java/.NET project
that uses a MyBatis framework or TkMyBatis framework, and you want to
view a transaction consisting of MyBatis objects with their
corresponding links. Links to corresponding database tables can also be
resolved, provided that the SQL database has been extracted and DDL
has been created.

## Supported MyBatis versions

| Language Supported | Version | Support |
|---|---|---|
| Java | 3.4.0 | Mapper XML files<<br>MyBatis Annotation for Java<br> MyBatis-Spring Integration |
| .NET | 4.5.2 | Mapper XML file |

## Supported TkMyBatis Libraries

| Library | Version |
|---|---|
| mapper-spring-boot-starter | 2.1.5 |

## Supported TkMyBatis Operations


| Operation | Methods  |
|---|---|
| Insert | insert |
| Insert | insertSelective |
| Update | updateByExample |
| Insert | updateByExampleSelective |
| Insert | updateByPrimarykey |
| Insert | updateByPrimaryKeySelective |
| Select | select |
| Select | selectAll |
| Select | selectByExample |
| Select | selectByExampleAndRowBounds |
| Select | selectByPrimaryKey |
| Select | selectByRowBounds |
| Select | selectCount |
| Select | selectCountByExample |
| Select | selectOneByExample |
| Select | selectOne |
| Delete | delete |
| Delete | deleteByExample |
| Delete | deleteByPrimaryKey |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions 

Some extensions require the presence of other extensions in order to
function correctly. The MyBatis extension requires that the
following other CAST extensions are also installed:

-   CAST AIP Internal extension (internal technical extension)
-   [SQL Analyzer extension](../../../../sql/extensions/com.castsoftware.sqlanalyzer)

## Download and installation instructions

The extension will be automatically downloaded and installed.

## Source code discovery

The MyBatis extension does not contain any discoverers or
extractors, therefore, no "MyBatis" projects will be discovered.
However, since the code delivered is Java/.NET and SQL, when using AIP
Console, all source should be detected automatically. You should
therefore ensure that your code is organised as follows before delivery:

Java

-   one folder containing all the Java related code along with
    MyBatis.
-   one folder containing the SQL DDL extraction

.NET

-   one folder containing all the .NET related code and the XML
    related files
-   one folder containing the SQL DDL extraction

## Delivery and analysis configuration

After delivery you should end up with the
following technologies:

-   JEE or .NET for the main source code
-   Universal for the SQL source code

![](../images/669254327.jpg)

All dependencies will be automatically configured (example for JEE):

![](../images/669254326.jpg)

Note that:

-   When working with .NET, the package discovered is equivalent to the
    main source code package described in [.NET - Prepare and deliver
    the source code](.NET_-_Prepare_and_deliver_the_source_code).
-   you may need to create additional packages, for example, for
    .NET an additional package may be required to package the .NET
    Assemblies. Please refer to [.NET - Prepare and deliver the
    source code](.NET_-_Prepare_and_deliver_the_source_code).

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner. Below are the transactions obtained after
analysis as shown in CAST Enlighten:

### For Java

#### Mapper XML files

For Mapper XML files - this extension will detect SQL queries and create
objects and links. Below is a Mapper XML file with its result in
Enlighten:

![](../images/669254318.png)

![](../images/669254317.png) 

#### Support of "Include" tag

This extension provide support of "Include" tag used in Mapper XML file.
Below is a sample of "Include" tag with its result in Enlighten:

![](../images/669254316.png)

![](../images/669254315.png)

#### Support of Dynamic SQL Tags

This extension provide support of following Dynamic SQL tag used in
Mapper XML file. Below is list of tags support with sample source code
and result in Enlighten:

| Element                | Description                                                                            |
|------------------------|----------------------------------------------------------------------------------------|
| isEmpty                | Checks to see if the value of a Collection, String property is null or empty.          |
| isNotEmpty             | Checks to see if the value of a Collection, String property is not null and not empty. |
| isNotNull              | Checks if a property is not null.                                                      |
| isNull                 | Checks if a property is null.                                                          |
| isPropertyAvailable    | Checks if a property is available.                                                     |
| isNotPropertyAvailable | Checks if a property is unavailable.                                                   |
| isParameterPresent     | Checks to see if the parameter object is present (not null).                           |
| isNotParameterPresent  | Checks to see if the parameter object is not present (null).                           |
| isEqual                | Checks the equality of a property and a value, or another property.                    |
| isNotEqual             | Checks the inequality of a property and a value, or another property.                  |
| isGreaterThan          | Checks if a property is greater than a value or another property.                      |
| isGreaterEqual         | Checks if a property is greater than or equal to a value or another property.          |
| isLessEqual            | Checks if a property is less than or equal to a value or another property.             |

![](../images/669254314.png)

![](../images/669254313.png)

![](../images/669254312.png)

list of Add-on Dynamic SQL tags support with sample source code and
result in Enlighten:

|         |
|---------|
| Tags    |
| where   |
| foreach |
| if      |

``` xml
<select id="getSelectVillage" resultMap="SelectVillage">
            select ID,Name,district from Village
            <where>
                file.ID in
                <foreach collection="interNames" item="interName">#{interName}</foreach>
                <if test="District != null and !District.isEmpty">
                    and District in
                    <foreach collection="District" item="district" open="(" separator="," close=")">#{district}</foreach>
                </if>
            </where>
            group by ID
        ) as import_errors
        on persist_errors.ID = import_errors.ID
    </select>
```
![](../images/669254311.png)

#### MyBatis for Annotation / MyBatis-Spring Integration

##### Supported Annotations

This extension supports the following Annotations used in MyBatis
Annotations for Java  and MyBatis-Spring integration applications:

-   @Select - org.apache.ibatis.annotations.Select

-   @Insert - org.apache.ibatis.annotations.Insert

-   @Update - org.apache.ibatis.annotations.Update

-   @Delete - org.apache.ibatis.annotations.Delete

##### For MyBatis Annotations

In Java mapper File, when we pass the SQL Queries with annotation
parameter directly. Below is a MyBatis-Annotation sample source code
with its result in Enlighten



![](../images/669254310.png)

![](../images/669254309.png)  

In the Java Mapper file, when the annotation parameter contains
variables which are initialized with a query instead of a query
statement itself. Below is a sample source code with its result in
Enlighten:

![](../images/669254308.png)

![](../images/669254307.png)

##### For MyBatis-Spring

Below a Mybatis Spring Integration sample source code with its result in
Enlighten:

![](../images/669254306.png)
![](../images/669254305.png)

### For .NET

For Mapper XML files, below is a mapper XML file with its result in
Enlighten: 

MYBATIS DOTNET SQLNAMEDQUERY are created to link between C# method
and tables

![](../images/669254304.png)
![](../images/669254303.png)

### For TkMyBatis

#### TkMyBatis source code scenario - 1

Insert operation:

``` java
public int addVillageCore(VillagePropertysTypeDao card) throws IOException
    {
        int chck = villagePropertysTypeDao.insertSelective(entity);    
        return chck;
    }
```

![](../images/669254302.png)

Select operation:

``` java
private List<VillagePropertysTypeDTO> getVillagePropertysTypeAll() {
        return VillageNameutil.get("VillagePropertysType_All", new Callable<List<VillagePropertysTypeDTO>>() {
            public List<VillagePropertysTypeDTO> call() throws IOException {
                var list = villagePropertysTypeDao.selectAll();
                return Districtvillageutil.copyList(list, VillagePropertysTypeDTO.class);
            }
        });
    }
```
![](../images/669254301.png)

Update operation:

``` java
public int updateVillageCore(villageEntity entity) {
      
        entity.setModifyTime(new Date());
        entity.setVillageID(null);
        entity.setName(null);
        return villagePropertysTypeDao.updateByExampleSelective(entity,example);
    }
```
![](../images/669254300.png)

#### TkMyBatis source code scenario - 2

A quick introduction of how to create the project with its result in
Enlighten:

##### Configuration Entity class:

``` java
@Table(name = "`village`")
public class Village_data 
{
 .....
}
```

##### Create the interface:

``` java
import tk.mybatis.mapper.common.Mapper;
@Component
public interface VillageDao<T> extends Mapper<T>{
}
```

##### Create the DAO interface:

``` java
public interface VillagePropertysTypeDao extends VillageDao<Village_data>{
}
```

##### Test case - Use of Interface:

``` java
public List<VillagePropertysTypeDTO> getVillagePropertysCore(String memberId) {
        var queryVillage = new Village_data();
        queryVillage.setMemberID(memberId);
        var list = villagePropertysTypeDao.selectByRowBounds(queryVillage);
        return BeanUtil.copyList(list, VillagePropertysTypeDTO.class);
 }
```

![](../images/669254299.png)

### Objects

The following specific objects are displayed in CAST Enlighten:

-   Mapper xml file - this object is created by the JEE
    Analyzer/.NET Analyzer.
-   SQL_Named query - this object(s) is created by the MyBatis
    extension for Java Projects.
-   MYBATIS DOTNET SQLNAMEDQUERY - this object(s) is created by the MyBatis extension for .NET Projects.

### Links

Various links are created but the following are the links specific to
the MyBatis extension:

#### MyBatis for Java

-   Call link between a Java Method object and an SQL_Named
    query object.
-   Use (typed with Select, Delete, Insert, Update) link between an
    SQL_Named query object and a Table object.

#### MyBatis for .NET

-   Call link between a .NET Method object and an MYBATIS
    DOTNET SQLNAMEDQUERY object.
-   Use (typed with Select, Delete, Insert, Update) link between an
    MYBATIS DOTNET SQLNAMEDQUERY object and a Table object.
-   Refer link between a .NET Constructor object and an MYBATIS
    DOTNET SQLNAMEDQUERY object.

#### TkMyBatis

-   Use (typed with Select, Delete, Insert, Update) link between an Java
    Method object and a Table object.

``` java
public int addUpdateVillageCore(VillagePropertysTypeDao card) throws IOException
    {   
        Village_data entity =new Village_data();
        int chck = villagePropertysTypeDao.insertSelective(entity);    
        
        var type = new typed(Village_data.class);
        entity.setModifyTime(new Date());
        entity.setVillageID(null);
        entity.setName(null);
        return villagePropertysTypeDao.updateByExampleSelective(entity,type);
    }
```

![](../images/669254311.png)

### Structural Rules

The following structural rules are provided with MyBatis extension:

| Release   | Link    |
|---------------|-------------------|
| 1.2.2         | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.2.2](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.2.2)                 |
| 1.2.1         | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.2.1](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.2.1)                 |
| 1.2.0         | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.2.0](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.2.0)                 |
| 1.2.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.2.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.2.0-funcrel) |
| 1.2.0-beta2   | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.2.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.2.0-beta2)     |
| 1.2.0-beta1   | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.2.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.2.0-beta1)     |
| 1.2.0-alpha1  | [https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=\|\|1.2.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_ibatis&ref=%7C%7C1.2.0-alpha1)   |
