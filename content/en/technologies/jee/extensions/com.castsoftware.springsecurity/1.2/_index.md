---
title: "Spring Security - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.springsecurity

## What's new?

See [Release Notes](rn/).

## Description

### In what situation should you install this extension?

This extension provides specific rules for the Spring Security
technology. These rules are compliant with CWE and OWASP TOP 10
Standards for Security.

### How to identify if an application is using Spring Security?

Check for the presence of the Spring Security Filter in
the web.xml file:

``` xml
<filter>
    <filter-name>springSecurityFilterChain</filter-name>
    <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
</filter>
```

Check for a dependency of spring-security-web and
spring-security-config in the pom.xml file:

``` xml
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-web</artifactId>
    <version>${spring.version}</version>
</dependency>
<dependency>
    <groupId>org.springframework.security</groupId>
    <artifactId>spring-security-config</artifactId>
    <version>${spring.version}</version>
</dependency>
```

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Supported Spring Security and Framework versions

This extension is compatible with the following Spring Security and
Framework versions:

| Item | Version |
|:--------------------------|:----------------|
| Spring Security versions  | 3.2.0 and above |
| Spring Framework versions | 3.2.0 and above |

## Packaging, delivering and analyzing your source code

Once the extension is downloaded and installed, there is nothing
specific to do: analyze your source code with the JEE Analyzer and the
rules will be triggered.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner.

### Structural rules

The following structural rules are provided:

| Release | Link |
|---------|------|
| 1.2.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.13-funcrel) |
| 1.2.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.12-funcrel) |
| 1.2.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.11-funcrel) |
| 1.2.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.10-funcrel) |
| 1.2.9-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.9-funcrel) |
| 1.2.8-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.8-funcrel) |
| 1.2.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.7-funcrel) |
| 1.2.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.6-funcrel) |
| 1.2.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.5-funcrel) |
| 1.2.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.4-funcrel) |
| 1.2.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.3-funcrel) |
| 1.2.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.2-funcrel) |
| 1.2.1-funcrel | Extension withdrawn                                                                                                                                                                                    |
| 1.2.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=\|\|1.2.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_springsecurity&ref=%7C%7C1.2.0-funcrel) |
