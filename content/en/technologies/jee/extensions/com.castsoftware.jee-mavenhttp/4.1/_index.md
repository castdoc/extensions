---
title: "JEE Maven Http Extractor - 4.1"
linkTitle: "4.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.JEE-MavenHttp

## What's new ?

See [Release Notes](rn/).

## Extension description

This extension provides the means to extract JAR based source code via
http/https from a remote Maven repository as well from Maven repository
on your file system (file://). This is similar in function to the Maven
repository on your file system option that is provided "out of the box"
in the CAST Delivery Manager Tool, along with an enhancement that
combines the capabilities of the old Maven HTTP Extractor and of the
legacy Maven Extractor on your file system.

### In what situation should you install this extension?

This extension should be used when you want to extract JAR based source
code that is stored in a remote Maven repository. For example, when
your JEE application relies on JAR files and the initial extraction of
this application in the CAST Delivery Manager Tool throws "missing
library file" alerts, you can use this extension to extract the
missing files from a remote Maven repository and resolve the alerts.

### Technical information

-   The extension does not contain a source code "discoverer" (to
    determine the type of source code project) and therefore relies on
    other discoverers that are already installed in the CAST Delivery
    Manager Tool or via other extensions to do so.
-   This extractor supports remote Maven repository as well as Maven
    repository on your file system. The URL format must use the
    http/https/file protocol.
-   If you need to extract data from a https repository, please
    ensure that you are using the extension with AIP Core ≥ 8.3.10.

## Supported Maven releases

| Maven release | Supported |
|---|---|
| 3.x.x | :white_check_mark: |
| 2.x.x | :white_check_mark: |
| 1.x.x | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Extension interface

The following screen shots show the differences in the product when the
extension is installed:

-   A new sub-option will be added to the Automated extraction of
    required jar files option:

![](../images/669253681.png)

-   The new sub-option is called Maven repository:

![](../images/669253680.png)

-   The Package Configuration tab then offers the following
    interface to access your Maven repository:

![](../images/669253679.jpg)

![](../images/669253678.jpg)

<table class="confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: center;"><p>Option</p></th>
<th class="confluenceTh"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<th class="confluenceTh" style="text-align: center;">1</th>
<td class="confluenceTd">Enter the direct URL for your Maven repository
using the <strong>http/https</strong> protocol<strong><br />
</strong></td>
</tr>
<tr class="even">
<th class="confluenceTh" style="text-align: center;"> 2</th>
<td class="confluenceTd"><p>Select the <strong>Proxy</strong> option, if
Maven http repository requires a proxy pass, then configure:</p>
<ul>
<li><strong>Host:</strong> Used to configure the proxy hostname requires
to access the Maven http repository</li>
<li><strong>Port:</strong> Used to configure the proxy port requires to
access the Maven http repository.</li>
</ul></td>
</tr>
<tr class="odd">
<th class="confluenceTh" style="text-align: center;">3</th>
<td class="confluenceTd"><p>Tick the <strong>Credentials</strong> option
if the Maven repository requires authenticated access, then
configure:</p>
<ul>
<li><strong>User name:</strong> Used to configure the user name that has
sufficient privileges to access the Maven repository for packaging
purposes.</li>
<li><strong>Password:</strong> Used to configure the password that
corresponds to your User name configured above.</li>
<li><strong>Remember password:</strong> This option enables you to force
the CAST Delivery Manager Tool to save the database access credentials
you have entered above. Choosing an option has no impact on the
extraction (i.e. the CAST Delivery Manager Tool can still access the
required resources). However, if you are creating subsequent versions of
the same schemas on the same server, you can choose to store the
password (by storing you will not need to re-enter it). There are two
save options:
<ul>
<li><strong>Local</strong> &gt; The credentials are saved in the user's
local workspace on the current machine. Choose this option if you do not
want the password to be available to other Delivery Managers.</li>
<li><strong>Server</strong> &gt; The credentials are saved locally (as
above) and are also synchronized back to the CAST AIC Portal (i.e. the
Source Code Delivery Folder). Choose this option if you want the
password to be available to other Delivery Managers.</li>
</ul></li>
</ul></td>
</tr>
<tr class="even">
<th class="confluenceTh" style="text-align: center;">4</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This option enables you to specify specific artifacts that you know
need to be extracted in order to resolve a packaging alert. You can
specify the artifact using:</p>
<ul>
<li><strong>Group ID</strong> (usually a reverse domain name like
com.example.foo) - this option is mandatory</li>
<li><strong>Artifact ID</strong> (usually the name) - this option is
mandatory</li>
<li><strong>Version</strong> (the artifact's version string) - this
option is mandatory</li>
<li>Classifier (an arbitrary string that distinguishes artifacts that
were built from the same POM but differ in their content) - this option
is mandatory.</li>
</ul>
<div>
<div>
<ul>
<li>If you do not specify an element, the CAST Delivery Manager Tool
will automatically populate the list of elements when you run the
Package action based on what it finds in the repository URL you have
entered.</li>
<li><strong>&lt;groupID&gt;</strong> or
<strong>&lt;artifactID&gt;</strong> configured in
<strong>&lt;relocation&gt;</strong> tags are supported.</li>
<li><strong>maven-metadata.xml</strong> will be used to determine a best
version for the &lt;versionID&gt; if no pom.xml can be found.</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh" style="text-align: center;">5</th>
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>Additional Maven Repository:</strong> This option enables
configuring additional Maven repositories.</p>
<ul>
<li><strong>Repository URL:</strong> Enter the direct URL for your
additional Maven repository using the http/https/file protocol</li>
<li><strong>User name:</strong> Used to configure the user name that has
sufficient privileges to access the Maven repository for packaging
purposes</li>
<li><strong>Password:</strong> Used to configure the password that
corresponds to your User name configured above.</li>
</ul>
<div>
<div>
<p>Each additional repository has individual configuration option for
username and password.</p>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh" style="text-align: center;">6</th>
<td class="confluenceTd"><div class="content-wrapper">
<div>
<div>
These options should not be modified unless you are having issues such
as missing JARs, missing classes, broken transactions etc. In the vast
majority of situations, the default values for these options are
sufficient.
</div>
</div>
<p>These three options (available in ≥ 4.1.x) govern how the extractor
behaves with regard to depth of artifacts extracted. The default
settings are as shown below:</p>
<p><img src="../images/669253677.jpg" draggable="false"
data-image-src="../images/669253677.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="669253677"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="depth.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="669253676"
data-linked-resource-container-version="1" width="423"
height="133" /></p>
<p>To allow the extractor extract as many artifacts as needed, you can
use the following settings:</p>
<ul>
<li>Extract Dependencies Depth: 99</li>
<li>Maximum Extracted Artifacts Factor: 999</li>
<li>Maximum Extracted Artifacts before Loops Factor: 999</li>
</ul>
<p>However, when using these unlimited options, the maximum extracted
artifacts would be <strong>999 * minimum remediated
artifacts.</strong> This could lead to performance issues, therefore,
changing these parameters should be done with caution (e.g., to insure
stability of transaction call graphs).</p>
</div></td>
</tr>
</tbody>
</table>

## Packaging and extraction messages

The following messages may appear during the packaging action:

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Message ID</th>
<th class="confluenceTh">Format</th>
<th class="confluenceTh">Message</th>
<th class="confluenceTh">Action</th>
</tr>
&#10;<tr class="odd">
<td
class="confluenceTd">.http.connectionFailed</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Connection failed for %URL%: %MESSAGE%</td>
<td class="confluenceTd">Check the connection URL.</td>
</tr>
<tr class="even">
<td
class="confluenceTd">.http.authenticationFailed</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Authentication failed: %MESSAGE%</td>
<td class="confluenceTd">Check the credentials are correct.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">.http.artifactRetrievalFailed</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Technical error during the extraction of the
artifact from %URL% failed: %MESSAGE%</td>
<td class="confluenceTd">Check the access to the repository.</td>
</tr>
<tr class="even">
<td
class="confluenceTd">.http.artifactMetadataRetrievalFailed</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Technical error during the extraction of the
maven-metadata.xml file from %URL% failed: %MESSAGE%</td>
<td class="confluenceTd">Without the data required to identify the
versions for this artifact, we can't determine a best version. Please
contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a> and report a bug.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">.http.versionMetadataRetrievalFailed</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Technical error during the extraction of the
maven-metadata.xml file from %URL% failed: %MESSAGE%</td>
<td class="confluenceTd">Without the data required to identify the files
for the SNAPSHOT, the artifact can't be extracted. Please contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a> and report a bug.</td>
</tr>
<tr class="even">
<td
class="confluenceTd">.http.pomReadContentError</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Technical error while reading the pom file for
the artifact [%GROUP_ID%][%ARTIFACT_ID%][%VERSION%]: %MESSAGE%</td>
<td class="confluenceTd">If the relocation is defined, the jar file will
not be extracted. Please contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a> and report a bug.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">.http.artifactWithoutVersion</td>
<td class="confluenceTd">WARNING</td>
<td class="confluenceTd">No version has been provided for the artifact
[%GROUP_ID%][%ARTIFACT_ID%].</td>
<td class="confluenceTd"><p>In the maven dependency, the version should
be defined or inherited from the parent. Check the packaging of the
source code.</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd">.http.artifactWithVersionVariable</td>
<td class="confluenceTd">WARNING</td>
<td class="confluenceTd">Version has been provided with variable for the
artifact [%GROUP_ID%][%ARTIFACT_ID%].</td>
<td class="confluenceTd">The variable should be defined or inherited
from the parent. Check the packaging of the source code.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">.http.notSupportedArtifactWithRange</td>
<td class="confluenceTd">WARNING</td>
<td class="confluenceTd">The automated extraction of artifacts with
range is not supported: artifact
[%GROUP_ID%][%ARTIFACT_ID%][%VERSION%].</td>
<td class="confluenceTd">Please contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a> and report a feature
request.</td>
</tr>
<tr class="even">
<td
class="confluenceTd">.http.getArtifact</td>
<td class="confluenceTd">INFO</td>
<td class="confluenceTd">Start to retrieve the artifact
[%GROUP_ID%][%ARTIFACT_ID%][%VERSION%] from the repository.</td>
<td class="confluenceTd">None.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">.http.notFoundArtifact</td>
<td class="confluenceTd">INFO</td>
<td class="confluenceTd">The artifact %GROUP_ID% %ARTIFACT_ID% has not
been found in the repository.</td>
<td class="confluenceTd">Check the configuration details and update if
necessary.</td>
</tr>
<tr class="even">
<td
class="confluenceTd">.http.notFoundArtifactMetadata</td>
<td class="confluenceTd">INFO</td>
<td class="confluenceTd">The metadata for artifact %GROUP_ID%
%ARTIFACT_ID% has not been found in the repository.</td>
<td class="confluenceTd">Check the configuration details and update if
necessary.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">.combined.invalidURL</td>
<td class="confluenceTd"> ERROR</td>
<td
class="confluenceTd">.combined.invalidURL
=&gt; %URL%=%MESSAGE%</td>
<td class="confluenceTd">Check the repository url provided.<br />
supported protocols: file://, http://, https://</td>
</tr>
</tbody>
</table>

## Limitations

### Parent POM files scanned

Parent POM files are scanned but a limit is added to the recursive
extraction of the Maven dependent artifacts.

When packaging a J2EE Maven resource package, if a pom file has a
parent, the parent file is scanned and we extract all JAR dependencies
from first level.

### Packaging Type \<POM\>

POM file with the packaging type as \<POM\> will not extract
additional resources "JAR","War" and "Zip" files. 

### Extraction Limitations

Extraction levels are limited to:

-   Two for dependent artifacts
-   No limit for parent artifacts

The total number of artifacts extracted using additional levels is
limited to ten times the number of remediation artifacts (extracted at
the base level), the first two additional levels are not started if this
number is greater than three times the number of remediation artifacts.

Avoid using JEE Maven Http Extractor ≥ 2.0.3 and \<=2.0.5, as JAR
dependencies are extracted recursively without limit, this could have a
big impact on performance.
