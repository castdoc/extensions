---
title: "EJB - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.ejb

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for EJB (Enterprise Java Beans).

### In what situation should you install this extension?

If your JEE application contains source code which uses EJB and you want
to view these object types and their links with other objects, then you
should install this extension. The main purpose of this extension is to
enable linking between matching methods of Java Remote Interfaces to
Bean Java Classes based on information provided in the ejb-jar.xml
deployment descriptor file or via annotations.

## Supported EJB releases

| EJB release | Supported | Notes |
|-------------|-----------|-------|
| EJB ≤ 2.x (via deployment descriptor ejb-jar.xml ) | :white_check_mark: | This extension handles links between Java Remote Interfaces to Beans operated by the ejb-jar.xml <session> block. All .xml file ending with ejb-jar.xml are analyzed. See the following non-exhaustive list of supported filenames for the ejb-jar.xml file: orion-ejb-jar.xml, weblogic-ejb-jar.xml, sun-ejb-jar.xml, ibm-ejb-jar.xml |
| EJB ≥ 3.x (via annotations) | :white_check_mark: | - |                                                                                                                                                                                                                         
## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For JEE applications using Enterprise Java Beans (EJB), the extension
will be automatically installed by CAST Console:

![](../images/669253696.jpg)

## What results can you expect?

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Supported package</th>
<th class="confluenceTh">Supported API Methods</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">javax.ejb.*</td>
<td class="confluenceTd"><p>javax.ejb.Remote</p>
<p>javax.ejb.Stateless</p>
<p>javax.ejb.Stateful</p>
<p>javax.ejb.EJBObject</p>
<p>javax.ejb.SessionBean</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">jakarta.ejb.*</td>
<td class="confluenceTd"><p>jakarta.ejb.Remote</p>
<p>jakarta.ejb.Stateless</p>
<p>jakarta.ejb.Stateful</p>
<p>jakarta.ejb.EJBObject</p>
<p>jakarta.ejb.SessionBean</p></td>
</tr>
</tbody>
</table>

### Basic support for Remote to Bean communication via the ejb-jar.xml deployment descriptor file (EJB ≤ 2.x)

Given the following ejb-jar.xml file:

``` java
<?xml version="1.0" encoding="UTF-8"?>
<ejb-jar version="3.0" xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/ejb-jar_3_0.xsd">
  <display-name>MyEJB </display-name>
  <enterprise-beans>
        <session id="MyFacade">
            <ejb-name>MyFacade</ejb-name>
            <home>*.MyFacadeHome</home>
            <remote>*.MyFacade</remote>
            <ejb-class>*.MyFacadeBean</ejb-class>
            <session-type>Stateless</session-type>
            <transaction-type>Bean</transaction-type>
        </session>
    </enterprise-beans>
  <ejb-client-jar>AWDControllerEJBClient.jar</ejb-client-jar> 
 </ejb-jar>
```

Assuming that the java interface `*.MyFacade` and the java class
`*``.MyFacadeBean` are declared correctly, the extension will produce
callLink links between matching methods as follows: 

![](../images/669253695.png)

### Basic support for Remote to Bean communication via annotations (EJB ≥ 3.x)

#### Case 1: When both the Interface and Class are annotated

Given the following code snippets from the annotated interface :

``` java
package com.tuto.ejb;
import java.util.Map;
import javax.ejb.Remote;

@Remote
public interface ExampleService {
    public String greet(String name);

    public Map<Object, Object> getSystemProperties();
}
```

And the annotated class:

``` java
package com.tuto.ejb;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;


@Stateless
public class ExampleServiceImpl implements ExampleService {

    @Override
    public String greet(String name) {
        return "Hello " + name + "!";
    }

    @Override
    public Map<Object, Object> getSystemProperties() {
        return new HashMap<>(System.getProperties());
    }
}
```

...the extension will create the following callLink:

![](../images/669253694.png)

#### Case 2: When only the class is annotated

Given the following code snippets from the annotated interface:

``` java
package com.tuto.ejb;
import java.util.Map;

public interface ExampleService2 {
    public String greet(String name);

    public Map<Object, Object> getSystemProperties();

    public String greet_toto(String name);
}
```

And the annotated class:

``` java
package com.tuto.ejb;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.Remote;

@Remote(ExampleService2.class)
@Stateless
public class ExampleServiceImpl2 implements ExampleService2 {

    @Override
    public String greet(String name) {
        return "Hello " + name + "!";
    }

    @Override
    public Map<Object, Object> getSystemProperties() {
        return new HashMap<>(System.getProperties());
    }

    @Override
    public String greet_toto(String name) {
        return "Hello Toto !";
    }

}
```

...the extension will create the following callLink:

![](../images/669253693.png)

#### Case 3: Extension of the Remote Interface with the business logic

Following the same pattern as in case 2, except that methods are
declared in an inherited interface of the Remote Interface:

![](../images/669253692.png)
