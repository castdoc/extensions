---
title: "JDBC - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.sql

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for the following APIs found in JDBC packages which are responsible for typical CRUD operations with the
database (see https://docs.oracle.com/javase/7/docs/technotes/guides/jdbc/).

## In what situation should you install this extension?

If your Java application uses JDBC API to access a relational database.

## Technology support

The JDBC API is part of Java SE. Therefore, supported JDBC versions follow those supported by the JEE Analyzer - see [Covered Technologies](../../../technos/). Also, all overrides in vendor or custom JDBC extensions are supported. 

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For JEE applications using JDBC, the extension will be automatically installed. This is in place since october 2023. For upgrade, if the Extension Strategy is not set to set to "Auto Update", you will need to manually install the new release.

## What results can you expect?

### Objects

| Icon | Type Description | When is this object created ? |
|---|---|---|
| ![](../images/669254075.png) | JDBC SQL Query | an objects is created for each SQL query found and resolved in a JDBC method call  |
| ![](../images/unknown_sql_query_icon.png) | JDBC Unknown SQL Query | an objects is created for each SQL query found and the exact query cannot be resolved  |

### Links

The following link types are created by the JDBC extension, or by other
extensions.

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller type</th>
<th class="confluenceTh">Callee type</th>
<th class="confluenceTh">Methods Supported / comment</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Java Method</td>
<td class="confluenceTd">JDBC SQL Query, JDBC Unknown SQL Query </td>
<td class="confluenceTd"><div class="content-wrapper">

<div id="expander-239846969" class="expand-container">

<div id="expander-control-239846969" class="expand-control"
aria-expanded="true">

</div>
<div id="expander-content-239846969" class="expand-content">
<details>
<summary>java.sql.Statement</summary>
<ul>
<li>java.sql.Statement.executeQuery</li>
<li>java.sql.Statement.executeUpdate</li>
<li>java.sql.Statement.execute</li>
<li>java.sql.Statement.executeBatch</li>
<li>java.sql.Statement.executeLargeUpdate</li>
<li>java.sql.Statement.executeLargeBatch</li>
</ul>
</div></details>
</div>
<div id="expander-432808502" class="expand-container">
<div id="expander-control-432808502" class="expand-control"
aria-expanded="true">

</div>
<div id="expander-content-432808502" class="expand-content">
<details>
<summary>java.sql.PreparedStatement</summary>
<ul>
<li>java.sql.PreparedStatement.executeQuery</li>
<li>java.sql.PreparedStatement.executeUpdate</li>
<li>java.sql.PreparedStatement.execute</li>
<li>java.sql.PreparedStatement.executeBatch</li>
<li>java.sql.PreparedStatement.executeLargeUpdate</li>
<li>java.sql.PreparedStatement.executeLargeBatch</li>
</ul>
</div></details>
</div>
<div id="expander-470155258" class="expand-container">
<div id="expander-control-470155258" class="expand-control"
aria-expanded="true">

</div>
<div id="expander-content-470155258" class="expand-content">
<details>
<summary>java.sql.CallableStatement</summary>
<ul>
<li>java.sql.CallableStatement.executeQuery</li>
<li>java.sql.CallableStatement.executeUpdate</li>
<li>java.sql.CallableStatement.execute</li>
<li>java.sql.CallableStatement.executeBatch</li>
<li>java.sql.CallableStatement.executeLargeUpdate</li>
<li>java.sql.CallableStatement.executeLargeBatch</li>
</ul>
</div></details>
</div>
<div id="expander-1937471009" class="expand-container">
<div id="expander-control-1937471009" class="expand-control"
aria-expanded="true">

</div>
<div id="expander-content-1937471009" class="expand-content">
<details>
<summary>javax.sql.RowSet</summary>
<ul>
<li>javax.sql.RowSet.execute</li>
</ul>
</div></details>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">JDBC SQL Query</td>
<td class="confluenceTd">Table, View</td>
<td rowspan="2" class="confluenceTd">Created by <strong>SQL
Analyzer</strong> when DDL source files are analyzed</td>
</tr>
<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">JDBC SQL Query</td>
<td class="confluenceTd">Procedure</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">JDBC SQL Query</td>
<td class="confluenceTd">Missing Table</td>
<td rowspan="2" class="confluenceTd">Created by <strong>Missing tables
and procedures for JEE</strong> extension when the object is not
analyzed</td>
</tr>
<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">JDBC SQL Query</td>
<td class="confluenceTd">Missing Procedure</td>
</tr>
</tbody>
</table>

## Example code scenarios

### CRUD Operation for statement/preparedStatement

``` java
    public static void deleteRecord(Connection con, String query) {
        try {
            Statement st1 = con.createStatement();
            int countQuery = st1.executeUpdate("delete from cars where id = 6");
            System.out.println(
                    "\n Output of executeUpdate() without PrepareStatement--> \nRow affected by Query  " + countQuery);

            int countQuery_1 = st1.executeUpdate(query);
            System.out.println(
                    "\n Output of executeUpdate() with PrepareStatement--> \nRow affected by Query  " + countQuery_1);
        } catch (Exception e) {
            System.out.println("\n Exception");
        }
    }
```

![](../images/669254074_1.png)![](../images/669254073.png)

### CRUD Operation for Callable Statement 

``` java
    public static void callStoredProcedure(Connection con, String stored_procedure) {
        try {
            CallableStatement csmt = con.prepareCall(stored_procedure);
            ResultSet rs_call = csmt.executeQuery();
            while (rs_call.next()) {
                System.out.println("Company_name is " + rs_call.getString("Company_name"));
            }
        } catch (Exception e) {
            System.out.println("\n Exception");
        }

    }
```

![](../images/669254072.png)

### CRUD Operation for RowSet

``` java
    public static void JdbcRowSet_method(String query) {
        try {
            JdbcRowSet rowSet = RowSetProvider.newFactory().createJdbcRowSet();
            rowSet.setUrl("jdbc:mysql://localhost:3306/vehicle");
            rowSet.setUsername("root");
            rowSet.setCommand(query);
            rowSet.setPassword("mysql");
            rowSet.execute();
            System.out.println("JDBC RowSet ");
            while (rowSet.next()) {
                // Generating cursor Moved event
                System.out.println("Id: " + rowSet.getInt(1));
                System.out.println("Company Name : " + rowSet.getString(2));
                System.out.println("Model name: " + rowSet.getString(3));
                System.out.println("Launch year : " + rowSet.getInt(4));
            }
        } catch (Exception e) {
            System.out.println("\n Exception");
        }

    }
```

![](../images/669254071.png)

### Unknown SQL Query

``` java
private static void writeFile(Statement statement, String request, String destFile) throws Exception {
    ResultSet resultSet = statement.executeQuery(request);
    ResultSetMetaData resultSetMetadata = resultSet.getMetaData();
    int columnsCount = resultSetMetadata.getColumnCount();
    
    FileOutputStream fos = new FileOutputStream(destFile);
    while (resultSet.next()) {
      for (int i = 0; i < columnsCount; ++i) {
        Object columnValue = resultSet.getObject(i + 1);
        if (columnValue != null) {
          fos.write(columnValue.toString().getBytes());
        }
        if (i != columnsCount - 1) {
          fos.write(CSV_SEPARATOR.getBytes());
        } else {
          fos.write("\n".getBytes());
        }
      }
    }
    System.out.println("Génération du fichier csv effectuée.");    
    fos.close();
    resultSet.close();
  }
```

![](../images/unknown_jdbc.png)
