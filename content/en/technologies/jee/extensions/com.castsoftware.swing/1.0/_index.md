---
title: "Java UI - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.swing

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Java Swing, AWT, SWT and JFace presentation frameworks:

-   Java AWT (Abstract Window Toolkit)
    - [https://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html](https://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html)
-   Java Swing
    - [https://docs.oracle.com/javase/tutorial/uiswing/start/about.html](https://docs.oracle.com/javase/tutorial/uiswing/start/about.html)
-   Eclipse SWT/JFace
    - [https://www.eclipse.org/swt/](https://www.eclipse.org/swt/) and [https://www.eclipse.org/swt/javadoc.php](https://www.eclipse.org/swt/javadoc.php)

This extension works together with the [JEE Analyzer](../../com.castsoftware.jee/).

### Object structure

#### UI objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Name</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td rowspan="4" class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669253923.png" draggable="false"
data-image-src="../images/669253923.png"
data-unresolved-comment-count="0" data-linked-resource-id="669253923"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669253911"
data-linked-resource-container-version="1" width="32" /></p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
</div></td>
<td class="confluenceTd">Swing UI</td>
<td class="confluenceTd"><p>One Swing UI object is created for each
class inheriting from:</p>
<ul>
<li>javax.swing.JFrame</li>
<li>javax.swing.JWindow</li>
<li>javax.swing.JDialog</li>
<li>javax.swing.JApplet</li>
<li>javax.swing.text.EditorKit</li>
<li>javax.swing.AbstractCellEditor</li>
<li>javax.swing.JComponent</li>
</ul>
<p>...except for the following classes which inherit from
JComponent:</p>
<ul>
<li>javax.swing.plaf.basic.BasicInternalFrameTitlePane</li>
<li>javax.swing.Box</li>
<li>javax.swing.Box.Filler</li>
<li>javax.swing.JInternalFrame.JDesktopIcon</li>
<li>javax.swing.JLabel</li>
<li>javax.swing.JSeparator</li>
<li>javax.swing.JToolTip</li>
<li>javax.swing.JViewport</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">AWT UI</td>
<td class="confluenceTd"><p>One AWT UI object is created for each class
inheriting from:</p>
<ul>
<li>java.awt.Frame</li>
<li>other java.awt.xxx classes</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">SWT UI</td>
<td class="confluenceTd"><p>One SWT UI object is created for each class
inheriting from:</p>
<ul>
<li>org.eclipse.swt.widgets.Dialog</li>
<li>org.eclipse.swt.widgets.Widget</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">JFace UI</td>
<td class="confluenceTd"><p>One JFace UI object is created for each
class inheriting from:</p>
<ul>
<li>org.eclipse.jface.dialogs.Dialog</li>
<li>org.eclipse.jface.dialogs.DialogPage</li>
<li>org.eclipse.ui.part.WorkbenchPart</li>
</ul></td>
</tr>
</tbody>
</table>

#### Swing, SWT, JFace Event Handler objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Name</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669253921.png" draggable="false"
data-image-src="../images/669253921.png"
data-unresolved-comment-count="0" data-linked-resource-id="669253921"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669253911"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">Common Event Handler, Swing Event Handler, SWT
Event Handler, JFace Event Handler</td>
<td class="confluenceTd"><p>One event Handler object is created for each
method of anonymous classes present as parameter of following calls:</p>
<ul>
<li>addActionListener</li>
<li>addMouseListener</li>
<li>addFocusListener</li>
<li>...</li>
</ul>
<p>If the parameter is not an anonymous class, but an instance on a
listener, no handler is created, but a link will be created to the
methods which represent event handlers in the listener class. Supported
listener classes are those provided in <strong>swing</strong> and
<strong>awt</strong> (in the following packages:
<strong>javax.swing.event</strong> and <strong>java.awt.event</strong>).
Examples:</p>
<ul>
<li>java.awt.event.ActionListener</li>
<li>java.awt.event.ChangeListener</li>
<li>java.awt.event.ChangeListener</li>
<li>....</li>
</ul>
<p>The <strong>Jive Swing</strong> framework is supported, as soon as
the corresponding jar file is defined in the classpath.</p>
</td>
</tr>
</tbody>
</table>

### Link structure

-   Creates links from UI objects to Event Handler objects.
-   Creates links from UI objects to java methods which represent
    event handlers (ex: actionPerformed methods).
-   Creates links from Event Handler objects to java methods called
    by these handlers.
-   Creates links from Swing UI objects to "actionPerformed" java
    methods of classes representing actions, when such a class instance
    is passed as parameters of "JButton::JButton()" or
    "JButton::setAction()".

In some cases, when a call to a swing listener method as
"actionPerformed" for example is too difficult to find precisely
(through addActionListener), a post application procedure is done with a
specific algorithm searching for calls to constructors of these listener
classes. Then a link to the method can be created.

### Examples

``` java
public class BookingPanel extends javax.swing.JPanel {
    
    private javax.swing.JButton btnSearch;

    private void initComponents() {
       btnSearch = new javax.swing.JButton();
       btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
    }
}
```

![](../images/669253920.png)

``` java
public class PanneauEditProfilOuvrage extends JPanel
{
  private Box creerToolBar(TablePaletteFiltre table)
  {
    Action action = new ActionMonter(table);
    JButton btn = new JButton(action);
  }
}
```

![](../images/669253919.png)

Case of a link found by the post application procedure (with calls to
constructors):

``` java
public class ASTFrame extends JFrame {
    public ASTFrame(String lab, AST r) {
        super(lab);
        JTreeASTPanel tp = new JTreeASTPanel(new JTreeASTModel(r), null);
   }
}
```
![Alt text](../images/669253918.png)


  
#### Transactions

A CAST Transaction Configuration Center .tccsetup file is provided
defining one transaction entry point for all Swing/SWT/JFace UI objects:

![](../images/669253914.png)

Note that the [JEE Analyzer](../../com.castsoftware.jee/)extension also provides
Java Swing transaction entry points and these overlap with the
configuration provided in the Java Swing extension. CAST recommends
disabling the Java Swing transaction entry points provided by the JEE
Analyzer extension on a case by case basis, where conflicting results
are produced.

![](../images/669253913.jpg)

## In what situation should you install this extension?

If your JEE application source code uses Swing library (javax.swing.\*), you should install this extension.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Console. If you need to use it, should manually install the
extension using the Application -
Extensions interface:

![](../images/669253912.jpg)
