---
title: "Java UI - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.swing

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Java Swing, AWT, SWT, JFace and
JavaFX/OpenJFX presentation frameworks:

-   Java AWT (Abstract Window Toolkit)
    - [https://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html](https://docs.oracle.com/javase/7/docs/api/java/awt/package-summary.html)
-   Java Swing
    - [https://docs.oracle.com/javase/tutorial/uiswing/start/about.html](https://docs.oracle.com/javase/tutorial/uiswing/start/about.html)
-   Eclipse SWT/JFace
    - [https://www.eclipse.org/swt/](https://www.eclipse.org/swt/) and [https://www.eclipse.org/swt/javadoc.php](https://www.eclipse.org/swt/javadoc.php)
-   JavaFX/OpenJFX - https://openjfx.io/javadoc/12/ and https://www.javatpoint.com/first-javafx-application

This extension works together with the [JEE Analyzer](../../com.castsoftware.jee/).

### Object structure

#### UI objects

<table>
<thead>
  <tr>
    <th>Icon</th>
    <th>Name</th>
    <th>Description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td rowspan="4" valign="top"><img src="../images/669253923.png"/></td>
    <td>Swing UI</td>
    <td>One Swing UI object is created for each class inheriting from:<ul>
      <li>javax.swing.JFrame</li>
      <li>javax.swing.JWindow</li>
      <li>javax.swing.JDialog</li>
      <li>javax.swing.JApplet</li>
      <li>javax.swing.text.EditorKit</li>
      <li>javax.swing.AbstractCellEditor</li>
      <li>javax.swing.JComponent </li>
    </ul>
    <p>...except for the following classes which inherit from JComponent:</p>
    <ul>
      <li>javax.swing.plaf.basic.BasicInternalFrameTitlePane</li>
      <li>javax.swing.Box</li>
      <li>javax.swing.Box.Filler</li>
      <li>javax.swing.JInternalFrame.JDesktopIcon</li>
      <li>javax.swing.JLabel</li>
      <li>javax.swing.JSeparator</li>
      <li>javax.swing.JToolTip</li>
      <li>javax.swing.JViewport </li>
    </ul>
    </td>
  </tr>
  <tr>
    <td>AWT UI</td>
    <td>One AWT UI object is created for each class inheriting from:<ul>
      <li>java.awt.Frame</li>
      <li>other java.awt.xxx classes </li>
    </ul>
    </td>
  </tr>
  <tr>
    <td>SWT UI</td>
    <td>One SWT UI object is created for each class inheriting from:<ul>
      <li>org.eclipse.swt.widgets.Dialog</li>
      <li>org.eclipse.swt.widgets.Widget </li>
    </ul>
    </td>
  </tr>
  <tr>
    <td>JFace UI</td>
    <td>One JFace UI object is created for each class inheriting from:<ul>
      <li>org.eclipse.jface.dialogs.Dialog</li>
      <li>org.eclipse.jface.dialogs.DialogPage</li>
      <li>org.eclipse.ui.part.WorkbenchPart </li>
    </ul>
    </td>
  </tr>
  <tr>
    <td><img src="../images/669253923.png"/></td>
    <td>JavaFX View</td>
    <td>One JavaFX View is created for each .fxml file</td>
  </tr>
  <tr>
    <td><img src="../images/669253922.png" /></td>
    <td>JavaFX Application</td>
    <td>One JavaFX Application for each class inheriting from:<ul>
      <li>javafx.application.Application</li>
    </ul>
    </td>
  </tr>
</tbody>
</table>

#### Swing, SWT, JFace Event Handler objects

<table>
<thead>
  <tr>
    <th>Icon</th>
    <th>Name</th>
    <th>Description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td valign="top"><img src="../images/669253921.png" width="32"></td>
    <td>Common Event Handler, Swing Event Handler, SWT Event Handler, JFace Event Handler, JavaFX Event Handler</td>
    <td>One event Handler object is created for each method of anonymous classes present as parameter of following calls:<ul>
      <li>addActionListener</li>
      <li>addMouseListener</li>
      <li>addFocusListener</li>
      <li>...</li>
    </ul>
    <p>If the parameter is not an anonymous class, but an instance on a listener, no handler is created, but a link will be created to the methods which represent event handlers in the listener class. Supported listener classes are those provided in swing and awt (in the following packages: javax.swing.event and java.awt.event). Examples:</p>
    <ul>
      <li>java.awt.event.ActionListener</li>
      <li>java.awt.event.ChangeListener</li>
      <li>java.awt.event.ChangeListener</li>
      <li>...</li>
    </ul>
    <p>.The Jive Swing framework is supported, as soon as the corresponding jar file is defined in the classpath.</p>
    <p>For JavaFX:</p>
    <ul>
      <li>javafx.scene.Node.setOnXXX, as javafx.scene.Node.setOnMouseClicked for example
      </li>
    </ul>
    </td>
  </tr>
</tbody>
</table>

### Link structure

-   Creates links from UI objects to Event Handler objects.
-   Creates links from UI objects to java methods which represent
    event handlers (ex: actionPerformed methods).
-   Creates links from Event Handler objects to java methods called
    by these handlers.
-   Creates links from Swing UI objects to "actionPerformed" java
    methods of classes representing actions, when such a class instance
    is passed as parameters of "JButton::JButton()" or
    "JButton::setAction()".
-   Creates links from JavaFX View objects to handlers or java
    methods present in the view controller.
-   Creates links from JavaFX Application objects to handlers or
    java "start" methods present in the application class, and links from the handlers to the Java Lambda expressions when such a lambda exists at the same position.

In some cases, when a call to a swing listener method as
"actionPerformed" for example is too difficult to find precisely
(through addActionListener), a post application procedure is done with a
specific algorithm searching for calls to constructors of these listener
classes. Then a link to the method can be created.

#### Swing

``` java
public class BookingPanel extends javax.swing.JPanel {
    
    private javax.swing.JButton btnSearch;

    private void initComponents() {
       btnSearch = new javax.swing.JButton();
       btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
    }
}
```

![](../images/669253920.png)

``` java
public class PanneauEditProfilOuvrage extends JPanel
{
  private Box creerToolBar(TablePaletteFiltre table)
  {
    Action action = new ActionMonter(table);
    JButton btn = new JButton(action);
  }
}
```

![](../images/669253919.png)

Case of a link found by the post application procedure (with calls to
constructors):

``` java
public class ASTFrame extends JFrame {
    public ASTFrame(String lab, AST r) {
        super(lab);
        JTreeASTPanel tp = new JTreeASTPanel(new JTreeASTModel(r), null);
   }
}
```
![Alt text](../images/669253918.png)

#### JavaFX

##### Case 1

generic_formulaire.fxml

``` java
<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.control.Button?>
<?import javafx.scene.layout.BorderPane?>
<?import javafx.scene.layout.HBox?>

<BorderPane fx:id="borderPane" xmlns="http://javafx.com/javafx/8.0.171"
            xmlns:fx="http://javafx.com/fxml/1"
            fx:controller="fr.insee.com.controller.formulaires.GenericFormulaireController">
    <bottom>
        <HBox alignment="CENTER" spacing="50.0">
            <children>
                <Button fx:id="annulerBtn" alignment="CENTER" mnemonicParsing="false"
                        onAction="#close" text="Annuler"/>
            </children>
        </HBox>
    </bottom>

</BorderPane>
```

GenericFormulaireController.java

``` java
package fr.insee.com.controller.formulaires;

import java.net.URL;
import java.util.ResourceBundle;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.fxml.Initializable;

@Controller
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GenericFormulaireController implements Initializable {
   @FXML
   public void close() {
      ((Stage) borderPane.getScene().getWindow()).close();
   }
}
```

![](../images/669253917.png)

"fx:controller" gives the controller java class and "onAction="#close""
the controller java method. A link to a method is searched for whenever
an attribute value is found whose text starts with "#".

##### Case 2

Same as "Case 1", but without "fx:controller" in the fxml file. In this case, if a .java file exists with the same name that the .fxml
file, in the same directory, then the class present in the java file is taken as the controller.

##### Case 3

The link between the fxml file and the controller may be done in java
code in the application code:

``` java
package com.jenkov.javafx.fxml;
 
import javafx.application.Application;
 
public class FXMLExample extends Application{
 
    public static void main(String[] args) {
        launch(args);
    }
 
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
 
        MyFxmlController controller = new MyFxmlController();
        controller.setValue("New value");
        loader.setController(controller);
 
        File fxmlFile = new File("assets/fxml/hello-world.fxml");
        URL fxmlUrl = fxmlFile.toURI().toURL();
        loader.setLocation(fxmlUrl);

        Scene scene = new Scene(vbox);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
```

The 2 statements "loader.setController(controller);" and
"loader.setLocation(fxmlUrl);" make the link between fxml file and
controller.

##### Case 4

The link between the fxml file and a method may be done in java code
through the "fx:id" present in the .fxml file.

dash.fxml

``` java
<?xml version="1.0" encoding="UTF-8"?>

<?import com.jfoenix.controls.*?>
<?import javafx.geometry.*?>
<?import javafx.scene.control.*?>
<?import javafx.scene.image.*?>
<?import javafx.scene.layout.*?>
<?import javafx.scene.text.*?>

<AnchorPane fx:id="basePane" prefHeight="733.0" prefWidth="1119.0" styleClass="base_pane" stylesheets="@style.css" xmlns="http://javafx.com/javafx/10.0.2-internal" xmlns:fx="http://javafx.com/fxml/1" fx:controller="Gramophy.dashController">
   <children>
      <VBox fx:id="sidePane" alignment="TOP_CENTER" prefHeight="686.0" prefWidth="217.0" styleClass="side_pane" AnchorPane.bottomAnchor="86.0" AnchorPane.leftAnchor="0.0" AnchorPane.topAnchor="0.0">
         <VBox spacing="10.0" VBox.vgrow="ALWAYS">
            <children>
               <Label fx:id="libraryButton" styleClass="label_green" text=" Library">
               </Label>
            </children>
         </VBox>
      </VBox>
   </children>
</AnchorPane>
```

dashController.java

``` java
package Gramophy;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class dashController implements Initializable {

    @FXML
    public Label libraryButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        libraryButton.setOnMouseClicked(event -> switchPane(2));
    }
}
```

A handler is created for the parameter of the "setOnMouseClicked" call.
A link is created from the JavaFX view to the handler and from the handler to the corresponding Java lambda.
The link which was created by JEE analyzer from the parent "initialize" to the Java lambda is deleted, as it is replaced by the new link.

![](../images/image.png)

##### Case 5

Handlers are defined directly in the java application, and not linked to
a view.

``` java
package com.jenkov.javafx.splitmenubutton;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

public class SplitMenuButtonExample extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        MenuItem choice2 = new MenuItem("Choice 2");

        choice2.setOnAction((e)-> {
            System.out.println("Choice 2 selected");
        });

        button.getItems().addAll(choice1, choice2, choice3);

        VBox vBox = new VBox(button);
        Scene scene = new Scene(vBox, 960, 600);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
```

![](../images/669253915.png)
  

``` java
package com.jenkov.javafx.splitmenubutton;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

public class DragAndDropExample extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Circle circle2 = createCircle("#00ffff", "#88ffff",300);
        circle2.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
            }
        });
    }

}
```
![Alt text](../images/image2.png)

#### Transactions

A CAST Transaction Configuration Center .tccsetup file is provided
defining one transaction entry point for all Swing/SWT/JFace UI/JavaFX
View/JavaFX Application objects:

![](../images/669253914.png)

Note that the [JEE Analyzer](../../com.castsoftware.jee/) extension also provides
Java Swing transaction entry points and these overlap with the
configuration provided in the Java Swing extension. CAST recommends
disabling the Java Swing transaction entry points provided by the JEE
Analyzer extension on a case by case basis, where conflicting results
are produced.

![](../images/669253913.jpg)

## In what situation should you install this extension?

If your JEE application source code uses Swing library (javax.swing.\*), you should install this extension.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Console. If you need to use it, should manually install the
extension using the Application -
Extensions interface:

![](../images/669253912.jpg)
