---
title: "REST Service Calls for Java - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.service

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for some web services calls from Java.

## Supported REST client libraries

| Type | API |
|---|---|
| Native | java.net.URLConnection<br>java.net.HttpURLConnection  |
| JAX-RS | javax.ws.rs.client.SyncInvoker <br>javax.ws.rs.client.AsyncInvoker<br>javax.ws.rs.client.ClientBuilder<br>javax.ws.rs.client.Client<br>javax.ws.rs.client.WebTarget<br>javax.ws.rs.client.Invocation<br>javax.ws.rs.client.Invocation.Builder<br>javax.ws.rs.core.UriBuilder<br>jakarta.ws.rs.client.SyncInvoker<br>jakarta.ws.rs.client.AsyncInvoker<br>jakarta.ws.rs.client.ClientBuilder<br>jakarta.ws.rs.client.Client<br>jakarta.ws.rs.client.WebTarget<br>jakarta.ws.rs.client.Invocation<br>jakarta.ws.rs.client.Invocation.Builder<br>org.jboss.resteasy.client.jaxrs.ResteasyClient<br>org.jboss.resteasy.client.jaxrs.ResteasyWebTarget<br>org.jboss.resteasy.client.ClientRequest<br>com.sun.jersey.api.client.Client<br>com.sun.jersey.api.client.WebResource<br>org.glassfish.jersey.client.JerseyClient  |
| Spring | org.springframework.web.client.RestTemplate<br>org.springframework.web.util.UriComponentsBuilder<br>org.springframework.web.util.UriComponents<br>org.springframework.web.util.HtmlUtils<br>org.springframework.social.support.URIBuilder<br>org.springframework.web.reactive.function.client.WebClient  |
| Apache | org.apache.commons.httpclient.HttpClient<br>org.apache.cxf.jaxrs.client.WebClient<br>org.apache.http.client.HttpClient<br>org.apache.http.impl.client.CloseableHttpClient<br>org.apache.http.impl.nio.client.CloseableHttpAsyncClient<br>org.apache.http.client.utils.URIBuilder<br>org.apache.wink.client.Resource<br>org.apache.wink.client.RestClient  |
| Vert.x | io.vertx.core.http.HttpClientRequest<br>io.vertx.ext.web.client.HttpRequest<br>io.vertx.ext.web.client.WebClient  |
| okhttp | com.squareup.okhttp.OkHttpClient<br>okhttp3.OkHttpClient  |
| Other | org.resthub.web.Client<br>org.springframework.cloud.openfeign.FeignClient<br>org.springframework.cloud.netflix.feign.FeignClient<br>feign.Feign<br>feign.RequestLine<br>retrofit2.http.DELETE<br>retrofit2.http.GET<br>retrofit2.http.PATCH<br>retrofit2.http.POST<br>retrofit2.http.PUT  |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

The REST Service Calls for Java extension requires
that ≥ 1.2.8-funcrel of the [JEE Analyzer](../../com.castsoftware.jee/) is also
installed and used in order to ensure the most complete set of results.
This dependency with the [JEE Analyzer](../../com.castsoftware.jee/) is not
automatically handled when downloading the REST Service Calls for
Java extension via CAST Extension Downloader, CAST Server Manager or
AIP Console, therefore you must MANUALLY download and install the
[JEE Analyzer](../../com.castsoftware.jee/) before starting an analysis.

## What results can you expect?

### Features

This extension analyzes web service calls made from Java code through
classic fluent APIs. For example with com.sun.jersey:

``` java
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


class Main
{
    public static void main(String[] args) {
        Client client = Client.create();

        WebResource webResource = client
                .resource("http://localhost:8080/RESTfulExample/rest/json/metallica/get");

        ClientResponse response = webResource.accept("application/json")
                .get(ClientResponse.class);

    }

}
```

![](../images/401670354.png)

### Client side load balancing

Usage of
[org.springframework.cloud.client.loadbalancer.LoadBalancerClient](https://www.javadoc.io/doc/org.springframework.cloud/spring-cloud-commons/1.2.1.RELEASE/org/springframework/cloud/client/loadbalancer/LoadBalancerClient.html)
allows to choose a service to call so impacts the URL called:

``` java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.ServiceInstance;
import java.net.URI;

class Main
{
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    void f()
    {
        RestTemplate client ;
        ServiceInstance serviceInstance = loadBalancerClient.choose("user-auth");
        String path = serviceInstance.getUri().toString() + "/oauth/token";
        client.put(path, client);
    }
}
```

![](../images/497287237.png)

### Feign

Feign is a declarative web service client which makes writing web
service clients easier. Native sample: "RequestLine" annotation
indicates the presence of feign web service:

``` java
interface GitHub {
  @RequestLine("GET /repos/{owner}/{repo}/contributors")
  List<Contributor> contributors(@Param("owner") String owner, @Param("repo") String repo);
}
 
public static class Contributor {
  String login;
  int contributions;
}
 
public class MyApp {
  public static void main(String... args) {
    GitHub github = Feign.builder()
                         .decoder(new GsonDecoder())
                         .target(GitHub.class, "https://api.github.com");
   
    // Fetch and print a list of the contributors to this library.
    List<Contributor> contributors = github.contributors("OpenFeign", "feign");
    for (Contributor contributor : contributors) {
      System.out.println(contributor.login + " (" + contributor.contributions + ")");
    }
  }
}
```

![](../images/412221590.jpg)

"FeignClient" annotation associated with "RequestMapping" and
"GetMapping" annotations indicates the presence of feign web service:

``` java
import java.util.List;
 
@FeignClient(
        name = "${client.name}",
        path= "${client.path}",
        configuration = FeignConfiguration.class
)
public interface ClientConfiguration {
 
    @RequestMapping(method = RequestMethod.POST, value = "/services/clients")
    List<SupplierTransitionDTO> getData(@RequestBody List<String> codes) throws FeignException;
}
```

![](../images/464945204.jpg)

``` java
@FeignClient(name = "Client", url = "${build.api.url}", path = "/api/v1")
public interface ClientConfiguration {

    @GetMapping("/sites/{site-id}/clients")
    ConsentResponse getConfiguration(
            @PathVariable("site-id") String siteId,
            @RequestParam("types") List<ConfigType> types);
}
```

![](../images/464945203.jpg)

### Retrofit2.http

With Retrofit 2, endpoints are defined inside an interface using special retrofit annotations to encode details about the parameters and request method. Annotations like "GET", "POST", "PUT", "DELETE" and "PATCH", for example:

``` java
import retrofit2.http.GET;

public interface ExampleGetService {
    @GET("getservice.example")
    Single<ExampleApi> getMode();
}
```

![](../images/412221589.jpg)

### Objects

| Icon | Description |
|---|---|
| ![](../images/401670349.png) | Delete Resource Service |
| ![](../images/401670350.png) | Get Resource Service |
| ![](../images/401670351.png) | Post Resource Service |
| ![](../images/401670352.png) | Put Resource Service |
| ![](../images/401670352.png) | Patch Resource Service |

### Rules

None.

### Limitations

-   When the code uses a custom URL builder, we cannot evaluate the URL.
-   When the standard java API is encapsulated in a custom method where
    the "http method" (put, get, post, delete) is passed as a parameter,
    the dynamic evaluation will generally fail.

``` java
class MyHttpCall
{
   void execute(String url, String method)
   {
      Client client = ClientBuilder.newClient();
      WebTarget myResource = client.target(url);

      if (method == "GET")
      {
          String response = myResource.request(MediaType.TEXT_PLAIN).get(String.class);
      }
      else if (method == "PUT")
      {
          myResource.request(MediaType.TEXT_PLAIN).put(null);
      }
         
   }

}
```

-   Manual creation of feign clients is not supported
