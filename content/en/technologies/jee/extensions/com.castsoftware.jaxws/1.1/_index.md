---
title: "JAX-WS - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.jaxws

## What's new?

See [Release Notes](rn/).

## Description

In what situation should you install this extension?

The main purpose of this extension is to enable linking client side
requests to the server side services that use JAX-WS. If your
JEE application contains source code which uses JAX-WS (JSR
224) and you want to view these object types and their links with
other objects, then you should install this extension. 

![](../images/669254058.png)

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The JAX-WS extension requires that the
following other CAST extensions are also installed:

- [Universal linker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest) (internal technical extension).

Note that when using the CAST Extension Downloader to download the
extension and the Manage Extensions interface in CAST Server
Manager to install the extension, any dependent extensions
are automatically downloaded and installed for you. You do not need
to do anything.

## Features

### Annotations 

This extension handles JAX-WS web services (particularly
SOAP services) used in JEE applications. JAX-WS contains a collection of
annotations that enables the definition of the web service contract
directly inside the java code:

-   JAX-WS Service is basically defined by a javax.jws.WebService
    (@WebService) annotation set on top of a class. This
    annotation may also be set on top of an interface, in this case, the
    interface will be the "Service Endpoint Interface" and no new web
    service will be created during the analysis

-   An operation represents an action that can be triggered by a client
    application. It is represented by an object called "SOAP Java
    Operation". One operation represents a Java method of a
    @WebService class that is an annotation with @WebMethod. 

-   A port type represents a collection of operations, it is represented
    by an object called "SOAP Java Port Type" which is a child of
    the java file containing the class annotated by @WebService.

-   The JAX-WS Extension also handles the
    annotations @WebServiceClient and @WebEndpoint. Two
    different types of object are created to represent these items:
    "SOAP Java Client" and "Soap Client end point". Each web end
    point contains a list of operations called "SOAP Java Client
    Operation" and they represent the operations that can be remotely
    invoked on the server offering the web service.

For example, the following code:

CalculatorImplCeiling.java

``` java
@WebService(
 serviceName="CalculatorServiceCeiling",
 portName="CalculatorPort",
 name="CalculatorCeiling",
 endpointInterface="com.castsoftware.ws.Calculator",
 targetNamespace="http://ws.castsoftware.com/")
public class CalculatorImplCeiling implements Calculator {
@Override
 public float add(int x, int y) {
 return x+y;
 }
}
```

Calculator.java

``` java
@WebService()
public interface Calculator {
 @WebMethod(operationName="addTwoIntegers")
 float add(int x, int y);
}
```

Will generate:

![](../images/669254057.png)

### Support for Apache CXF 

The Apache CXF API offers a mechanism where the web service is
implemented without using annotations. It is instead specified inside an
XML file (cxf-context.xml file). This is in fact a kind of integration
with Spring. This extension will handle JAX-WS seb services generated
using the Apache CXF framework.

A JAX-WS web service implemented by a Spring bean defined in a Spring
XML configuration file and declared via the Apache CXF should be
correctly detected and created with this extension. All expected
operations and links will be created.

For example, the following code:

web.xml

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app version="2.5" xmlns="http://java.sun.com/xml/ns/javaee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd">

    <display-name>Product Service Web Service</display-name>

    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>/WEB-INF/beans-cxf.xml</param-value>
        <!-- <param-value>/WEB-INF/mvc-dispatcher-servlet.xml</param-value> -->
    </context-param>
```

beans-cxf.xml

``` xml
<jaxws:endpoint id="productEndpoint"
        implementor="com.spring_cxf.ProductServiceImpl"
        address="/products" />
```

ProductServiceImpl.java

``` java
package com.spring_cxf;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    private ProductServiceMockDaoImpl productServiceMockImpl;

    public void setProductServiceMockImpl(ProductServiceMockDaoImpl productServiceMockImpl) {
        this.productServiceMockImpl = productServiceMockImpl;
    }
    public Product getProduct(int productId) {

        return productServiceMockImpl.getProduct(productId);
    }

    public List<Product> getAllProducts() {
        return productServiceMockImpl.getAllProducts();
    }

}
```

will generate:

![](../images/669254056.png)

### Support for Server Tag in CXF web service

Apache CXF allows the creation of jaxws:server endpoints based
on Server and ServerBeans tags defined in
the applicationContext.xml file. The applicationContext.xml file
contains the mapping to the implementor class. The implementor class
contains the web service operations definitions. This extension is
capable of handling these scenarios and is able to create the expected
links and objects.

In the example below,
the serviceBean is "#administration" and administration is
mapped to the implementing
class cpp.administrationGestionnaire.Administration. The required
objects and links for the class Administration should be created
during an analysis.

![](../images/669254055.jpg)

This will generate the following objects and links:

![](../images/669254054.png)

### Support of Handlerchain 

This extension handles the links to the close, handleMessage and
handleFault methods which will be called for the request or
response. @HandlerChain annotation receives as argument the path to the
xml file where the handle class name is specified (we don't support
paths given as URLs). The class containing the handler methods is
identified in the handler file and the respective call-links are created
from the operations to the methods.

For example, the following code:

handler-chain.xml

``` xml
<handler-chains xmlns:javaee="http://java.sun.com/xml/ns/javaee">
    <handler-chain>
        <handler>
            <handler-name>com.cast.handler.MacAddressValidatorHandler</handler-name>
            <handler-class>com.cast.handler.MacAddressValidatorHandler</handler-class>
        </handler>
    </handler-chain>
</handler-chains>
```

MacAddressValidatorHandler.java

``` java
public class MacAddressValidatorHandler implements SOAPHandler<SOAPMessageContext>{

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        return true;
    }
    @Override
    public boolean handleFault(SOAPMessageContext context) {
        System.out.println("Server : handleFault()......");
        return true;
    }
    @Override
    public void close(MessageContext context) {
        System.out.println("Server : close()......");
    }
}
```

ServerInfo.java

``` java
@WebService
@HandlerChain(file="handler-chain.xml")
public class ServerInfo{

    @WebMethod
    public String getServerName() {
        
        return "Cast server";
        
    }
    @WebMethod
    public String getServerAddress() {
        
        return "Cast server Address";
    }
}
```

will generate:

![](../images/669254049.png)

### Support for Jax-RPC

The analyzer inspects webservices.xml and ejb_jar.xml in search of SOAP
operations configured by Jax-RPC framework.

#### Support for client-side Jax-RPC libraries

| Service | Library |
|---|---|
| Jax-RPC | <details><summary>javax.xml.rpc.Call</summary>javax.xml.rpc.Call.invoke<br>javax.xml.rpc.Call.invokeOneWay</details><details><summary>jakarta.xml.rpc.Call</summary>jakarta.xml.rpc.Call.invoke<br>jakarta.xml.rpc.Call.invokeOneWay</details><details><summary>org.apache.axis.client.Call</summary>org.apache.axis.client.Call.invoke<br>org.apache.axis.client.Call.invokeOneWay</details><details><summary>org.apache.soap.rpc.Call</summary>org.apache.soap.rpc.Call.invoke</details><details><summary>org.jboss.axis.client.Call</summary>org.jboss.axis.client.Call.invoke<br>org.jboss.axis.client.Call.invokeOneWay</details><details><summary>com.sun.xml.rpc.client.dii.BasicCall</summary>com.sun.xml.rpc.client.dii.BasicCall.invoke<br>com.sun.xml.rpc.client.dii.BasicCall.invokeOneWay</details> |

HelloClient.java
```java

public class HelloClient {      

    public static submit(SubmitRequest submitRequest) throws Exception{
        
        org.apache.axis.client.Call _call;
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://sample_test/AsgnMgmt/submit");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("submit"));

        setRequestHeaders(_call);
        setAttachments(_call);
        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {submitRequest});
    }
```
will generate:

![](../images/669254059.png)

#### Unknown Client Operation Object

SoapClient.java
```java

import org.apache.soap.rpc.Call;
import org.apache.soap.rpc.Response;


public class SoapClient {
    protected static String url= "http://example.com/apache-soap-service";

    public void invokeUrlWithParam(String url, String methodName, String serviceName) throws Exception {
        URL ur = new URL(url);
        Call call = new Call();
        // ...
        call.setMethodName(methodName);
        // ...
        Response resp;
        resp = call.invoke(ur,"");
        System.out.println("Response for "+resp);
    }
}
```
will generate:

![](../images/669254060.png)

But when the SOAP method name is resolved, but not the porttype, a regular SOAP Operation Call object will be created. The name of the object, however, will be incomplete '?.<methodName>'.

```java
    public void invokeUrlWithOperation() throws Exception {

        Call call = new Call();
        Header header = new Header();
        call.setHeader(header);
		call.setTargetObjectURI( "" ) ;
		call.setMethodName( "orderStatus" );
		call.setEncodingStyleURI("http://example.com/apache-soap-service/profile");
		Response resp;
		String uri = "profile";
		resp = call.invoke(new URL(url),uri);
		System.out.println("Response for "+resp);
    }
```
will generate:

![](../images/669254062.png)

### Support for Spring-WS
Spring-WS provides a client-side web service API that allows for
consistent, XML-driven access to web service. The WebServiceTemplate is
the core class for client-side web service access in Spring-WS. It
contains methods for sending Source objects, and receiving response
messages as either Source or Result. This extension will identify
client-side services using WebServiceTemplate Methods that are used to
invoke the web service.

Note: We need wsdl file to create the SOAP Operation Call object. For marshalSendAndReceive we are able to resolve the operation name but for others three APIs we are currently creating unknown object.

#### Support for client-side Spring-WS libraries

| Service | Library |
|---|---|
| Spring WS | <details><summary>org.springframework.ws.client.core.WebServiceTemplate</summary>org.springframework.ws.client.core.WebServiceTemplate.marshalSendAndReceive<br>org.springframework.ws.client.core.WebServiceTemplate.sendAndReceive<br>org.springframework.ws.client.core.WebServiceTemplate.sendSourceAndReceive<br>org.springframework.ws.client.core.WebServiceTemplate.sendSourceAndReceiveToResult<br>org.springframework.ws.client.core.WebServiceTemplate.doSendAndReceive</details><details><summary>org.springframework.ws.client.core.WebServiceOperations</summary>org.springframework.ws.client.core.WebServiceOperations.marshalSendAndReceive<br>org.springframework.ws.client.core.WebServiceOperations.sendAndReceive<br>org.springframework.ws.client.core.WebServiceOperations.sendSourceAndReceive<br>org.springframework.ws.client.core.WebServiceOperations.sendSourceAndReceiveToResult</details>  |

GetFlights.java
```java
public void getFlights() {

    GetFlightsRequest getFlightsRequest = new GetFlightsRequest();
    getFlightsRequest.setFrom("AMS");
    getFlightsRequest.setTo("VCE");
    XMLGregorianCalendar departureDate = null;

    getFlightsRequest.setDepartureDate(departureDate);
    GetFlightsResponse response = null;
    try {
        response = (GetFlightsResponse) getWebServiceTemplate().marshalSendAndReceive(getFlightsRequest);
    } catch (Exception e) {
        throw new RuntimeException(e);
    }
}
```
will generate

![](../images/669254063.png)

Case 1:
In some case for resolving operation name we need to check resolved class annotation **javax.xml.bind.annotation.XmlRootElement/jakarta.xml.bind.annotation.XmlRootElement** and check for **'name'** attribute if present otherwise class name needs to be considered.
The operation name can be found in wsdl file if present and will create SOAP Operation Call object otherwise Unknown object.

```java
public void run() throws Exception{
    StringResult result = new StringResult();
    
    String requestString = "<ns2:GetPromiseRecommendationRequest "
            + "</ns2:PromiseRecommendationRequest></ns2:GetPromiseRecommendationRequest>";

    GetPromiseRecommendationRequest request = (GetPromiseRecommendationRequest)this.marshaller.unmarshal(new StringSource(requestString));
    
    this.marshaller.marshal(request, result);
    
    GetPromiseRecommendationResponse response = (GetPromiseRecommendationResponse)this.template.marshalSendAndReceive(request);
    
    result = new StringResult(); 
    this.marshaller.marshal(response, result);
}
```
```java
@XmlRootElement(name = "GetPromiseRecommendationRequest", namespace = "http://example.com/namespaces/container/public/promiserecommender.xsd")
public class GetPromiseRecommendationRequest {

    @XmlElement(name = "WSHeader", namespace = "http://example.com/commonheader/v3", required = true)
    protected WSHeader wsHeader;
    @XmlElement(name = "PromiseRecommendationRequest", namespace = "http://eexample.com/namespaces/container/public/promiserecommender.xsd", required = true)
    protected PromiseRecommendationRequest promiseRecommendationRequest;
```

#### @Endpoint and @PayloadRoot or @SoapAction annotations

The classes where SOAP web service requests are handled are annotated by
@Endpoint. The analyzer inspects these classes in search of further
@PayloadRoot or @SoapAction-annotated methods. This annotation maps the method to the root element of the request payload. It should be noted that typical
setup of Spring Web Service configurations is performed from initial
.xsd schema files to generate .java files and stubs as well as contract
.wsdl files. These .wsdl files are generated following some customizable
naming conventions (using for example jsxb2-maven-plugin). The analyzer
directly searches the corresponding SOAP web service operations exposed
by directly inspecting the nearby .wsdl files in the analysis unit. Thus
the presence of .wsdl files (as it is usually the case) in the source
code is necessary for the analyzer to correctly interpret SOAP web
service operations based on the @PayloadRoot or @SoapAction annotation.

Consider the below example code:

**Endpoint and PayloadRoot**

``` java
// Source: http://justcompiled.blogspot.com/2010/09/building-web-service-with-spring-ws.html
package com.live.order.service.endpoint;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

@Endpoint
public class CthSimulator {

    @Autowired
    private ObjectFactory objectFactory;
    
    @PayloadRoot(localPart = "CompleteTaskRequest", namespace = "http://example.com/cth")
    @ResponsePayload
    public  JAXBElement<CompleteTaskResponseType> completeTask(@RequestPayload JAXBElement<CompleteTaskRequestType> request) throws WSException{
    	
```

Together with an excerpt of the .wsdl file, note the connection between
the localPart and the name of the input message

``` xml
	<wsdl:portType name="TaskPortType">	
		<wsdl:operation name="completeTask">
			<wsdl:input message="tns:completeTaskRequestMsg" />
			<wsdl:output message="tns:completeTaskResponseMsg" />
			<wsdl:fault message="tns:WSException" name="WSException" />
		</wsdl:operation>
        ...
    </wsdl:portType>
```

The analyzer will create a SOAP Java Operation object (with fullname
TaskPortType.completeTask) together with a link to the handler method
completeTask.

![](../images/669254052.png)

**Endpoint and SoapAction**
```java
@Endpoint
public class TicketAgentEndpoint {

  // map a message to this endpoint based on the SOAPAction
  @SoapAction(value = "http://example.com/TicketAgent/listFlights")
  @ResponsePayload
  public JAXBElement<TFlightsResponse> listFlights(
      @RequestPayload JAXBElement<TListFlights> request, MessageContext messageContext) {
    // access the SOAPAction value
    WebServiceMessage webServiceMessage = messageContext.getRequest();
    SoapMessage soapMessage = (SoapMessage) webServiceMessage;

    ObjectFactory factory = new ObjectFactory();
    TFlightsResponse tFlightsResponse = factory.createTFlightsResponse();
    tFlightsResponse.getFlightNumber().add(BigInteger.valueOf(101));

    return factory.createListFlightsResponse(tFlightsResponse);
  }
}
```
Together with an excerpt of the .wsdl file, note the connection between
the soapAction and the name of the input message
``` xml
  <wsdl:portType name="TicketAgent">
    <wsdl:operation name="listFlights">
      <wsdl:input message="tns:listFlightsRequest" />
      <wsdl:output message="tns:listFlightsResponse" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="TicketAgentSoap" type="tns:TicketAgent">
    <soap:binding style="document"
      transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="listFlights">
      <soap:operation soapAction="http://example.com/TicketAgent/listFlights" />
      <wsdl:input>
        <soap:body parts="body" use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body parts="body" use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
```

The analyzer will create a SOAP Java Operation object (with fullname
TicketAgent.listFlights) together with a link to the handler method
listFlights.

![](../images/669254065.png)

A warning note: the .wsdl file itself will contain a WSDL Operation
object that can lead to confusion:

![](../images/669254051.png)

This WSDL Operation, though related, does not belong to
com.castsoftware.jaxws, and in contrast to SOAP Java Operation objects,
it is not expected to participate in transactions, and thus only
inter-techno links are expected to be found for the latter (via the
web-service linker at application analysis level).

### Support for Jakarta/Javax SoapMessage
The SOAP specification does not provide a programming model or even an API for the construction of SOAP messages; it simply defines the XML schema to be used in packaging a SOAP message.

| Servce | Library |
|---|---|
| Jakarta/Javax Soap | <details><summary>javax.xml.soap.SOAPConnection</summary>javax.xml.soap.SOAPConnection.call</details><details><summary>jakarta.xml.soap.SOAPConnection</summary>jakarta.xml.soap.SOAPConnection.call</details> |

WebServiceCodeSample.java
```java
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;

public final class WebServiceCodeSample {
    private static final String WEBSERVICE_SECURE_URL =
    "https://technet.rapaport.com/webservices/prices/rapaportprices.asmx";


    private String login( final String username, final String password) throws SOAPException {
        final SOAPMessage soapMessage = getSoapMessage();
        final SOAPBody soapBody = soapMessage.getSOAPBody();

        soapMessage.saveChanges();

        final SOAPConnection soapConnection = getSoapConnection();
        final SOAPMessage soapMessageReply = soapConnection.call(soapMessage,WEBSERVICE_SECURE_URL);
        final String textContent = soapMessageReply.getSOAPHeader().getFirstChild().getTextContent();

        soapConnection.close();

        return textContent;
    }

}
```

will generate

![](../images/669254064.png)

## What results can you expect?

-   The extension is shipped with a set of CAST Transaction
    Configuration Center Entry Points, specifically related
    to JAX-WS Please see [CAST Transaction Configuration Center
    (TCC) Entry Points](#JAXWS1.1-tcc) for more information about this.
-   Once the analysis/snapshot generation has completed, HTTP
    API transaction entry points will be available for use when
    configuring the CAST Transaction Configuration Center.

### Objects

| Icon | Object Type |
|---|---|
| ![](../images/669254043.png) | SOAP Java Web Service |
| ![](../images/669254042.png) | SOAP Java Port Type |
| ![](../images/669254044.png) | SOAP Java Operation |
| ![](../images/669254044.png) | Java SOAP Operation Call |
| ![](../images/669254061.png) | Java SOAP Unknown Operation Call |
| ![](../images/669254043.png) | SOAP Java Client |
| ![](../images/669254042.png) | SOAP Client end point |
