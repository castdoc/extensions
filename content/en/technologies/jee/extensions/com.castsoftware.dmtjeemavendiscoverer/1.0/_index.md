---
title: "Maven Project Discoverer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
rnurl: "https://extend.castsoftware.com/api/delta/export/release-notes/format/markdown?id=com.castsoftware.dmtjeemavendiscoverer&major=1&minor=0"
---

***

## Extension ID

com.castsoftware.dmtjeemavendiscoverer

## What's new ?

See [Release Notes](rn/).

## Extension description

This discoverer detects a project on each folder that contains
a pom.xml file except when the type of packaging declared in the pom.xml
is "pom".

### In what situation should you install this extension?

This extension should be used when you are analyzing Maven based JEE
applications. The discoverer will automatically create one
project for every folder that is delivered for analysis containing a
pom.xml file (except when the type of packaging declared in the pom.xml
is "pom") and one corresponding Analysis Unit will then be created for
analysis purposes. 

-   project inheritance and project aggregation is supported
-   the pom.xml must be valid XML and not contain any erroneous
    characters or spaces - you can check the consistency of an XML file
    by opening it in a browser - errors will be shown.

### Resulting source package configuration

-   Java language and environment (1.1 to 8.0).
-   Application root path:
    -   Standard MAVEN path (src/main/webapp) when "war" packaging is
        declared in pom.xml
    -   otherwise folder containing POM
-   Source folders configured in standard maven paths:
    -   src\main\java
    -   src\test\java
-   Libraries according to artifact ids in the pom.xml dependencies
    declaration: JUnit, Log4J, Commons logging, DOM4J, JSF, MX4J,
    Hibernate, Struts, Spring etc.

### Technical information

The discoverer is already embedded in AIP Core. This embedded version of
the extension will not undergo any further updates and instead all
functional changes/customer bug fixes will be actioned in the extension.

## Maven support

| Maven release | Supported |
|---|:-:|
| 3.x | :white_check_mark: |
| 2.x | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

When using Console ≥ 2.7, this extension will be automatically
downloaded and installed when JEE Maven source code is delivered for
analysis. In all other situations, this extension will not be
downloaded and installed automatically, and instead the JEE Maven
discoverer embedded in AIP Core on the Node will be used. If the
extension is required in this situation, it should be downloaded and
installed manually:

![](../images/617185422.jpg)

## Packaging messages

The following messages emitted by the discoverer may appear during the
packaging action:

|         |                                                         |                                                                                    |                                                                                                                                                                                               |
|---------|---------------------------------------------------------|------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Format  | Message ID                                              | Message                                                                            | Remediation                                                                                                                                                                                   |
| Error   | cast.dmt.discover.jee.maven.resources.pomAnalysisFailed | The analysis of POM file %PATH% has failed                                         | Verify the corresponding file. If the file is good, please raise a ticket.                                                                                                                    |
| Warning | cast.dmt.discover.jee.maven.project.superPomParseError  | Error parsing Maven Super POM!                                                     | Technical issue. Please contact Support.                                                                                                                                                      |
| Warning | cast.dmt.discover.jee.maven.project.parseError          | Error parsing Maven project '%FILE_PATH%'                                          | Verify the corresponding file. If the file is good, please raise a ticket.                                                                                                                    |
| Warning | cast.dmt.discover.jee.maven.noPackaging                 | No packaging found in maven file %PATH%                                            | This project will be considered as a jar.                                                                                                                                                     |
| Warning | cast.dmt.discover.jee.maven.missingPomParent            | The maven parent artifact %PARENT% required by the maven artifact %ID% is missing. | Make sure that the pom.xml file of the parent is included in the source code of this package.                                                                                                 |
| Warning | cast.dmt.discover.jee.maven.resources.jarFileNotFound   | The jar file is not present in the maven artifact folder %PROJECT_PATH%            | The maven artifact is ignored to avoid to hide the alert. Please raise a ticket to investigate the issue.                                                                                     |
| Warning | cast.dmt.discover.jee.maven.moreWebService              | More then one webservice file %FILE_PATH% discovered in project.                   | You can either ignore this warning and select the WSDL version manually in the Analysis Unit for that project or you need to reconfigure your project to a single WSDL file (web descriptor). |

## Notes

### \<optional\>true\</optional\>

When a pom.xml file defines a dependency as "optional", a "missing
project" alerts will be raised for the dependencies, even if they are
actually present. These missing project alerts can be ignored. For
example, the following will raise a missing project alert for "junit":

``` xml
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <optional>true</optional>
</dependency>
```

### Dependency versions

When a dependency \<version\> is defined in a \<properties\> tag in the
parent pom.xml, CAST will be able to successfully locate the project.
However, if the \<properties\> tag is then followed by a \<profile\> in
a \<profiles\> tag, CAST is not able to recognize the dependent project
and an undefined variable alert will be raised for this. To solve the
undefined variable alert:

-   reconfigure the pom.xml by removing the \<profile\>
-   manually remediate the alert in the CAST Delivery Manager Tool
