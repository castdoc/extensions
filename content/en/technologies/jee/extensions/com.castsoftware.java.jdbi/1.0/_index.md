---
title: "JDBI - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.java.jdbi

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for JDBI v2 and v3, Fluent API (Core
API) and Declarative API (SQL Object
API). [JDBI](https://jdbi.org/) (Java Database
Interface) is an open source Java library (Apache license) that uses
lambda expressions and reflection to provide a friendlier, higher level
interface than JDBC to access the database.

## In what situation should you install this extension?

If your Java application uses JDBI API to access a relational database.

## Technology support

| Framework | Version |   Supported |
|---|:-:|:-:|
| JDBI      | [v2](http://jdbi.org/jdbi2/apidocs/org/skife/jdbi/v2/package-summary.html) and [v3](https://jdbi.org/) up to [3.39.1](https://github.com/jdbi/jdbi/releases/tag/v3.39.1) | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For JEE applications using JDBI, the extension will be automatically
installed. This is in place since october 2023.

For upgrade, if the Extension Strategy is not set to Auto
update, you can manually install the new release.

## What results can you expect?

### Objects

| Icon | Description | When is this object created ? |
|---|---|---|
| ![](../images/669254100.png) | JDBI SQL Query | an object is created for each SQL query found and resolved in a JDBI method call  |
| ![](../images/unknown_query.png) | JDBI Unknown SQL Query | an object is created for each unresolved SQL query found in a JDBI method call |

### Links

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 19%" />
<col style="width: 9%" />
<col style="width: 62%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: center;">Link Type</th>
<th class="confluenceTh">Caller type</th>
<th class="confluenceTh" style="text-align: center;">Callee type</th>
<th class="confluenceTh">Supported Jdbi Methods and Annotations</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"
style="text-align: center;"><p><strong>callLink</strong></p></td>
<td class="confluenceTd">Java method</td>
<td class="confluenceTd" style="text-align: center;">JDBI SQL Query</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-41962448" class="expand-container">
<div id="expander-control-41962448" class="expand-control"
aria-expanded="true">
v2 APIs
</div>
<div id="expander-content-41962448" class="expand-content">
<div id="expander-1265122093" class="expand-container">
<div id="expander-control-1265122093" class="expand-control"
aria-expanded="true">
Handle APIs
</div>
<div id="expander-content-1265122093" class="expand-content">
<p>org.skife.jdbi.v2.Handle.insert<br />
org.skife.jdbi.v2.Handle.update<br />
org.skife.jdbi.v2.Handle.execute</p>
</div>
</div>
<div id="expander-1194029515" class="expand-container">
<div id="expander-control-1194029515" class="expand-control"
aria-expanded="true">
Batch APIs
</div>
<div id="expander-content-1194029515" class="expand-content">
<p>org.skife.jdbi.v2.Batch.execute<br />
org.skife.jdbi.v2.PreparedBatch.execute<br />
org.skife.jdbi.v2.PreparedBatch.executeAndGenerateKeys</p>
</div>
</div>
<div id="expander-1109233956" class="expand-container">
<div id="expander-control-1109233956" class="expand-control"
aria-expanded="true">
Call APIs
</div>
<div id="expander-content-1109233956" class="expand-content">
<p>org.skife.jdbi.v2.Call.invoke</p>
</div>
</div>
<div id="expander-1130141972" class="expand-container">
<div id="expander-control-1130141972" class="expand-control"
aria-expanded="true">
Script APIs
</div>
<div id="expander-content-1130141972" class="expand-content">
<p>org.skife.jdbi.v2.Script.execute<br />
org.skife.jdbi.v2.Script.executeAsSeparateStatements</p>
</div>
</div>
<div id="expander-216939621" class="expand-container">
<div id="expander-control-216939621" class="expand-control"
aria-expanded="true">
Update APIs
</div>
<div id="expander-content-216939621" class="expand-content">
<p>org.skife.jdbi.v2.Update.execute<br />
org.skife.jdbi.v2.Update.executeAndReturnGeneratedKeys</p>
</div>
</div>
<div id="expander-864274781" class="expand-container">
<div id="expander-control-864274781" class="expand-control"
aria-expanded="true">
Query APIs
</div>
<div id="expander-content-864274781" class="expand-content">
<p>org.skife.jdbi.v2.Query.list<br />
org.skife.jdbi.v2.Query.first</p>
</div>
</div>
</div>
</div>
<div id="expander-12339254" class="expand-container">
<div id="expander-control-12339254" class="expand-control"
aria-expanded="true">
v3 APIs
</div>
<div id="expander-content-12339254" class="expand-content">
<div id="expander-1906721216" class="expand-container">
<div id="expander-control-1906721216" class="expand-control"
aria-expanded="true">
Handle APIs
</div>
<div id="expander-content-1906721216" class="expand-content">
<p>org.jdbi.v3.core.Handle.execute</p>
</div>
</div>
<div id="expander-736131437" class="expand-container">
<div id="expander-control-736131437" class="expand-control"
aria-expanded="true">
Batch APIs
</div>
<div id="expander-content-736131437" class="expand-content">
<p>org.jdbi.v3.core.statement.Batch.execute<br />
org.jdbi.v3.core.statement.PreparedBatch.execute<br />
org.jdbi.v3.core.statement.PreparedBatch.executeAndGetModCount<br />
org.jdbi.v3.core.statement.PreparedBatch.executeAndReturnGeneratedKeys<br />
org.jdbi.v3.core.statement.PreparedBatch.executePreparedBatch</p>
</div>
</div>
<div id="expander-1895671273" class="expand-container">
<div id="expander-control-1895671273" class="expand-control"
aria-expanded="true">
Call APIs
</div>
<div id="expander-content-1895671273" class="expand-content">
<p>org.jdbi.v3.core.statement.Call.invoke</p>
</div>
</div>
<div id="expander-1389587970" class="expand-container">
<div id="expander-control-1389587970" class="expand-control"
aria-expanded="true">
Script APIs
</div>
<div id="expander-content-1389587970" class="expand-content">
<p>org.jdbi.v3.core.statement.Script.execute<br />
org.jdbi.v3.core.statement.Script.executeAsSeparateStatements</p>
</div>
</div>
<div id="expander-79096463" class="expand-container">
<div id="expander-control-79096463" class="expand-control"
aria-expanded="true">
Update APIs
</div>
<div id="expander-content-79096463" class="expand-content">
<p>org.jdbi.v3.core.statement.Update.execute<br />
org.jdbi.v3.core.statement.Update.executeAndReturnGeneratedKeys</p>
</div>
</div>
<div id="expander-337761999" class="expand-container">
<div id="expander-control-337761999" class="expand-control"
aria-expanded="true">
Query APIs
</div>
<div id="expander-content-337761999" class="expand-content">
<p>org.jdbi.v3.core.statement.Query.execute<br />
org.jdbi.v3.core.result.ResultIterable.filter<br />
org.jdbi.v3.core.result.ResultIterable.findFirst<br />
org.jdbi.v3.core.result.ResultIterable.findOne<br />
org.jdbi.v3.core.result.ResultIterable.findOnly<br />
org.jdbi.v3.core.result.ResultIterable.first<br />
org.jdbi.v3.core.result.ResultIterable.forEach<br />
org.jdbi.v3.core.result.ResultIterable.forEachWithCount<br />
org.jdbi.v3.core.result.ResultIterable.iterator<br />
org.jdbi.v3.core.result.ResultIterable.list<br />
org.jdbi.v3.core.result.ResultIterable.map<br />
org.jdbi.v3.core.result.ResultIterable.of<br />
org.jdbi.v3.core.result.ResultIterable.one<br />
org.jdbi.v3.core.result.ResultIterable.reduce<br />
org.jdbi.v3.core.result.ResultIterable.stream<br />
org.jdbi.v3.core.result.ResultIterable.useIterator<br />
org.jdbi.v3.core.result.ResultIterable.useStream<br />
org.jdbi.v3.core.result.ResultIterable.withIterator<br />
org.jdbi.v3.core.result.ResultIterable.withStream</p>
</div>
</div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;">callLink</td>
<td class="confluenceTd">Declarative API annotated Interface method</td>
<td class="confluenceTd" style="text-align: center;">JDBI SQL Query</td>
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-447167999" class="expand-container">
<div id="expander-control-447167999" class="expand-control"
aria-expanded="true">
v2 APIs
</div>
<div id="expander-content-447167999" class="expand-content">
<ul>
<li>@SqlQuery - org.skife.jdbi.v2.sqlobject.SqlQuery</li>
<li>@SqlUpdate - org.skife.jdbi.v2.sqlobject.SqlUpdate</li>
<li>@SqlBatch - org.skife.jdbi.v2.sqlobject.SqlBatch</li>
<li>@SqlCall - org.skife.jdbi.v2.sqlobject.SqlCall</li>
</ul>
</div>
</div>
<div id="expander-1509979585" class="expand-container">
<div id="expander-control-1509979585" class="expand-control"
aria-expanded="true">
v3 APIs
</div>
<div id="expander-content-1509979585" class="expand-content">
<ul>
<li><p>@SqlQuery - org.jdbi.v3.sqlobject.statement.SqlQuery</p></li>
<li><p>@SqlUpdate - org.jdbi.v3.sqlobject.statement.SqlUpdate</p></li>
<li><p>@SqlBatch - org.jdbi.v3.sqlobject.statement.SqlBatch</p></li>
<li><p>@SqlCall - org.jdbi.v3.sqlobject.statement.SqlCall</p></li>
<li><p>@SqlScript - org.jdbi.v3.sqlobject.statement.SqlScript</p></li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;">useLink</td>
<td class="confluenceTd">JDBI SQL Query</td>
<td class="confluenceTd" style="text-align: center;">Table, View</td>
<td rowspan="2" class="confluenceTd">Created by <strong>SQL
Analyzer</strong> when DDL source files are analyzed</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;">callLink</td>
<td class="confluenceTd">JDBI SQL Query</td>
<td class="confluenceTd" style="text-align: center;">Procedure</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;">useLink</td>
<td class="confluenceTd">JDBI SQL Query</td>
<td class="confluenceTd" style="text-align: center;">Missing Table</td>
<td rowspan="2" class="confluenceTd">Created by <strong>Missing tables
and procedures for JEE</strong> extension when the object is not
analyzed.</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;">callLink</td>
<td class="confluenceTd">JDBI SQL Query</td>
<td class="confluenceTd" style="text-align: center;">Missing
Procedure</td>
</tr>
</tbody>
</table>

### Code Examples

#### Fluent API

Fluent API Examples

list ResultIterable API to perform Select operation

Select Operation

``` java
public void whenException_thenTransactionIsRolledBack() {
        Jdbi jdbi = Jdbi.create("jdbc:hsqldb:mem:testDB", "sa", "");
        jdbi.useHandle(handle -> {
            List<Map<String, Object>> list = handle.select("SELECT * FROM PROJECT_14").mapToMap().list();
            assertEquals(0, list.size());
        });
}
```



![](../images/669254099.png)

execute Handle API to perform Insert operation

Insert Operation

``` java
public void whenException_thenTransactionIsRolledBack() {
        Jdbi jdbi = Jdbi.create("jdbc:hsqldb:mem:testDB", "sa", "");
        jdbi.useHandle(handle -> {
            try {
                handle.useTransaction(h -> {
                    h.execute("create table PROJECT_14 (ID IDENTITY, NAME VARCHAR (50), URL VARCHAR (100))");
                    h.execute("INSERT INTO PROJECT_14 (NAME, URL) VALUES ('tutorials', 'https://github.com/eugenp/tutorials')");
                    List<Map<String, Object>> list = handle.select("SELECT * FROM PROJECT_14").mapToMap().list();

                    assertTrue(h.isInTransaction());
                    assertEquals(1, list.size());

                    throw new Exception("rollback");
                });
            } catch (Exception ignored) {}
        });
}
```



![](../images/669254098.png)

createUpdate Update API to perform Update operation

Update Operation

``` java
public void whenParameters_thenReplacement() {
        Jdbi jdbi = Jdbi.create("jdbc:hsqldb:mem:testDB", "sa", "");
        jdbi.useHandle(handle -> {
            Update update = handle.createUpdate("UPDATE PROJECT_10 SET url = 'github.com/spring/REST-With-Spring'  WHERE name = 'REST with Spring'");
            int numRowsUpdated = update.execute();
        });
}
```



![](../images/669254097.png)

createUpdate Update API to perform Delete operation

Delete Operation

``` java
public void whenParameters_thenReplacement() {
        Jdbi jdbi = Jdbi.create("jdbc:hsqldb:mem:testDB", "sa", "");
        jdbi.useHandle(handle -> {            
            Update delete = handle.createUpdate("DELETE FROM PROJECT_10 WHERE NAME = 'tutorials'");
            delete_rows = delete.execute();
            assertEquals(1, delete_rows);
        });
}
```



![](../images/669254096.png)

#### Declarative API

Declarative API Examples

Jdbi SQL Object extension Interface

``` java
package com.jdbi.declartive.users;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.SqlCall;
import com.jdbi.declartive.users.User;

@RegisterBeanMapper(User.class)
public interface UserDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS users (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(50), age INT)")
    void createTable();
    
    @SqlUpdate("UPDATE users SET name = :name, age = :age WHERE id = :id")
    void updateUser(@BindBean User user);

    @SqlUpdate("INSERT INTO users (id, name, age) VALUES (:id, :name, :age)")
    void insert(@BindBean User user);

    @SqlQuery("SELECT * FROM users")
    List<User> getAll();
    
    @SqlCall("{call delete_user(:id)}")
    int deleteUser(int id);
    
    @SqlCall("{call GETUSER(:id)}")
    int getUser(int id);
}
```

JdbiDeclarativeExample class to call JDBI Interface methods

``` java
package com.jdbi.declartive.users;

import org.jdbi.v3.core.Jdbi;

public class JdbiDeclarativeExample {
    public static void main(String[] args) {
        // Create a Jdbi instance and configure it to connect to your database
        Jdbi jdbi = Jdbi.create("jdbc:mysql://localhost:3306/mydatabase", "username", "password");

        // Define your data access object (DAO) interface
        UserDao userDao = jdbi.onDemand(UserDao.class);
        CompanyDao companyDao = jdbi.onDemand(UserDao.class);

        // Use the DAO to perform database operations
        userDao.createTable();
        
        companyDao.createCompanyTable();

        // Insert a new user
        User user = new User(1, "John Doe", 25);
        userDao.insert(user);
        
        Company company = new Company(1,"John_company");
        companyDao.insert_company_details(company);
        
        company = new Company(2,"anu_company");
        companyDao.insert_company_details(company);
        
        companyDao.deleteCompany(1);
        
        user = new User(1, "John Doe", 26);
        userDao.updateUser(user);
        
        user = new User(2, "Dev", 25);
        userDao.insert(user);
        
        userDao.getUser(2);
        
        userDao.deleteUser(2);
        // Retrieve all users
        List<User> users = userDao.getAll();
        for (User u : users) {
            System.out.println(u.getName());
        }
    }
}
```

Select Operation

``` java
@SqlQuery("SELECT * FROM users")
List<User> getAll();
```

![](../images/669254095.png)

Insert Operation

``` java
@SqlUpdate("INSERT INTO users (id, name, age) VALUES (:id, :name, :age)")
void insert(@BindBean User user);
```

![](../images/669254094.png)

Update Operation

``` java
@SqlUpdate("UPDATE users SET name = :name, age = :age WHERE id = :id")
void updateUser(@BindBean User user);
```

![](../images/669254093.png)

Delete Operation

``` java
@SqlCall("{call delete_user(:id)}")
int deleteUser(int id);  
```

![](../images/669254092.png)

Unresolved Case

![](../images/unresolved_example.png)

## Limitations

-   Unknown object will be created if evaluation fails to resolve the
    necessary parameter
