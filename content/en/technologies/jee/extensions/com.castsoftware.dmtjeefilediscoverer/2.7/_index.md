---
title: "JEE File Discoverer - 2.7"
linkTitle: "2.7"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtjeefilediscoverer

## What's new?

See [Release Notes](rn/).

## Extension description

This discoverer detects "projects" (and therefore Analysis Units) based on the presence of `.java` files.

### In what situation should you install this extension?

This extension should be used when the Java/JSP project files such as .project or pom.xml are not delivered with the source code (without these files, the discoverers provided "out of the box" in CAST Imaging Core cannot detect all `.java` files). In summary the extension provides a "catch-all" to ensure that all `.java`/`.jsp` files will be packaged for analysis.

{{% alert color="info" %}}Note that this discoverer also supports `.wsdl`/`.wsdd` files which belong to a path of an already discovered project (i.e. these files will be analyzed). However, if these files are located outside the path of a discovered project path, then they will not cause the creation of a project/Analysis Unit and will therefore be skipped during the analysis.{{% /alert %}}

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Technical information

This extension is designed to detect `.java`/`.jsp` files in the following scenarios:

### Case 1

The package identified in the first `.java` file matches the folder hierarchy. In this situation one project is created by the discoverer as follows:

-   the project name will be the name of the parent folder of the folder containing the main package (i.e. `com`)
-   the root path of the project will be the same as the project name
-   the folder containing the main package (i.e. `com`) will be selectedas the source code

For example, for a `.java` file with the following folder hierarchy `C:\temp\project1\src\com\cast\test.java`, the project will be named `project1`, the root path will be `project1` and the selected folder is `project1/src`.

### Case 2

Similar to Case 1 above, when a sub-folder `src` and a sub-folder `test` are located inside the same folder `project1`, the project created by the discoverer (`project1`) will reference the two folders for the source code: `project1/src `and `project1/test`.

### Case 3

In this situation the `.java` files do not have a package and one project will be created by the discoverer for each `.java` file that is discovered (and not each folder). The project name uses the same name as the discovered `.java` file.

This is known as an **Unnamed Package** and will be recorded as such in the log - see **Unnamed Package** below for more information.

### Case 4

In this situation, the package doesn't match the folder hierarchy (for example, `a.b.c.class1` but the folders `a`and `b` are missing). A warning is displayed and the folder `c` is considered as the main package. The parent folder of `c` is selected for the source code and the grand-parent is used for the project name and root.

This is known as an **Incomplete Package** and will be recorded as such in the log - see **Incomplete Package** below for more information.

### Case 5

Similar to Case 4 above, however, the `.java` file is not located in the correct folder. In this situation (known as an "UnmatchingPackage") the behavior of the discovery is the same as **Case 4**, however, the log file will contain the following to indicate the nature of the discovery:

This is known as an `Unmatching Package` and will be recorded as such in the log - see `Unmatching Package` below for more information.

### Case 6

In this situation, each Java branch is assigned to a separate project, for example:

```text
c:\temp\Project1\src\com\cast\file1.java
c:\temp\Project1\x\y\z\src\com\component\file2.java
```

In the above situation (known as a `ParentProject`), two projects will be created.

This is known as a `Parent Project` and will be recorded as such in the log - see `Parent Project` below for more information.

### About Analysis Unit/Technology dependencies

No dependencies to other Analysis Units/Technologies will be automatically created based on the use of this extension since the "discovery" is based on the presence of `.java` files, rather than Maven or Eclipse projects. As such, you may need to manually create dependencies between Analysis Units to achieve any inter-Analysis Unit
links you require.

## Activation

The JEE File Discoverer will be automatically installed whenever a `.java` file is encountered in your delivered source code. This extension will ensure that the required Analysis Units are created for your source code and that an analysis can proceed without issue.

## Upgrading from previous releases

If you have using an existing release of the extension, (particularly 1.x) with existing Applications and you decide to install JEE File Extension 2.x and use it with these Applications, you will find that a new packaging action on the same source code may produce different packaging results, i.e the list of projects discovered may change.

## Log messages

When packaging your source code and you have activated the JEE File Discoverer, the following messages will be present in the log file to
help you understand the nature of the discovery.

{{% alert color="info" %}}For performance reasons, the extension only scans ONE `.java` file per folder it encounters, therefore it is highly recommended that if a log
message is emitted for a `.java` file in a specific folder, ALL the `.java` files in that folder are manually checked consistency.{{% /alert %}}

### Unnamed Package

- Message type: INFO
- Location: Scan and Discover phases
- Meaning: The package declaration is missing; therefore, the corresponding `.java` file should be considered as a "batch" written in Java.

```text
Java file %FILE_PATH% has no package, and will lead to a specific JEE file project
```

### Invalid Package

- Message type: WARNING
- Location: Scan phase
- Meaning: The package declaration is incorrect; therefore, the corresponding `.java` file is skipped from any Java project.

```text
Java file %FILE_PATH% has an invalid package, and will be skipped
```

This warning corresponds to an invalid declaration in the `.java` file,
for example:

```java
package;

package a.b
```

These invalid declarations are typically found in "scrap" `.java` files, i.e. files which are bundled in the delivery but which are not taken into account during compilation.

### Incomplete Package

- Message type: INFO
- Location: Scan and Discover phases
- Meaning: The package declaration does not match the directory structure; therefore, the discovered project might be incomplete.

```text
Java file %FILE_PATH% has an incomplete package
```

### Unmatching Package

- Message type: INFO
- Location: Scan phase
- Meaning: The package declaration does not at all match the directory structure; therefore, the discovered project might be incomplete.

```text
Java file %FILE_PATH% has an unmatching package
```

### Parent Project

- Message type: INFO
- Location: Discover phase
- Meaning: The folder associated to the project contains sub folders associated with valid (i.e., they are not Invalid Packages, Incomplete Packages or Unmatching Packages) projects; in this situation, some XML and properties resource references are added to the configuration, so that the application root path, which is too large, will not be used as a default by the analyzer to scan the XML and properties files.

```text
Java project %PROJECT_NAME% under %PROJECT_PATH% contains additional Java projects
```