---
title: "jOOQ - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

## Extension ID

com.castsoftware.jooq

## What's new?

See [Release Notes](rn/).

## Description

jOOQ (Java Object Oriented Query), is a library that generates Java
classes based on database tables and allows the creation of type-safe
SQL queries through its fluent API. This extension supports API for the
jOOQ framework, which is responsible for the typical CRUD operations
with the database.

## In what situation should you install this extension?

If your Java application contains CRUD operations with fluent API or
"raw" SQL queries in its source code provided by jOOQ packages and you
want to view these object types and their links, then you should install
this extension.

## Technology support

| Item  | Version  | Supported  | Supported Technology |
|-------|----------|:-:|----------------------|
| [jOOQ](https://www.jooq.org/doc/3.20/manual/) | Up to : 3.20 | :white_check_mark: | Java |

>jOOQ requires minimum of Java 8 to run.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For JEE applications using jOOQ, the extension will be automatically
installed by CAST Console. This is in place since october 2023.

For upgrade, if the Extension Strategy is not set to Auto
update, you can manually install the new release using
the Application - Extensions interface.

## What results can you expect?

### Objects

The following objects are displayed in CAST Enlighten:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
<th class="confluenceTh">When is this object created ?</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669254158.png" /></p>
</div></td>
<td class="confluenceTd"><p>jOOQ SQL Query</p></td>
<td class="confluenceTd">An object is created for each SQL query found
and resolved in a jOOQ DSLContext method call.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669254157.png" /></p>
</div></td>
<td class="confluenceTd"><p>jOOQ Entity</p></td>
<td class="confluenceTd">An object is created for each jOOQ Entity class
found which inherits from Table TableImpl.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669254155.png" /></p>
</div></td>
<td class="confluenceTd"><p>jOOQ Entity Operation</p></td>
<td class="confluenceTd">An object is created for each CRUD operation
performed on Entity.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/unknown_sql_query_icon.png" /></p>
</div></td>
<td class="confluenceTd"><p>jOOQ Unknown SQL Query</p></td>
<td class="confluenceTd">An object is created for each SQL query found
and and the exact query cannot be resolved.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/Unknown_Entity.png" /></p>
</div></td>
<td class="confluenceTd"><p>jOOQ Unknown Entity</p></td>
<td class="confluenceTd">An object is created when entity cannot be resolved.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/UnknownEntity_Operation.png" /></p>
</div></td>
<td class="confluenceTd"><p>jOOQ Unknown Entity Operation</p></td>
<td class="confluenceTd">An object is created when CRUD operation is performed and Entity cannot be resolved.</td>
</tr>
</tbody>
</table>

### Links

The following link types are created by the jOOQ extension, or by other
extensions.

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller type</th>
<th class="confluenceTh">Callee type</th>
<th class="confluenceTh">Methods Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Java Method</td>
<td class="confluenceTd">jOOQ SQL Query, jOOQ Unknown SQL Query</td>
<td class="confluenceTd"><ul>
<li>org.jooq.DSLContext.fetch</li>
<li>org.jooq.DSLContext.execute</li>
<li>org.jooq.DSLContext.query</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">Java method</td>
<td class="confluenceTd">jOOQ Entity Operation, jOOQ Unknown Entity Operation</td>
<td class="confluenceTd"><ul>
<li>org.jooq.ResultQuery.fetch</li>
<li>org.jooq.ResultQuery.fetchOne</li>
<li>org.jooq.ResultQuery.fetchAny</li>
<li>org.jooq.ResultQuery.fetchLazy</li>
<li>org.jooq.ResultQuery.fetchResultSet</li>
<li>org.jooq.ResultQuery.fetchStream</li>
<li>org.jooq.Query.execute</li>
<li>org.jooq.Batch.execute</li>
<li>org.jooq.ResultQuery.stream</li>
<li>org.jooq.Query.executeAsync</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">jOOQ SQL Query</td>
<td class="confluenceTd">Table, View</td>
<td rowspan="2" class="confluenceTd">Created by <strong>SQL
Analyzer</strong> when DDL source files are analyzed</td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">jOOQ SQL Query</td>
<td class="confluenceTd">Procedure</td>
</tr>
<tr class="odd">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">jOOQ Entity Operation</td>
<td class="confluenceTd">Table, View</td>
<td rowspan="2" class="confluenceTd">Created by <strong>WBS</strong>
when DDL source files are analyzed by SQL Analyzer</td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">jOOQ Entity Operation</td>
<td class="confluenceTd">Procedure</td>
</tr>
<tr class="odd">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">jOOQ SQL Query</td>
<td class="confluenceTd">Missing Table</td>
<td rowspan="2" class="confluenceTd">Created by <strong>Missing tables
and procedures for JEE</strong> extension when the object is not
analyzed.</td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">jOOQ SQL Query</td>
<td class="confluenceTd">Missing Procedure</td>
</tr>
<tr class="even">
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><p>jOOQ Entity</p>
</td>
<td class="confluenceTd">Java Class</td>
<td rowspan="2" class="confluenceTd"></td>
</tr>
</tbody>
</table>

## Example code scenarios

### Fluent API

Fluent API Examples

Select Operation

``` java
package com.Jooqexample.jooq;

import static com.JooQDemo.jooq.sample.model.Tables.*;
import com.JooQDemo.jooq.sample.model.tables.records.BikeRecord;
import com.JooQDemo.jooq.sample.model.tables.records.CarsRecord;
import org.jooq.*;
import org.jooq.Record;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.sql.ResultSet;
import java.util.stream.Stream;

@SpringBootTest

class SpringBootJooqApplicationTests {

    @Autowired
    private DSLContext dsl;

    @Test
    void DisplayData() {
        CarsRecord Cars = dsl.selectFrom(CARS).fetchAny();
        System.out.println("Result are -- " + Cars);

        Result<Record> result0 = dsl.select().from(AIR_WAYS).fetch();
        System.out.println("Result are1 -- " + result0);

        Result<Record> result0 = dsl.select().from(CARS).stream();

        Result<Record> result1  = dsl.select().from(CARS).fetch();
        System.out.println("Result are1 -- " + result1);

        Result<BikeRecord> result = dsl.selectFrom(BIKE).fetch();
        System.out.println("Result are -- " + result);

        ResultSet rs1 = dsl.selectFrom(CARS).fetchResultSet();
        System.out.println("Result are -- " + rs1);

        Cursor<Record1<String>> rs2 = dsl.select(CARS.MODEL_NAME).from(CARS).fetchLazy();
        for (Record1<String> record : rs2){
            System.out.println("Result are fetchlazy -- " + record);
        }
        Stream<Record1<String>> rs3 = dsl.select(CARS.MODEL_NAME).from(CARS).fetchStream();

        rs3.forEach(record -> {
            System.out.println("Result are fetchStream -- " + record);
            // ...
        });

        CarsRecord Cars1 = dsl.selectFrom(CARS).where(CARS.MODEL_NAME.eq("Ertiga")).fetchOne();
        System.out.println("Result are -- " + Cars1);



    }
}


    
```

Example With Cars Entity

![](../images/669254154_2.png)![](../images/669254153_2.png)

Example With Bike Entity

![](../images/669254152_2.png)![](../images/669254151_2.png)

Insert Operation

``` java
    void AddData() {
        int insertedrow = dsl.insertInto(CARS).values(2,"TATA","NEXON",2012,100000000).execute();
        System.out.println("Insert Operation Row Affected are -- " + insertedrow);

        int insertedrow_bike = dsl.insertInto(BIKE).values(1,"Bajaj","Avenger",2012,150000).execute();
        System.out.println("Insert Operation Row Affected are -- " + insertedrow_bike);

        int insertedrow_bike2 = dsl.insertInto(BIKE).values(2,"Duke","KTM",2016,150000).execute();
        System.out.println("Insert Operation Row Affected are -- " + insertedrow_bike2);



    }
```

![](../images/669254150_2.png)

Delete Operation

``` java
  void DeleteData(){

        int deleterow = dsl.deleteFrom(CARS).where(CARS.ID.eq(2)).execute();
        System.out.println("Delete Operation Row Affected are -- " + deleterow);

        int deleterow_bike = dsl.deleteFrom(BIKE).where(BIKE.ID.eq(1)).execute();
        System.out.println("Delete Operation Row Affected are -- " + deleterow_bike);

    }
```

![](../images/669254149_3.png)

Update Operation

``` java
	void UpdateData() {
		int updateRow =  dsl.update(CARS).set(CARS.COMPANY_NAME ,"Tesla").where(CARS.ID.eq(2)).execute();
		System.out.println("Update Operation Row Affected are -- " + updateRow);
	}
```

![](../images/669254148_2.png)

Batch Operation

``` java
void execute_batchUpdate() {

    Result<BikeRecord> existingProjects = DSLContext.selectFrom(BIKE).where(BIKE.ID.eq(1)); 

    DSLContext.batchUpdate(existingProjects.stream().collect(Collectors.toList())).execute();

}
```

![](../images/669254147_3.png)

### Raw SQL API

Raw SQL API Example

org.jooq.DSLContext.fetch

``` java
 // Import the necessary jOOQ classes
import org.jooq.impl.DSL;
import org.jooq.*;
import org.jooq.Record;
import static org.jooq.impl.DSL.*;
   
public class JooqSample {

    public static void main(String[] args) {

        String url = "jdbc:mysql://localhost:3306/vehicle";
        String uname = "root";
        String password = "mysql";
        String query = "select * from cars";

        /////////// Creating the connection with database //////////
        try {

            DSLContext context = DSL.using(url, uname, password);

            fetchRecord(context, query);
           

        } catch (Exception e) {
            System.out.println("\n Exception" + e);
        }

    }        


    public static void fetchRecord(DSLContext context, String query) {
        try {

            
            Result<Record> result = context.fetch(query);
            System.out.println("Result are -- " + result);

        } catch (Exception e) {
            System.out.println("\n Exception " + e);
        }

    }

}
```

![](../images/669254146_1.png)![](../images/669254145_1.png)

org.jooq.DSLContext.execute

``` java
        public static void executeRecord(DSLContext context) {
        try {

            
            int rowCount = context.execute("insert into cars values(100,'Hyundai','verna',2016,1500000,3)");
            

            System.out.println("Number of row affected are -- " + rowCount);

        } catch (Exception e) {
            System.out.println("\n Exception " + e);
        }

    }
```

![](../images/669254144_2.png)

org.jooq.DSLContext.query

``` java
    public static void queryRecord(DSLContext context) {
        try {

            
            Query query = context.query("insert into cars values(100,'Hyundai','verna',2016,1500000)");
            

            System.out.println("Number of row affected are -- " + query);

        } catch (Exception e) {
            System.out.println("\n Exception " + e);
        }

    }
```

![](../images/669254143_2.png)

### Unknown SQL Query
``` java
   public static void deleteRecord(DSLContext context) {
	
        try {

            // Build the query
            String new_query = getQuery();
            int rowCount = context.execute(new_query);
            // Loop through the results

            System.out.println("Number of row affected are -- " + rowCount);

        } catch (Exception e) {
            System.out.println("\n Exception " + e);
        }

    }
```
![](../images/unknown_sql_jooq.png)

### Unknown Entity Operation
``` java
    void DisplayData() {
		String table = get_table();
		CarsRecord Cars = dsl.selectFrom(table).fetchAny();
		System.out.println("Result are -- " + Cars);

		Result<Record> result0 = dsl.select().from(AIR_WAYS).fetch();
		System.out.println("Result are1 -- " + result0);

		Result<Record> result0 = dsl.select().from(CARS).stream();

		Result<Record> result1  = dsl.select().from(CARS).fetch();
		System.out.println("Result are1 -- " + result1);

		Result<BikeRecord> result = dsl.selectFrom(BIKE).fetch();
		System.out.println("Result are -- " + result);

		ResultSet rs1 = dsl.selectFrom(CARS).fetchResultSet();
		System.out.println("Result are -- " + rs1);

		Cursor<Record1<String>> rs2 = dsl.select(CARS.MODEL_NAME).from(CARS).fetchLazy();
		for (Record1<String> record : rs2){
			System.out.println("Result are fetchlazy -- " + record);
		}
		Stream<Record1<String>> rs3 = dsl.select(CARS.MODEL_NAME).from(CARS).fetchStream();

		rs3.forEach(record -> {
			System.out.println("Result are fetchStream -- " + record);
			// ...
		});

		CarsRecord Cars1 = dsl.selectFrom(CARS).where(CARS.MODEL_NAME.eq("Ertiga")).fetchOne();
		System.out.println("Result are -- " + Cars1);


	}
```
![](../images/unknown_entity_jooq_1.png)


