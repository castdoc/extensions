---
title: "SAP Hybris - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.sap.hybris

## What's new?

See [Release Notes](rn/).

## In what situation should you install this extension?

If your application source code uses the SAP Hybris framework you
should install this extension.

This extension provides support for the Hybris framework (java package
com.hybris.ymkt.common.http). The analysis of your JEE application
will be enriched with modelling of the HTTP API created with Spring MVC.
This will allow the resolution of transactions starting in a client
application which uses these API (web app for instance) to be traced
into the backend application.

-   The extension creates Hybris HttpRequest services which represent
    end-points of transactions when they are not connected to an
    operation on an ABAP server side (analyzed by the ABAP analyzer).
-   JEE is acting here as the client of an ABAP server.

### Application configuration

In an xml file present in a "resources" folder, we have beans with
property name equal to "oDataService". The extension will focus on these
particular beans, for example:

``` xml
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd
           http://www.springframework.org/schema/aop
           http://www.springframework.org/schema/aop/spring-aop.xsd">
...
    <bean id="bookingDetailsODataService" parent="oDataServiceCRM">
        <property name="rootUrl" value="#{configurationService.configuration.getProperty('crm.odata.booking.url.root')}" />
    </bean>
    <bean id="bookingODataService"
        class="com.company.companyodata.service.impl.BookingODataServiceImpl" parent="AbstractODataService">
        <property name="oDataService" ref="bookingDetailsODataService" />
        <property name="modelService" ref="modelService"/>
    </bean>
....
</beans>
```

In this xml file, we can see that the "bookingODataService" bean has
"oDataService" property ref equal to "bookingDetailsODataService" which
corresponds to another bean whose "rootUrl" property is

``` xml
"#{configurationService.configuration.getProperty('crm.odata.booking.url.root')}".
```

We can also see that this bean is connected to the
"com.company.companyodata.service.impl.BookingODataSeviceImpl" java
class. This url will be the root url of http services that the extension
will create for the class
"com.company.companyodata.service.impl.BookingODataSeviceImpl". This
root url is defined through a property which can be found in a
"project.properties" file - note that the "${CRM_ODATA_URL}" part will
be removed from url:

``` xml
...
crm.odata.booking.url.root=${CRM_ODATA_URL}/ZAPPOINTMENT_PLANNER_SRV/
...
```

### Application analysis

The extension will take into account the following Java code:

``` java
package com.company.companyodata.service.impl;
 
import com.hybris.ymkt.common.http.HttpURLConnectionRequest;
import com.hybris.ymkt.common.http.HttpURLConnectionResponse;
 
public class BookingODataServiceImpl extends AbstractODataService implements BookingODataService
{
    public String createBookingServiceRequest(final CsTicketModel csTicketModel)
            throws IOException, EdmException, EntityProviderException
    {
        ...
        final URL url = getoDataService().createURL(companyodataConstants.BOOKING_HEADERSET);
        final HttpURLConnectionRequest request = new HttpURLConnectionRequest(companyodataConstants.HTTP_METHOD_POST,
                url);
        ...
    }
}
```

The extension focuses on "createUrl" and "HttpURLConnectionRequest" in
order to create http resources. The "url" variable is evaluated and the
url root (mentioned previously) is added to it as a prefix.

Below is an example of what is obtained after an analysis. The "Hybris
Post Http Request service" has been created by the extension with the
call link from the java method to the request service. The part on the
right is created by the ABAP analyzer, and the link between the Hybris
object and the ABAP operation is created by the web service linker:

![](../images/277479999.png)

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The SAP Hybris extension requires
that the following other CAST extensions are also installed:

-   [JEE Analyzer](../../com.castsoftware.jee/)
-   [Web Services Linker](../../../../multi/com.castsoftware.wbslinker/)
-   com.castsoftware.internal.platform (internal extension)

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/277479997.png) | HYBRIS Get HttpRequest service |
| ![](../images/277479996.png) | HYBRIS Post HttpRequest service |
| ![](../images/277479995.png) | HYBRIS Put HttpRequest service |
| ![](../images/277479998.png) | HYBRIS Delete HttpRequest service |

### Transactions

Get, Post, Put and Delete HttpRequestService objects are transaction
end-points except if they are calling an external object (an object
belonging to a server: ABAP operation in this case).
