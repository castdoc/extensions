---
title: "JEE Manifest Discoverer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtjeemanifestdiscoverer

## What's new?

See [Release Notes](rn/).

## Extension description

This discoverer detects projects based on the presence of `MANIFEST.MF` files.

### In what situation should you install this extension?

This extension should be used when the java project files such as
.project or pom.xml are not delivered with the source code (without
these files, the discoverers provided "out of the box" in the CAST
Delivery Manager Tool cannot detect java projects) but there is a
"MANIFEST.MF" file present under a META-INF folder.

When the extension is installed, you can choose to activate it or not in
each package you create in the CAST Delivery Manager Tool. To do so,
click the Package Configuration tab and define the type of projects
to discover in the package (refer to the Extension interface
paragraph below).

### Technical information

What does the extension identify - i.e. what does it consider as a
"project" and therefore an Analysis Unit in the CAST Management Studio?

A project is created for each parent of META-INF/MANIFEST.MF.

As such, given the following folder structure:

    root
    -folder1
    --folder2
    ---lib
    ----file.jar
    ---META-INF
    ----MANIFEST.MF
    ---src
    ----com
    -----demo
    ------class1.java
    ---test
    ----com
    -----demo
    ------test1.java

A project "folder2" will be created with a root path of
folder1/folder2/META-INF/MANIFEST.MF. 

This project references the various java source folders:

-   %project%/src
-   %project%/test

If jar files are found inside the project root path, they are added in
the references of the project:

-   %project%/lib/file.jar

Note that even if the extension is activated, NO MANIFEST based project
will be identified when the following conditions are true:

-   root path contains at least one "JEE Eclipse project" .project
    definition file
-   root path contains at least one "Maven project" pom.xml
    definition file

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |    

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Extension interface

The following screen shots show the differences in the product when the
extension is installed:

-   in the CAST Delivery Manager Tool, in the Package configuration
    tab, the interface contains a new item in the list of "Project
    types discovered":

![](../images/235482651.png)

-   in the CAST Delivery Manager Tool, in the Package Content tab,
    when the package contains some java files, the interface will
    display a new item in the list of "Project types discovered":

![](../images/235482650.png)

-   in the CAST Delivery Manager Tool, in the Package Configuration
    tab, you can define the various exclusion of projects with the
    "Filtering rules" and "Project exclusion" lists :

![](../images/235482649.png)

Note: please ensure that you exclude projects that correspond to the
same source code.

-   in the CAST Management Studio, when the delivery is accepted and set
    as current version, the package will contain Analysis Units
    corresponding to these projects:

![](../images/235482648.png)

-   in the CAST Management Studio, the Analysis Units will contain:
    -    the Source Settings discovered by the extension:
        -   Application Root Path
        -   Java Source Folders

![](../images/235482647.png)

-   -   the Analysis Settings discovered by the extension:  
        -   Class Paths: list of jar files inside the Project Path

![](../images/235482646.png)

## Packaging your source code with the CAST Delivery Manager Tool

When you use the CAST Delivery Manager Tool to package the source code,
please ensure that you tick the following option in the CAST Delivery
Manager Tool to activate the extension:

Note that:

-   this option will be disabled (unticked) in all pre-existing
    packages - you need to check it before starting the packaging
    process otherwise it will be ignored.
-   for new packages created after the extension's installation, the
    option will be enabled by default

![](../images/235482645.jpg)

Also note that when a MANIFEST project is identified, it will be marked
as "ignored" in the Package Content tab once the packaging action is
complete (this means that no corresponding Analysis Unit will be
created):

![](../images/235482643.jpg)

To ensure that the project is marked as "selected" in the Package
Content tab once the packaging action is complete (therefore also
ensuring the automatic creation of an Analysis Unit), please ensure that
you untick the "Exclude all empty projects" option in the Package
Configuration tab before you run the package action:

![](../images/235482644.jpg)
