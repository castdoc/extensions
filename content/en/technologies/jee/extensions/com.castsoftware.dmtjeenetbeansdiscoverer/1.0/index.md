---
title: "JEE Netbeans Discoverer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtjeenetbeansdiscoverer

## What's new ?

See [Release Notes](rn/).

## Extension description

This discoverer detects projects based on the presence of
project.xml and project.properties files which are located in
inside the "nbproject" folder.

### In what situation should you install this extension?

This extension should be installed when delivering Application source
code for which the NetBeans IDE is being used.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

This extension contains a Project discoverer and you should take
note of the specific instructions in the installation guide that
explains how to package your source code with the CAST Delivery Manager
Tool when you have an existing Version.

## Packaging your source code with the CAST Delivery Manager Tool

### Interface

When you use the CAST Delivery Manager Tool to package the source code,
please ensure that you tick the following option in the CAST Delivery
Manager Tool to activate the extension (*click to enlarge*):

![](../images/235482675.jpg)

Note that:

-   this option will be disabled (unticked) in all pre-existing
    packages - you need to check it before starting the packaging
    process otherwise it will be ignored.
-   for new packages created after the extension's installation, the
    option will be enabled by default

### Packaging messages

The following messages may appear during the packaging action:

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh"><div>
Message ID
</div></th>
<th class="confluenceTh"><div>
Format
</div></th>
<th class="confluenceTh"><div>
Message
</div></th>
<th class="confluenceTh"><div>
Action
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.getJavaLanguageFailure</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Internal error while getting the information
from the plugin. The JavaLanguage was not found.</td>
<td class="confluenceTd">Please contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a> and open a ticket to
investigate the extension and its compatibility with the version of CAST
AIP.</td>
</tr>
<tr class="even">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.getJavaContainerLanguageFailure</td>
<td class="confluenceTd">ERROR</td>
<td class="confluenceTd">Internal error while getting the information
from the plugin. The JavaContainerLanguage was not found.</td>
<td class="confluenceTd">Please contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a> and open a ticket to
investigate the extension and its compatibility with the version of CAST
AIP.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">cast.dmt.discover.jee.netbeans.unknownType</td>
<td class="confluenceTd">WARNING</td>
<td class="confluenceTd">The type %TYPE% is not supported.</td>
<td class="confluenceTd"><p>The discovery of some information might be
missing. Please contact the provider of the extension.</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.missingProjectProperties</td>
<td class="confluenceTd">WARNING</td>
<td class="confluenceTd">The project.properties file has not been found
for %PROJECT_PATH%.</td>
<td class="confluenceTd">The discovery of some information might be
missing. Please contact the provider of the extension.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.loadProjectPropertiesIllegalArgumentException</td>
<td class="confluenceTd">WARNING</td>
<td class="confluenceTd">IllegalArgumentException while loading the
project.properties file for %PROJECT_PATH%.</td>
<td class="confluenceTd">Check if a malformed Unicode escape appears in
the project.properties file.</td>
</tr>
<tr class="even">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.loadProjectPropertiesIOException</td>
<td class="confluenceTd">WARNING</td>
<td class="confluenceTd">IOException while loading the
project.properties file for %PROJECT_PATH%.</td>
<td class="confluenceTd">Check the content of the project.properties
file.</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.buildProjectStart</td>
<td class="confluenceTd">INFO</td>
<td class="confluenceTd">Start processing of project %PATH%</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.buildProjectEnd</td>
<td class="confluenceTd">INFO</td>
<td class="confluenceTd">End processing of project %PATH%</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.reparseProjectStart</td>
<td class="confluenceTd">INFO</td>
<td class="confluenceTd">Start reparsing of project %PATH%</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="even">
<td
class="confluenceTd">cast.dmt.discover.jee.netbeans.reparseProjectEnd</td>
<td class="confluenceTd">INFO</td>
<td class="confluenceTd">End reparsing of project %PATH%</td>
<td class="confluenceTd">-</td>
</tr>
</tbody>
</table>
