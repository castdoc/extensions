---
title: "Apache Velocity - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension Id

com.castsoftware.java.velocity

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Apache Velocity Framework. Apache
Velocity is a Java-based templating engine, used to create dynamic web
pages by parsing data model to template.

## In what situation should you install this extension?

If your Java application uses the Apache Velocity Framework.

## Technology support

| Component | Version | Supported | Supported Technology |
|:----------|:--------|:---------:|:--------------------:|
| velocity-engine-core | 2.x     | :white_check_mark: |         JAVA         |
| velocity-tools-view | 3.x     | :white_check_mark: |     JAVA                 |
| velocity-tools-generic | 3.x     | :white_check_mark: |     JAVA                 |
| spring-velocity-support | 2.x     | :white_check_mark: |     JAVA                 |
| spring-context-support | <= 4.3.x     | :white_check_mark: |     JAVA                 |
| spring-webmvc | <= 4.3.x     | :white_check_mark: |     JAVA                 |

{{% alert color=info %}} For Spring Web MVC support, please install the [com.castsoftware.java.springtemplate](https://extend.castsoftware.com/#/extension?id=com.castsoftware.java.springtemplate&version=latest) extension to enable seamless integration with Spring framework features.{{% /alert %}}

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST Console. If you need to use it, should manually install the extension.

## What results can you expect?

### Objects

| Icon  | Description |
|---------|---------|
| ![](../images/663879843.png) | Java Velocity VTL Parameter |
| ![](../images/673841174.png) | Java Velocity Call To VTLContext |

### Links

| Link Type | Source and destination of link | Supported Velocity Engine Core Methods |
|---|---|---|
| relyonLink | Between Java Velocity VTL Parameter and respective Context JAVA class. |<details open><summary>Velocity Engine</summary>org.apache.velocity.app.Velocity.mergeTemplate<br>org.apache.velocity.app.VelocityEngine.mergeTemplate<br>org.apache.velocity.Template.merge<br>org.apache.velocity.app.Velocity.evaluate<br>org.apache.velocity.app.VelocityEngine.evaluate</details><br> <details open><summary>Velocity Tools</summary>org.apache.velocity.tools.view.VelocityViewServlet.mergeTemplate<br>org.apache.velocity.tools.view.VelocityLayoutServlet.mergeTemplate<br>org.apache.velocity.tools.view.VelocityView.merge</details> <br> <details open><summary>Velocity Spring</summary>org.springframework.ui.velocity.VelocityEngineUtils.mergeTemplateIntoString<br>org.springframework.ui.velocity.VelocityEngineUtils.mergeTemplate<br>org.apache.velocity.spring.VelocityEngineUtils.mergeTemplateIntoString<br>org.apache.velocity.spring.VelocityEngineUtils.mergeTemplate<br>org.springframework.web.servlet.view.velocity.VelocityView.mergeTemplate</details>|
| relyonLink | Between Java Velocity VTL Parameter and respective Context JAVA class. | org.apache.velocity.tools.view.VelocityViewServlet.getTemplate<br>org.apache.velocity.app.Velocity.getTemplate<br>org.apache.velocity.app.VelocityEngine.getTemplate<br>org.apache.velocity.tools.view.VelocityView.getTemplate<br>org.apache.velocity.context.Context.put<br>org.apache.velocity.VelocityContext.put<br>org.apache.velocity.context.AbstractContext.put<br>org.apache.velocity.tools.ToolContext.put<br>org.apache.velocity.tools.ToolContext.putAll<br> <br>**_NOTE:_**  Java Velocity VTL Parameter object is created, only if the above APIs are called from overridden handleRequest method of VelocityViewServlet/VelocityLayoutServlet derived class. |
| callLink  | Between HTML source code objects and Java Velocity Call To VTLContext objects. |  |
| callLink | Between Java Velocity Call To VTLContext objects and JAVA methods. |  |

## Code Examples

### Velocity Engine Core

<details><summary>Velocity Engine code - Test6.java</summary>

``` java
package test.velocity;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;

import java.io.Writer;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;

import bean.Product;

class Test6 {

    public Test6() throws Exception {

        Velocity.init("src/main/java/velocity.properties");

        Context context = new VelocityContext();

        final Collection<Product> products = new ArrayList<>();
        //context.put("test", "test");

        products.add(new Product("Widget", 12.99));
        products.add(new Product("Wotsit", 13.99));
        products.add(new Product("Thingy", 11.99));

        context.put("products", products);

        Template template2 = Velocity.getTemplate("Test7.vm");

        Writer writer2 = new StringWriter();
        template2.merge(context, writer2);
        System.out.println(writer2.toString());
    }
}
```
</details>

<details><summary>Context Class - product.java</summary>

``` java
package bean;

public class Product {

    private String name;
    private double price;

    public Product(String aName, double aPrice) {
        name = aName;
        price = aPrice;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setName(String val) {
        name = val;
    }

    public void setPrice(double val) {
        price = val;
    }

    public String getNameAndPrice() {
        return "Name: " + getName() + " Price: " + getPrice();
    }
}
```
</details>

<details><summary>Velocity Template - Test7.vm</summary>

``` Html
#macro (writeTable $productList)
    #set ($rowCount = 1)
    #foreach($product in $productList)
    #if ($rowCount % 2 == 0)
        #set ($bgcolor = "#FFFFFF")
    #else
        #set ($bgcolor = "#CCCCCC")
    #end
        <tr>
            <td bgcolor="$bgcolor">$product.name</td>
            <td bgcolor="$bgcolor">$product.price</td>
        </tr>
        #set ($rowCount = $rowCount + 1)
    #end
#end


<html>
    <head>
        <title>Macros Test</title>
    </head>
    <body>
        <table>
            #writeTable($products)
        </table>
    </body>
</html>
```
</details>

![](../images/663879842.png)

### Velocity Tools View

<details><summary>VelocityViewServlet - ProductServlet.java</summary>

``` java
package com.test.apache.velocity.servlet;

import com.test.apache.velocity.model.Product;
import com.test.apache.velocity.service.ProductService;
import org.apache.velocity.Template;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.view.VelocityViewServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ProductServlet extends VelocityViewServlet {

    ProductService service = new ProductService();

    @Override
    public Template handleRequest(HttpServletRequest request, HttpServletResponse response, Context context) {

        Logger logger= LoggerFactory.getLogger(ProductServlet.class);

        List<Product> products = service.getProducts();

        context.put("products", products);

        Template template = null;

        try {
            template = getTemplate("templates/index.vm");
            response.setHeader("Template Returned", "Success");
        } catch (Exception e) {
            logger.error("Error while reading the template ", e);
        }

        return template;

    }
}
```
</details>

<details><summary>Context Class - Product.java</summary>

``` java
package com.test.apache.velocity.model;

public class Product {

    private String name;
    private double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "name='" + name + '\'' + ", price=" + price + '}';
    }
}
```
</details>

<details><summary>Velocity Template - index.vm</summary>

``` Html
<HTML>
<HEAD>
    <TITLE>Online Electronic Store</TITLE>
    <style>
        body {background-color: powderblue;}
        h1   {color: blue;}
        p    {color: red;}
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            color:#333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #666666;
            background-color: #dedede;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #666666;
            background-color: #ffffff;
        }

    </style>

</HEAD>
<BODY>
<CENTER>
    <h1>Today's Offers</h1>
    <BR/>
    <BR/>
    <h2>$products.size() Products on Sale!</h2>
    <BR/>
    We are proud to offer these fine products
    at these amazing prices.
    <BR/>
    <BR/>
    #set( $count = 1 )
    <TABLE class="gridtable">
        <TR>
            <TH>Serial #</TH><TH>Product Name</TH><TH>Price</TH>
        </TR>
        #foreach( $product in $products )
            <TR>
                <TD>$count()</TD>
                <TD>$product.getName()</TD>
                <TD>$product.getPrice()</TD>
            </TR>
            #set( $count = $count + 1 )
        #end
    </TABLE>
    <BR/>
</CENTER>

</BODY>
</HTML>
```
</details>

![](../images/673841172.png)
