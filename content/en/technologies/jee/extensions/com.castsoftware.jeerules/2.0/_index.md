---
title: "JEE Rules - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.jeerules

## What's new?

See [Release Notes](rn/).

## Description

This extension provides additional rules for JEE technology and related
Frameworks supported by
[com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest).
These rules are compliant with CWE and OWASP TOP 10 Standards for
Security. These rules are in addition to other rules provided for JEE.

## Isofunctionality

Compared to versions 1.x, the support of the two following rules has been dropped in versions 2.x:

 - 1039016 "Avoid Unvalidated URL Redirect"
 - 1039058 "Avoid generating key with insufficient random generator in cookies"

If you wish to continue monitoring such vulnerabilities in your code, please consider using extension [com.castsoftware.securityanalyzer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.securityanalyzer&version=latest) which contains the following rules:

 - 8446 "Avoid URL redirection to untrusted site"
 - 8242 "Avoid using insufficient random values for cookies"

## Structural rules

The following structural rules are provided:

| Release | Link |
|---------|------|
| 2.0.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|2.0.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C2.0.0-beta1) |
| 2.0.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|2.0.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C2.0.0-alpha1) |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

When using CAST Imaging Console and your application contains JEE/Java
code, the extension will be automatically downloaded and installed for
you:

![](../images/632455272.jpg)

## Packaging, delivering and analyzing your source code

Once the extension is downloaded and installed, there is nothing
specific to do: ensure your code is analyzed
with [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest) and
the rules will be triggered.
