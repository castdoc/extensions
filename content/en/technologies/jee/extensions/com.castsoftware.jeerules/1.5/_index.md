---
title: "JEE Rules - 1.5"
linkTitle: "1.5"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.jeerules

## What's new?

See [Release Notes](rn/).

## Description

This extension provides additional rules for JEE technology and related frameworks supported by [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest). These rules are compliant with CWE and OWASP TOP 10 Standards for
Security. These rules are in addition to other rules provided for JEE.

## Structural rules

The following structural rules are provided:

| Release | Link |
|---------|------|
| 1.5.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|1.5.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C1.5.2-funcrel) |
| 1.5.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|1.5.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C1.5.1-funcrel) |
| 1.5.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|1.5.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C1.5.0-funcrel) |
| 1.5.0-beta3 | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|1.5.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C1.5.0-beta3) |
| 1.5.0-beta2 | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|1.5.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C1.5.0-beta2) |
| 1.5.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|1.5.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C1.5.0-beta1) |
| 1.5.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=\|\|1.5.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_jeerules&ref=%7C%7C1.5.0-alpha1) |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :white_check_mark: |

## Compatibility

| Core Release | Operating System | Supported |
|---|---|:--:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

When using CAST Imaging Console and your application contains JEE/Java
code, the extension will be automatically downloaded and installed for
you:

![](../images/632455272.jpg)

## Packaging, delivering and analyzing your source code

Once the extension is downloaded and installed, there is nothing
specific to do: ensure your code is analyzed
with [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest) and
the rules will be triggered.
