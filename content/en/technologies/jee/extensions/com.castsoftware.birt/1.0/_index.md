---
title: "Birt - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.birt

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for BIRT (Business Intelligence and
Reporting Tools) technology for JEE projects
(see [https://eclipse.github.io/birt-website/](https://eclipse.github.io/birt-website/)). This
extension has two parts:

-   a standalone part which analyzes files with following extensions:
    -   .rptdesign
    -   .dashboard
    -   .datadesign
    -   .rptlibrary
    -   .gadget
    -   .rpttemplate
-   a part which works together with the JEE Analyzer. This part creates
    the links from the Java methods to the BIRT reports created by the
    standalone part.

## In what situation should you install this extension?

If your JEE application source code uses BIRT Report files
(\*.rptdesign), you should install this extension.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download instructions

You will need to manually install the extension.

## What results can you expect?

### Objects

| Icon | Description |
|:---:|---|
| ![](../images/CAST_Birt_Report.png) | BIRT Report |
| ![](../images/CAST_Birt_Dashboard.png) | BIRT Dashboard |
| ![](../images/CAST_Birt_Dataset.png) | BIRT Datamart |
| ![](../images/CAST_Birt_Library.png) | BIRT Library |
| ![](../images/CAST_Birt_Gadget.png) | BIRT Gadget |
| ![](../images/CAST_Birt_Template.png) | BIRT Template |
| ![](../images/CAST_Birt_Query.png) | BIRT SQL Query |
| ![](../images/CAST_Birt_CsvAccess.png) | BIRT CSV Data |
| ![](../images/CAST_Birt_Data_Set.png) | BIRT Data Set |
| ![](../images/CAST_Birt_Component.png) | BIRT Component |
| ![](../images/CAST_Birt_Cube.png) | BIRT Cube |
| ![](../images/CAST_Birt_Report_Call.png) | BIRT Report Call |

### Objects and Links

#### Reports, Dashboards, Gadgets, Templates

BIRT reports, dashboards, gadgets, templates have same structure. They
can be considered as reports or parts of reports. They may refer to each
other through include links.

Example Dashboard code:

``` java
<property name="reportName">^/Report Designs/MySpendDetail.rptdesign</property>
```
![](../images/579469332.png)

#### Data sets, SQL Queries, Csv data

BIRT data sets can be defined in any report or library. They may define
an access to a database or .CSV through SQL code. Data sets may be
called from any object through call links.

Example Report code:

``` java
<oda-data-set extensionID="org.eclipse.birt.report.data.oda.jdbc.JdbcSelectDataSet" name="Charges Data Set" id="4765">
    <xml-property name="queryText"><![CDATA[select ch.transaction_date, ch.description, ch.amount, c.locale
        from all_charges ch inner join cust_info c on (c.customer_id=ch.customer_id)
        where c.customer_id = ?
        and ch.statement_date_id=?]]>
    </xml-property>
</oda-data-set>
...
<property name="dataSet">Charges Data Set</property>
```
![](../images/579469330.png)

#### Libraries

BIRT libraries contain components which may be called from other objects
through use links.

Example Library code:

``` java
<components>
    <extended-item extensionName="Chart" name="Merchants-Chart" id="55">
    </extended-item>
</components>
```

Accompanying Dashboard code:

``` java
<property name="reportlibrary">{"reportLibrary":"/Applications/SFWealthApp-21Jul/SF Wealth.rptlibrary","reportItem":"Merchants-Chart"}</property>
```

![](../images/579469331.png)

#### Report calls

Reports may be called from java code where following method calls can be
found:

``` java
org.eclipse.birt.report.engine.api.ReportEngine.openReportDesign

com.axelor.apps.ReportFactory.createReport
```

Example Java code:

``` java
import com.axelor.apps.ReportFactory;
...

public void printMove(ActionRequest request, ActionResponse response) throws AxelorException {
 String fileLink =
        ReportFactory.createReport(IReport.ACCOUNT_MOVE, moveName + "-${date}")
            .addParam("Locale", ReportSettings.getPrintingLocale(null))
            .addParam(
                "Timezone", move.getCompany() != null ? move.getCompany().getTimezone() : null)
            .addParam("moveId", move.getId())
            .generate()
            .getFileLink();
}
```

![](../images/579469329.png)

### Rules

No rules are provided with this extension.
