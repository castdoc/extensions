---
title: "JEE Analyzer - 1.3"
linkTitle: "1.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.jee

## What's new?

See [Release Notes](rn).

## Prerequisites

See [JEE - Required third-party software](../../../requirements/).

## Technology support

See [Covered Technologies](../../../../coverage-overview/) and also [JEE - Technology support notes](../../../notes/)for
additional information.

## Function Point, Quality and Sizing support

-   Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

The JEE Analyzer extension requires that the following other extensions are also installed:

- [com.castsoftware.wbslinker](../../../../multi/com.castsoftware.wbslinker/)
- [com.castsoftware.java.config.tcc](../../com.castsoftware.java.config.tcc)

{{% alert color="info" %}}Note that any dependent extensions are automatically downloaded and installed for
you. You do not need to do anything.{{% /alert %}}

## Application qualification information

Please see: [JEE - Application qualification specifics](../../../qualification-specifics/).

## Prepare and deliver the source code

Please see: [JEE - Prepare and deliver the source code](../../../prepare).

## Analysis configuration and execution

Please see: [JEE - Analysis configuration and execution](../../../analysis-config/).

## What analysis results can you expect?

Please see: [JEE - Analysis results](../../../results/).

## Structural rules

The vast majority of rules provided for the JEE Analyzer are embedded in
CAST AIP - see [JEE - Structural rules](../../../rules) for more
information. In addition, some rules are also provided with the
extension:

| Release  | Link  |
|----------------|------------|
| 1.3.21-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.21-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.21-funcrel) |
| 1.3.20-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.20-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.20-funcrel) |
| 1.3.19-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.19-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.19-funcrel) |
| 1.3.18-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.18-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.18-funcrel) |
| 1.3.17-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.17-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.17-funcrel) |
| 1.3.16-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.16-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.16-funcrel) |
| 1.3.15-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.15-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.15-funcrel) |
| 1.3.14-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.14-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.14-funcrel) |
| 1.3.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.13-funcrel) |
| 1.3.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.12-funcrel) |
| 1.3.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.11-funcrel) |
| 1.3.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.10-funcrel) |
| 1.3.9-funcrel  | Extension removed. |
| 1.3.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.8-funcrel)   |
| 1.3.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.7-funcrel)   |
| 1.3.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.6-funcrel)   |
| 1.3.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.5-funcrel)   |
| 1.3.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.4-funcrel)   |
| 1.3.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.3-funcrel)   |
| 1.3.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.2-funcrel)   |
| 1.3.1-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.1-funcrel)   |
| 1.3.0-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.0-funcrel)   |
| 1.3.0-beta3    | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.0-beta3)       |
| 1.3.0-beta2    | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|1.3.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C1.3.0-beta2)       |

### Quality Rules

#### Avoid static field of type collection (7562)

When calculating a snapshot for applicationusing JSE 5.0, the Quality
Rule "Avoid static field of type collection (7562)" does not list as
"Very High Risk Objects" classes that are or inherit from a Generic
collection. The Quality Rule lists only the non generic form of
collections. For example static attributes of type
java.util.Collection\<E\> will not be reported as a violation.

#### Persistence: Avoid table and column names that are too long (portability) (7706)

Situation:

-   JEE application using the Java Persistence API (JPA), e.g. in the
    form of Hibernate.
-   A table or column name is not specified in the annotations of the
    JPA entity.

Symptoms: The JPA entity is not listed as a "Very High Risk" object in
the results.

### No link bookmarks generated

When Java Methods are defined inside a JSP file, no link bookmarks are
generated (bookmarks can be seen in CAST Enlighten or in the Dynamic
Link Manager). Example of a Java Method defined in a JSP file:

``` xml
<%@ page language="Java" %> 
<%! 
public void myMethod(String message)
{ System.out.println(message); } 
%> 
```