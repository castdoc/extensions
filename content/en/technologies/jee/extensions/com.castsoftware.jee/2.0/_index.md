---
title: "JEE Analyzer - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.jee

## What's new?

See [Release Notes](rn/).

## Isofunctionality

This 2.x release of the extension is based on previous releases of the extension: see the [Release Notes](rn/) for more information. Note however, that the following support provided in previous releases of the extension has been removed from this release (and all future releases):

- The web client part of an application is no longer analyzed by this extension: specifically this means that JavaScript files and the JavaScript part of JSP pages will not be analyzed. In previous 1.x releases of this extension, these objects were analyzed by this extension and were materialized as "eFiles" in the analysis results. As a result of this change, your analysis results will differ in comparison to previous 1.x releases: there will be a change (a reduction) in LOC values due to the fact that eFile type objects that were previously created will no longer be created. Analysis of this code and the generation of associated objects is instead handled soley by [com.castsoftware.html5](../../../../web/html5-js/com.castsoftware.html5/) as was already the case in previous 1.x releases.
- Legacy environment profiles for Hibernate, Servlet, JSF and WBS have been removed.

## Description

This extension provides support for analyzing applications built with JEE related technologies: objects and links between these objects are identified and Automated Function Point values are calculated. A set of JEE specific structural rules are also available with the extension.

## Technology support

See [Covered Technologies](../../../../coverage-overview/) and also [Technology support notes](../../../notes/) for additional information.

## Function Point, Quality and Sizing support

-   Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :x: |

## Dependencies with other extensions

The JEE Analyzer extension requires that the following other extensions are also installed:

- [com.castsoftware.wbslinker](../../../../multi/com.castsoftware.wbslinker/)
- [com.castsoftware.java.config.tcc](../../com.castsoftware.java.config.tcc)

{{% alert color="info" %}}Note that any dependent extensions are automatically downloaded and installed for
you. You do not need to do anything.{{% /alert %}}

## Download and installation instructions

For JEE applications, the extension will be automatically installed by CAST Imaging:

![](../images/662143607.jpg)

## What analysis results can you expect?

### Objects - Java

| Icon | MetaModel name |
|:----:|----------------|
| ![](../images/664436949.png) | Generic Java Class            |
| ![](../images/664436950.png) | Generic Java Constructor      |
| ![](../images/664436951.png) | Generic Java Interface        |
| ![](../images/664436952.png) | Generic Java Method           |
| ![](../images/664436953.png) | Generic Java Type Parameter   |
| ![](../images/664436954.png) | Java Annotation Type          |
| ![](../images/664436955.png) | Java Annotation Type Method   |
| ![](../images/664436956.png) | Java Block                    |
| ![](../images/664436957.png) | Java Class                    |
| ![](../images/664436958.png) | Java Component                |
| ![](../images/664436959.png) | Java Constructor              |
| ![](../images/664436960.png) | Java Enum                     |
| ![](../images/664436961.png) | Java Enum Item                |
| ![](../images/664436962.png) | Java Field                    |
| ![](../images/664436963.png) | Java File                     |
| ![](../images/664436964.png) | Java Import                   |
| ![](../images/664436965.png) | Java Initializer              |
| ![](../images/664436966.png) | Java Instantiated Class       |
| ![](../images/664436967.png) | Java Instantiated Constructor |
| ![](../images/664436968.png) | Java Instantiated Interface   |
| ![](../images/664436969.png) | Java Instantiated Method      |
| ![](../images/664436970.png) | Java Interface                |
| ![](../images/664436971.png) | Java Local Variable           |
| ![](../images/664436972.png) | Java Lambda Expression        |
| ![](../images/664436973.png) | Java Method                   |
| ![](../images/664436974.png) | Java Package                  |
| ![](../images/664437041.png) | Servlet                       |
| ![](../images/664437042.png) | Servlet Mapping               |
| ![](../images/664437044.png) | Java Properties File          |
| ![](../images/664437045.png) | Java Property Mapping         |
| ![](../images/664437046.png) | J2EE XML File                 |

### Objects - JSP (Presentation)

| Icon | MetaModel name |
|:----:|----------------|
| ![](../images/664436993.png) | J2EE Scoped Bean         |
| ![](../images/664436983.png) | Servlet Attributes Scope |
| ![](../images/664436994.png) | Bean Property            |
| ![](../images/664436984.png) | JSP EL Function          |
| ![](../images/664436985.png) | JSP File Template        |
| ![](../images/664436987.png) | J2EE XML Object          |
| ![](../images/664436989.png) | JSP Custom Tag           |
| ![](../images/664436989.png) | JSP Custom Tag Attribute |
| ![](../images/664436990.png) | JSP Custom Tag Library   |
| ![](../images/664436988.png) | Xml Bean                 |

### Objects - Apache Struts

| Icon | MetaModel name |
|:----:|----------------|
| ![](../images/664436996.png)  | Struts Action / Common Struts Action |
| ![](../images/664436996.png)  | Struts Action Mapping                |
| ![](../images/664436997.png)  | Struts Configuration File            |
| ![](../images/664436998.png)  | Struts2 Configuration File           |
| ![](../images/664436999.png)  | Struts Form Bean                     |
| ![](../images/664437000.png)  | Struts Forward                       |
| ![](../images/664437000.png)  | Struts Interceptor                   |
| ![](../images/664437001.png)  | Struts Interceptor Stack             |
| ![](../images/664437002.png)  | Struts Package                       |
| ![](../images/664437003.png)  | Struts Result                        |
| ![](../images/664437004.png)  | Struts Validator                     |
| ![](../images/664437005.png)  | Validation                           |
| ![](../images/664437006.png)  | Validation Form Field                |
| ![](../images/664437007.png)  | Validation Forms Set                 |

### Objects - STXX (Presentation)

| Icon | MetaModel name |
|:----:|----------------|
| ![](../images/664437008.png) | STXX Pipeline        |
| ![](../images/664437009.png) | STXX Transform       |
| ![](../images/664437010.png) | STXX Transforms File |

### Objects - JPA (Persistence)

| Icon | MetaModel name |
|:----:|----------------|
| ![](../images/664437021.png) | JPA Embeddable                     |
| ![](../images/664437022.png) | JPA Embeddable Property            |
| ![](../images/664437023.png) | JPA Entity                         |
| ![](../images/664437024.png) | JPA Entity Property                |
| ![](../images/664437025.png) | JPA Named Native Query             |
| ![](../images/664437026.png) | JPA Named Query                    |
| ![](../images/664437027.png) | JPA ORM Configuration File         |
| ![](../images/664437028.png) | JPA Persistence Configuration File |
| ![](../images/664437029.png) | JPA Persistence Unit               |
| ![](../images/664437030.png) | JPA SQL Result Set                 |


### Objects - EJB (Persistence)

| Icon | MetaModel name |
|:----:|----------------|
| ![](../images/664437047.png) | Entity Java Bean         |
| ![](../images/664437049.png) | Message Driven Java Bean |
| ![](../images/664437050.png) | Session Java Bean        |

### Objects - Other (Spring related)

| Icon | MetaModel name |
|:----:|----------------|
| ![](../images/664437032.png) | Singleton Java Bean |
| ![](../images/664437033.png) | Spring Bean         |
| ![](../images/664437034.png) | Spring Beans File   |
| ![](../images/664437035.png) | Spring Controller   |
| ![](../images/664437036.png) | Spring View         |
| ![](../images/664437037.png) | CDI Named Bean      |
| ![](../images/664437038.png) | Spring Batch Job    |
| ![](../images/664437040.png) | Spring Batch Step   |


### Links - References detected by the analyzer when carrying out a pure Java analysis

<table>
<thead>
  <tr>
    <th>Link type</th>
    <th>When is this type of link created?</th>
    <th colspan="2">Example</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>MENTION</td>
    <td>This link is traced on following expressions (Callee always refers to a class or interface): <br>"instance of" operator<br> <br>cast expression<br> <br>class literal<br> <br>this literal<br> <br>class creation<br> <br>array creation</td>
    <td colspan="2">- if (b instanceof A . class) ...<br> <br>- A . this = 1;<br> <br>- B b = (B) new A(1);<br> <br>- B b [] = new A[5];</td>
  </tr>
  <tr>
    <td>THROW, RAISE, CATCH</td>
    <td>These links are used to describe exception handling (Callee always refers to a class):<br> <br>Throw link is traced on "throws" clause of method declaration towards exceptions possibly raised by the method.<br> <br>Raise link is traced on "throw" statement towards type of the exception actually thrown by the method.<br> <br>Catch link is traced on "try" statement towards type of the exception parameter declared in catch clause(s).</td>
    <td colspan="2">- void f () throws C1 {...} <br>- throw new Exception()<br> <br>- try {...}<br> catch (Throwable e) {...)</td>
  </tr>
  <tr>
    <td>RELY ON</td>
    <td>This link is used to describe typing. Caller uses callee to define its Java type. Callee always refers to a class or interface.</td>
    <td colspan="2">- void f (A a) {...} <br>- A a [] = null;</td>
  </tr>
  <tr>
    <td>ACCESS</td>
    <td>This link is used to describe run-time interactions. It may be enriched by the following sub-types:<br> <br>Read<br> <br>Write<br> <br>Exec<br> <br>Member<br> <br>Array</td>
    <td colspan="2">- return x - (read) <br>- x = 1 - (write)<br> <br>- x++ - (read and write)<br> <br>- x[1] = 2 - (array not read)<br> <br>- x[] = {1,2} - (read)<br> <br>- f(1) - (exec)<br> <br>- x.toString() - (member)<br> <br>- x.f() - (member on x ; exec on f)<br> <br>- x.y = 1 - (member on x ;write on y)</td>
  </tr>
  <tr>
    <td>INHERIT</td>
    <td>This link is used to describe object hierarchy. It may be enriched by the following sub-types (Caller and callee always refer to classes or interfaces):<br> <br>Extend:<br> <br>class to super-class<br> <br>interface to super-interface<br> <br>Implement:<br> <br>class to super-interface(s)<br> <br>By extension the Inherit link is also used to describe method overriding as directly related to inheritance. In this case exclusive sub-types are available (Caller and callee always refer to methods):<br> <br>Hide: for static methods;<br> <br>Override: for instance methods; in addition link can be enriched by the Implement sub-type if overriding is from a non abstract overriding method to an abstract overridden one.</td>
    <td colspan="2">- class C1 extends C2 implements I<br> {...} <br>- interface I1 extends implements I2<br> {...}</td>
  </tr>
  <tr>
    <td>USE</td>
    <td>This type is reserved for dynamic link resolution. It covers server side objects referencing:<br> <br>Select<br> <br>Insert<br> <br>Update<br> <br>Delete</td>
    <td colspan="2">-</td>
  </tr>
  <tr>
    <td>PROTOTYPE</td>
    <td>This link is reserved for J2EE components' support and is used as follows (Caller and callee always refer to a component and to a class or interface):<br> <br>Applet: between the applet component and its applet class.<br> <br> </td>
    <td colspan="2">-</td>
  </tr>
</tbody>
</table>

### Links - References that are detected by the analyzer while analyzing JSP pages and Tag Files as client files

<table>
<tbody>
<tr>
<td><strong>Link Type</strong></td>
<td><strong>Situation</strong></td>
<td><strong>When is this type of link created?</strong></td>
</tr>
<tr class="even">
<td><strong>USE</strong></td>
<td><strong>TypLib</strong></td>
<td>
<pre>
&lt;!-- METADATA TYPE = &quot;TypeLib&quot; FILE = &quot;TypLibFileName&quot;--&gt;
</pre>
</td>
</tr>
<tr class="odd">
<td><strong>USE</strong></td>
<td><strong>Applet</strong></td>
<td>
<pre>
&lt;APPLET CODE = "AppletCode" CODEBASE = "AppletCodeBase"&gt;
</pre>
</td>
</tr>
<tr class="even">
<td><strong>USE</strong></td>
<td><strong>ActiveX</strong> through a
<strong>variable</strong></td>
<td>
<pre>
x <span class="op">=</span> <span class="kw">new</span> <span class="fu">ActiveXObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>x<span class="op">.</span><span class="fu">Method</span><span class="op">()</span></span></pre>
<p><strong>Use</strong> link between f and A.B</p>
</div></td>
</tr>
<tr class="odd">
<td><strong>USE</strong></td>
<td><strong>Dynamic data source</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>OBJECT id <span class="op">=</span> id_obj classid <span class="op">=</span> <span class="st">&quot;clsid:Sample&quot;</span><span class="op">&gt;&lt;/</span>OBJECT<span class="op">&gt;</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>A DATASRC<span class="op">=</span>#id_obj datafld <span class="op">=</span> <span class="st">&quot;url&quot;</span> id<span class="op">=</span>id_a<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Use</strong> link between id_a and id_obj</p>
</div></td>
</tr>
<tr class="even">
<td><p><strong>USE</strong></p></td>
<td><p><strong>Database object</strong></p>
<p><strong><br />
</strong></p></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb5"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="fu">ExecuteSQL</span><span class="op">(</span><span class="st">&quot;select * from authors&quot;</span><span class="op">)</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td><strong>USE</strong></td>
<td><strong>MAP</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb6"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>img src<span class="op">=</span><span class="st">&quot;images/cover.jpg&quot;</span> border<span class="op">=</span><span class="dv">0</span> usemap<span class="op">=</span><span class="st">&quot;#covermap&quot;</span> ismap id<span class="op">=</span>id_img<span class="op">&gt;</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>map name<span class="op">=</span><span class="st">&quot;covermap&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;whatis.htm&quot;</span> coords<span class="op">=</span><span class="st">&quot;0,0,315,198&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;signup.asp&quot;</span> coords<span class="op">=</span><span class="st">&quot;0,198,230,296&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;holdings.asp&quot;</span> coords<span class="op">=</span><span class="st">&quot;229,195,449,296&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;question.htm&quot;</span> coords<span class="op">=</span><span class="st">&quot;314,0,449,196&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>map<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
</tbody>
<tbody>
<tr class="odd">
<td><strong>MENTION</strong></td>
<td>Indicates the area in which the
<strong>ActiveX</strong> is mentioned</td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb7"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="fu">CreateObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
</div>
</div>
<p><strong>Mention</strong> link between f and A.B</p>
</div></td>
</tr>
<tr class="even">
<td><strong>MENTION</strong></td>
<td>Indicates the area in which the <strong>class
name</strong> is mentioned</td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb8"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="kw">new</span> <span class="fu">g</span><span class="op">()</span></span>
<span id="cb8-4"><a href="#cb8-4" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
</div>
</div>
<p><strong>Mention</strong> link between f and g</p>
</div></td>
</tr>
<tr class="odd">
<td><strong>INCLUDE</strong></td>
<td>Indicates the <strong>inclusion</strong> of a
<strong>file</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb9"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT LANGUAGE <span class="op">=</span> <span class="st">&quot;JavaScript&quot;</span> SRC <span class="op">=</span> <span class="st">&quot;inc.js&quot;</span><span class="op">&gt;</span></span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td><strong>CALL</strong></td>
<td>Indicates a <strong>call</strong> to a
<strong>file</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb10"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>A HREF <span class="op">=</span> <span class="st">&quot;called_file.htm&quot;</span> <span class="op">&gt;&lt;/</span>A<span class="op">&gt;</span></span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT LANGUAGE <span class="op">=</span> JavaScript<span class="op">&gt;</span></span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true" tabindex="-1"></a>window<span class="op">.</span><span class="fu">open</span><span class="op">(</span><span class="st">&quot;called_file.htm&quot;</span><span class="op">)</span></span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td><strong>CALL</strong></td>
<td>Indicates a <strong>function call</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb11"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="dv">1</span><span class="op">;</span></span>
<span id="cb11-5"><a href="#cb11-5" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb11-6"><a href="#cb11-6" aria-hidden="true" tabindex="-1"></a>function <span class="fu">g</span><span class="op">()</span></span>
<span id="cb11-7"><a href="#cb11-7" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb11-8"><a href="#cb11-8" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="fu">f</span><span class="op">()</span></span>
<span id="cb11-9"><a href="#cb11-9" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb11-10"><a href="#cb11-10" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Call</strong> link between g and f</p>
</div></td>
</tr>
<tr class="even">
<td><strong>ACCESS</strong></td>
<td>Indicates a <strong>access</strong> type link
enters a <strong>property</strong> of an HTC component together with the
associated PUT or GET function.</td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb12"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>PUBLIC<span class="op">:</span>PROPERTY NAME<span class="op">=</span><span class="st">&quot;xmlData&quot;</span></span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true" tabindex="-1"></a> ID<span class="op">=</span><span class="st">&quot;xmlDataID&quot;</span> </span>
<span id="cb12-3"><a href="#cb12-3" aria-hidden="true" tabindex="-1"></a>GET<span class="op">=</span><span class="st">&quot;getxmlData&quot;</span> PUT<span class="op">=</span><span class="st">&quot;putxmlData&quot;</span><span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>ACCESS</strong> link between xmlData and the getxmlData and
putxmlData functions.</p>
</div></td>
</tr>
<tr class="odd">
<td><strong>ACCESS and<br />
READ</strong></td>
<td><strong>Read only access</strong> to a
<strong>file</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb13"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;!--</span>#FLASTMODE FILE <span class="op">=</span> <span class="st">&quot;accessed_file.htm&quot;</span><span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td><strong>ACCESS and<br />
READ</strong></td>
<td><strong>Read only access</strong> to a
<strong>variable</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb14"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb14-2"><a href="#cb14-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb14-3"><a href="#cb14-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb14-4"><a href="#cb14-4" aria-hidden="true" tabindex="-1"></a>y<span class="op">=</span>x</span>
<span id="cb14-5"><a href="#cb14-5" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb14-6"><a href="#cb14-6" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Read only access</strong> between f and x</p>
</div></td>
</tr>
<tr class="odd">
<td><strong>ACCESS and<br />
WRITE</strong></td>
<td><strong>Read and write access</strong> to a
<strong>variable</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb15"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb15-1"><a href="#cb15-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb15-2"><a href="#cb15-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb15-3"><a href="#cb15-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb15-4"><a href="#cb15-4" aria-hidden="true" tabindex="-1"></a>y<span class="op">=</span>x</span>
<span id="cb15-5"><a href="#cb15-5" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb15-6"><a href="#cb15-6" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Read and write access</strong> between f and y</p>
</div></td>
</tr>
<tr class="even">
<td><strong>ACCESS and PAGE_FORWARD</strong></td>
<td>Indicates a redirection. Only available for
analyzed IIS applications.</td>
<td>-</td>
</tr>
<tr class="odd">
<td><strong>REFER</strong></td>
<td>Indicates that a <strong>variable</strong>
refers to another <strong>variable</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb16"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb16-2"><a href="#cb16-2" aria-hidden="true" tabindex="-1"></a>x <span class="op">=</span> <span class="kw">new</span> <span class="fu">ActiveXObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb16-3"><a href="#cb16-3" aria-hidden="true" tabindex="-1"></a><span class="fu">Application</span><span class="op">(</span><span class="st">&quot;y&quot;</span><span class="op">)=</span>x</span>
<span id="cb16-4"><a href="#cb16-4" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Refer</strong> link between Application("y") and x</p>
</div></td>
</tr>
<tr class="even">
<td><strong>RELY ON and INSTANCE OF</strong></td>
<td>Indicates that a <strong>variable</strong> is
an <strong>instance of a class</strong></td>
<td><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb17"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb17-1"><a href="#cb17-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb17-2"><a href="#cb17-2" aria-hidden="true" tabindex="-1"></a>x<span class="op">=</span><span class="kw">new</span> <span class="fu">ActiveXObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb17-3"><a href="#cb17-3" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>INSTANCE_OF</strong> link between x and A.B</p>
</div></td>
</tr>
<tr class="odd">
<td><strong>GO THROUGH</strong></td>
<td>Indicates the error file(s) used. Only
available for analyzed IIS applications.</td>
<td>-</td>
</tr>
</tbody>
</table>

### Links - References that are detected by the analyzer while analyzing the JSP page implementation class and/or Tag Handlers are dynamic references

For the dynamic references which are issued from a grep or have the
caller located in a JSP script element as a declaration, expression or
scriplet, a **static** link will be created. This new link has almost
the same properties as the dynamic link, however the caller can be
different. For example, when an object is located in an included file we
will have a dynamic reference from the \_jspService function of the JSP
Page (resp. doTag method of a Tag File) and the object and a static
reference from the included JSP File (resp. Tag File) and the object.
Note that the main function of the generated servlet/handler class is a
pure dynamic object. So, it cannot have static references and we will
create them at the function's parent (JSP Page/Tag File) level.

The following table summarizes the process of creation of the static
links using the dynamic links while parsing generated servlet classes
('Dynamic Caller' are replace by 'Static caller'):

| Caller: Dynamic Object | Link code located in                 | Caller: Static Object                |
|------------------------|--------------------------------------|--------------------------------------|
| _jspService            | JSP page                             | JSP page                             |
| _jspService            | JSP page                             | JSP page                             |
| servlet class          | JSP page                             | JSP page                             |
| servlet class          | JSP File (included file)             | JSP File (included file)             |
| caller                 | JSP Page or JSP File (included file) | JSP Page or JSP File (included file) |

Example:

fileA.jsp

``` java
<%! String redirectA; %>
<% redirectA = "/jsp/fileA.jsp"; %>
```

fileA.jsp/ includedA.jsp

``` java
<%@ include file="included.jsp"%>
```

``` java
<%! String includedA; %>
<% includedA = "/jsp/fileA.jsp"; %>
```

### Links - EJB

The following lists describes references that are detected by the JEE
analyzer and the context in which corresponding links are traced.Links
between bean and class/interfaces are flagged as "Prototype" and are
oriented as follows:

-   From bean local and remote interfaces to the EJB itself,
-   From the EJB itself to its bean class and, for entity bean, primary
    key class.

In addition the following links are also traced:

<table>
<tbody>
<tr>
<th colspan="2">Link Type</th>
<th>When is this type of link created?</th>
</tr>
<tr>
<td colspan="2" class="confluenceTd"><strong>FIRE</strong></td>
<td><div class="content-wrapper">
<p>1. Traced between <strong>bean class member methods</strong> that are
implicitly called by the <strong>EJB Container</strong>:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable" data-mce-resize="false">
<colgroup>
<col />
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Home interface</strong></td>
<td><br />
</td>
<td><strong>Bean class</strong></td>
<td><br />
</td>
<td><strong>Remote interface</strong></td>
</tr>
<tr class="even">
<td>create(...)</td>
<td><strong>--F--&gt;</strong></td>
<td>ejbCreate(...)</td>
<td><strong>&lt;--F--</strong></td>
<td>create(...)</td>
</tr>
<tr class="odd">
<td>remove(...)</td>
<td><strong>--F--&gt;</strong></td>
<td>ejbRemove(...)</td>
<td><strong>&lt;--F--</strong></td>
<td>remove(...)</td>
</tr>
<tr class="even">
<td><br />
</td>
<td><br />
</td>
<td><em>&lt; implemented business methods
&gt;</em></td>
<td><strong>&lt;--F--</strong></td>
<td><em>&lt; exported business methods
&gt;</em></td>
</tr>
<tr class="odd">
<td>find<em>&lt;method&gt;</em>(...)</td>
<td><strong>--F--&gt;</strong></td>
<td>ejbFind<em>&lt;method&gt;</em>(...)</td>
<td><br />
</td>
<td><br />
</td>
</tr>
</tbody>
</table>
</div>
<p>2. Traced between <strong>bean class/home interface</strong> and
<strong>server side objects</strong>. For example in this standard
ejb-jar.xml file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>entity<span class="op">&gt;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>CustomerBean<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span>Customer<span class="op">&lt;/</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>query<span class="op">&gt;</span></span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>query<span class="op">-</span>method<span class="op">&gt;</span></span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>method<span class="op">-</span>name <span class="op">&gt;</span>findByFirstName<span class="op">&lt;/</span>method<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>method<span class="op">-</span>params<span class="op">&gt;</span></span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a>            <span class="op">&lt;</span>method<span class="op">-</span>param<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">lang</span><span class="op">.</span><span class="fu">String</span><span class="op">&lt;/</span>method<span class="op">-</span>param<span class="op">&gt;</span></span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;/</span>method<span class="op">-</span>params<span class="op">&gt;</span></span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;/</span>query<span class="op">-</span>method<span class="op">&gt;</span></span>
<span id="cb1-13"><a href="#cb1-13" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>ejb<span class="op">-</span>ql<span class="op">&gt;</span>SELECT DISTINCT <span class="fu">OBJECT</span><span class="op">(</span>c<span class="op">)</span> FROM Customer AS c WHERE c<span class="op">.</span><span class="fu">firstName</span> <span class="op">=</span> <span class="op">?</span><span class="dv">1</span><span class="op">&lt;/</span>ejb<span class="op">-</span>ql<span class="op">&gt;</span></span>
<span id="cb1-14"><a href="#cb1-14" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>query<span class="op">&gt;</span></span>
<span id="cb1-15"><a href="#cb1-15" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>entity<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p>The signature of the method is created from the values of the tags
&lt;method-name&gt; and &lt;method-param&gt;. The method is searched for
using the home interfaces (for the findXXX) and the bean class (for the
"select methods").</p>
<p>The abstract schema name identified by grep in the query "EJB-QL" is
used to find the associated bean (the query can reference the abstract
schema name of another bean). From the bean it is possible to retrieve
the associated table names and then link resolution is possible.</p>
<p>3. Also traced between <strong>SEI.Mi (Service Endpoint
Interface)</strong> and <strong>BC.Mi (Bean Class)</strong>. Mi stands
for methods that have the same signature in the SEI and the BC. The EJB
must be a stateless session bean:</p>
<ul>
<li>SEI.Mi -- Fire --&gt; BC.Mi</li>
</ul>
<p>4. In addition, this link type is used for implicit invocations. They
are used to reflect:</p>
<ul>
<li>either the life cycle events of a component (such as
creation/destruction)</li>
<li>or the interaction of an external actor such as a container (for
EJBs or servlets) or a web browser (for applets)</li>
</ul>
<p><strong>Caller</strong> always refers to a component or a class
member method. <strong>Callee</strong> always refers to a class member
method.</p>
<div>
<div>
Note: "Fire" links are never escalated.
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>USE SELECT, DELETE, UPDATE,
INSERT</strong></td>
<td><div class="content-wrapper">
<p>Traced between bean and server objects (tables).</p>
Bean <strong>--Usdui--&gt;</strong> Table (U for Use, s for Select, d
for Delete, u for Update, i for Insert)
<ul>
<li>Finder and select Method <strong>--Us--&gt;</strong> Table</li>
</ul>
<p>The link <strong>Bean --Usdui--&gt; Table</strong> is traced only
with container managed persistence (cmp) beans. For Bean managed
persistence (bmp) beans, this link is indirect and is represented by two
links:</p>
<ul>
<li>One from Bean component to its java class (traced by EJB Assistant
).</li>
<li>One from Methods of the bean class where access to database objects
is coded (via jdbc) to server objects corresponding to these objects
(tables in this case). This link is traced by Java Analyzer.</li>
</ul>
<p>For example in this standard ejb-jar.xml file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>entity id<span class="op">=</span><span class="st">&quot;ContainerManagedEntity_1&quot;</span><span class="op">&gt;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span>Player<span class="op">&lt;/</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span></code></pre></div>
</div>
</div>
<p>The bean in question is found in the ejb-jar.xml file using its name.
Persistence information is identified using descriptors specific to each
server type:</p>
</div></td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>PROTOTYPE</strong></td>
<td><div class="content-wrapper">
This link type is reserved for J2EE components' support and is used as
follows:
<ul>
<li><strong>EJB:</strong> between the bean component and its
implementation classes (bean class, local/remote interfaces and possibly
primary key class).</li>
<li><strong>Servlet:</strong> between the servlet component and its
servlet class.</li>
<li><strong>EJB</strong>: between the bean and the SEI (Service Endpoint
Interface).</li>
</ul>
<p>For example the <strong>Webservices.xml</strong> (must be placed in
the module directory)</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>webservices <span class="kw">...</span><span class="op">&gt;</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>webservice<span class="op">-</span>description<span class="op">&gt;</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>      <span class="kw">...</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>webservice<span class="op">-</span>description<span class="op">-</span>name<span class="op">&gt;</span>MyHelloService<span class="op">&lt;/</span>webservice<span class="op">-</span>description<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>jaxrpc<span class="op">-</span>mapping<span class="op">-</span>file<span class="op">&gt;</span>mapping<span class="op">.</span><span class="fu">xml</span><span class="op">&lt;/</span>jaxrpc<span class="op">-</span>mapping<span class="op">-</span>file<span class="op">&gt;</span> <span class="co">// relatif ou absolu</span></span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>port<span class="op">-</span>component<span class="op">&gt;</span></span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>      <span class="kw">...</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>port<span class="op">-</span>component<span class="op">-</span>name<span class="op">&gt;</span>HelloIF<span class="op">&lt;/</span>port<span class="op">-</span>component<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span>helloservice<span class="op">.</span><span class="fu">HelloIF</span><span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span></span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>service<span class="op">-</span>impl<span class="op">-</span>bean<span class="op">&gt;</span></span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a>            <span class="op">&lt;</span>ejb<span class="op">-</span>link<span class="op">&gt;</span>HelloServiceEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>link<span class="op">&gt;</span></span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;/</span>service<span class="op">-</span>impl<span class="op">-</span>bean<span class="op">&gt;</span></span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;/</span>port<span class="op">-</span>component<span class="op">&gt;</span></span>
<span id="cb3-14"><a href="#cb3-14" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>webservice<span class="op">-</span>description<span class="op">&gt;</span></span>
<span id="cb3-15"><a href="#cb3-15" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>webservices<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>JAX-RPC mapping file</strong> (mapping.xml in the
example) indicates that the mapping information between the web service
operation and the java methods can be found in mapping.xml.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>java<span class="op">-</span>wsdl<span class="op">-</span>mapping <span class="kw">...</span><span class="op">&gt;</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="kw">...</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>port<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>port<span class="op">-</span>name<span class="op">&gt;</span>HelloIFPort<span class="op">&lt;/</span>port<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;/</span>port<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>service<span class="op">-</span><span class="kw">interface</span><span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span>helloservice<span class="op">.</span><span class="fu">HelloIF</span><span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span></span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span>method<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>java<span class="op">-</span>method<span class="op">-</span>name<span class="op">&gt;</span>sayHello<span class="op">&lt;/</span>java<span class="op">-</span>method<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>wsdl<span class="op">-</span>operation<span class="op">&gt;</span>sayHello<span class="op">&lt;/</span>wsdl<span class="op">-</span>operation<span class="op">&gt;</span></span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true" tabindex="-1"></a>            <span class="op">&lt;</span>method<span class="op">-</span>param<span class="op">-</span>parts<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-13"><a href="#cb4-13" aria-hidden="true" tabindex="-1"></a>               <span class="op">&lt;</span>param<span class="op">-</span>position<span class="op">&gt;</span><span class="dv">0</span><span class="op">&lt;/</span>param<span class="op">-</span>position<span class="op">&gt;</span></span>
<span id="cb4-14"><a href="#cb4-14" aria-hidden="true" tabindex="-1"></a>               <span class="op">&lt;</span>param<span class="op">-</span>type<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">lang</span><span class="op">.</span><span class="fu">String</span><span class="op">&lt;/</span>param<span class="op">-</span>type<span class="op">&gt;</span></span>
<span id="cb4-15"><a href="#cb4-15" aria-hidden="true" tabindex="-1"></a>               <span class="kw">...</span></span>
<span id="cb4-16"><a href="#cb4-16" aria-hidden="true" tabindex="-1"></a>               <span class="op">&lt;/</span>method<span class="op">-</span>param<span class="op">-</span>parts<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-17"><a href="#cb4-17" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span>method<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-18"><a href="#cb4-18" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-19"><a href="#cb4-19" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>java<span class="op">-</span>wsdl<span class="op">-</span>mapping<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><br />
</p>
<div>
<div>
Note: <strong></strong> "Prototype" links are never escalated.
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>ACCESS</strong></td>
<td><div class="content-wrapper">
These links correspond to inter-bean references. These references are
made via a logical name (declared in the ejb-jar.xml file and that
specifies the target ejb) used in the bean client code. The tags
containing this logical name are &lt;ejb-ref&gt; or
&lt;ejb-local-ref&gt;.
<ul>
<li>EJB client -- Access --&gt; EJB target</li>
</ul>
<p>For example in this standard ejb-jar.xml file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb5"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>entity<span class="op">&gt;</span> <span class="co">//ou session ou message-driven</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span> AddressBean<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>ref<span class="op">&gt;</span> <span class="co">//ou ejb-local-ref</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>ejb<span class="op">-</span>ref<span class="op">-</span>name<span class="op">&gt;</span>ejb<span class="op">/</span>CustomerRef<span class="op">&lt;/</span>ejb<span class="op">-</span>ref<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a>      <span class="kw">...</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>ejb<span class="op">-</span>link<span class="op">&gt;</span>CustomerBean<span class="op">&lt;/</span>ejb<span class="op">-</span>link<span class="op">&gt;</span></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>ejb<span class="op">-</span>ref<span class="op">&gt;</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>entity<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p>Each bean specifies the list of other beans it accesses as well as
the references used to do so.</p>
</div></td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>JOIN</strong></td>
<td><div class="content-wrapper">
Traced between entity beans based on the abstract schema defined in
deployment descriptor (beneath the
&lt;relationships&gt;...&lt;/relationships&gt; tag).
<p>An entity bean (source EJB) is linked to another entity bean
(destination EJB) only if navigation is possible from source EJB to
destination EJB, i.e the relation-role in which source EJB is involved
contains at least one cmr (container managed relationship) field
allowing access to the destination EJB.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb6"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>ejb<span class="op">-</span>relation<span class="op">&gt;</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>description<span class="op">&gt;</span>Team To Player<span class="op">&lt;/</span>description<span class="op">&gt;</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>ejb<span class="op">-</span>relation<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">.</span><span class="fu">teams</span><span class="op">-</span>TeamEJB<span class="op">.</span><span class="fu">players</span><span class="op">&lt;/</span>ejb<span class="op">-</span>relation<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span>TeamEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>multiplicity<span class="op">&gt;</span>many<span class="op">&lt;/</span>multiplicity<span class="op">&gt;</span></span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-8"><a href="#cb6-8" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>TeamEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-9"><a href="#cb6-9" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-10"><a href="#cb6-10" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-11"><a href="#cb6-11" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span>players<span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-12"><a href="#cb6-12" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">util</span><span class="op">.</span><span class="fu">Collection</span><span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span></span>
<span id="cb6-13"><a href="#cb6-13" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-14"><a href="#cb6-14" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-15"><a href="#cb6-15" aria-hidden="true" tabindex="-1"></a> </span>
<span id="cb6-16"><a href="#cb6-16" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-17"><a href="#cb6-17" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-18"><a href="#cb6-18" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>multiplicity<span class="op">&gt;</span>many<span class="op">&lt;/</span>multiplicity<span class="op">&gt;</span></span>
<span id="cb6-19"><a href="#cb6-19" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-20"><a href="#cb6-20" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-21"><a href="#cb6-21" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-22"><a href="#cb6-22" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-23"><a href="#cb6-23" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span>teams<span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-24"><a href="#cb6-24" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">util</span><span class="op">.</span><span class="fu">Collection</span><span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span></span>
<span id="cb6-25"><a href="#cb6-25" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-26"><a href="#cb6-26" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-27"><a href="#cb6-27" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>ejb<span class="op">-</span>relation<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p>Considering the example above, the source EJB
<strong>TeamEJB</strong> will be linked to the destination EJB
<strong>PlayerEJB</strong> because the cmr field
<strong>players</strong> makes navigation possible from source to
destination EJB.</p>
<p>For the same reasons, <strong>PlayerEJB</strong> (now source EJB)
will have <strong>Refer link</strong> to <strong>TeamEJB</strong>
(destination EJB for this link).</p>
<p>For each Join link, the information below is retrieved from the
deployment descriptor and accessible in HTML Report:</p>
<ul>
<li><p><strong>Relation Name</strong></p>
<p>The name of the EJB relation as found in the
&lt;ejb-relation-name&gt; tag of the deployment descriptor
(<strong>PlayerEJB.teams-TeamEJB.players</strong> in the example). If no
name is provided, EJB Assistant computes a name for the relation, based
on the names of the two beans involved. For instance, the computed name
would be <strong>TeamEJB-PlayerEJB</strong> if
<strong>PlayerEJB.teams-TeamEJB.players</strong> was missing in the
example.</p></li>
<li><p><strong>Relation Description</strong></p>
<p>The relation description as specified in the descriptor. No default
value for this attribute.</p></li>
<li><p><strong>Multiplicity</strong></p>
<p>One of the string <em>Many</em>, <em>One</em> or <em>N/S</em> if
relation role multiplicity is not specified in the descriptor.</p></li>
</ul>
</div></td>
</tr>
</tbody>
</table>

### Structural rules

The following rules are provided in the extension:

| Release | Link |
|---------|------|
| 2.0.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.10-funcrel) |
| 2.0.9-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.9-funcrel) |
| 2.0.8-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.8-funcrel) |
| 2.0.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.7-funcrel) |
| 2.0.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.6-funcrel) |
| 2.0.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.5-funcrel) |
| 2.0.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.4-funcrel) |
| 2.0.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.3-funcrel) |
| 2.0.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.2-funcrel) |
| 2.0.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.1-funcrel) |
| 2.0.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.0-funcrel) |
| 2.0.0-beta4 | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.0-beta4](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.0-beta4) |
| 2.0.0-beta3 | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.0-beta3) |
| 2.0.0-beta2 | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.0-beta2) |
| 2.0.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_jee&ref=\|\|2.0.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_jee&ref=%7C%7C2.0.0-beta1) |

See also the following rules provided in the default CAST Assessment Model shipped with CAST Imaging Core:

| Technology | URL |
|------------|-----|
| JEE        | [https://technologies.castsoftware.com/rules?sec=t_140029&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_140029&ref=%7C%7C) |

The following extensions also provide JEE specific rules:

- [com.castsoftware.jeerules](../../com.castsoftware.jeerules/)
- [com.castsoftware.springsecurity](../../com.castsoftware.springsecurity/)
- [com.castsoftware.securityanalyzer](../../../../multi/com.castsoftware.securityanalyzer/)