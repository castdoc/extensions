---
title: "Web JSP Discoverer"
linkTitle: "Web JSP Discoverer"
type: "docs"
---

>The Web JSP Discoverer is not currently provided as an extension:
instead it is embedded in CAST Imaging Core and is therefore present "out of
the box".

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Supported project type</th>
<th class="confluenceTh">CAST Delivery Manager Tool interface</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Web JSP</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Configures a project for each <strong>web.xml file</strong>
identified:</p>
<p><em>Click to enlarge</em></p>
<p><em><img src="images/248833760.jpg" /><br />
</em></p>
</div></td>
</tr>
</tbody>
</table>
