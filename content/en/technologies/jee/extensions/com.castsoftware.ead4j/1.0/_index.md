---
title: "IBM EAD4J Jade - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.ead4j

## What's new?

See [Release Notes](rn/).

## In what situation should you install this extension?

If your JEE application source code uses the IBM EAD4J
Jade framework you should install this extension.

This extension creates EAD4J.JADE Operation objects which represent
entry-points for web services. For each operation, an Operation object
of type Any is created (this can receive all kinds of HTTP
requests). For example:

### Application configuration

In the "web.xml" file, the extension takes into account only servlets
pointing to a class which inherits directly (or not)
from com.ibm.ead4j.jade.servlet.JadeGatewayServlet. The servlet
mapping (url-pattern) is identified. For example:

``` xml
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.jcp.org/xml/ns/javaee" xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd" id="WebApp_ID" version="3.1">

...
  <servlet>
    <display-name>InitialSAMLaunchServlet</display-name>
    <servlet-name>InitialSAMLaunchServlet</servlet-name>
    <servlet-class>com.att.uam.ui.jade.servlet.InitialSamLaunchServlet</servlet-class>
  </servlet>
...
  <servlet-mapping>
    <servlet-name>InitialSAMLaunchServlet</servlet-name>
    <url-pattern>/InitialSAMGateway</url-pattern>
  </servlet-mapping>

....
</web-app>
```

The following files are taken into account:

#### application-config.xml

The name of the file is not important, but it must contain the
"/appl-config" xpath. This file also contains the application name
and the URL to access the application:

``` xml
<appl-config>
    <appl>
        <appl-name>euam</appl-name>
        <appl-codebase>/euam</appl-codebase>
    </appl>
</appl-config>
```

#### actionHandler.xml

The name of the file is not important, but it must contain the
"/actionHandler-config" xpath:

-   actionHandler-name is the part of the url after
    "jadeAction=". See following example screenshot.
-   actionHandler-class is the class whose method will be called.

``` xml
<actionHandler-config>
    <actionHandler>
        <actionHandler-name>ADD_ADDITIONAL_LANGUAGE_ACTION_HANDLER</actionHandler-name>
        <actionHandler-class>com.att.uam.ui.jade.action.AddAdditionalLanguageActionHandler</actionHandler-class>

    </actionHandler>
</actionHandler-config>
```

![](../images/669253789.png)

### Application analysis

The extension will take into account:

-   Actions inheriting from
    com.ibm.ead4j.jade.action.AbstractActionHandler (actionHandler.xml)
-   Servlets inheriting
    from com.ibm.ead4j.jade.servlet.JadeGatewayServlet (web.xml)

#### Operation creation

-   The application-config.xml file provides the application url
    which will be the start of the operation url (in the examples above
    this is "/euam").
-   In actionHandler.xml, provides the jadeAction parameter url part
    (in the examples above this
    is "/euam?jadeAction=ADD_ADDITIONAL_LANGUAGE_ACTION_HANDLER"),
    with the associated class (in the examples above this is
    "com.att.uam.ui.jade.action.AddAdditionalLanguageActionHandler").
-   In web.xml, provides the url mapping. The extension retains the
    mappings corresponding to classes inheriting
    from com.ibm.ead4j.jade.servlet.JadeGatewayServlet. You will see
    this mapping when tapping the F12 key when any CAST_EAD4J_Operation
    object is selected in CAST Enlighten.

The class is taken into account if it inherits
from com.ibm.ead4j.jade.action.AbstractActionHandler (directly or
not), and the method whose return type is
"com.ibm.ead4j.jade.bean.ResultBean" (in the examples above this
is execute_startAction) will be selected.

``` java
public class AddAdditionalLanguageActionHandler extends StartTaskActionHandler{

    public ResultBean execute_startAction(ResultHandler handler, Parameters params, JadeSession jadeSession)
            throws ProcessException, ResultBeanException 
    {
...
    }
}
```

A callLink is created from the operation to the method.

## Supported IBM EAD4J Jade framework items

-   Actions inheriting from
    com.ibm.ead4j.jade.action.AbstractActionHandler (actionHandler.xml)
-   Servlets inheriting from
    com.ibm.ead4j.jade.servlet.JadeGatewayServlet (web.xml)

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The IBM EAD4J Jade extension requires
that the following other CAST extensions are also installed:

-   [HTML5 and JavaScript](../../../../web/html5-js/)- minimum release 2.0.2
-   [JEE Analyzer](../../com.castsoftware.jee/)
-   [Web Services Linker](../../../../multi/com.castsoftware.wbslinker/)
-   com.castsoftware.internal.platform (internal extension)

Note that:

- any dependent extensions are automatically downloaded and installed for you. You do not need to do anything.

### Transaction configuratuion setup

A transaction configuration is automatically imported when the extension is
installed:

![](../images/669253788.jpg)

## What results can you expect?

### Objects

| Icon | Metamodel description |
|---|---|
| ![](../images/669253783.png) | EAD4J.JADE Operation |
