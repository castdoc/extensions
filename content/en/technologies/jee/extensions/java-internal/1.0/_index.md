---
title: "Java Internal Extension - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

>This extension is delivered as a dependency of the JEE Analyzer
specifically to provide additional features for Java/JEE analyses. It
will be automatically downloaded, so there is no need to download the
extension manually. It provides an additional metamodel that defines the
following object type:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/641269839.png" width="32px"/></p>
</div></td>
<td class="confluenceTd"><ul>
<li>Java Method (<strong>≥</strong>1.0.1-funcrel)</li>
<li>Method of Anonymous Class (1.0.0-funcrel)</li>
</ul></td>
</tr>
</tbody>
</table>