---
title: "Gradle Project Discoverer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtjeegradlediscoverer

## What's new?

See [Release Notes](rn/).

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## What is discovered?

The Gradle Project Discoverer detects following project types :

### Java/Android projects built with Gradle (single as well multimodule)

Discovery is based on the presence of \*.gradle files in the
Application source code. The extension considers the following project
build files to discover corresponding Gradle Java/Android projects:

-   "settings.gradle"
-   "build.gradle"
-   "dependency.gradle" (any external file that is imported in
    build.gradle using "apply from")

Note: For Android specific projects, the Gradle discoverer ≥ 1.1.6
will discover the folder containing the AndroidManifest.xml file as
part of the Resource files - this will appear as shown below:

![](../images/629276874.png)

### Kotlin projects built with the Kotlin Gradle Plugin (single as well multimodule)

Discovery is based on the presence of \*.gradle.kts files in the
Application source code. The extension considers the following project
build files to discover corresponding Gradle Java projects:

-   "settings.gradle.kts"
-   "build.gradle.kts"
-   "dependency.gradle.kts" (any external file that is imported in
    build.gradle.kts using "apply from")

### References required

All gradle / .gradle.kts files MUST contain at least one reference to
the plugins listed below either using the apply plugin option or
using the plugin command mentioned below.

``` java
\\apply plugin references:

apply plugin: 'java'
apply plugin: 'maven'
apply plugin: '%android%'
apply plugin: 'application'
apply plugin: '%gradle%'
apply plugin: '%kotlin%'

\\plugin command references:

plugins
{
    id : 'java'
}

plugins
{
    id : 'maven'
}

plugins
{
    id : '%android%'
}

plugins
{
    id : 'application'
}

plugins
{
    id : '%gradle%'
}

plugins
{
    id : '%kotlin%'
}
```

## In what situation should you install this extension?

When you want to discover and select all the Gradle projects from your
source code. The extension reads all the projects based on their
respective \*.gradle or \*.gradle.kts file.

## Exclusion rules

The following exclusion rules are provided:

| Exclusion Rule                         | Behavior                                                        | By-default Behavior |
|:---------------------------------------|:----------------------------------------------------------------|:--------------------|
| ExclusionRuleJavaPreferGradleToEclipse | Prefer Gradle Project over Eclipse Project for the same project | True                |
| ExclusionRuleJavaPreferGradleToMaven   | Prefer Gradle Project over Maven Project for the same project   | True                |
| ExclusionRuleJavaPreferMavenToGradle   | Prefer Maven Project over Gradle Project for the same project   | False               |
| ExclusionRuleJavaPreferEclipseToGradle | Prefer Eclipse Project over Gradle Project for the same project | False               |

These options are exposed in the CAST Delivery Manager Tool (but not in
AIP Console):

![](../images/490274832.png)

## Delivery messages

The following messages emitted by the discoverer may appear during the
source code delivery:

|         |                                                                                  |                                                                         |                                                                                                                                  |
|:--------|:---------------------------------------------------------------------------------|:------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------|
| Format  | Message Id                                                                       | Message                                                                 | Remediation                                                                                                                      |
| Error   | cast.dmt.discover.jee.gradle.missingLanguage                                     | Missing language %PATH%. Invalid extension.                             | Check if in the bug fix of the extension and update it.                                                                          |
| Error   | cast.dmt.extend.discover.jee.gradle.project.parseError                           | %FILE_PATH% .Invalid File                                               | error while parsing the file. Correct the corresponding file.                                                                    |
| Error   | cast.dmt.discover.jee.gradle.getApplyFromURLContent.ExceptionInURLAccess         | URL%applyFromURL% PROJECT%PROJECT%                                      | exception while accessing the file mentioned in tag applyFromURL                                                                 |
| Info    | cast.dmt.discover.jee.gradle.buidProjectStart                                    | Start processing of project %PATH%                                      | This is information only message.                                                                                                |
| Info    | cast.dmt.discover.jee.gradle.buidProjectEnd                                      | End processing of project %PATH%                                        | This is information only message.                                                                                                |
| Info    | cast.dmt.discover.jee.gradle.buidProjectStart                                    | Start re-parsing of project %PATH%                                      | This is information only message.                                                                                                |
| Info    | cast.dmt.discover.jee.gradle.buidProjectEnd                                      | End re-parsing of project %PATH%                                        | This is information only message.                                                                                                |
| Info    | cast.dmt.discover.jee.gradle.DiscoveredJavaProject                               | discovered plugins in build.gradle                                      | This is information only message.                                                                                                |
| Info    | cast.dmt.extend.discover.jee.gradle.project.parseApplyFromURL                    | FILE_PATH %filepath%                                                    | This is information only message.                                                                                                |
| Info    | cast.dmt.extend.discover.jee.gradle.project.parseIncludedFile File Not Parsed!!! | PATH %filepath%                                                         | IncludedFile is not parsed. check the file                                                                                       |
| Warning | cast.dmt.discover.jee.gradle.IgnoredProject                                      | missing java plugin in build.gradle                                     | Check if corresponding build.gradle has plugin as java                                                                           |
| Warning | cast.dmt.discover.jee.gradle.parser.GradleProjectParser                          | build.gradle is Empty                                                   | Check if corresponding build.gradle is empty. Impact : The particular module with empty build.gradle file will not be discovered |
| Warning | cast.dmt.discover.jee.gradle.parser.GradleSettingParser                          | settings.gradle is Empty                                                | Check if settings.gradle is empty                                                                                                |
| Warning | cast.dmt.extend.discover.jee.gradle.project.parseIncludeFiles.IncorrectFile      | IncludedFile is Empty                                                   | Check if includedFile is empty                                                                                                   |
| Warning | cast.dmt.discover.jee.gradle.ProjectWithNoName                                   | missing project name, keeping default project name as GradleJavaProject | Check if the project name is missing in settings.gradle file. The particular project default name will be GradleJavaProject      |

## Results after using the discoverer

Gradle Java projects will be selected by default - example shown for the
CAST Delivery Manager Tool:

![](../images/490274831.png)

## Notes

### src/main/java directory

Gradle defines the directory src/main/java as the default source
directory for compilation
(reference: [https://docs.gradle.org/current/userguide/organizing_gradle_projects](https://docs.gradle.org/current/userguide/organizing_gradle_projects.)).
Therefore, the Gradle extension works in exactly the same way, it will
add the src/main/java as the default source directory if no source
directories are explicitly mentioned in the build.gradle file within
the sourceSet block.

As a result of this, there may be scenarios where we do not have
src/main/java folder for project modules structure but still it shows
src/main/java as source directory in discovery files, if discovered. In
these scenarios, the discovered Gradle modules will be shown as
"Deactivated" in AIP Console.  
  
*There is difference in the syntax for classic gradle and kotlin gradle
project. Below is the official documentation for the same:*  
*[https://docs.gradle.org/current/userguide/migrating_from_groovy_to_kotlin_dsl.html](https://docs.gradle.org/current/userguide/migrating_from_groovy_to_kotlin_dsl.html)*

### Required Maven repositories

Contrary to pure Maven projects, Maven repositories used by Gradle
projects will not be fetched and included by default, therefore, if your
Gradle project uses any Maven repositories, you must ensure that these
repositories are delivered manually. 
