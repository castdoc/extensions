---
title: "Vaadin Framework - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.vaadin

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for [Vaadin Framework](https://vaadin.com/) - Vaadin is a web app development framework for Java that includes a large library of UI components.

## In what situation should you install this extension?

The main purpose of this extension is to provide support for views
and events provided in the Vaadin Framework and link them to the
relevant java objects (resolved by the [JEE
Analyzer](JEE_Analyzer)).

## Supported versions

The following table displays the list of version that this extension
supports.

| Framework Name | Version          | Supported          |
|----------------|------------------|:------------------:|
| Vaadin         | 7.x.x<br>8.x.x   | :white_check_mark: |
| Vaadin Flow    | 10.x.x<br>14.x.x<br>22.x.x <br>23.x.x<br>24.x.x | :white_check_mark: |

## Supported APIs responsible for creating a Vaadin View Object

<table>
  <tbody>
    <tr>
      <th>Framework</th>
      <th>Vaadin API responsible</th>
      <th>Type of support</th>
      <th>Supported</th>
    </tr>
    <tr>
      <th>Vaadin </th>
      <td>
        <p>com.vaadin.navigator.View com.vaadin.ui.UI com.vaadin.ui.Panel com.vaadin.ui.Window com.vaadin.ui.CssLayout com.vaadin.ui.HorizontalLayout com.vaadin.ui.VerticalLayout com.vaadin.ui.GridLayout com.vaadin.ui.FormLayout</p>
      </td>
      <td>
        <p>Class inheritance</p>
        <p>Since 1.0.0-alpha2:</p>
        <ul>
          <li>intermediary class inheriting from Vaadin API are no longer Vaadin Views,only the top most class is one.</li>
        </ul>
      </td>
      <td><img src="/images/icons/emoticons/check.svg" alt="(tick)"> 1.0.0-alpha1</td>
    </tr>
    <tr>
      <th>Vaadin Flow</th>
      <td>com.vaadin.flow.component.orderedlayout.* com.vaadin.flow.component.applayout.* com.vaadin.flow.component.html.* com.vaadin.flow.router.Route</td>
      <td>
        <p>Presence of Route annotation</p>
        <p>Since 1.0.0-alpha2:</p>
        <ul>
          <li>add recursive class inheritance support for Vaadin Event Handler Object to be bind to Views </li>
        </ul>
      </td>
      <td><img src="/images/icons/emoticons/check.svg" alt="(tick)"> 1.0.0-alpha1</td>
    </tr>
  </tbody>
</table>

## Supported APIs responsible for creating a Vaadin Event Handler Object

Colored cell are the latest additions:

|             |                                                                           |                                                                 |                             |                                                                                             |                                                                                             |                                                                                             |                                                                                             |                                                                                             |
|:---:|:-----------:|:----------:|:-------:|:-----:|:----:|:---:|:----------:|:-----:|
|  Framework  |                                    API                                    |                      Signature to support                       |        Version added        |                                      Handling listener                                      |                                                                                             |                                                                                             |                                                                                             |                                                                                             |
|             |                                                                           |                                                                 |                             |                                      Lambda expression                                      |                                      Method reference                                       |                                       Anonymous class                                       |                   Custom Instance of Click/Focus/Blur/Listener API class                    |                                   Regular Method Call\*                                     |
|   Vaadin    |                  com.vaadin.ui.Button.<u>Button</u>                   |   (text, <u>listener</u>) or (icon, <u>listener</u>)    | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |             com.vaadin.ui.Button.<u>addClickListener</u>              |                       <u>listener</u>                       | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |        com.vaadin.ui.AbstractFocusable.<u>addFocusListener</u>        |                       <u>listener</u>                       | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |        com.vaadin.ui.AbstractFocusable.<u>addBlurListener</u>         |                       <u>listener</u>                       | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |       com.vaadin.ui.AbstractField.<u>addValueChangeListener</u>       |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |            com.vaadin.ui.CheckBox.<u>addFocusListener</u>             |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |             com.vaadin.ui.CheckBox.<u>addBlurListener</u>             |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |  com.vaadin.event.FieldEvents.FocusNotifier.<u>addFocusListener</u>   |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |   com.vaadin.event.FieldEvents.BlurNotifier.<u>addBlurListener</u>    |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |         com.vaadin.ui.ComboBox.<u>addValueChangeListener</u>          |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |            com.vaadin.ui.ComboBox.<u>addFocusListener</u>             |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|   Vaadin    |             com.vaadin.ui.ComboBox.<u>addBlurListener</u>             |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
| Vaadin Flow |         com.vaadin.flow.component.button.Button.<u>Button</u>         |   (text, <u>listener</u>) or (icon, <u>listener</u>)    | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|             |                                                                           |                (text, icon, <u>listener</u>)                | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
| Vaadin Flow |    com.vaadin.flow.component.ClickNotifier.<u>addClickListener</u>    |                       <u>listener</u>                       | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
| Vaadin Flow |    com.vaadin.flow.component.FocusNotifier.<u>addFocusListener</u>    |                       <u>listener</u>                       | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
| Vaadin Flow |     com.vaadin.flow.component.BlurNotifier.<u>addBlurListener</u>     |                       <u>listener</u>                       | 1.0.0-alpha1 & 1.0.0-alpha2 | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
| Vaadin Flow |      com.vaadin.flow.component.checkbox.Checkbox.<u>Checkbox</u>      |  (boolean, <u>listener</u>) or (str, <u>listener</u>)   |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|             |                                                                           |               (str, boolean, <u>listener</u>)               |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
| Vaadin Flow |      com.vaadin.flow.component.combobox.ComboBox.<u>ComboBox</u>      |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
|             |                                                                           | (str, <u>listener</u>) or (str, <u>listener</u>, items) |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |
| Vaadin Flow | com.vaadin.flow.component.AbstractField.<u>addValueChangeListener</u> |                       <u>listener</u>                       |         1.0.0-beta1         | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  |

Note

-   Providing a regular methodcall\* as listener implies that there must
    by a return statement of type Click/Focus/Blur/Listener that
    declares the Click/Focus/Blur/Event to be executed on action.
-   Vaadin implicitly treats Lambdas and Method references patterns such
    that the body of the Lambda or Method are seen as XxxEvents.
-   When custom (or anonymous XxxListener) classes are explicitly
    instantiated then the according action methods is executed for
    example, the method "ButtonClick" for a Click, "blur" for a focus
    loss and "focus" for a focus gained.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Vaadin Framework extension requires
that the following other CAST extensions are also installed:

-   CAST AIP Internal extension (internal technical extension)
-   [JEE Analyzer](../../com.castsoftware.jee/)

## Download and installation instructions

The extension will not be automatically installed by CAST Console,
therefore you should ensure that the extension is manually installed
using the Application -
Extensions interface:

![](../images/645693570.jpg)

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner. The
following objects and  links will be resolved:

### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
<th class="confluenceTh">Name</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">Vaadin View</td>
<td class="confluenceTd">Name of the java class inheriting Vaadin views
or annotated with @Route. </td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">Vaadin Event Handler</td>
<td class="confluenceTd">name_of_component.name_of_event, e.g.:<br />
&#10;<ul>
<li>Button.Click</li>
<li>CheckBox.Click</li>
<li>Button.Blur</li>
<li>Button.Focus</li>
<li>CheckBox.ValueChange</li>
<li>ComboBox&lt;String&gt;.ValueChange</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">Java Method
(com.castsoftware.java.internal)</td>
<td class="confluenceTd"><p>since alpha2:</p>
<p>For overrided methods:</p>
<ul>
<li>buttonClick</li>
<li>focus</li>
<li>blur</li>
<li>valueChange (Vaadin) </li>
<li>valueChanged (Vaadin Flow)</li>
</ul>
<p>A new object is created when one of those methods name is found
within a listener parameter declare with an Anonymous Vaadin Class. 
  </p></td>
</tr>
</tbody>
</table>

### Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Link Type</th>
<th class="confluenceTh" style="text-align: left;">Source </th>
<th class="confluenceTh" style="text-align: left;">Destination</th>
<th class="confluenceTh" style="text-align: left;">Action </th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">link created by
com.castsoftware.vaadin</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693565.png" draggable="false"
data-image-src="../images/645693565.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693565"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Lambda-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Lambda
Expression</p>
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method
(<strong>com.castsoftware.jee</strong>)</p>
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Method of
Anonymous Class/Java Method
(<strong>com.castsoftware.internal.java</strong>)</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">link created by
com.castsoftware.vaadin</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Access Exec</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693565.png" draggable="false"
data-image-src="../images/645693565.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693565"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Lambda-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /> Java Lambda
Expression</p>
</div></td>
<td class="confluenceTd" style="text-align: left;"><p>link created by
com.castsoftware.jee</p>
<p><strong>link deleted by com.castsoftware.vaadin</strong></p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Mention</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;"><p>link created by
com.castsoftware.jee</p>
<p><strong>link deleted by com.castsoftware.vaadin</strong></p></td>
</tr>
</tbody>
</table>

## Example code scenarios

### Example of Support for View and Event Handler

vaadin8 Expand source

``` java
package org.example;

import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class MyView extends VerticalLayout implements View {
    public MyView()  {
        // Create the content of your VerticalLayout view
        Label label = new Label("This is a VerticalLayout-based view!");
        addComponent(label);

        Button button = new Button("Go to Home View");
        button.addClickListener(event -> {
            getUI().getNavigator().navigateTo("");
        });
        addComponent(button);

    }
}
 
```

Result in Enlighten:

![](../images/vaadin_MyView.png)

For this result the current extension adds or removed the following
Objects, Links and Properties:

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Icon</th>
<th class="confluenceTh" style="text-align: left;">Type of Object</th>
<th class="confluenceTh" style="text-align: left;">Name of Object</th>
<th class="confluenceTh" style="text-align: left;">With guid ending if
same fullname</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><p>Vaadin
View</p></td>
<td class="confluenceTd" style="text-align: center;">MyView</td>
<td class="confluenceTd" style="text-align: center;">N/A</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd" style="text-align: center;">Button.Click</td>
<td class="confluenceTd"
style="text-align: center;">parent.Button.Click_1</td>
</tr>
</tbody>
</table>

#### Created Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
GUID</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /> Vaadin View</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">MyView</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd"
style="text-align: left;">parent.Button.Click_1</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click
(Click_1)</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693565.png" draggable="false"
data-image-src="../images/645693565.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693565"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Lambda-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Lambda
Expression </p>
</div></td>
<td class="confluenceTd" style="text-align: left;">MyView$lambda$0</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
</tbody>
</table>

#### Removed Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
name</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">MyView</td>
<td class="confluenceTd" style="text-align: left;">Access Exec</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693565.png" draggable="false"
data-image-src="../images/645693565.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693565"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Lambda-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Lambda
Expression </p>
</div></td>
<td class="confluenceTd" style="text-align: left;">MyView$lambda$0</td>
</tr>
</tbody>
</table>

### Example of Support for View and Event Handler in Vaadin Flow with Method Reference

handlerMethod Expand source

``` java
package com.example.application.views.list;

import com.vaadin.flow.component.BlurNotifier;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.FocusNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route(value = "RefButton", layout = MainLayout.class)
public class ViewWithMethodReference extends VerticalLayout {

    public ViewWithMethodReference() {
        // Button 1 method ref within the Button component
        Button button1 = new Button("click me!",this::NotifTrigger);
        // Button 2 method ref with add*Listener
        Button button2 = new Button("Click and Look");
        // see the same nofitication handler for both buttons
        button2.addClickListener(this::NotifTrigger);
        button2.addFocusListener(this::RedBackground);
        button2.addBlurListener(this::GoBackToDefault);

        add(button1,button2);
    }

    private void NotifTrigger(ClickEvent<Button> event) {
        Notification.show("Clicked!");
    }
    private void RedBackground(FocusNotifier.FocusEvent<Button> event) {
        event.getSource().getStyle().set("background-color", "red");

    }

    private void GoBackToDefault(BlurNotifier.BlurEvent<Button> event) {
        event.getSource().getStyle().remove("background-color");
    }
}
```

Result in Enlighten:

![](../images/vaadinflow_handlerMethod.png)

For this result the current extension adds or removed the following
Objects, Links and Properties:

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Icon</th>
<th class="confluenceTh" style="text-align: left;">Type of Object</th>
<th class="confluenceTh" style="text-align: left;">Name of Object</th>
<th class="confluenceTh" style="text-align: left;">With GUID ending if
same fullname</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><p>Vaadin
View</p></td>
<td class="confluenceTd"
style="text-align: center;">ViewWithMethodReference</td>
<td class="confluenceTd" style="text-align: center;">N/A</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd" style="text-align: center;">Button.Click</td>
<td class="confluenceTd"
style="text-align: center;">parent.Button.Click_1</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd" style="text-align: center;">Button.Click</td>
<td class="confluenceTd"
style="text-align: center;">parent.Button.Click_2</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd" style="text-align: center;">Button.Focus</td>
<td class="confluenceTd"
style="text-align: center;">parent.Button.Focus_1</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd" style="text-align: center;">Button.Blur</td>
<td class="confluenceTd"
style="text-align: center;">parent.Button.Blur_1</td>
</tr>
</tbody>
</table>

#### Created Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
GUID</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">HomeView</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler </p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd"
style="text-align: left;">parent.Button.Click_1</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">HomeView</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd"
style="text-align: left;">parent.Button.Click_2</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">HomeView</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Focus</td>
<td class="confluenceTd"
style="text-align: left;">parent.Button.Focus_1</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">HomeView</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Blur</td>
<td class="confluenceTd"
style="text-align: left;">parent.Button.Blur_1</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click
(Click_1)</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">NotifTrigger</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click
(Click_2)</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">NotifTrigger</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Focus</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">RedBackground</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Blur</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">GoBackToDefault</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
</tbody>
</table>

#### Removed Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
name</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
</div></td>
<td class="confluenceTd"
style="text-align: left;">ViewWithMethodReference</td>
<td class="confluenceTd" style="text-align: left;">Mention</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">NotifTrigger</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
</div></td>
<td class="confluenceTd"
style="text-align: left;">ViewWithMethodReference</td>
<td class="confluenceTd" style="text-align: left;">Mention</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">RedBackground</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
</div></td>
<td class="confluenceTd"
style="text-align: left;">ViewWithMethodReference</td>
<td class="confluenceTd" style="text-align: left;">Mention</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">GoBackToDefault</td>
</tr>
</tbody>
</table>

### Example of Support for View and Event Handler with MethodOfAnonymousClass

handlerMethod Expand source

``` java
package org.example;

import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

public class AnonymousView extends VerticalLayout implements View {
    public AnonymousView() {
        // Create the content of your home page view
        Label label = new Label("Welcome to the Anonymous Page!");
        addComponent(label);
        // Create the content root layout for the UI
        HorizontalLayout content = new HorizontalLayout();

        // Display the greeting
        content.addComponent(new Label("Hello World!"));

        // Have a clickable button
        content.addComponent(new Button("Push Me!",
                click -> Notification.show("Pushed!")));

        Button button = new Button("Go to My View");
        button.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                // Handle the button click event in Vaadin 8.20
                // You can perform actions here
                getUI().getNavigator().navigateTo("my-view");

            }
        });
        addComponent(button);


        MyForm myform = new MyForm();
        addComponent(myform);

    }
}
```

Result in Enlighten:

![](../images/645693562.jpg)

For this result the current extension adds or removes the following
Objects, Links and Properties:

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Icon</th>
<th class="confluenceTh" style="text-align: left;">Type of Object</th>
<th class="confluenceTh" style="text-align: left;">Name of Object</th>
<th class="confluenceTh" style="text-align: left;">With GUID ending if
same fullname</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><p>Vaadin
View</p></td>
<td class="confluenceTd" style="text-align: center;">AnonymousView</td>
<td class="confluenceTd" style="text-align: center;">N/A</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd" style="text-align: center;">Button.Click</td>
<td class="confluenceTd"
style="text-align: center;">parent.Button.Click_1</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd" style="text-align: center;">Button.Click</td>
<td class="confluenceTd"
style="text-align: center;">parent.Button.Click_2</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Method of Anonymous
Class</td>
<td class="confluenceTd" style="text-align: center;">buttonClick</td>
<td class="confluenceTd"
style="text-align: center;">parent.buttonClick_1</td>
</tr>
</tbody>
</table>

#### Created Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
GUID</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd"
style="text-align: left;"><p>AnonymousView</p></td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd"
style="text-align: left;">parent.Button.Click_1</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd"
style="text-align: left;"><p>AnonymousView</p></td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd"
style="text-align: left;">parent.Button.Click_2</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693565.png" draggable="false"
data-image-src="../images/645693565.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693565"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Lambda-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Lambda
Expression </p>
</div></td>
<td class="confluenceTd"
style="text-align: left;">AnonymousView$lambda$0</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Method of
Anonymous Class</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">buttonClick</td>
<td class="confluenceTd"
style="text-align: left;">parent.buttonClick_1</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Method of
Anonymous Class</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">buttonClick</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">getUI</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
</tbody>
</table>

#### Removed Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
name</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">AnonymousView</td>
<td class="confluenceTd" style="text-align: left;">Access Exec</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693565.png" draggable="false"
data-image-src="../images/645693565.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693565"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Lambda-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Lambda
Expression </p>
</div></td>
<td class="confluenceTd"
style="text-align: left;">AnonymousView$lambda$0</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693564.png" draggable="false"
data-image-src="../images/645693564.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693564"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="JV_CTOR-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java
Constructor</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">AnonymousView</td>
<td class="confluenceTd" style="text-align: left;">Access Exec</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">getUI</td>
</tr>
</tbody>
</table>

### Complex binding between View, Event Handler and Action to perform.

ComboBoxView10.java Expand source

``` java
package org.example2.combobox;

import com.vaadin.data.HasValue;
import com.vaadin.navigator.View;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

import static org.example2.combobox.MyCustomComboBoxCls.MyCustomComboBox;

public class ComboBoxView10 extends VerticalLayout implements View {
    public ComboBoxView10() {

        final ComboBox<String> ComboBox10 = MyCustomComboBox("MyComboBox",ComboBoxCustomListener::myComboBoxListener);
        addComponent(ComboBox10);

    }

}
```

MyCustomComboBoxCls.java Expand source

``` java
package org.example2.combobox;

import com.vaadin.data.HasValue;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;

public class MyCustomComboBoxCls {

    public static ComboBox<String> MyCustomComboBox(final String caption, final HasValue.ValueChangeListener<String> clickListener ) {
        final ComboBox<String> cmbx = new ComboBox<>(caption);
        cmbx.setItems("Chrome", "Edge", "Firefox", "Safari");
        cmbx.setTextInputAllowed(true);
        cmbx.addValueChangeListener(clickListener);
        return cmbx;
    }

}
```

ComboBoxCustomListener.java Expand source

``` java
package org.example2.combobox;

import com.vaadin.data.HasValue;
import com.vaadin.ui.Notification;

public class ComboBoxCustomListener {
    public static void myComboBoxListener(HasValue.ValueChangeEvent<String> stringValueChangeEvent) {
        Notification.show("ComboBox 10");
    }

    public static HasValue.ValueChangeListener<String> myComboBoxListenerbis() {
        return e -> Notification.show("ComboBox11");
    }
}
```

Result in Enlighten:

![](../images/658014411.png)

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Icon</th>
<th class="confluenceTh" style="text-align: left;">Type of Object</th>
<th class="confluenceTh" style="text-align: left;">Name of Object</th>
<th class="confluenceTh" style="text-align: left;">With GUID ending if
same fullname</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;"><p>Vaadin
View</p></td>
<td class="confluenceTd" style="text-align: center;">ComboBoxView10</td>
<td class="confluenceTd" style="text-align: center;">N/A</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">Vaadin Event
Handler</td>
<td class="confluenceTd"
style="text-align: center;">ComboBox&lt;String&gt;.ValueChange</td>
<td class="confluenceTd"
style="text-align: center;">ComboBox&lt;String&gt;.ValueChange_1</td>
</tr>
</tbody>
</table>

#### Created Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
name</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
GUID</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911775.png" draggable="false"
data-image-src="../images/636911775.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911775"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_UI.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin View</p>
</div></td>
<td class="confluenceTd"
style="text-align: left;"><p>ComboBoxView10</p></td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd"
style="text-align: left;">ComboBox&lt;String&gt;.ValueChange</td>
<td class="confluenceTd"
style="text-align: left;">ComboBox&lt;String&gt;.ValueChange_1</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/636911774.png" draggable="false"
data-image-src="../images/636911774.png"
data-unresolved-comment-count="0" data-linked-resource-id="636911774"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Swing_EventHandler.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Vaadin Event
Handler</p>
</div></td>
<td class="confluenceTd" style="text-align: left;">Button.Click</td>
<td class="confluenceTd" style="text-align: left;">callLink</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<p><img src="../images/645693566.png" draggable="false"
data-image-src="../images/645693566.png"
data-unresolved-comment-count="0" data-linked-resource-id="645693566"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JAVA_MethodOfAnonymousClass-1.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="636911773"
data-linked-resource-container-version="1" width="32" />Java Method </p>
</div></td>
<td class="confluenceTd"
style="text-align: left;">myComboBoxListener</td>
<td class="confluenceTd" style="text-align: left;">N/A</td>
</tr>
</tbody>
</table>

#### Removed Links

None
