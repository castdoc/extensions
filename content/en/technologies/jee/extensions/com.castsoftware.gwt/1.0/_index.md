---
title: "Google Web Toolkit - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

{{% alert color="info" %}}This document provides basic information about the extension providing support for analyzing JEE applications (via the [JEE Analyzer](../../com.castsoftware.jee/)) that use the Google Web Toolkit (GWT) framework.{{% /alert %}}

## Extension ID

com.castsoftware.gwt

## What's new?

See [Release Notes](rn/).

## In what situation should you install this extension?

- If you have some imports starting with `com.google.gwt.` in your java code.
- If you have some files with the extensions `.gwt.xml` and `.ui.xml` with your java code.

## Supported Google Web Toolkit (GWT) framework items

- Modules present in files `*.gwt.xml`.
- Views present in files `*.ui.xml` and links to their handler methods present in java code.
- Client/server links created through `@RemoteServiceRelativePath` java interface annotation.

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in order to function correctly. The Google Web Toolkit extension
requires that the following other CAST extensions are also installed:

-   [JEE Analyzer](../../com.castsoftware.jee/)
-   [Web Services Linker](../../../../multi/com.castsoftware.wbslinker/)
-   com.castsoftware.internal.platform (internal extension)

The JEE Analyzer is not strictly a dependency, but since this extension is always installed, you do not need to do anything.

## What results can you expect?

### Objects

| Icon                         | Description                 |
|:----------------------------:|-----------------------------|
| ![](../images/669253736.png) | GWT Module                  |
| ![](../images/669253735.png) | GWT View                    |
| ![](../images/669253734.png) | GWT Activity                |
| ![](../images/669253733.png) | GWT Event Handler           |
| ![](../images/668337198.png) | GWT Remote resource service |
| ![](../images/668337198.png) | GWT Remote Operation        |
| ![](../images/668337198.png) | GWT Post resource service   |
| ![](../images/668337199.png) | GWT Get resource service    |
| ![](../images/668337197.png) | GWT Put resource service    |
| ![](../images/669253732.png) | GWT Delete resource service |

### Code examples

#### Modules

![](../images/669253731.png)

MobileWebApp.gwt.xml:

-   The "rename-to" value is used as the module name.
-   The "entry-point" tag is used for the class which contains the
    UI initialisation.

``` java
<module rename-to='mobilewebapp'>
  <!-- Inherit the core Web Toolkit stuff.                        -->
  <inherits name='com.google.gwt.activity.Activity'/>
...
  <!-- Specify the app entry point class.                         -->
  <entry-point class='com.google.gwt.sample.mobilewebapp.client.MobileWebApp'/>
...
</module>
```

MobileWebApp.java:

``` java
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;

public class MobileWebApp implements EntryPoint {
  public void onModuleLoad() {
    ClientFactory clientFactory = GWT.create(ClientFactory.class);
    clientFactory.getApp().run(RootLayoutPanel.get());
  }
}
```

Links from the module to the initialisation java methods are created.

#### Views and event handlers

![](../images/669253730.png)

View and java class have the same file names, but differ by their
extension (".ui.xml" for the view and ".java" for the class).
Handler and view are matched using "ui:field" value in view and
"@UiField" class members.

MobileTaskEditView.ui.xml (view code):

``` java
<ui:UiBinder
  xmlns:ui="urn:ui:com.google.gwt.uibinder"
  xmlns:g="urn:import:com.google.gwt.user.client.ui"
  xmlns:app="urn:import:com.google.gwt.sample.mobilewebapp.client.ui">
...
                <g:Button
                  ui:field="saveButton"
                  addStyleNames="{style.button}">Done</g:Button>
...
</ui:UiBinder>
```

MobileTaskEditView.java (Event handler code):

``` java
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;

public class MobileTaskEditView extends Composite implements TaskEditView {

  @UiField
  Button saveButton;

  public MobileTaskEditView() {
    initWidget(uiBinder.createAndBindUi(this));
    nameEditor = EditorDecorator.create(nameField.asEditor(), nameViolation);
    driver.initialize(this);

    saveButton.addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        if (presenter != null) {
          presenter.saveTask();
        }
      }
    });
  }
}
```

A view can be linked directly to a java method when the java method is
annotated with "@UiHandler":

![](../images/669253729.png)

ValidationView.ui.xml

``` java
<ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
  xmlns:g="urn:import:com.google.gwt.user.client.ui">
...
                <g:Button styleName="{style.close}" ui:field="closeButton">Close</g:Button>
...
</ui:UiBinder>
```

ValidationView.java

``` java
public class ValidationView extends Composite {
  @UiHandler("closeButton")
  public void doClick(ClickEvent e) {
    dialogBox.hide();
    sendButton.setEnabled(true);
    sendButton.setFocus(true);
  }
}
```

#### Activities

![](../images/669253728.png)

``` java
import com.google.gwt.activity.shared.AbstractActivity;

public class TraitementASideBarActivity extends AbstractActivity implements TraitementASideBarView.Presenter {

    public void start(AcceptsOneWidget panel, EventBus eventBus) {
    }
}
```

A callLink is created to the "start", "mayStop", "onCancel", "onStop"
class methods.

#### Remote resource services and remote operations

They are created when the interface "@RemoteServiceRelativePath"
annotation is present.

![](../images/669253727.png)

GreetingService.java

``` java
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
 
    SafeHtml greetServer(Person name);
  
}
```

GreetingServiceImpl.java

``` java
import com.google.gwt.sample.validation.client.GreetingService;

public class GreetingServiceImpl extends RemoteServiceServlet implements
    GreetingService {
  public SafeHtml greetServer(Person person) {
        ...
    }
}
```

The operation is created from the class implementing the interface. The
difficult part is to find from where the resource service is called, and
because there is no class implementing the interface on the client side,
an instance is created directly from the interface using GWT.create.
For example:

``` java
GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
...
greetingService.greetServer(...);
```

### Support of RestyGWT

![](../images/669253726.png)

``` java
import org.fusesource.restygwt.client.RestService;

public interface IDetailServiceAsync extends RestService {

    @GET
    @Path("resupererDossier")
    public void listeContrat(...);

    @POST
    @Path("verificationPerimetreContrat/{codeOfrCial}/{codeSysInfo}/{codeAppli}")
    public void verificationPerimetreContrat(...);
}
```

### Support of AsyncProvider

Classes which inherit from "com.google.gwt.core.client.AsyncProvider"
have a "get" method. A link is created from their constructor to this
method.

![](../images/669253725.png)

``` java
import com.google.gwt.core.client.AsyncProvider;
 
public class AnalyseDemandeAsyncProvider implements AsyncProvider<Activity, Throwable> {
    @Override
    public void get(final Callback<? super Activity, ? super Throwable> callback) {
    }
}
```

## Limitations

- Resource services corresponding to `com.google.gwt.http.client.RequestBuilder.sendRequest` and using `com.google.gwt.http.client.RequestBuilder`.
