---
title: "ORMLite - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.ormlite

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for ORMLite framework for interactions
between Java code and RDBMS via CRUD operations.

Note that
[com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest)
provides full support for JPA and Hibernate, however, this extension
provides additional support and should be used for any application
using ORMLite libraries.

## In what situation should you install this extension?

If your Java application uses a supported ORMLite framework and you want
to view these object types and their links, then you should install this
extension. More specifically the extension will identify:

-   "callLinks" from Java methods to ORMLite Entity Operation objects.
-   "callLinks" from Java methods to ORMLiteSQL Query objects.

## Technology support

| Library name | Version    |   Supported   | Supported Technology |
|--------------|------------|:-------------:|:--------------------:|
| ORMLite      | 1.0 to 6.1 | :white_check_mark: |         Java         |

## Supported persistence libraries

### Supported operations

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: left;">Operation</th>
<th class="confluenceTh" style="text-align: left;"><div
class="content-wrapper">
<p>Methods Supported</p>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Select</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>com.j256.ormlite.dao.Dao.extractId</li>
<li>com.j256.ormlite.dao.Dao.refresh</li>
<li>com.j256.ormlite.dao.Dao.iterator</li>
<li>com.j256.ormlite.dao.Dao.getWrappedIterable</li>
<li>com.j256.ormlite.dao.Dao.objectsEqual</li>
<li>com.j256.ormlite.dao.Dao.getDataClass</li>
<li>com.j256.ormlite.dao.Dao.indForeignFieldType</li>
<li>com.j256.ormlite.dao.Dao.isUpdatable</li>
<li>com.j256.ormlite.dao.Dao.isTableExists</li>
<li>com.j256.ormlite.dao.Dao.countOf</li>
<li>com.j256.ormlite.dao.Dao.assignEmptyForeignCollection</li>
<li>com.j256.ormlite.dao.Dao.getEmptyForeignCollection</li>
<li>com.j256.ormlite.dao.Dao.setObjectCache</li>
<li>com.j256.ormlite.dao.Dao.mapSelectStarRow</li>
<li>com.j256.ormlite.dao.Dao.getSelectStarRowMapper</li>
<li>com.j256.ormlite.dao.Dao.idExists</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.iterator</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.closeableIterator</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getWrappedIterable</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.extractId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.countOf</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.mapSelectStarRow</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getSelectStarRowMapper</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.idExists</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getTableName</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getTableInfo</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Insert</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>com.j256.ormlite.dao.Dao.create</li>
<li>com.j256.ormlite.dao.Dao.createIfNotExists</li>
<li>com.j256.ormlite.dao.Dao.createOrUpdate</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.create</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.createIfNotExists</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.createOrUpdate</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Update</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>com.j256.ormlite.dao.Dao.update</li>
<li>com.j256.ormlite.dao.Dao.updateId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.update</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.updateId</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">Delete</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>com.j256.ormlite.dao.Dao.delete</li>
<li>com.j256.ormlite.dao.Dao.deleteById</li>
<li>com.j256.ormlite.dao.Dao.deleteIds</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.delete</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.deleteById</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.deleteIds</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">Query Based</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>com.j256.ormlite.dao.Dao.queryRaw</li>
<li>com.j256.ormlite.dao.Dao.query</li>
<li>com.j256.ormlite.dao.Dao.queryRawValue</li>
<li>com.j256.ormlite.dao.Dao.executeRaw</li>
<li>com.j256.ormlite.dao.Dao.executeRawNoArgs</li>
<li>com.j256.ormlite.stmt.QueryBuilder.query</li>
<li>com.j256.ormlite.stmt.QueryBuilder.queryRaw</li>
<li>com.j256.ormlite.dao.Dao.queryForId</li>
<li>com.j256.ormlite.dao.Dao.queryForFirst</li>
<li>com.j256.ormlite.dao.Dao.queryForAll</li>
<li>com.j256.ormlite.dao.Dao.queryForEq</li>
<li>com.j256.ormlite.dao.Dao.queryForMatching</li>
<li>com.j256.ormlite.dao.Dao.queryForMatchingArgs</li>
<li>com.j256.ormlite.dao.Dao.queryForFieldValues</li>
<li>com.j256.ormlite.dao.Dao.queryForFieldValuesArgs</li>
<li>com.j256.ormlite.dao.Dao.queryForSameId</li>
<li>com.j256.ormlite.stmt.QueryBuilder.queryForFirst</li>
<li>com.j256.ormlite.stmt.QueryBuilder.queryRawFirst</li>
<li>com.j256.ormlite.stmt.QueryBuilder.iterator</li>
<li>com.j256.ormlite.stmt.QueryBuilder.countOf</li>
<li>com.j256.ormlite.stmt.DeleteBuilder.delete</li>
<li>com.j256.ormlite.stmt.UpdateBuilder.update</li>
<li>com.j256.ormlite.dao.Dao.updateRaw</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForFirst</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForAll</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForEq</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForMatching</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForMatchingArgs</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForFieldValues</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForFieldValuesArgs</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForSameId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.query</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryRaw</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryRawValue</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryRaw</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.executeRaw</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.executeRawNoArgs</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.updateRaw</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

### Supported ORMLite Annotations

-   DatabaseTable

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For Java applications using any of the above mentioned libraries, this
extension will be automatically installed by CAST Imaging Console.
This is in place since October 2023. For upgrade, if the Extension
Strategy is not set to Auto update, you can manually upgrade the
extension using the Application - Extensions interface.

## What results can you expect?

Once the analysis/snapshot generation is completed, you can view the
results. The following objects and links will be displayed:

### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
<th class="confluenceTh">Comment</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669614288.png" draggable="false"
data-image-src="../images/669614288.png"
data-unresolved-comment-count="0" data-linked-resource-id="669614288"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_JPA_Entity.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="663879871"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ORMLite Entity</td>
<td class="confluenceTd">an object is created when @DatabaseTable
annotation is encountered</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669614287.png" draggable="false"
data-image-src="../images/669614287.png"
data-unresolved-comment-count="0" data-linked-resource-id="669614287"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_JPA_Entity_Operation.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="663879871"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ORMLite Entity Operation</td>
<td class="confluenceTd">an object is created for each CRUD operation
performed on Entity</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669614286.png" draggable="false"
data-image-src="../images/669614286.png"
data-unresolved-comment-count="0" data-linked-resource-id="669614286"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_JPA_SQLQuery.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="663879871"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ORMLite SQL Query</td>
<td class="confluenceTd">an object is created for each native SQL query
found and resolved in a  method call</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/ORMLite_Unknown_Entity.png" draggable="false"
data-image-src="../images/ORMLite_Unknown_Entity.png"
data-unresolved-comment-count="0" data-linked-resource-id="ORMLite_Unknown_Entity"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Unknown_ORMLite_Entity.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="663879871"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ORMLite Unknown Entity</td>
<td class="confluenceTd">an object is created when entity cannot be resolved</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/ORMLite_UnknownEntity_Operation.png" draggable="false"
data-image-src="../images/ORMLite_UnknownEntity_Operation.png"
data-unresolved-comment-count="0" data-linked-resource-id="ORMLite_UnknownEntity_Operation"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Unknown_ORMLite_Entity_Operation.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="663879871"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ORMLite Unknown Entity Operation</td>
<td class="confluenceTd">an object is created when CRUD operation is performed and Entity cannot be resolved</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/ORMLite_unknown_sql_query.png" draggable="false"
data-image-src="../images/ORMLite_unknown_sql_query.png"
data-unresolved-comment-count="0" data-linked-resource-id="ORMLite_unknown_sql_query"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_Java_Unknown_ORMLite_SQLQuery.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="663879871"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ORMLite Unknown SQL Query</td>
<td class="confluenceTd">an object is created for each SQL query found and the exact query cannot be resolved</td>
</tr>
</tbody>
</table>

### Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>Link Type</p></th>
<th class="confluenceTh">Caller type</th>
<th class="confluenceTh" style="text-align: left;"><p>Callee
type</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Supported
APIs</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>callLink </p>
<p><br />
</p></td>
<td class="confluenceTd">Java Method</td>
<td class="confluenceTd" style="text-align: left;"><p>ORMLite Entity
Operation</p></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>com.j256.ormlite.dao.Dao.create</li>
<li>com.j256.ormlite.dao.Dao.createIfNotExists</li>
<li>com.j256.ormlite.dao.Dao.createOrUpdate</li>
<li>com.j256.ormlite.dao.Dao.extractId</li>
<li>com.j256.ormlite.dao.Dao.refresh</li>
<li>com.j256.ormlite.dao.Dao.iterator</li>
<li>com.j256.ormlite.dao.Dao.getWrappedIterable</li>
<li>com.j256.ormlite.dao.Dao.objectsEqual</li>
<li>com.j256.ormlite.dao.Dao.getDataClass</li>
<li>com.j256.ormlite.dao.Dao.indForeignFieldType</li>
<li>com.j256.ormlite.dao.Dao.isUpdatable</li>
<li>com.j256.ormlite.dao.Dao.isTableExists</li>
<li>com.j256.ormlite.dao.Dao.countOf</li>
<li>com.j256.ormlite.dao.Dao.assignEmptyForeignCollection</li>
<li>com.j256.ormlite.dao.Dao.getEmptyForeignCollection</li>
<li>com.j256.ormlite.dao.Dao.setObjectCache</li>
<li>com.j256.ormlite.dao.Dao.mapSelectStarRow</li>
<li>com.j256.ormlite.dao.Dao.getSelectStarRowMapper</li>
<li>com.j256.ormlite.dao.Dao.idExists</li>
<li>com.j256.ormlite.dao.Dao.delete</li>
<li>com.j256.ormlite.dao.Dao.deleteById</li>
<li>com.j256.ormlite.dao.Dao.deleteIds</li>
<li>com.j256.ormlite.dao.Dao.update</li>
<li>com.j256.ormlite.dao.Dao.updateId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.create</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.createIfNotExists</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.createOrUpdate</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.update</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.updateId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.update</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.delete</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.deleteById</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.deleteIds</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.iterator</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.closeableIterator</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getWrappedIterable</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.extractId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.countOf</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.mapSelectStarRow</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getSelectStarRowMapper</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.idExists</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getTableName</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.getTableInfo</li>
</ul>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">callLink </td>
<td class="confluenceTd">Java Method</td>
<td class="confluenceTd" style="text-align: left;">callLink between the
caller Java Method and ORMLite Query object</td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>com.j256.ormlite.dao.Dao.queryForId</li>
<li>com.j256.ormlite.dao.Dao.queryForFirst</li>
<li>com.j256.ormlite.dao.Dao.queryForAll</li>
<li>com.j256.ormlite.dao.Dao.queryForEq</li>
<li>com.j256.ormlite.dao.Dao.queryForMatching</li>
<li>com.j256.ormlite.dao.Dao.queryForMatchingArgs</li>
<li>com.j256.ormlite.dao.Dao.queryForFieldValues</li>
<li>com.j256.ormlite.dao.Dao.queryForFieldValuesArgs</li>
<li>com.j256.ormlite.dao.Dao.queryForSameId</li>
<li>com.j256.ormlite.stmt.QueryBuilder.queryForFirst</li>
<li>com.j256.ormlite.stmt.QueryBuilder.queryRawFirst</li>
<li>com.j256.ormlite.stmt.QueryBuilder.iterator</li>
<li>com.j256.ormlite.stmt.QueryBuilder.countOf</li>
<li>com.j256.ormlite.stmt.UpdateBuilder.update</li>
<li>com.j256.ormlite.dao.Dao.updateRaw</li>
<li>com.j256.ormlite.dao.Dao.queryRaw</li>
<li>com.j256.ormlite.dao.Dao.query</li>
<li>com.j256.ormlite.dao.Dao.queryRawValue</li>
<li>com.j256.ormlite.dao.Dao.executeRaw</li>
<li>com.j256.ormlite.dao.Dao.executeRawNoArgs</li>
<li>com.j256.ormlite.stmt.QueryBuilder.query</li>
<li>com.j256.ormlite.stmt.QueryBuilder.queryRaw</li>
<li>com.j256.ormlite.stmt.DeleteBuilder.delete</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForFirst</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForAll</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForEq</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForMatching</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForMatchingArgs</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForFieldValues</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForFieldValuesArgs</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryForSameId</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.query</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryRaw</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryRawValue</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.queryRaw</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.executeRaw</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.executeRawNoArgs</li>
<li>com.j256.ormlite.dao.RuntimeExceptionDao.updateRaw</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useLink</td>
<td class="confluenceTd" style="text-align: left;">ORMLite Entity
Operation</td>
<td class="confluenceTd" style="text-align: left;">Table / View</td>
<td class="confluenceTd" style="text-align: left;">Created by <a
href="https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&amp;version=latest"
rel="nofollow">com.castsoftware.wbs</a> when DDL source files are
analyzed by SQL Analyzer</td>
</tr>
</tbody>
</table>

## Code examples

### CRUD Operations

#### ORMLite Entity

``` java
package com.spring.ormlite.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "record")
public class GeneralRecord {
    
    @DatabaseField(id = true)
    int id;

    @DatabaseField
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeneralRecord(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public GeneralRecord() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public String toString() {
        return "GeneralRecord [id=" + id + ", name=" + name + "]";
    }
    
    
}
```

![](../images/663879874.png)


#### Add operation

``` java
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.jdbc.db.MysqlDatabaseType;
import com.j256.ormlite.stmt.*;
import com.j256.ormlite.support.ConnectionSource;

public class generateGeneralApp{

public static void generateGeneralRecord() throws Exception {
          String databaseUrl = "jdbc:mysql://localhost:3306/ormlite";
            //String databaseUrl = "jdbc:h2:~/test";
            ConnectionSource connectionSource =
                    new JdbcConnectionSource(databaseUrl,"root","root",new MysqlDatabaseType());

            System.out.println("Connection successfull");
            
            Dao<GeneralRecord,String> recordDao =
                    DaoManager.createDao(connectionSource, GeneralRecord.class);

            TableUtils.createTableIfNotExists(connectionSource, GeneralRecord.class);

           int name = 2;        
           GeneralRecord record = new GeneralRecord(name, "record "+name);


           recordDao.create(record);
           connectionSource.close();
    }
}
```

![](../images/663879883.png)

### Add operation through android application

``` java
package com.example.helloandroid;

import java.util.List;
import java.util.Random;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;
import com.j256.ormlite.android.apptools.OrmLiteBaseActivity;
import com.j256.ormlite.dao.RuntimeExceptionDao;

/**
 * Sample Android UI activity which displays a text window when it is run.
 */
public class HelloAndroid extends OrmLiteBaseActivity<DatabaseHelper> {

	private final String LOG_TAG = getClass().getSimpleName();
	private final static int MAX_NUM_TO_CREATE = 8;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(LOG_TAG, "creating " + getClass() + " at " + System.currentTimeMillis());
		TextView tv = new TextView(this);
		tv.setMovementMethod(new ScrollingMovementMethod());
		doSampleDatabaseStuff("onCreate", tv);
		setContentView(tv);
	}

	/**
	 * Do our sample database stuff.
	 */
	private void doSampleDatabaseStuff(String action, TextView tv) {
		// get our dao
		RuntimeExceptionDao<SimpleData, Integer> simpleDao = getHelper().getSimpleDataDao();
		// query for all of the data objects in the database
		List<SimpleData> list = simpleDao.queryForAll();
		// our string builder for building the content-view
		StringBuilder sb = new StringBuilder();
		sb.append("Found ").append(list.size()).append(" entries in DB in ").append(action).append("()\n");
		
		int createNum;
		do {
			createNum = new Random().nextInt(MAX_NUM_TO_CREATE) + 1;
		} while (createNum == list.size());
		sb.append("Creating ").append(createNum).append(" new entries:\n");
		for (int i = 0; i < createNum; i++) {

			long millis = System.currentTimeMillis();
			SimpleData simple = new SimpleData(millis);
			simpleDao.create(simple);
			Log.i(LOG_TAG, "created simple(" + millis + ")");
			
		}
	}
}
```

![](../images/android_add_operation.png)

#### Update Operation


``` java
public static void updateSchoolData() throws SQLException {
        
        String databaseUrl = "jdbc:mysql://localhost:3306/ormlite";
        ConnectionSource connectionSource =
                new JdbcConnectionSource(databaseUrl,"root","root",new MysqlDatabaseType());
        
        Dao<School,String> schoolDao =
                DaoManager.createDao(connectionSource, School.class);

         School s = new School("5","sunbeam");  
            
           schoolDao.updateId(s,"8");
        
    }
```

![](../images/663879875.png)

#### Delete operation

``` java
public static void deleteSchoolData() throws SQLException {
           
        String databaseUrl = "jdbc:mysql://localhost:3306/ormlite";
        ConnectionSource connectionSource =
                new JdbcConnectionSource(databaseUrl,"root","root",new MysqlDatabaseType());
        
        Dao<School,String> schoolDao =
                DaoManager.createDao(connectionSource, School.class);
           schoolDao.deleteById("8");
           
        
    }
```

![](../images/663879882.png)

#### Select operation

``` java
public class AccountApp {
     public static void main(String[] args) throws Exception {

            String databaseUrl = "jdbc:mysql://localhost:3306/ormlite";
            ConnectionSource connectionSource =
                    new JdbcConnectionSource(databaseUrl,"root","root",new MysqlDatabaseType());
            Dao<Account,String> accountDao =
                    DaoManager.createDao(connectionSource, Account.class);
            
            querywithWhereSchool(accountDao);
            connectionSource.close();
        }
      
     public static void querywithWhereSchool(Dao<Account,String> accountDao) throws SQLException {

                Account s=new Account("10","ava");
                String id = accountDao.extractId(s);
                        
                System.out.println("account id  "+id);
                
            }
}
```

![](../images/669614290.png)

#### Unknown Entity CRUD

``` java
public class GenericDao<T, ID> {

    private Dao<T, ID> dao;
    public GenericDao(ConnectionSource connectionSource, Class<T> entityClass) throws SQLException {
        // Create a DAO for the given entity class
        this.dao = DaoManager.createDao(connectionSource, entityClass);
    }

    public void deleteById(ID id) throws SQLException {
        DeleteBuilder<T, ID> deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq("id", id);
        deleteBuilder.delete();
    }
}
```

![](../images/Unknown_Entity_crud.png)

### ORMLite Query

#### Raw SQL Query

``` java
public static void rawquery() throws SQLException {
        String query = "select * from libraries where libraryId = 1";
        rawSqlQuery(query);
    }
    
    public static void rawSqlQuery(String query) throws SQLException {
        
         String databaseUrl = "jdbc:mysql://localhost:3306/ormlite";
         ConnectionSource connectionSource =
                 new JdbcConnectionSource(databaseUrl,"root","root",new H2DatabaseType());
   
         Dao<Libraries,String> accountDao =
                 DaoManager.createDao(connectionSource, Libraries.class);
         
        GenericRawResults<String[]> rawResults =
               accountDao.queryRaw(query);
             // there should be 1 result
             List<String[]> results = rawResults.getResults();
             // the results array should have 1 value
             String[] resultArray = results.get(0);
             // this should print the number of orders that have this account-id
             System.out.println("id " + resultArray[0] + " value " + resultArray[1]);
             
    }
```

![](../images/669614289.png)

#### Custom SQL Query

``` java
public class UserApp {

    public static void main(String[] args) throws Exception {
        String databaseUrl = "jdbc:mysql://localhost:3306/ormlite";
        ConnectionSource connectionSource =
                new JdbcConnectionSource(databaseUrl,"root","root",new MysqlDatabaseType());
        Dao<User,String> userDao =
                DaoManager.createDao(connectionSource, User.class);

        queryUser(userDao);
        
        connectionSource.close();
    }

    public static void queryUser(Dao<User, String> schoolDao) throws SQLException {

        QueryBuilder<User, String> queryBuilder =
                schoolDao.queryBuilder();
        queryBuilder.selectColumns(User.name_FIELD_NAME);
        queryBuilder.groupBy(User.name_FIELD_NAME);
        queryBuilder.orderBy(User.name_FIELD_NAME,false);
        queryBuilder.limit(1L);
        List<User> accountList = queryBuilder.query();
    }
```

![](../images/669254343.png)

#### Unknown SQL Query

``` java
public class GenericDao<T, ID> {

    private Dao<T, ID> dao;

    public GenericDao(ConnectionSource connectionSource, Class<T> entityClass) throws SQLException {
        // Create a DAO for the given entity class
        this.dao = DaoManager.createDao(connectionSource, entityClass);
    }
    public void deleteById(ID id) throws SQLException {
        DeleteBuilder<T, ID> deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq("id", id);
        deleteBuilder.delete();
    }
}
```

![](../images/ORMLite_unknown_sql_querry.png)

## Limitation

-   Unknown Objects will be created if the evaluation fails to resolve the
    necessary parameters.
