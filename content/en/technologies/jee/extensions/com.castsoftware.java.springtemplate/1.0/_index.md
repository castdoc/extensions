---
title: "Spring Template - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension Id

com.castsoftware.java.springtemplate

## What's new?

See [Release Notes](rn/).

## Description

Spring Template Extension provide support for Spring Web MVC applications, which uses Apache FreeMarker or Apache Velocity.

## In what situation should you install this extension?

If your Java Spring Web MVC application uses the Apache FreeMarker or Apache Velocity Framework.

## Technology support

| Component | Version | Supported | Supported Technology |
|:----------|:--------|:---------:|:--------------------:|
| spring-webmvc | ≤ 4.3.x | :white_check_mark: | JAVA |
| spring-context | ≤ 4.3.x | :white_check_mark: | JAVA |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, you should manually install the
extension using the Application - Extensions interface.

## What results can you expect?

### Objects


| Icon  | Description |
|---------|---------|
| ![](../images/stp_icon.png) | Java Spring Template Parameter |

### Links

| Link Type | Source and destination of link | Supported Velocity Engine Core Methods |
|---|---|---|
| relyonLink | Between CAST Java Spring Template Parameter and respective Context JAVA class. |<details open><summary>Model Attribute</summary>org.springframework.ui.Model.addAttribute<br>org.springframework.ui.Model.addAllAttributes<br>org.springframework.ui.Model.mergeAttributes<br>org.springframework.ui.ModelMap.addAttribute<br>org.springframework.ui.ModelMap.addAllAttributes<br>org.springframework.ui.ModelMap.mergeAttributes</details><br>**_NOTE:_**  Java Spring Template Parameter object will be created, if the Model Attribute APIs are called from Spring Controller methods which returns template name string |
| callLink  | Between HTML source code objects and Java Velocity Call To VTLContext or Java Freemarker FTL Call objects. |  |
| callLink | Between Java Velocity Call To VTLContext or Java Freemarker FTL Call objects and JAVA methods. |  |

## Code Examples

### Velocity Spring

<details><summary>Context Class - Tutorial.java</summary>

``` Java
package com.baeldung.mvc.velocity.domain;

public class Tutorial {

    private final Integer tutId;
    private final String title;
    private final String description;
    private final String author;

    public Tutorial(Integer tutId, String title, String description, String author) {
        this.tutId = tutId;
        this.title = title;
        this.description = description;
        this.author = author;
    }

    public Integer getTutId() {
        return tutId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }

}
```
</details>

<details><summary>Spring Controller Class - MainController.java</summary>

``` Java
package com.baeldung.mvc.velocity.controller;

import com.baeldung.mvc.velocity.domain.Tutorial;
import com.baeldung.mvc.velocity.service.ITutorialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/")
public class MainController {

    @Autowired
    private ITutorialsService tutService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcomePage() {
        return "index";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listTutorialsPage(Model model) {
        List<Tutorial> list = tutService.listTutorials();
        model.addAttribute("tutorials", list);
        return "list";
    }

    public ITutorialsService getTutService() {
        return tutService;
    }

    public void setTutService(ITutorialsService tutService) {
        this.tutService = tutService;
    }

}
```
</details>

<details><summary>Velocity Template - list.vm</summary>

``` Html
<h1>Index</h1>
 
<h2>Tutorials list</h2>
<table border="1">
<tr>
 <th>Tutorial Id</th>
 <th>Tutorial Title</th>
 <th>Tutorial Description</th>
 <th>Tutorial Author</th>
</tr>
#foreach($tut in $tutorials)
  <tr>
   <td id="tutId_$foreach.count">$tut.tutId</td>
   <td id="title_$foreach.count">$tut.title</td>
   <td id="desc_$foreach.count">$tut.description</td>
   <td id="auth_$foreach.count">$tut.author</td>
  </tr>
#end
</table>
```
</details>

![](../images/spring_mvc.png)
