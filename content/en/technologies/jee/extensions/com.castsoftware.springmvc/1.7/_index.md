---
title: "Spring MVC - 1.7"
linkTitle: "1.7"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.springmvc

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Spring MVC.

### In what situation should you install this extension?

The main purpose of this extension is to create HTTP API entry points
for JEE back-end applications that are used to create REST API
Services. If you need to have links from a Client Front-end (see
examples below) to the Spring MVC/JEE Back-end you should install
this extension.

<table>
  <tbody>
    <tr>
      <td>
        <p><img src="../images/669254469.png" width="500" alt="669254469"></p>
      </td>
      <td>
        <p><img src="../images/669254468.png" width="500" alt="669254468"></p>
      </td>
    </tr>
    <tr>
      <td>iOS Front-End Example</td>
      <td>AngularJS Front-End Example</td>
    </tr>
  </tbody>
</table>

### Introduction

The SpringMVC framework is responsible of receiving all incoming HTTP
requests and processing them by passing the requests to the relevant
java component (controllers). The Spring analyzer will create Spring MVC
Operation objects to represent these web services and links to Java
objects to represent the posterior processing.

In SpringMVC, the rules mapping requests (URLs) to particular actions
(methods inside controllers) can be configured with the help of handler
mappings. The most common are:

| Handler mapping | Supported |
|---|---|
| RequestMappingHandlerMapping      | :white_check_mark: |
| BeanNameUrlHandlerMapping         | :white_check_mark: |
| SimpleUrlHandlerMapping           | :white_check_mark: |
| ControllerClassNameHandlerMapping | :white_check_mark: |

RequestMappingHandlerMapping uses annotations to configuration web
services configuration. The rest are usually configured via .xml files. 

The supported controllers are:

AbstractController, BaseCommandController, AbstractFormController, SimpleFormController, CancellableFormController, AbstractWizardFormController,
AbstractCommandController,   
AbstractUrlViewController, UrlFilenameViewController, ParameterizableViewController, ServletForwardingController, ServletWrappingController, MultiActionController (we
highlight in red the deprecated controllers as of Spring 3.0, in favor
of annotated controllers).

We further support mapping via resolution of method names and query
parameters (methodNameResolver, and ParameterMethodNameResolver).

### Features

This extension handles Spring MVC Web Services used in JEE applications,
for example:

``` java
@RequestMapping("/users")
public class UserController {
     
    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public PagedResources<UserResource> collectionList(...){
       ...
    }
```

For each class annotated with

-   org.springframework.web.bind.annotation.RequestMapping
    (@RequestMapping )
-   org.springframework.web.bind.annotation.GetMapping (@GetMapping
    )

<!-- -->

-   org.springframework.web.bind.annotation.PostMapping (@PostMapping
    )
-   org.springframework.web.bind.annotation.PutMapping (@PutMapping
    )

<!-- -->

-   org.springframework.web.bind.annotation.DeleteMapping
    (@DeleteMapping )

-   org.springframework.web.bind.annotation.PatchMapping
    (@PatchMapping ),

the following will be done:

-   a Web Service object will be created
-   a Web Service Port object child of the web service will be created
-   for each Method annotated with "Annotation"Mapping, the following
    will be created:  
    -   a Web Service operation child of the Web Service Port with
        correct get/put/delete/post type
    -   a fire link from the Web Service operation to the method

#### Basic case @RequestMapping

The following Java code

``` java
@RequestMapping("/home")
public class HomeController {
     
    @RequestMapping(value="/method0")
    @ResponseBody
    public String method0(){
        return "method0";
    }

}
```

will generate:

![](../images/669254467.png)

#### Several urls

The following Java code:

``` java
@RequestMapping("/home")
public class HomeController {
     
    @RequestMapping(value={"/method1","/method1/second"})
    @ResponseBody
    public String method1(){
        return "method1";
    }

}
```

will generate one operation per url mapping:

![](../images/669254466.png)

#### Several methods

The following Java code:

``` java
@RequestMapping("/home")
public class HomeController {
     
    @RequestMapping(value="/method3", method={RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public String method3(){
        return "method3";
    }
}
```

will generate one operation per receiving method:

![](../images/669254465.png)

#### Spring 4 annotations

The syntax @GetMapping, @PostMapping, @PutMapping,
@DeleteMapping, @PatchMapping are supported. Example with the following
Java code:

``` java
@RestController
public class CustomerRestController {
     
    @GetMapping("/customers")
    public List getCustomers() {
        // ...
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity deleteCustomer(@PathVariable Long id) {
        // ...
    }
}
```

will generate:

![](../images/669254464.png)

#### Property evaluation

 Properties used inside annotations are evaluated by looking them into
properties file. Example with the following Java code:

``` java
@RequestMapping("${my.home}")
public class HomeController {
     
    @RequestMapping(value="${my.service1}")
    @ResponseBody
    public String method0(){
        return "method0";
    }

}
```

With properties file:

``` java
my.home=/home
my.service1=/method0
```

will generate:

![](../images/669254467.png)

SimpleUrlHandlerMapping

The SimpleUrlHandlerMapping mapping class allows to specify the mapping
of URL patterns to handlers (methods of Controllers). This class can be
declared using XML bean definitions in at least three different ways as
shown below (all of them being supported). Only the servlet XML files
explicitly or implictly (default) referenced in the web.xml deployment
configuration file are considered.  For each URL path we will create a
different Spring MVC Any Operation object. 

###### Method 1

``` xml
<beans ...>
    <bean class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping">
    <property name="urlMap">
        <map>
                <entry key="/home.htm">
                  <ref local="homeController"/>
                </entry>
               ...
         </map>
       </property>
    </bean>

    <bean id="homeController" class="com.castsoftware.example.HomeController" />      
</beans>
```

###### Method 2

``` xml
<beans ...>    
    <bean class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping">
       <property name="mappings">
        <value>
           /home.htm=homeController
           ...
        </value>
       </property>
    </bean>
    
    <bean id="homeController" class="com.castsoftware.example.HomeController" />      
</beans>
```

###### Method 3

``` xml
<beans ...>    
    <bean class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping">
       <property name="mappings">
        <props>
            <prop key="home.htm">homeController</prop>
            ...
        </props>
       </property>
    </bean>
    
    <bean id="homeController" class="com.castsoftware.example.HomeController" />      
</beans>
```

In addition we can have further mapping to specific methods of a
Controller through the methodNameResolver property.

``` xml
<bean id="manageSomeController"
    class="com.cingular.km.lbs.gearsadmin.web.controller.DTVAddressDataController ">
    ...
    <property name="methodNameResolver">
       <bean
         class="org.springframework.web.servlet.mvc.multiaction.PropertiesMethodNameResolver">
         <property name="mappings">
           <props>
             <prop key="/*/deleteAddress">deleteAddress</prop>
           ...
```

  

###### Creation of links between web service operations and controller methods

When defining a URL mapping to a given controller class, each type of
controller (determined by its ancestors) can have a set of overridden
methods to further customize the response behavior to a URL loading.
These methods are usually called by the framework itself (such as
*onSubmit* in the *SimpleFormController*). The
*com.castsoftware.springmvc* analyser will create a callLink from
the *Spring MVC Any Operation* object and to each of the overridden
methods of the corresponding controller. Potential overriding of default
intra-method calls is ignored. When using the XML
property PropertiesMethodNameResolver, the CallLinks are created to the
explicitly referenced callback methods (custom methods added to the
controller class). For example, returning to the code snippet above
illustrating the use of the *methodNameResolver* property, we can
observe a reference to the method of the controller defined below:

DTVAddressDataController.java

``` java
public class DTVAddressDataController extends MultiActionController {


    public ModelAndView deleteAddress(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ....
    }
    ...
}
```

The corresponding link would appear as below:

![](../images/669254463.png)

Note that the Spring *MVC Any Operation* will be created below the same
Java File (denoted by the Belong link) where the referenced
controller is found. In addition to the above mentioned links, a
*callLink* from the *Spring MVC Any Operation* object to the
controller class' constructor method (if present) will be created when
using the above mentioned Method 1-2-3 mapping approaches.

#### Support of BeanNameUrlHandlerMapping, ControllerClassNameHandlerMapping

These similarly work by configuring web services via xml files (web.xml,
dispatcher-servlet.xml, ...) as SimpleUrlHandlerMapping (see above),
but with different rules. 

#### Support of thymeleaf

The following syntaxes are supported for thymeleaf templating:

``` java
      <form ... th:action="@{/seedstartermng}"... method="post">
```

Will create a link from the HTML5 content to a call to a webservice with
url '/seedstartermng'

![](../images/669254461.png)

'th:href' are also supported:

``` java
<... th:href="@{/css/stsm.css}"/>
```

![](../images/669254460.png)

#### Support for User Input Security

Service entry points are created automatically for applications that
have a presentation layer based on SpringMVC with @RequestMapping (and
associated annotations) usage. This can be seen in the analysis log file
as follows:

``` text
2018-09-13 10:00:40,148 INFO SecurityAnalyzer.FlawAnalysisEnvironment LoadBlackboxesForApplication cast#spec cast#lib SpringMVCServiceEntryPoints
```

This corresponds to the generation of a file in the following location:

``` text
<BytecodeFolder>\com.castsoftware.springmvc\ServiceEntryPoints.blackbox.xml
```

Note that while the ServiceEntryPoints.blackbox.xml file is generated
when the extension is used with any release of CAST AIP, it will only be
exploited by the CAST User Input Security feature in CAST AIP ≥ 8.3.3.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Spring MVC extension requires that
the following other CAST extensions are also installed:

-   Web services linker service (internal technical extension)

Note that when using the CAST Extension Downloader to download the
extension and the Manage Extensions interface in CAST Server
Manager to install the extension, any dependent extensions
are automatically downloaded and installed for you. You do not need
to do anything.

### CAST Transaction Configuration Center (TCC) Entry Points

In Spring MVC ≥ 1.3.x, if you are using the extension with CAST
AIP ≥ 8.3.x, a set of Spring MVC specific Transaction Entry
Points are now automatically imported when the extension is
installed. These Transaction Entry Points will be available in the
CAST Transaction Configuration Center:

![](../images/669254459.jpg)

## Packaging, delivering and analyzing your source code

Once the extension is installed, no further configuration changes are
required before you can package your source code and run an analysis.
The process of packaging, delivering and analyzing your source code does
not change in any way:

-   Package and deliver your application (that includes source code
    which uses Spring MVC) in the exact same way as you always have.
-   Analyze your delivered application source code in the CAST
    Management Studio in the exact same way as you always have -
    the source code which uses Spring MVC will be detected and
    handled correctly.

### Log messages

#### Warnings

<table class="relative-table confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Message ID</th>
<th class="confluenceTh">Message Type</th>
<th class="confluenceTh"><p>Logged during</p></th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remediation</th>
<th class="confluenceTh">Action</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">SPMVC-001</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">Various</td>
<td class="confluenceTd">None</td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">SPMVC-002</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">A property could not be found so we could not
determine the name of a service.</td>
<td class="confluenceTd">Provide properties file with the value</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

#### Regular logs

We inform about duplicated entries in servlet configuration files and
whether multiple controllers are associated for a single URL path (we
don't filter mappings by order priorities).

``` java
Info : [com.castsoftware.springmvc] Found duplicated entry for path: /otherAddress
Info : [com.castsoftware.springmvc] Found duplicated entry for path: /manageAccount
Info : [com.castsoftware.springmvc] Found multiple controllers for path: /addUser
```

#### AIA expectations

At the end of the log file you will find a message indicating how many
services have been found:

``` java
5 Spring-MVC web service operations created.
```

## What results can you expect?

Once the analysis/snapshot generation has completed, HTTP API
transaction entry points will be available for use when configuring the
CAST Transaction Configuration Center. In addition, you can view the
results in the normal manner (for example via CAST Enlighten).

![](../images/669254453.png)

Note on image: The links of type *fire* (F) are substituted
by *callLink*s (C) in *com.castsoftware.springmvc*versions \>=1.5.3.

### Objects

#### Server side

The following objects are displayed in CAST Enlighten on the Java side:

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669254452.png" draggable="false"
data-image-src="../images/669254452.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254452"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_Service32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Spring MVC Any Operation</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<img src="../images/669254451.png" draggable="false"
data-image-src="../images/669254451.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254451"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_DeleteOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" />
</div></td>
<td class="confluenceTd">Spring MVC Delete Operation</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<img src="../images/669254450.png" draggable="false"
data-image-src="../images/669254450.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254450"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_GetOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" />
</div></td>
<td class="confluenceTd">Spring MVC Get Operation</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669254449.png" draggable="false"
data-image-src="../images/669254449.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254449"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_Port32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Spring MVC Port</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<img src="../images/669254448.png" draggable="false"
data-image-src="../images/669254448.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254448"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_PostOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" />
</div></td>
<td class="confluenceTd">Spring MVC Post Operation</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<img src="../images/669254447.png" draggable="false"
data-image-src="../images/669254447.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254447"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_PutOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" />
</div></td>
<td class="confluenceTd">Spring MVC Put Operation</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/669254452.png" draggable="false"
data-image-src="../images/669254452.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254452"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_Service32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Spring MVC Service</td>
</tr>
</tbody>
</table>

#### Client side

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669254447.png" draggable="false"
data-image-src="../images/669254447.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254447"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_PutOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Thymeleaf DELETE resource service</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/669254450.png" draggable="false"
data-image-src="../images/669254450.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254450"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_GetOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Thymeleaf GET resource service</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/669254447.png" draggable="false"
data-image-src="../images/669254447.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254447"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_PutOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Thymeleaf POST resource service</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/669254447.png" draggable="false"
data-image-src="../images/669254447.png"
data-unresolved-comment-count="0" data-linked-resource-id="669254447"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_SpringMVC_PutOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="669254446"
data-linked-resource-container-version="1" /></p>
</div></td>
<td class="confluenceTd">Thymeleaf PUT resource service</td>
</tr>
</tbody>
</table>

### Rules

None.

### Known limitations 

-   Order priority in SimpleUrlHandlerMapping is ignored (for each
    mapping found a web service operation will be created).
-   The less common programmatic configuration of web services is not
    supported.

