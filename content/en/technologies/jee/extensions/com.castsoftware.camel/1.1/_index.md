---
title: "Apache Camel - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.camel

## What's new?

See [Release Notes](rn/).

## In what situation should you install this extension?

This extension should be installed when your Java application consists
of Apache Camel Routes. Objects created for the components encountered
in Apache Camel Route are linked to objects produced by the [JEE Analyzer](../../com.castsoftware.jee/). This will result in better transactions and
calculation of Automated Function Points.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

Include the extension using the UI:

![](../images/668337937.jpg)

There is nothing further to do. Any Apache Camel configuration defined
in XML files in the application source code will be automatically
detected.

## What results can you expect?

Once the analysis/snapshot generation is completed, you can view the
results in the normal manner (for example via CAST Enlighten):

### Objects

| Icon | Description |
|---|---|
| ![](../images/668337936.png) | Apache Camel HTTP Get Operation |
| ![](../images/668337933.png) | Apache Camel HTTP Put Operation |
| ![](../images/668337930.png) | Apache Camel HTTP Post Operation |
| ![](../images/668337927.png) | Apache Camel HTTP Delete Operation |
| ![](../images/668337924.png) | Apache Camel HTTP Any Operation |
| ![](../images/668337922.png) | Apache Camel Route Call |
| ![](../images/668337919.png) | Apache Camel Route |
| ![](../images/668337916.png) | Apache Camel Bean Call |
| ![](../images/668337913.png) | Apache Camel Process Call |
| ![](../images/668337911.png) | Apache Camel Database Query |
| ![](../images/668337929.png) | Apache Camel Unknown Kafka Topic Receive |
| ![](../images/668337935.png) | Apache Camel JMS Queue Receive |
| ![](../images/668337932.png) | Apache Camel JMS QueueCall |
| ![](../images/668337929.png) | Apache Camel Unknown JMS Queue Receive |
| ![](../images/668337926.png) | Apache Camel Unknown JMS Queue Call |
| ![](../images/668337923.png) | Apache Camel IBM Queue Receive |
| ![](../images/668337921.png) | Apache Camel IBM Queue Call |
| ![](../images/668337918.png) | Apache Camel Unknown IBM_Queue Receive |
| ![](../images/668337915.png) | Apache Camel Unknown IBM Queue Call |
| ![](../images/668337912.png) | Apache Camel ActiveMQ Queue Receive |
| ![](../images/668337910.png) | Apache Camel ActiveMQ Queue Call |
| ![](../images/668337926.png) | Apache Camel Unknown Kafka Topic Call |
| ![](../images/668337934.png) | Apache Camel Unknown ActiveMQ Queue Receive |
| ![](../images/668337931.png) | Apache Camel Unknown ActiveMQ Queue Call |
| ![](../images/668337928.png) | Apache Camel RabbitMQ Topic Receive |
| ![](../images/668337925.png) | Apache Camel RabbitMQ Topic Call |
| ![](../images/668337934.png) | Apache Camel Unknown RabbitMQ Topic Receive |
| ![](../images/668337920.png) | Apache Camel Unknown RabbitMQ Topic Call |
| ![](../images/668337917.png) | Apache Camel File |
| ![](../images/668337914.png) | Apache Camel File Call |
| ![](../images/668337912.png) | Apache Camel Kafka Topic Receive |
| ![](../images/668337910.png) | Apache Camel Kafka Topic Call |
| ![](../images/mongo_coll.png) | Apache Camel MongoDB collection
| ![](../images/mongo_coll_unknown.png) | Apache Camel unknown MongoDB collection
| ![](../images/mongo_db.png) | Apache Camel MongoDB database
| ![](../images/mongo_db_unknown.png) | Apache Camel unknown MongoDB database
| ![](../images/s3_bucket.png) | Apache Camel AWS S3 Bucket
| ![](../images/s3_bucket_unknown.png) | Apache Camel AWS Unknown S3 Bucket



### Links

For MongoDB and AWS-S3, below links are created between source and destination objects based on the operation.
* useSelectLink
* useInsertLink
* useUpdateLink
* useDeleteLink

For other components, only callLinks are created between various objects created by this extension.

### Code examples

#### XML DSL- callLink between REST POST service and Apache Camel Route Call



``` xml
<rest path="restservices/sapmp/v1/cart/">
            <post uri="/addCart">
                <to uri="direct:addCart" />
             </post>
```

![](../images/668337909.png)

#### Java DSL- callLink between REST POST service and Apache Camel Route Call



``` java
public void subscriberServiceRoute() {

      ajscRoute.setRoute(from("restlet:/subscriber/subscribernotifications?restletMethods=POST&restletBinding=#customBinding")
    .log(" Determine the event type ")
        .choice()
    .when().simple("${body} =~ '" + SubscriberConstants.CANCEL_SUBSCRIBER + "'").to("direct:cancelSubscriber")
}
```

![](../images/668337908.png)

#### XML DSL - callLink between REST DELETE and Apache Camel Route Call



  

``` xml
<delete uri="/order/modify/modifyOrder">
                <to uri="direct:modifyOrder" />            
</delete>
```

![](../images/668337907.png)

#### XML DSL - callLink between REST PUT and Apache Camel Route Call



``` xml
<put uri="/order/modify/modifyOrder">
                <to uri="direct:modifyOrder" />            
</put>
```

![](../images/668337906.png)

#### XML DSL - callLink between ApacheCamel Route and Apache Camel Route Call



  

``` xml
<from uri="direct:submitOrder" />
    <to uri="direct:fanAuthorization" />
```

![](../images/668337905.png)

#### Java DSL - useInsertLink between REST POST and MongoDB Collection

``` java
public class MongoDBInsertRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {
        from("jetty:http://0.0.0.0:8081/hello?httpMethodRestrict=POST")
                .log("Called insert API")
                .to("mongodb:myDb?database=test&collection=test&operation=insert")
                .setBody(simple("${body}"));
    }
}

```
![](../images/mongo_dsl_example.png)

#### Java DSL - useDeleteLink between REST GET and AWS S3 Bucket

``` java
public class AwsRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {
        from("jetty:http://0.0.0.0:8081/s3?httpMethodRestrict=GET")
                .log("Called s3 delete API")
                .to("aws2-s3://customer-cart?amazonS3Client=#s3Client&operation=deleteBucket")
                .setBody(simple("${body}"));
    }
}

```
![](../images/s3_example.png)

#### Java DSL - callLink between Apache Camel Route and Apache Camel Route Call



``` java
public void unenroll()
{
      from("direct:unenrollSubscriber")
      .process("prepareInputToUnEnroll")
   .to("invokeUnenrollService")
        .log("Unenroll Service Status Code in MobileSplit :: ${in.headers.CamelHttpResponseCode}")
        .choice()
        .when().simple("${in.headers.CamelHttpResponseCode} =~ "+ "'"+SubscriberConstants.RESPONSE_SUCCESS_CODE+"'"
          + " || ${in.headers.CamelHttpResponseCode} =~ '"+SubscriberConstants.ACCEPTED_CODE+"'")
           .to("direct:retireRecord")
           .process(SubscriberConstants.RESPONSE_HANDLER)
        .otherwise()
             .setBody().constant(SubscriberConstants.MESSAGE_UNENROLLMENT_UNSUCCESSFUL)
             .process(SubscriberConstants.RESPONSE_ERROR_HANDLER)
        .endChoice().end(); 
}
```

![](../images/668337904.png)

#### Java DSL - callLink between Apache Camel Route (seda)  and Apache Camel Route Call (direct)



``` java
public void configure() throws Exception {
    from("seda:largeListConsumer?concurrentConsumers=1&size=1000&timeout=0&blockWhenFull=true")
                .routeId("largeListConsumer")
                .to("direct:processFile")
                .end();
}
```

![](../images/668337903.png)

#### XML DSL - callLinks between Rest POST service and Bean Call, Process Call



``` xml
<from id="_from1" uri="restlet:/addp/v1/account/profiles?restletMethods=POST&restletBinding=#customBinding"/>
        <doTry id="_doTrymp">
            <to id="_to_MAN1" uri="bean:restServiceImpl?method=manageProfile"/>
            <process id="_manageProfileService" ref="manageProfileService"/>
```

![](../images/668337902.png)

#### Java DSL - callLinks between Rest POST service and Bean Call, Process Call



``` java
public void subscriberServiceRoute()
{
      ajscRoute.setRoute(from("restlet:/subscriber/subscribernotifications?restletMethods=POST&restletBinding=#customBinding")
        .bean("SubscriberNotificationBeans","getSubscriberNotifications(exchange)")
     .process("decideEventType"));
}
```

![](../images/668337901.png)

#### XML DSL - callLink between Apache Camel Route and Bean Call



  

``` xml
<from uri="direct:submitOrder" />
  <interceptFrom id="_interceptFrom2">
     <to id="submitOrderCamelInterceptor" uri="bean:camelInterceptor?method=invokepreInterceptorChain" />
  </interceptFrom>
```

![](../images/668337900.png)

#### XML DSL - callLink between Apache Camel Route and Process Call



  

``` xml
<from id="_from_D4" uri="direct:fanAuthorization" />
  <process ref="prepareProfileInputData" />
```

![](../images/668337899.png)

#### Java DSL - callLinks between Apache Camel Route and Bean Call, Process Call



``` java
public void cancelSubscriber() {
    from("direct:cancelSubscriber").process("prepareInputToAddpDeviceDetails") 
                                       .bean("inquireEnterpriseDeviceDeploymentDetails", "inquireDeviceDetailsByCtn(${body})");
}
```

![](../images/668337898.png)

#### XML DSL - callLinks between REST Post Service and JDBC Query



``` xml
<from uri="restlet:/user?restletMethod=POST"/>
          <setBody>
                <simple>select * from user ORDER BY id desc LIMIT 1</simple>
            </setBody>
            <to uri="jdbc:dataSource"/>        
```

![](../images/668337897.png)

``` xml
<route id="createUser">
            <from uri="restlet:/user?restletMethod=POST"/>
            <setBody>
                <simple>insert into user(firstName, lastName) values('${header.firstName}','${header.lastName}');  
                    CALL IDENTITY();               
        </simple>
            </setBody>
            <to uri="jdbc:dataSource"/>
```

![](../images/668337896.png)

#### Java DSL - callLink between REST Post Service and JDBC Query



``` java
public void configure() {
    from("restlet:/user?restletMethod=POST")
    .setBody(simple("select * from user ORDER BY id desc LIMIT 1"))
        .to("jdbc:dataSource");
}
```

![](../images/668337897.png)

``` java
public void configure() {  
   from("restlet:/user/{userId}?restletMethods=GET,PUT,DELETE")
     .setBody(simple("update user set firstName='${header.firstName}', lastName='${header.lastName}' where id = ${header.userId}"))
         .to("jdbc:dataSource"); 
}
```

![](../images/668337895.png)

#### Java DSL - callLink between JMS Queue Receive and JMS Queue Call



  

``` java
public void configure() throws Exception 
{
   from("jms:JmsQueue").bean(ProcessingBean.class, "doSomething").to("jms:redirected");
}
```

![](../images/668337894.png)

#### XML DSL - callLink between File and File Call



``` xml
<route id="fileRoute2">
    <from uri="file:home/customers/new?fileName=new.xml"/>
    <to uri="file://home/customers/old"/>
</route>
```

![](../images/668337893.png)

#### Java DSL - callLink between File and File Call



``` java
public void configure() throws Exception {
    from("file:home/customers/old")
                .routeId("transfer")
                .log(LoggingLevel.INFO, "com.toyota.tme.cws.transform.route.error", "Transferring File - ${file:name}")
                .to("file:home/customers/new")
                .end();
}
```

![](../images/668337892.png)

#### Java DSL - callLink between File and Bean Call



``` java
public void configure () throws Exception {
    from("file://inputdir?fileName=order.xml")
                .routeId("readFilefromDir")
                .log(LoggingLevel.INFO, "com.toyota.tme.cws.transform.route.error", "Processing File - ${file:name}")
                .bean("FileBusiness", "moveToSedaQueue")
                .end();
}
```

![](../images/668337891.png)

#### Java DSL - callLink between Apache Camel Route and Kafka Topic Call



``` java
public void configure () throws Exception {
    from("direct:start")       
            .to("kafka:localhost:8080?topic=testkafkaprod3");
}
```

![](../images/668337890.png)

#### Java DSL - callLink between Kafka Topic Receive and File Call



``` java
public void configure () throws Exception {
    from("kafka:localhost:8080?topic=testkafkacon1&zookeeperHost=localhost&zookeeperPort=2181&groupId=group1").to("file:input");
}
```

![](../images/668337889.png)

#### callLinks in the complete transaction



![](../images/668337888.jpg)![](../images/668337887.png)![](../images/668337886.png)![](../images/668337885.png)

![](../images/668337884.png)![](../images/668337883.png)

![](../images/668337882.png)

## Limitations

-   Unknown queue/process/bean/route/file/jms/kafka object is created in
    case where the exact name can't be retrieved
-   Routes defined using route templates result in unknown route
    objects 
-   All other route components are ignored. Any Route starting with a
    component not mentioned in the Object section , the whole of the
    Route with its components will be ignored.
-   Any component created with a customized name will not be handled or
    created
-   There may be missing/multiple links instead of one between process
    call and process method of the target class
-   There may be multiple links instead of one between bean call and
    target method of the bean
-   There may be incorrect bookmarks in few call links and objects
-   One of the signature for the overloaded method idempotentConsumer
    method of org.apache.camel.model.ProcessDefinition is not supported.
    This suspends the creation of the objects for the components in that
    route following this method.
-   Current version does not support Kafka topic using Kafka idempotent
    repository
-   Consuming messages from multiple Kafka topics is not supported
-   Kafka topic receive/call object is always created by the name
    whatever is present in Kafka component topic. There is a possibility
    that name mentioned of the topic is an alias and refers to something
    else. Due to some design constraints, it will not be evaluated to
    refer to the exact value. Hence, object will be created in the name
    present in Kafka component.
-   In case of AWS-S3 copy operation, only source bucket is considered currently.