---
title: "Apache Camel - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.camel

## What's new?

See [Release Notes](rn/).

## In what situation should you install this extension?

This extension should be installed when your Java application consists
of Apache Camel Routes. Objects created for the components encountered
in Apache Camel Route are linked to objects produced by the [JEE Analyzer](../../com.castsoftware.jee/). This will result in better transactions and
calculation of Automated Function Points.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

Include the extension using the interface in AIP Console:

![](../images/668337937.jpg)

There is nothing further to do. Any Apache Camel configuration defined
in XML files in the application source code will be automatically
detected.

## What results can you expect?

Once the analysis/snapshot generation is completed, you can view the
results in the normal manner (for example via CAST Enlighten):

### Objects

<table class="relative-table confluenceTable" style="width: 97.8867%;">
<colgroup>
<col style="width: 4%" />
<col style="width: 26%" />
<col style="width: 5%" />
<col style="width: 27%" />
<col style="width: 5%" />
<col style="width: 31%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
<th class="confluenceTh"> Icon</th>
<th class="confluenceTh"> Description</th>
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337936.png" draggable="false"
data-image-src="../images/668337936.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337936"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_HTTP_GetOperation.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel HTTP Get Operation</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337935.png" draggable="false"
data-image-src="../images/668337935.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337935"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-41-46.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel JMS Queue Receive</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337934.png" draggable="false"
data-image-src="../images/668337934.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337934"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-45-12.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown ActiveMQ Queue
Receive</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337933.png" draggable="false"
data-image-src="../images/668337933.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337933"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_HTTP_PutOperation.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel HTTP Put Operation</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337932.png" draggable="false"
data-image-src="../images/668337932.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337932"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-38-52.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel JMS QueueCall</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337931.png" class="image-left"
draggable="false" data-image-src="../images/668337931.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337931"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-44-8.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown ActiveMQ Queue Call</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337930.png" draggable="false"
data-image-src="../images/668337930.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337930"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_HTTP_PostOperation.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel HTTP Post Operation</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337929.png" draggable="false"
data-image-src="../images/668337929.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337929"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-46-12.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="24" /></p>
</div></td>
<td class="confluenceTd"> Apache Camel Unknown JMS Queue Receive</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337928.png" draggable="false"
data-image-src="../images/668337928.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337928"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-41-26.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel RabbitMQ Topic Receive</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337927.png" draggable="false"
data-image-src="../images/668337927.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337927"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_HTTP_DeleteOperation.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel HTTP Delete Operation </td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337926.png" draggable="false"
data-image-src="../images/668337926.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337926"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-44-36.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd"> Apache Camel Unknown JMS Queue Call</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337925.png" draggable="false"
data-image-src="../images/668337925.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337925"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-21_14-18-29.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel RabbitMQ Topic Call</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337924.png" draggable="false"
data-image-src="../images/668337924.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337924"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_HTTP_AnyOperation.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel HTTP Any Operation </td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337923.png" draggable="false"
data-image-src="../images/668337923.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337923"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-41-57.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel IBM Queue Receive</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337934.png" draggable="false"
data-image-src="../images/668337934.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337934"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-45-12.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown RabbitMQ Topic
Receive</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337922.png" class="image-center"
draggable="false" data-image-src="../images/668337922.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337922"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_RouteCall.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Route Call</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337921.png" draggable="false"
data-image-src="../images/668337921.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337921"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-39-10.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel IBM Queue Call</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337920.png" draggable="false"
data-image-src="../images/668337920.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337920"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-21_14-19-17.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown RabbitMQ Topic Call</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337919.png" class="image-center"
draggable="false" data-image-src="../images/668337919.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337919"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_Route.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Route</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337918.png" draggable="false"
data-image-src="../images/668337918.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337918"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-45-55.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown IBM_Queue Receive</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337917.png" draggable="false"
data-image-src="../images/668337917.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337917"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-3-1_23-51-4.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel File</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337916.png" class="image-center"
draggable="false" data-image-src="../images/668337916.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337916"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_BeanCall.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Bean Call</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337915.png" draggable="false"
data-image-src="../images/668337915.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337915"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-44-20.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown IBM Queue Call</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337914.png" draggable="false"
data-image-src="../images/668337914.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337914"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-3-1_23-51-38.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel File Call</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/668337913.png" draggable="false"
data-image-src="../images/668337913.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337913"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_Process.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Process Call</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337912.png" draggable="false"
data-image-src="../images/668337912.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337912"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-42-46.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel ActiveMQ Queue Receive</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337912.png" draggable="false"
data-image-src="../images/668337912.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337912"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-42-46.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Kafka Topic Receive</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337911.png" draggable="false"
data-image-src="../images/668337911.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337911"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_ApacheCamel_DatabaseQuery.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Database Query</td>
<td class="confluenceTd"><div class="content-wrapper">
<p> <img src="../images/668337910.png" draggable="false"
data-image-src="../images/668337910.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337910"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-39-23.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel ActiveMQ Queue Call</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337910.png" draggable="false"
data-image-src="../images/668337910.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337910"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-39-23.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Kafka Topic Call</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337929.png" draggable="false"
data-image-src="../images/668337929.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337929"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-46-12.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" height="24" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown Kafka Topic Receive</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/668337926.png" draggable="false"
data-image-src="../images/668337926.png"
data-unresolved-comment-count="0" data-linked-resource-id="668337926"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="image2021-1-20_17-44-36.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="668337881"
data-linked-resource-container-version="1" width="22" /></p>
</div></td>
<td class="confluenceTd">Apache Camel Unknown Kafka Topic Call</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

### Links

Only callLinks are created between various objects created by this
extension.

### Code examples

#### XML DSL- callLink between REST POST service and Apache Camel Route Call



``` xml
<rest path="restservices/sapmp/v1/cart/">
            <post uri="/addCart">
                <to uri="direct:addCart" />
             </post>
```

![](../images/668337909.png)

#### Java DSL- callLink between REST POST service and Apache Camel Route Call



``` java
public void subscriberServiceRoute() {

      ajscRoute.setRoute(from("restlet:/subscriber/subscribernotifications?restletMethods=POST&restletBinding=#customBinding")
    .log(" Determine the event type ")
        .choice()
    .when().simple("${body} =~ '" + SubscriberConstants.CANCEL_SUBSCRIBER + "'").to("direct:cancelSubscriber")
}
```

![](../images/668337908.png)

#### XML DSL - callLink between REST DELETE and Apache Camel Route Call



  

``` xml
<delete uri="/order/modify/modifyOrder">
                <to uri="direct:modifyOrder" />            
</delete>
```

![](../images/668337907.png)

#### XML DSL - callLink between REST PUT and Apache Camel Route Call



``` xml
<put uri="/order/modify/modifyOrder">
                <to uri="direct:modifyOrder" />            
</put>
```

![](../images/668337906.png)

#### XML DSL - callLink between ApacheCamel Route and Apache Camel Route Call



  

``` xml
<from uri="direct:submitOrder" />
    <to uri="direct:fanAuthorization" />
```

![](../images/668337905.png)

#### Java DSL - callLink between Apache Camel Route and Apache Camel Route Call



``` java
public void unenroll()
{
      from("direct:unenrollSubscriber")
      .process("prepareInputToUnEnroll")
   .to("invokeUnenrollService")
        .log("Unenroll Service Status Code in MobileSplit :: ${in.headers.CamelHttpResponseCode}")
        .choice()
        .when().simple("${in.headers.CamelHttpResponseCode} =~ "+ "'"+SubscriberConstants.RESPONSE_SUCCESS_CODE+"'"
          + " || ${in.headers.CamelHttpResponseCode} =~ '"+SubscriberConstants.ACCEPTED_CODE+"'")
           .to("direct:retireRecord")
           .process(SubscriberConstants.RESPONSE_HANDLER)
        .otherwise()
             .setBody().constant(SubscriberConstants.MESSAGE_UNENROLLMENT_UNSUCCESSFUL)
             .process(SubscriberConstants.RESPONSE_ERROR_HANDLER)
        .endChoice().end(); 
}
```

![](../images/668337904.png)

#### Java DSL - callLink between Apache Camel Route (seda)  and Apache Camel Route Call (direct)



``` java
public void configure() throws Exception {
    from("seda:largeListConsumer?concurrentConsumers=1&size=1000&timeout=0&blockWhenFull=true")
                .routeId("largeListConsumer")
                .to("direct:processFile")
                .end();
}
```

![](../images/668337903.png)

#### XML DSL - callLinks between Rest POST service and Bean Call, Process Call



``` xml
<from id="_from1" uri="restlet:/addp/v1/account/profiles?restletMethods=POST&restletBinding=#customBinding"/>
        <doTry id="_doTrymp">
            <to id="_to_MAN1" uri="bean:restServiceImpl?method=manageProfile"/>
            <process id="_manageProfileService" ref="manageProfileService"/>
```

![](../images/668337902.png)

#### Java DSL - callLinks between Rest POST service and Bean Call, Process Call



``` java
public void subscriberServiceRoute()
{
      ajscRoute.setRoute(from("restlet:/subscriber/subscribernotifications?restletMethods=POST&restletBinding=#customBinding")
        .bean("SubscriberNotificationBeans","getSubscriberNotifications(exchange)")
     .process("decideEventType"));
}
```

![](../images/668337901.png)

#### XML DSL - callLink between Apache Camel Route and Bean Call



  

``` xml
<from uri="direct:submitOrder" />
  <interceptFrom id="_interceptFrom2">
     <to id="submitOrderCamelInterceptor" uri="bean:camelInterceptor?method=invokepreInterceptorChain" />
  </interceptFrom>
```

![](../images/668337900.png)

#### XML DSL - callLink between Apache Camel Route and Process Call



  

``` xml
<from id="_from_D4" uri="direct:fanAuthorization" />
  <process ref="prepareProfileInputData" />
```

![](../images/668337899.png)

#### Java DSL - callLinks between Apache Camel Route and Bean Call, Process Call



``` java
public void cancelSubscriber() {
    from("direct:cancelSubscriber").process("prepareInputToAddpDeviceDetails") 
                                       .bean("inquireEnterpriseDeviceDeploymentDetails", "inquireDeviceDetailsByCtn(${body})");
}
```

![](../images/668337898.png)

#### XML DSL - callLinks between REST Post Service and JDBC Query



``` xml
<from uri="restlet:/user?restletMethod=POST"/>
          <setBody>
                <simple>select * from user ORDER BY id desc LIMIT 1</simple>
            </setBody>
            <to uri="jdbc:dataSource"/>        
```

![](../images/668337897.png)

``` xml
<route id="createUser">
            <from uri="restlet:/user?restletMethod=POST"/>
            <setBody>
                <simple>insert into user(firstName, lastName) values('${header.firstName}','${header.lastName}');  
                    CALL IDENTITY();               
        </simple>
            </setBody>
            <to uri="jdbc:dataSource"/>
```

![](../images/668337896.png)

#### Java DSL - callLink between REST Post Service and JDBC Query



``` java
public void configure() {
    from("restlet:/user?restletMethod=POST")
    .setBody(simple("select * from user ORDER BY id desc LIMIT 1"))
        .to("jdbc:dataSource");
}
```

![](../images/668337897.png)

``` java
public void configure() {  
   from("restlet:/user/{userId}?restletMethods=GET,PUT,DELETE")
     .setBody(simple("update user set firstName='${header.firstName}', lastName='${header.lastName}' where id = ${header.userId}"))
         .to("jdbc:dataSource"); 
}
```

![](../images/668337895.png)

#### Java DSL - callLink between JMS Queue Receive and JMS Queue Call



  

``` java
public void configure() throws Exception 
{
   from("jms:JmsQueue").bean(ProcessingBean.class, "doSomething").to("jms:redirected");
}
```

![](../images/668337894.png)

#### XML DSL - callLink between File and File Call



``` xml
<route id="fileRoute2">
    <from uri="file:home/customers/new?fileName=new.xml"/>
    <to uri="file://home/customers/old"/>
</route>
```

![](../images/668337893.png)

#### Java DSL - callLink between File and File Call



``` java
public void configure() throws Exception {
    from("file:home/customers/old")
                .routeId("transfer")
                .log(LoggingLevel.INFO, "com.toyota.tme.cws.transform.route.error", "Transferring File - ${file:name}")
                .to("file:home/customers/new")
                .end();
}
```

![](../images/668337892.png)

#### Java DSL - callLink between File and Bean Call



``` java
public void configure () throws Exception {
    from("file://inputdir?fileName=order.xml")
                .routeId("readFilefromDir")
                .log(LoggingLevel.INFO, "com.toyota.tme.cws.transform.route.error", "Processing File - ${file:name}")
                .bean("FileBusiness", "moveToSedaQueue")
                .end();
}
```

![](../images/668337891.png)

#### Java DSL - callLink between Apache Camel Route and Kafka Topic Call



``` java
public void configure () throws Exception {
    from("direct:start")       
            .to("kafka:localhost:8080?topic=testkafkaprod3");
}
```

![](../images/668337890.png)

#### Java DSL - callLink between Kafka Topic Receive and File Call



``` java
public void configure () throws Exception {
    from("kafka:localhost:8080?topic=testkafkacon1&zookeeperHost=localhost&zookeeperPort=2181&groupId=group1").to("file:input");
}
```

![](../images/668337889.png)

#### callLinks in the complete transaction



![](../images/668337888.jpg)![](../images/668337887.png)![](../images/668337886.png)![](../images/668337885.png)

![](../images/668337884.png)![](../images/668337883.png)

![](../images/668337882.png)

## Limitations

-   Unknown queue/process/bean/route/file/jms/kafka object is created in
    case where the exact name can't be retrieved
-   Routes defined using route templates result in unknown route
    objects 
-   All other route components are ignored. Any Route starting with a
    component not mentioned in the Object section , the whole of the
    Route with its components will be ignored.
-   Any component created with a customized name will not be handled or
    created
-   There may be missing/multiple links instead of one between process
    call and process method of the target class
-   There may be multiple links instead of one between bean call and
    target method of the bean
-   There may be incorrect bookmarks in few call links and objects
-   One of the signature for the overloaded method idempotentConsumer
    method of org.apache.camel.model.ProcessDefinition is not supported.
    This suspends the creation of the objects for the components in that
    route following this method.
-   Current version does not support Kafka topic using Kafka idempotent
    repository
-   Consuming messages from multiple Kafka topics is not supported
-   Kafka topic receive/call object is always created by the name
    whatever is present in Kafka component topic. There is a possibility
    that name mentioned of the topic is an alias and refers to something
    else. Due to some design constraints, it will not be evaluated to
    refer to the exact value. Hence, object will be created in the name
    present in Kafka component.