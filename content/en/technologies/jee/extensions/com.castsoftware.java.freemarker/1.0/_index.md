---
title: "Apache FreeMarker - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension Id

com.castsoftware.java.freemarker

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Apache FreeMarker Framework. 

## In what situation should you install this extension?

If your Java application uses the Apache FreeMarker Framework and you
want to view these object types and their links, then you should install
this extension. More specifically the extension will identify:

-   "callLink" from HTML source code objects (.ftl and similar files) to
    CallToFTL objects.
-   "callLink" from CallToFTL objects to Java methods.

## Technology support

| Component                   | Version | Supported          | Supported Technology |
|:----------------------------|:--------|:------------------:|:--------------------:|
| Apache FreeMarker Framework | 2.3.x   | :white_check_mark: | Java |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST Console. If you need to use it, should manually install the extension.

## What results can you expect?

### Objects

| Icon  | Description  |
|---------|---------|
| ![](../images/663879843.png) | Java Freemarker FTL Parameter |
| ![](../images/673841174.png) | Java Freemarker FTL Call  |

### Links

| Link Type | Source and destination link |
|---|---|
| callLink  | Between HTML source code objects(.ftl and similar files) and CallToFTL objects. |
| callLink | Between CallToFTL objects and JAVA methods. |
| relyonLink | Between Java Freemarker FTLParameter and its JAVA class |

### Code Examples

MainTest.java

``` java
public static void main(String[] args) throws Exception {

        Configuration cfg = new Configuration();
        String content = null;

        cfg.setClassForTemplateLoading(MainTest.class, "templates");
        cfg.setIncompatibleImprovements(new Version(2, 3, 20));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        Map<String, Object> input = new HashMap<String, Object>();

        input.put("title", "Dummy example");

        List<ValueExampleObject> systems = new ArrayList<ValueExampleObject>();
        systems.add(new ValueExampleObject("Android", "Google"));
        systems.add(new ValueExampleObject("iOS States", "Apple"));
        systems.add(new ValueExampleObject("Ubuntu", "Canonical"));
        systems.add(new ValueExampleObject("Windows7", "Microsoft"));
        input.put("systems", systems);

        Cal cal=new Cal();
        input.put("cal", cal);

        input.put("exampleObject", new ValueExampleObject("Java object", "me"));

        Template template = cfg.getTemplate("helloworld.ftl");

        template.process(input, consoleWriter);
}
```

Cal.java

``` java
public class Cal {
    public int add(int a, int b) {
        return a+b;
    }
    
    public int mul(int a, int b) {
        return a*b;
    }
}
```

helloworld.ftl

``` xml
<html>
    
    <head>
      <title>${title}
    </head>
    <body>
      <h1>${title}</h1>
    
      <p>${exampleObject.name} by ${exampleObject.developer}</p>
    
      <ul>
        <#list systems as system>
          <li>${system_index + 1}. ${system.name} from ${system.developer}</li>
        </#list>
      </ul>
    
    <h1>
        2+3= ${cal.add(2,3)}
        10/2= ${cal.mul(10,2)}
    </h1>

    <#assign multiplied = cal.mul(40,20)>
    
    <h1>${multiplied}</h1>
    </body>
</html>
```

![](../images/637370423.png)
