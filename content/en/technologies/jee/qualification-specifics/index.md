---
title: "Qualification specifics"
linkTitle: "Qualification specifics"
type: "docs"
weight: 1
---

## Step 1: Gather code context

At the end of this step, you should know:

-   The JDK version used to compile the source code
-   The frameworks used and the version numbers
-   The different application layers
-   The archives (third-party)
-   The application server used to run the application

And you should have:

-   All the JAVA/JSP application source code, the relevant XML and
    .properties files and all the client files (HTML, HTM, JavaScript
    ...). Note that the JEE Analyzer does not analyze HTML, HTM,
    JavaScript, CSS files. Instead you must use the [HTML5 and JavaScript](../../web/html5-js/com.castsoftware.html5)
    extension.
-   The deployment descriptor files required for EJB and Web Services
    analyses
-   The archives of libraries required to run the application, including
    those of the application server

### Questions to ask

-   What JDK version is used?
-   Which frameworks are used for the presentation layer and the
    persistence layer?
    -   Presentation layer:
        -   Struts
        -   Spring MVC or Spring Web Flow
        -   JSF
    -   Persistence layer:
        -   Specific Application Server EJB (Oracle iAS, WebSphere,
            Weblogic, Resin...)
        -   Hibernate
        -   iBATIS
        -   Cayenne
        -   A more complete list can be found here:
            [http://en.wikipedia.org/wiki/List_of_object-relational_mapping_software#Java](http://en.wikipedia.org/wiki/List_of_object-relational_mapping_software#Java)
-   Are there other frameworks used, and if so, for what purpose?
    Frameworks like:
    -   Spring
    -   Custom framework
-   What are the application server brands and versions used for
    persistence and/or web services?
-   Get the software development guide if possible. This will give you
    the naming convention used and will allow you to customize quality
    rules.

### Required source code components for the analysis

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th colspan="3" class="highlight-grey confluenceTh"
data-highlight-colour="grey"><strong>Requirement</strong></th>
<th colspan="2" class="highlight-grey confluenceTh"
data-highlight-colour="grey"><strong>Comment</strong></th>
</tr>
&#10;<tr class="odd">
<td colspan="3" class="confluenceTd">All the source code must encoded in
ASCII or in a supported Unicode encoding (UTF-8, …).</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">All .java files in the
application</td>
<td colspan="2" class="confluenceTd">When collecting application source
code, ensure that you will have the .java files and not .class or .jar
files for source code within the scope of the analysis. .class and .jar
files are decompiled only to extract the signature.</td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">All libraries (.JAR) required to
build the application including JDK, J2EE, related libraries e.g.
application server, 3rd party, framework, presentation, persistence,
etc.</td>
<td colspan="2" class="confluenceTd"><p>Custom JAR files, included e.g.
custom J2EE.JAR, used for Presentation and Persistence should be
delivered with the source code. Failure to do so will prevent
transaction identification, and all depending metrics.</p></td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">.XML files used by frameworks</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">.properties files</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">EJB configuration files and all
their deployment mapping files</td>
<td colspan="2" class="confluenceTd">EJB configuration files
(ejb-jar.xml)</td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">All associated Deployment Mapping
Files (required to link with the database).</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">web.xml for each
webapplication</td>
<td colspan="2" class="confluenceTd">Must be provided in the WEB-INF
directory of each web-application</td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">All .jsp / .jcx / .jspf / .jhtml
and similar types of files  in the application</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">All xsl files</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">All Tags libs (.tld)</td>
<td colspan="2" class="confluenceTd">If the application uses custom tags
(JSTL), please provide the TLD documentation</td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">All XML configuration files
(struts-config.xml, ...) and mapping files.</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">All DTD files associated to the XML
files</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">All HTML files</td>
<td colspan="2" rowspan="3" class="confluenceTd">Note that the JEE
Analyzer does not analyze these files types. Instead you must use the [HTML5 and JavaScript](../../web/html5-js/com.castsoftware.html5) extension.<br />
  </td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">All JavaScript files</td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">All css and image files</td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">for webservices, WSDL/WSDD/mapping
files</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">TLD documentation</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd">Maven and/or Eclipse project files
: *.pom, *.project, *.classpath</td>
<td colspan="2" class="confluenceTd">Including the parent .pom. These
files will allow automated discovery and analysis configuration.</td>
</tr>
<tr class="even">
<td colspan="3" class="confluenceTd">.EAR or .WAR (Production
archive)</td>
<td colspan="2" class="confluenceTd">Required to reproduce the real
production structure with the sources.</td>
</tr>
<tr class="odd">
<td colspan="3" class="confluenceTd"> All test code and redundant
components must be excluded before using Cast DMT</td>
<td colspan="2" class="confluenceTd"> </td>
</tr>
</tbody>
</table>

-   When collecting application source code, ensure that you will have
    the .java files and not .class or .jar files for source code within
    the scope of the analysis. These files (.class and .jar), are
    decompiled only to extract the signature. As a result, metrics and
    quality rules based on the code (such as Cyclomatic Complexity) will
    not apply to them.
-   Mostly, the structure of the source code does not reflect the
    production tree. In this case, you may have a lot of unresolved
    links between html, JSP, Java Script, and JAVA files.The correct way
    to handle this situation is to ask for the production package (WAR
    or EAR). With this archive you will be able to reproduce the real
    production structure with the sources and avoid link issues between
    components.

-   The incorrect way is to try to handle this situation with Text
    Replacement because the risk is the influence of Text Replacement on
    correct links. You will have to write lot of regexp and it will be
    hard to validate their effects. So, first change the source tree to
    reflect the production environment and then you can use Text
    Replacement.
-   For Java files, there is no issue with regard to their location
    since links with these files are based on their name or full
    qualified name. The full qualified name is derived from their
    package that is specified with the keyword package in the Java file.
-   The production package provided could be a different version from
    the source code. As a result, don't rely on it for the analysis,
    just use it to copy the tree.

## Step 2: Identify your application components

A typical J2EE application is composed of:

-   A presentation layer
-   A persistent/session layer
-   A business layer
-   (Optionally) a web service layer
-   (Optionally) other components

A typical J2EE source path tree:

![](383881216.png)

Before starting this analysis, you should have a tool such as
[grep](http://unxutils.sourceforge.net/) (free to
download from
[http://unxutils.sourceforge.net/](http://unxutils.sourceforge.net/)),
Notepad++
([http://notepad-plus.sourceforge.net](http://notepad-plus.sourceforge.net))
or UltraEdit
([http://www.ultraedit.com/](http://www.ultraedit.com/))
to search for expressions in files. These tools have the advantage over
Windows tools to output the text that matches a given expression for
each parsed file.

The methodology is as follows:

### Identifying the presentation layer

There are various ways to find out what the presentation layer is based
on:

#### Different methods

-   Look at the libraries included in the distribution of your
    application on the file system: for example if you find struts.jar,
    there is a high chance that the Struts framework is used. To verify
    this, you can read the web.xml (see below)
-   Look at the XML files in the WEB-INF directory: if you find
    struts-config.xml, there is a high chance that the Struts framework
    is used. To verify this, you can read the web.xml (see below)
-   Look at the XML in the java source directory, if you find a
    struts.xml file then there is a high chance that the Struts
    framework is used.
-   Read the web.xml file in the WEB-INF directory and identify the
    technology that the servlets are based on (see the next chapter).
    This is the only method that is 100% foolproof since this file is
    used by the application server to load the application.

#### Reading a web.xml file

When reading the web.xml file, pay attention to the classes and related
parameters referred by the Servlet tag to get information on the
underlying technology:

For example, a Servlet class that has an init-param value equal to
struts-config.xml means that you are using the struts framework:

``` xml
<servlet-name>action</servlet-name>
<servlet-class>org.apache.struts.action.ActionServlet</servlet-class>
<init-param>
    <param-name>config</param-name>
    <param-value>/WEB-INF/struts-config.xml</param-value>
</init-param>
```

Another example is a reference to the StrutsCX servlet meaning that you
are using the StrutsCX framework:

``` xml
<servlet>
     <servlet-name>StrutsCXServlet</servlet-name>
     <servlet-class>*com.cappuccinonet.strutscx.xslt.StrutsCXServlet*</servlet-class>
     <init-param>
          <param-name>debug</param-name>
          <param-value>false</param-value>
    </init-param>
    <load-on-startup>2</load-on-startup>
</servlet>
```

Another interesting tag is servlet-mapping that allows you to identify
which type of files are handled by your web server.  
For example:

``` xml
<servlet-mapping>
    <servlet-name>jsp</servlet-name>
    <url-pattern>.jspf*</url-pattern>
</servlet-mapping>
```

These extensions will allow you to check if the default extension list
handled by the analyzer is correct.

#### Specific action if Struts is used

If you find that Struts is the presentation layer framework, open the
struts-config.xml file to see if there are other frameworks used, such
as StrutsCX.  
To do so look at the specific tag plug-in:  
For example:

``` xml
<plug-in className="com.cappuccinonet.strutscx.util.StrutsCXPlugIn">
   <set-property property="config" value="/WEB-INF/strutscx-config.xml"/>
</plug-in>
```

Struts PlugIns are configured using the \<plug-in\> element within the
Struts configuration file. This element only has one valid attribute,
'className', which is the fully qualified name of the Java class which
implements the org.apache.struts.action.PlugIn interface. For PlugIns
that require configuration themselves, the nested \<set-property\>
element is available. The plug-in tag in the struts-config.xml file is
used to declare the PlugIn to be loaded at the time of server start-up.
Since Struts PlugIns can be developed by anyone, you can find various
plug-ins, but the most common is StrutsCX. Here is a link about Struts
plug-ins:
[http://www.roseindia.net/struts/developing-struts-plugin.shtml](http://www.roseindia.net/struts/developing-struts-plugin.shtml)

### Identifying the Persistent/Session layer

To identify the persistent layer, first look at the META-INF directory.
Is there an ejb-jar.xml? If yes, the application is using EJBs to make
persistence and/or sessions. NOTE:the presence of the ejb-jar.xml
file does not always mean that EJB is used for persistence. It can be
used for EJB sessions with another framework like hibernate for the
persistence. As a result you must also look for other frameworks.

If this is not the case, you can use Hibernate or another ORM framework
to ensure persistence if any. See Identifying other
components/frameworks below.

### Identifying Web services

To find out if your application implements web services, simply search
for files with the extension .wsdl or .wsdd in the source file
directories. Usually, these files are located in the META-INF directory,
but as the source folders do not always match the production folder
tree, we recommend searching all the source files.

### Identifying other components/frameworks

To identify unknown frameworks or custom frameworks, there are various
methods:

Find all the XML files and look at the DTD they are referring to. This
allows you to find frameworks like Spring, Hibernate... (You can use
UltraEdit or a grep command to find the pattern DTD inside XML files in
one go).Examples:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Framework</p></th>
<th class="confluenceTh"><p>DTD</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>Hibernate</p></td>
<td class="confluenceTd"><p>&lt;!DOCTYPE hibernate-mapping PUBLIC<br />
"-//Hibernate/Hibernate Mapping DTD 2.0//EN"<br />
"http://hibernate.sourceforge.net/hibernate-mapping-2.0.dtd"&gt;<br />
<br />
&lt;!DOCTYPE hibernate-configuration PUBLIC<br />
"-//Hibernate/Hibernate Configuration DTD 2.0//EN"<br />
"http://hibernate.sourceforge.net/hibernate-configuration-2.0.dtd"&gt;</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>iBATIS</p></td>
<td class="confluenceTd"><p>&lt;!DOCTYPE daoConfigPUBLIC
"-//iBATIS.com//DTD DAO Configuration
2.0//EN""http://www.ibatis.com/dtd/dao-2.dtd"&gt;</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Spring</p></td>
<td class="confluenceTd"><p>&lt;!DOCTYPE beans PUBLIC "-//SPRING//DTD
BEAN//EN"
"http://www.springframework.org/dtd/spring-beans.dtd"&gt;</p></td>
</tr>
</tbody>
</table>

Look at the libraries: the names of the libraries can give you an
indication.Examples:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Framework</p></th>
<th class="confluenceTh"><p>Libraries</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>Hibernate</p></td>
<td class="confluenceTd"><p>hibernate3.jar</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>iBATIS</p></td>
<td class="confluenceTd"><p>ibatis-common-2.jar<br />
ibatis-dao-2.jar<br />
ibatis-sqlmap-2.jar</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Spring</p></td>
<td class="confluenceTd"><p>spring-1.2.7.jar</p></td>
</tr>
</tbody>
</table>

Look at the different extensions used throughout the entire application.
This information will also allow you to verify if the default extension
list used by the analyzer to parse client and server source code is
complete.Examples:

-   .jhtml: for Java HTML (See
    [http://en.wikipedia.org/wiki/JHTML](http://en.wikipedia.org/wiki/JHTML)).
    This is used with Dynamo, a Java-based Web application server
-   .jspx: JSPX pages are a special type of JSP page: They are
    pre-processed by the JSPXServlet, and converted into regular JSP
    pages (See:
    [http://stackoverflow.com/questions/28235/should-i-be-doing-jspx-instead-of-jsp](http://stackoverflow.com/questions/28235/should-i-be-doing-jspx-instead-of-jsp))
-   .jspf: for JSP Page Fragment
-   .jrxml: for Jasper Report
    ([http://en.wikipedia.org/wiki/JasperReports#JRXML](http://en.wikipedia.org/wiki/JasperReports#JRXML))

If you have detected this framework through an XML file, you can start
to customize the support of this XML (see chapter 9.1 Manage XML files),
but in any case, we recommend that you read up on how it works and how
it is configured.

## Step 3: Identify the limit of your application

Identify what the limits of your analysis are

-   You should not include framework or tool source code if they are not
    part of your development (the related .jar will be referenced as an
    archive through a J2EE Environment Profile \> Classpath or the
    Classpath of an Analysis Unit; refer to for more information).
-   You must pay attention to the .jar that contains source code: a
    source code delivery often contains both the Java source code and
    .jar that contains the compilation of these Java classes. You must
    refer only to the Java source code and never to the compiled
    Java classes.
-   In many cases, a 2EE application contains generated code (stubs for
    Web Services ([AXIS](http://ws.apache.org/axis/)),
    presentation layer and/or DAO
    with [velocity](http://jakarta.apache.org/velocity/) templates,
    SAP connectors
    via [BAPI](http://help.sap.com/saphelp_46c/helpdata/en/7e/5e115e4a1611d1894c0000e829fbbd/frameset.htm),
    Mainframe integration
    with [SCORT](http://www.proatria.com/scort-mainframe.htm)...).
    Usually, the generated code must be analyzed in a specific Analysis
    Unit to avoid influencing the metrics associated to the developed
    code. This is important in the case of Third Party Application
    Maintenance where usually, the integrator is not responsible for the
    generated code.Taking into account the previous constraints, there
    are two cases:  
    -   If the generated code concerns fully generated classes, it is
        possible to analyze them in a specific Analysis Unit
    -   If the generated code concerns the skeleton of classes, there
        are no possibilities for handling this case.

### Notion of internal and external Java components

When determining the limit of a JEE analysis it is important to make a
distinction between Java components that are internal to an Analysis
Unit, external to this Analysis Unit and external to the application:

-   Internal: means that the source code will produce the Java
    module in the Analysis Service. This refers to:
    -   All Java sources referenced through Analysis Unit \> Java Files

![](383881215.png)

-   External: this refers to all java files, .class files or Archive
    files that are referenced through:  
    -   Any J2EE Analysis Units selected as a target dependency will be
        implicitly referenced by the other J2EE Analysis Units source of
        the dependency. 
    -   Java code that is referenced in Analysis \> Java \> Define
        classpaths used by the application
    -   Environment Profile: in General \> Add Path (for the classes
        and Java sources) and General \> Add Archive or Add Archive
        folder for the JAR files.

![](383881214.png)

-   -   Analysis Unit: an analysis unit which is marked "don't show
        in dashboard" will be considered as external and it's content
        will only be used by other analysis unit to resolve objects. No
        quality rules will be computed on the analysis unit code when
        this option is selected.

Important:

-   when Java files are shared between different Analysis Unit, the only
    means to refer them as external is to refer them through the Add New
    Class Root Folder option. Never refer the related JAR files or
    .class files to make them external: it will create two distinct
    objects in the KB. As a result, metrics will not work and some Java
    classes may appear as unreferenced. Note that this means also that
    any .class or JAR that contains class that are defined as Java file
    in any Analysis Units MUST NOT be selected in any Analysis
    Units.
-   don't forget to select the checkbox to save the links from internal
    artifacts to external artifacts.