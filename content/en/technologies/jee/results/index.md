---
title: "Analysis results"
linkTitle: "Analysis results"
type: "docs"
weight: 7
---

## Objects

### Java

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Class.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_APPLET | Applet | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Class.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_CLASS | Java Class | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Interface.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_GENERIC_CLASS | Generic Java Class | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Category.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_GENERIC_INTERFACE | Generic Java Interface | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ClassInstance.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_INST_CLASS | Java Instantiated Class | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Category.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_INST_INTERFACE | Java Instantiated Interface | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Category.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_INTERFACE | Java Interface | Class |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Constructor.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_CTOR | Java Constructor | Class Constructor |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/VirtualConstructor.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_GENERIC_CTOR | Generic Java Constructor | Class Constructor |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ConstructorInstance.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_INST_CTOR | Java Instantiated Constructor | Class Constructor |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObjectBrown.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_EJB_ENTITY | Entity Java Bean | Data Entity |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_EJB_SESSION | Session Java Bean | Data Object |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_JDO | Java Data Object | Data Object |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataBlock.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_RECORD | Java Record | Data Object |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_RECORD_COMPONENT | Java Record Component | Data Object |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Enumeration.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_ENUM | Java Enum | Enumeration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/EnumItem.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_ENUM_ITEM | Java Enum Item | Enumeration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Java.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_FILE | Java File | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_GENERIC_METHOD | Generic Java Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Lambda.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_Lambda | Java Lambda Expression | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlueBracket.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_INIT | Java Initializer | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_INST_METHOD | Java Instantiated Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_METHOD | Java Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_PACKAGE | Java Package | Package |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FolderLightBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_IMPORT | Java Import | Project |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBean.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_EJB_MESSAGE | Message Driven Java Bean | Service Interface |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Variable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_FIELD | Java Field | Variable |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Variable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JV_TYPEPARAM | Generic Java Type Parameter | Variable |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Annotation.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_AnnotationType | Java Annotation Type |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionAnnotation.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_AnnotationTypeMethod | Java Annotation Type Method | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JAVA_AnonymousMethod | Method of Anonymous Class | Function |


### JSF (Java Server Face)

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Beans.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSF_MANAGED_BEAN | JSF Managed Bean | Bean |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSF_FACES_CONFIG_FILE | JSF Faces Config File | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionAction.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSF_CONVERTER | JSF Converter | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionAction.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSF_OUTCOME | JSF Outcome | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionValidator.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSF_VALIDATOR | JSF Validator | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UITextField.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSF_INPUT_FIELD | JSF Input Field | UI |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/UIView.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSF_VIEW_ID_PATTERN | JSF View Id Pattern | UI |

### JSP (Java Server Page)

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_PROPERTY_MAPPING | Java Property Mapping |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_SERVLET | Servlet |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_SERVLET_MAPPING | Servlet Mapping |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServicePurple.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_STXX_PIPELINE | STXX Pipeline |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Annotation.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_TAG | JSP Custom Tag |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Annotation.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_TAG_ATTR | JSP Custom Tag Attribute |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Package.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_TAG_LIB | JSP Custom Tag Library |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Beans.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_BEAN | J2EE Scoped Bean | Bean |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBean.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_BEAN_TILES_DEF | Tiles Definition | Bean |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_ACTION_MAPPING | Struts Action Mapping | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/CursorBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_FORWARD | Struts Forward | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_UIXML.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_GENERIC | J2EE XML Object | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_STXX_TRANSFORMS_FILE | STXX Transforms File | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_JSP.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_HTML5_JSP_Content | JSP Content | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_PROPERTIES_FILE | Java Properties File | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_TILES_DEF_FILE | Tiles Configuration File | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_XMLCONFIG_FILE | J2EE XML File | File |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionOrangeProcess.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_APPDESCRIPTOR | J2EE Web Application Descriptor | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_ELFUNCTION | JSP EL Function | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_STXX_TRANSFORM | STXX Transform | Service Invocation |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/VariablePropertyBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_ATTRIBUTES_SCOPE | Servlet Attributes Scope | Variable |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Beans.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_BEAN_PROP | Bean Property | Variable |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Variable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_BEAN_PROP_TILES_DEF_ATTRIBUTE | Tiles Definition Attribute | Variable |

### JEE Servlet

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_SERVLET | Servlet |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JSP_SERVLET_MAPPING | Servlet Mapping |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/WebService.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Servlet_AnyOperation | Servlet Any Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ResourceServiceDelete.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Servlet_DeleteOperation | Servlet Delete Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/GetResourceService.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Servlet_GetOperation | Servlet Get Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ResourceServicePost.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Servlet_PostOperation | Servlet Post Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ResourceServicePut.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Servlet_PutOperation | Servlet Put Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_HTML5_JEE_Servlet_Mapping_Access | JEE Servlet Mapping Call | Service Interface |

### Struts

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ListBean.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | STRUTS_FORM_BEAN | Struts Form Bean | Bean |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_Struts2_ConfigurationFile | Struts2 Configuration File | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_StrutsAction | Struts Action | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionAction.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_StrutsFilter | Struts Filter | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_StrutsInterceptor | Struts Interceptor | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessRed.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_StrutsInterceptorStack | Struts Interceptor Stack | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataSet.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_StrutsResult | Struts Result | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | STRUTS_CONFIG_FILE | Struts Configuration File | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ResourceServiceGet.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Struts_GetOperation | Struts Get Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/WebService.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Struts_Operation | Struts Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ResourceServicePost.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Struts_PostOperation | Struts Post Operation | Exposed Web Services |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionValidator.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_StrutsValidator | Struts Validator | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionValidator.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Struts_FormValidator | Struts Validate Operation | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/FunctionConverter.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Struts_ParamInterceptor | Struts ParameterInterceptor | Function |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/PackageOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_JEE_StrutsPackage | Struts Package | Package |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ServiceOrange.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_HTML5_JEE_Struts_Action_Access | JEE Struts Action Call | Service Interface |

### JPA/Hibernate

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_EMBEDDABLE | JPA Embeddable |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_PERSISTENCE_UNIT | JPA Persistence Unit |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataSet.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_SQL_RESULT_SET | JPA Result Set |  |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_ORM_CONFIG_FILE | JPA ORM Configuration File | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/File_Grey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_PERS_CONFIG_FILE | JPA Persistence configuration File | Configuration |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataModel.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_JPA_Entity | JPA Entity | Data Entity |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessBlue.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_JPA_Entity_Operation | JPA Entity Operation | Data Entity |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataModuleExternal.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_Unknown_JPA_Entity | JPA Unknown Entity | Data Entity |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/ProcessExternal.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_Unknown_JPA_Entity_Operation | JPA Unknown Entity Operation | Data Entity |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/DataObject.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_ENTITY | JPA Entity | Data Entity |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/SQLNamedQuery.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_JPA_JPQLQuery | JPQL Query | Data Query |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/SQLNamedQuery.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_JPA_SQLQuery | JPA SQL Query | Data Query |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/SQLNamedQueryGrey.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java_Unknown_JPA_SQLQuery | JPA Unknown SQL Query | Data Query |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/SQLNamedQuery.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_SpringData_JPA_SQLQuery | Spring Data JPQL Query | Data Query |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/SQLNamedQuery.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_NAMED_NATIVE_QUERY | JPA Named Native Query | Data Query |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/SQLNamedQuery.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_NAMED_QUERY | JPA Named Query | Data Query |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Variable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_EMBEDDABLE_PROPERTY | JPA Embeddable Property | Variable |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/Variable.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | JPA_ENTITY_PROPERTY | JPA Entity Property | Variable |

### Program Call

| Icon Image | ID | Description | Concept |
|---|---|---|---|
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/CallToProgram.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java2Program_GeneralCall | Java Call to Generic Program | Run Service |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/CallToJavaProgram.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java2Program_JarProgram | Java JAR Program | Run Service |
| <img src="https://raw.githubusercontent.com/CAST-Extend/resources/master/genericIcons/CallToJavaProgram.svg" alt="icon" width="32" height="32" style="display: block; margin: auto;" /> | CAST_Java2Program_JavaCall | Java Call to Java Program | Run Service |


## Links

The following is a non-exhaustive list of links that may be identified
between objects saved in the CAST Analysis Service:

### References that are detected by the analyzer while analyzing JSP Pages and static references 

Click here to expand...

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><p>Link Type</p></td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">When is this type of link created?</td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey">Example</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>MENTION</strong></td>
<td class="confluenceTd">This link is created for the <strong>taglib
directive</strong>.
<p>The caller is the JSP File where the directive is defined.</p>
<p>The callee is the Tag Library identified by the
"<strong>uri"</strong> attribute.</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%</span><span class="at">@</span> taglib uri<span class="op">=</span><span class="st">&quot;/examples-taglib&quot;</span> prefix<span class="op">=</span><span class="st">&quot;eg&quot;</span><span class="op">%&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>INCLUDE</strong></td>
<td class="confluenceTd">This link is created for the <strong>include
directive</strong>.
<p>The caller is the JSP File where the directive is defined.</p>
<p>The callee is a JSP File, Static File or a JSP Mapping as specified
by the "<strong>file"</strong> attribute.</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%</span><span class="at">@</span> include file<span class="op">=</span><span class="st">&quot;URL&quot;</span><span class="op">%&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>INHERIT</strong></td>
<td class="confluenceTd">This link is used to describe object hierarchy.
It is created for the <strong>page directive</strong>.
<p>It is always enriched by the sub-type <strong>Extend</strong> (class
to super-class).</p>
<p>The caller is the JSP File where the directive is defined.</p>
<p>The callee is a Java Class as specified by the
"<strong>extends"</strong> attribute.</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%</span><span class="at">@</span> page <span class="kw">extends</span><span class="op">=</span><span class="st">&quot;com.cast.SuperClass&quot;</span><span class="op">%&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>THROW</strong></td>
<td class="confluenceTd">This link is used to describe exception
handling. It is created for the <strong>page directive</strong>.
<p>The caller is the JSP File where the directive is defined.</p>
<p>The callee is a File (JSP or static) or a JSP Mapping as specified by
the "<strong>errorPage"</strong> attribute.</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%</span><span class="at">@</span> page errorPage<span class="op">=</span><span class="st">&quot;URL&quot;</span><span class="op">%&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>ACCESS</strong></td>
<td class="confluenceTd"><p>This link is used to describe run-time
interactions. It may be enriched by the following sub-types:</p>
<ul>
<li><strong>Read</strong></li>
<li><strong>Write</strong></li>
<li><strong>Forward</strong></li>
<li><strong>Include</strong></li>
</ul>
<p>In the "example" column you will find a list of all cases when these
links are created. If omitted, the caller is the JSP File where the
standard or custom action is defined.</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Read</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb5"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>jsp<span class="op">:</span>getProperty name<span class="op">=</span><span class="st">&quot;clock&quot;</span> property<span class="op">=</span><span class="st">&quot;dayOfMonth&quot;</span><span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
<p>The callee is the Bean Property as specified by the
<strong>"property"</strong> attribute.</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Write</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb6"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>jsp<span class="op">:</span>setProperty name<span class="op">=</span><span class="st">&quot;clock&quot;</span> property<span class="op">=</span><span class="st">&quot;dayOfMonth&quot;</span> value<span class="op">=</span><span class="st">&quot;13&quot;</span><span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
<p>The callee is the Bean Property as specified by the
<strong>"property"</strong>attribute.&gt;</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb7"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>eg<span class="op">:</span>foo attr1<span class="op">=</span><span class="st">&quot;value&quot;</span><span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
<p>The callee is the Tag Attribute as specified by the name of an
attribute of the custom action.</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Forward</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb8"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>jsp<span class="op">:</span>forward page<span class="op">=</span><span class="st">&quot;URL&quot;</span><span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
<p>The callee is a File (JSP or static) or a JSP Mapping.</p>
<p>Definition of a servlet mapping: the caller is the Servlet Mapping
and the callee is the associated Servlet.</p>
<p>Definition of a property mapping: the caller is the Property Mapping
and the callee is a File (JSP or static), a JSP Mapping or a Java
Type.</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Include</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb9"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>jsp<span class="op">:</span>include page<span class="op">=</span><span class="st">&quot;file&quot;</span><span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
<p>The callee is a File (JSP or static) or a JSP Mapping.</p>
</div></td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>RELY ON</strong></td>
<td class="confluenceTd">This link is used to describe typing.
<p><strong>Caller</strong> uses callee to define its Java type.
<strong>Callee</strong> always refers to a class or interface.</p>
<p>In our case the caller is a Bean and the callee is its class as
specified by either <strong>"class"</strong> or
<strong>"beanName"</strong> attribute.</p>
<p>Note that for all beans created from a XML Configuration file, a link
of this type is automatically created.</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb10"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>jsp<span class="op">:</span>useBean id<span class="op">=</span><span class="st">&quot;clock&quot;</span> <span class="kw">class</span><span class="op">=</span><span class="st">&quot;beans.clockBean&quot;</span> <span class="kw">...</span> <span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>USE</strong></td>
<td class="confluenceTd">This link is created for the
<strong>jsp:useBean</strong>standard action.
<p>The caller is the JSP File where the action is defined.</p>
<p>The callee is a Bean as specified by the <strong>"id"</strong>
attribute.</p>
<p>This type is also used for dynamic link resolution. It covers server
side objects referencing.</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb11"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>jsp<span class="op">:</span>useBean id<span class="op">=</span><span class="st">&quot;clock&quot;</span> <span class="kw">class</span><span class="op">=</span><span class="st">&quot;beans.clockBean&quot;</span> <span class="kw">...</span> <span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>PROTOTYPE</strong></td>
<td class="confluenceTd">This link is used as follows:
<p><strong>Servlet:</strong> between the servlet component and its
servlet class or JSP File as specified in the application descriptor via
"<strong>servlet-class</strong>" or "<strong>jsp-file</strong>" XML
element.</p>
<p><strong>Custom Tag</strong>: between the tag object and its class or
tei-class as specified in the tag library descriptor via
<strong>"tag-class"</strong> and "<strong>tei-class"</strong> xml
elements.</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>USER DEFINED</strong></td>
<td class="confluenceTd">These links are entirely specified by the user
in the application's additional XML configuration files.
<p>Users can use the following string values to specify the link
type:</p>
<p><strong>access</strong> (Access), <strong>call</strong> (Call),
<strong>catch</strong> (Catch), <strong>fire</strong> (Fire),
<strong>include</strong> (Include), <strong>inherit</strong> (Inherit),
<strong>issonof</strong> (Is Son Of), <strong>lock</strong> (Lock ),
<strong>mention</strong> (Mention), <strong>prototype</strong>
(Prototype), <strong>raise</strong> (Raise), <strong>refer</strong>
(Refer), <strong>relyon</strong> (Rely On), <strong>throw</strong>
(Throw) or <strong>use</strong> (Use).</p>
<p>The following table shows a non exhaustive list of these types of
links:</p>
<div data-align="left">
<p><br />
</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Caller</strong></td>
<td class="confluenceTd"><strong>Callee</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd">XML Configuration File</td>
<td class="confluenceTd"><ul>
<li>Java Type (class or interface)</li>
<li>Java Method</li>
<li>JSP Mapping</li>
<li><p>File (JSP or static)</p></li>
<li><p>Bean</p></li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Action Mapping</td>
<td class="confluenceTd"><ul>
<li>Java Type (class or interface)</li>
<li>JSP Mapping</li>
<li>File (JSP or static)</li>
<li>Bean</li>
</ul></td>
</tr>
</tbody>
</table>
</div>
</div></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>ESCALATED</strong></td>
<td class="confluenceTd">When two objects are linked via a third
object.</td>
<td class="confluenceTd">Denoted by the square brackets around the link
type in CAST Enlighten: <strong>[U]</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>INTERNAL ESCALATED</strong></td>
<td class="confluenceTd">Created when the calling object is not saved in
the CAST Knowledge Base (i.e. a variable that is local to a function).
As a result the analyzer will save the link and associate it with the
parent of the calling object.</td>
<td class="confluenceTd">Denoted by the curly brackets (or braces)
around the link type in CAST Enlighten: <strong>{U}</strong></td>
</tr>
</tbody>
</table>

### References detected by the analyzer when carrying out a pure Java analysis

Click here to expand...

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">When is this type of link created?</th>
<th colspan="2" class="confluenceTh">Example</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><strong>MENTION</strong></td>
<td class="confluenceTd">This link is traced on following expressions:
<p>- "instance of" operator</p>
<p>- cast expression</p>
<p>- class literal</p>
<p>- this literal</p>
<p>- class creation</p>
<p>- array creation</p>
<p><strong>Callee</strong> always refers to a class or
interface.</p></td>
<td colspan="2" class="confluenceTd">- if (b instanceof A . class) ...
<p>- A . this = 1;</p>
<p>- B b = (B) new A(1);</p>
<p>- B b [] = new A[5];</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>THROW, RAISE, CATCH</strong></td>
<td class="confluenceTd">These links are used to describe exception
handling.
<ul>
<li><strong>Throw</strong> link is traced on "throws" clause of method
declaration towards exceptions possibly raised by the method.</li>
<li><strong>Raise</strong> link is traced on "throw" statement towards
type of the exception actually thrown by the method.</li>
<li><strong>Catch</strong> link is traced on "try" statement towards
type of the exception parameter declared in catch clause(s).</li>
</ul>
<p><strong>Callee</strong> always refers to a class.</p></td>
<td colspan="2" class="confluenceTd">- void f () throws C1 {...}
<p>- throw new Exception()</p>
<p>- try {...}<br />
catch (Throwable e) {...)</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>RELY ON</strong></td>
<td class="confluenceTd">This link is used to describe typing.
<p><strong>Caller</strong> uses callee to define its Java type.</p>
<p><strong>Callee</strong> always refers to a class or
interface.</p></td>
<td colspan="2" class="confluenceTd">- void f (A a) {...}
<p>- A a [] = null;</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>ACCESS</strong></td>
<td class="confluenceTd">This link is used to describe run-time
interactions.
<p>It may be enriched by the following sub-types:</p>
<p>- <strong>Read</strong></p>
<p>- <strong>Write</strong></p>
<p>- <strong>Exec</strong></p>
<p>- <strong>Member</strong></p>
<p>- <strong>Array</strong></p></td>
<td class="confluenceTd">- return x
<p>- x = 1</p>
<p>- x++</p>
<p>- x[1] = 2</p>
<p>- x[] = {1,2}</p>
<p>- f(1)</p>
<p>- x.toString()</p>
<p>- x.f()</p>
<p>- x.y = 1</p></td>
<td class="confluenceTd">- read
<p>- write</p>
<p>- read and write</p>
<p>- array not read</p>
<p>- read</p>
<p>- exec</p>
<p>- member</p>
<p>- member on x ; exec on f</p>
<p>- member on x ;write on y</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>INHERIT</strong></td>
<td class="confluenceTd">This link is used to describe object hierarchy.
It may be enriched by the following sub-types:
<p><strong>- Extend</strong>:</p>
<blockquote>
<ul>
<li>class to super-class</li>
<li>interface to super-interface</li>
</ul>
</blockquote>
<p>- <strong>Implement</strong>:</p>
<blockquote>
<ul>
<li>class to super-interface(s)</li>
</ul>
</blockquote>
<p><strong>Caller</strong> and <strong>callee</strong> always refer to
classes or interfaces.</p>
<p>By extension the Inherit link is also used to describe method
overriding as directly related to inheritance. In this case exclusive
sub-types are available:</p>
<p>- <strong>Hide</strong>: for static methods;</p>
<p>- <strong>Override</strong>: for instance methods; in addition link
can be enriched by the <strong>Implement</strong> sub-type if overriding
is from a non abstract overriding method to an abstract overridden
one.</p>
<p><strong>Caller</strong> and <strong>callee</strong> always refer to
methods.</p></td>
<td colspan="2" class="confluenceTd">- class C1 extends C2 implements
I<br />
{...}
<p>- interface I1 extends implements I2<br />
{...}</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>USE</strong></td>
<td class="confluenceTd">This type is reserved for dynamic link
resolution. It covers server side objects referencing:
<p><strong>- Select</strong></p>
<p><strong>- Insert</strong></p>
<p><strong>- Update</strong></p>
<p><strong>- Delete</strong></p></td>
<td colspan="2" class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>PROTOTYPE</strong></td>
<td class="confluenceTd">This link is reserved for J2EE components'
support and is used as follows:
<ul>
<li><strong>Applet:</strong> between the applet component and its applet
class.</li>
</ul>
<p><strong>Caller</strong> and <strong>callee</strong> always refer to a
component and to a class or interface.</p></td>
<td colspan="2" class="confluenceTd">-</td>
</tr>
</tbody>
</table>

-   "Exec access" links can be thought of as invocation calls.
-   To avoid redundancy with a corresponding escalated link, a "member
    access" link is not traced on a qualifier if this one matches the
    member parent.
-   "Prototype" links are never escalated.

### References that are detected by the analyzer while analyzing JSP pages and Tag Files as client files

Click here to expand...

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td colspan="2" class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Link Type</strong></td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>When is this type of link
created?</strong></td>
</tr>
<tr class="even">
<td rowspan="6" class="confluenceTd"><strong>USE</strong></td>
<td class="confluenceTd"><strong>TypLib</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;!--</span>METADATA TYPE <span class="op">=</span> <span class="st">&quot;TypeLib&quot;</span> FILE <span class="op">=</span> <span class="st">&quot;TypLibFileName&quot;</span> <span class="op">--&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Applet</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>APPLET CODE <span class="op">=</span> <span class="st">&quot;AppletCode&quot;</span> CODEBASE <span class="op">=</span> <span class="st">&quot;AppletCodeBase&quot;</span> <span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>ActiveX</strong> through a
<strong>variable</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a>x <span class="op">=</span> <span class="kw">new</span> <span class="fu">ActiveXObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>x<span class="op">.</span><span class="fu">Method</span><span class="op">()</span></span></code></pre></div>
</div>
</div>
<p><strong>Use</strong> link between f and A.B</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Dynamic data source</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>OBJECT id <span class="op">=</span> id_obj classid <span class="op">=</span> <span class="st">&quot;clsid:Sample&quot;</span><span class="op">&gt;&lt;/</span>OBJECT<span class="op">&gt;</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>A DATASRC<span class="op">=</span>#id_obj datafld <span class="op">=</span> <span class="st">&quot;url&quot;</span> id<span class="op">=</span>id_a<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Use</strong> link between id_a and id_obj</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p><strong>Database object</strong></p>
<p><strong><br />
</strong></p></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb5"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a><span class="fu">ExecuteSQL</span><span class="op">(</span><span class="st">&quot;select * from authors&quot;</span><span class="op">)</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>MAP</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb6"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>img src<span class="op">=</span><span class="st">&quot;images/cover.jpg&quot;</span> border<span class="op">=</span><span class="dv">0</span> usemap<span class="op">=</span><span class="st">&quot;#covermap&quot;</span> ismap id<span class="op">=</span>id_img<span class="op">&gt;</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>map name<span class="op">=</span><span class="st">&quot;covermap&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;whatis.htm&quot;</span> coords<span class="op">=</span><span class="st">&quot;0,0,315,198&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;signup.asp&quot;</span> coords<span class="op">=</span><span class="st">&quot;0,198,230,296&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;holdings.asp&quot;</span> coords<span class="op">=</span><span class="st">&quot;229,195,449,296&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>area shape<span class="op">=</span>rect href<span class="op">=</span><span class="st">&quot;question.htm&quot;</span> coords<span class="op">=</span><span class="st">&quot;314,0,449,196&quot;</span><span class="op">&gt;</span></span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>map<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
</tbody>
<tbody>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><strong>MENTION</strong></td>
<td class="confluenceTd">Indicates the area in which the
<strong>ActiveX</strong> is mentioned</td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb7"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="fu">CreateObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
</div>
</div>
<p><strong>Mention</strong> link between f and A.B</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">Indicates the area in which the <strong>class
name</strong> is mentioned</td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb8"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="kw">new</span> <span class="fu">g</span><span class="op">()</span></span>
<span id="cb8-4"><a href="#cb8-4" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
</div>
</div>
<p><strong>Mention</strong> link between f and g</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>INCLUDE</strong></td>
<td class="confluenceTd">Indicates the <strong>inclusion</strong> of a
<strong>file</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb9"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT LANGUAGE <span class="op">=</span> <span class="st">&quot;JavaScript&quot;</span> SRC <span class="op">=</span> <span class="st">&quot;inc.js&quot;</span><span class="op">&gt;</span></span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"><strong>CALL</strong></td>
<td class="confluenceTd">Indicates a <strong>call</strong> to a
<strong>file</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb10"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>A HREF <span class="op">=</span> <span class="st">&quot;called_file.htm&quot;</span> <span class="op">&gt;&lt;/</span>A<span class="op">&gt;</span></span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT LANGUAGE <span class="op">=</span> JavaScript<span class="op">&gt;</span></span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true" tabindex="-1"></a>window<span class="op">.</span><span class="fu">open</span><span class="op">(</span><span class="st">&quot;called_file.htm&quot;</span><span class="op">)</span></span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Indicates a <strong>function call</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb11"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="dv">1</span><span class="op">;</span></span>
<span id="cb11-5"><a href="#cb11-5" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb11-6"><a href="#cb11-6" aria-hidden="true" tabindex="-1"></a>function <span class="fu">g</span><span class="op">()</span></span>
<span id="cb11-7"><a href="#cb11-7" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb11-8"><a href="#cb11-8" aria-hidden="true" tabindex="-1"></a><span class="cf">return</span> <span class="fu">f</span><span class="op">()</span></span>
<span id="cb11-9"><a href="#cb11-9" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb11-10"><a href="#cb11-10" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Call</strong> link between g and f</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>ACCESS</strong></td>
<td class="confluenceTd">Indicates a <strong>access</strong> type link
enters a <strong>property</strong> of an HTC component together with the
associated PUT or GET function.</td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb12"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>PUBLIC<span class="op">:</span>PROPERTY NAME<span class="op">=</span><span class="st">&quot;xmlData&quot;</span></span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true" tabindex="-1"></a> ID<span class="op">=</span><span class="st">&quot;xmlDataID&quot;</span> </span>
<span id="cb12-3"><a href="#cb12-3" aria-hidden="true" tabindex="-1"></a>GET<span class="op">=</span><span class="st">&quot;getxmlData&quot;</span> PUT<span class="op">=</span><span class="st">&quot;putxmlData&quot;</span><span class="op">/&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>ACCESS</strong> link between xmlData and the getxmlData and
putxmlData functions.</p>
</div></td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><strong>ACCESS and<br />
READ</strong></td>
<td class="confluenceTd"><strong>Read only access</strong> to a
<strong>file</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb13"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;!--</span>#FLASTMODE FILE <span class="op">=</span> <span class="st">&quot;accessed_file.htm&quot;</span><span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Read only access</strong> to a
<strong>variable</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb14"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb14-2"><a href="#cb14-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb14-3"><a href="#cb14-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb14-4"><a href="#cb14-4" aria-hidden="true" tabindex="-1"></a>y<span class="op">=</span>x</span>
<span id="cb14-5"><a href="#cb14-5" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb14-6"><a href="#cb14-6" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Read only access</strong> between f and x</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>ACCESS and<br />
WRITE</strong></td>
<td class="confluenceTd"><strong>Read and write access</strong> to a
<strong>variable</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb15"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb15-1"><a href="#cb15-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb15-2"><a href="#cb15-2" aria-hidden="true" tabindex="-1"></a>function <span class="fu">f</span><span class="op">()</span></span>
<span id="cb15-3"><a href="#cb15-3" aria-hidden="true" tabindex="-1"></a><span class="op">{</span></span>
<span id="cb15-4"><a href="#cb15-4" aria-hidden="true" tabindex="-1"></a>y<span class="op">=</span>x</span>
<span id="cb15-5"><a href="#cb15-5" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb15-6"><a href="#cb15-6" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Read and write access</strong> between f and y</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>ACCESS and PAGE_FORWARD</strong></td>
<td class="confluenceTd">Indicates a redirection. Only available for
analyzed IIS applications.</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>REFER</strong></td>
<td class="confluenceTd">Indicates that a <strong>variable</strong>
refers to another <strong>variable</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb16"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb16-2"><a href="#cb16-2" aria-hidden="true" tabindex="-1"></a>x <span class="op">=</span> <span class="kw">new</span> <span class="fu">ActiveXObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb16-3"><a href="#cb16-3" aria-hidden="true" tabindex="-1"></a><span class="fu">Application</span><span class="op">(</span><span class="st">&quot;y&quot;</span><span class="op">)=</span>x</span>
<span id="cb16-4"><a href="#cb16-4" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>Refer</strong> link between Application("y") and x</p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>RELY ON and INSTANCE OF</strong></td>
<td class="confluenceTd">Indicates that a <strong>variable</strong> is
an <strong>instance of a class</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb17"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb17-1"><a href="#cb17-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>SCRIPT<span class="op">&gt;</span></span>
<span id="cb17-2"><a href="#cb17-2" aria-hidden="true" tabindex="-1"></a>x<span class="op">=</span><span class="kw">new</span> <span class="fu">ActiveXObject</span><span class="op">(</span><span class="st">&quot;A.B&quot;</span><span class="op">)</span></span>
<span id="cb17-3"><a href="#cb17-3" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>SCRIPT<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>INSTANCE_OF</strong> link between x and A.B</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>GO THROUGH</strong></td>
<td class="confluenceTd">Indicates the error file(s) used. Only
available for analyzed IIS applications.</td>
<td class="confluenceTd">-</td>
</tr>
</tbody>
</table>

### References that are detected by the analyzer while analyzing the JSP page implementation class and/or Tag Handlers are dynamic references

Click here to expand...

For the dynamic references which are issued from a grep or have the
caller located in a JSP script element as a declaration, expression or
scriplet, a static link will be created. This new link has almost
the same properties as the dynamic link, however the caller can be
different. For example, when an object is located in an included file we
will have a dynamic reference from the \_jspService function of the JSP
Page (resp. doTag method of a Tag File) and the object and a static
reference from the included JSP File (resp. Tag File) and the object.

Note that the main function of the generated servlet/handler class is a
pure dynamic object. So, it cannot have static references and we will
create them at the function's parent (JSP Page/Tag File) level.

The following table summarizes the process of creation of the static
links using the dynamic links while parsing generated servlet classes
('Dynamic Caller' are replace by 'Static caller'):

|                            |                                      |                                      |
|----------------------------|--------------------------------------|--------------------------------------|
| Caller: Dynamic Object | Link code located in             | Caller: Static Object            |
| \_jspService               | JSP page                             | JSP page                             |
| \_jspService               | JSP page                             | JSP page                             |
| servlet class              | JSP page                             | JSP page                             |
| servlet class              | JSP File (included file)             | JSP File (included file)             |
| caller                     | JSP Page or JSP File (included file) | JSP Page or JSP File (included file) |

##### Code sample

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>file a.jsp</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%!</span> <span class="bu">String</span> redirectA<span class="op">;</span> <span class="op">%&gt;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%</span> redirectA <span class="op">=</span> <span class="st">&quot;/jsp/fileA.jsp&quot;</span><span class="op">;</span> <span class="op">%&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img
src="http://doc.castsoftware.com/plugins/servlet/confluence/placeholder/unknown-attachment?locale=en_GB&amp;version=2"
data-image-src="http://doc.castsoftware.com/plugins/servlet/confluence/placeholder/unknown-attachment?locale=en_GB&amp;version=2" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>file a.jsp</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%</span><span class="at">@</span> include file<span class="op">=</span><span class="st">&quot;included.jsp&quot;</span><span class="op">%&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>file included.jsp</strong></p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%!</span> <span class="bu">String</span> includedA<span class="op">;</span> <span class="op">%&gt;</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;%</span> includedA <span class="op">=</span> <span class="st">&quot;/jsp/fileA.jsp&quot;</span><span class="op">;</span> <span class="op">%&gt;</span></span></code></pre></div>
</div>
</div>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img
src="http://doc.castsoftware.com/plugins/servlet/confluence/placeholder/unknown-attachment?locale=en_GB&amp;version=2"
data-image-src="http://doc.castsoftware.com/plugins/servlet/confluence/placeholder/unknown-attachment?locale=en_GB&amp;version=2" /></p>
</div></td>
</tr>
</tbody>
</table>

### EJB links

Click here to expand...

The following lists describes references that are detected by the JEE
analyzer and the context in which corresponding links are traced and
stored in the CAST Analysis Service:

Links between bean and class/interfaces are flagged as "Prototype" and
are oriented as follows:

-   From bean local and remote interfaces to the EJB itself,
-   From the EJB itself to its bean class and, for entity bean, primary
    key class.

In addition the following links are also traced:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th colspan="2" class="confluenceTh">Link Type</th>
<th class="confluenceTh">When is this type of link created?</th>
</tr>
&#10;<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>FIRE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>1. Traced between <strong>bean class member methods</strong> that are
implicitly called by the <strong>EJB Container</strong>:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Home interface</strong></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><strong>Bean class</strong></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><strong>Remote interface</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd">create(...)</td>
<td class="confluenceTd"><strong>--F--&gt;</strong></td>
<td class="confluenceTd">ejbCreate(...)</td>
<td class="confluenceTd"><strong>&lt;--F--</strong></td>
<td class="confluenceTd">create(...)</td>
</tr>
<tr class="odd">
<td class="confluenceTd">remove(...)</td>
<td class="confluenceTd"><strong>--F--&gt;</strong></td>
<td class="confluenceTd">ejbRemove(...)</td>
<td class="confluenceTd"><strong>&lt;--F--</strong></td>
<td class="confluenceTd">remove(...)</td>
</tr>
<tr class="even">
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><em>&lt; implemented business methods
&gt;</em></td>
<td class="confluenceTd"><strong>&lt;--F--</strong></td>
<td class="confluenceTd"><em>&lt; exported business methods
&gt;</em></td>
</tr>
<tr class="odd">
<td class="confluenceTd">find<em>&lt;method&gt;</em>(...)</td>
<td class="confluenceTd"><strong>--F--&gt;</strong></td>
<td class="confluenceTd">ejbFind<em>&lt;method&gt;</em>(...)</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>
</div>
<p>2. Traced between <strong>bean class/home interface</strong> and
<strong>server side objects</strong>. For example in this standard
ejb-jar.xml file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>entity<span class="op">&gt;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>CustomerBean<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span>Customer<span class="op">&lt;/</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>query<span class="op">&gt;</span></span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>query<span class="op">-</span>method<span class="op">&gt;</span></span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>method<span class="op">-</span>name <span class="op">&gt;</span>findByFirstName<span class="op">&lt;/</span>method<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>method<span class="op">-</span>params<span class="op">&gt;</span></span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true" tabindex="-1"></a>            <span class="op">&lt;</span>method<span class="op">-</span>param<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">lang</span><span class="op">.</span><span class="fu">String</span><span class="op">&lt;/</span>method<span class="op">-</span>param<span class="op">&gt;</span></span>
<span id="cb1-11"><a href="#cb1-11" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;/</span>method<span class="op">-</span>params<span class="op">&gt;</span></span>
<span id="cb1-12"><a href="#cb1-12" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;/</span>query<span class="op">-</span>method<span class="op">&gt;</span></span>
<span id="cb1-13"><a href="#cb1-13" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>ejb<span class="op">-</span>ql<span class="op">&gt;</span>SELECT DISTINCT <span class="fu">OBJECT</span><span class="op">(</span>c<span class="op">)</span> FROM Customer AS c WHERE c<span class="op">.</span><span class="fu">firstName</span> <span class="op">=</span> <span class="op">?</span><span class="dv">1</span><span class="op">&lt;/</span>ejb<span class="op">-</span>ql<span class="op">&gt;</span></span>
<span id="cb1-14"><a href="#cb1-14" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>query<span class="op">&gt;</span></span>
<span id="cb1-15"><a href="#cb1-15" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>entity<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p>The signature of the method is created from the values of the tags
&lt;method-name&gt; and &lt;method-param&gt;. The method is searched for
using the home interfaces (for the findXXX) and the bean class (for the
"select methods").</p>
<p>The abstract schema name identified by grep in the query "EJB-QL" is
used to find the associated bean (the query can reference the abstract
schema name of another bean). From the bean it is possible to retrieve
the associated table names and then link resolution is possible.</p>
<p>3. Also traced between <strong>SEI.Mi (Service Endpoint
Interface)</strong> and <strong>BC.Mi (Bean Class)</strong>. Mi stands
for methods that have the same signature in the SEI and the BC. The EJB
must be a stateless session bean:</p>
<ul>
<li>SEI.Mi -- Fire --&gt; BC.Mi</li>
</ul>
<p>4. In addition, this link type is used for implicit invocations. They
are used to reflect:</p>
<ul>
<li>either the life cycle events of a component (such as
creation/destruction)</li>
<li>or the interaction of an external actor such as a container (for
EJBs or servlets) or a web browser (for applets)</li>
</ul>
<p><strong>Caller</strong> always refers to a component or a class
member method. <strong>Callee</strong> always refers to a class member
method.</p>
<div>
<div>
Note: "Fire" links are never escalated.
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd"><strong>USE</strong></td>
<td class="confluenceTd"><strong>SELECT</strong></td>
<td rowspan="4" class="confluenceTd"><div class="content-wrapper">
Traced between bean and server objects (tables).
<ul>
<li>Bean <strong>--Usdui--&gt;</strong> Table (U for Use, s for Select,
d for Delete, u for Update, i for Insert)</li>
<li>Finder and select Method <strong>--Us--&gt;</strong> Table</li>
</ul>
<p>The link <strong>Bean --Usdui--&gt; Table</strong> is traced only
with container managed persistence (cmp) beans. For Bean managed
persistence (bmp) beans, this link is indirect and is represented by two
links:</p>
<ul>
<li>One from Bean component to its java class (traced by EJB Assistant
).</li>
<li>One from Methods of the bean class where access to database objects
is coded (via jdbc) to server objects corresponding to these objects
(tables in this case). This link is traced by Java Analyzer.</li>
</ul>
<p>For example in this standard ejb-jar.xml file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>entity id<span class="op">=</span><span class="st">&quot;ContainerManagedEntity_1&quot;</span><span class="op">&gt;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span>Player<span class="op">&lt;/</span><span class="kw">abstract</span><span class="op">-</span>schema<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true" tabindex="-1"></a>   <span class="kw">...</span></span></code></pre></div>
</div>
</div>
<p>The bean in question is found in the ejb-jar.xml file using its name.
Persistence information is identified using descriptors specific to each
server type:</p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>DELETE</strong></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>UPDATE</strong></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>INSERT</strong></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>PROTOTYPE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
This link type is reserved for J2EE components' support and is used as
follows:
<ul>
<li><strong>EJB:</strong> between the bean component and its
implementation classes (bean class, local/remote interfaces and possibly
primary key class).</li>
<li><strong>Servlet:</strong> between the servlet component and its
servlet class.</li>
<li><strong>EJB</strong>: between the bean and the SEI (Service Endpoint
Interface).</li>
</ul>
<p>For example the <strong>Webservices.xml</strong> (must be placed in
the module directory)</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>webservices <span class="kw">...</span><span class="op">&gt;</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>webservice<span class="op">-</span>description<span class="op">&gt;</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a>      <span class="kw">...</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>webservice<span class="op">-</span>description<span class="op">-</span>name<span class="op">&gt;</span>MyHelloService<span class="op">&lt;/</span>webservice<span class="op">-</span>description<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>jaxrpc<span class="op">-</span>mapping<span class="op">-</span>file<span class="op">&gt;</span>mapping<span class="op">.</span><span class="fu">xml</span><span class="op">&lt;/</span>jaxrpc<span class="op">-</span>mapping<span class="op">-</span>file<span class="op">&gt;</span> <span class="co">// relatif ou absolu</span></span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>port<span class="op">-</span>component<span class="op">&gt;</span></span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>      <span class="kw">...</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>port<span class="op">-</span>component<span class="op">-</span>name<span class="op">&gt;</span>HelloIF<span class="op">&lt;/</span>port<span class="op">-</span>component<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span>helloservice<span class="op">.</span><span class="fu">HelloIF</span><span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span></span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>service<span class="op">-</span>impl<span class="op">-</span>bean<span class="op">&gt;</span></span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true" tabindex="-1"></a>            <span class="op">&lt;</span>ejb<span class="op">-</span>link<span class="op">&gt;</span>HelloServiceEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>link<span class="op">&gt;</span></span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;/</span>service<span class="op">-</span>impl<span class="op">-</span>bean<span class="op">&gt;</span></span>
<span id="cb3-13"><a href="#cb3-13" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;/</span>port<span class="op">-</span>component<span class="op">&gt;</span></span>
<span id="cb3-14"><a href="#cb3-14" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>webservice<span class="op">-</span>description<span class="op">&gt;</span></span>
<span id="cb3-15"><a href="#cb3-15" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>webservices<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p><strong>JAX-RPC mapping file</strong> (mapping.xml in the
example) indicates that the mapping information between the web service
operation and the java methods can be found in mapping.xml.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb4"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>java<span class="op">-</span>wsdl<span class="op">-</span>mapping <span class="kw">...</span><span class="op">&gt;</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="kw">...</span></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>port<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>port<span class="op">-</span>name<span class="op">&gt;</span>HelloIFPort<span class="op">&lt;/</span>port<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;/</span>port<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>service<span class="op">-</span><span class="kw">interface</span><span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span>helloservice<span class="op">.</span><span class="fu">HelloIF</span><span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">&gt;</span></span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>service<span class="op">-</span>endpoint<span class="op">-</span>method<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>java<span class="op">-</span>method<span class="op">-</span>name<span class="op">&gt;</span>sayHello<span class="op">&lt;/</span>java<span class="op">-</span>method<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>wsdl<span class="op">-</span>operation<span class="op">&gt;</span>sayHello<span class="op">&lt;/</span>wsdl<span class="op">-</span>operation<span class="op">&gt;</span></span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true" tabindex="-1"></a>            <span class="op">&lt;</span>method<span class="op">-</span>param<span class="op">-</span>parts<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-13"><a href="#cb4-13" aria-hidden="true" tabindex="-1"></a>               <span class="op">&lt;</span>param<span class="op">-</span>position<span class="op">&gt;</span><span class="dv">0</span><span class="op">&lt;/</span>param<span class="op">-</span>position<span class="op">&gt;</span></span>
<span id="cb4-14"><a href="#cb4-14" aria-hidden="true" tabindex="-1"></a>               <span class="op">&lt;</span>param<span class="op">-</span>type<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">lang</span><span class="op">.</span><span class="fu">String</span><span class="op">&lt;/</span>param<span class="op">-</span>type<span class="op">&gt;</span></span>
<span id="cb4-15"><a href="#cb4-15" aria-hidden="true" tabindex="-1"></a>               <span class="kw">...</span></span>
<span id="cb4-16"><a href="#cb4-16" aria-hidden="true" tabindex="-1"></a>               <span class="op">&lt;/</span>method<span class="op">-</span>param<span class="op">-</span>parts<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-17"><a href="#cb4-17" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span>method<span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-18"><a href="#cb4-18" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>service<span class="op">-</span>endpoint<span class="op">-</span><span class="kw">interface</span><span class="op">-</span>mapping<span class="op">&gt;</span></span>
<span id="cb4-19"><a href="#cb4-19" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>java<span class="op">-</span>wsdl<span class="op">-</span>mapping<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p>Link example:</p>
<p><img
src="http://doc.castsoftware.com/plugins/servlet/confluence/placeholder/unknown-attachment?locale=en_GB&amp;version=2"
data-image-src="http://doc.castsoftware.com/plugins/servlet/confluence/placeholder/unknown-attachment?locale=en_GB&amp;version=2" /></p>
<div>
<div>
Note: <strong></strong> "Prototype" links are never escalated.
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>ACCESS</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
These links correspond to inter-bean references. These references are
made via a logical name (declared in the ejb-jar.xml file and that
specifies the target ejb) used in the bean client code. The tags
containing this logical name are &lt;ejb-ref&gt; or
&lt;ejb-local-ref&gt;.
<ul>
<li>EJB client -- Access --&gt; EJB target</li>
</ul>
<p>For example in this standard ejb-jar.xml file:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb5"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>entity<span class="op">&gt;</span> <span class="co">//ou session ou message-driven</span></span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span> AddressBean<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;</span>ejb<span class="op">-</span>ref<span class="op">&gt;</span> <span class="co">//ou ejb-local-ref</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>ejb<span class="op">-</span>ref<span class="op">-</span>name<span class="op">&gt;</span>ejb<span class="op">/</span>CustomerRef<span class="op">&lt;/</span>ejb<span class="op">-</span>ref<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a>      <span class="kw">...</span></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a>      <span class="op">&lt;</span>ejb<span class="op">-</span>link<span class="op">&gt;</span>CustomerBean<span class="op">&lt;/</span>ejb<span class="op">-</span>link<span class="op">&gt;</span></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a>   <span class="op">&lt;/</span>ejb<span class="op">-</span>ref<span class="op">&gt;</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>entity<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p>Each bean specifies the list of other beans it accesses as well as
the references used to do so.</p>
</div></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>JOIN</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
Traced between entity beans based on the abstract schema defined in
deployment descriptor (beneath the
&lt;relationships&gt;...&lt;/relationships&gt; tag).
<p>An entity bean (source EJB) is linked to another entity bean
(destination EJB) only if navigation is possible from source EJB to
destination EJB, i.e the relation-role in which source EJB is involved
contains at least one cmr (container managed relationship) field
allowing access to the destination EJB.</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb6"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;</span>ejb<span class="op">-</span>relation<span class="op">&gt;</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>description<span class="op">&gt;</span>Team To Player<span class="op">&lt;/</span>description<span class="op">&gt;</span></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>ejb<span class="op">-</span>relation<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">.</span><span class="fu">teams</span><span class="op">-</span>TeamEJB<span class="op">.</span><span class="fu">players</span><span class="op">&lt;/</span>ejb<span class="op">-</span>relation<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span>TeamEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>multiplicity<span class="op">&gt;</span>many<span class="op">&lt;/</span>multiplicity<span class="op">&gt;</span></span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-8"><a href="#cb6-8" aria-hidden="true" tabindex="-1"></a>         <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>TeamEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-9"><a href="#cb6-9" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-10"><a href="#cb6-10" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-11"><a href="#cb6-11" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span>players<span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-12"><a href="#cb6-12" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">util</span><span class="op">.</span><span class="fu">Collection</span><span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span></span>
<span id="cb6-13"><a href="#cb6-13" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-14"><a href="#cb6-14" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-15"><a href="#cb6-15" aria-hidden="true" tabindex="-1"></a> </span>
<span id="cb6-16"><a href="#cb6-16" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-17"><a href="#cb6-17" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-18"><a href="#cb6-18" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>multiplicity<span class="op">&gt;</span>many<span class="op">&lt;/</span>multiplicity<span class="op">&gt;</span></span>
<span id="cb6-19"><a href="#cb6-19" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-20"><a href="#cb6-20" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>ejb<span class="op">-</span>name<span class="op">&gt;</span>PlayerEJB<span class="op">&lt;/</span>ejb<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-21"><a href="#cb6-21" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>relationship<span class="op">-</span>role<span class="op">-</span>source<span class="op">&gt;</span></span>
<span id="cb6-22"><a href="#cb6-22" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-23"><a href="#cb6-23" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span>teams<span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>name<span class="op">&gt;</span></span>
<span id="cb6-24"><a href="#cb6-24" aria-hidden="true" tabindex="-1"></a>        <span class="op">&lt;</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span>java<span class="op">.</span><span class="fu">util</span><span class="op">.</span><span class="fu">Collection</span><span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">-</span>type<span class="op">&gt;</span></span>
<span id="cb6-25"><a href="#cb6-25" aria-hidden="true" tabindex="-1"></a>     <span class="op">&lt;/</span>cmr<span class="op">-</span>field<span class="op">&gt;</span></span>
<span id="cb6-26"><a href="#cb6-26" aria-hidden="true" tabindex="-1"></a>  <span class="op">&lt;/</span>ejb<span class="op">-</span>relationship<span class="op">-</span>role<span class="op">&gt;</span></span>
<span id="cb6-27"><a href="#cb6-27" aria-hidden="true" tabindex="-1"></a><span class="op">&lt;/</span>ejb<span class="op">-</span>relation<span class="op">&gt;</span></span></code></pre></div>
</div>
</div>
<p>Considering the example above, the source EJB
<strong>TeamEJB</strong> will be linked to the destination EJB
<strong>PlayerEJB</strong> because the cmr field
<strong>players</strong> makes navigation possible from source to
destination EJB.</p>
<p>For the same reasons, <strong>PlayerEJB</strong> (now source EJB)
will have <strong>Refer link</strong> to <strong>TeamEJB</strong>
(destination EJB for this link).</p>
<p>For each Join link, the information below is retrieved from the
deployment descriptor and accessible in HTML Report:</p>
<ul>
<li><p><strong>Relation Name</strong></p>
<p>The name of the EJB relation as found in the
&lt;ejb-relation-name&gt; tag of the deployment descriptor
(<strong>PlayerEJB.teams-TeamEJB.players</strong> in the example). If no
name is provided, EJB Assistant computes a name for the relation, based
on the names of the two beans involved. For instance, the computed name
would be <strong>TeamEJB-PlayerEJB</strong> if
<strong>PlayerEJB.teams-TeamEJB.players</strong> was missing in the
example.</p></li>
<li><p><strong>Relation Description</strong></p>
<p>The relation description as specified in the descriptor. No default
value for this attribute.</p></li>
<li><p><strong>Multiplicity</strong></p>
<p>One of the string <em>Many</em>, <em>One</em> or <em>N/S</em> if
relation role multiplicity is not specified in the descriptor.</p></li>
</ul>
</div></td>
</tr>
</tbody>
</table>

  

How are references traced?

The names of underlying tables for each entity bean are retrieved from
descriptors containing persistence information (that map beans to
database tables or other devices). These names are then compared to the
selected  server objects' names. If the names match each other then a
link is created  between the entity bean and the server objects
(tables).

For bean class and/or interface methods, links with server objects are
traced based on a string search of the entity bean's Abstract schema
name from the method's EJB-QL statement in the ejb-jar.xml file. Once
the abstract schema name is found, the methods are then linked to the
underlying tables of the entity bean.

The examples below give an overview of the mapping process:

-   Common file (ejb-jar.xml)

``` xml
 <entity id="ContainerManagedEntity_1">>
       <ejb-name>PlayerEJB</ejb-name>
    ...
       <abstract-schema-name>Player</abstract-schema-name>
    ...
    ...
       <query>
         <query-method>
           <method-name>ejbSelectLeagues</method-name>
           <method-params>
             <method-param>team.LocalPlayer</method-param>
           </method-params>
         </query-method>
         <ejb-ql> select distinct t.league from Player p, in (p.teams) as t where p = ?1 </ejb-ql>
       </query>
    ...
```

-   Weblogic application server (weblogic-cmp20-rdbms-jar.xml)

``` xml
 <weblogic-rdbms-bean>
     <ejb-name>PlayerEJB</ejb-name>
    ...
     <table-map>
       <table-name>localplayers</table-name>
       <field-map>
         <cmp-field>name</cmp-field>
         <dbms-column>name</dbms-column>
       </field-map>
    ...
    ...
```

-   Borland enterprise server (ejb-borland.xml)

``` xml
<ejb-jar>
     <enterprise-beans>
         <entity>
             <ejb-name>PlayerEJB</ejb-name>
             <bean-home-name>PlayerEJBRemote</bean-home-name>
             <cmp2-info>
                 <cmp-field>
                     <field-name>playerId</field-name>
                 </cmp-field>
        ...
                 <table-name>localplayers</table-name>
             </cmp2-info>
         </entity>
     </enterprise-beans>
    ...
    ...
```

-   Websphere application server (Map.mapxmi)

``` xml
<ejbrdbmapping:EjbRdbDocumentRoot xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI"
 xmlns:ejbrdbmapping="ejbrdbmapping.xmi" xmlns:Mapping="Mapping.xmi" xmlns:ejb="ejb.xmi"
 xmlns:RDBSchema="RDBSchema.xmi" xmlns:java="java.xmi" xmi:id="EjbRdbDocumentRoot_1"
 outputReadOnly="false" topToBottom="true">
    ...
   <nested xsi:type="ejbrdbmapping:RDBEjbMapper" xmi:id="RDBEjbMapper_1">
    ...
     <inputs xsi:type="ejb:ContainerManagedEntity" href="META-INF/ejb-jar.xml#ContainerManagedEntity_1"/>
     <outputs xsi:type="RDBSchema:RDBTable" href="META-INF/Schema/Schema.dbxmi#Player"/>
```

-   Websphere application server (Schema.dbxmi)

``` xml
<xmi:XMI xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:RDBSchema="RDBSchema.xmi">
    ...
   <RDBSchema:RDBTable xmi:id="Player" name="localplayers" primaryKey="playerId" database="MyDB_ID">
     <namedGroup xsi:type="RDBSchema:SQLReference" xmi:id="SQLReference_1" members="RDBColumn_1"
     table="Player" constraint="Constraint_HistoryPK"/>
```

  

In the ejb-jar.xml file of the above Webspheres example, there is an
entity bean, PlayerEJB, whose abstract schema name is Player
that has a method called ejbSelectLeagues. This method references
the abstract schema name (Player) of PlayerEJB in its EJB-QL
statement.

Every server specific deployment descriptor (listed above) maps
PlayerEJB to the table localplayers. For websphere format: In
ejb-jar.xml, PlayerEJB is given the Id ContainerManagedEntity_1.
The Map.mpaxmi file maps ContainerManagedEntity_1 to the Id
Player and this Id is mapped in Schema.dbxmi to the table
localPlayers.

From this information, the following links are created:

-   PlayerEJB ==\> localplayers (the server object has a name
    matching the string "localplayers". If none, no link will be
    created).
-   ejbSelectLeagues ==\> localplayers (this is done via PlayerEJB
    and its abstract schema name).

Example below shows how the mapping (bean-table) process is carried out
when a Weblogic Application server delegates persistence management to
TopLink:

-   weblogic-ejb-jar.xml

``` xml
 <weblogic-ejb-jar>
    <weblogic-enterprise-bean>
        <ejb-name>Employee</ejb-name>
        <entity-descriptor>
            <persistence>
                <persistence-use>
                    <type-identifier>TopLink_CMP_2_0</type-identifier>
                    <type-version>4.5</type-version>
                    <type-storage>META-INF/toplink-ejb-jar.xml</type-storage>
                </persistence-use>
            </persistence>
        </entity-descriptor>
        ...
    </weblogic-enterprise-bean>
    ...
 </weblogic-ejb-jar>
```

-   META-INF/toplink-ejb-jar.xml (referenced above)

``` xml
<toplink-ejb-jar>
   <session>
       <name>ejb20_EmployeeDemo</name>
       <project-xml>Employee.xml</project-xml>
       ...
   </session>
 </toplink-ejb-jar>
```

-   Employee.xml (referenced in META-INF/toplink-ejb-jar.xml)

``` xml
<project>
    <project-name>Employee</project-name>
    ...
    <descriptors>
        <descriptor>
            <java-class>examples.ejb.cmp20.advanced.EmployeeBean</java-class>
            <tables>
                <table>Employee</table>
            </tables>
            ...
        </descriptor>
    </descriptors>
</project>
```

  

-   For the \<type-identifier\> tag in weblogic-ejb-jar.xml file, only
    identifiers TopLink_CMP and  WebLogic_CMP_RDBMS are recognized.
-   From toplink-ejb-jar.xml file, \<session\> tags are scanned until we
    meet the project file (considering bean class fully qualified name)
    that provide persistent information for the given bean (Employee in
    the example above) .
-   If the project file path(Employee.xml) is not a full path, then it
    is supposed to be relative to the module directory (in general, this
    directory corresponds to the one containing the META-INF folder).
-   Only references to server objects that have been previously
    registered in the Analysis Service can be traced. When a reference
    to an unregistered object is detected, CAST will first try
    to register the object. If it fails, a warning message is issued and
    the reference will not be saved.
