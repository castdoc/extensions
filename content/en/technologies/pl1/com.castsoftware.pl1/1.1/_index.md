---
title: "PL/1 - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.pl1

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for applications written using
Enterprise PL/I for z/OS languages.

{{% alert color="info" %}}Although this extension is officially supported by CAST, please note that it has been developed within the technical constraints of the CAST Universal Analyzer technology and to some extent adapted to meet specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other analyzers.{{% /alert %}}

## Supported Versions of PL/I

| Enterprise PL/I for z/OS | Supported |
|---|:-:|
| 5.x | :white_check_mark: |
| 4.x | :white_check_mark: |
| 3.x | :white_check_mark: |

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed when you deliver PL/1 code.

## Prepare and deliver the source code

The following file extensions are recognised by CAST Console:

- `.PLC`
- `.PLI`

However, PL/I source code files can sometimes be created with any
extension (most commonly a `.TXT` extension) or no extension at all.
If these type of files are encountered during the source code delivery
process, CAST will try to determine whether the file is a valid PL/I
source file and if so, a built-in preprocessor will change the extension
to `.PLI` or `.PLC` (for files with extensions other than .PLI or .PLC, such
as .TXT) and will add a `.PLI` or `.PLC` extension for files with no
extension.

Please note the following recommendations and notes about source code:

- It is important to organize the PL/1 source code in a specific way otherwise the preprocessor will not be launched during the delivery:
  - In general, CAST recommends ensuring that all PL/I source code (`.PLC` / `.PLI` / `.TXT` / files with no extension) is located in one single folder dedicated to PL/1 in the delivery. You can also use multiple folders for your PL/1 source code if necessary. Other source code for other technologies should be located in separate dedicated folders.
    - to trigger the PL/1 preprocessor, the presence of at least one `.PLC` or `.PLI` file in a folder in the delivery (including the root if this is used) is required
    - Do not mix other file types with the PL/1 source code in the same folder (including the root if this used) - these other files may be processed by the PL/1 preprocessor rendering them corrupt and causing the subsequent analysis to fail.
- The PL/I extension ignores empty files and they will not be considered as valid source code files.
- Binary files should be avoided since it is not always possible to detect whether a given file is binary or not. Presenting binary files in the source code delivery will not result in a packaging failure, but an error message can be expected in the packaging/extraction log.

## Source code discovery

A discoverer is provided with the extension to automatically detect PL/1
code: a PL/1 project will be discovered for the package's root folder
when at least one  .PLC or .PLI file in a folder (including post
preprocessing) is detected in the root folder or any sub-folders. For
every PL/1 project located, one Universal Technology Analysis
Unit will be created:

![](../images/668336746.jpg)

## Deliver the source code

-   Upload the ZIP file containing your Application source (or define a
    folder in which to deposit your source code)
-   Configure any file/folder exclusions and objectives
-   If you want to inspect the version (recommended), disable the
    analysis/snapshot at this point
-   Start the delivery process - the extension will be auto-downloaded
    during this phase.
-   Validate and accept the version - inspect the delivered files and
    ensure that you have an Analysis Unit for your PL/I source code:

![](../images/668336747.jpg)![](../images/668336748.jpg)

## Logging mechanism

### Analysis log files

Analysis logs are stored in the default locations used by the CAST
Management Studio/AIP Console:

<table class="relative-table wrapped confluenceTable"
style="width: 100.0%;">
<colgroup>
<col style="width: 30%" />
<col style="width: 69%" />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh"><p>Error/Warning Message</p></th>
<th class="confluenceTh"><p>Action</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd">Unable to find the object end for type
'XXXXXXX' </td>
<td class="confluenceTd"><p>This is caused by complex source code that
makes the engine miss the end of the object, whereas the begin of object
type was captured. Object won't be created in Analysis Service, will
lead to incomplete analysis. Issue must be reported to CAST Support,
along with source code (original source file + preprocessed source code)
to allow reproduction and investigation.</p>
<p>A few such warnings for object types like PLIProcedure, PLCProcedure,
PLIFunction could be acceptable.</p>
<p>Same warning for PLIMainProcedure is problematic : the whole program
is not saved in Analysis Service.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">An unnamed object of type 'PLIWhenCall' has
been detected</td>
<td class="confluenceTd">This is because PLIWhenCall has no specific
name, as such, the CAST framework will give them the name "unnamed". You
can safely ignore this warning.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><ul>
<li>Duplicate object of type 'PLIWhenCall' has been detected:
'&lt;unnamed&gt;'</li>
<li>Duplicate object of type 'PLIErrorBlock' has been detected</li>
</ul></td>
<td class="confluenceTd"><p>As mentioned above, PLIWhenCall and
PLIErrorBlock have no specific name and are automatically given the name
"unnamed" by CAST - as such multiple objects with the name "unnamed"
will exist and will cause this error. You can safely ignore this
warning.</p>
<p>Same for PLIProcSubscriptedVar object type.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><ul>
<li>Duplicate object of type 'PLIProcedure' has been detected</li>
<li>Duplicate object of type 'PLCProcedure' has been detected</li>
</ul></td>
<td class="confluenceTd"><p>PLIProcedure and PLCProcedure are supposed
to be unique within the same source file. So such warning is the sign of
an incorrect object type identification.</p>
<p>Issue must be reported to CAST Support.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><ul>
<li>end of string ''' not found</li>
<li>end of string '[']' not found</li>
<li>File skipped</li>
</ul></td>
<td class="confluenceTd"><p>UA engine did not find the end of a string,
whereas the begin of the string was captured.</p>
<p>This generally leads to a second warning : File skipped, meaning the
source file and contained objects have not been saved in Analysis
Service.</p>
<p>On large applications, a few such warnings could be acceptable.</p>
<p>Otherwise, issue must be reported to CAST Support, along with source
code (original source file + preprocessed source code) to allow
reproduction and investigation.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">end of comment '[\*][/]' not found</td>
<td class="confluenceTd"><p>Alone, this warning can be safely
ignored.</p>
<p>Check if other warnings are present for the same source
file.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><ul>
<li>Could not calculate code depth correctly for file File: &lt;file
path&gt;</li>
<li>Could not calculate code depth correctly for file, added &lt;TAG&gt;
File: &lt;file path&gt; where TAG can be $END or $$END.</li>
</ul></td>
<td class="confluenceTd">The analyzer is not capable of detecting the
source code depth correctly.</td>
</tr>
<tr class="even">
<td class="confluenceTd">PL1 Extractor Version: &lt;version&gt;</td>
<td class="confluenceTd">A simple information message to help identify
the version of the extractor that is being used.</td>
</tr>
</tbody>
</table>

### PL/I Preprocessor

PL/I source code needs to be preprocessed so that CAST can understand it
and analyze it correctly. This source code preprocessing is actioned
automatically. In other words you only need to package, deliver and
launch an analysis/generate a snapshot for the preprocessing to be
completed.

Note that as part of the source code pre-processing phase, from PL1
1.0.5 onwards, CAST will now add "$$" to all PL/I source code just
prior to the ";". For example:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Original source code</th>
<td class="confluenceTd"><div class="content-wrapper">
<div class="table-wrap">
<table class="wrapped confluenceTable" data-mce-resize="false">
<tbody>
<tr class="odd">
<td class="confluenceTd"><p><code
class="sourceCode java">someproc<span class="op">:</span>proc<span class="op">;</span></code></p></td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">Source code after pre-processing
in ≥ 1.0.5</th>
<td class="confluenceTd"><div class="content-wrapper">
<div class="table-wrap">
<table class="wrapped confluenceTable" data-mce-resize="false">
<tbody>
<tr class="odd">
<td class="confluenceTd"><p><code
class="sourceCode java">someproc<span class="op">:</span>proc$$<span class="op">;</span></code></p></td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
</tbody>
</table>

The impact of this change is as follows:

-   When upgrading to PL1 ≥ 1.0.5, existing objects will be shown
    as updated when a post upgrade snapshot is run.
-   There will be a reduction in the number of messages of the type
    "end of object of type \<PLC\PLIProcedure\|Function\> not found"
    that were previously displayed in the log.

This change has been implemented because of a limitation in the analyzer
with regard to the way object start and end patterns are handled. In
PL/I, object start and end patterns do not match (contrary to most other
languages, such as PHP where { and } are used) and therefore the
analyzer is not able to correctly identify when an object ends.

### Analysis failures

The PL1 extension needs to process all the file extensions delivered in
the version because PL/I files can have any extension. During this
parsing if the file is from a different technology, a failure will
occur. The file that has caused the failure will be stored under a
"ParsingFailures" folder at the package level and the file extension
of the file will be changed to "pfail". A reason for the failure will
also be recorded in each subfolder with name of the exception that
caused it. For example StringCommentProcessorException:

![](../images/668336755.jpg)

## What results can you expect?

### Objects

#### PL/I

| Icon | Metamodel description |
|---|---|
| ![](../images/668336756.png) | PLI DB2 Table |
| ![](../images/668336757.png) | PLI Error Block |
| ![](../images/668336758.png) | PLI FileStructure |
| ![](../images/668336759.png) | PLI Function |
| ![](../images/668336760.png) | PLI MainProcedure |
| ![](../images/668336761.png) | PLI Procedure  |
| ![](../images/668336762.png) | PLI Project |

#### PLC

| Icon | Metamodel description |
|---|---|
| ![](../images/668336763.png) | PLC DB2 Table |
| ![](../images/668336764.png) | PLC FileStructure |
| ![](../images/668336765.png) | PLC Function |
| ![](../images/668336766.png) | PLC Include |
| ![](../images/668336767.png) | PLC Procedure |
| ![](../images/668336768.png) | PLC Project |

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---------|------|
| 1.1.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.7-funcrel) |
| 1.1.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.6-funcrel) |
| 1.1.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.5-funcrel) |
| 1.1.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.4-funcrel) |
| 1.1.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.3-funcrel) |
| 1.1.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.2-funcrel) |
| 1.1.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.1-funcrel) |
| 1.1.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=\|\|1.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_pl1&ref=%7C%7C1.1.0-funcrel) |

You can also find a global list
here: [https://technologies.castsoftware.com/rules?sec=t_1004000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1004000&ref=%7C%7C).

## Known Limitations

### PL/1 integration

-   PL/I integration with Database is supported.
-   PL/I integration with IMS DB, IMS DC, CICS is not supported.

### Rules searching for the use of the builtin DATE function

The following two rules do not work as expected. Instead of searching
for use of the DATE builtin function, they search for inclusion of the
function. This is a limitation. These rules are disabled from
1.1.0-funcrel "out of the box". If they are enabled, it will list all
the files that include DATE function through "declare DATE BUILTIN;"
statement rather than actual use of function DATE.

-   Avoid Main procedures using the DATE builtin function (PL1) -
    1001164

-   Avoid Main procedures using the DATE builtin Function via include
    PLC (PL1) - 1001166

### Metrics Assistant (embedded in CAST Imaging Core)

#### Searches not limited only to embedded SQL

The MA (Metric Assistant) which is used for metric search cannot search
only in embedded SQL. In order to overcome this limitation the
processing for some metrics/rules has been moved to the extractor in
1.1.0-funcrel. However, the following rules still can be affected by
this limitation and may produce false violations:

-   Avoid Main Procedures having queries with joins on more than 4
    Tables (PL1) - 1001128
-   Avoid Main Procedures with High Raw SQL Complexity (SQL complexity
    greater than X) (PL1) - 1001130

#### Cannot calculate metric excluding comments

The MA (Metric Assistant) which is used for metric search cannot search
correctly while excluding comments especially if comments start or end
adjacent to the keyword. If such a condition exists, random false
violations may occur.
