---
title: "PHP - 3.2"
linkTitle: "3.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.php

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications written using the PHP language.

{{% alert color="info" %}}Although this extension is officially supported by CAST, please note that it has been developed within the technical constraints of the CAST Universal Analyzer technology and to some extent adapted to meet specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other analyzers.{{% /alert %}}

## Supported versions

This version of the extension provides support for:

| Technology | Supported | Notes |
|------------|:---------:|-------|
| PHP 5.x           | :white_check_mark: | -                         |
| Symfony Framework | :white_check_mark: | Doctrine is NOT supported. |

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points<br> (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST
Imaging Console when you deliver PHP code. You can also manually
install the extension using the Application -
Extensions interface.  Once the extension is downloaded and installed,
you can now package your source code and run an analysis.

## Prepare and deliver the source code

### Source code preparation

- Only files with following extensions will be analyzed `*.php`; `*.php4`; `*.php5`; `*.php6`; `*.inc`; `*.phtml`. The `*.yml` and `*.yaml` extensions are also supported for Symfony framework.
- The analysis of `.xml` and `.xsl` files contained in the PHP application is not supported. 
- The analysis of any HTML and JavaScript source code delivered with the PHP code is managed by the HTML and JavaScript extension / .NET analyzer, to be configured in addition to the PHP analysis.

### Source code preprocessing

PHP source code needs to be preprocessed so that CAST can understand it
and analyze it correctly. This code preprocessing is actioned
automatically when an analysis is launched or a snapshot is generated
(the code is preprocessed before the analysis starts). In other words
you only need to package, deliver and launch an analysis/generate a
snapshot for the preprocessing to be completed. The PHP Preprocessor log
file is stored in the following location:

``` java
%PROGRAMDATA%\CAST\CAST\Logs\<application_name>\Execute_Analysis_<guid>\com.castsoftware.php.<_extension_version>.prepro_YYYYMMDDHHMMSS.log
```

{{% alert color="info" %}}Note that the LISA folder will be used to analyze the preprocessed files - this is in contrast to standard behaviour for most other analyzers, where source code is analyzed directly from the Delivery folder. Due to this, if you use [User Defined Modules based on subfolders](../../../../interface/analysis-config/config/modules/udm/#module-per-subfolder) then you will need to ensure that you configure your paths based on the LISA folder.{{% /alert %}}

#### Short tags

PHP short tags `\<?` and `\<?=` in the delivered source code cannot be
handled as is. Therefore, the analyzer will automatically convert them to
`\<? /*php short tag*/` and `\<? /*php short echo tag*/` respectively.

## What results can you expect?

### PHP Objects

| Icon | Metamodel Name |
|---|---|
| ![](../images/668336723.png) | PHP Array |
| ![](../images/668336722.png) | PHP Class |
| ![](../images/668336721.png) | PHP Class Constant |
| ![](../images/668336720.png) | PHP Constructor |
| ![](../images/668336719.png) | PHP Define |
| ![](../images/668336718.png) | PHP Function |
| ![](../images/668336717.png) | PHP Interface |
| ![](../images/668336716.png) | PHP Member |
| ![](../images/668336715.png) | PHP Method |
| ![](../images/668336714.png) | PHP Section |
| ![](../images/668336713.png) | Script Function |
| ![](../images/668336712.png) | Script Section |

### Symfony Framework objects

| Icon | Metamodel Name |
|---|---|
| ![](../images/668336711.png) | PHP Symfony Controller |
| ![](../images/668336710.png) | PHP Symfony Controller Class |
| ![](../images/668336709.png) | PHP Symfony Route |
| ![](../images/668336708.png) | PHP Symfony Service |

All Symfony objects will appear under their respective folders as shown below :

![](../images/668336707.png)

#### PHP Symfony Controller Class

- Supported scenario: If the Class name ends with Controller, we will create PHP Symfony Controller Class objects
- Links:
  - PHP Symfony Controller Class --- Refer Link ---\> PHP Class
- Limitations: Alternate syntax where you can give the class name that does not have suffix "Controller" is not supported

#### PHP Symfony Controller 

- Supported scenario: If the method or function ends with suffix "Action", then PHP Symfony Controller Object will be created
- Links:
  - PHP Symfony Controller --- Refer Link ---\> PHP Symfony Route
  - PHP Symfony Controller --- Refer Link ---\> PHP Method\Function

#### PHP Symfony Route

- Supported scenario:
  - If a route has been declared in the yml file, a route object will be created
    - If a route has been declared in PHP file an annotation route object will be created as follows:
        - Default naming convention for route annotation when declared without name above class `"<classname>_Class_Annotation_<number>"`
        - Default naming convention for route annotation when declared without name above method `"<methodname>_Method_Annotation_<number>"`
- Links:
  - PHP Symfony Route --- Call Link ---\> PHP Symfony Controller

#### PHP Symfony Service

- Supported scenario: If a service has been declared in the `yml` configuration files, PHP Symfony Service Object will be created
- Links:
  - PHP Symfony Service --- Call Link ---\> PHP Method
  - PHP Symfony Service --- Call Link ---\> PHP Property
  - PHP Symfony Service --- Call Link ---\> PHP Class constructor
- Limitation: Inheritance is not supported while determining property setter or constructor injection - they need to be defined in the same class which is being referred to in the service

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 3.2.0-beta3 | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.2.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.2.0-beta3) |
| 3.2.0-beta2 | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.2.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.2.0-beta2) |
| 3.2.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.2.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.2.0-beta1) |
| 3.2.0-alpha2 | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.2.0-alpha2](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.2.0-alpha2) |
| 3.2.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.2.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.2.0-alpha1) |

You can also find a global list here:

[https://technologies.castsoftware.com/rules?sec=t_1017000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1017000&ref=%7C%7C)

## Logging mechanism

### Analysis log files

Analysis logs are stored in the default locations.

### PHP Preprocessor

PHP Preprocessor log file name (the preprocessor is launched
automatically during an analysis) would be in
format com.castsoftware.php.prepro\_\<ExtensionVersion\>\_\<YYYYMMDDHHMMSS\>.log

### Errors and Warnings

The PHP configuration included in the extension uses external plugins.
During the analysis, the Universal Analyzer or the plugin can throw
errors or warnings. The table below list the most significant
errors/warnings and lists a suggested remediation action:

| Tool | Error or Warning | Action |
|------|------------------|--------|
| Analyzer & Code Sniffer | UA Plugin : No property (......) found in meta model for php... | No action required. The analyzer is telling you that not all the properties are considered to be injected into the Analysis Service. |

## Known limitations

### Autowiring

This extension does not support PHP autowiring
(see [https://php-di.org/doc/autowiring.html](https://php-di.org/doc/autowiring.html)).

### LISA path length limited to 256 characters

If the LISA (Large Intermediate Storage Area) path for a specific file
exceeds 256 characters the following occurs:

-   violation calculation for this file will fail with message
    "\<filepath\> does not exist".
-   the analysis may fail with the error "Error while executing
    C:\php\phpmd.bat:The given file "\<filepath\>" does not exist.

This warning will appear in com.castsoftware.plugin\*.log file. This is
a limitation of PHP itself and not the PHP extension. To remediate this
issue reduce path to the LISA folder where possible.

### Name matching links - Universal Analyzer limitation

Due to a limitation in the Universal Analyzer (the "engine" used for PHP
analyses), links will be created from any name to any matching name. At
a minimum the following rules may be impacted and give erroneous results:

| ID      | Name                                                                      |
|---------|---------------------------------------------------------------------------|
| 1007004 | Avoid Methods and Functions with High Fan-In (PHP)                        |
| 1007006 | Avoid Methods and Functions with High Fan-Out (PHP)                       |
| 1007008 | Avoid JavaScript Functions with High Fan-In (PHP)                         |
| 1007010 | Avoid JavaScript Functions with High Fan-Out (PHP)                        |
| 1007168 | Avoid using function or method return value that do not have return (PHP) |
| 1007170 | Avoid function return value ignored (PHP)                                 |

### Analysis of XML and XSL files contained in the PHP application

The analysis of XML and XSL files contained in the PHP application is
not supported. Any links between these files and any other file in the
application will not be detected. This will impact the results of all
the Quality Rules using these files.

### Support of JavaScript source code

The PHP extension does not support JavaScript and as such, any
JavaScript source code located in PHP or JavaScript  files will not be
analyzed. CAST recommends using the [HTML5 and
JavaScript](HTML5_and_JavaScript) extension to analyze JavaScript files
in the source code.

### Support of PHTML files

PHTML files are supported with some limitations. If the files contain
calls to functions or methods defined in other files and these other
files are not specifically included, then these links will be lost.

### Missing objects

#### Several properties on the same line

If a php class has properties declared on the same line, only the first property will be detected. For example:

``` php
class Test {

    public $first, $second, $third;

}
```
After analysis only object for `$first` will be created.

#### Property declared on several lines

If a php class has a property declared on several lines, no corresponding object will be created. For example:
``` php
class Test {

    public
    $first;

}
```

#### Method headers declared on several lines

If a php class has a method with a signature declared on several lines, no corresponding object will be created. For example:
``` php
class Test {

    public function
    $my_function{
        /* Body of the function */
    }

}
```


### Limitations specific to rules

#### Avoid artifacts having recursive calls

"Avoid artifacts having recursive calls" (7388 - a standard CAST rule) -
in some cases, a false positive may be detected: a call to a parent
function can be detected as a recursive call

Note that an equivalent rule specific to the PHP extension (Avoid
artifacts having recursive calls (PHP) - 1007242) was added in PHP
1.2.0. This replacement rule now produces accurate results and the
results of 7388 should be ignored.

#### Avoid using break or continue statements in loops with high cyclomatic complexity

"Avoid using break or continue statements in loops with high cyclomatic
complexity" (1007176) - if the break statement is located in
JavaScript functions, no violations will be detected. JavaScript source
code located in .PHP or JavaScript files is not analyzed (see limitation
listed above).

#### Avoid unreferenced PHP Files

The rule "Avoid unreferenced PHP Files" (1007052) will return a false
positive violation when a PHP file is referenced only from other
technologies, for example from only within html/javascript source code.


## Third-party components

### PHP_CodeSniffer

This extension redistributes the source code of [PHP_CodeSniffer](https://github.com/PHPCSStandards/PHP_CodeSniffer) version 3.9.0

| Native PHPCS rule | Equivalent CAST rule |
|-------------------|----------------------|
| Generic.CodeAnalysis.ForLoopShouldBeWhileLoop.CanSimplify | 1007022 - Avoid artifacts using "for" loops which can be simplified to a "while" loop (PHP) |
| Generic.CodeAnalysis.JumbledIncrementer.Found | 1007024 - Avoid incrementer jumbling in loops (PHP) |
| Squiz.Operators.ComparisonOperatorUsage.NotAllowed | 1007026 - Use identical type operator rather than "equal" operator (PHP) |
| Squiz.Operators.IncrementDecrementUsage.Found | 1007028 - Use increment/decrement operators where possible (PHP) |
| Generic.CodeAnalysis.EmptyStatement.Detected* | 1007030 - Avoid using empty statement (PHP) |
| Generic.CodeAnalysis.UnnecessaryFinalModifier.Found | 1007056 - Avoid unnecessary final modifiers inside final Classes (PHP) |
| Generic.CodeAnalysis.UnusedFunctionParameter.* | 1007058 - Avoid artifacts with unused parameters (PHP) |
| Squiz.Commenting.ClosingDeclarationComment.Missing <br> Squiz.Commenting.ClosingDeclarationComment.Incorrect <br> Squiz.Commenting.ClosingDeclarationComment.Misplaced | 1007070 - Avoid method and functions without comments denoting scope end (PHP) |
| Squiz.Classes.ClassFileName.NoMatch | 1007080 - Avoid Class name not matching parent file name (PHP) |
| Squiz.ControlStructures.LowercaseDeclaration.FoundUppercase | 1007086 - Use lowercase for control structures (PHP) |
| Squiz.ControlStructures.ElseIfDeclaration.NotAllowed | 1007096 - Avoid "elseif" statements (PHP) |
| Generic.Strings.UnnecessaryStringConcat.Found | 1007114 - Avoid unnecessary string concatenations (PHP) |
| Squiz.Commenting.EmptyCatchComment.Missing | 1007122 - Avoid empty catch blocks (PHP) |
| Squiz.Commenting.FunctionCommentThrowTag.Missing | 1007124  - Avoid artifacts throwing exceptions without @Throws tag in doc comments (PHP) |
| Generic.CodeAnalysis.UnconditionalIfStatement.Found | 1007146 - Avoid unconditional "if" and "else if\elseif" statements (PHP) |
| Generic.CodeAnalysis.UselessOverridingMethod.Found | 1007148 - Avoid methods and constructor overridden only with parent artifact call (PHP) |
| PEAR.Functions.ValidDefaultValue.NotAtEnd | 1007150 - Avoid artifacts with optional parameters before required parameters (PHP) |
| Generic.PHP.NoSilencedErrors.Forbidden | 1007206 - Avoid using @error suppression (PHP) |
| Zend.NamingConventions.ValidVariableName.NotCamelCaps <br> Zend.NamingConventions.ValidVariableName.PublicHasUnderscore <br> Zend.NamingConventions.ValidVariableName.PrivateNoUnderscore <br> Zend.NamingConventions.ValidVariableName.MemberVarNotCamelCaps <br> Zend.NamingConventions.ValidVariableName.StringVarNotCamelCaps | 1007212 - Avoid having variables without naming conventions (PHP) |
| PSR2.ControlStructures.ControlStructureSpacing.SpacingAfterOpenBrace <br> PSR2.ControlStructures.ControlStructureSpacing.SpaceBeforeCloseBrace | 1007228 - Avoid control structures without proper spacing before and after open\close braces - PSR2 (PHP) |
| PSR2.ControlStructures.SwitchDeclaration.* | 1007230 - Avoid artifacts without proper switch case declarations (PSR2) (PHP) |
| Generic.Functions.CallTimePassByReference.NotAllowed | 1007232 - Avoid artifacts with variables passed by reference when calling a function (PHP) |
| Generic.ControlStructures.InlineControlStructure.NotAllowed | 1007234 - Avoid having inline control statements (PHP) |
| Squiz.Scope.MethodScope.Missing | 1007236 - Avoid having Class Methods or Constructor without scope modifiers - Symfony STD (PHP) |
| PSR1.Files.SideEffects.FoundWithSymbols | 1007254 - Avoid files that declare both symbols and execute logic with side effects (PHP)|

| Custom PHPCS rule | Equivalent CAST rule |
|-------------------|----------------------|
| CAST.HTML.EmptyInlineStyleDefinition.StyleDefinitionFound | 1007012 - Avoid using embedded CSS in Web Pages (PHP) |
| CAST.HTML.EmptyInlineStyleDefinition.EmptyStyleDefinitionFound | 1007034 - Avoid empty style definition (PHP) |
| CAST.OOP.Classes.FoundImplementsInterface | 1007042 - Avoid Classes implementing excessive number of Interfaces (PHP) |
| CAST.CodeAnalysis.ForLoopWithTestFunctionCall.NotAllowed | 1007050 - Avoid method invocation in loop termination expression (PHP) |
| CAST.Loops.LoopBasedRules.ObjectInstantiationInLoop | 1007116 - Avoid artifacts with Object Instantiation in loops (PHP) |
| CAST.SQL.EmbeddedSQL.MoreThan4TablesInQuery | 1007118 - Avoid artifacts with sql statements referring more than 4 Tables (PHP) |
| CAST.SQL.EmbeddedSQL.GroupByFound | 1007120 - Avoid artifacts with Group By sql statement (PHP) |
| CAST.Keywords.Goto.goto | 1007154 - Avoid using goto statements (PHP) |
| CAST.Keywords.Eval.eval | 1007156  - Avoid using eval expressions (PHP) |
| CAST.Keywords.ExitOrDie.exitordie | 1007158 - Avoid artifacts using exit and die expressions (PHP) |
| CAST.Constructor.ConstructorReturningValue.ConstructorReturningValue | 1007172 - Avoid having constructors with return value (PHP) |
| CAST.Loops.LoopBasedRules.BreakOrContinueInHighCyclomaticComplexityLoop | 1007176 - Avoid using break or continue statements in loops with high cyclomatic complexity (PHP) |
| CAST.Files.IncludingFile.FileInclusionRecommondation | 1007180 - Use file inclusion based on API suitability (PHP) |
| CAST.Loops.LoopBasedRules.SizeFunctionInLoopFound | 1007184  - Avoid using size functions inside loops (PHP) |
| CAST.Files.IncludingFile.AvoidFileInclusionUsingRelativePaths | 1007190 - Avoid using relative path for including files (PHP) |
| CAST.PHP.DisallowShortOpenTag | 1007192 - Avoid using PHP short tags (PHP) |
| CAST.Security.SuperGlobals.SuperGlobalAccessed | 1007202 - Avoid direct access to superglobals (PHP) |
| CAST.Functions.DicouragedFunctionCalls.FoundtimeCall | 1007210 - Avoid using time() to get the system time (PHP) |
| CAST.Database.PreferFetchingAssocOverRow.FoundArrayAccessToDatabaseData | 1007218 - Avoid fetching database rows as array and accessing using subscript (PHP) |
| CAST.SQL.EmbeddedSQL.SelectStarFound | 1007220 - Avoid artifacts with "select *" Sql statement (PHP) |
| CAST.BadFunctions.InsufficientSSLTLSConfig.InsufficientSSLTLSConfig | 1007248 - Use sufficient SSL\TLS context (PHP) |

### phpcs-security-audit

This extension redistributes the source code of [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit) version 2.0.1

| PHPCS rule | Equivalent CAST rule |
|------------|----------------------|
| PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSerr | 1007244 - Avoid cross-site scripting - Improper Neutralization of script-related HTML tags in a web page (PHP) |
| PHPCS_SecurityAudit.BadFunctions.EasyRFI.ErrEasyRFI | 1007246 - Avoid Remote File Inclusion - Improper Control of Filename for Include/Require Statement in PHP Program (PHP) |
| PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceE <br> PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceUserInputE <br> PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceUserInput | 1007250 - Avoid preg_replace with /e option (PHP) |
| PHPCS_SecurityAudit.BadFunctions.FilesystemFunctions.ErrFilesystem | 1007252 - Avoid filesystem function calls without sanitizing user input (PHP)|

### symfony-coding-standard

This extension redistributes the source code of [symfony-coding-standard](https://github.com/djoos/symfony-coding-standard) version 3.13.0

| Symfony PHPCS rule | Equivalent CAST rule |
|--------------------|----------------------|
| Symfony.Classes.MultipleClassesOneFile | 1007238 - Avoid having Class Methods or Constructor without scope modifiers - Symfony STD (PHP)  |
| Symfony.Objects.ObjectInstantiation.Invalid | 1007240 - Avoid artifacts having object instantiation without parenthesis - Symfony STD (PHP) |

### PHP Mess Detector

This extension redistributes the binary of [PHP Mess Detector](https://phpmd.org/) version 2.14.1

| PHPMD rule | Equivalent CAST rule |
|------------|----------------------|
| NumberOfChildren | 1007036 - Avoid classes having excessive number of derived classes (PHP) |
| CouplingBetweenObjects | 1007038 - Avoid classes having excessive number of dependencies (PHP) |
| DepthOfInheritance | 1007046 - Avoid Classes with High Depth of Inheritance Tree (PHP) |
| ShortVariable | 1007088 - Avoid having variable with too short name (PHP) |
| LongVariable | 1007090 - Avoid having variable with too long name (PHP) |
| ExcessiveClassLength | 1007126 - Avoid classes with excessive lines of code (PHP) |
| ExcessiveParameterList | 1007128 - Avoid artifacts with excessive number of parameters (PHP) |
| ExcessiveMethodLength | 1007130 - Avoid Artifacts with excessive lines of code (PHP) |
| TooManyFields | 1007132 - Avoid classes with excessive number of fields (PHP) |
| TooManyMethods | 1007134 - Avoid classes with excessive number of methods (PHP) |
| ExcessivePublicCount | 1007136 - Avoid classes having with excessive number of public methods and fields (PHP) |
| UnusedLocalVariable | 1007138 - Avoid unused local variables (PHP) |
| UnusedPrivateField | 1007140 - Avoid unused private fields (PHP) |
| UnusedPrivateMethod | 1007142 - Avoid unused private methods (PHP) |
| ExcessiveClassComplexity | 1007144 - Avoid classes with excessive number of weighted methods (PHP) |
