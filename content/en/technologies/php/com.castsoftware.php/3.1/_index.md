---
title: "PHP - 3.1"
linkTitle: "3.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.php

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for applications written using the PHP language.

{{% alert color="info" %}}Although this extension is officially supported by CAST, please note that it has been developed within the technical constraints of the CAST Universal Analyzer technology and to some extent adapted to meet specific customer needs. Therefore the extension may not address all of
the coding techniques and patterns that exist for the target technology
and may not produce the same level of analysis and precision regarding
e.g. quality measurement and/or function point counts that are typically
produced by other analyzers.{{% /alert %}}

## Supported versions

| Technology | Supported | Notes |
|---|:-:|---|
| PHP 5.x | :white_check_mark: | - |
| Symfony Framework | :white_check_mark: | Doctrine is NOT supported. |

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points<br> (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed when you deliver PHP code.

### Upgrading from previous releases of the PHP Analyzer

Previous releases of the PHP Analyzer required that an instance of
PHP was installed on the Node (i.e. the machine on which the analysis
is run). This requirement has been removed in ≥ 3.1.0. The PHP
Analyzer now uses an instance of PHP embedded within the extension (v.
5.4.3). Therefore, please note the following:

-   If you have already installed a previous version of the PHP
    Analyzer on your Node and already have a functioning PHP install
    from that extension, please ensure that you uninstall PHP before
    proceeding with the instructions below. To remove the PHP
    installation provided with the PHP Analyzer:
    -   delete the folder into which it was installed (by default
        this is usually set to `C:\php`).
    -   delete the system environment variable `PHP_HOME`
-   Check that you do not have an existing third party (i.e.
    not provided by CAST) installation of PHP on this machine. If a
    third party installation of PHP already exists, please follow the
    PHP uninstall procedure for the install method that was used, before
    starting an analysis. Third party PHP installations are not
    compatible with the PHP extension.

## Prepare and deliver the source code

### Source code preparation

- Only files with following extensions will be analyzed `*.php`; `*.php4`; `*.php5`; `*.php6`; `*.inc`; `*.phtml`. The `*.yml` and `*.yaml` extensions are also supported for Symfony framework.
- The analysis of `.xml` and `.xsl` files contained in the PHP application is not supported. 
- The analysis of any HTML and JavaScript source code delivered with the PHP code is managed by the HTML and JavaScript extension / .NET analyzer, to be configured in addition to the PHP analysis.

### Source code preprocessing

PHP source code needs to be preprocessed so that CAST can understand it
and analyze it correctly. This code preprocessing is actioned
automatically when an analysis is launched (the code is preprocessed before the analysis starts). In other words you only need to package, deliver and launch an analysis for the preprocessing to be completed. The PHP Preprocessor log
file is stored in the following location:

```java
%PROGRAMDATA%\CAST\CAST\Logs\<application_name>\Execute_Analysis_<guid>\com.castsoftware.php.<_extension_version>.prepro_YYYYMMDDHHMMSS.log
```

{{% alert color="info" %}}Note that the LISA folder will be used to analyze the preprocessed files - this is in contrast to standard behaviour for most other analyzers, where source code is analyzed directly from the Delivery folder. Due to this, if you use [User Defined Modules based on subfolders](../../../../interface/analysis-config/config/modules/udm/#module-per-subfolder) then you will need to ensure that you configure your paths based on the LISA folder.{{% /alert %}}

#### Short tags

PHP short tags `\<?` and `\<?=` in the delivered source code cannot be
handled as is. Therefore, the analyzer will automatically convert them to
`\<? /*php short tag*/`.

## What results can you expect?

### PHP Objects

| Icon | Metamodel Name |
|---|---|
| ![](../images/668336723.png) | PHP Array |
| ![](../images/668336722.png) | PHP Class |
| ![](../images/668336721.png) | PHP Class Constant |
| ![](../images/668336720.png) | PHP Constructor |
| ![](../images/668336719.png) | PHP Define |
| ![](../images/668336718.png) | PHP Function |
| ![](../images/668336717.png) | PHP Interface |
| ![](../images/668336716.png) | PHP Member |
| ![](../images/668336715.png) | PHP Method |
| ![](../images/668336714.png) | PHP Section |
| ![](../images/668336713.png) | Script Function |
| ![](../images/668336712.png) | Script Section |

### Symfony Framework objects

| Icon | Metamodel Name |
|---|---|
| ![](../images/668336711.png) | PHP Symfony Controller |
| ![](../images/668336710.png) | PHP Symfony Controller Class |
| ![](../images/668336709.png) | PHP Symfony Route |
| ![](../images/668336708.png) | PHP Symfony Service |

#### PHP Symfony Controller Class

- Supported scenario: If the Class name ends with Controller, we will create PHP Symfony Controller Class objects
- Links:
  - PHP Symfony Controller Class --- Refer Link ---\> PHP Class
- Limitations: Alternate syntax where you can give the class name that does not have suffix "Controller" is not supported

#### PHP Symfony Controller 

- Supported scenario: If the method or function ends with suffix "Action", then PHP Symfony Controller Object will be created
- Links:
  - PHP Symfony Controller --- Refer Link ---\> PHP Symfony Route
  - PHP Symfony Controller --- Refer Link ---\> PHP Method\Function

#### PHP Symfony Route

- Supported scenario:
  - If a route has been declared in the yml file, a route object will be created
    - If a route has been declared in PHP file an annotation route object will be created as follows:
        - Default naming convention for route annotation when declared without name above class `"<classname>_Class_Annotation_<number>"`
        - Default naming convention for route annotation when declared without name above method `"<methodname>_Method_Annotation_<number>"`
- Links:
  - PHP Symfony Route --- Call Link ---\> PHP Symfony Controller

#### PHP Symfony Service

- Supported scenario: If a service has been declared in the `yml` configuration files, PHP Symfony Service Object will be created
- Links:
  - PHP Symfony Service --- Call Link ---\> PHP Method
  - PHP Symfony Service --- Call Link ---\> PHP Property
  - PHP Symfony Service --- Call Link ---\> PHP Class constructor
- Limitation: Inheritance is not supported while determining property setter or constructor injection - they need to be defined in the same class which is being referred to in the service

### Structural Rules

The following structural rules are provided:

| Release       |  Link   |
|---------------|---------|
| 3.1.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.1.3-funcrel) |
| 3.1.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.1.2-funcrel) |
| 3.1.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.1.1-funcrel) |
| 3.1.0-beta1   | [https://technologies.castsoftware.com/rules?sec=srs_php&ref=\|\|3.1.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_php&ref=%7C%7C3.1.0-beta1)     |

You can also find a global list here:

[https://technologies.castsoftware.com/rules?sec=t_1017000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1017000&ref=%7C%7C)

## Logging mechanism

### Analysis log files

Analysis logs are stored in the default locations.

### PHP Preprocessor

PHP Preprocessor log file name (the preprocessor is launched automatically during an analysis) will be in the format `com.castsoftware.php.prepro_<ExtensionVersion>_<YYYYMMDDHHMMSS>.log`.

### PHP Plugin

PHP Plugin, which uses PHP CodeSniffer, PDepend, PMD, log file name (the
PHP Plugin is launched automatically during an analysis) will be in the format `com.castsoftware.php.plugin_<ExtensionVersion>_<YYYYMMDDHHMMSS>.log`.

### Errors and Warnings

The PHP configuration included in the extension uses external plugins.
During the analysis, the Universal Analyzer or the plugin can throw
errors or warnings. The table below list the most significant
errors/warnings and lists a suggested remediation action:

| Tool | Error or Warning | Action |
|------|------------------|--------|
| Analyzer & Code Sniffer | UA Plugin : No property (......) found in meta model for php... | No action required. The analyzer is telling you that not all the properties are considered to be injected into the Analysis Service. |

## Known limitations

### Autowiring

This extension does not support PHP autowiring
(see [https://php-di.org/doc/autowiring.html](https://php-di.org/doc/autowiring.html)).

### LISA path length limited to 256 characters

If the LISA (Large Intermediate Storage Area) path for a specific file
exceeds 256 characters the following occurs:

-   violation calculation for this file will fail with message
    "\<filepath\> does not exist".
-   the analysis may fail with the error "Error while executing
    C:\php\phpmd.bat:The given file "\<filepath\>" does not exist.

This warning will appear in com.castsoftware.plugin\*.log file. This is
a limitation of PHP itself and not the PHP extension. To remediate this
issue reduce path to the LISA folder where possible.

### Name matching links - Universal Analyzer limitation

Due to a limitation in the Universal Analyzer (the "engine" used for PHP
analyses), links will be created from any name to any matching name. At
a minimum the following rule may be impacted and give erroneous results:

| ID      | Name                                                                      |
|---------|---------------------------------------------------------------------------|
| 1007004 | Avoid Methods and Functions with High Fan-In (PHP)                        |
| 1007006 | Avoid Methods and Functions with High Fan-Out (PHP)                       |
| 1007008 | Avoid JavaScript Functions with High Fan-In (PHP)                         |
| 1007010 | Avoid JavaScript Functions with High Fan-Out (PHP)                        |
| 1007168 | Avoid using function or method return value that do not have return (PHP) |
| 1007170 | Avoid function return value ignored (PHP)                                 |

### Analysis of XML and XSL files contained in the PHP application

The analysis of XML and XSL files contained in the PHP application is
not supported. Any links between these files and any other file in the
application will not be detected. This will impact the results of all
the Quality Rules using these files.

### Support of JavaScript source code

The PHP extension does not support JavaScript and as such, any
JavaScript source code located in PHP or JavaScript  files will not be
analyzed. CAST recommends using the [HTML5 and
JavaScript](HTML5_and_JavaScript) extension to analyze JavaScript files
in the source code.

### Support of PHTML files

PHTML files are supported with some limitations. If the files contain
calls to functions or methods defined in other files and these other
files are not specifically included, then these links will be lost.

### Missing objects

#### Section between quotes

If a PHP section is enclosed between quotes, no corresponding object will be created. For example:

```html
   <a href='<?php echo getUrl(); ?>'>Website</a>
```

#### Several properties on the same line

If a php class has properties declared on the same line, only the first property will be detected. For example:

``` php
class Test {

    public $first, $second, $third;

}
```
After analysis only object for `$first` will be created.

#### Property declared on several lines

If a php class has a property declared on several lines, no corresponding object will be created. For example:
``` php
class Test {

    public
    $first;

}
```

#### Method headers declared on several lines

If a php class has a method with a signature declared on several lines, no corresponding object will be created. For example:
``` php
class Test {

    public function
    $my_function{
        /* Body of the function */
    }

}
```


### Limitations specific to rules

#### Avoid artifacts having recursive calls

"Avoid artifacts having recursive calls" (7388 - a standard CAST rule) -
in some cases, a false positive may be detected: a call to a parent
function can be detected as a recursive call

Note that an equivalent rule specific to the PHP extension (Avoid
artifacts having recursive calls (PHP) - 1007242) was added in PHP
1.2.0. This replacement rule now produces accurate results and the
results of 7388 should be ignored.

#### Avoid using break or continue statements in loops with high cyclomatic complexity

"Avoid using break or continue statements in loops with high cyclomatic
complexity" (1007176) - if the break statement is located in
JavaScript functions, no violations will be detected. JavaScript source
code located in .PHP or JavaScript files is not analyzed (see limitation
listed above).

#### Avoid unreferenced PHP Files

The rule "Avoid unreferenced PHP Files" (1007052) will return a false
positive violation when a PHP file is referenced only from other
technologies, for example from only within html/javascript source code.

## License agreements

The PHP extension uses several third-party tools. The Licence Agreements
for these tools are listed below:



### PHP_CodeSniffer

More information about this tool is available
here: [http://pear.php.net/package/PHP_CodeSniffer](http://pear.php.net/package/PHP_CodeSniffer)

#### Version

CAST ships a fork of PHP_CodeSniffer version 2.5.0.

#### License

The licence agreement for the PHP_CodeSniffer tool is available here:

-   [https://opensource.org/licenses/BSD-2-Clause](https://opensource.org/licenses/BSD-2-Clause)

and is detailed below:

*Copyright (c) 2012, Squiz Pty Ltd (ABN 77 084 670 600)*  
*All rights reserved.*

*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:*

-   *Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.*
-   *Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the
    distribution.*
-   *Neither the name of Squiz Pty Ltd nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.*

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*

#### Quality Rules calculated by the PHP_CodeSniffer tool

| Rule name                                                                                       | ID      |
|-------------------------------------------------------------------------------------------------|---------|
| Avoid artifacts using "for" loops which can be simplified to a "while" loop (PHP)               | 1007022 |
| Avoid incrementer jumbling in loops (PHP)                                                       | 1007024 |
| Use identical type operator rather than "equal" operator (PHP)                                  | 1007026 |
| Use increment/decrement operators where possible (PHP)                                          | 1007028 |
| Avoid using empty statement (PHP)                                                               | 1007030 |
| Avoid empty class definition (PHP)                                                              | 1007032 |
| Avoid classes having excessive number of derived classes(PHP)                                   | 1007036 |
| Avoid classes having excessive number of dependencies (PHP)                                     | 1007038 |
| Avoid Classes with High Depth of Inheritance Tree (PHP)                                         | 1007046 |
| Avoid unnecessary final modifiers inside final Classes (PHP)                                    | 1007056 |
| Avoid unused parameters (PHP)                                                                   | 1007058 |
| Avoid Class name not matching parent file name (PHP)                                            | 1007080 |
| Use lowercase for control structures in Sections (PHP)                                          | 1007084 |
| Use lowercase for control structures in Methods and Functions (PHP)                             | 1007086 |
| Avoid having variable with too short name (PHP)                                                 | 1007088 |
| Avoid having variable with too long name (PHP)                                                  | 1007090 |
| Avoid "elseif" statements (PHP)                                                                 | 1007096 |
| Avoid Functions throwing exceptions and not having a @Throws tag (PHP)                          | 1007124 |
| Avoid classes exceeding maximum length (PHP)                                                    | 1007126 |
| Avoid methods having too many parameters (PHP)                                                  | 1007128 |
| Avoid Methods exceeding maximum length (PHP)                                                    | 1007130 |
| Avoid classes with too many fields (PHP)                                                        | 1007132 |
| Avoid classes with too many methods (PHP)                                                       | 1007134 |
| Avoid classes having a number of public methods and attributes exceeds maximum (PHP)            | 1007136 |
| Avoid having unused variables (PHP)                                                             | 1007138 |
| Avoid unused private fields (PHP)                                                               | 1007140 |
| Avoid unused private methods (PHP)                                                              | 1007142 |
| Avoid classes exceeding number of weighted methods (PHP)                                        | 1007144 |
| Avoid unconditional "if" and "elseif" statements (PHP)                                          | 1007146 |
| Avoid useless overriding Methods (PHP)                                                          | 1007148 |
| Avoid unassigned default values in Functions (PHP)                                              | 1007150 |
| Avoid having variables without naming conventions (PHP)                                         | 1007212 |
| Avoid having For-loops that use a function call in the test expression (PHP)                    | 1007226 |
| Avoid control structures without proper spacing before and after open\close braces - PSR2 (PHP) | 1007228 |
| Avoid Having control structures without proper switch case declarations (PSR2) (PHP)            | 1007230 |
| Avoid having variables passed by reference when calling a function (PHP)                        | 1007232 |
| Avoid having inline control statements (PHP)                                                    | 1007234 |
| Avoid having Class Methods or Constructor without scope modifiers - Symfony STD (PHP)           | 1007236 |
| Avoid having multiple classes defined in a single file - Symfony STD (PHP)                      | 1007238 |
| Avoid artifacts having object instantiation without parenthesis - Symfony STD (PHP)             | 1007240 |
| CWE-311: Use sufficient SSL\TLS context (PHP)                                                   | 1007248 |
| Avoid files that declare both symbols and execute logic with side effects (PHP)                 | 1007254 |

#### Rules using the PHP_CodeSniffer framework but implemented by CAST

| Rule name                                                                               | ID      |
|-----------------------------------------------------------------------------------------|---------|
| Avoid using embedded CSS in Web Pages (PHP)                                             | 1007012 |
| Avoid empty style definition (PHP)                                                      | 1007034 |
| Avoid artifacts with Object Instantiation in loops (PHP)                                | 1007116 |
| CWE-624: Avoid using eval expressions (PHP)                                             | 1007156 |
| Avoid artifacts using exit and die expressions (PHP)                                    | 1007158 |
| Avoid using variable without testing them for initialization (PHP)                      | 1007160 |
| Avoid having constructors with a return value (PHP)                                     | 1007172 |
| Avoid using break or continue statements in loops with high cyclomatic complexity (PHP) | 1007176 |
| Avoid using size functions inside loops (PHP)                                           | 1007184 |
| Avoid direct access to superglobals (PHP)                                               | 1007202 |
| Avoid fetching database rows as array and accessing using subscript (PHP)               | 1007218 |
| Avoid artifacts with Group By sql statement (PHP)                                       | 1007120 |
| Avoid artifacts with "select \*" Sql statement (PHP)                                    | 1007220 |
| Avoid artifacts with sql statements referring more than 4 Tables (PHP)                  | 1007118 |

### phpcs-security-audit

This package integrates with the existing "Pear" code sniffer. This
package is used to generate results for certain security related rules.
More information about this package is available
here: [https://github.com/FloeDesignTechnologies/phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit).
The licence agreement for this tool is available here:
[https://github.com/FloeDesignTechnologies/phpcs-security-audit/blob/master/LICENSE](https://github.com/FloeDesignTechnologies/phpcs-security-audit/blob/master/LICENSE).

#### Rules calculated by the phpcs-security-audit tool

| Rule name                                                                            | ID      |
|--------------------------------------------------------------------------------------|---------|
| CWE-79: Avoid use of raw user input that can expose XSS vulnerability (PHP)          | 1007244 |
| CWE-98: Avoid use of user input that can expose Stream Injection vulnerability (PHP) | 1007246 |
| CWE-624: Avoid preg_replace with /e option (PHP)                                     | 1007250 |
| CWE-661: Avoid filesystem function calls without sanitizing user input (PHP)         | 1007252 |

### PHPMD

More information about this tool is available
here:[ http://phpmd.org/](http://phpmd.org/). The
licence agreement for the PHPMD tool is detailed below:

*Copyright (c) 2009-2011, Manuel Pichler
\<[mapi@phpmd.org](mailto:mapi@phpmd.org)\>.*  
*All rights reserved. *  
  
*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:*

-   *Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.*
-   *Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the
    distribution.*
-   *Neither the name of Manuel Pichler nor the names of his
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.*

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*

### PHP Depend

More information about this tool is available
here: [http://pdepend.org/](http://pdepend.org/). The
licence agreement for the PHP Depend tool is available in the file
"LICENSE.txt" delivered in the source folder of the tool and is detailed
below:

*Copyright (c) 2008-2012, Manuel Pichler
\<[mapi@pdepend.org](mailto:mapi@pdepend.org)\>.*  
*All rights reserved.*  
  
*Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:*

-   *Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.*
-   *Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the
    distribution.*
-   *Neither the name of Manuel Pichler nor the names of his
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.*

*THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*
