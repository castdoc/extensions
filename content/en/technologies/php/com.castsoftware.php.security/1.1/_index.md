---
title: "Psalm Security Rules - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.php.security

## What's new?

See [Release Notes](rn/).

## Description

This extension provides a dedicated set of quality rules that have been
designed to check your PHP source code for user input security
defects or violations. These quality rules are based on the user input
security checks provided by the open source static analysis tool called
Psalm (see https://psalm.dev/) and are in addition to the PHP quality
rules provided by com.castsoftware.php (this is a dependent extension
and the Psalm Security Rules extension will run its quality rules
against the results of the PHP Analyzer analysis).

The following Psalm user input security checks are supported by this
extension:

- [https://psalm.dev/docs/running_psalm/issues/TaintedCallable/](https://psalm.dev/docs/running_psalm/issues/TaintedCallable/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedCookie/](https://psalm.dev/docs/running_psalm/issues/TaintedCookie/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedEval/](https://psalm.dev/docs/running_psalm/issues/TaintedEval/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedFile/](https://psalm.dev/docs/running_psalm/issues/TaintedFile/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedHeader/](https://psalm.dev/docs/running_psalm/issues/TaintedHeader/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedInclude/](https://psalm.dev/docs/running_psalm/issues/TaintedInclude/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedLdap/](https://psalm.dev/docs/running_psalm/issues/TaintedLdap/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedShell/](https://psalm.dev/docs/running_psalm/issues/TaintedShell/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedSql/](https://psalm.dev/docs/running_psalm/issues/TaintedSql/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedSSRF/](https://psalm.dev/docs/running_psalm/issues/TaintedSSRF/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedTextWithQuotes/](https://psalm.dev/docs/running_psalm/issues/TaintedTextWithQuotes/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedUnserialize/](https://psalm.dev/docs/running_psalm/issues/TaintedUnserialize/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedXpath/](https://psalm.dev/docs/running_psalm/issues/TaintedXpath/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedSleep/](https://psalm.dev/docs/running_psalm/issues/TaintedSleep/)
- [https://psalm.dev/docs/running_psalm/issues/TaintedExtract/](https://psalm.dev/docs/running_psalm/issues/TaintedExtract/)

A detailed explanation of the security checks provided by Psalm can be
seen in [https://psalm.dev/docs/security_analysis/](https://psalm.dev/docs/security_analysis/) with a dedicated section about [avoiding false-positives](https://psalm.dev/docs/security_analysis/avoiding_false_positives/).

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Item | Supported |
|---|:-:|
| Function Points (transactions) | :x: |
| Quality and Sizing | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Psalm Security Rules extension
requires that the following other CAST extensions are also installed:

- [com.castsoftware.php](https://extend.castsoftware.com/#/extension?id=com.castsoftware.php&version=latest) (in order to get the objects to attach violations to)
- [com.castsoftware.php.runtime82](https://extend.castsoftware.com/#/extension?id=com.castsoftware.php.runtime82&version=latest) (in order to have an executable launch Psalm)

Note that any dependent extensions are automatically downloaded and installed.

## Download and installation instructions

The extension will not be automatically downloaded and installed.If you need to use it, you should manually install the extension.

## What analysis results can you expect?

### Structural rules

| Release | Link |
|---------|------|
| 1.1.0-beta2 | [https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=\|\|1.1.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=%7C%7C1.1.0-beta2) |
| 1.1.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=\|\|1.1.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=%7C%7C1.1.0-beta1) |
| 1.1.0-alpha3 | [https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=\|\|1.1.0-alpha3](https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=%7C%7C1.1.0-alpha3) |
| 1.1.0-alpha2 | [https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=\|\|1.1.0-alpha2](https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=%7C%7C1.1.0-alpha2) |
| 1.1.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=\|\|1.1.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_php.security&ref=%7C%7C1.1.0-alpha1) |
