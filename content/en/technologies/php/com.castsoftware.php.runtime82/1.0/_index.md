---
title: "PHP Runtime 8.2 - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.php.runtime82

## What's new

See [Release Notes](rn/) for more information.

## Description

This extension redistributes PHP 8.2 binaries available at
[https://windows.php.net/downloads/releases/archives/](https://windows.php.net/downloads/releases/archives/).
Their content is described at
[https://www.php.net/releases/8.2/en.php](https://www.php.net/releases/8.2/en.php).

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |
