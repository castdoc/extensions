---
title: "Go - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.go

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for the Go programming language: https://go.dev/.

## Technology support

| Component | Version | Supported |
|-----------|---------|:----------:|
| Go        | 1.x     | :white_check_mark: |

## Supported file types

The following file types will be analyzed by the extension:

- `.go`
- `.mod`

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: no Quality Rules are implemented for gRPC-java

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

Whenever source code containing files with the extensions [listed above](#supported-file-types) is delivered for analysis, this extension will be automatically installed.

## What results can you expect?

### Objects

| Icon                                       | MetaModel Name     |
|:------------------------------------------:| ------------------ |
| ![](../images/CAST_Go_Package.png)         | Go Package         |
| ![](../images/CAST_Go_Module.png)          | Go Module          |
| ![](../images/CAST_Go_Struct.png)          | Go Struct          |
| ![](../images/CAST_Go_Type.png)            | Go Type            |
| ![](../images/CAST_Go_Interface.png)       | Go Interface       |
| ![](../images/CAST_Go_Method.png)          | Go Method          |
| ![](../images/CAST_Go_Abstract_Method.png) | Go Abstract Method |
| ![](../images/CAST_Go_Function.png)        | Go Function        |
| ![](../images/CAST_Go_Field.png)           | Go Field           |

### Links

Analysis of the Go application will result in the following links:

- callLink: Created when a method/abstract method or a function is called. These links connect Go Method/Abstract Go Method and Go Function objects between them.
- inheritExtendLink: Represents direct inheritance between:
    - Go Struct and Go Struct objects,
    - Go Type and Go Type objects,
    - Go Interface and Go Interface objects.
- inheritImplementLink: Represents inheritance between:
    - Go Struct and Go Interface objects,
    - Go Type and Go Interface objects,
    - Go Method and Abstract Go Method objects.
- inheritOverridetLink: Represents inheritance between Go Method and Abstract Go Method objects.
- accessReadLink: Created when a field is called. These links connect:
    - Go Function and Go Field objects,
    - Go Method and Go Field objects.
- mentionLink: Created when a struct is called. These links connect Go Function and Go Struct objects.
- relyOnLink: Created when a struct, a type or a interface is called. These links connect:
    - Go Function and Go Struct objects,
    - Go Function and Go Type objects,
    - Go Function and Go Interface objects.

#### callLink

```go
type S struct {
}
 
func (s S) m() string {
}
 
func f() {
    my_var := S {
    }
    my_var.m()
}
```
Go Analyzer will generate this Call link:

![](../images/calllink_example.png)

#### inheritExtendLink

Example 1:

```go
type I1 interface {
}
 
type I2 interface {
    I1
}
```
Go Analyzer will generate this Inherit Extend link (Go Interface):

![](../images/inheritextendlink1.png)

Example 2:

```go
type S1 struct {
}
 
type S2 struct {
    S1
```
Go Analyzer will generate this Inherit Extend link (Go Struct):

![](../images/inheritextendlink2.png)

#### inheritImplementLink

```go
type I interface {
    m()
}
 
type S struct {
}
 
func (s S) m() {
}
```
Go Analyzer will generate these Inherit Implement links:

![](../images/inheritimplementlink.png)

#### inheritOverrideLink

Go Analyzer will generate Inherit Override links between Go Methods and Abstract Go Methods as shown in the image above.

#### accessReadLink

```go
type S struct {
    field string
}
 
func (s S) m() string {
    return s.field
}
 
func main() {
    my_var := S {
        field: "Field",
    }
    my_var.m()
    my_var.field
}
```
Go Analyzer will generate these Access Read links:

![](../images/accessreadlink.png)

#### mentionLink

```go
type I interface {
    m()
}
 
type S struct {
}
 
func (s S) m() {
}
 
func f() I {
    return S {}
}
```
Go Analyzer will generate this Mention link when a Go Struct type is mentioned:

![](../images/mentionlink.png)

#### relyOnLink

```go

type I interface {
    m()
}

type S struct {
}
 
func (s S) m() {
}
 
func f() {
    my_var := S {}
    var v I = my_var
}
```

Go Analyzer will generate these Rely On links when the variables are declared as Go Struct and Go Interface types (and an additonal mentionLink as above):

![](../images/relyonlink.png)

### Structural Rules

| Release | Link |
|---|---|
| 1.0.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_go&ref=\|\|1.0.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_go&ref=%7C%7C1.0.0-beta1) |
| 1.0.0-alpha1 | [https://technologies.castsoftware.com/rules?sec=srs_go&ref=\|\|1.0.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_go&ref=%7C%7C1.0.0-alpha1) |
