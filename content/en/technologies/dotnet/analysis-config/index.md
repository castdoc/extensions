---
title: "Analysis configuration"
linkTitle: "Analysis configuration"
type: "docs"
weight: 5
---

## Introduction

As explained in [.NET - Prepare and deliver the source code](../prepare), CAST AIP extracts
relevant information used to create the automated analysis configuration
from the .NET project files. Currently the .NET Analyzer extension
supports these build project files:

-   Visual Studio 2003 - 2019

For any other build project format CAST AIP will not be able to
automatically retrieve build information: no Analysis Unit will be
created and no analysis configuration will be provided. To address this
situation, the Analysis Unit and the analysis configuration should be
created manually using the legacy CAST Management Studio (no interface
available in AIP Console to do this). This case falls outside of the
out-of-the-box support and is out of scope of a standard analysis.

# Using CAST Imaging Console

CAST Imaging Console exposes the Technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
DOTNET Technology to display the available options:

![](462553112.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application (i.e. all Analysis Units)
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same or similar as at Application level) that
        allow you to make fine-grained configuration changes.

Settings are initially set according to the information discovered
during the [.NET - Prepare and deliver the source code](../prepare) when creating a
version. You should check that these auto-determined settings are as
required and that at least one Analysis Unit exists for the specific
technology:

![](462553111.jpg)

## Technology level settings

### Dependencies

![](462553110.jpg)

Dependencies are configured at Application level for each
technology and are configured between individual Analysis
Units/groups of Analysis Units. You can find out more detailed
information about how to ensure dependencies are set up correctly,
in Validate dependency configuration.

## Analysis Units

This section lists all Analysis Units created automatically based on the
source code discovery process. You should check that at least one
Analysis Unit exists for the specific technology. Settings at Analysis
Unit level can be modified independently of the parent Technology
settings and any other Analysis Units if required - click the Analysis
Unit to view its technology settings:

![](462553107.jpg)

Clicking an
Analysis Unit will take you directly to the available Analysis Unit
level settings:

![](462553108.jpg)

### Technology settings at Analysis Unit level

All settings at this level are read-only for the .NET technology.

Main files

![](462553106.jpg)

|              |                                                                                                                                                                                |                                                                                                                                                                              |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Project path | The file based location of the corresponding project. This field is read-only.                                                                                                 |                                                                                                                                                                              |
| Project type | Indicates whether a Visual Studio project file or a Web site project folder will be used to determine the source code files for the analysis. This field is read-only. |                                                                                                                                                                              |
|              | Visual Studio project file                                                                                                                                                 | Indicates the .vbproj/.vbdproj/.csproj/.csdproj file that is defined as the Visual Studio project file for the Analysis Unit for analysis purposes. This field is read only. |
|              | Web site project folder                                                                                                                                                    | Indicates the folder that is defined as the Web site project folder for the Analysis Unit for analysis purposes. This field is read only.                                    |

Files to analyze

Lists all files that are included in the current Analysis Unit and which
will be analyzed:

![](462553105.jpg)

Referenced assemblies

This section lists all the .NET assemblies that have been referenced
while delivering the source code (see [.NET - Prepare and deliver the
source code](../prepare)) and which
are external to your project's source code, i.e. custom assemblies or
assemblies from the .NET Framework:

![](462553104.jpg)

# Using legacy CAST Management Studio

## Introduction to analysis configuration options

The CAST Management Studio has three levels at which analysis
configuration options can be set:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Technology</strong></td>
<td class="confluenceTd"><ul>
<li>The options available at this level are valid for <strong>all
Applications</strong> managed in the CAST Management Studio.</li>
<li>These are the default options that will be used to populate the same
fields at <strong>Application </strong>and <strong>Analysis
Unit </strong>level. If you need to define specific options for a
specific Application or Analysis Unit, then you can override them.</li>
<li>If you make a change to a specific option
at <strong>Application</strong> or <strong>Analysis Unit level</strong>,
and then subsequently change the same option at <strong>Technology
level</strong>, this setting will NOT be mirrored back to the
Application or Analysis Unit - this is because specific settings at
Application and Analysis Unit level have precedence if they have been
changed from the default setting available at Technology level.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Application</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>The options available at this level set are valid for
all <strong>corresponding Analysis Units</strong> defined in
the <strong>current Applicatio</strong>n (so making changes to a
specific option will mean all Analysis Units in that specific Technology
will "inherit" the same setting). If you need to define specific options
for a specific Analysis Unit in a specific Technology, then you can do
so at Analysis Unit level.</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Analysis Unit</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>Options available at this level are valid only for the specific
Analysis Unit.</li>
<li>An Analysis Unit can best be described as a set of configuration
settings that govern how a perimeter of source code is consistently
analyzed.</li>
<li>Analysis Units are automatically created when you use
the <strong>Set as current version</strong> option to deploy the
delivered source code - as such they correspond
to <strong>Projects</strong> discovered by the CAST Delivery Manager
Tool. However, they can also be created manually for situations where no
Analysis Unit has been automatically created for a given project.
<ul>
<li>When the Analysis Unit has been
created <strong>automatically</strong>, options will "inherit" their
initial configuration settings from the discovery process in the CAST
Delivery Manager Tool (i.e. "project" settings). Where an option could
not be defined automatically via the CAST Delivery Manager Tool, it will
"inherit" its initial configuration settings from those defined
at <strong>Technology level </strong>and at <strong>Application
level</strong>.</li>
<li>Analysis Units that are<strong> manually</strong> defined will
"inherit" their initial configuration settings from the settings defined
at <strong>Technology level</strong> and at <strong>Application
level</strong>.</li>
</ul></li>
<li>Modifying an identical option at <strong>Technology
level</strong> or at <strong>Application level </strong>will
automatically update the <strong>same option</strong> in
the <strong>Analysis Unit editor</strong> unless that specific option
has already been modified independently in the Analysis Unit
editor.</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

Some settings at Application and Analysis Unit level have a "Reset"
option - using this will reset the option to the value set at the parent
level:

![](attachments/219061153/219061152.jpg)

## Auto-configuration validation

### Technology / Application level

Using the Technology level or Application level options,
validate the settings for .NET packages. Make any update as
required. These settings apply to the Technology or Application as a
whole (i.e. all Analysis Units):

![](268370645.jpg)

![](268370644.jpg)

### Analysis Unit level

To inspect the auto-generated analysis configuration, you should
review the settings in each Analysis Unit - they can be accessed through
the Application editor:

![](268370646.jpg)

### Technology options

The available options for configuring an analysis are described below.
Note that some options are not available at specific levels
(Technology/Application/Analysis Unit):

### Source Settings

This tab shows the location of each type of source code in the .NET
Analysis Unit - this is determined automatically by the CAST Delivery
Manager Tool. You should, however, review the configuration and make any
changes you need:

![](268370648.jpg)

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Project path</strong></td>
<td colspan="2" class="confluenceTd">The file based location of the
corresponding project. This field is read-only. When the field contains
<strong>User defined</strong>, this indicates that the Analysis Unit has
been defined manually instead of automatically following the use of the
CAST Delivery Manager Tool.</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd"><strong>Source
Selection</strong></td>
<td colspan="2" class="confluenceTd">Indicates whether a <strong>Visual
Studio project file</strong> or a <strong>Web site project
folder</strong> will be used to determine the source code files for the
analysis. This field is read-only for all auto-created Analysis Units.
<p>When working with a manually defined Analysis Unit, you must select
an option to determine what will be analyzed. Selecting an option will
change the options below:</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Visual Studio project
file</strong></td>
<td class="confluenceTd">Indicates the .vbproj/.vbdproj/.csproj/.csdproj
file that is defined as the Visual Studio project file for the Analysis
Unit for analysis purposes.
<p>If this Analysis Unit is automatically defined, then this field is
read only.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Web site project folder</strong></td>
<td class="confluenceTd">Indicates the folder that is defined as the Web
site project folder for the Analysis Unit for analysis purposes.
<p>If this Analysis Unit is automatically defined, then this field is
read only.</p></td>
</tr>
</tbody>
</table>

### Analysis

The settings in this tab govern how the source code is handled by the
analyzer:

*Click to enlarge*

![](268370647.jpg)

#### Referenced Assemblies

Only visible at Analysis Unit level.

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>External Assemblies</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
This section lists all the <strong>.NET assemblies</strong> that have
been packaged in the CAST Delivery Manager Tool (usually via the
specific <strong>.NET assemblies on your file system</strong> option)
and which are external to your project's source code, i.e. custom
assemblies or assemblies from the .NET General Assembly Cache (GAC)
located under:
<ul>
<li><strong>C:\Windows\assembly</strong> (.NET Framework 1.0 - 3.5)</li>
<li><strong>C:\Windows\Microsoft .NET\assembly</strong> (.NET Framework
4 and above)</li>
</ul>
<p>These assemblies will be listed but will be "greyed" out and cannot
be edited, re-ordered or deleted - unlike manually added assemblies:</p>
<p><img src="268370649.jpg" draggable="false"
data-image-src="268370649.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="268370649"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="assemblies_list.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="268370642"
data-linked-resource-container-version="22" height="250" /></p>
<p>If an assembly was not delivered via the CAST Delivery Manager Tool,
it is possible to <strong>manually add</strong> an assembly or a folder
of assemblies:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="268370650.png" draggable="false"
data-image-src="268370650.png"
data-unresolved-comment-count="0" data-linked-resource-id="268370650"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="add.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="268370642"
data-linked-resource-container-version="22" />
</div></td>
<td class="confluenceTd">Add a <strong>new Assembly</strong> - you will
be given the choice of adding:
<ul>
<li>a single assembly as a <strong>file</strong> (.DLL)</li>
<li>a <strong>folder</strong> containing a set of assemblies (.DLL)</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="268370652.png" draggable="false"
data-image-src="268370652.png"
data-unresolved-comment-count="0" data-linked-resource-id="268370652"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="edit.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="268370642"
data-linked-resource-container-version="22" />
</div></td>
<td class="confluenceTd"><strong>Edit</strong> an assembly that has been
added via the CAST Management Studio.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="268370656.jpg" draggable="false"
data-image-src="268370656.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="268370656"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="up.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="268370642"
data-linked-resource-container-version="22" />
</div></td>
<td rowspan="2" class="confluenceTd"><strong>Re-order</strong> the list
of assemblies that have been added via the CAST Management Studio.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="268370651.jpg" draggable="false"
data-image-src="268370651.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="268370651"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="down.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="268370642"
data-linked-resource-container-version="22" />
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="268370654.png" draggable="false"
data-image-src="268370654.png"
data-unresolved-comment-count="0" data-linked-resource-id="268370654"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="remove.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="268370642"
data-linked-resource-container-version="22" />
</div></td>
<td class="confluenceTd"><strong>Delete</strong> an assembly that has
been added via the CAST Management Studio.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<img src="268370653.jpg" draggable="false"
data-image-src="268370653.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="268370653"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="reset.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="268370642"
data-linked-resource-container-version="22" />
</div></td>
<td class="confluenceTd"><p>Use this button to reset the list to the
entries defined by the CAST Delivery Manager Tool.</p></td>
</tr>
</tbody>
</table>
</div>
<p>Key for the list of assemblies table:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Type</strong></td>
<td class="confluenceTd"><p>Will display either:</p>
<ul>
<li><strong>Assembly folder</strong> (this is only displayed when an
assembly has been manually added)</li>
<li><strong>Assembly</strong></li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Path</strong></td>
<td class="confluenceTd">Shows the path to the assembly or assembly
folder. Assemblies delivered via the CAST Delivery Manager Tool will
point to the <strong>Delivery</strong> folder.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Recursive</strong></td>
<td class="confluenceTd">Only visible for a manually added Assembly
folder. Indicates whether (or not) the folder will be searched
recursively for .DLL Assemblies.</td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
</tbody>
</table>

#### Default Scripting

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Default Scripting Language for the
client side</strong></td>
<td class="confluenceTd">This option enables you to select the
<strong>Default Scripting Language</strong> for your
<strong>client-side</strong> files. Use the drop down list box to choose
between:
<ul>
<li><strong>&lt;none&gt;</strong></li>
<li><strong>JavaScript</strong></li>
<li><strong>VBScript</strong></li>
</ul></td>
</tr>
</tbody>
</table>

### Text Replacement

This section enables you to define Regular Expressions (that match
character strings in your selected files) that you want to replace with
other text:

-   Click the button to add a new line to the table listing the Text
    Replacements
-   Double click the new line in the table and a hidden section will
    appear in which you can enter the details you require:

![](attachments/219676875/219676868.jpg)

-   Enter the Regular Expression in the Regular Expression field.
-   Chose the type of Regular Expression in the drop down list:
    -   Perl (default format) - Specifies that when a regular
        expression match is to be replaced by a new string, that the new
        string is constructed using the same rules as Perl 5.
    -   Sed - Specifies that when a regular expression match is to
        be replaced by a new string, that the new string is constructed
        using the rules used by the Unix Sed utility in IEEE Std
        1003.1-2001, Portable Operating System Interface (POSIX), Shells
        and Utilities.
    -   Tcl - Specifies that when a regular expression match is to
        be replaced by a new string, the text that matches the regular
        expression will be replaced by the replacement text.
-   Enter the text you want to replace the Regular Expression with, in
    the Replacing Text field.
-   The new line in the table above should now be populated with your
    changes:

![](attachments/219676875/219676869.jpg)

<table class="confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="attachments/219676875/219676870.png" draggable="false"
data-image-src="attachments/219676875/219676870.png"
data-unresolved-comment-count="0" data-linked-resource-id="219676870"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="add.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="219676875"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Add a new Text Replacement</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="attachments/219676875/219676871.jpg" draggable="false"
data-image-src="attachments/219676875/219676871.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="219676871"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="up.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="219676875"
data-linked-resource-container-version="2" /></p>
</div></td>
<td rowspan="2" class="confluenceTd">Re-order the list of Text
Replacements. This is <strong>important</strong> because the order in
which the items appear in the list, is the order in which the analyzer
will replace the character strings that match the Regular Expressions.
Thus, if you have a complicated set of Regular Expressions and
replacement texts, difficulties could arise if the order in which they
are dealt with is not correct.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="attachments/219676875/219676872.jpg" draggable="false"
data-image-src="attachments/219676875/219676872.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="219676872"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="down.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="219676875"
data-linked-resource-container-version="2" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="attachments/219676875/219676873.png" draggable="false"
data-image-src="attachments/219676875/219676873.png"
data-unresolved-comment-count="0" data-linked-resource-id="219676873"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="remove.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="219676875"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Delete an existing Text Replacement</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="attachments/219676875/219676874.jpg" draggable="false"
data-image-src="attachments/219676875/219676874.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="219676874"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="reset.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="219676875"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd"><strong>Only visible at Application and
Analysis Unit level</strong>
<p>Use this button to reset the list to the entries defined at one level
higher:</p>
<ul>
<li><strong>Analysis Unit level</strong> - entries will be reset to all
listed at <strong>Appplication level</strong></li>
<li><strong>Application level</strong> - entries will be reset to only
those listed at <strong>Technology level</strong></li>
</ul></td>
</tr>
</tbody>
</table>

-   Use Text Replacement with caution. First try to change the
    source tree to reflect the production environment and then use Text
    Replacement for other cases.
-   You can use the Test Text Replacement option to check whether
    your proposed configuration will function as expected:
    -   You will be prompted to choose a source file on which the Text
        Replacement will be tested.
    -   Results of the replacement will be shown in a copy of the
        selected source file.

#### JavaScript Frameworks

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Exclude standard JavaScript
libraries?</strong></td>
<td class="confluenceTd">This option enables you to exclude standard
JavaScript libraries from the analysis: when excluded, standard
JavaScript libraries will be analyzed and stored in the CAST Analysis
Service (thus allowing links to resolve correctly), however the objects
will not appear in the CAST Engineering Dashboard. This prevents the
"pollution" of Technical Size measures, module/application grades, and
violation lists (among others) with large numbers of JavaScript objects
that are not used and not maintained by the Development team. This
option is active by default and activates the default .NET Environment
Profile. for <strong>JavaScript Frameworks</strong>.</td>
</tr>
</tbody>
</table>

### Production

![](268370658.jpg)

#### Data to generate - Dynamic Analysis options

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Resolve virtual function
calls</strong></td>
<td class="confluenceTd"><p>Selecting this option (default position)
will force the analyzer to resolve calls made by virtual functions. When
selected, this option will automatically activate what was known as the
<strong>Inference Engine</strong> in previous releases of CAST AIP (run
time type information will be computed in order to simulate program
behaviour during execution of the analyzer and thus identify additional
links that would not otherwise be "discovered" using standard analysis
techniques. This technology detects a reference to an object wherever
its name is mentioned, regardless of the context in which this reference
occurs).</p>
<p>Unselecting this option will cause a loss of links to virtual
functions and therefore have a significant (negative) impact on the
calculation of Automated Function Points.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Create accurate client / server
links</strong></td>
<td class="confluenceTd"><p>Selecting this option will force the
analyzer to find all method calls that target a database, and find all
strings that eventually reach those calls. Any database link created
from those strings is guaranteed to be correct. Without this option, all
database links created from strings are not guaranteed to be correct
(e.g. strings used in logs).</p>
<p>When selected, this option will automatically activate what was known
as the <strong>Inference Engine</strong> in previous releases of CAST
AIP (run time type information will be computed in order to simulate
program behaviour during execution of the analyzer and thus identify
additional links that would not otherwise be "discovered" using standard
analysis techniques. This technology detects a reference to an object
wherever its name is mentioned, regardless of the context in which this
reference occurs).</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>String Concatenation</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>When <strong>Resolve virtual function calls</strong> or
<strong>Create accurate client / server links</strong> is selected, this
value (default = 15000) limits the number of strings that can be found
during the search of each object value.</p>
<div>
<div>
Note that limiting the number of strings can lead to incomplete results,
however, performance is improved.
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Procedure Call Depth</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>When <strong>Resolve virtual function calls</strong> or
<strong>Create accurate client / server links</strong> is selected, this
value (default value in <strong>≤ 8.3.17</strong>
= <strong>3000</strong> and in <strong>≥ 8.3.18</strong> =
<strong>300</strong>) limits the number of intermediate values that the
Inference Engine can resolve in order to obtain the type of the object
that is being searched for.</p>
<div>
<div>
<ul>
<li>Limiting the number of intermediate values will improve performance
for certain applications where the default value is too high. For
example, changing the value to 300 for Applications which ordinarily
have a very long analysis time with the default value of 3000
in ≤ 8.3.17, will significantly reduce the analysis time. However,
reducing the value can lead to less precise results, but this is usually
an acceptable trade off, especially when the unsuccessful candidates are
automatically linked to all overrides using the fallback mechanism that
has been introduced in releases 1.0.14, 1.1.6 and 1.2.0-beta5 of the
.NET Analyzer. The lowest value you can enter is 1.</li>
</ul>
<ul>
<li>For all Applications newly onboarded with ≥ 8.3.18 the value will be
set to 300. For Applications that are upgraded from a previous release
of AIP to ≥ 8.3.18, the previous value for this option will be
retained to avoid impacting analysis results, therefore to benefit from
improved performance, you will need to manually update the value to
300.</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Allow extensions</strong></td>
<td class="confluenceTd">This option is active by default. If you have
installed any extensions to support technologies such as WPF or WCF,
then the extension will be called and run during the .NET analysis. If
you would prefer to deactivate the extensions during a .NET analysis
(for example to troubleshoot or to improve performance), unticking this
option will do so. The extensions will not be called during the .NET
Analysis.</td>
</tr>
</tbody>
</table>

#### Data to generate - Web parsing options

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Add called files to selected files
list</strong></td>
<td class="confluenceTd">Selecting this option will automatically
<strong>add</strong> any files to the list of files selected for
analysis that are <strong>called</strong> by files that you have already
selected.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Add included files to selected files
list</strong></td>
<td class="confluenceTd">Selecting this option will <strong>add</strong>
any files to the analysis list that are <strong>included</strong> in
files that you have already selected.</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Maximal Complexity</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>This option enables you to manually enter a percentage which will
refer to the m complexity of the analysis process.</p>
<ul>
<li>A complexity percentage of <strong>100</strong> (default)
corresponds to a maximum of <strong>10,000</strong> function analyses
(function * context) for a given file.</li>
<li>A complexity percentage of <strong>70</strong> corresponds to a
maximum of <strong>7,000</strong> function analyses (function * context)
for a given file.</li>
<li>A complexity percentage of <strong>0</strong> corresponds to an
unlimited analysis of all the called functions.</li>
</ul>
<p>If the maximum number of function analyses is reached, this will be
logged.</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>File analysis too complex. Next function calls will not be followed by a 
function analysis if the function has already been analyzed</code></pre>
</div>
</div>
<p>When the limit is reached, links to the function will be created
correctly, but links from the function will not be created.</p>
<p>You should alter the complexity percentage if your analyses are
taking a long time to complete with the default setting of 100%.
Changing the percentage to a lower figure will reduce the number of
function analyses for a given file, thus improving performance. Remember
though that doing will cause some links not to be created.</p>
</div></td>
</tr>
</tbody>
</table>

#### Dynamic Links Rules

See the CAST Management Studio help for more information about this
global option.

### Dependency settings

If the CAST Delivery Manager Tool did not detect any inter-Analysis Unit
dependencies (i.e. Discovered dependencies) then it will create a
"global" default dependency between .NET and .NET. CAST highly
recommends that this default rule is removed if the deployed package
contains more than three Analysis Units. In addition, CAST highly
recommends that you avoid creating a custom .NET \> .NET rule if the
deployed package contains more than three Analysis Units.

This global default rule can cause the creation of inaccurate links
between objects which has a knock on effect on Quality Rule results and
on Quality Rule results and on Transaction flow.


