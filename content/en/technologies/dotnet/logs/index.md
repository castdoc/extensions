---
title: "Log messages"
linkTitle: "Log messages"
type: "docs"
weight: 6
---

>This page presents the different messages that are produced by the .NET
Analyzer during application source code analysis. Messages are presented
with their identifier (referenced in the analysis log files), a
description, and a possible remediation. 

## DOTNET.0156

|                 |                                                                                                                                                                                                                             |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0156                                                                                                                                                                                                                 |
| Message     | Dead source file {filename} found in source code, this file will not be analyzed!                                                                                                                                           |
| Severity    | Info                                                                                                                                                                                                                        |
| Explanation | Source file found which is not compiles for the project and therefore will not be analyzed. There are several possible reasons: file not referenced in a not SDK-style project, file excluded by pattern,content file, etc. |
| User Action | None.                                                                                                                                                                                                                       |

## DOTNET.0155

|                 |                                                                                                                                                |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0155                                                                                                                                    |
| Message     | Unrecognized format of project file.                                                                                                           |
| Severity    | Warning                                                                                                                                        |
| Explanation | Due to the unrecognized file format, the analyzer fails to get any information from project file. Hence the project is skipped (not analyzed). |
| User Action | Contact CAST support                                                                                                                           |

## DOTNET.0150

|                 |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0150                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Message     | No definition found for the name 'xxxxx'. Therefore no link will be drawn to that object.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Explanation | This message is a summary of messages coming from the C# or VB.NET compiler.For C#: CS0234 (The type or namespace name 'name' does not exist in the namespace 'namespace'), CS0246 (The type or namespace name 'type/namespace' could not be found), CS0103 (The name 'name' does not exist in the current context). For VB.NET: BC30002 (Type '`<typename>`' is not defined), BC30456 ('\<member name\>' is not a member of '\<type name\>'), BC30451 ('name' is not declared. It may be inaccessible due to its protection level). When several of these compiler messages are emited for the same name, one single message DOTNET.0150 is emited to summarize them. The original compiler messages (CS0234, CS0246, ...) can be obtained by activating debug messages. These messages are caused by a missing dll, nupkg, or user project. The impacts are: 1) no link will be drawn to the unresolved object, 2) links may be less accurate because of more overload resolution errors, 3) dataflow computations (dynamic links, devirtualization, user input security) may be less accurate because of leaks in the generated CASTIL files. |
| User Action | Address first the corresponding DOTNET.0142 Message, if any corresponding. Provide the missing dll, nupkg file, or user project.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |

## DOTNET.0147

|                 |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0147                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Message     | Dependency {reference name} not found for project {project path}.                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| Explanation | The reference cannot be not found.\<br/\>\<br/\>The following situations will cause this warning to be displayed in the log, however, they can also be safely ignored after investigation:\<br/\>\<br/\>- The same reference is found in a .dll that is included in the source code: in this case the only thing to verify is that .dll has been packaged correctly and is referenced in the .NET analysis units.\<br/\>- The same reference is found in a .csproj that is included in the application source code. |
| User Action | Provide the missing dll.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |

## DOTNET.0143

|                 |                                                                                                                          |
|-----------------|--------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0143                                                                                                              |
| Message     | {FrameworkKind} project detected. {FrameworkKind} projects are not officially supported. Project is : {ProjectFilePath}. |
| Severity    | Warning                                                                                                                  |
| Explanation | .NET framework used here is not supported by the analyzer. The results may be degraded.                                  |
| User Action | None.                                                                                                                    |

## DOTNET.0142

|                 |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0142                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| Message     | No resource found for package {0}. Package reference ignored in project {1}", pkgName, currentProject.ProjectDesc.Path                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| Explanation | The project mentioned in the message references a Nuget package for which neither a .nupkg file nor an extraction was found. The impacts are:\<br/\>1) no link will be drawn to external objects of that package\<br/\>2) links may be less accurate because of more overload resolution errors\<br/\>3) dataflow computations (dynamic links, devirtualization, user input security) may be less accurate because of leaks in the generated CASTIL files.\<br/\>\<br/\>Note that some missing package warnings do not affect an analysis, because they do not define programming symbols, they just define tools used at build time and as such they can be ignored. For example, the following can be safely ignored:\<br/\>\<br/\>1) DOTNET.0142:No resource found for package Microsoft.AspNetCore.Razor.Design.\<br/\>2) DOTNET.0142:No resource found for package Microsoft.VisualStudio.Web.CodeGeneration.Design.\<br/\>3) DOTNET.0142:No resource found for package SerilogAnalyzer.\<br/\>4) DOTNET.0142:No resource found for package StyleCop.Analyzers.\<br/\>5) DOTNET.0142:No resource found for package Selenium.\<br/\>\<br/\>In addition, the following situations will cause a warning to be displayed in the log, however, they can also be safely ignored after investigation:\<br/\>\<br/\>1) The same reference is found in a .dll that is included in the source code: in this case the only thing to verify is that .dll has been packaged correctly and is referenced in the .NET analysis units.\<br/\>2) The same reference is found in a .csproj that is included in the application source code. |
| User Action | Provide the missing .nupkg file with the sources of the application. Once downloaded from Nuget Gallery (<https://www.nuget.org/>), the .nupkg file should be provided with the source code in the root folder.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |

## DOTNET.0141

|                 |                                                                                                   |
|-----------------|---------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0141                                                                                       |
| Message     | Assembly references folder/file {assembly reference path} not found for project {project path}.   |
| Severity    | Info                                                                                              |
| Explanation | The path to the assembly reference leads to a folder or a file that doesn't exist on the machine. |
| User Action | Provide correct path to the assembly reference.                                                   |

## DOTNET.0140

|                 |                                                                                                                                 |
|-----------------|---------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0140                                                                                                                     |
| Message     | Required .Net framework version could not be recognized. Version {version} will be used for analysis of project {project path}. |
| Severity    | Info                                                                                                                            |
| Explanation | .NET framework version could not be recognized. The results may be degraded.                                                    |
| User Action | None.                                                                                                                           |

## DOTNET.0131

|                 |                                                 |
|-----------------|-------------------------------------------------|
| Identifier  | DOTNET.0131                                     |
| Message     | Starting to remove references.                  |
| Severity    | Info                                            |
| Explanation | The operation to remove references has started. |
| User Action | None.                                           |

## DOTNET.0130

|                 |                                                                                                                                     |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0130                                                                                                                         |
| Message     | Memory profile: GC: {0:n0} / Total: {1:n0} / Peak: {2:n0}", GC.GetTotalMemory(gcCollect), p.PrivateMemorySize64, p.PeakWorkingSet64 |
| Severity    | Info                                                                                                                                |
| Explanation | The profile of the memory consumption is displayed.                                                                                 |
| User Action | None.                                                                                                                               |

## DOTNET.0103

|                 |                                                                                                                                                                                                                                    |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0103                                                                                                                                                                                                                        |
| Message     | An exception occurred while generating code for method {method mangling}.                                                                                                                                                          |
| Severity    | Warning                                                                                                                                                                                                                            |
| Explanation | Error during generation of CASTIL code of specified method, due to limitations of the support of some C# and VB features, mainly lambda functions. It may impact: client/server links, de-virtualization, and user input security. |
| User Action | None.                                                                                                                                                                                                                              |

## DOTNET.0063

|                 |                                         |
|-----------------|-----------------------------------------|
| Identifier  | DOTNET.0063                             |
| Message     | Project loaded: " + projectPathWithCase |
| Severity    | Info                                    |
| Explanation | The indicated project has loaded.       |
| User Action | None.                                   |

## DOTNET.0062

|                 |                                                                                |
|-----------------|--------------------------------------------------------------------------------|
| Identifier  | DOTNET.0062                                                                    |
| Message     | IdToSymbol:\t{0}\tIdToProjects:\t{1}\tUsed memory (estimation):\t{2}\t{3}\t{4} |
| Severity    | Info                                                                           |
| Explanation | The used memory for object in the project indicated.                           |
| User Action | None.                                                                          |

## DOTNET.0061

|                 |                                                                                                                 |
|-----------------|-----------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0061                                                                                                     |
| Message     | {0} candidates found, starting dataflow resolution.                                                             |
| Severity    | Info                                                                                                            |
| Explanation | A number of candidates for the external link builder have been found. The dataflow resolution has been started. |
| User Action | None.                                                                                                           |

## DOTNET.0060

|                 |                                                                                                                    |
|-----------------|--------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0060                                                                                                        |
| Message     | {0} candidates found, starting external links engine (grep mode).                                                  |
| Severity    | Info                                                                                                               |
| Explanation | A number of candidates for the external link builder have been found, starting external links engine in grep mode. |
| User Action | None.                                                                                                              |

## DOTNET.0059

|                 |                                                                                    |
|-----------------|------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0059                                                                        |
| Message     | Starting to look for external link entry points (grep mode).                       |
| Severity    | Info                                                                               |
| Explanation | Started to look for the external links entry points for the external link builder. |
| User Action | None.                                                                              |

## DOTNET.0058

|                 |                                                                                         |
|-----------------|-----------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0058                                                                             |
| Message     | {x} candidates found, starting external link engine (dataflow mode)", entryPoints.Count |
| Severity    | Info                                                                                    |
| Explanation | A number of candidates found for the external links builder. Starting link engine.      |
| User Action | None.                                                                                   |

## DOTNET.0057

|                 |                                                                                                     |
|-----------------|-----------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0057                                                                                         |
| Message     | Starting to look for external link entry points (dataflow mode) for " + GetDatabaseAccessName(kind) |
| Severity    | Info                                                                                                |
| Explanation | Starting to look for the external links entry points for the specified database.                    |
| User Action | None.                                                                                               |

## DOTNET.0056

|                 |                                                                                                                                                                                                                                                                            |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0056                                                                                                                                                                                                                                                                |
| Message     | End of removing references.                                                                                                                                                                                                                                                |
| Severity    | Info                                                                                                                                                                                                                                                                       |
| Explanation | The operation of removing referenced has ended. No nupkg file was found in the source delivery corresponding to a package reference found in the specified project. This means that no external object from that dependency will be saved and no link will be drawn to it. |
| User Action | None.                                                                                                                                                                                                                                                                      |

## DOTNET.0055

|                 |                                       |
|-----------------|---------------------------------------|
| Identifier  | DOTNET.0055                           |
| Message     | Visiting file " + document.FilePath   |
| Severity    | Info                                  |
| Explanation | The file indicated is being analyzed. |
| User Action | None.                                 |

## DOTNET.0054

|                 |                                                              |
|-----------------|--------------------------------------------------------------|
| Identifier  | DOTNET.0054                                                  |
| Message     | Calling extensibility callbacks.                             |
| Severity    | Info                                                         |
| Explanation | The operation of calling extensibility callback has started. |
| User Action | None.                                                        |

## DOTNET.0053

|                 |                                                |
|-----------------|------------------------------------------------|
| Identifier  | DOTNET.0053                                    |
| Message     | Saving CastIL.                                 |
| Severity    | Info                                           |
| Explanation | The CAST Intermediate Language is being saved. |
| User Action | None.                                          |

## DOTNET.0052

|                 |                                       |
|-----------------|---------------------------------------|
| Identifier  | DOTNET.0052                           |
| Message     | Visiting file " + document.FilePath   |
| Severity    | Info                                  |
| Explanation | The file indicated is being analyzed. |
| User Action | None.                                 |

## DOTNET.0051

|                 |                                             |
|-----------------|---------------------------------------------|
| Identifier  | DOTNET.0051                                 |
| Message     | Cleaning AST objects out of memory.         |
| Severity    | Info                                        |
| Explanation | The AST objects are cleaned up from memory. |
| User Action | None.                                       |

## DOTNET.0050

|                 |                                                                                |
|-----------------|--------------------------------------------------------------------------------|
| Identifier  | DOTNET.0050                                                                    |
| Message     | Starting analysis of " + project.Path + " (" + (++i) + " / " + nbProjects + ") |
| Severity    | Info                                                                           |
| Explanation | The analysis of the project has started.                                       |
| User Action | None.                                                                          |

## DOTNET.0049

|                 |                                              |
|-----------------|----------------------------------------------|
| Identifier  | DOTNET.0049                                  |
| Message     | Running managed analyzer for .NET languages. |
| Severity    | Info                                         |
| Explanation | The analysis of .NET languages has started.  |
| User Action | None.                                        |

## DOTNET.0048

|                 |                                                                                                                                                                                 |
|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0048                                                                                                                                                                     |
| Message     | Error while loading XML document '{0}': {1}. The analysis results may be incomplete.                                                                                            |
| Severity    | Warning                                                                                                                                                                         |
| Explanation | The XML document could not be loaded as an error has occurred therefore the results of the analysis could be incomplete.                                                        |
| User Action | In the first instance, please ensure that the XML file listed in the error message is syntactically correct and is not empty. If the error persists, then contact CAST Support. |

## DOTNET.0047

|                 |                                                                                                                                                            |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0047                                                                                                                                                |
| Message     | Duplicated project path will be ignored: {0}. The analysis results may be incomplete.                                                                      |
| Severity    | Warning                                                                                                                                                    |
| Explanation | For the current analysis there are project with a duplicated path. The duplicated path will be ignored and the results of the analysis will be incomplete. |
| User Action | Contact CAST Support.                                                                                                                                      |

## DOTNET.0046

|                 |                                                                                                                                                            |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0046                                                                                                                                                |
| Message     | Duplicated project path will be ignored: {0}. The analysis results may be incomplete.                                                                      |
| Severity    | Warning                                                                                                                                                    |
| Explanation | For the current analysis there are project with a duplicated path. The duplicated path will be ignored and the results of the analysis will be incomplete. |
| User Action | Contact CAST Support.                                                                                                                                      |

## DOTNET.0045

|                 |                                                                                                                                                                      |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0045                                                                                                                                                          |
| Message     | No 'web.config' found in {0}. The folder is not a website, and will not be analyzed. The analysis results may be incomplete.                                         |
| Severity    | Warning                                                                                                                                                              |
| Explanation | As there is no web.config file found the application cannot be considered as n web site and it will not be analysed. The results of the analysis will be incomplete. |
| User Action | Check that the application has an web.config file. If that is the case then check that the delivery of the sources has been done without any error and warning.      |

## DOTNET.0044

|                 |                                                                                                                                     |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0044                                                                                                                         |
| Message     | Source file '{0}' referenced in '{1}' was not found. The analysis results may be incomplete.                                        |
| Severity    | Warning                                                                                                                             |
| Explanation | The source file referenced has not been found and therefore cannot be analyzed .The result of the analysis will be impacted.        |
| User Action | Check that referenced file is available in the sources and the delivery of the sources has been done without any error or warnings. |

## DOTNET.0043

|                 |                                                                                                                              |
|-----------------|------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0043                                                                                                                  |
| Message     | Could not load assembly " + assdir.AssemblyName + ". The analysis results may be incomplete.                                 |
| Severity    | Warning                                                                                                                      |
| Explanation | The indicated assembly could not be loaded. The results of the analysis can be incomplete with fewer objects and links.      |
| User Action | Check that the assembly is part of the sources and the delivery of the sources has been done without any error and warnings. |

## DOTNET.0042

|                 |                                                                                                                            |
|-----------------|----------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0042                                                                                                                |
| Message     | Source file '{0}' referenced in '{1}' was not found. The analysis results may be incomplete.", codeFile, asmxFile.FullName |
| Severity    | Warning                                                                                                                    |
| Explanation | The source file indicated was not found. This will impact the result of the analysis.                                      |
| User Action | Check that the source file indicated is part of the project and has been included to the delivery.                         |

## DOTNET.0041

|                 |                                                                                                                             |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0041                                                                                                                 |
| Message     | Source file '{0}' referenced in '{1}' was not found. The analysis results may be incomplete.", codeFile, asmxFile.FullName. |
| Severity    | Warning                                                                                                                     |
| Explanation | The asmx source file indicated was not found. This will impact the result of the analysis.                                  |
| User Action | Check that the asmx source file indicated is part of the project and has been included to the delivery.                     |

## DOTNET.0040

|                 |                                                                                                                                      |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0040                                                                                                                          |
| Message     | Source file is never referenced, and therefore will be ignored: " + source.FullName + ". The analysis results may be incomplete.     |
| Severity    | Warning                                                                                                                              |
| Explanation | The indicated source file is never referenced by any project, therefore the file will not be taken into account during the analysis. |
| User Action | Check that the file is used by a project and if this not the case remove it from the analysis.                                       |

## DOTNET.0039

|                 |                                                                                                                                                                                                                                                                                                 |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0039                                                                                                                                                                                                                                                                                     |
| Message     | App_Code folder contains both C# code and VB code. Could not create a compilation unit for that folder. The analysis results will be incomplete.                                                                                                                                                |
| Severity    | Warning                                                                                                                                                                                                                                                                                         |
| Explanation | The application that is about to be analysed contains both C# and VB .Net source code in the App_Code folder and the analyser cannot create a compilation unit for that folder. All the source code contained in that folder will not be analysed and therefore the results will be incomplete. |
| User Action | Review the application and include source code for only one language in the App_Code folder.                                                                                                                                                                                                    |

## DOTNET.0038

|                 |                                                                                                                    |
|-----------------|--------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0038                                                                                                        |
| Message     | Dependency {dependency project path} not found for project {project path}. The analysis results may be incomplete. |
| Severity    | Warning                                                                                                            |
| Explanation | Missing project dependency therefore analysis results may be incomplete.                                           |
| User Action | Add missing project.                                                                                               |

## DOTNET.0037

|                 |                                                                                                                 |
|-----------------|-----------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0037                                                                                                     |
| Message     | File could not be loaded: {0}. The analysis results may be incomplete.", file.AbsolutePath.                     |
| Severity    | Warning                                                                                                         |
| Explanation | The file cannot be loaded therefore the results of the analysis can be impacted with missing objects and links. |
| User Action | Contact CAST support.                                                                                           |

## DOTNET.0036

|                 |                                                                                                                                                                              |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0036                                                                                                                                                                  |
| Message     | Failed to generate code for dataset: {0}. The analysis results may be incomplete.", xsdFile.                                                                                 |
| Severity    | Warning                                                                                                                                                                      |
| Explanation | The source code for the dataset ca not be generated. This source code is used in the analysis of the application, therefore the results of the analysis could be incomplete. |
| User Action | Contact CAST Support.                                                                                                                                                        |

## DOTNET.0035

|                 |                                                                                                                                            |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0035                                                                                                                                |
| Message     | Could not generate {0} files from XAML files. The analysis results may be incomplete.", sourceExtension.                                   |
| Severity    | Warning                                                                                                                                    |
| Explanation | The analyzer can not generate the files from the XAML files that are used for the analysis, therefore the analysis results are incomplete. |
| User Action | Contact CAST Support.                                                                                                                      |

## DOTNET.0034

|                 |                                                                                                                                            |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0034                                                                                                                                |
| Message     | Could not generate {0} files from XAML files. The analysis results may be incomplete.", sourceExtension.                                   |
| Severity    | Warning                                                                                                                                    |
| Explanation | The analyzer can not generate the files from the XAML files that are used for the analysis, therefore the analysis results are incomplete. |
| User Action | Contact CAST Support.                                                                                                                      |

## DOTNET.0033

|                 |                                                                                                                                                                  |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0033                                                                                                                                                      |
| Message     | Could not generate {0} files from XAML files: {1} not found. The analysis results may be incomplete.", sourceExtension, exe.                                     |
| Severity    | Warning                                                                                                                                                          |
| Explanation | Because XAML files are not found, the generation of the intermediate file that will be analyzed cannot be done therefore the analysis results may be incomplete. |
| User Action | Contact CAST Support.                                                                                                                                            |

## DOTNET.0032

|                 |                                                                             |
|-----------------|-----------------------------------------------------------------------------|
| Identifier  | DOTNET.0032                                                                 |
| Message     | Failed to create the WSDL importer. The analysis results may be incomplete. |
| Severity    | Warning                                                                     |
| Explanation | An issue has occurred and the WSDL importer could not be created.           |
| User Action | Contact CAST Support.                                                       |

## DOTNET.0031

|                 |                                                                                                                                                  |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0031                                                                                                                                      |
| Message     | Failed to create the WSDL importer. The analysis results may be incomplete.                                                                      |
| Severity    | Warning                                                                                                                                          |
| Explanation | An issue has occurred during the preprocess of the WSDL. The results of the analysis may be incomplete leading to the loss of objects and links. |
| User Action | Contact CAST support.                                                                                                                            |

## DOTNET.0030

|                 |                                                                                                                                                          |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0030                                                                                                                                              |
| Message     | Failed to generate proxy code for service reference: {0}. The analysis results may be incomplete.", svcmapFile.                                          |
| Severity    | Warning                                                                                                                                                  |
| Explanation | An issue has occurred during the preprocessing of the wsdl file. The results of the analysis may be incomplete leading to the loss of objects and links. |
| User Action | Contact CAST support.                                                                                                                                    |

## DOTNET.0029

|                 |                                                                                      |
|-----------------|--------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0029                                                                          |
| Message     | Unexpected exception: {0}", ex.Message.                                              |
| Severity    | Warning                                                                              |
| Explanation | An unexpected exception occurred with unknown impact on the results of the analysis. |
| User Action | Contact CAST support.                                                                |

## DOTNET.0028

|                 |                                                                                                                                     |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0028                                                                                                                         |
| Message     | An error happened while treating {0}. The analysis results may be incomplete.", xsdFile.FullName.                                   |
| Severity    | Warning                                                                                                                             |
| Explanation | An issue has occurred while threating the file therefore the results of the analysis may be incomplete (loss of objects and links). |
| User Action | Contact CAST support.                                                                                                               |

## DOTNET.0027

|                 |                                                                                                    |
|-----------------|----------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0027                                                                                        |
| Message     | An error happened while treating {0}. The analysis results may be incomplete.", wsdlFile.FullName. |
| Severity    | Warning                                                                                            |
| Explanation | An issue has occurred while treating the WSDL file. The results of the analysis may be incomplete. |
| User Action | Contact CAST support.                                                                              |

## DOTNET.0026

|                 |                                                                                                            |
|-----------------|------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0026                                                                                                |
| Message     | Failed to generate code for web reference: {0}. The analysis results may be incomplete.", wsdlFile.        |
| Severity    | Warning                                                                                                    |
| Explanation | An issue has occurred while generating code for the web reference. The results analysis may be incomplete. |
| User Action | Contact CAST support.                                                                                      |

## DOTNET.0025

|                 |                                                                                                                              |
|-----------------|------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0025                                                                                                                  |
| Message     | Could not load referenced assembly {0} in {1}. The analysis results may be incomplete.", asmName.Value, svcmapFile.FullName. |
| Severity    | Warning                                                                                                                      |
| Explanation | The referenced assembly cannot be loaded therefore the results may be incomplete.                                            |
| User Action | Contact CAST support.                                                                                                        |

## DOTNET.0024

|                 |                                                                                                           |
|-----------------|-----------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0024                                                                                               |
| Message     | Failed to load types from referenced assembly: {0}. The analysis results may be incomplete.", ex.Message. |
| Severity    | Warning                                                                                                   |
| Explanation | An issue has occurred and the types has not been loaded from the indicated assembly.                      |
| User Action | Contact CAST support.                                                                                     |

## DOTNET.0023

|                 |                                                                                                                                    |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0023                                                                                                                        |
| Message     | Could not trace link for {0}: {1}. The analysis results may be incomplete.", request, e.Message.                                   |
| Severity    | Warning                                                                                                                            |
| Explanation | An issue has occurred during the build of the external links therefore the link cannot be created. The analysis may be incomplete. |
| User Action | Contact CAST support.                                                                                                              |

## DOTNET.0022

|                 |                                                                                                            |
|-----------------|------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0022                                                                                                |
| Message     | Error when creating devirtualization link: {0}. The analysis results may be incomplete.", e.Message.       |
| Severity    | Warning                                                                                                    |
| Explanation | An issue has occurred when creating devirtualisation links. The results of the analysis may be incomplete. |
| User Action | Contact CAST support.                                                                                      |

## DOTNET.0021

|                 |                                                                                                                 |
|-----------------|-----------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0021                                                                                                     |
| Message     | Error while processing visitor: " + castilAnalyzer.GetType().Name + ". The analysis results may be incomplete." |
| Severity    | Warning                                                                                                         |
| Explanation | An issue has occurred during the analysis, during the processing a visitor. The analysis may be incomplete.     |
| User Action | Contact CAST Support.                                                                                           |

## DOTNET.0020

|                 |                                                                                                             |
|-----------------|-------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0020                                                                                                 |
| Message     | Error while processing visitor: " + astAnalyzer.GetType().Name + ". The analysis results may be incomplete. |
| Severity    | Warning                                                                                                     |
| Explanation | An error has occurred while processing visitor and the analysis results may be incomplete.                  |
| User Action | Contact CAST Support.                                                                                       |

## DOTNET.0019

|                 |                                                                                                                          |
|-----------------|--------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0019                                                                                                              |
| Message     | Ignored unknown directive \\" + directiveName + "\\ in " + \_fileName.                                                   |
| Severity    | Warning                                                                                                                  |
| Explanation | The directive listed in the asmx file is unknown by the .NET Analyzer therefore the analysis results will be incomplete. |
| User Action | Contact CAST Support.                                                                                                    |

## DOTNET.0018

|                 |                                                                                                               |
|-----------------|---------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0018                                                                                                   |
| Message     | WebService ignored : " + \_fileName.                                                                          |
| Severity    | Warning                                                                                                       |
| Explanation | The web service is ignored and will not be analyzed therefore the results of the analysis will be incomplete. |
| User Action | Contact CAST Support.                                                                                         |

## DOTNET.0017

|                 |                                                                                               |
|-----------------|-----------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0017                                                                                   |
| Message     | Source language " + WebServiceDirective.Language + " in " + \_fileName + " is not supported." |
| Severity    | Warning                                                                                       |
| Explanation | The language present in the file is not supported therefore it will not be analyzed.          |
| User Action | Contact CAST Support.                                                                         |

## DOTNET.0016

|                 |                                                                                                    |
|-----------------|----------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0016                                                                                        |
| Message     | WebService directive is missing in " + \_fileName.                                                 |
| Severity    | Warning                                                                                            |
| Explanation | In the asmx file, the web service directive is missing, therefore the analysis will be incomplete. |
| User Action | Please check that the web service directive is part of the file.                                   |

## DOTNET.0015

|                 |                                                                                                             |
|-----------------|-------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0015                                                                                                 |
| Message     | No code behind source code found for " + \_fileName.                                                        |
| Severity    | Warning                                                                                                     |
| Explanation | There is no code behind found to the file listed in the message.                                            |
| User Action | Please check if there is any code source behind for the listed file and if 'true' contact the CAST Support. |

## DOTNET.0014

|                 |                                                                                                                   |
|-----------------|-------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0014                                                                                                       |
| Message     | An error occurred when creating link for string "{interpreted string}": {exception information}.                  |
| Severity    | Warning                                                                                                           |
| Explanation | An issue has occurred when trying to create a link to the listed string. The analysis results will be incomplete. |
| User Action | Contact CAST Support.                                                                                             |

## DOTNET.0013

|                 |                                                                                                                                           |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0013                                                                                                                               |
| Message     | Failed to compile project {project file path}. The project will not be analyzed. {exception information}.                                 |
| Severity    | Warning                                                                                                                                   |
| Explanation | An issue has occurred and the project cannot be compiled therefore it cannot be analyzed.                                                 |
| User Action | Please check that the project compile in Visual Studio and add any missing element. If the problem persists, please contact CAST Support. |

## DOTNET.0012

|                 |                                                                                                                            |
|-----------------|----------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0012                                                                                                                |
| Message     | Could not load assembly {0}. No link will be saved toward its objects. (candidates are {1})", assemblyRef.Key, filePaths); |
| Severity    | Warning                                                                                                                    |
| Explanation | The assembly listed could not be loaded, therefore the links to this assembly cannot be created.                           |
| User Action | Check that the assembly is part of the resource package, and check packaging alerts.                                       |

## DOTNET.0011

|                 |                                                                                                                                                 |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0011                                                                                                                                     |
| Message     | Analysis of " + project.Path + " has failed. An unexpected error happened leaving the analysis in an unknown state.\r\n"                        |
| Severity    | Error                                                                                                                                           |
| Explanation | An expected error has happened during the analysis of the project leaving the project in an unexpected state. The analysis is an unknown state. |
| User Action | Contact CAST Support.                                                                                                                           |

## DOTNET.0010

|                 |                                                                                                                      |
|-----------------|----------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0010                                                                                                          |
| Message     | Could not compute GUID for: " + Symbol.Accept(AnalysisSharedObjects.Instance.ManglingVisitor), e);                   |
| Severity    | Error                                                                                                                |
| Explanation | The GUID cannot be calculated for the listed symbol. The symbol cannot be saved and the analysis will be incomplete. |
| User Action | Contact CAST support.                                                                                                |

## DOTNET.0009

|                 |                                                           |
|-----------------|-----------------------------------------------------------|
| Identifier  | DOTNET.0009                                               |
| Message     | Could not load project. The project will not be analyzed. |
| Severity    | Error                                                     |
| Explanation | The project will not be analyzed as it cannot be loaded.  |
| User Action | Contact CAST support.                                     |

## DOTNET.0008

|                 |                                                               |
|-----------------|---------------------------------------------------------------|
| Identifier  | DOTNET.0008                                                   |
| Message     | Could not load project {0}. The project will not be analyzed. |
| Severity    | Error                                                         |
| Explanation | The project will not be analyzed as it cannot be loaded.      |
| User Action | Contact CAST support.                                         |

## DOTNET.0007

|                 |                                                          |
|-----------------|----------------------------------------------------------|
| Identifier  | DOTNET.0007                                              |
| Message     | Unknown language. Couldn't load project.                 |
| Severity    | Error                                                    |
| Explanation | The project cannot be loaded as the language is unknown. |
| User Action | Contact CAST support.                                    |

## DOTNET.0006

|                 |                                                                                                                                                                                  |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0006                                                                                                                                                                      |
| Message     | Unmanaged database access kind. An error has occurred and related external links will not be saved.                                                                              |
| Severity    | Error                                                                                                                                                                            |
| Explanation | An error has occurred during the access to the database. The related external links will not be saved into the database, hence the results of the analysis will not be complete. |
| User Action | Contact CAST support.                                                                                                                                                            |

## DOTNET.0005

|                 |                                                                                                                                                                                  |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0005                                                                                                                                                                      |
| Message     | Unmanaged database access kind. An error has occurred and related external links will not be saved.                                                                              |
| Severity    | Error                                                                                                                                                                            |
| Explanation | An error has occurred during the access to the database. The related external links will not be saved into the database, hence the results of the analysis will not be complete. |
| User Action | Contact CAST support.                                                                                                                                                            |

## DOTNET.0004

|                 |                                                                                |
|-----------------|--------------------------------------------------------------------------------|
| Identifier  | DOTNET.0004                                                                    |
| Message     | Trying to save a symbol twice.                                                 |
| Severity    | Error                                                                          |
| Explanation | A .NET object is saved into the database twice, this is an abnormal situation. |
| User Action | Contact CAST support.                                                          |

## DOTNET.0003

|                 |                                                                      |
|-----------------|----------------------------------------------------------------------|
| Identifier  | DOTNET.0003                                                          |
| Message     | Unknown exception.                                                   |
| Severity    | Warning                                                              |
| Explanation | An exception has occurred, and the analysis has stopped incorrectly. |
| User Action | Contact CAST support.                                                |

## DOTNET.0002

|                 |                                                                                                |
|-----------------|------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0002                                                                                    |
| Message     | Analysis failure, could not load a type. The following assemblies could not be loaded as well. |
| Severity    | Error                                                                                          |
| Explanation | The analysis has not ended correctly, because the listed assemblies cannot be loaded.          |
| User Action | Check that the listed assemblies are correctly delivered.                                      |

## DOTNET.0001

|                 |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | DOTNET.0001                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| Message     | Missing mandatory reference towards mscorlib: project {0} will be skipped. The references to mscorlib are mandatory to the analysis of the .NET projects.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| Severity    | Fatal Error                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| Explanation | This is a missing mandatory reference towards mscorlib.dll therefore the project will be skipped.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| User Action | During the delivery of the source code, ensure that the framework assemblies are delivered and check that mscorlib is delivered as well. Note that if you are using ≥ 1.1 of the .NET Analyzer extension, CAST provides some specific frameworks and third party packages with the extension, which will automatically be "used" during an analysis - as such, no specific package is required for these. However, as it is not necessary to package the frameworks and third party packages used by your source code, missing library/assembly alerts for these items will be generated during the packaging action. The alerts can be safely ignored if the alert references an item that CAST provides in the extension. See the section Dependent frameworks and third-party packages provided in the extension documentation for more information and for a list of the specific frameworks and third party packages provided with the extension. |

## CS0433

|                 |                                                                                                                                                                                             |
|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | CS0433                                                                                                                                                                                      |
| Message     | The type TypeName1 exists in both TypeName2 and TypeName3.                                                                                                                                  |
| Severity    | Warning                                                                                                                                                                                     |
| Explanation | Two different assemblies referenced in your application contain the same namespace and type, which produces ambiguity. The impact is, the analysis results could have some false negatives. |
| User Action | None currently.                                                                                                                                                                             |

## CS0234

|                 |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | CS0234                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| Message     | The type or namespace name 'XXXXX' does not exist in the namespace 'Services' (are you missing an assembly reference?).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Severity    | Warning                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| Explanation | A dependent project or a third party assembly was not provided within the perimeter of the application, and any reference to these items will cause this message. The immediate impact is that the analysis results will be missing some missing links to external objects. In the case where these external objects hold important semantics (presentation layer including exposed REST or SOAP API, persistence layer, logging library,...) the impact on results will be more important.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| User Action | Ensure that all required source code is packaged and delivered. If you are using release ≥ 1.1 of the .NET Analyzer extension, CAST provides some specific frameworks and third party packages with the extension, which will automatically be "used" during an analysis - as such, no specific package is required for these assemblies. However, as it is not necessary to package the frameworks and third party packages used by your source code, missing library/assembly alerts for these items will be generated during the packaging action. The alerts can be safely ignored if the alert references an item that CAST provides in the extension. See the section Dependent frameworks and third-party packages provided in the extension documentation for more information and for a list of the specific frameworks and third party packages provided with the extension. Since release ≥ 1.2.4 of the .NET Analyzer extension, this message is available only in debug mode. In normal mode, multiple occurrences of CS0234 are replaced by a single DOTNET.150 message for one symbol per project. |

## BC30560

|                 |                                                                                                                                                                                                |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | BC30560                                                                                                                                                                                        |
| Message     | '`<name>`' is ambiguous in the namespace '`<namespacename>`'.                                                                                                                                  |
| Severity    | Error                                                                                                                                                                                          |
| Explanation | Compiler's Error Message when code statement provides a name that is ambiguous and therefore conflicts with another name. The impact is, the analysis results could have some false negatives. |
| User Action | None currently.                                                                                                                                                                                |

## BC30002

|                 |                                                                                                                                                                                                                                  |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Identifier  | BC30002                                                                                                                                                                                                                          |
| Message     | Type '`<typename>`' is not defined.                                                                                                                                                                                              |
| Severity    | Error                                                                                                                                                                                                                            |
| Explanation | Compiler's Error Message when code statement makes reference to a type (Class, Enum, Structure or Interface) that has not been defined because of missing source. The impact is the analysis results will be missing some links. |
| User Action | Ensure that all required source code is packaged and delivered.                                                                                                                                                                  |
