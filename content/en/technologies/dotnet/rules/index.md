---
title: "Structural rules"
linkTitle: "Structural rules"
type: "docs"
weight: 8
---

| Technology | Link |
|---------|---------|
|C#   | [https://technologies.castsoftware.com/rules?sec=t_138383&ref=&#124;&#124;](https://technologies.castsoftware.com/rules?sec=t_138383&ref=&#124;&#124;) |  
|VB.NET   |	[https://technologies.castsoftware.com/rules?sec=t_138635&ref=&#124;&#124;](https://technologies.castsoftware.com/rules?sec=t_138635&ref=&#124;&#124;) |
|ASP.NET   | [https://technologies.castsoftware.com/rules?sec=t_138636&ref=&#124;&#124;](https://technologies.castsoftware.com/rules?sec=t_138636&ref=&#124;&#124;)  |

>Note that some rules for .NET are provided in the default CAST Assessment Model shipped with CAST AIP and installed automatically with new CAST AIP schemas. The following extensions also provide .NET specific rules:
>- [.NET Analyzer](../extensions/com.castsoftware.dotnet/)
>- [NoSQL for .NET](../../nosql/extensions/com.castsoftware.nosqldotnet/)
>- [ASP.NET Web API Framework and Security Rules](../extensions/com.castsoftware.dotnetweb/)

