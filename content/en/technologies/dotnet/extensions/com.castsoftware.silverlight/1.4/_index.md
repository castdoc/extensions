---
title: "Silverlight - 1.4"
linkTitle: "1.4"
type: "docs"
no_list: true
---

***

>The Silverlight extension is deprecated and no new development will
be actioned. All new features and functionality for the support of
Silverlight are now handled instead by the [.NET XAML](../../com.castsoftware.wpf/)
extension. Please see the section of documentation entitled **Existing
extensions for WPF and
Silverlight** in [.NET XAML](../../com.castsoftware.wpf/)
for more information about the .NET XAML extension and the impacts of
moving to it.

## Extension ID

com.castsoftware.silverlight

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Silverlight. The calculation of
Automated Function Points for your .NET analyses will be supplemented
through the creation of new objects and links specific to the
Silverlight framework that will link back to objects/links produced by
the base .NET analyzer.

### In what situation should you install this extension?

If your .NET application contains Silverlight source code and you want
to view these object types and their links, then you should install this
extension.

## Silverlight framework support

The following Silverlight frameworks are supported by this extension:

<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh"><div>
<div>
Version
</div>
</div></th>
<th class="confluenceTh" style="text-align: center;"><div>
<div>
Supported
</div>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd">All versions up to 5.0</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
</tbody>
</table>

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Console. If you need to use it, should manually install the
extension using the **Application -
Extensions** interface:

![](../images/462880852.jpg)

### CAST Transaction Configuration Center (TCC) Entry Points

In Silverlight ≥ 1.2.x, if you are using the extension with CAST
AIP ≥ 8.3.x, a set of Silverlight specific Transaction Entry
Points are now automatically imported when the extension is
installed. These Transaction Entry Points will be available in the
CAST Transaction Configuration Center:

![](../images/382625259.jpg)

## What results can you expect?

### Objects

All objects are represented under the File browser  \> Xaml Source file
folders in CAST Enlighten:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Object type</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/382625270.png" /></p>
</div></td>
<td class="confluenceTd">Silverlight XAML Control</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/382625271.png" /></p>
</div></td>
<td class="confluenceTd">Silverlight XAML Custom Control</td>
</tr>
</tbody>
</table>

Note that if the Name Attribute of an object is present, then it
will be used to display that object in CAST Enlighten. For example:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Name not present</th>
<th class="confluenceTh">Name present</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/382625273.jpg" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/382625272.jpg" /></p>
</div></td>
</tr>
</tbody>
</table>

The following objects are detected:

|                          |
|--------------------------|
| Border                   |
| BulletDecorator          |
| Button                   |
| Calendar                 |
| Canvas                   |
| CheckBox                 |
| ComboBox                 |
| ContextMenu              |
| DataGrid                 |
| DatePicker               |
| DockPanel                |
| DocumentViewer           |
| DropShadowBitmapEffect   |
| Expander                 |
| FlowDocumentPageViewer   |
| FlowDocumentReader       |
| FlowDocumentScrollViewer |
| Frame                    |
| Grid                     |
| GridSplitter             |
| GroupBox                 |
| HyperlinkButton          |
| Image                    |
| Label                    |
| ListBox                  |
| ListView                 |
| Menu                     |
| Panel                    |
| PasswordBox              |
| Popup                    |
| ProgressBar              |
| PrintDialog              |
| RadioButton              |
| RepeatButton             |
| Ribbon                   |
| RichTextBox              |
| ScrollBar                |
| ScrollViewer             |
| Separator                |
| Slider                   |
| StackPanel               |
| StatusBar                |
| TabControl               |
| TextBlock                |
| TextBox                  |
| ToolBar                  |
| ToolTip                  |
| TreeView                 |
| UserControl              |
| WrapPanel                |
| Viewbox                  |

### Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Source</th>
<th class="confluenceTh">Link type</th>
<th class="confluenceTh">Target</th>
<th class="confluenceTh">Example</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">XAML Control</td>
<td class="confluenceTd">call</td>
<td class="confluenceTd">Action Event method implemented in source
file</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/382625264.jpg" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">XAML Control</td>
<td class="confluenceTd">relyOn</td>
<td class="confluenceTd">Set and get accessors of Property implemented
in source file</td>
<td class="confluenceTd"><div class="content-wrapper">
<img src="../images/382625258.jpg" />
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">XAML Control</td>
<td class="confluenceTd">relyOn</td>
<td class="confluenceTd">XAML Control which is used by another XAML
Control</td>
<td class="confluenceTd"><div class="content-wrapper">
<img src="../images/382625262.jpg"/>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">XAML Control</td>
<td class="confluenceTd">call</td>
<td class="confluenceTd">Binding property in DataContext</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd">XAML Source File</td>
<td class="confluenceTd">relyOn</td>
<td class="confluenceTd">Referred Class</td>
<td class="confluenceTd"><div class="content-wrapper">
<img src="../images/382625265.jpg" />
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">C# property (relay command)</td>
<td class="confluenceTd">relyOn</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>C# method (delegate</p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/382625260.png" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd">XAML Control</td>
<td class="confluenceTd">call</td>
<td class="confluenceTd">C# property (relay command)</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/382625263.png" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd">XAML Source File</td>
<td class="confluenceTd">relyOn</td>
<td class="confluenceTd">Datacontext class</td>
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/382625269.jpg" /></p>
</div></td>
</tr>
</tbody>
</table>

### Limitations

In this section we list the most significant functional limitations that
may affect the analysis of applications using Silverlight:

-   Binding in code is not supported - see
    [https://msdn.microsoft.com/en-us/library/ms742863(v=vs.110).aspx?cs-save-lang=1&cs-lang=csharp#code-snippet-1](https://msdn.microsoft.com/en-us/library/ms742863%28v=vs.110%29.aspx?cs-save-lang=1&cs-lang=csharp#code-snippet-1)
    for an example
-   Binding links are not handled correctly when the same binding
    target exists in different classes
-   The direction of the data flow (Binding.Mode properties such as
    OneWay, TwoWay and OneWayToSource) is not supported - see
    [https://msdn.microsoft.com/en-us/library/ms752347(v=vs.110).aspx#direction_of_data_flow](https://msdn.microsoft.com/en-us/library/ms752347%28v=vs.110%29.aspx#direction_of_data_flow)
    for an example
-   The RelativeSource property (Gets or sets the binding source by
    specifying its location relative to the position of the binding
    target) is not supported - see
    https://msdn.microsoft.com/en-us/library/system.windows.data.binding.relativesource(v=vs.110).aspx
    for an example
-   Some XAML control objects which are visible in CAST Enlighten may
    not be seen in the CAST Application Engineering Dashboard.
-   Links between XAML Control objects and code embedded in XAML are not
    supported.
