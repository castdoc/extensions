---
title: "RabbitMQ for .NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.rabbitmq

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for RabbitMQ for .NET APIs which are
responsible for publishing and receiving messages across messaging
queues.

## In what situation should you install this extension?

If your C# application utilizes RabbitMQ for messaging and you want
to view a model that shows the message publisher and receiver objects
with their corresponding links, then you should install this extension.

## Technology Libraries

| Library | Version | Supported |
|---|---|:-:|
| RabbitMQ.Client      | upto 6.5 | :white_check_mark: |
| EasyNetQ             | upto 7.5 | :white_check_mark: |
| MassTransit.RabbitMQ | upto 8.1 | :white_check_mark: |
| CAP.RabbitMQ         | upto 6.0 | :white_check_mark: |
| NServiceBus.RabbitMQ | upto 8.0 | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies

The RabbitMQ extension is dependent on following extensions to provide
fully functional analysis:

-   CAST AIP Internal extension
-   Web Services Linker

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, you should manually install the
extension:

![](../images/670630259.png)

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/670630258.png) | RabbitMQ DotNet Publisher |
| ![](../images/670630256.png) | RabbitMQ DotNet Consumer |
| ![](../images/670630255.png) | RabbitMQ Unknown DotNet Publisher |
| ![](../images/670630254.png) | RabbitMQ Unknown DotNet Consumer |

#### Config Objects

|  Icon | Description |              Supported APIs |
|---|---|---|
| ![](../images/670630253.png) | RabbitMQ DotNet ExchangeDeclaration | <details><summary>RabbitMQ.Client Exchange Creation APIs</summary>RabbitMQ.Client.IModel.ExchangeDeclare<br>RabbitMQ.Client.IModel.ExchangeDeclareNoWait<br>RabbitMQ.Client.IModelExensions.ExchangeDeclare<br>RabbitMQ.Client.IModelExensions.ExchangeDeclareNoWait</details><details><summary>EasyNetQ Exchange Creation APIs </summary>EasyNetQ.IAdvancedBus.ExchangeDeclare<br>EasyNetQ.IAdvancedBus.ExchangeDeclareAsync<br>EasyNetQ.Producer.IExchangeDeclareStrategy.DeclareExchangeAsync<br>EasyNetQ.Producer.IPublishExchangeDeclareStrategy.DeclareExchangeAsync</details><details><summary>MassTransit.RabbitMQ Exchange Creation APIs</summary>MassTransit.IRabbitMqBusFactoryConfigurator.Publish</details><details><summary>NServiceBus.RabbitMQ Exchange Creation APIs</summary>NServiceBus.EndpointConfiguration.EndpointConfiguration</details> |
| ![](../images/670630253.png)  | RabbitMQ DotNet QueueBind | <details><summary>RabbitMQ.Client QueueBind APIs</summary>RabbitMQ.Client.IModel.QueueBind<br>RabbitMQ.Client.IModel.QueueBindNoWait<br>RabbitMQ.Client.IModelExensions.QueueBind<br>RabbitMQ.Client.IModelExensions.QueueBindNoWait</details><details><summary>EasyNetQ Bind APIs </summary>EasyNetQ.IAdvancedBus.Bind<br>EasyNetQ.IAdvancedBus.BindAsync</details><details><summary>MassTransit.RabbitMQ Bind APIs</summary>MassTransit.IRabbitMqReceiveEndpointConfigurator.Bind</details>|
| ![](../images/670630253.png) | RabbitMQ DotNet ExchangeBind | <details><summary>RabbitMQ.Client ExchangeBind APIs</summary>RabbitMQ.Client.IModel.ExchangeBind<br>RabbitMQ.Client.IModel.ExchangeBindNoWait<br>RabbitMQ.Client.IModelExensions.ExchangeBind<br>RabbitMQ.Client.IModelExensions.ExchangeBindNoWait</details><details><summary>EasyNetQ Bind APIs </summary>EasyNetQ.IAdvancedBus.Bind<br>EasyNetQ.IAdvancedBus.BindAsync</details><details><summary>MassTransit.RabbitMQ Bind APIs</summary>MassTransit.IRabbitMqReceiveEndpointConfigurator.Bind </details>|

### Link Types

| Link Type | Source and Destination Link | Supported APIs |
|---|---|---|
| callLink | Link between the caller C# method and the DotNet RabbitMQ Publisher object | <details><summary>RabbitMQ Publisher APIs</summary>RabbitMQ.Client.IModel.BasicPublish<br>RabbitMQ.Client.IModelExensions.BasicPublish</details><details><summary>EasyNetQ Publisher APIs</summary>EasyNetQ.IAdvancedBus.Publish<br>EasyNetQ.IAdvancedBus.PublishAsync<br>EasyNetQ.IBus.Publish<br>EasyNetQ.IBus.PublishAsync<br>EasyNetQ.IBus.Send<br>EasyNetQ.IBus.SendAsync<br>EasyNetQ.IBus.Request<br>EasyNetQ.IBus.RequestAsync<br>EasyNetQ.IPubSub.PublishAsync<br>EasyNetQ.IRpc.RequestAsync<br>EasyNetQ.ISendReceive.SendAsync<br>EasyNetQ.IScheduler.FuturePublishAsync<br>EasyNetQ.Scheduling.IScheduler.FuturePublishAsync<br>EasyNetQ.Scheduling.IScheduler.FuturePublish<br>EasyNetQ.SchedulerExtensions.FuturePublish<br>EasyNetQ.SchedulerExtensions.FuturePublishAsync<br>EasyNetQ.NonGenericSchedulerExtensions.FuturePublish<br>EasyNetQ.NonGenericSchedulerExtensions.FuturePublishAsync</details><details><summary>MassTransit.RabbitMQ Publisher APIs</summary>MassTransit.IPublishEndpoint.Publish<br>MassTransit.PublishExecuteExtensions.Publish<br>MassTransit.ISendEndpoint.Send</details><details><summary>CAP.RabbitMQ Publisher APIs</summary>DotNetCore.CAP.ICapPublisher.Publish<br>DotNetCore.CAP.ICapPublisher.PublishDelayAsync<br>DotNetCore.CAP.ICapPublisher.PublishAsync<br>DotNetCore.CAP.ICapPublisher.PublishDelay</details><details><summary>NServiceBus.RabbitMQ Publisher APIs</summary>NServiceBus.IMessageSession.Send<br>NServiceBus.MessageSessionExtensions.Send<br>NServiceBus.MessageSessionExtensions.Publish<br>NServiceBus.IMessageSession.Publish<br>NServiceBus.MessageSessionExtensions.SendLocal<br>NServiceBus.IMessageSession.SendLocal</details> |
| callLink | Link between the DotNet RabbitMQ Consumer object and the caller C# method  | <details><summary>RabbitMQ Consumer APIs</summary>RabbitMQ.Client.IModel.BasicConsume<br>RabbitMQ.Client.IModel.BasicGet<br>RabbitMQ.Client.IModelExensions.BasicConsume</details><details><summary>EasyNetQ Consumer APIs</summary>EasyNetQ.IAdvancedBus.Get<br>EasyNetQ.IAdvancedBus.Consume<br>EasyNetQ.IBus.Subscribe<br>EasyNetQ.IBus.SubscribeAsync<br>EasyNetQ.IBus.Receive<br>EasyNetQ.IBus.Respond<br>EasyNetQ.IBus.RespondAsync<br>EasyNetQ.IPubSub.SubscribeAsync<br>EasyNetQ.IRpc.RespondAsync<br>EasyNetQ.ISendReceive.ReceiveAsync<br>EasyNetQ.AutoSubscribe.IConsumeAsync.ConsumeAsync<br>EasyNetQ.AutoSubscribe.IConsumeAsync.Consume<br>EasyNetQ.AutoSubscribe.IConsume.Consume</details><details><summary>MassTransit.RabbitMQ Consumer APIs</summary>MassTransit.IReceiveConfigurator.ReceiveEndpoint<br>MassTransit.IRegistrationConfigurator.AddConsumer</details><details><summary>CAP.RabbitMQ Consumer APIs</summary>DotNetCore.CAP.CapSubscribeAttribute.CapSubscribeAttribute</details><details><summary>NServiceBus.RabbitMQ Consumer APIs</summary>NServiceBus.IHandleMessages </details>|

## Code Examples

### RabbitMQ.Client

#### Publisher

``` c#
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace Routing
{
    class Subs
    {
  
    static void Main()
    {
        var factory = new ConnectionFactory { HostName = "localhost" };
        using var connection = factory.CreateConnection();
        using var channel = connection.CreateModel();
  
        var queueName1 = channel.QueueDeclare("queueName1").QueueName;
    var queueName2 = channel.QueueDeclare("queueName2").QueueName;

    ExchangeInit(channel);
  
        
    ReceiveMessage("queueName1", channel);
    ReceiveMessage("queueName2", channel);
    SendMEssage("HelloWOrld", "test2", channel);
  
        
}

private static void SendMEssage(string message, string routingkey, IModel channel)
{

var body = Encoding.UTF8.GetBytes(message);
  channel.BasicPublish(exchange: "Sep",
                            routingKey: routingkey,
                            basicProperties: null,
                            body: body);

}
```

![](../images/670630252.png)

![](../images/670630250.png)

#### Unknown Publisher

``` c#
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace Routing
{
    class Subs
    {
  
    static void Main()
    {
        var factory = new ConnectionFactory { HostName = "localhost" };
        using var connection = factory.CreateConnection();
        using var channel = connection.CreateModel();
  
        var queueName1 = channel.QueueDeclare("queueName1").QueueName;
    var queueName2 = channel.QueueDeclare("queueName2").QueueName;
 
        channel.ExchangeDeclare(exchange: "Sep", type: "fanout", true);
  
        channel.ExchangeDeclare(exchange: "Oct", type: "direct", true);
  
        channel.ExchangeBind("Oct", "Sep", "");
  
  
        var body = Encoding.UTF8.GetBytes("Hello World");
 
            channel.BasicPublish(exchange: unknown_exchange_name,
                            routingKey: "test2",
                            basicProperties: channel.CreateBasicProperties(),
                            body: body);
  
            Console.WriteLine(" [x] Sent {0}", "Hello World");
        Console.WriteLine(" Press [enter] to exit.");
                       Console.ReadLine();
  
  
      
}
}
}
```

![](../images/670630249.png)
![](../images/670630248.png)

#### Consumer

``` c#
using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Consumer
{
    internal class Receive
    {
        const string QueueName = "task_queue3";

        static void Main()
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
            };

            try
            {
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {

                        channel.QueueDeclare(queue: QueueName,
                            durable: true,
                            exclusive: false,
                            autoDelete: false,
                            arguments: null);

                        channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

                        var channel1 = channel;
                        var consumer = new EventingBasicConsumer(channel);
                        consumer.Received += (model, ea) =>
                        {
                            var body = ea.Body;
                            var message = Encoding.UTF8.GetString(body);
                            Console.WriteLine(" [x] Received {0}", message);

                            var dots = message.Split('.').Length - 1;
                            Thread.Sleep(dots * 1000);

                            Console.WriteLine(" [x] Done");

                            // Message acknowledgment
                            channel1.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                        };

   
                        channel.BasicConsume(queue: QueueName,
                            autoAck: false,
                            consumer: consumer);

                        channel.BasicConsume(queue: Unknown_QueueName,
                            autoAck: false,
                            consumer: consumer);

                        Console.WriteLine(" Press [enter] to exit.");
                        Console.ReadLine();
                    }
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }
        }
    }
}

```

![](../images/rabbit_consumer.png)

##### Unknown Consumer

![](../images/rabbit_unknown_consumer.png)

#### ExchangeDeclare

``` c#
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace Routing
{
    class Subs
    {
  
    static void Main()
    {
        var factory = new ConnectionFactory { HostName = "localhost" };
        using var connection = factory.CreateConnection();
        using var channel = connection.CreateModel();
  
        var queueName1 = channel.QueueDeclare("queueName1").QueueName;
    var queueName2 = channel.QueueDeclare("queueName2").QueueName;

    ExchangeInit(channel);
        
}

private static void ExchangeInit(IModel channel)
{
    channel.ExchangeDeclare(exchange: "Sep", type: "fanout", true);
  
    channel.ExchangeDeclare(exchange: "Oct", type: "direct", true);
  
    channel.ExchangeBind("Oct", "Sep", "");

}
```

![](../images/670630243.png)
![](../images/670630241.png)

#### QueueBind

``` c#
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace Routing
{
    class Subs
    {
  
    static void Main()
    {
        var factory = new ConnectionFactory { HostName = "localhost" };
        using var connection = factory.CreateConnection();
        using var channel = connection.CreateModel();
  
        var queueName1 = channel.QueueDeclare("queueName1").QueueName;
    var queueName2 = channel.QueueDeclare("queueName2").QueueName;

    ExchangeInit(channel);
  
    channel.QueueBind(queue: "queueName1" ,
                  exchange: "Oct",
                  routingKey: "test2");
 
        channel.QueueBind(queue: "queueName2" ,
                  exchange: "Sep",
                   routingKey: "");
  
  
        Console.WriteLine(" [*] Waiting for logs.");
  
        
    ReceiveMessage("queueName1", channel);
    ReceiveMessage("queueName2", channel);
    SendMEssage("HelloWOrld", "test2", channel);
  
  
      
}
```
![](../images/670630240.png)
![](../images/670630238.png)

#### ExchangeBind

``` c#
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace Routing
{
    class Subs
    {
  
    static void Main()
    {
        var factory = new ConnectionFactory { HostName = "localhost" };
        using var connection = factory.CreateConnection();
        using var channel = connection.CreateModel();
  
        var queueName1 = channel.QueueDeclare("queueName1").QueueName;
    var queueName2 = channel.QueueDeclare("queueName2").QueueName;

    ExchangeInit(channel);
   
}

private static void ExchangeInit(IModel channel)
{
    channel.ExchangeDeclare(exchange: "Sep", type: "fanout", true);
  
    channel.ExchangeDeclare(exchange: "Oct", type: "direct", true);
  
    channel.ExchangeBind("Oct", "Sep", "");

}
```
![](../images/670630237.png)
![](../images/670630235.png)

### EasyNetQ

#### IBus

##### Publish

``` c#
using System;
using EasyNetQ;
using EasyNetQMessages;
using EasyNetQ.SystemMessages;
using System.Configuration;

namespace Publish
{
    class Program
    {
        public static void Main(string[] args)
        {
            
            var payment1 = new CardPaymentRequestMessage
            {
                CardNumber = "415411585",
                CardHolderName = "Mr F Bloggs",
                ExpiryDate = "12/12",
                Amount = 99.00m
            };

            var payment2 = new CardPaymentRequestMessage
            {
                CardNumber = "3456345634563456",
                CardHolderName = "Mr S Claws",
                ExpiryDate = "03/11",
                Amount = 15.00m
            };

            var payment3 = new CardPaymentRequestMessage
            {
                CardNumber = "6789678967896789",
                CardHolderName = "Mrs E Curry",
                ExpiryDate = "01/03",
                Amount = 1250.24m
            };

            var payment4 = new CardPaymentRequestMessage
            {
                CardNumber = "9991999299939994",
                CardHolderName = "Mrs D Parton",
                ExpiryDate = "04/07",
                Amount = 34.87m
            };

            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                Console.WriteLine("Publishing messages with publish and subscribe.");
                Console.WriteLine((new EasyNetQ.SystemMessages.Error()).Queue);
                Console.WriteLine();

                bus.Publish(payment1);
                bus.Publish(payment2);
                bus.Publish(payment3);
                bus.Publish(payment4);
               
            }
        }
    }
}

```
![](../images/easy_netq_publish.png)

##### Subscribe

``` c#
using System;
using EasyNetQ;
using EasyNetQMessages;

namespace Subscribe
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                bus.Subscribe<CardPaymentRequestMessage>("cardPayment", HandleCardPaymentMessage);

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }

        static void HandleCardPaymentMessage(CardPaymentRequestMessage paymentMessage)
        {
            Console.WriteLine("Payment = <" +
                              paymentMessage.CardNumber + ", " +
                              paymentMessage.CardHolderName + ", " +
                              paymentMessage.ExpiryDate + ", " +
                              paymentMessage.Amount + ">");
        }
    }
}
```

![](../images/easy_netq_subs.png)

##### Send

``` c#
using System;
using EasyNetQ;
using EasyNetQMessages;

namespace Send
{
    class Program
    {
        public static void Main(string[] args)
        {
            var payment1 = new CardPaymentRequestMessage
            {
                CardNumber = "1234123412341234",
                CardHolderName = "Mr F Bloggs",
                ExpiryDate = "12/12",
                Amount = 99.00m
            };


            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                Console.WriteLine("Publishing messages with send and receive.");
                Console.WriteLine();

                bus.Send("my.paymentsqueue", payment1);
                bus.Send("my.paymentsqueue", purchaseOrder1);
                bus.Send("my.paymentsqueue", payment2);
                bus.Send("my.paymentsqueue", payment3);
                bus.Send("my.paymentsqueue", purchaseOrder2);
                bus.Send("my.paymentsqueue", payment4);
            }
          
        }
    }
}

```

![](../images/easy_netq_send.png)

##### Receive

``` c#
using System;
using EasyNetQ;
using EasyNetQMessages;

namespace Receive
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                bus.Receive<CardPaymentRequestMessage>("my.paymentsqueue", message => HandleCardPaymentMessage(message));

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }

        static void HandleCardPaymentMessage(CardPaymentRequestMessage paymentMessage)
        {
            Console.WriteLine("Processing Payment = <" +
                              paymentMessage.CardNumber + ", " +
                              paymentMessage.CardHolderName + ", " +
                              paymentMessage.ExpiryDate + ", " +
                              paymentMessage.Amount + ">");
        }
    }
}


```

![](../images/easy_netq_receive.png)

##### Request

``` c#
using System;
using EasyNetQ;
using EasyNetQMessages;

namespace RequestAsync
{
    class Program
    {
        static void Main(string[] args)
        {
            var payment = new CardPaymentRequestMessage
            {
                CardNumber = "1234123412341234",
                CardHolderName = "Mr F Bloggs",
                ExpiryDate = "12/12",
                Amount = 99.00m
            };

            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                Console.WriteLine("Publishing messages with request and response.");
                Console.WriteLine();

                var task = bus.RequestAsync<CardPaymentRequestMessage, CardPaymentResponseMessage>(payment);

                task.ContinueWith(response => {
                    Console.WriteLine("Got response: '{0}'", response.Result.AuthCode);
                });

                Console.ReadLine();
            }
        }
    }
}

```

![](../images/easy_netq_request.png)

##### Respond

``` c#
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using EasyNetQ;
using EasyNetQMessages;

namespace ResponseAsync
{
    class Program
    {
        public  class MyWorker
        {
            public CardPaymentResponseMessage Execute(CardPaymentRequestMessage request)
            {
                CardPaymentResponseMessage responseMessage = new CardPaymentResponseMessage();
                responseMessage.AuthCode = "1234";
                Console.WriteLine("Worker activated to process response.");

                return responseMessage;
            }
        }

        static void Main(string[] args)
        {
            // Create a group of worker objects
            var workers = new BlockingCollection<MyWorker>();
            for (int i = 0; i < 10; i++)
            {
                workers.Add(new MyWorker());
            }

            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                // Respond to requests
                bus.RespondAsync<CardPaymentRequestMessage, CardPaymentResponseMessage>(request =>
                    Task.Factory.StartNew(() =>
                    {
                        var worker = workers.Take();
                        try
                        {
                            return worker.Execute(request);
                        }
                        finally
                        {
                            workers.Add(worker);
                        }
                    }));

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }
    }
}

```
![](../images/easy_netq_respond.png)


#### IAdvancedBus

##### Publisher

``` c#
using System;
using System.Text;
using EasyNetQ;
using EasyNetQMessages;
using EasyNetQ.Topology;

namespace Publish
{
    class Program
    {
        public static void Main(string[] args)
        {
           

            using (var advancedBus = RabbitHutch.CreateBus("host=localhost").Advanced)
            {   
        var exchange = advancedBus.ExchangeDeclare("my.exchange", ExchangeType.Topic);
        var queue = advancedBus.QueueDeclare("my.queue");
            
            var binding = advancedBus.Bind(exchange, queue, "A.*");

                Console.WriteLine("Publishing messages with publish and subscribe.");
                Console.WriteLine();
        var properties = new MessageProperties();
            var body = Encoding.UTF8.GetBytes("Hello World!");
                advancedBus.Publish(exchange, "A.AA", false, properties, body);

            }
        }



    }
}
```
![](../images/easy_netq_adv_publish.png)


##### Unknown Publisher

``` c#
using System;
using System.Text;
using EasyNetQ;
using EasyNetQMessages;
using EasyNetQ.Topology;

namespace Publish
{
    class Program
    {
        public static void Main(string[] args)
        {
           

            using (var advancedBus = RabbitHutch.CreateBus("host=localhost").Advanced)
            {   
        var exchange = advancedBus.ExchangeDeclare("my.exchange", ExchangeType.Topic);
        var queue = advancedBus.QueueDeclare("my.queue");
            
            var binding = advancedBus.Bind(exchange, queue, "A.*");

                Console.WriteLine("Publishing messages with publish and subscribe.");
                Console.WriteLine();
        var properties = new MessageProperties();
            var body = Encoding.UTF8.GetBytes("Hello World!");
                advancedBus.Publish(unknwon_exchange, "A.AA", false, properties, body);

            }
        }



    }
}

```

![](../images/670630212.png)

![](../images/670630211.png)

##### Consumer

``` c#
using System;
using System.Text;
using EasyNetQ;
using EasyNetQMessages;
using EasyNetQ.Topology;

namespace Subscribe
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var advancedBus = RabbitHutch.CreateBus("host=localhost").Advanced)
            {   
                    
                 var queue = advancedBus.QueueDeclare("my.queue");

        advancedBus.Consume(queue, (body, properties, info) => Task.Factory.StartNew(() =>
                  {
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine("Got message: '{0}'", message);
                     }));
    
                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }


    }
}
```

![](../images/easy_netq_consume.png)


##### Unknown Consumer

``` c#
using System;
using System.Text;
using EasyNetQ;
using EasyNetQMessages;
using EasyNetQ.Topology;

namespace Subscribe
{
    class Program
    {
        public static void Main(string[] args)
        {
            using (var advancedBus = RabbitHutch.CreateBus("host=localhost").Advanced)
            {   
                    
                 var queue = advancedBus.QueueDeclare("my.queue");

        advancedBus.Consume(unknown_queue, (body, properties, info) => Task.Factory.StartNew(() =>
                  {
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine("Got message: '{0}'", message);
                     }));
    
                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }


    }
}
```

![](../images/easy_netq_unknown.png)

##### ExchangeDeclare

``` c#
using System;
using System.Text;
using EasyNetQ;
using EasyNetQMessages;
using EasyNetQ.Topology;

namespace Publish
{
    class Program
    {
        public static void Main(string[] args)
        {
           

            using (var advancedBus = RabbitHutch.CreateBus("host=localhost").Advanced)
            {   
        var exchange = advancedBus.ExchangeDeclare("my.exchange", ExchangeType.Topic);
        var queue = advancedBus.QueueDeclare("my.queue");
            
            var binding = advancedBus.Bind(exchange, queue, "A.*");

                Console.WriteLine("Publishing messages with publish and subscribe.");
                Console.WriteLine();
        var properties = new MessageProperties();
            var body = Encoding.UTF8.GetBytes("Hello World!");
                advancedBus.Publish(exchange, "A.AA", false, properties, body);

            }
        }



    }
}
```

![](../images/670630205.png)

![](../images/670630204.png)

![](../images/670630203.png)

##### Bind

``` c#
using System;
using System.Text;
using EasyNetQ;
using EasyNetQMessages;
using EasyNetQ.Topology;

namespace Publish
{
    class Program
    {
        public static void Main(string[] args)
        {
           

            using (var advancedBus = RabbitHutch.CreateBus("host=localhost").Advanced)
            {   
        var exchange = advancedBus.ExchangeDeclare("my.exchange", ExchangeType.Topic);
        var queue = advancedBus.QueueDeclare("my.queue");
            
            var binding = advancedBus.Bind(exchange, queue, "A.*");

                Console.WriteLine("Publishing messages with publish and subscribe.");
                Console.WriteLine();
        var properties = new MessageProperties();
            var body = Encoding.UTF8.GetBytes("Hello World!");
                advancedBus.Publish(exchange, "A.AA", false, properties, body);

            }
        }



    }
}
```

![](../images/670630202.png)

![](../images/670630201.png)

![](../images/670630200.png)

### MassTransit.RabbitMQ

#### Publisher

``` c#
class Program
{
    static async Task Main()
    {
        var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
        {
            cfg.Host(new Uri("rabbitmq://localhost"), h => 
            {
                h.Username("guest");
                h.Password("guest");
            });

        });

        await busControl.StartAsync();
        try
        {
            do
            {   var endpoint = await busControl.GetSendEndpoint(new Uri("rabbitmq://localhost/your_queue_name"));

                endpoint.Send(new MyMessage { Text = "Hello MyMessage, World from Send!" });
                busControl.Publish(new MyMessage { Text = "Hello MyMessage, World from Publish!" }, x=>x.SetRoutingKey("high"));

                Console.WriteLine("Press [q] to exit");
            } while (Console.ReadKey().KeyChar != 'q');
        }
        finally
        {
            await busControl.StopAsync();
        }


    }


}
```
##### Publish

![](../images/masstransit_pub.png)

##### Send
![](../images/mass_send.png)


#### Consumer

``` c#
class Program
{
    static async Task Main()
    {
        var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
        {
            cfg.Host(new Uri("rabbitmq://localhost"), h => 
            {
                h.Username("guest");
                h.Password("guest");
            });

           cfg.Publish<MyMessage>(x => x.ExchangeType = "direct");

            cfg.ReceiveEndpoint("end_point", e =>
            {
                e.Consumer<MyMessageConsumer>();

                e.Bind<MyMessage>(s => 
                {
                    s.RoutingKey = "high";
                    s.ExchangeType = "direct";
                });

            });

    });

}
}

```
##### ReceiveEndpoint

![](../images/mass_endpoint.png)
##### Bind
![](../images/mass_bind.png)

##### AddConsumer
```c#
using MassTransit;

namespace Microservice.TodoApp.Consumers
{
    public class TodoConsumer : IConsumer<Todo>
    {
        public async Task Consume(ConsumeContext<Todo> context)
        {
            var data = context.Message;

        }
    }
}
```
``` c#
namespace Microservice.TodoApp.Consumers
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMassTransit(x =>
            {
                x.AddConsumer<TodoConsumer>();
                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host(new Uri(RabbitMqConsts.RabbitMqRootUri), h =>
                    {
                        h.Username(RabbitMqConsts.UserName);
                        h.Password(RabbitMqConsts.Password);
                    });
                    
                }));
            });
            services.AddMassTransitHostedService();
        }
    }
}

```
![](../images/mass_addconsumer.png)


#### Unknown Publihser

``` c#
using System;
using System.Threading.Tasks;
using MassTransit;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
namespace MassTransitRabbitMQDemo;

class Program
{
    static async Task Main()
    {
        var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
        {
            cfg.Host(new Uri("rabbitmq://localhost"), h => 
            {
                h.Username("guest");
                h.Password("guest");
            });


public void SendMessage(IBus busControl)

{

try
        {
            do
            {   var endpoint = await busControl.GetSendEndpoint(new Uri(unknown));
                endpoint.Send(new MyMessage { Text = "Hello MyMessage, World from Send!" });
                busControl.Publish(Unknown, x=>x.SetRoutingKey("high"));
                Console.WriteLine("Press [q] to exit");
            } while (Console.ReadKey().KeyChar != 'q');
        }
        finally
        {
            await busControl.StopAsync();
        }
}

}
```
![](../images/670630184.png)

![](../images/670630183.png)

#### Unknown Consumer

``` c#
using System;
using System.Threading.Tasks;
using MassTransit;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
namespace MassTransitRabbitMQDemo;

class Program
{
    static async Task Main()
    {
        var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
        {
            cfg.Host(new Uri("rabbitmq://localhost"), h => 
            {
                h.Username("guest");
                h.Password("guest");
            });

            cfg.Publish<MyMessage>(x => x.ExchangeType = "direct");


             cfg.ReceiveEndpoint(unknown_variable, e =>
            {
                e.Consumer<MyMessageResponder>();

            });

        });

        await busControl.StartAsync();
}
```
![](../images/mass_unknown_receiver.png)


### CAP.RabbitMQ

#### Publish

``` c#
namespace Sample.RabbitMQ.MongoDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private readonly IMongoClient _client;
        private readonly ICapPublisher _capBus;
private readonly string _topic;

        public ValuesController(IMongoClient client, ICapPublisher capBus, IConfiguration configuration)
        {
            _client = client;
            _capBus = capBus;
Configuration = configuration;
_topic = "sample.rabbitmq.mongodb";

        }
    public IConfiguration Configuration { get; }

        [Route("~/without/transaction")]
        public IActionResult WithoutTransaction()

        {
        
            _capBus.PublishAsync(_topic, DateTime.Now);

            return Ok();
        }


        [Route("~/delay/{delaySeconds:int}")]
        public async Task<IActionResult> Delay(int delaySeconds)
        {
            await _capBus.PublishDelayAsync(TimeSpan.FromSeconds(delaySeconds), _topic, DateTime.Now);

            return Ok();
        }

}
}
```

![](../images/670630180.png)

![](../images/670630179.png)


#### CapSubscribe

``` c#
namespace Sample.RabbitMQ.MongoDB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        private readonly IMongoClient _client;
        private readonly ICapPublisher _capBus;
private readonly string _topic;

        public ValuesController(IMongoClient client, ICapPublisher capBus, IConfiguration configuration)
        {
            _client = client;
            _capBus = capBus;
Configuration = configuration;
_topic = "sample.rabbitmq.mongodb";

        }
    public IConfiguration Configuration { get; }

        [CapSubscribe("sample.rabbitmq.mongodb")]
        public void ReceiveMessage(DateTime time)
        {
            Console.WriteLine($@"{DateTime.Now}, Subscriber invoked, Sent time:{time}");
        }

}
}
```
![](../images/cap_consumer.png)

### NServiceBus.RabbitMQ

#### Publish

``` c#
using System;
using System.Threading.Tasks;
using NServiceBus;

class Program
{
    static async Task Main()
    {
        Console.Title = "Samples.RabbitMQ.SimpleSender";

        #region ConfigureRabbit
        var endpointConfiguration = new EndpointConfiguration("Samples.RabbitMQ.SimpleSender");
        var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
        transport.UseConventionalRoutingTopology(QueueType.Quorum);
        transport.ConnectionString("host=localhost");
        #endregion

        transport.Routing().RouteToEndpoint(typeof(MyCommand), "Samples.RabbitMQ.SimpleReceiver");
        endpointConfiguration.EnableInstallers();

        var endpointInstance = await Endpoint.Start(endpointConfiguration)
            .ConfigureAwait(false);
        await SendMessages(endpointInstance);
        await endpointInstance.Stop()
            .ConfigureAwait(false);
    }

    static async Task SendMessages(IMessageSession messageSession)
    {
        Console.WriteLine("Press [c] to send a command, or [e] to publish an event. Press [Esc] to exit.");
        while (true)
        {
            var input = Console.ReadKey();
            Console.WriteLine();

            switch (input.Key)
            {
                case ConsoleKey.C:
                    await messageSession.Send(new MyCommand());
                    break;
                case ConsoleKey.L:
                    await messageSession.SendLocal(new MyCommand());
                    break;
                case ConsoleKey.E:
                    await messageSession.Publish(new MyEvent());
                    break;
                case ConsoleKey.Escape:
                    return;
            }
        }
    }
}
```

![](../images/670630174.png)

![](../images/670630173.png)

![](../images/670630172.png)

#### Send

``` c#
using System;
using System.Threading.Tasks;
using NServiceBus;

class Program
{
    static async Task Main()
    {
        Console.Title = "Samples.RabbitMQ.SimpleSender";

        #region ConfigureRabbit
        var endpointConfiguration = new EndpointConfiguration("Samples.RabbitMQ.SimpleSender");
        var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
        transport.UseConventionalRoutingTopology(QueueType.Quorum);
        transport.ConnectionString("host=localhost");
        #endregion

        transport.Routing().RouteToEndpoint(typeof(MyCommand), "Samples.RabbitMQ.SimpleReceiver");
        endpointConfiguration.EnableInstallers();

        var endpointInstance = await Endpoint.Start(endpointConfiguration)
            .ConfigureAwait(false);
        await SendMessages(endpointInstance);
        await endpointInstance.Stop()
            .ConfigureAwait(false);
    }

    static async Task SendMessages(IMessageSession messageSession)
    {
        Console.WriteLine("Press [c] to send a command, or [e] to publish an event. Press [Esc] to exit.");
        while (true)
        {
            var input = Console.ReadKey();
            Console.WriteLine();

            switch (input.Key)
            {
                case ConsoleKey.C:
                    await messageSession.Send(new MyCommand());
                    break;
                case ConsoleKey.L:
                    await messageSession.SendLocal(new MyCommand());
                    break;
                case ConsoleKey.E:
                    await messageSession.Publish(new MyEvent());
                    break;
                case ConsoleKey.Escape:
                    return;
            }
        }
    }
}
```

![](../images/670630171.png)

![](../images/670630170.png)

![](../images/670630169.png)

#### SendLocal

``` c#
using System;
using System.Threading.Tasks;
using NServiceBus;

class Program
{
    static async Task Main()
    {
        Console.Title = "Samples.RabbitMQ.SimpleSender";

        #region ConfigureRabbit
        var endpointConfiguration = new EndpointConfiguration("Samples.RabbitMQ.SimpleSender");
        var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
        transport.UseConventionalRoutingTopology(QueueType.Quorum);
        transport.ConnectionString("host=localhost");
        #endregion

        transport.Routing().RouteToEndpoint(typeof(MyCommand), "Samples.RabbitMQ.SimpleReceiver");
        endpointConfiguration.EnableInstallers();

        var endpointInstance = await Endpoint.Start(endpointConfiguration)
            .ConfigureAwait(false);
        await SendMessages(endpointInstance);
        await endpointInstance.Stop()
            .ConfigureAwait(false);
    }

    static async Task SendMessages(IMessageSession messageSession)
    {
        Console.WriteLine("Press [c] to send a command, or [e] to publish an event. Press [Esc] to exit.");
        while (true)
        {
            var input = Console.ReadKey();
            Console.WriteLine();

            switch (input.Key)
            {
                case ConsoleKey.C:
                    await messageSession.Send(new MyCommand());
                    break;
                case ConsoleKey.L:
                    await messageSession.SendLocal(new MyCommand());
                    break;
                case ConsoleKey.E:
                    await messageSession.Publish(new MyEvent());
                    break;
                case ConsoleKey.Escape:
                    return;
            }
        }
    }
}
```

![](../images/670630168.png)

![](../images/670630167.png)

#### Consumer

##### IHandleMessages

``` c#
namespace Receiver
{
    using System.Threading.Tasks;
    using NServiceBus;
    using NServiceBus.Logging;

    public class MyEventHandler : IHandleMessages<MyEvent>
    {
        static ILog log = LogManager.GetLogger<MyEventHandler>();

        public Task Handle(MyEvent eventMessage, IMessageHandlerContext context)
        {
            log.Info($"Hello from {nameof(MyEventHandler)}");
            return Task.CompletedTask;
        }
    }
}
```

![](../images/nservice_receiver.png)

##### Endpoint Configuration - Exchange

``` c#
using System;

namespace Receiver
{
    using System.Threading.Tasks;
    using NServiceBus;

    class Program
    {
        static async Task Main()
        {
            Console.Title = "Samples.RabbitMQ.SimpleReceiver";
            var endpointConfiguration = new EndpointConfiguration("Samples.RabbitMQ.SimpleReceiver");
            var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
            transport.UseConventionalRoutingTopology(QueueType.Quorum);
            transport.ConnectionString("host=localhost");
            endpointConfiguration.EnableInstallers();

            var endpointInstance = await Endpoint.Start(endpointConfiguration)
                .ConfigureAwait(false);
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            await endpointInstance.Stop()
                .ConfigureAwait(false);
        }
    }
}
```

![](../images/670630162.png)

![](../images/670630161.png)