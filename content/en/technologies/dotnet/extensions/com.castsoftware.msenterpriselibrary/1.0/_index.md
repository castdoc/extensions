---
title: "Microsoft Enterprise Library - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.msenterpriselibrary

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for Microsoft Enterprise Library APIs
(see Methods Supported) which are responsible for typical CRUD
operations with the database.

## In what situation should you install this extension?

If your C# application performs SQL queries with the Microsoft
Enterprise Library framework, and you want to modelize the Client/Server
links with appropriate objects and links, then you should use this
extension.

## Technology support

| Item                        | Version         |                                          Supported                                          | Supported Technology | Notes                                                                                                                                              |
|:-----------|:--------|:----:|:--------:|:------------------------------------|
| MS Enterprise Library       | 2.0 to 6.0.1304 | ![(tick)](/images/icons/emoticons/check.svg)  |          C#          | See [https://www.nuget.org/packages/EnterpriseLibrary.Data](https://www.nuget.org/packages/EnterpriseLibrary.Data)             |
| MS Enterprise Library SqlCe | 5.0 to 6.0.1304 |  ![(tick)](/images/icons/emoticons/check.svg) |          C#          | See [https://www.nuget.org/packages/EnterpriseLibrary.Data.SqlCe](https://www.nuget.org/packages/EnterpriseLibrary.Data.SqlCe) |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For applications using Microsoft Enterprise Library APIs, the extension
will be automatically installed by CAST Console. This is in place since
October 2023.

For upgrade, if the Extension Strategy is not set to Auto update, you
can manually install the extension using the Application -
Extensions interface.

## What results can you expect?

### Objects

| Icon | Type Description | When is this object created ? |
|----|------------------|-------------------------------|
| ![](../images/670630295.png) | MS Enterprise Library SQL Query | An object is created for each SQL query found and resolved in a MS EntLib CRUD method call |
| ![](../images/unknown_sql_query_icon.png)  | MS Enterprise Library Unknown SQL Query | An object is created for each SQL query found and not resolved in a MS EntLib CRUD method call

### Links created by this extension and other related extensions

| Link Type | Caller type | Callee type | Methods Supported |
|---|---|---|---|
| callLink | C# Method | MS Enterprise Library SQL Query<br>MS Enterprise Library Unknown SQL Query | <details><summary>Supported APIs</summary>Microsoft.Practices.EnterpriseLibrary.Data.Database.ExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.ExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.ExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.ExecuteDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.LoadDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.UpdateDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.BeginExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.BeginExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.BeginExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.Database.DoExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.ExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.ExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.ExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.ExecuteXmlReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.ExecuteDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.LoadDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.UpdateDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.BeginExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.BeginExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.BeginExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.BeginExecuteXmlReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Sql.SqlDatabase.DoExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.ExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.ExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.ExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.ExecuteDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.LoadDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.UpdateDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.BeginExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.BeginExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.BeginExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.Oracle.OracleDatabase.DoExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.GenericDatabase.ExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.GenericDatabase.ExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.GenericDatabase.ExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.GenericDatabase.ExecuteDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.GenericDatabase.LoadDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.GenericDatabase.UpdateDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.GenericDatabase.DoExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteNonQuery<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteNonQuerySql<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteScalar<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteScalarSql<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteResultSet<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.DoExecuteResultSet<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteReader<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteReaderSql<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.ExecuteDataSetSql<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.LoadDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.UpdateDataSet<br>Microsoft.Practices.EnterpriseLibrary.Data.SqlCe.SqlCeDatabase.DoExecuteNonQuery</details> |
| useLink | MS Enterprise Library SQL Query | Table, View | Created by SQL Analyzer when DDL source files are analyzed |
| callLink | MS Enterprise Library SQL Query | Procedure | As above |
| useLink | MS Enterprise Library SQL Query | Missing Table | Created by Missing tables and procedures for .Net extension when the object is not analyzed. |
| callLink | MS Enterprise Library SQL Query | Missing Procedure | As above |

## Example code scenarios

### ExecuteNonQuery

``` c#
public void RecordIsInsertedWhenExecutingExecuteReaderWithTransactionStoredProcParamsAndCallBack()
        {
            Database db = new DatabaseProviderFactory(base.ConfigurationSource).Create("DefaultSqlAsync");
            DbCommand command1 = new SqlCommand();
            AsyncCallback cb = new AsyncCallback(EndExecuteReaderCallBack);
            DbAsyncState state;
            DbTransaction transaction;
            DataTable dt = new DataTable();
            object[] paramsArray = { "BONAP" };

            DataSet ds = db.ExecuteDataSet("CustOrdersOrders", paramsArray);
            int originalCount = ds.Tables[0].Rows.Count;
            try
            {
                using (SqlConnection connection = new SqlConnection(db.ConnectionString))
                {
                    connection.Open();
                    transaction = connection.BeginTransaction();

                    // Execute the NonQuery insert statement synchronously.                    
                    command1.CommandType = CommandType.Text;
                    command1.CommandText = @"INSERT INTO [Northwind].[dbo].[Orders]
                                                   ([CustomerID]
                                                   ,[EmployeeID]
                                                   ,[OrderDate]
                                                   ,[RequiredDate]
                                                   ,[ShippedDate]
                                                   ,[ShipVia]
                                                   ,[Freight]
                                                   ,[ShipName]
                                                   ,[ShipAddress]
                                                   ,[ShipCity]
                                                   ,[ShipRegion]
                                                   ,[ShipPostalCode]
                                                   ,[ShipCountry])
                                             VALUES
                                                   ('BONAP'
                                                   ,1
                                                   ,GETDATE()
                                                   ,GETDATE()
                                                   ,NULL
                                                   ,2
                                                   ,10
                                                   ,'TestShip'
                                                   ,'TestShip address'
                                                   ,'Marseille'
                                                   ,NULL
                                                   ,'13008'
                                                   ,'France')";
                    db.ExecuteNonQuery(command1, transaction);
                    // Get the records using overloaded BeginExecuteReader with stored proc and params Asynchronously with DBTransaction.                                        
                    state = BeginExecuteReader(db, "CustOrdersOrders", transaction, cb, paramsArray);
                    dt.Load((IDataReader)state.State);

                    transaction.Commit();

                    Assert.AreEqual<int>(originalCount + 1, dt.Rows.Count);
                    Assert.AreEqual<ConnectionState>(ConnectionState.Open, state.AsyncResult.Connection.State);
                }
            }
```

![](../images/670630294.png)

ExecuteNonQuery

``` c#
public string UpdateSC(SubComponentSavedInfo UpdateSCObject, string LoginID, Int64 DisputeID, int UpdateID)
        {
            string strSPNm = "dbo.dt3_web_UpdateSC";
            string NewUpdateID = "";
            using (DbCommand ObjDbCommand = objDB.GetStoredProcCommand(strSPNm))
            {
                objDB.AddInParameter(ObjDbCommand, "@SONo", DbType.String, UpdateSCObject.SoNo);
                objDB.AddInParameter(ObjDbCommand, "@SOEBD", DbType.String, UpdateSCObject.SoEBD);
                objDB.AddInParameter(ObjDbCommand, "@ChkSOFollowUpDate", DbType.String, UpdateSCObject.ChkSoFollowUpDate);
                objDB.AddInParameter(ObjDbCommand, "@ChkSOBilledDate", DbType.String, UpdateSCObject.ChkSOBilledDate);
                objDB.AddInParameter(ObjDbCommand, "@SpiritAdjustmentURL", DbType.String, UpdateSCObject.SpiritAdjUrl);
                objDB.AddInParameter(ObjDbCommand, "@DeMinimisInd", DbType.String, UpdateSCObject.DeminimisInd);
                objDB.AddInParameter(ObjDbCommand, "@DisputeTypeInd", DbType.String, UpdateSCObject.DisputeTypeInd);
                objDB.AddInParameter(ObjDbCommand, "@UpdateID", DbType.Int32, UpdateID);
                objDB.AddOutParameter(ObjDbCommand, "@NewUpdateID", DbType.String, Int32.MaxValue);
                objDB.ExecuteNonQuery(ObjDbCommand);
                NewUpdateID = Convert.ToString(objDB.GetParameterValue(ObjDbCommand, "NewUpdateID"));
            }
            return NewUpdateID;
        }
```

![](../images/670630293.png)

### ExecuteReader

``` c#
public void ValuesAreReadWhenUsingInnerReader()
         {
            Database db = DatabaseFactory.CreateDatabase("OracleTest");
            const string sqlCommand = "select * from Customers where customerId ='BLAUS'";
            DbCommand dbCommandWrapper = db.GetSqlStringCommand(sqlCommand);
            //IDataReader dataReader = db.ExecuteReader(CommandType.Text, sqlCommand);
            using (IDataReader dataReader = db.ExecuteReader(dbCommandWrapper))
            {
                using (OracleDataReader orareader = ((OracleDataReaderWrapper)dataReader).InnerReader)
                {
                    Assert.IsNotNull(orareader);
                    int n = orareader.GetOrdinal("customerId");
                    while (dataReader.Read())
                    {
                        string str1 = orareader.GetOracleValue(n).ToString();
                        string str = ((string)dataReader[0]).Trim();
                        Assert.AreNotSame(str1, str);
                    }
                }
            }
        }
  
```

![](../images/670630292.png)

### ExecuteScalar

``` c#
public void AllRecordsAreReturnedWhenExecutingExecuteXmlReaderWithSqlCommandAndWithCallBack()
        {
            DbAsyncState state;
            DataTable dt = new DataTable();
            DbCommand command = new SqlCommand();
            DbCommand command1 = new SqlCommand();
            AsyncCallback cb = new AsyncCallback(EndExecuteXmlReaderCallBack);
            int rowCount = 0;
            SqlDatabase db = new DatabaseProviderFactory(base.ConfigurationSource).Create("DefaultSqlAsync") as SqlDatabase;

            //Get the row count synchronously.
            command1.CommandType = CommandType.Text;
            command1.CommandText = "SELECT count(*) FROM Employees";
            int originalCount = (int)db.ExecuteScalar(command1);

            // Get the data Asynchronously.
            command.CommandType = CommandType.Text;
            command.CommandText = "SELECT * FROM Employees FOR XML AUTO, XMLDATA";
            state = BeginExecuteXmlReader(db, command, cb);

        }
```

![](../images/670630291.png)

### ExecuteDataSet

``` c#
public void ResultsAreReturnedWhenExecutingAsyncStoredProcAccessor()
        {
            Database db = new DatabaseProviderFactory(base.ConfigurationSource).Create("DefaultSqlAsync");
            DataAccessor<TopTenProduct> accessor = db.CreateSprocAccessor<TopTenProduct>("Ten Most Expensive Products");
            AsyncCallback cb = new AsyncCallback(EndExecuteAccessor<TopTenProduct>);
            DbAsyncState state = new DbAsyncState(db, accessor);
            IAsyncResult result = accessor.BeginExecute(cb, state);
            state.AutoResetEvent.WaitOne(new TimeSpan(0, 0, 15));
            IEnumerable<TopTenProduct> resultSet = (IEnumerable<TopTenProduct>)state.State;

            object[] paramsArray = { };
            DataSet ds = db.ExecuteDataSet("Ten Most Expensive Products", paramsArray);
            Assert.IsNull(state.Exception);
            Assert.AreEqual<int>(ds.Tables[0].Rows.Count, resultSet.Count());
        }
```

![](../images/670630290.png)

### UpdateDataSet

``` c#
public void TestUpdateDatasetMethod()
        {
            SqlCeDatabase db = (SqlCeDatabase)database;
            DataSet dataSet = new DataSet();
            db.UpdateDataSet(dataSet, "Region", "insert into region values (99, 'Midwest')", "UPDATE region SET RegionDescription=@RegionDescription WHERE RegionID=@RegionId",
                                 "Delete from Region where RegionId = 99", "Incremental");
        }
   
```

![](../images/670630289.png)

### ExecuteXmlReader

``` c#
public void CanExecuteXmlQueryThroughTransaction()
        {
            string insertString = "insert into region values (99, 'Midwest')";
            DbCommand insertCommand = sqlDatabase.GetSqlStringCommand(insertString);

            string queryString = "Select * from Region for xml auto";
            SqlCommand sqlCommand = sqlDatabase.GetSqlStringCommand(queryString) as SqlCommand;

            string actualOutput = "";

            using (DbConnection connection = sqlDatabase.CreateConnection())
            {
                connection.Open();
                using (RollbackTransactionWrapper transaction = new RollbackTransactionWrapper(connection.BeginTransaction()))
                {
                    sqlDatabase.ExecuteNonQuery(insertCommand, transaction.Transaction);

                    XmlReader results = sqlDatabase.ExecuteXmlReader(sqlCommand, transaction.Transaction);
                    results.MoveToContent();
                    for (string value = results.ReadOuterXml(); value != null && value.Length != 0; value = results.ReadOuterXml())
                    {
                        actualOutput += value;
                    }
                    results.Close();
                }
            }
            
        }
```

![](../images/670630288.png)

### BeginExecuteReader

``` c#
protected override void Act()
        {
            var asyncResult = database.BeginExecuteReader("Ten Most Expensive Products", null, null);
            var reader = database.EndExecuteReader(asyncResult);
            reader.Dispose();
        }
```

![](../images/670630287.png)

### BeginExecuteScalar

``` c#
public class WhenAsynchronouslyInvokingExecuteScalarOnSproc : AsynchronousConnectionContext
    {
        IAsyncResult asyncResult;

        protected override void Arrange()
        {
            base.Arrange();

            asyncResult = database.BeginExecuteScalar("Ten Most Expensive Products", null, null);
        }
}
```

![](../images/670630286.png)

### BeginExecuteNonQuery

``` c#
public class WhenAsynchronouslyInvokingBeginExecuteNonQueryOnSproc : AsynchronousConnectionContext
    {
        IAsyncResult asyncResult;

        protected override void Arrange()
        {
            base.Arrange();

            asyncResult = database.BeginExecuteNonQuery("Ten Most Expensive Products", null, null);
        }
    }
```

![](../images/670630285.png)

### BeginExecuteXmlReader

``` c#
public class WhenAsynchronouslyInvokingExecuteXmlReader : AsynchronousConnectionContext
    {
        private readonly Barrier barrier = new Barrier(2);
        private XmlReader reader;

        protected override void Arrange()
        {
            base.Arrange();

            string queryString = "Select * from Region for xml auto, xmldata";
            SqlCommand sqlCommand = database.GetSqlStringCommand(queryString) as SqlCommand;
            database.BeginExecuteXmlReader(sqlCommand,
                ar =>
                {
                    barrier.Await();
                    reader = database.EndExecuteXmlReader(ar);
                    barrier.Await();
                }, null);
        }
```

![](../images/670630284.png)

### DoExecuteNonQuery

``` c#
public static void DoExecuteResultSetOracleSample(Database db)
        {
            
            db.DoExecuteNonQuery("insert into Region values (99, 'Midwest');");
        }
```

![](../images/670630283.png)

### LoadDataSet

``` c#
public static void LoadTestData(Database database)
        {
            SqlCeDatabase db = (SqlCeDatabase)database;

            db.LoadDataSet("insert into Region values (99, 'Midwest');");
        }
```

![](../images/670630282.png)

### ExecuteScalarSql

``` c#
public void SimplifiedExecuteScalarWithParameters()
    {
        string sql = "select count(*) from region where regionId=" + db.BuildParameterName("regionId");
        int result = (int)db.ExecuteScalarSql(sql, db.CreateParameter("regionId", DbType.Int32, 0, 2));
        Assert.AreEqual(1, result);
    }
```

![](../images/670630281.png)

### ExecuteNonQuerySql

``` c#
public void AlternateSqlStatementWithParameters()
        {
            string sql = "insert into region values ({0}, {1})";
            sql = String.Format(sql, db.BuildParameterName("regionId"), db.BuildParameterName("description"));

            DbParameter[] parameters = new DbParameter[]
                {
                    db.CreateParameter("regionId", DbType.Int32, 0, 99),
                    db.CreateParameter("description", DbType.String, 20, "test value")
                };
            db.ExecuteNonQuerySql(sql, parameters);
        }
```

![](../images/670630280.png)

### Unknown

``` c#
public int GetIssuersCount(string cusip, string name, string ticker)
        {
            LogHelper lLogger = new LogHelper();
            int resultvalues = 0;
            try
            {
                if (!string.IsNullOrEmpty(cusip))
                    _sqlDB.AddInParameter(dbCommand, "@Cusip", SqlDbType.Char, cusip);

                if (!string.IsNullOrEmpty(name))
                    _sqlDB.AddInParameter(dbCommand, "@Name", SqlDbType.VarChar, name);

                if (!string.IsNullOrEmpty(ticker))
                    _sqlDB.AddInParameter(dbCommand, "@Ticker", SqlDbType.VarChar, ticker);

                resultvalues = (int)_sqlDB.ExecuteScalar(dbCommand);
            }
            catch (Exception ex)
            {
                lLogger.LogError(ex.ToString());

                throw new ApplicationException(Properties.Settings.Default.DBExceptionUserFriendlyMessage);
            }

            return resultvalues;
        }
```

![](../images/msenterprise_unknown.png)

## Limitations

-   The support works when the Microsoft Enterprise Library is used
    directly, that is, not through a custom wrapper delivered as an
    assembly.
