---
title: "Microsoft Visual Studio .NET Discoverer - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dmtdotnetdiscoverer

## What's new ?

See [Release Notes](rn/) for more information.

## Extension description

This discoverer detects projects and creates the resulting Analysis
Units required for analysis based on the presence of Visual Studio
VB.NET `*.vbproj` and C# `*.csproj` files.

### In what situation should you install this extension?

This extension should be used when you are analyzing Visual Studio based
.NET (VB.NET/C#) applications. The discoverer will automatically create
one project for every VB.NET `*.vbproj` or C# `*.csproj` file that is
delivered for analysis and one corresponding Analysis Unit will then be
created for analysis purposes. For example:

### Project Exclusion Rules

The discoverer contains various Project Exclusion Rules which are
enabled by default. Following the first delivery of an application
where the discoverer was installed, the rules will be available in
Console UI for subsequent version deliveries:

![](../images/636911690.jpg)

### Technical information

The discoverer is already embedded in AIP Core: this embedded version of
the extension will not undergo any further updates and instead all
functional changes/customer bug fixes will be actioned in the extension.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically installed: instead if it is
required, the extension should be downloaded and installed "manually"
using the Console interface:

![](../images/636911689.jpg)

## Technical notes

### Behaviour when duplicate projects are encountered

The Microsoft Visual Studio .NET Discoverer behaves as follows
when duplicates (i.e, VB.NET / C# projects producing an assembly with
the same name) are encountered:

#### ≥ 8.3.43

Projects are selected based on which one is used more in \*.sln
files. If two or more are used equally in \*.sln files, then the
project with the more recent Visual Studio version will be selected. 

#### ≤ 8.3.42

The project that is selected is the first one in alphabetical order.
