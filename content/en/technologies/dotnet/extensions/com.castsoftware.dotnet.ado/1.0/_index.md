---
title: "ADO.NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.ado

## What's new?

See [Release Notes](rn) for more information.

## Description

This extension provides support for SQL query execution
calls under ADO.NET using C# language.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Supported ADO.NET data providers

All supported methods/properties below are used to execute SQL queries
on their corresponding server. Other API methods are considered
internally so that the query statements are correctly evaluated.

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Vendor</th>
<th class="confluenceTh">Namespace</th>
<th class="confluenceTh">Method</th>
<th class="confluenceTh">Parameter</th>
<th class="confluenceTh">Attribute</th>
<th class="confluenceTh">Value</th>
</tr>
&#10;<tr class="odd">
<td rowspan="31" class="confluenceTd"><strong>IBM</strong></td>
<td rowspan="10" class="confluenceTd"><p>IBM.Data.DB2</p>
<p><strong>(Db2 .NET Provider)</strong></p></td>
<td
class="confluenceTd"><p>IBM.Data.DB2.DB2Command.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="7"
class="confluenceTd">IBM.Data.DB2.DB2Command.CommandText</td>
<td rowspan="7" class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<div class="content-wrapper">
<p><br />
</p>
<p><br />
</p>
</div></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.DB2Command.ExecutePageReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.DB2Command.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.DB2Command.ExecuteResultSet</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p>IBM.Data.DB2.DB2Command.ExecuteScalar</p>
</div></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.DB2Command.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>IBM.Data.DB2.DB2Command.ExecuteRow</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">IBM.Data.DB2.DB2DataAdapter.Fill</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>IBM.Data.DB2.DB2DataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">IBM.Data.DB2.DB2DataAdapter.FillSchema</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IBM.Data.DB2.DB2DataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>IBM.Data.DB2.DB2DataAdapter.InsertCommand</p>
<p>IBM.Data.DB2.DB2DataAdapter.UpdateCommand</p>
<p>IBM.Data.DB2.DB2DataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="11" class="confluenceTd"><p>IBM.Data.DB2.Core</p>
<p><strong>(Db2 .NET Provider for .NET Core)</strong></p></td>
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2Command.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="7"
class="confluenceTd">IBM.Data.DB2.Core.DB2Command.CommandText</td>
<td rowspan="7" class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2Command.ExecutePageReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2Command.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2Command.ExecuteResultSet</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2Command.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2Command.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2Command.ExecuteRow</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2BulkCopy.WriteToServer</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2BulkCopy.DestinationTableName</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2DataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2DataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2DataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2DataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>IBM.Data.DB2.Core.DB2DataAdapter.InsertCommand</p>
<p>IBM.Data.DB2.Core.DB2DataAdapter.UpdateCommand</p>
<p>IBM.Data.DB2.Core.DB2DataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="7" class="confluenceTd"><p>IBM.Data.DB2.iSeries</p>
<p><strong>(IBM iSeries Db2 .NET Provider)</strong></p></td>
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2Command.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="4"
class="confluenceTd">IBM.Data.DB2.iSeries.iDB2Command.CommandText</td>
<td rowspan="4" class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2Command.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2Command.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2Command.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2DataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2DataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2DataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2DataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>IBM.Data.DB2.iSeries.iDB2DataAdapter.InsertCommand</p>
<p>IBM.Data.DB2.iSeries.iDB2DataAdapter.UpdateCommand</p>
<p>IBM.Data.DB2.iSeries.iDB2DataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="3" class="confluenceTd"><p>IBM.Data.Informix</p>
<p><strong>(IBM Informix .NET Provider)</strong></p></td>
<td
class="confluenceTd"><p>IBM.Data.Informix.IfxCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3"
class="confluenceTd">IBM.Data.Informix.IfxCommand.CommandText</td>
<td rowspan="3" class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>IBM.Data.Informix.IfxCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>IBM.Data.Informix.IfxCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="106" class="confluenceTd"><strong>Microsoft</strong></td>
<td rowspan="16" class="confluenceTd"><p>Microsoft.Data.SqlClient</p>
<p><strong>(.NET Data Provider for SQL Server)</strong></p></td>
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="11"
class="confluenceTd">Microsoft.Data.SqlClient.SqlCommand.CommandText
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
<td rowspan="11" class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.BeginExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.BeginExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.ExecuteXmlReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlCommand.BeginExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">Microsoft.Data.SqlClient.SqlBulkCopy.WriteToServer</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd">Microsoft.Data.SqlClient.SqlBulkCopy.DestinationTableName</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Microsoft.Data.SqlClient.SqlBulkCopy.WriteToServerAsync</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Microsoft.Data.SqlClient.SqlDataAdapter.InsertCommand</p>
<p>Microsoft.Data.SqlClient.SqlDataAdapter.UpdateCommand</p>
<p>Microsoft.Data.SqlClient.SqlDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="6" class="confluenceTd"><p>Microsoft.Data.Sqlite</p>
<p><strong>(.NET Data Provider for SQLite)</strong></p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
<td
class="confluenceTd"><p>Microsoft.Data.Sqlite.SqliteCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">Microsoft.Data.Sqlite.SqliteCommand.CommandText
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
<td rowspan="6" class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.Sqlite.SqliteCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.Sqlite.SqliteCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.Sqlite.SqliteCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Microsoft.Data.Sqlite.SqliteCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Microsoft.Data.Sqlite.SqliteCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="16" class="confluenceTd"><p>System.Data.SqlClient</p>
<p><strong>(.NET Data Provider for SQL Server)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="11"
class="confluenceTd">System.Data.SqlClient.SqlCommand.CommandText
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
<td rowspan="11" class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.BeginExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.BeginExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.ExecuteXmlReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlCommand.BeginExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">System.Data.SqlClient.SqlBulkCopy.WriteToServer</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd">System.Data.SqlClient.SqlBulkCopy.DestinationTableName</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">System.Data.SqlClient.SqlBulkCopy.WriteToServerAsync</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>System.Data.SqlClient.SqlDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>System.Data.SqlClient.SqlDataAdapter.InsertCommand</p>
<p>System.Data.SqlClient.SqlDataAdapter.UpdateCommand</p>
<p>System.Data.SqlClient.SqlDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="10" class="confluenceTd"><p>System.Data.SQLite</p>
<p><strong>(.NET Data Provider for SQLite)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="7"
class="confluenceTd">System.Data.SQLite.SQLiteCommand.CommandText</td>
<td rowspan="7" class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteCommand.Execute</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>System.Data.SQLite.SQLiteDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>System.Data.SQLite.SQLiteDataAdapter.InsertCommand</p>
<p>System.Data.SQLite.SQLiteDataAdapter.UpdateCommand</p>
<p>System.Data.SQLite.SQLiteDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="11" class="confluenceTd"><p>System.Data.OracleClient</p>
<p><strong>(.NET Data Provider for Oracle)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="8"
class="confluenceTd">System.Data.OracleClient.OracleCommand.CommandText</td>
<td rowspan="8" class="confluenceTd"><p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteOracleNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteOracleScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>System.Data.OracleClient.OracleDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>System.Data.OracleClient.OracleDataAdapter.InsertCommand</p>
<p>System.Data.OracleClient.OracleDataAdapter.UpdateCommand</p>
<p>System.Data.OracleClient.OracleDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="9" class="confluenceTd"><p>System.Data.OleDb</p>
<p><strong>(.NET Data Provider for OLE DB)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd"><p>System.Data.OleDb.OleDbCommand.CommandText</p>
<p>System.Data.OleDb.OleDbCommand.CommandType</p>
<p><br />
</p>
<p><br />
</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="5" class="confluenceTd"><p>CommandType.Text</p>
<p>CommandType.TableDirect</p>
<p>CommandType.StoredProcedure</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>System.Data.OleDb.OleDbDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>System.Data.OleDb.OleDbDataAdapter.InsertCommand</p>
<p>System.Data.OleDb.OleDbDataAdapter.UpdateCommand</p>
<p>System.Data.OleDb.OleDbDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="9" class="confluenceTd"><p>System.Data.Odbc</p>
<p><strong>(.NET Data Provider for ODBC)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">System.Data.Odbc.OdbcCommand.CommandText</td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>System.Data.Odbc.OdbcDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">System.Data.Odbc.OdbcDataAdapter.FillSchema</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>System.Data.Odbc.OdbcDataAdapter.InsertCommand</p>
<p>System.Data.Odbc.OdbcDataAdapter.UpdateCommand</p>
<p>System.Data.Odbc.OdbcDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="3" class="confluenceTd"><p>System.Data.EntityClient</p>
<p><strong>(.NET Data Provider for Entity Framework)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.EntityClient.EntityCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3"
class="confluenceTd">System.Data.EntityClient.EntityCommand.CommandText</td>
<td rowspan="3" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.EntityClient.EntityCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.EntityClient.EntityCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="5"
class="confluenceTd"><p>System.Data.Entity.Core.EntityClient</p>
<p><strong>(.NET Core Data Provider for Entity
Framework)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.Entity.Core.EntityClient.EntityCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="5"
class="confluenceTd">System.Data.Entity.Core.EntityClient.EntityCommand.CommandText</td>
<td rowspan="5" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Entity.Core.EntityClient.EntityCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Entity.Core.EntityClient.EntityCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Entity.Core.EntityClient.EntityCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Entity.Core.EntityClient.EntityCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="4" class="confluenceTd"><p>System.Data.SqlServerCe</p>
<p><strong>(.NET Data Provider for SQL Server Compact)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.SqlServerCe.SqlCeCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="4"
class="confluenceTd"><p>System.Data.SqlServerCe.SqlCeCommand.CommandText</p>
<p>System.Data.SqlServerCe.SqlCeCommand.CommandType</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlServerCe.SqlCeCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3" class="confluenceTd"><p>CommandType.Text</p>
<p>CommandType.TableDirect</p>
<p>CommandType.StoredProcedure</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.SqlServerCe.SqlCeCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.SqlServerCe.SqlCeCommand.ExecuteResultSet</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="15" class="confluenceTd"><p>System.Data.Common</p>
<p><strong>(ADO.NET Common Provider Interfaces)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.Common.DbCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">System.Data.Common.DbCommand.CommandText</td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Common.DbCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Common.DbCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Common.DbCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Common.DbCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Common.DbCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Common.DbBatch.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">System.Data.Common.DbBatchCommand.CommandText</td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Common.DbBatch.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Common.DbBatch.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Common.DbBatch.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Common.DbBatch.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Common.DbBatch.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Common.DbDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>System.Data.Common.DbDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>System.Data.Common.DbDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Common.DbDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>System.Data.Common.DbDataAdapter.InsertCommand</p>
<p>System.Data.Common.DbDataAdapter.UpdateCommand</p>
<p>System.Data.Common.DbDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"><p>System.Data.Linq</p>
<p><strong>(.NET Data Provider for LINQ to SQL)</strong></p></td>
<td
class="confluenceTd"><p>System.Data.Linq.DataContext.ExecuteCommand</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>System.Data.Linq.DataContext.ExecuteQuery</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="30" class="confluenceTd"><p>MySQL</p></td>
<td rowspan="11" class="confluenceTd"><p>MySql.Data.MySqlClient</p>
<p><strong>(MySQL Connector/NET)</strong></p></td>
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="8"
class="confluenceTd">MySql.Data.MySqlClient.MySqlCommand.CommandText</td>
<td rowspan="8" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.BeginExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.BeginExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>MySql.Data.MySqlClient.MySqlDataAdapter.InsertCommand</p>
<p>MySql.Data.MySqlClient.MySqlDataAdapter.UpdateCommand</p>
<p>MySql.Data.MySqlClient.MySqlDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="19" class="confluenceTd"><p>MySqlConnector</p>
<p><strong>(MySqlConnector ADO.NET provider)</strong></p></td>
<td
class="confluenceTd"><p>MySqlConnector.MySqlCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">MySqlConnector.MySqlCommand.CommandText</td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySqlConnector.MySqlCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySqlConnector.MySqlCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySqlConnector.MySqlCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySqlConnector.MySqlCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySqlConnector.MySqlCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySqlConnector.MySqlBatch.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd"><p>MySqlConnector.MySqlBatch.BatchCommands</p></td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySqlConnector.MySqlBatch.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySqlConnector.MySqlBatch.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySqlConnector.MySqlBatch.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySqlConnector.MySqlBatch.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySqlConnector.MySqlBatch.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">MySqlConnector.MySqlBulkCopy.WriteToServer</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd">MySqlConnector.MySqlBulkCopy.DestinationTableName</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">MySqlConnector.MySqlBulkCopy.WriteToServerAsync</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">MySqlConnector.MySqlBulkLoader.Load</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>MySqlConnector.MySqlBulkLoader.TableName</p></td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">MySqlConnector.MySqlBulkLoader.LoadAsync</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySqlConnector.MySqlDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>MySqlConnector.MySqlDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>MySqlConnector.MySqlDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>MySqlConnector.MySqlDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>MySqlConnector.MySqlDataAdapter.InsertCommand</p>
<p>MySqlConnector.MySqlDataAdapter.UpdateCommand</p>
<p>MySqlConnector.MySqlDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="20" class="confluenceTd"><strong>Oracle
Server</strong></td>
<td rowspan="10" class="confluenceTd"><p>Oracle.DataAccess.Client</p>
<p><strong>(Oracle Data Provider for .NET - Unmanaged
Driver)</strong></p></td>
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">Oracle.DataAccess.Client.OracleCommand.CommandText</td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleCommand.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleCommand.ExecuteStream</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleCommand.ExecuteToStream</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Oracle.DataAccess.Client.OracleBulkCopy.WriteToServer</td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd">Oracle.DataAccess.Client.OracleBulkCopy.DestinationTableName</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd">Oracle.DataAccess.Client.OracleDataAdapter.SelectCommand</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Oracle.DataAccess.Client.OracleDataAdapter.FillSchema</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Oracle.DataAccess.Client.OracleDataAdapter.InsertCommand</p>
<p>Oracle.DataAccess.Client.OracleDataAdapter.UpdateCommand</p>
<p>Oracle.DataAccess.Client.OracleDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="10"
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client</p>
<p><strong>(Oracle Data Provider for .NET - Managed
Driver)</strong></p></td>
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">Oracle.ManagedDataAccess.Client.OracleCommand.CommandText</td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleCommand.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleCommand.ExecuteStream</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleCommand.ExecuteToStream</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Oracle.ManagedDataAccess.Client.OracleBulkCopy.WriteToServer</td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd">Oracle.ManagedDataAccess.Client.OracleBulkCopy.DestinationTableName</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Oracle.ManagedDataAccess.Client.OracleDataAdapter.InsertCommand</p>
<p>Oracle.ManagedDataAccess.Client.OracleDataAdapter.UpdateCommand</p>
<p>Oracle.ManagedDataAccess.Client.OracleDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="41" class="confluenceTd"><strong>Devart</strong></td>
<td rowspan="10" class="confluenceTd"><p>Devart.Common</p>
<p><strong>(Devart’s dotConnect)</strong></p></td>
<td
class="confluenceTd"><p>Devart.Common.DbCommandBase.ExecutePageReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="5"
class="confluenceTd"><p>Devart.Common.DbCommandBase.CommandText</p></td>
<td rowspan="5" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Devart.Common.DbCommandBase.BeginExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Common.DbCommandBase.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Devart.Common.DbCommandBase.BeginExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Common.DbCommandBase.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Devart.Common.SqlStatement.Execute</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd">Devart.Common.SqlStatement.Text</td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Common.SqlStatement.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Devart.Common.DbScript.Execute</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Devart.Common.DbScript.ExecuteNext</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Devart.Common.DbDataAdapter.FillPage</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">Devart.Common.DbDataAdapter.SelectCommand</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="9" class="confluenceTd"><p>Devart.Data.Oracle</p>
<p><strong>(Devart’s dotConnect for Oracle)</strong></p></td>
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="5"
class="confluenceTd">Devart.Data.Oracle.OracleCommand.CommandText</td>
<td rowspan="5" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleCommand.ExecutePageReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleCommand.ExecuteArray</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3"
class="confluenceTd"><p>Devart.Data.Oracle.OracleDataAdapter.SelectCommand</p></td>
<td rowspan="3" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleDataAdapter.FillPage</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Devart.Data.Oracle.OracleDataAdapter.InsertCommand</p>
<p>Devart.Data.Oracle.OracleDataAdapter.UpdateCommand</p>
<p>Devart.Data.Oracle.OracleDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="12" class="confluenceTd"><p>Devart.Data.MySql</p>
<p><strong>(Devart’s dotConnect for MySql)</strong></p></td>
<td
class="confluenceTd">Devart.Data.MySql.MySqlCommand.ExecuteReader</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3"
class="confluenceTd">Devart.Data.MySql.MySqlCommand.CommandText</td>
<td rowspan="3" class="confluenceTd"><br />
<br />
<br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Devart.Data.MySql.MySqlCommand.BeginExecuteReader</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">Devart.Data.MySql.MySqlCommand.ExecutePageReader</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Devart.Data.MySql.MySqlHelper.ExecuteReader</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3" class="confluenceTd"><br />
</td>
<td rowspan="3" class="confluenceTd"><br />
<br />
<br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">Devart.Data.MySql.MySqlHelper.ExecuteNonQuery</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Devart.Data.MySql.MySqlHelper.ExecuteScalar</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Devart.Data.MySql.MySqlStatement.Execute</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(Overriden from Devart.Common)</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">Devart.Data.MySql.MySqlScript.ScriptText</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">Devart.Data.MySql.MySqlDataAdapter.Fill</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3"
class="confluenceTd">Devart.Data.MySql.MySqlDataAdapter.SelectCommand</td>
<td rowspan="3" class="confluenceTd"><br />
<br />
<br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Devart.Data.MySql.MySqlDataAdapter.FillPage</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd">Devart.Data.MySql.MySqlDataAdapter.FillSchema</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">Devart.Data.MySql.MySqlDataAdapter.Update</td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Devart.Data.MySql.MySqlDataAdapter.InsertCommand</p>
<p>Devart.Data.MySql.MySqlDataAdapter.UpdateCommand</p>
<p>Devart.Data.MySql.MySqlDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="4" class="confluenceTd"><p>Devart.Data.SQLite</p>
<p><strong>(Devart’s dotConnect for SQLite)</strong></p></td>
<td
class="confluenceTd">Devart.Data.SQLite.SQLiteCommand.ExecuteReader</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd">Devart.Data.SQLite.SQLiteCommand.CommandText</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd">Devart.Data.SQLite.SQLiteCommand.ExecutePageReader</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>(Overriden from Devart.Common)</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">Devart.Data.SQLite.SQLiteScript.ScriptText</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(Overriden from System.Data.Common)</td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Devart.Data.SQLite.SQLiteDataAdapter.SelectCommand</p>
<p>Devart.Data.SQLite.SQLiteDataAdapter.UpdateCommand</p>
<p>Devart.Data.SQLite.SQLiteDataAdapter.InsertCommand</p>
<p>Devart.Data.SQLite.SQLiteDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="4" class="confluenceTd"><p>Devart.Data.PostgreSql</p>
<p><strong>(Devart’s dotConnect for PostgreSql)</strong></p></td>
<td
class="confluenceTd"><p>Devart.Data.PostgreSql.PgSqlCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>Devart.Data.PostgreSql.PgSqlCommand.CommandText</p>
<p><br />
</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Data.PostgreSql.PgSqlCommand.ExecutePageReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">(Overriden from Devart.Common)</td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd">Devart.Data.PostgreSql.PgSqlScript.ScriptText</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">(Overriden from System.Data.Common)</td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Devart.Data.PostgreSql.PgSqlDataAdapter.SelectCommand</p>
<p>Devart.Data.PostgreSql.PgSqlDataAdapter.UpdateCommand</p>
<p>Devart.Data.PostgreSql.PgSqlDataAdapter.InsertCommand</p>
<p>Devart.Data.PostgreSql.PgSqlDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><p>Devart.Data.Linq</p>
<p><strong>(Devart’s dotConnect for LINQ to SQL)</strong></p></td>
<td
class="confluenceTd"><p>Devart.Data.Linq.DataContext.ExecuteCommand</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Devart.Data.Linq.DataContext.ExecuteQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="15" class="confluenceTd"><strong>PostgreSQL</strong></td>
<td rowspan="15" class="confluenceTd"><p>Npgsql</p>
<p><strong>(.NET Data Provider for PostgreSQL)</strong></p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
<td class="confluenceTd"><p>Npgsql.NpgsqlCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd">Npgsql.NpgsqlCommand.CommandText</td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Npgsql.NpgsqlCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Npgsql.NpgsqlCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Npgsql.NpgsqlCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Npgsql.NpgsqlCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Npgsql.NpgsqlCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Npgsql.NpgsqlBatch.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="6"
class="confluenceTd"><p>Npgsql.NpgsqlBatch.BatchCommands</p></td>
<td rowspan="6" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Npgsql.NpgsqlBatch.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Npgsql.NpgsqlBatch.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Npgsql.NpgsqlBatch.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Npgsql.NpgsqlBatch.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Npgsql.NpgsqlBatch.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Npgsql.NpgsqlDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>Npgsql.NpgsqlDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Npgsql.NpgsqlDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Npgsql.NpgsqlDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><p>Npgsql.NpgsqlDataAdapter.InsertCommand</p>
<p>Npgsql.NpgsqlDataAdapter.UpdateCommand</p>
<p>Npgsql.NpgsqlDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="65" class="confluenceTd"><strong>praeclarum</strong></td>
<td rowspan="65" class="confluenceTd"><p>SQLite</p>
<p><strong>(Praeclarum SQLite ADO.NET Provider)</strong></p></td>
<td class="confluenceTd"><p>SQLite.SQLiteCommand.ExecuteQuery</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="5"
class="confluenceTd"><p>SQLite.SQLiteCommand.CommandText</p></td>
<td class="confluenceTd"><p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>SQLite.SQLiteCommand.ExecuteDeferredQuery</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>SQLite.SQLiteCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>SQLite.SQLiteCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>SQLite.SQLiteCommand.ExecuteQueryScalars</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Execute</p></td>
<td class="confluenceTd"><p>string</p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.ExecuteScalar</p></td>
<td class="confluenceTd"><p>string</p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Query</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string</p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.QueryScalars</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string</p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.DeferredQuery</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string</p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.GetTableInfo</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.DropTable</p></td>
<td class="confluenceTd" style="text-align: left;"><p><br />
</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.CreateTable</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.CreateTables</p></td>
<td class="confluenceTd"
style="text-align: left;"><p>CreateFlags</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Get</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>TableMapping</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Find</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>TableMapping</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.FindWithQuery</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string </p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>TableMapping </p>
<p>string </p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Insert</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object </p>
<p>Type </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><p>object </p>
<p>string </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object </p>
<p>string</p>
<p>Type</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.InsertAll</p></td>
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>bool</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>string extra</p>
<p>bool</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>Type </p>
<p>bool</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.InsertOrReplace</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>Type</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Update</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>Type</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.UpdateAll</p></td>
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>bool</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Delete</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.DeleteAll</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteConnection.Table</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.CreateTableAsync</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.CreateTablesAsync</p></td>
<td class="confluenceTd"
style="text-align: left;"><p>CreateFlags </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.DropTableAsync</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.InsertAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>Type</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>string</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>string</p>
<p>Type</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.InsertOrReplaceAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>Type</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.InsertAllAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>bool </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>string</p>
<p>bool </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>Type</p>
<p>bool </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.UpdateAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>Type</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.UpdateAllAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>IEnumerable</p>
<p>bool </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.DeleteAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.DeleteAllAsync</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.GetAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>TableMapping</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.FindAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>object</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>object</p>
<p>TableMapping</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.FindWithQueryAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string</p>
<p>params object[]</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;"><p>TableMapping</p>
<p>string</p>
<p>params object[] </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.GetTableInfoAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.ExecuteAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string </p>
<p>params object[] </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;">SQLite.SQLiteAsyncConnection.ExecuteScalarAsync</td>
<td class="confluenceTd" style="text-align: left;"><p>string </p>
<p>params object[] </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.QueryAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string </p>
<p>params object[] </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.QueryScalarsAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string </p>
<p>params object[] </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.DeferredQueryAsync</p></td>
<td class="confluenceTd" style="text-align: left;"><p>string </p>
<p>params object[] </p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: left;"><p>SQLite.SQLiteAsyncConnection.Table</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="17" class="confluenceTd"><strong>SAP ASE</strong></td>
<td rowspan="10" class="confluenceTd"><p>Sybase.Data.AseClient</p>
<p><strong>(.NET Data Provider for Sybase ASE)</strong></p></td>
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="7"
class="confluenceTd">Sybase.Data.AseClient.AseCommand.CommandText</td>
<td rowspan="7" class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseCommand.BeginExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseCommand.BeginExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseCommand.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseCommand.BeginExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>Sybase.Data.AseClient.AseDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Sybase.Data.AseClient.AseDataAdapter.InsertCommand</p>
<p>Sybase.Data.AseClient.AseDataAdapter.UpdateCommand</p>
<p>Sybase.Data.AseClient.AseDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="7" class="confluenceTd"><p>AdoNetCore.AseClient</p>
<p><strong>(.NET Core Data Provider for Sybase ASE)</strong></p></td>
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="4"
class="confluenceTd">AdoNetCore.AseClient.AseCommand.CommandText</td>
<td rowspan="4" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseCommand.ExecuteXmlReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>AdoNetCore.AseClient.AseDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>AdoNetCore.AseClient.AseDataAdapter.InsertCommand</p>
<p>AdoNetCore.AseClient.AseDataAdapter.UpdateCommand</p>
<p>AdoNetCore.AseClient.AseDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="12" class="confluenceTd"><strong>Teradata</strong></td>
<td rowspan="12" class="confluenceTd"><p>Teradata.Client.Provider</p>
<p><strong>(.NET Data Provider for Teradata)</strong></p></td>
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.ExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="9"
class="confluenceTd">Teradata.Client.Provider.TdCommand.CommandText</td>
<td rowspan="9" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.ExecuteReaderAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.BeginExecuteReader</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.BeginExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdCommand.ExecuteCreateProcedure</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdDataAdapter.Fill</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2"
class="confluenceTd"><p>Teradata.Client.Provider.TdDataAdapter.SelectCommand</p></td>
<td rowspan="2" class="confluenceTd"><br />
&#10;<p><br />
</p></td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdDataAdapter.FillSchema</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdDataAdapter.Update</p></td>
<td class="confluenceTd"><br />
</td>
<td
class="confluenceTd"><p>Teradata.Client.Provider.TdDataAdapter.InsertCommand</p>
<p>Teradata.Client.Provider.TdDataAdapter.UpdateCommand</p>
<p>Teradata.Client.Provider.TdDataAdapter.DeleteCommand</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="8" class="confluenceTd"><strong>SnowFlake</strong></td>
<td rowspan="4" class="confluenceTd"><p>Snowflake.Client</p>
<strong>(.NET Client for Snowflake DB REST API)</strong><br />
<br />
<br />
</td>
<td
class="confluenceTd"><p>Snowflake.Client.SnowflakeClient.ExecuteAsync</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="4" class="confluenceTd"><br />
<br />
</td>
<td rowspan="4" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Snowflake.Client.SnowflakeClient.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Snowflake.Client.SnowflakeClient.QueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Snowflake.Client.SnowflakeClient.QueryRawResponseAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="4" class="confluenceTd"><p>Snowflake.Data.Client</p>
<p><strong>(Snowflake Connector for .NET)</strong></p>
<br />
<br />
</td>
<td
class="confluenceTd"><p>Snowflake.Data.Client.SnowflakeDbCommand.ExecuteNonQuery</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="4"
class="confluenceTd">Snowflake.Data.Client.SnowflakeDbCommand.CommandText</td>
<td rowspan="4" class="confluenceTd"><br />
&#10;<p><br />
</p>
<p><br />
</p>
<p><br />
</p></td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Snowflake.Data.Client.SnowflakeDbCommand.ExecuteNonQueryAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Snowflake.Data.Client.SnowflakeDbCommand.ExecuteScalar</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Snowflake.Data.Client.SnowflakeDbCommand.ExecuteScalarAsync</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="89" class="confluenceTd"><strong>Spring</strong></td>
<td rowspan="32" class="confluenceTd"><p>Spring.Data.Generic</p>
<p><strong>(Spring Data Generic ADO.NET Provider)</strong></p></td>
<td rowspan="7"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.Execute</p></td>
<td class="confluenceTd">IDataAdapterCallback</td>
<td class="confluenceTd"><p>DoInDataAdapter(IDbDataAdapter)</p></td>
<td rowspan="7" class="confluenceTd"><br />
<br />
<br />
<br />
<br />
<br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">IDbCommandCreator</td>
<td class="confluenceTd"><p>CreateDbCommand(IDbProvider)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">IDbCommandDelegate</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">IDbCommandCallback</td>
<td class="confluenceTd"><p>DoInCommand(IDbCommand)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">CommandDelegate</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">ICommandCallback</td>
<td class="confluenceTd"><p>DoInCommand(DbCommand)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">DataAdapterDelegate</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.ExecuteNonQuery</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3" class="confluenceTd"><br />
<br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">IDbCommandCreator</td>
<td class="confluenceTd">CreateDbCommand(IDbProvider)</td>
</tr>
<tr class="even">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.ExecuteScalar</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="3" class="confluenceTd"><br />
<br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">IDbCommandCreator</td>
<td class="confluenceTd">CreateDbCommand(IDbProvider)</td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryForObject</p></td>
<td class="confluenceTd"><p>string</p></td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryForObjectDelegate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">ICommandSetter</td>
<td class="confluenceTd">SetValues(IDbCommand)</td>
</tr>
<tr class="odd">
<td
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryWithCommandCreator</p></td>
<td class="confluenceTd"><p>IDbCommandCreator</p></td>
<td class="confluenceTd">CreateDbCommand(IDbProvider)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryWithResultSetExtractor</p></td>
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td rowspan="3" class="confluenceTd"><br />
<br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>CommandSetterDelegate</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryWithResultSetExtractorDelegate</p></td>
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td rowspan="3" class="confluenceTd"><br />
<br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>CommandSetterDelegate</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryWithRowCallback</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryWithRowCallbackDelegate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryWithRowMapper</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Generic.AdoTemplate.QueryWithRowMapperDelegate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
</tr>
<tr class="even">
<td rowspan="57" class="confluenceTd"><p>Spring.Data.Core</p>
<p><strong>(Spring Data Core ADO.NET Provider)</strong></p></td>
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataSetCreate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataSetCreateWithParams</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataSetFill</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataSetFillWithParameters</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="even">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataSetUpdate</p>
<p><br />
</p></td>
<td class="confluenceTd"><p>IDbCommand</p>
<p>IDbCommand</p>
<p>IDbCommand</p></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>CreateDbCommand(IDbProvider)</p>
<p>-</p>
<p>-</p>
</div></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>string</p>
<p>string</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>IDbCommand</p>
<p>IDbCommand</p>
<p>IDbCommand</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p>CreateDbCommand(IDbProvider)</p>
<p>-</p>
<p>-</p>
<p>SetValues(IDbDataAdapter)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataSetUpdateWithCommandBuilder</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataTableCreate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataTableCreateWithParams</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataTableFill</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataTableFillWithParams</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataTableUpdate</p></td>
<td class="confluenceTd"><p>string</p>
<p>string</p>
<p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p><br />
</p>
<p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>string</p>
<p>string</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.DataTableUpdateWithCommandBuilder</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td rowspan="2" class="confluenceTd"><br />
<br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>IDataAdapterSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbDataAdapter)</p></td>
</tr>
<tr class="odd">
<td rowspan="7"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.Execute</p></td>
<td class="confluenceTd">IDataAdapterCallback</td>
<td class="confluenceTd"><p>DoInDataAdapter(IDbDataAdapter)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">IDbCommandCreator</td>
<td class="confluenceTd"><p>CreateDbCommand(IDbProvider)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">IDbCommandDelegate</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">IDbCommandCallback</td>
<td class="confluenceTd"><p>DoInCommand(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">CommandDelegate</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">ICommandCallback</td>
<td class="confluenceTd"><p>DoInCommand(DbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">DataAdapterDelegate</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.ExecuteNonQuery</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">IDbCommandCreator</td>
<td class="confluenceTd">CreateDbCommand(IDbProvider)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.ExecuteScalar</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">IDbCommandCreator</td>
<td class="confluenceTd">CreateDbCommand(IDbProvider)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryForObject</p></td>
<td class="confluenceTd"><p>string</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryForObjectDelegate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">ICommandSetter</td>
<td class="confluenceTd">SetValues(IDbCommand)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryWithCommandCreator</p></td>
<td class="confluenceTd">IDbCommandCreator</td>
<td class="confluenceTd">CreateDbCommand(IDbProvider)</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryWithResultSetExtractor</p></td>
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>CommandSetterDelegate</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td rowspan="3"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryWithResultSetExtractorDelegate</p></td>
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>string</p>
<p>CommandSetterDelegate</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryWithRowCallback</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryWithRowCallbackDelegate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryWithRowMapper</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="odd">
<td rowspan="2"
class="confluenceTd"><p>Spring.Data.Core.AdoTemplate.QueryWithRowMapperDelegate</p></td>
<td class="confluenceTd">string</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>string</p>
<p>ICommandSetter</p></td>
<td class="confluenceTd"><p><br />
</p>
<p>SetValues(IDbCommand)</p></td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

## What results can you expect?

### Objects

The following objects are created by the extension, with the
corresponding icon displayed in CAST Enlighten:

| Icon | Object type Description | When is this object created ? |
|:----:|-------------------------|-------------------------------|
| ![](../images/query_icon.png)  | Dotnet Ado Query | An object is created for each SQL query found and resolved in an Dotnet CRUD method call  |
| ![](../images/unknown_query_icon.png)  | DotNet Unknown ADO Query | An object is created for each SQL query found could not be resolved correctly in an Dotnet CRUD method call  |

### Links

The following links will be created by the Ado.Net extension, or by
alternate extensions.

| Link type | Caller type | Callee type | Methods Supported |
|-----------|-------------|:-----------:|-------------------|
| callLink  | C# Method | ![](../images/query_icon.png) | All CRUD methods supported from ADO.NET providers (see above)  |
| callLink  | C# Method | ![](../images/unknown_query_icon.png) | All CRUD methods supported from ADO.NET providers (see above)  |
| useLink   | Dotnet Ado Query | ![](../images/table_icon.png) | Created by SQL Analyzer when DDL source files are analyzed  |
| callLink  | Dotnet Ado Query | ![](../images/procedure_icon.png) |   |
| useLink   | Dotnet Ado Query |   ![](../images/missing_table.png) | Created by [Missing tables and procedures for .NET](../../../../sql/extensions/missing-tables/com.castsoftware.dotnet.missingtable/) when the object is not analyzed |
| callLink  | Dotnet Ado Query |   ![](../images/missing_procedure.png) |   |

### Git samples

#### [Direct SQL](https://github.com/DirectSQL/DirectSQL)

##### DirectSQL-main\TestSqlLiteDatabase\SqlResultTest.cs

``` c#
private static void CreateTableForTest(IDbConnection connection)
{
   using (var command = connection.CreateCommand())
   {
       command.CommandText =
           "create table " +
           "TEST_TABLE(" +
           "TEST_COL1 text," +
           "TEST_COL2 integer" +
           ")";

       command.ExecuteNonQuery();
   }
}
```

One Ado object is created:

![](../images/670629911.png)

##### DirectSQL-main\TestSqlLiteDatabase\QueryDefaultTransaction.cs

``` c#
private static void CreateTableForTest(IDbConnection connection)
{
   using( var command = connection.CreateCommand())
   {
       command.CommandText =
                   "create table " +
                    "TEST_TABLE(" +
                    "TEST_VAL1 text," +
                    "TEST_VAL2 integer" +
                    ")";

       command.ExecuteNonQuery();
   }
}
```

One Ado object is created:

![](../images/670629910.png)

#### [Employee management](https://github.com/mysticrenji/ASPNetCore_ADO)

##### ASPNetCore_ADO-master\EmployeeManagement\Models\EmployeeDataAccessLayer.cs

``` c#
public Employee GetEmployeeData(int? id)
        {
            Employee employee = new Employee();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                string sqlQuery = "SELECT * FROM tblEmployee WHERE EmployeeID= " + id;
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    employee.ID = Convert.ToInt32(rdr["EmployeeID"]);
                    employee.Name = rdr["Name"].ToString();
                    employee.Gender = rdr["Gender"].ToString();
                    employee.Department = rdr["Department"].ToString();
                    employee.City = rdr["City"].ToString();
                }
            }
            return employee;
        }
```

One Ado object is created:

![](../images/670629909.png)

## Limitations

-   Each evaluation that results in standard exception or empty value will lead to one unknown object creation. Also, failure in value conversion will create unknown object, the current condition for an object to be considered as unknown is that it contains only "?" for its sql string value.
-   Generic parameter still needs better support in string evaluation to find
resolution for object like class, method, etc. in order to better retrieve
table name or primary key attribute of a certain column.
-   List and other similar containers also need better support in
string evaluation in order to return full values for a complete SQL
query construction (in SQLite for example).
