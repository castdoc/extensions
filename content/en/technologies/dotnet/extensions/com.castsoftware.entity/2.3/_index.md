---
title: "Entity Framework - 2.3"
linkTitle: "2.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.entity

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for Entity Framework. The
calculation of Automated Function Points for your .NET analyses will be
supplemented through the links between objects produced by the base .NET
Analyzer and database tables, using Entity Framework CRUD operations.

## In what situation should you install this extension?

If your .NET application contains Entity Framework source code and you
want to view these object types and their links, then you should install
this extension. More specifically the extension will identify:

-   "call" links from C# methods using Entity Framework operations to
    Entity framework operation objects
-   "use" links from Entity framework operation objects to participating
    Database tables

## Technology support

The following .NET framework / Libraries are supported by this extension:

| Framework / Library name | Version | Supported | Supported Technology | Comments |
|---|---|:---:|---|---|
| [Entity Framework (EF, EF6)](https://docs.microsoft.com/en-us/ef/ef6/) | 3.0 to 6.4.x | :white_check_mark: | C# |   |
| Entity Framework Core (EF Core) | 1.0.x to 7.0.x | :white_check_mark: | C# |   |
| Z.EntityFramework.Plus.EF6 | 1.6.4 to 7.21.0 | :white_check_mark: | C# | Entity Framework Plus extends DbContext with must-have features: Include Filter, Auditing, Caching, Query Future, Batch Delete and Batch Update. |
| Z.EntityFramework.Plus.EFCore | 2.22.3 to 7.22.4 | :white_check_mark: | C# | Same as above |
| [EFCore.BulkExtensions](https://www.nuget.org/packages/EFCore.BulkExtensions) | 1.0 to 7.1 | :white_check_mark: | C# |   |
| [LINQ to Entities - method syntax](https://learn.microsoft.com/en-us/dotnet/framework/data/adonet/ef/language-reference/linq-to-entities) |   | :white_check_mark: | C# |   |

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST
Console. You can manage the extension using the Application -
Extensions interface.

![](../images/670630054.jpg)

## What results can you expect?

### Objects

| Icon                            | Object Type Description  | Comment                                                                                  |
|:-------------------------------:|--------------------------|------------------------------------------------------------------------------------------|
| ![](../images/670630053.png)    | EF Entity                   | Created when the DbSet Entity is declared in the derived class inheriting from DbContext |
| ![](../images/670630052.png)    | EF Entity Operation         | Used when CRUD operation is performed on Entity                                          |
| ![](../images/670630051.png)    | EF SQL Query         | Used for native queries                                                                  |
| ![](../images/UnknownQuery.png) | EF Unknown SQL Query | Used when the query could not be resolved 
| ![](../images/Unknown_Entity_Operation.png) | EF Unknown Entity | Created and used when the entity could not be resolved 
| ![](../images/Entity_Operation.png) | EF Unknown Entity Operation | Used when CRUD operation is performed and Entity could not be resolved                                                |

### Links

<table>
<tbody>
<tr>
<th>Link type</th>
<th>Source and destination of link</th>
<th> Methods supported</th>
</tr>
<tr>
<td><p>callLink </p>
<p>useInsertLink</p></td>
<td><p>callLink between the caller .NET Method object and EF Entity Operation /EF Unknown Entity Operation object</p>
<p>useInsertLink between the EF Entity Operation object and Database Table
object</p></td>
<td>
<ul>
<details><summary>EF Insert Operation APIs</summary>
<li>System.Data.Entity.DbSet.Add</li>
<li>System.Data.Entity.DbSet.AddRange</li>
</details>
</ul>

<ul>
<details><summary>EF Core DbSet &amp; DbContext Insert Operation APIs</summary>
<li>Microsoft.EntityFrameworkCore.DbSet.Add</li>
<li>Microsoft.EntityFrameworkCore.DbSet.AddRange</li>
<li>Microsoft.EntityFrameworkCore.DbSet.AddAysnc</li>
<li>Microsoft.EntityFrameworkCore.DbSet.AddRangeAsync</li>
<li>Microsoft.EntityFrameworkCore.DbContext.AddRange</li>
<li>Microsoft.EntityFrameworkCore.DbContext.AddRangeAsync</li>
<li>Microsoft.EntityFrameworkCore.DbContext.Add</li>
<li>Microsoft.EntityFrameworkCore.DbContext.AddAsync</li>
</details>
</ul>

<ul>
<details><summary>Insert Operation supported through ObjectContext APIs</summary>
<li>System.Data.Objects.ObjectContext.AddObject</li>
<li>System.Data.Objects.ObjectSet.AddObject</li>
<li>System.Data.Entity.Core.Objects.ObjectContext.AddObject</li>
</details>
</ul>
<ul>
<details><summary>Insert Operation through EFCore.BulkExtensions</summary>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsert</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertAsync</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdate</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateAsync</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateOrDeleteAsync</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateOrDelete</li>
</details>
</ul>
</td>
</tr>
<tr>
<td><p>callLink </p>
<p>useDeleteLink</p></td>
<td><p>callLink between the caller .NET Method object and EF Entity Operation / EF Unknown Entity Operation object</p>
<p>useDeleteLink between the EF Entity Operation object and Database Table
object</p></td>
<td>
<ul><details><summary>EF Delete Operation APIs</summary>
<li>System.Data.Entity.DbSet.Remove</li>
<li>System.Data.Entity.DbSet.RemoveRange</li>
</details>
</ul>

<ul><details><summary>EF Core DbSet &amp; DbContext Delete Operation APIs</summary>
<li>Microsoft.EntityFrameworkCore.DbSet.Remove</li>
<li>Microsoft.EntityFrameworkCore.DbSet.RemoveRange</li>
<li>Microsoft.EntityFrameworkCore.DbContext.Remove</li>
</details>
</ul>
<ul>
<details><summary>Delete Operation supported through ObjectContext APIs</summary>
<li>System.Data.Objects.ObjectContext.DeleteObject</li>
<li>System.Data.Objects.ObjectSet.DeleteObject</li>
<li>System.Data.Entity.Core.Objects.ObjectContext.DeleteObject</li>
</details>
</ul>

<ul><details><summary>Delete Operations supported through EF Plus APIs</summary>
<li>Z.EntityFramework.Plus.BatchDeleteExtensions.Delete</li>
</details>
</ul>

<ul>
<details><summary>Delete Operation through EFCore.BulkExtensions</summary>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkDelete</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkDeleteAsync</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateOrDelete</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateOrDeleteAsync</li>
<li>EFCore.BulkExtensions.IQueryableBatchExtensions.BatchDelete</li>
<li>EFCore.BulkExtensions.IQueryableBatchExtensions.BatchDeleteAsync</li>
</details>
</ul>
</td>
</tr>
<tr>
<td><p>callLink </p>
<p>useUpdateLink</p></td>
<td><p>callLink between the caller .NET Method object and EF Entity Operation /EF Unknown Entity Operation object</p>
<p>useUpdateLink between the EF Entity Operation object and Database Table
object</p></td>
<td>

<ul><details><summary>EF Core DbSet &amp; DbContext Update Operation APIs</summary>
<li>Microsoft.EntityFrameworkCore.DbSet.Update</li>
<li>Microsoft.EntityFrameworkCore.DbSet.UpdateRange</li>
<li>Microsoft.EntityFrameworkCore.DbContext.Update</li>
</details>
</ul>

<ul><details><summary>Update Operations supported through EF Plus APIs</summary>
<li>Z.EntityFramework.Plus.BatchUpdateExtensions.Update</li>
</details>
</ul>

<ul><details><summary>Update Operation through EFCore.BulkExtensions</summary>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkUpdate</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkUpdateAsync</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdate</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateAsync</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateOrDelete</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkInsertOrUpdateOrDeleteAsync</li>
<li>EFCore.BulkExtensions.IQueryableBatchExtensions.BatchUpdate</li>
<li>EFCore.BulkExtensions.IQueryableBatchExtensions.BatchUpdateAsync</li>
</details>
</ul>
</td>
</tr>
<tr>
<td><p>callLink </p>
<p>useSelectLink</p></td>
<td><p>callLink between the caller .NET Method object and EF Entity Operation / EF Unknown Entity Operation object</p>
<p>useSelecttLink between the EF Entity Operation object and Database Table
object</p></td>
<td>

<ul><details><summary>EF Select Operation APIs</summary>
<li>System.Data.Entity.DbSet.Attach</li>
<li>System.Data.Entity.DbSet.Create</li>
<li>System.Data.Entity.DbSet.Equals</li>
<li>System.Data.Entity.DbSet.Find</li>
<li>System.Data.Entity.DbSet.FindAsync</li>
<li>System.Data.Entity.DbSet.GetHashCode</li>
<li>System.Data.Entity.DbSet.GetType</li>
<li>System.Data.Entity.DbSet.Include</li>
<li>System.Data.Entity.DbSet.SqlQuery</li>
<li>System.Data.Entity.DbSet.ToString</li>
<li>System.Data.Entity.QueryableExtensions.AllAsync</li>
<li>System.Data.Entity.QueryableExtensions.AnyAync</li>
<li>System.Data.Entity.QueryableExtensions.AverageAsync</li>
<li>System.Data.Entity.QueryableExtensions.AsNoTracking</li>
<li>System.Data.Entity.QueryableExtensions.AsTracking</li>
<li>System.Data.Entity.QueryableExtensions.Contains</li>
<li>System.Data.Entity.QueryableExtensions.CountAsync</li>
<li>System.Data.Entity.QueryableExtensions.FirstAsync</li>
<li>System.Data.Entity.QueryableExtensions.FirstOrDefaultAsync</li>
<li>System.Data.Entity.QueryableExtensions.ForEachAsync</li>
<li>System.Data.Entity.QueryableExtensions.Include</li>
<li>System.Data.Entity.QueryableExtensions.Load</li>
<li>System.Data.Entity.QueryableExtensions.LoadAsync</li>
<li>System.Data.Entity.QueryableExtensions.LongCountAsync</li>
<li>System.Data.Entity.QueryableExtensions.MaxAsync</li>
<li>System.Data.Entity.QueryableExtensions.MinAsync</li>
<li>System.Data.Entity.QueryableExtensions.SingleAsync</li>
<li>System.Data.Entity.QueryableExtensions.SingleOrDefaultAsync</li>
<li>System.Data.Entity.QueryableExtensions.SumAsync</li>
<li>System.Data.Entity.QueryableExtensions.ToArrayAsync</li>
<li>System.Data.Entity.QueryableExtensions.ToDictionaryAsync</li>
<li>System.Data.Entity.QueryableExtensions.ToListAsync</li>
<li>Immediate apis belonging to System.Linq.Enumerable</li>
<li>Immediate apis belonging to System.Linq.Queryable.</li>
</details>
</ul>
<p><br />
</p>

<ul><details><summary>EF Core Select Operation APIs</summary>
<li>Microsoft.EntityFrameworkCore.DbSet.Find</li>
<li>Microsoft.EntityFrameworkCore.DbSet.FindAsync</li>
<li>Microsoft.EntityFrameworkCore.DbSet.GetAsyncEnumerator</li>
<li>Microsoft.EntityFrameworkCore.DbSet.GetHashCode</li>
<li>Microsoft.EntityFrameworkCore.DbSet.ToString</li>
<li>Microsoft.EntityFrameworkCore.DbSet.Equals</li>
<li>Microsoft.EntityFrameworkCore.DbSet.Attach</li>
<li>Microsoft.EntityFrameworkCore.DbSet.AttachRange</li>
<li>Microsoft.EntityFrameworkCore.DbSet.AsQueryable</li>
<li>Microsoft.EntityFrameworkCore.DbSet.AsAsyncEnumerable</li>
</details>
</ul>

<ul><details><summary>Microsft.EntityFrameworkCore.EntityFrameworkQueryableExtensions Select
Operation APIs</summary>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AllAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AnyAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsAsyncEnumerable</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsNoTracking</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsNoTrackingWithIdentityResolution</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsTracking</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ContainsAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.FirstAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.FirstOrDefaultAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ForEachAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.IgnoreAutoIncludes</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.IgnoreQueryFilters</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.Include</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LastAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LastOrDefaultAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.Load</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LongCountAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.MaxAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.MinAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.NotQuiteInclude</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SingleAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SingleOrDefaultAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.TagWith</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.TagWithCallSite</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ThenInclude</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToArrayAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToDictionaryAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToListAsync</li>
<li>Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToQueryAsync</li>
</details>
</ul>

<ul><details><summary>Z.EntityFramework.Plus Select Operation APIs</summary>
<li>Z.EntityFramework.Plus.QueryFutureExtensions.FutureValue</li>
<li>Z.EntityFramework.Plus.QueryFutureExtensions.Future</li>
<li>Z.EntityFramework.Plus.QueryCacheExtensions.FromCache</li>
<li>Z.EntityFramework.Plus.QueryCacheExtensions.FromCacheAsync</li>
<li>Z.EntityFramework.Plus.QueryDeferred.Execute</li>
<li>Z.EntityFramework.Plus.QueryIncludeFilterExtensions.IncludeFilter</li>
<li>Z.EntityFramework.Plus.QueryIncludeFilterExtensions.IncludeFilterByPath</li>
<li>Z.EntityFramework.Plus.QueryIncludeFilterExtensions.IncludeOptimizedSingle</li>
<li>Z.EntityFramework.Plus.QueryIncludeOptimizedExtensions.IncludeOptimized</li>
<li>Z.EntityFramework.Plus.QueryIncludeOptimizedExtensions.IncludeOptimizedByPath</li>
<li>Z.EntityFramework.Plus.QueryIncludeOptimizedExtensions.IncludeOptimizedSingle</li>
<li>Z.EntityFramework.Plus.QueryIncludeOptimizedExtensions.IncludeOptimizedSingleLazy</li>
</details>
</ul>

<ul><details><summary>Select Operations in EF Plus supported through Eval-Expression.Net
Library</summary>
<li>Immediate apis belonging to System.Linq.QueryableDynamic</li>
<li>Immediate apis belonging to System.Linq.EnumerableDynamic</li>
</details>
</ul>

<ul><details><summary>Select Operation through EFCore.BulkExtensions</summary>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkRead</li>
<li>EFCore.BulkExtensions.DbContextBulkExtensions.BulkReadAsync</li>
</details>
</ul>
</td>
</tr>
<tr>
<td>callLink</td>
<td>callLink between the caller .NET Method object and EF SQL Query / EF Unknown SQl Query object</td>
<td>

<ul><details><summary>Execute APIs</summary>
<li>System.Data.Entity.Database.ExecuteSqlCommand</li>
<li>System.Data.Entity.Database.ExecuteSqlCommandAsync</li>
<li>Microsoft.EntityFrameworkCore.RelationalDatabaseFacadeExtensions.ExecuteSqlCommand</li>
<li>Microsoft.EntityFrameworkCore.RelationalDatabaseFacadeExtensions.ExecuteSqlCommandAsync</li>
<li>Microsoft.EntityFrameworkCore.RelationalDatabaseFacadeExtensions.ExecuteSqlRaw</li>
<li>Microsoft.EntityFrameworkCore.RelationalDatabaseFacadeExtensions.ExecuteSqlRawAsync</li>
<li>Microsoft.EntityFrameworkCore.RelationalQueryableExtensions.FromSql</li>
<li>Microsoft.EntityFrameworkCore.RelationalQueryableExtensions.FromSqlRaw</li>
<li>Microsoft.EntityFrameworkCore.RelationalQueryableExtensions.FromSqlInterpolated</li>
</details>
</ul>
<ul><details><summary>Execute APIs supported through ObjectContext APIs</summary>
<li>System.Data.Objects.ObjectContext.ExecuteFunction</li>
<li>System.Data.Objects.ObjectContext.ExecuteStoreCommand</li>
<li>System.Data.Objects.ObjectContext.ExecuteStoreQuery</li>
<li>System.Data.Entity.Core.Objects.ObjectContext.ExecuteFunction</li>
<li>System.Data.Entity.Core.Objects.ObjectContext.ExecuteStoreCommand</li>
<li>System.Data.Entity.Core.Objects.ObjectContext.ExecuteStoreQuery</li>
<li>System.Data.Entity.Core.Objects.ObjectContext.ExecuteStoreCommandAsync</li>
<li>System.Data.Entity.Core.Objects.ObjectContext.ExecuteStoreQueryAsync</li>
</details>
</ul>
</td>
</tr>
<tr>
<td><p>relyOnLink</p></td>
<td><p>relyOnLink between the EF Entity and .NET Class</p>
</td>
<td>
</tbody>
</table>

## Code examples

### Insert operation

Insert operation

Add

``` c#
public static void GenerateData()
{
    using EntityContext context = new();
    context.Customers.Add(new Customer() { Name = "Customer_A", Description = "Description", IsActive = true });
}
```

![](../images/1.jpg)

### Insert Operation through dbcontext.Set\<T\>

Insert through dbcontext.Set

``` c#
public void SaveCalculatedCycles(string discriminator, IEnumerable<AuditCycle> calculatedCycles)
        {
            foreach (AuditCycle cycle in calculatedCycles)
            {
                MissionAuditCycle saveableCycle = new MissionAuditCycle
                {
                    DateOfEarliestAuditPerformed = cycle.DateOfEarliestAuditPerformed,
                    StartDate = cycle.StartDate,
                    EndDate = cycle.EndDate,
                    MissionDiscriminatorWithoutIndex = discriminator,
                    ShiftId = cycle.ShiftId
                };
                _dbContext.Set<MissionAuditCycle>().Add(saveableCycle);
            }
        }
```

![](../images/2.jpg)

### Insert Operation through ObjectContext

Insert Operation through ObjectContext

``` c#
public void AddToACCESS_RIGHT_STATUS(ACCESS_RIGHT_STATUS aCCESS_RIGHT_STATUS)
{
    base.AddObject("ACCESS_RIGHT_STATUS", aCCESS_RIGHT_STATUS);
}
```

![](../images/3.jpg)

### Delete operation

Delete operation

Delete

``` c#
public override void DeleteEntity(Entities context, object entity)
        {
            var entityToRemove = (ShelfInspectionCreationPermission)entity;
            if (entityToRemove.AdditionApprovalId != null) { context.AdditionApprovals.Remove(entityToRemove.AdditionApproval); }
            if (entityToRemove.DeletionApprovalId != null) { context.DeletionApprovals.Remove(entityToRemove.DeletionApproval); }

            entityToRemove.User_Depots.ToList().ForEach(ud => ud.ShelfInspectionCreationPermission = null);
            context.ShelfInspectionCreationPermissions.Remove(entityToRemove);
        }
```

![](../images/relyon_1.jpg)

### Delete Operation through ObjectContext

Delete Operation through ObjectContext

``` c#
public void DeleteACCESS_RIGHT_STATUS(ACCESS_RIGHT_STATUS aCCESS_RIGHT_STATUS)
{
    base.DeleteObject(aCCESS_RIGHT_STATUS);
}
```

![](../images/5.jpg)

### Batch Delete Operation through EF Plus

Delete Operation through EF Plus

``` c#
public override void DeleteEntity(Entities context, object entity)
        {
            var entityToRemove = (Role_SupplierComplaints_ProcessingStage)entity;
            if (entityToRemove.AdditionApprovalId != null) { context.AdditionApprovals.Remove(entityToRemove.AdditionApproval); }
            if (entityToRemove.DeletionApprovalId != null) { context.DeletionApprovals.Remove(entityToRemove.DeletionApproval); }
            context.Role_SupplierComplaints_ProcessingStage.Remove(entityToRemove);
        }
```

![](../images/relyon_2.jpg)

  

### Update Operation

Update operation

Update

``` c#
public void UpdateContractorAddress()        
{
            var contractor = dbContext.Contractor.FirstOrDefault();
            contractor.Address = ModelFakes.ContractorFake.Generate().Address;
            dbContext.Contractor.Update(contractor);
            dbContext.SaveChanges();
}         
```

![](../images/7.jpg)

### Batch Update Operation through EF Plus

Batch Update Operation through EF Plus

``` c#
public static void UpdateRows()
{
            
    EntityContext context = new ();
            
    context.Customers.Where(x => x.Name == "Customer_C").Update(x => new Customer {Description = "Updated_C",IsActive = false});
}
```

![](../images/8.jpg)

  

### Bulk Operations through EFCore.BulkExtensions

Bulk Operations

``` c#
public static void BulkOperations()
    {
        List<InvoiceItem> invoiceDetails = new List<InvoiceItem>
        {
            new InvoiceItem { InvoiceItemId = 1, ProductName ="Apple" },
            new InvoiceItem { InvoiceItemId = 2, ProductName = "Orange"},
            // Add more invoice details to synchronize
        };
        
        var context = new EntityContext();
        context.BulkInsertOrUpdateOrDelete(invoiceDetails);
     }
```

![](../images/9.jpg)

  

### Select operation

Select operation

Find

``` c#
protected void AssignPermissionToUser(User user, int depotId, Entities context)
{
    var user_depotEntity = context.User_Depots.Find(user.Id, depotId);

    if (user_depotEntity.ShelfInspectionCreationPermission != null)
    {
        throw new EntityConflictException("Permission already existing or pending.");
    }

}
```

![](../images/relyon_3.jpg)

Select operation through Z.EntityFramework.Plus

Select

``` c#
public static void QueryDeferredTrial()
{
    using EntityContext context = new();
        
    //Query Cache
    context.Customers.DeferredCount().FromCache();

}       
        
```

![](../images/11.jpg)

### LINQ-To-Entities

LINQ-To-Entities

LINQ-To-Entities

``` c#
public string[] GetColumnPermissionsOfUser(int id, string tableName)
{
    using (var context = new Entities())
    {
        List<int> userRoles = context.User_Roles
        .Where(ur => ur.UserId == id)
        .Where(ur => ur.AdditionApproval.ApprovedAt.HasValue)
        .Where(ur => ur.Role.AdditionApproval.ApprovedAt.HasValue)
        .Select(ur => ur.RoleId)
        .ToList();

    }
}
```

![](../images/12.jpg)

### Support for EntityModelConfiguration

EntityModelConfiguration allows configuration to be performed for any
entity type in a model. Support has be been provided to create callLink
between method and entity operation object and appropriate crud link
between entity operation object and table,  when table name is
overridden through EntityModelConfiguration.

EntityModelConfiguration

  

``` c#
public DomainModel.Portaalstatus OpslaanLogin(Account account)        
{
            using (new LoggingMonitoredScope(LogConstants.Diagnostics))
            {
                ValidationHelper.Validate(account);
                if (ClaimHelper.IsMeekijker())
                    return new DomainModel.Portaalstatus();
                var cacheKey = CacheSettingsHelper.CreateCacheKey(account);
                                _cacheHelper.Remove(cacheKey, CacheGroups.CacheGroupPortaalStatussen);
                DomainModel.Portaalstatus cacheItem;
                using (var dbContext = new ApfPortalenDbContext())
                {
                    var portaalstatus =
                        dbContext.Portaalstatussen.FirstOrDefault(
                            p => p.AdministratieId.Equals(account.AdministratieIdentificatie) &&
                                 p.RelatienummerDeelnemerMaia.Equals(account.RelatienummerDeelnemerMAIA));
            if (portaalstatus == null)
                    {
                        portaalstatus = dbContext.Portaalstatussen.Add(new Portaalstatus());
            }
        }; 
                   
}
```

![](../images/13.jpg)

### Support for CRUD operations using dbContext 

CRUD using dbcontext

``` c#
public async Task<IActionResult> Create(Distribution distribution)
        {
            if (ModelState.IsValid)
            {
                context.Add(distribution);
                await context.SaveChangesAsync();

                return RedirectToAction("Index");
            }
            else
                return View();
        }
```

![](../images/14.jpg)

### Support for ExecuteSqlQuery

ExecuteSqlCommand

``` c#
protected void InsertInfo(List<DispatcherInfo> entities)
        {
            int chunkSize = 1000;
            var pendingBar = new PendingBar(Logger);
            pendingBar.Start();

            Entities context = new Entities();

            try
            {
                entities.GroupBy(e => e.DepotId).ToList().ForEach(g =>
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM DispatcherInfoes WHERE DepotId = {0}", g.Key);
                    context.BulkInsert(g.ToList(), chunkSize);
                    context.SaveChanges();
                });
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                pendingBar.Stop();
                if (context != null)
                    context.Dispose();
            }
        }
    }
```

  

![](../images/15.jpg)

Use of FromSql, FromSqlRaw, FromSqlInterpoated, ExecuteFunction,
ExecuteStoreCommand, ExecuteStoryQuery, ExecuteStoreCommand,
ExecuteSqlCommand and their Asynchronous counterpart results in the
similar transaction as above.



Unknown SQL Query

``` c#
protected void ResetWorkflowProgress(Entities context, DealerComplaint complaint)
        {
            context.Database.ExecuteSqlCommand(abcd);
            complaint.LastDeciderName = null;
            complaint.LastDecisionDate = null;
        }
```
![](../images/16.jpg)

### Reading of edmx file

edmx file mapping

``` c#
internal void AddDataChangeApprover(DataChangeApprover dca)
        {
            _context.DataChangeApprovers.Add(dca);
        }
```

DbContext and DbSet

``` c#
public partial class DisplayConfigEntities : DbContext
    {
        public DisplayConfigEntities()
            : base("name=DisplayConfigEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<DataChangeApprover> DataChangeApprovers { get; set; }
}
```

  

edmx

``` xml
<EntitySetMapping Name="DataChangeApprovers">
            <EntityTypeMapping TypeName="DispalyConfiguration.DataChangeApprover">
              <MappingFragment StoreEntitySet="Approval">
```

  

![](../images/17.jpg)

  

### Support for Table Per Hierarchy

Table Per Hierarchy

``` c#
public class AppDbContext : DbContext
    {
        public DbSet<CreditCard> CreditCard { get; set; }


        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BillingDetail>().ToTable("BankingBillingDetails");
        }
    }


    public abstract class BillingDetail
    {
        [Required]
        public int BillingDetailId { get; set; } 
        public string Owner { get; set; } 
        public string Number { get; set; } 
    }

    public class CreditCard : BillingDetail
    {
        public int CardType { get; set; }   
        public string ExpiryMonth { get; set; } 
        public string ExpiryYear { get; set; } 
    }
```

![](../images/18.jpg)

### Unknown Entity

Unknown Entity

``` c#
public class BaseRepository<TEntity> : IRepository<TEntity>
      where TEntity : class
    {
        private WP2AlertsContext alertsContext;

        public BaseRepository(WP2AlertsContext wP2Context)
        {
            alertsContext = wP2Context;
        }

        public virtual async Task<TEntity> AddAsync(TEntity entity)
        {
            await alertsContext.AddAsync(entity);
            alertsContext.Entry(entity).State = EntityState.Added;
            await alertsContext.SaveChangesAsync();
            return entity;
        }
    }
```

![](../images/19.jpg)

### Repository and Unit of Work Pattern

Repository and Unit of Work Pattern

``` c#
public partial class RepositoryPatternDemoContext : DbContext
    {
        
        public RepositoryPatternDemoContext()
        {
        }

        public RepositoryPatternDemoContext(DbContextOptions<RepositoryPatternDemoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Product> Product { get; set; }

        
    }

public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(RepositoryPatternDemoContext repositoryPatternDemoContext) : base (repositoryPatternDemoContext)
        {
        }

        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            return await GetAll().FirstOrDefaultAsync(x => x.Id == id);
        }
    }
```

![](../images/UnitWorkRepo.png)

## Assumptions

-   SaveChanges() method of DbContext class commits the operation in the
    database table. Hence no useLink is created between caller method
    and table.   

## Limitations

- Analyzing the participating database tables is mandatory for the extension to produce appropriate useLinks
- LINQ-Entity Query syntax is not supported
- Unknown Entity object gets created in case the entity in the CRUD transaction does not get resolved. In such a case, the caller method will get connected to the Unknown Entity Operation object of the Unknown Entity.
- Unknown SQL Query object gets created if the query used in the execution does not get resolved.
- Limited support for "repository" and "unit of work" patterns
