---
title: "Microsoft ApplicationBlocks Data - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.msappblocksdata

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for Microsoft Application Blocks Data
Library APIs (see Methods Supported) which are responsible for typical
CRUD operations with the database.

## In what situation should you install this extension?

If your C# application performs SQL queries with the Microsoft
ApplicationBlocks Data Library framework, and you want to modelize the
Client/Server links with appropriate objects and links, then you should
install this extension.

## Technology support

| Item                                                                                                                                                                                           | Version |                                         Supported                                          | Supported Technology | Notes                                                                                                                                                                                                              |
|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------|:------------------------------------------------------------------------------------------:|:--------------------:|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Microsoft Application Blocks Data Library](https://learn.microsoft.com/en-us/archive/msdn-magazine/2005/july/the-enterprise-library-data-access-application-block-part-1) | 2.0     | ![(tick)](/images/icons/emoticons/check.svg) |          C#          | Application Blocks Data Library is a separate download, see [https://www.nuget.org/packages/Microsoft.ApplicationBlocks.Data](https://www.nuget.org/packages/Microsoft.ApplicationBlocks.Data) |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For C# applications using Microsoft ApplicationBlocks Data, this
extension will be automatically installed by CAST Console. This is in
place since October 2023.

For upgrade, if the Extension Strategy is not set to Auto update, you
can manually upgrade the extension using the Application -
Extensions interface.

## What results can you expect?

The following objects and  links will be displayed in CAST
Enlighten:

### Objects

| Icon | Type Description | When is this object created ? |
|:----:|------------------|-------------------------------|
| ![](../images/642351128.png) | MS ApplicationBlocks Data SQL Query | an object is created for each SQL query found and resolved in an ApplicationBlocks Data method call  |
| ![](../images/unknown_sql_query_icon.png)  | MS ApplicationBlocks Data Unknown SQL Query | an object is created for each SQL query found and not resolved in an ApplicationBlocks Data method call

### Links

| Link type | Caller type | Callee type |  <br>Methods Supported  |
|---|---|---|---|
| callLink | C# Method | MS ApplicationBlocks Data SQL Query<br><br>MS ApplicationBlocks Data Unknown SQL Query | Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQuery<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteNonQueryTypedParams<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReader<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteReaderTypedParams<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteXmlReader<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteXmlReaderTypedParams<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalar<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteScalarTypedParams<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDataset<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.ExecuteDatasetTypedParams<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.UpdateDataset<br> <br>Microsoft.ApplicationBlocks.Data.SqlHelper.FillDataset  |
| useLink | MS ApplicationBlocks Data SQL Query | Table / View | Created by SQL Analyzer when DDL source files are analyzed |
| callLink | MS ApplicationBlocks Data SQL Query | Procedure |  |
| useLink | MS ApplicationBlocks Data SQL Query | Missing Table | Created by Missing tables and procedures for .Net extension when the object is not analyzed |
| callLink | MS ApplicationBlocks Data SQL Query | Missing Procedure |  |

### Example code scenarios

#### ExecuteDataset



ExecuteDataset

``` c#
public DataSet fConsultaRecibo(string dta_mov, string cod_sus, string num_rcb, string cod_stm, string strConnection)
        {
            StringBuilder query = new StringBuilder();
            query.Append("");
            query.Append("SELECT * FROM tes_pagamento a, tes_negocio b, tes_ocorrencias_pagamento c,");
            query.Append(" tes_cadastro_retorno_pagamento d");
            query.Append(" WHERE a.cod_idc_pgm = b.cod_idc_pgm");
            query.Append(" and a.cod_stm = b.cod_stm");
            query.Append(" and b.cod_idc_pgm = c.cod_idc_pgm_ori");
            query.Append(" and b.cod_stm = c.cod_stm");
            query.Append(" and a.cod_frm_pgm = d.cod_frm_pgm");
            query.Append(" and a.cod_ocr_pgm = d.cod_ocr_pgm");
            query.Append(" and d.ind_impr_recibo = 'S'");
            query.Append(" and c.dta_mov = '" + dta_mov + "'");
            query.Append(" and b.cod_sus = '" + cod_sus + "'");
            query.Append(" and c.num_rcb = '" + num_rcb + "'");
            query.Append(" and a.cod_stm = '" + cod_stm + "'");

            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return ms.SqlHelper.ExecuteDataset(Conexao.DBConnection, CommandType.Text, query.ToString());
        }
```

![](../images/642351120.png)

#### ExecuteNonQuery



ExecuteNonQuery

``` c#
public void sAtualizaStatus(string FLG_PGM_CMS_RPS, CLS_Lancamentos lanc)
        {
            try
            {
                StringBuilder query = new StringBuilder();
                query.Append("");

                query.Append(" Update CMS_LANCAMENTO_COMISSAO ");
                query.Append(" SET  FLG_PGM_CMS_RPS = '" + FLG_PGM_CMS_RPS + "'");
                query.Append(" WHERE COD_CRT = '" + lanc.COD_CRT + "' ");
                query.Append(" AND MES_RFR_EXT = '" + lanc.MES_RFR_EXT + "' ");
                query.Append(" AND COD_CIA_SGD ='" +lanc.COD_CIA_SGD + "' ");
                query.Append(" AND COD_EXT_CRT ='" + lanc.COD_EXT_CRT + "' ");
                query.Append(" AND COD_LNC_CMS ='" + lanc.COD_LNC_CMS+ "'");
                query.Append(" AND TIP_LNC_CMS ='" + lanc.TIP_LNC_CMS + "'");

                CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(_mainConnection.ConnectionString);
                Conexao.OpenConnection();

                ms.SqlHelper.ExecuteNonQuery(Conexao.DBConnection, CommandType.Text, query.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
```

![](../images/652050536.png)

#### ExecuteXmlReader



ExecuteXmlReader

``` c#
public DataSet fConsultaReciboExecuteXmlReader(string strConnection)
        {
            string strSql = " Select * from tmpTabPagTitSort ";

            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return ms.SqlHelper.ExecuteXmlReader(Conexao.DBConnection, CommandType.Text, strSql);
        }
```

![](../images/642351123.png)

#### ExecuteReader



ExecuteReader

``` c#
public string fConsultaProd(string codProd, string strConnection)
        {
            string query = " Select * from prd_produto_capitalizacao PC "
                         + " Inner join prd_produto_geral PG "
                         + " On PC.cod_prd = PG.cod_prd "
                         + " Where PC.cod_prd = '" + codProd + "' ";

            query += " order by PC.cod_prd";

            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return selectToXml(ms.SqlHelper.ExecuteReader(Conexao.DBConnection, CommandType.Text, query));
        }
```

![](../images/642351122.png)

#### ExecuteDataset



ExecuteDataset

``` c#
public DataSet fConsultaRecibo(string dta_mov, string cod_sus, string num_rcb, string cod_stm, string strConnection)
        {
            StringBuilder query = new StringBuilder();
            query.Append("");
            query.Append("SELECT * FROM tes_pagamento a, tes_negocio b, tes_ocorrencias_pagamento c,");
            query.Append(" tes_cadastro_retorno_pagamento d");
            query.Append(" WHERE a.cod_idc_pgm = b.cod_idc_pgm");
            query.Append(" and a.cod_stm = b.cod_stm");
            query.Append(" and b.cod_idc_pgm = c.cod_idc_pgm_ori");
            query.Append(" and b.cod_stm = c.cod_stm");
            query.Append(" and a.cod_frm_pgm = d.cod_frm_pgm");
            query.Append(" and a.cod_ocr_pgm = d.cod_ocr_pgm");
            query.Append(" and d.ind_impr_recibo = 'S'");
            query.Append(" and c.dta_mov = '" + dta_mov + "'");
            query.Append(" and b.cod_sus = '" + cod_sus + "'");
            query.Append(" and c.num_rcb = '" + num_rcb + "'");
            query.Append(" and a.cod_stm = '" + cod_stm + "'");

            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return ms.SqlHelper.ExecuteDataset(Conexao.DBConnection, CommandType.Text, query.ToString());
        }
```

![](../images/642351120.png)

#### FillDataset



FillDataset

``` c#
public DataSet fillDataSetMethod(string strConnection)
        {

            string strSql = "DELETE FROM [tkgs_cap].[dbo].[CAP_PROPOSTAS_titulares] WHERE [COD_CTR] = 'BOB'";

            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return ms.SqlHelper.FillDataset( Conexao.DBConnection,CommandType.Text, strSql);
        }
```

![](../images/642351125.png)

#### UpdateDataset



UpdateDataset

``` c#
public DataSet updateDataSetMethod(string dta_mov, string cod_sus, string num_rcb, string cod_stm, string strConnection)
        {
            string insert_strSql = "Insert into dbo.ife_ctl_datas_sistema (Cod_Ctr,cod_sts_tit ,tip_slt_res,Dta_Solicitado) values ('COB','RS', 'RES' ,Getdate() )";
            string delete_strSql = " delete from ife_ctl_datas_sistema where cod_ocr = 'CLO'";
            string update_strSql = " update ife_ctl_datas_sistema set dat_corrente = @data where cod_stm = 'COB' ";

            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return ms.SqlHelper.UpdateDataset(insert_strSql, delete_strSql, update_strSql, CommandType.Text, "ife_ctl_datas_sistema");
        }
```

![](../images/652050537.png)

#### ExecuteScalar



ExecuteScalar

``` c#
public DataSet fConsultaReciboExecuteScalar(string strConnection)
        {
            string strSql = " Select * from CAP_PROPOSTAS_titulares WHERE COD_CTR = 'BOB' ";

            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return ms.SqlHelper.ExecuteScalar(Conexao.DBConnection, CommandType.Text, strSql);
        }
```

![](../images/657686789.png)

### Unknown

ExecuteScalar

``` c#
public DataSet fConsultaReciboExecuteScalar(string strConnection)
        {
            CLS_ConnectionProvider Conexao = new CLS_ConnectionProvider(strConnection);
            Conexao.OpenConnection();

            return ms.SqlHelper.ExecuteScalar(Conexao.DBConnection, CommandType.Text, strSql);
        }
```
![](../images/msappblocks_unknown.png)

## Limitations

-   The support works when the Microsoft Application Blocks Data Library
    is used directly, that is, not through a custom wrapper delivered as
    an assembly.
