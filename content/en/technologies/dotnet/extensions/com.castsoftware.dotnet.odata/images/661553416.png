import re
import xml.etree.ElementTree as ET
from collections import OrderedDict

from cast.analysers import log, dotnet, create_link, Bookmark
from java_string import parser, StringEvaluation
from cast import Event

from constants import HttpMethods, HttpMethodsWithParams
from evaluation import convert_to_command, UNKNOWN
from exceptions import DuplicateObjectError
from utils import (
    get_bookmark,
    create_object,
    get_name_from_fully_qualified_name,
    recurse_left_and_evaluate,
    recurse_left_and_get_template_parameter,
    get_entity_from_template_param,
)


DEFAULT_ENTITY_CONTAINERS = (
    "Container",
    "DefaultContainer",
)

"""
Goal:
    - To create CAST_DotNet_ODataClient_{HttpMethod}HttpRequestService objects
      where HttpMethod can be Get, Post, Put, Patch, Delete and and link them to
      corresponding CSharp methods via 'Call Link'.
    - To link above mentioned objects via 'Call Link' to corresponding server objects created by
      other extensions using WbsLinker.
"""


class ODataClient(dotnet.Extension, StringEvaluation):
    def __init__(self):
        StringEvaluation.__init__(self)
        self.entity_containers = list(DEFAULT_ENTITY_CONTAINERS)
        self.object_full_names = OrderedDict()

    def register_callback(self, registeration):
        # Simple.OData APIs
        # registeration.register_callback("Simple.OData.Client.ODataClient.For", self.callback_simple)
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.FindEntriesAsync",
            self.callback_simple_read,
        )
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.InsertEntriesAsync",
            self.callback_simple_create,
        )
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.UpdateEntriesAsync",
            self.callback_simple_update,
        )
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.DeleteEntriesAsync",
            self.callback_simple_delete,
        )
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.FindEntryAsync", self.callback_simple_read
        )
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.InsertEntryAsync",
            self.callback_simple_create,
        )
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.UpdateEntryAsync",
            self.callback_simple_update,
        )
        registeration.register_callback(
            "Simple.OData.Client.IBoundClient.DeleteEntryAsync",
            self.callback_simple_delete,
        )

        # OData DataServiceQuery APIs
        registeration.register_callback(
            "Microsoft.OData.Client.DataServiceQuery.Execute", self.callback_get
        )
        registeration.register_callback(
            "Microsoft.OData.Client.DataServiceQuery.ExecuteAsync", self.callback_get
        )

        # OData DataServiceContext APIs
        registeration.register_callback(
            "Microsoft.OData.Client.DataServiceContext.AddObject", self.callback_add
        )
        registeration.register_callback(
            "Microsoft.OData.Client.DataServiceContext.UpdateObject",
            self.callback_change_resource,
        )
        registeration.register_callback(
            "Microsoft.OData.Client.DataServiceContext.DeleteObject",
            self.callback_change_resource,
        )
        registeration.register_callback(
            "Microsoft.OData.Client.DataServiceContext.ExecuteBatch",
            self.callback_batch_query,
        )

        # Linq APIs
        registeration.register_callback(
            "System.Linq.Queryable.Where", self.callback_get_with_param
        )
        registeration.register_callback(
            "System.Linq.Queryable.Count", self.callback_get
        )
        registeration.register_callback(
            "System.Linq.Queryable.OrderBy", self.callback_get
        )
        registeration.register_callback(
            "System.Linq.Queryable.OrderByDescending", self.callback_get
        )
        registeration.register_callback("System.Linq.Queryable.Skip", self.callback_get)
        registeration.register_callback("System.Linq.Queryable.Take", self.callback_get)
        registeration.register_callback(
            "System.Linq.Queryable.Select", self.callback_get
        )
        registeration.register_callback(
            "System.Linq.Queryable.Average", self.callback_get
        )
        registeration.register_callback("System.Linq.Queryable.Sum", self.callback_get)
        registeration.register_callback("System.Linq.Queryable.Min", self.callback_get)
        registeration.register_callback("System.Linq.Queryable.Max", self.callback_get)

    def callback_simple_create(self, caller, called, method_call, evaluator):
        self.callback_simple_handler(
            caller, called, method_call, evaluator, HttpMethodsWithParams.POST
        )

    def callback_simple_read(self, caller, called, method_call, evaluator):
        self.callback_simple_handler(
            caller, called, method_call, evaluator, HttpMethods.GET
        )

    def callback_simple_update(self, caller, called, method_call, evaluator):
        self.callback_simple_handler(
            caller, called, method_call, evaluator, HttpMethodsWithParams.PATCH
        )

    def callback_simple_delete(self, caller, called, method_call, evaluator):
        self.callback_simple_handler(
            caller, called, method_call, evaluator, HttpMethodsWithParams.DELETE
        )

    def callback_simple_handler(
        self, caller, called, method_call, evaluator, crud_type
    ):
        server_url = UNKNOWN
        controller_name = UNKNOWN
        evaluated_left = evaluator.evaluate([method_call.get_left()])[0]
        for value_key in evaluated_left:
            value = evaluated_left[value_key]
            server_url = recurse_left_and_evaluate(
                value, "Simple.OData.Client.ODataClient.ODataClient"
            )
            controller_name = recurse_left_and_evaluate(
                value, "Simple.OData.Client.ODataClient.For"
            )
        template_parameter = recurse_left_and_get_template_parameter(
            method_call.get_left(), "For"
        )
        entity_name = get_entity_from_template_param(template_parameter)
        if not entity_name:
            return
        entity_name = normalise_entity_name(entity_name, controller_name)
        object_name = self.get_object_name_from_entity_name(
            entity_name, crud_type, server_url
        )
        caller_file = self._symbols.get_position(caller).get_file()
        bookmark = get_bookmark(caller, method_call, caller_file)
        full_name = "".join([caller.get_fullname(), ".", object_name])
        if full_name not in self.object_full_names:
            client_object = create_object(
                object_name,
                crud_type.value,
                caller,
                full_name=full_name,
                guid=full_name,
                bookmark=bookmark,
            )
            self.object_full_names[full_name] = client_object
        else:
            client_object = self.object_full_names[full_name]
        create_link("callLink", caller, client_object, bookmark)

    def callback_batch_query(self, caller, called, method_call, evaluator):
        probable_entities = []

        evaluated_left = evaluator.evaluate([method_call.get_left()])[0]
        server_url = UNKNOWN
        for value_key in evaluated_left:
            value = evaluated_left[value_key]
            server_url = convert_to_command(value, containers=self.entity_containers)
        for param in method_call.get_parameters():
            resolved_param = param.get_resolution()
            for child in resolved_param.get_children():
                for child_1 in child.get_children():
                    if probable_entities and probable_entities[-1] == ".":
                        entity_name = child_1.text
                        object_name = self.get_object_name_from_entity_name(
                            entity_name, HttpMethods.GET, server_url
                        )
                        caller_file = self._symbols.get_position(caller).get_file()
                        bookmark = get_bookmark(caller, method_call, caller_file)
                        full_name = "".join([caller.get_fullname(), ".", object_name])
                        if full_name not in self.object_full_names:
                            client_object = create_object(
                                object_name,
                                HttpMethods.GET.value,
                                caller,
                                full_name=full_name,
                                guid=full_name,
                                bookmark=bookmark,
                            )
                            self.object_full_names[full_name] = client_object
                        else:
                            client_object = self.object_full_names[full_name]
                        create_link("callLink", caller, client_object, bookmark)
                    probable_entities.append(child_1.text)

    def callback_add(self, caller, called, method_call, evaluator):
        left_of_method = method_call.get_left().get_name()
        evaluated_left = evaluator.evaluate([method_call.get_left()])[0]
        server_url = UNKNOWN
        for value_key in evaluated_left:
            value = evaluated_left[value_key]
            server_url = convert_to_command(value, containers=self.entity_containers)
        evaluated_parameter = evaluator.evaluate([method_call.get_parameters()[1]])[0]
        for val in evaluated_parameter.values():
            history = val.get_history()[0].get_statement()
            for child in history.get_children():
                for child_1 in child.get_children():
                    if left_of_method in (child_1.text or ""):
                        tokens = child_1.text.split(".")
                        entity_name = tokens[tokens.index(left_of_method) + 1]
                        object_name = self.get_object_name_from_entity_name(
                            entity_name, HttpMethodsWithParams.POST, server_url
                        )
                        caller_file = self._symbols.get_position(caller).get_file()
                        bookmark = get_bookmark(caller, method_call, caller_file)
                        full_name = "".join([caller.get_fullname(), ".", object_name])
                        if full_name not in self.object_full_names:
                            client_object = create_object(
                                object_name,
                                HttpMethods.POST.value,
                                caller,
                                full_name=full_name,
                                guid=full_name,
                                bookmark=bookmark,
                            )
                            self.object_full_names[full_name] = client_object
                        else:
                            client_object = self.object_full_names[full_name]
                        create_link("callLink", caller, client_object, bookmark)

    def callback_change_resource(self, caller, called, method_call, evaluator):
        left_of_method = method_call.get_left().get_name()
        evaluated_left = evaluator.evaluate([method_call.get_left()])[0]
        server_url = UNKNOWN
        for value_key in evaluated_left:
            value = evaluated_left[value_key]
            server_url = convert_to_command(value, containers=self.entity_containers)
        evaluated_parameter = evaluator.evaluate([method_call.get_parameters()[0]])[0]
        for val in evaluated_parameter.values():
            history = val.get_history()[0].get_statement()
            for child in history.get_children():
                for child_1 in child.get_children():
                    if left_of_method in (child_1.text or ""):
                        tokens = child_1.text.split(".")
                        entity_name = tokens[tokens.index(left_of_method) + 1]
                        object_name = self.get_object_name_from_entity_name(
                            entity_name, HttpMethodsWithParams.DELETE, server_url
                        )
                        caller_file = self._symbols.get_position(caller).get_file()
                        bookmark = get_bookmark(caller, method_call, caller_file)
                        full_name = "".join([caller.get_fullname(), ".", object_name])
                        if full_name not in self.object_full_names:
                            client_object = create_object(
                                object_name,
                                HttpMethodsWithParams.DELETE.value,
                                caller,
                                full_name=full_name,
                                guid=full_name,
                                bookmark=bookmark,
                            )
                            self.object_full_names[full_name] = client_object
                        else:
                            client_object = self.object_full_names[full_name]
                        create_link("callLink", caller, client_object, bookmark)

    def callback_get(self, caller, called, method_call, evaluator):
        entity_name, server_url = self.get_entity_name(method_call, evaluator)
        if not entity_name:
            return
        object_name = self.get_object_name_from_entity_name(
            entity_name, HttpMethods.GET, server_url
        )
        caller_file = self._symbols.get_position(caller).get_file()
        bookmark = get_bookmark(caller, method_call, caller_file)
        full_name = "".join([caller.get_fullname(), ".", object_name])
        if full_name not in self.object_full_names:
            client_object = create_object(
                object_name,
                HttpMethods.GET.value,
                caller,
                full_name=full_name,
                guid=full_name,
                bookmark=bookmark,
            )
            self.object_full_names[full_name] = client_object
        else:
            client_object = self.object_full_names[full_name]
        create_link("callLink", caller, client_object, bookmark)

    def callback_get_with_param(self, caller, called, method_call, evaluator):
        entity_name, server_url = self.get_entity_name(method_call, evaluator)
        if not entity_name:
            return
        object_name = self.get_object_name_from_entity_name(
            entity_name, HttpMethodsWithParams.GET, server_url
        )
        caller_file = self._symbols.get_position(caller).get_file()
        bookmark = get_bookmark(caller, method_call, caller_file)
        full_name = "".join([caller.get_fullname(), ".", object_name])
        if full_name not in self.object_full_names:
            client_object = create_object(
                object_name,
                HttpMethods.GET.value,
                caller,
                full_name=full_name,
                guid=full_name,
                bookmark=bookmark,
            )
            self.object_full_names[full_name] = client_object
        else:
            client_object = self.object_full_names[full_name]
        create_link("callLink", caller, client_object, bookmark)

    def start_analysis(self, options):
        log.info("Start OData Analysis")

    def introducing_file(self, file):
        if file.get_path().endswith(".xml"):
            container_names = get_container_names_from_csdl_xml(file.get_path())
            self.entity_containers.extend(container_names)

    @Event("com.castsoftware.internal.platform", "dotnet.symbols")
    def register_symbols(self, symbols):
        self._symbols = symbols

    def get_entity_name(self, method_call, evaluator):
        is_odata_method = False
        entity_name = None
        server_url = None

        for child in method_call.get_left().get_children():
            if is_odata_method and child.text != ".":
                entity_name = child.text
            elif is_odata_method:
                continue
            try:
                evaluated_value = evaluator.evaluate([child])[0]
            except IndexError:
                evaluated_value = {}
            for value_key in evaluated_value:
                value = evaluated_value[value_key]
                if (
                    value.is_method_call()
                    and get_name_from_fully_qualified_name(value.get_method_fullname())
                    in self.entity_containers
                ):
                    server_url = convert_to_command(
                        value, containers=self.entity_containers
                    )
                    is_odata_method = True
        return entity_name, server_url

    def get_object_name_from_entity_name(self, entity_name, http_type, server_url):
        server_url = self.normalise_server_url(server_url)
        if http_type in (HttpMethods.GET, HttpMethods.POST):
            return (
                server_url + "/" + entity_name if server_url != UNKNOWN else entity_name
            )
        else:
            return (
                server_url + "/" + entity_name + "/{}"
                if server_url != UNKNOWN
                else entity_name + "/{}"
            )

    def normalise_server_url(self, server_url):
        return server_url.split("?")[0].rstrip("/")

def normalise_entity_name(entity_name, controller_name):
    if controller_name != UNKNOWN:
        return controller_name
    return entity_name + "s"

def get_container_names_from_csdl_xml(file_path):
    container_names = []
    element_tree = ET.parse(file_path)
    root = element_tree.getroot()
    root_tag_name = get_tag_name_without_namespace(root)
    if root_tag_name == "Edmx":
        for level_0_child in root:
            for level_1_child in level_0_child:
                for level_2_child in level_1_child:
                    tag_name = get_tag_name_without_namespace(level_2_child)
                    if tag_name == "EntityContainer" and level_2_child.attrib.get(
                        "Name"
                    ):
                        container_names.append(level_2_child.attrib.get("Name"))
    return container_names


def get_tag_name_without_namespace(xml_element):
    try:
        tag_name_without_namespace = xml_element.tag.split("}")[1]
    except IndexError:
        tag_name_without_namespace = xml_element.tag
    return tag_name_without_namespace
