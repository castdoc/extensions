---
title: "OData Client for .NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.odata

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for OData client libraries when used
inside .NET source code. 

## In what situation should you install this extension?

If your .NET application source code uses the odata client
libraries to consume OData services, you should install this
extension.

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Support

#### Supported Libraries

| Library Name                                                                                                                                     | Library Version(s) | OData Version(s) |
| ------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------ | ---------------- |
| [Microsoft.OData.Client](https://learn.microsoft.com/en-us/odata/client/getting-started)                                                         | Upto 7.17          | v4               |
| [Microsoft.Data.Services.Client](https://learn.microsoft.com/en-us/previous-versions/dotnet/framework/data/wcf/wcf-data-services-client-library) | Upto 5.8           | v1-v3            |
| [Simple.OData.Client](https://github.com/simple-odata-client/Simple.OData.Client)                                                                | Upto 6.0           | v1-v4            |


#### Supported APIs

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-941421200" class="expand-container">
<div id="expander-control-941421200" class="expand-control"
aria-expanded="true">
OData v4 DataServiceQuery APIs
</div>
<div id="expander-content-941421200" class="expand-content">
<ul>
<li>Microsoft.OData.Client.DataServiceQuery.Execute</li>
<li>Microsoft.OData.Client.DataServiceQuery.ExecuteAsync</li>
</ul>
</div>
</div>
<div id="expander-1071244246" class="expand-container">
<div id="expander-control-1071244246" class="expand-control"
aria-expanded="true">
OData v4 DataServiceContext APIs
</div>
<div id="expander-content-1071244246" class="expand-content">
<ul>
<li>Microsoft.OData.Client.DataServiceContext.AddObject</li>
<li>Microsoft.OData.Client.DataServiceContext.CreateQuery</li>
<li>Microsoft.OData.Client.DataServiceContext.DeleteObject</li>
<li>Microsoft.OData.Client.DataServiceContext.Execute</li>
<li>Microsoft.OData.Client.DataServiceContext.ExecuteBatch</li>
<li>Microsoft.OData.Client.DataServiceContext.UpdateObject</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1354570540" class="expand-container">
<div id="expander-control-1354570540" class="expand-control"
aria-expanded="true">
OData v1-v3 DataServiceQuery APIs
</div>
<div id="expander-content-1354570540" class="expand-content">
<ul>
<li>System.Data.Services.Client.DataServiceQuery.AddQueryOption</li>
<li>System.Data.Services.Client.DataServiceQuery.Execute</li>
<li>System.Data.Services.Client.DataServiceQuery.Expand</li>
<li>System.Data.Services.Client.DataServiceQuery.IncludeTotalCount</li>
</ul>
</div>
</div>
<div id="expander-1874413922" class="expand-container">
<div id="expander-control-1874413922" class="expand-control"
aria-expanded="true">
OData v1-v3 DataServiceContext APIs
</div>
<div id="expander-content-1874413922" class="expand-content">
<ul>
<li>System.Data.Services.Client.DataServiceContext.AddObject</li>
<li>System.Data.Services.Client.DataServiceContext.CreateQuery</li>
<li>System.Data.Services.Client.DataServiceContext.DeleteObject</li>
<li>System.Data.Services.Client.DataServiceContext.Execute</li>
<li>System.Data.Services.Client.DataServiceContext.UpdateObject</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-1472878699" class="expand-container">
<div id="expander-control-1472878699" class="expand-control"
aria-expanded="true">
Simple.OData APIs
</div>
<div id="expander-content-1472878699" class="expand-content">
<ul>
<li>Simple.OData.Client.IBoundClient.FindEntriesAsync</li>
<li>Simple.OData.Client.IBoundClient.InsertEntriesAsync</li>
<li>Simple.OData.Client.IBoundClient.UpdateEntriesAsync</li>
<li>Simple.OData.Client.IBoundClient.DeleteEntriesAsync</li>
<li>Simple.OData.Client.IBoundClient.FindEntryAsync</li>
<li>Simple.OData.Client.IBoundClient.InsertEntryAsync</li>
<li>Simple.OData.Client.IBoundClient.UpdateEntryAsync</li>
<li>Simple.OData.Client.IBoundClient.DeleteEntryAsync</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<div id="expander-413718521" class="expand-container">
<div id="expander-control-413718521" class="expand-control"
aria-expanded="true">
LINQ Enumerable APIs
</div>
<div id="expander-content-413718521" class="expand-content">
<ul>
<li>System.Linq.Enumerable.All</li>
<li>System.Linq.Enumerable.Any</li>
<li>System.Linq.Enumerable.Append</li>
<li>System.Linq.Enumerable.AsEnumerable</li>
<li>System.Linq.Enumerable.Average</li>
<li>System.Linq.Enumerable.Cast</li>
<li>System.Linq.Enumerable.Chunk</li>
<li>System.Linq.Enumerable.Concat</li>
<li>System.Linq.Enumerable.Contains</li>
<li>System.Linq.Enumerable.Count</li>
<li>System.Linq.Enumerable.DefaultIfEmpty</li>
<li>System.Linq.Enumerable.Distinct</li>
<li>System.Linq.Enumerable.DistinctBy</li>
<li>System.Linq.Enumerable.ElementAt</li>
<li>System.Linq.Enumerable.ElementAtOrDefault</li>
<li>System.Linq.Enumerable.Except</li>
<li>System.Linq.Enumerable.ExceptBy</li>
<li>System.Linq.Enumerable.First</li>
<li>System.Linq.Enumerable.FirstOrDefault</li>
<li>System.Linq.Enumerable.GroupBy</li>
<li>System.Linq.Enumerable.GroupJoin</li>
<li>System.Linq.Enumerable.Intersect</li>
<li>System.Linq.Enumerable.IntersectBy</li>
<li>System.Linq.Enumerable.Join</li>
<li>System.Linq.Enumerable.Last</li>
<li>System.Linq.Enumerable.LastOrDefault</li>
<li>System.Linq.Enumerable.LongCount</li>
<li>System.Linq.Enumerable.Max</li>
<li>System.Linq.Enumerable.MaxBy</li>
<li>System.Linq.Enumerable.Min</li>
<li>System.Linq.Enumerable.MinBy</li>
<li>System.Linq.Enumerable.OfType</li>
<li>System.Linq.Enumerable.Order</li>
<li>System.Linq.Enumerable.OrderBy</li>
<li>System.Linq.Enumerable.OrderByDescending</li>
<li>System.Linq.Enumerable.OrderDescending</li>
<li>System.Linq.Enumerable.Prepend</li>
<li>System.Linq.Enumerable.Reverse</li>
<li>System.Linq.Enumerable.Select</li>
<li>System.Linq.Enumerable.SelectMany</li>
<li>System.Linq.Enumerable.SequenceEqual</li>
<li>System.Linq.Enumerable.Single</li>
<li>System.Linq.Enumerable.SingleOrDefault</li>
<li>System.Linq.Enumerable.Skip</li>
<li>System.Linq.Enumerable.SkipLast</li>
<li>System.Linq.Enumerable.SkipWhile</li>
<li>System.Linq.Enumerable.Sum</li>
<li>System.Linq.Enumerable.Take</li>
<li>System.Linq.Enumerable.TakeLast</li>
<li>System.Linq.Enumerable.TakeWhile</li>
<li>System.Linq.Enumerable.ThenBy</li>
<li>System.Linq.Enumerable.ThenByDescending</li>
<li>System.Linq.Enumerable.Union</li>
<li>System.Linq.Enumerable.UnionBy</li>
<li>System.Linq.Enumerable.Where</li>
<li>System.Linq.Enumerable.Zip</li>
</ul>
</div>
</div>
<div id="expander-2013484768" class="expand-container">
<div id="expander-control-2013484768" class="expand-control"
aria-expanded="true">
LINQ Queryable APIs
</div>
<div id="expander-content-2013484768" class="expand-content">
<ul>
<li>System.Linq.Queryable.All</li>
<li>System.Linq.Queryable.Any</li>
<li>System.Linq.Queryable.Append</li>
<li>System.Linq.Queryable.AsQueryable</li>
<li>System.Linq.Queryable.Average</li>
<li>System.Linq.Queryable.Cast</li>
<li>System.Linq.Queryable.Chunk</li>
<li>System.Linq.Queryable.Concat</li>
<li>System.Linq.Queryable.Contains</li>
<li>System.Linq.Queryable.Count</li>
<li>System.Linq.Queryable.DefaultIfEmpty</li>
<li>System.Linq.Queryable.Distinct</li>
<li>System.Linq.Queryable.DistinctBy</li>
<li>System.Linq.Queryable.ElementAt</li>
<li>System.Linq.Queryable.ElementAtOrDefault</li>
<li>System.Linq.Queryable.Except</li>
<li>System.Linq.Queryable.ExceptBy</li>
<li>System.Linq.Queryable.First</li>
<li>System.Linq.Queryable.FirstOrDefault</li>
<li>System.Linq.Queryable.GroupBy</li>
<li>System.Linq.Queryable.GroupJoin</li>
<li>System.Linq.Queryable.Intersect</li>
<li>System.Linq.Queryable.IntersectBy</li>
<li>System.Linq.Queryable.Join</li>
<li>System.Linq.Queryable.Last</li>
<li>System.Linq.Queryable.LastOrDefault</li>
<li>System.Linq.Queryable.LongCount</li>
<li>System.Linq.Queryable.Max</li>
<li>System.Linq.Queryable.MaxBy</li>
<li>System.Linq.Queryable.Min</li>
<li>System.Linq.Queryable.MinBy</li>
<li>System.Linq.Queryable.OfType</li>
<li>System.Linq.Queryable.Order</li>
<li>System.Linq.Queryable.OrderBy</li>
<li>System.Linq.Queryable.OrderByDescending</li>
<li>System.Linq.Queryable.OrderDescending</li>
<li>System.Linq.Queryable.Prepend</li>
<li>System.Linq.Queryable.Reverse</li>
<li>System.Linq.Queryable.Select</li>
<li>System.Linq.Queryable.SelectMany</li>
<li>System.Linq.Queryable.SequenceEqual</li>
<li>System.Linq.Queryable.Single</li>
<li>System.Linq.Queryable.SingleOrDefault</li>
<li>System.Linq.Queryable.Skip</li>
<li>System.Linq.Queryable.SkipLast</li>
<li>System.Linq.Queryable.SkipWhile</li>
<li>System.Linq.Queryable.Sum</li>
<li>System.Linq.Queryable.Take</li>
<li>System.Linq.Queryable.TakeLast</li>
<li>System.Linq.Queryable.TakeWhile</li>
<li>System.Linq.Queryable.ThenBy</li>
<li>System.Linq.Queryable.ThenByDescending</li>
<li>System.Linq.Queryable.Union</li>
<li>System.Linq.Queryable.UnionBy</li>
<li>System.Linq.Queryable.Where</li>
<li>System.Linq.Queryable.Zip</li>
</ul>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

## What source code is needed by our analyzer?

For applications using Microsoft libraries, the OData extension requires
the client proxy file, service reference file (.xml, .edmx etc.)
generated by the code generation tool to function correctly. 

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The OData extension requires the
following CAST extensions to be installed:

-   [Universal Linker](../../../../multi/com.castsoftware.wbslinker/)
-   com.castsoftware.internal.platform (Internal_Extension)

Any dependent extensions are automatically downloaded and
    installed. You do not need to do anything. The [.NET Analyzer](../../com.castsoftware.dotnet/)is not a dependency, but this
    extension will be automatically installed when .NET source code is
    delivered for analysis.

## Download and installation instructions

For .NET applications using OData client libraries, the extension will
be automatically installed by CAST Console. This is in place since
October 2023.

For upgrade, if the Extension Strategy is not set to Auto update, you
can manually install the extension using the Application -
Extensions interface.

## What results can you expect?

### Objects

| Icon | Description                             |
|:----:|-----------------------------------------|
| ![](../images/649330786.png)     | OData Client HttpRequest Get Service    |
| ![](../images/649330785.png)     | OData Client HttpRequest Post Service   |
| ![](../images/649330784.png)     | OData Client HttpRequest Put Service    |
| ![](../images/649330785.png)     | OData Client HttpRequest Patch Service  |
| ![](../images/649330783.png)     | OData Client HttpRequest Delete Service |

### Links

| Link Type | Source and Destination                                                                            |
|-----------|---------------------------------------------------------------------------------------------------|
| callLink  | Link from C# method making API call to  service object (eg. OData Client HttpRequest Get Service) |

### Examples

#### Insert Operation

``` c#
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Services.Client;
using WpfApplication1.ServiceReference1;

namespace WpfApplication1
{
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Window1_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var svcUri = "http://localhost:52027/WcfDataService1.svc";
                var context = new castEntities(new Uri(svcUri));
                WpfApplication1.ServiceReference1.Table newProduct = WpfApplication1.ServiceReference1.Table.CreateTable(4);
                newProduct.Name = "Rian";
                context.AddObject("Tables", newProduct);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
} 
```

![](../images/673841198.png)

#### Select Operation

``` c#
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication2.Models;


namespace odata_client_new
{
    class Program
    {
        static void Main(string[] args)
        {
            ListProduct().Wait();
        }

        static async Task ListProduct()
        {
            var serviceRoot = "http://localhost:64700/odata";
            var context = new Default.Container(new Uri(serviceRoot));

            IEnumerable<Product> products = await context.Products.ExecuteAsync();
            foreach (var product in products)
            {
                Console.WriteLine("{0} --- {1}", product.ID, product.Name);
            }
            
        }
    }
}  
```

![](../images/673841200.png)

#### Update Operation





``` c#
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Services.Client;
using WpfApplication1.ServiceReference1;
 
namespace WpfApplication1
{
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }
 
        private void Window1_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Uri svcUri = new Uri("http://localhost:52027/WcfDataService1.svc");
                context = new castEntities(svcUri);
                var data_to_change = (from table in context.Tables
                                  where table.Name == "Sam"
                                  select table).Single();
                data_to_change.Name = "Wiley";
                context.UpdateObject(data_to_change);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
```
![](../images/OData_Update.png)
#### Delete Operation



  

``` c#
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication2.Models;


namespace odata_client_new
{
    class Program
    {
        static void Main(string[] args)
        {
            DeleteCustomer().Wait();
        }


        static async Task DeleteCustomer()
        {
            var serviceRoot = "http://localhost:64700/odata";
            var context = new Default.Container(new Uri(serviceRoot));

            var customer = await context.Customers.ByKey(new Dictionary<string, object>() { { "ID", 002 } }).GetValue();
            Console.WriteLine(customer.Name);
            context.DeleteObject(customer);
            context.SaveChanges();
        }
    }
}
```

![](../images/673841199.png)


[Universal Linker](../../../../multi/com.castsoftware.wbslinker/) is responsible for linking
service object (eg. OData Client HttpRequest Get Service) to any
matching operation objects. (Operation objects are generated by the
analysis of server code, refer to [com.castsoftware.dotnetweb](../../com.castsoftware.dotnetweb/)).

## Limitations

-   For applications using Microsoft.OData.Client, only client code
    generated by OData Connected Service is supported.

-   For applications using Microsoft.OData.Client, methods like 'ByKey'
    which are defined in the auto-generated class 'ExtensionMethods' are
    not supported.

-   Actions and Functions are supported only for Simple.OData.Client.

-   For applications using Microsoft.Data.Services.Client, objects are
    not created for some code.

    Example

    ``` c#
    Uri svcUri = new Uri("http://localhost:52027/WcfDataService1.svc");
    context = new castEntities(svcUri);
    DataServiceQuery<WpfApplication1.ServiceReference1.Customer> query = context.Customers;

    DataServiceCollection<WpfApplication1.ServiceReference1.Customer> records = new
        DataServiceCollection<WpfApplication1.ServiceReference1.Customer>(query);
    this.orderItemsGrid.DataContext = records;
    ```
