---
title: ".NET MSMQ - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.msmq

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for **MSMQ .NET APIs** which are responsible for send and receive operations in .NET applications. If your C# application utilizes MSMQ for messaging-related operations, the send and receive are handled using the C# language, and you want to modelize the sender and receiver links with appropriate objects and links, then you should install this extension.

## Supported libraries

| Library | Version | Supported |
|---|---|:---:|
| [System.Messaging](https://learn.microsoft.com/en-us/dotnet/api/system.messaging?view=netframework-4.8.1) | up to: 4.x | :white_check_mark:  |
| [MSMQ.Messaging](https://www.nuget.org/packages/MSMQ.Messaging) | up to 1.x | :white_check_mark: |
| [Experimental.System.Messaging](https://github.com/dotnetdev-kr/Experimental.System.Messaging) | up to 1.x | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in order to function correctly. The .NET NMS extension requires that
the following other CAST extensions are also installed (this will be managed automatically):

- [Internal Extension](https://extend.castsoftware.com/#/extension?id=com.castsoftware.internal.platform&version=latest)
- [Universal Linker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest)

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, you should manually install the extension.

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/669253775.png) | MSMQ Sender |
| ![](../images/669253773.png) | MSMQ Receiver |
| ![](../images/669253774.png) | MSMQ unknown Sender |
| ![](../images/669253772.png) | MSMQ unknown Receiver |

### Links

<table>
  <thead>
    <tr>
      <th>Link Type</th>
      <th>Source and Destination of link</th>
      <th>Supported APIs</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>callLink</td>
      <td>callLink between the caller C# method and the Sender object</td>
      <td>
        <details>
          <summary>Sender APIs</summary>
          <ul>
            <li>System.Messaging.MessageQueue.Send</li>
            <li>MSMQ.Messaging.MessageQueue.Send</li>
            <li>Experimental.System.Messaging.MessageQueue.Send</li>
          </ul>
        </details>
      </td>
    </tr>
    <tr>
      <td>callLink</td>
      <td>callLink between the Receiver object and the caller C# method</td>
      <td>
        <details>
          <summary>Receiver APIs</summary>
          <ul>
            <li>System.Messaging.MessageQueue.Receive</li>
            <li>System.Messaging.MessageQueue.ReceiveById</li>
            <li>System.Messaging.MessageQueue.ReceiveByCorrelationId</li>
            <li>System.Messaging.MessageQueue.BeginReceive</li>
            <li>System.Messaging.MessageQueue.ReceiveByLookupId</li>
            <li>System.Messaging.MessageQueue.Peek</li>
            <li>System.Messaging.MessageQueue.PeekById</li>
            <li>System.Messaging.MessageQueue.PeekByCorrelationId</li>
            <li>System.Messaging.MessageQueue.PeekByLookupId</li>
            <li>System.Messaging.MessageQueue.BeginPeek</li>
            <li>System.Messaging.MessageQueue.GetAllMessages</li>
            <li>MSMQ.Messaging.MessageQueue.Receive</li>
            <li>MSMQ.Messaging.MessageQueue.ReceiveById</li>
            <li>MSMQ.Messaging.MessageQueue.ReceiveByLookupId</li>
            <li>MSMQ.Messaging.MessageQueue.ReceiveByCorrelationId</li>
            <li>MSMQ.Messaging.MessageQueue.Peek</li>
            <li>MSMQ.Messaging.MessageQueue.PeekById</li>
            <li>MSMQ.Messaging.MessageQueue.PeekByCorrelationId</li>
            <li>MSMQ.Messaging.MessageQueue.PeekByLookupId</li>
            <li>MSMQ.Messaging.MessageQueue.BeginPeek</li>
            <li>MSMQ.Messaging.MessageQueue.BeginReceive</li>
            <li>MSMQ.Messaging.MessageQueue.GetAllMessages</li>
            <li>Experimental.System.Messaging.MessageQueue.Receive</li>
            <li>Experimental.System.Messaging.MessageQueue.ReceiveById</li>
            <li>Experimental.System.Messaging.MessageQueue.ReceiveByCorrelationId</li>
            <li>Experimental.System.Messaging.MessageQueue.ReceiveByLookupId</li>
            <li>Experimental.System.Messaging.MessageQueue.Peek</li>
            <li>Experimental.System.Messaging.MessageQueue.PeekById</li>
            <li>Experimental.System.Messaging.MessageQueue.PeekByLookupId</li>
            <li>Experimental.System.Messaging.MessageQueue.PeekByCorrelationId</li>
            <li>Experimental.System.Messaging.MessageQueue.BeginReceive</li>
            <li>Experimental.System.Messaging.MessageQueue.BeginPeek</li>
            <li>Experimental.System.Messaging.MessageQueue.GetAllMessages</li>
          </ul>
        </details>
      </td>
    </tr>
  </tbody>
</table>


## Example code scenarios

### System.Messaging

#### Sender APIs

``` c#
using System;
using System.Collections.Generic;
using System.Messaging;
using Castle.Windsor;
using DddCqrsEsExample.Framework;
using Newtonsoft.Json;

namespace DddCqrsEsExample.Web2.Infrastructure
{
    public class MsmqEventBus : IEventBus
    {
        private readonly IWindsorContainer _container;

        private const string QueueName = @".\private$\DddCqrsEsQueue";

        public MsmqEventBus(IWindsorContainer container)
        {
            if (container == null) throw new ArgumentNullException(nameof(container));
            _container = container;
        }

        public void Publish<TEvent>(TEvent evt) where TEvent : Event
        {
            HandleEvent(evt);

            PublishOnMsmq(evt);
        }

        private void HandleEvent<TEvent>(TEvent evt) where TEvent : Event
        {
            var handlerType = typeof(IEventHandler<>).MakeGenericType(evt.GetType());
            var handlers = _container.ResolveAll(handlerType);
            foreach (var handler in handlers)
            {
                try
                {
                    var handleMethod = handler.GetType().GetMethod("Handle");
                    handleMethod.Invoke(handler, new object[] {evt});
                }
                finally
                {
                    _container.Release(handler);
                }
            }
        }

        private static void PublishOnMsmq<TEvent>(TEvent evt) where TEvent : Event
        {
            if (!MessageQueue.Exists(QueueName))
            {
                MessageQueue.Create(QueueName);
            }

            using (var q = new MessageQueue(QueueName))
            {
                q.DefaultPropertiesToSend.Recoverable = true;

                q.Send(JsonConvert.SerializeObject(evt, Formatting.Indented) + "|" + evt.GetType().AssemblyQualifiedName);
            }
        }
```

![](../images/send_01.png)

### Receiver APIs

``` c#
using System;
using System.Messaging;
using DddCqrsEsExample.Framework;
using DddCqrsEsExample.ThinReadLayer.Core;
using Newtonsoft.Json;

namespace DddCqrsEsExample.ThinReadLayer.EventListener
{
    public class Listener
    {
        private const string QueueName = @".\private$\DddCqrsEsQueue";
        private MessageQueue _listeningQueue;

        public void Start()
        {
            if (!MessageQueue.Exists(QueueName))
            {
                MessageQueue.Create(QueueName);
            }

            try
            {
                if (MessageQueue.Exists(QueueName))
                {
                    _listeningQueue = new MessageQueue(QueueName);
                    _listeningQueue.ReceiveCompleted +=
                        (sender, args) =>
                        {
                            args.Message.Formatter = new XmlMessageFormatter(new[] {typeof(string)});
                            var msg = args.Message.Body.ToString();

                            Console.WriteLine("Message received:{0}", msg);

                            var parts = msg.Split('|');
                            var json = parts[0];
                            var typeName = parts[1];
                            var type = Type.GetType(typeName);
                            var evt = (Event)JsonConvert.DeserializeObject(json, type);

                            new Denormaliser().StoreEvent(evt);

                            _listeningQueue.BeginReceive();
                        };
                    _listeningQueue.BeginReceive();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("Unable to initialise message queue on your local machine.\r\n\r\n{0}", e));
            }
        }
    }
}
```

![](../images/receive_01.png)

#### Dotnet Sender and Receiver

When the queue path matches for both the sender and receiver, a `callLink` will be established between the sender object and the receiver object:

![](../images/send_receive_01.png)

## Known limitations

- Unknown Sender/Receiver objects will be created if the evaluation fails to resolve the necessary parameter.
