---
title: "SOAP Service Calls for .NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.soap

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for SOAP services in .NET applications, specifically focusing on ASMX (ASP .NET) and SoapCore implementations. This extension works in complement with the **com.castsoftware.dotnet**, **com.castsoftware.dotnetweb**, and **com.castsoftware.wcf** extensions, each addressing specific libraries and patterns of SOAP in the .NET ecosystem.

## In what situation should you install this extension?

If your C# application utilizes SOAP based web services such as ASMX or SoapCore and you want to view objects with their corresponding links then you should install this extension.

## Technology support

SOAP is built-in features of the .NET Framework:

| Language |  Version | Supported | Comment |
|---|---|:-:|:-:|
| .NET Framework |  1.1 to 4.8 | :white_check_mark: | Supports ASMX Web Services  (ASP.NET)  |

### SoapCore
| SoapCore version | .NET | .NET Core |.NET Standard | Supported | 
|---|---|---|---|:-:|
| 1.1.0.21 - 1.1.0.51 | 5.0 - 6.0 | 3.1 | 2.0 - 2.1 | :white_check_mark: |   
| 1.1.0.10 - 1.1.0.20 | 5.0 |2.1 - 3.1 | 2.0 | :white_check_mark: |
| 1.0.0 - 1.1.0.9 | - | 2.1 - 3.1 | 2.0 | :white_check_mark: |   

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility 

This extension is compatible with:

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies

The com.castsoftware.dotnet.soap extension is dependent on following other extensions:

- com.castsoftware.internal.platform
- [com.castsoftware.wbslinker](https://extend.castsoftware.com/#/extension?id=com.castsoftware.wbslinker&version=latest)

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, you should manually install the
extension using the Extensions interface:

![](../images/Installation.png)

## What results can you expect?

### Objects

| Icon | Description | Comment |
|---|---|---|
| ![](../images/CAST_DotNet_SOAP_Operation-3.png) | DotNet SOAP Operation | an object is created when we encounter [WebMethod] attribute (or) an object is created for each method of specified class in SoapCore endpoint  |
| ![](../images/CAST_DotNet_SOAP_Operation-3.png) | DotNet SOAP Operation Call | an object is created for each SOAP operation call and Web service method is resolved  |
| ![](../images/CAST_DotNet_SOAP_Unknown_OperationCall-3.png) | DotNet SOAP Unknown Operation Call | an object is created for each SOAP operation call and Web service method is not resolved  |

### Link Types

| Link Type | Source and Destination Link | Supported APIs |
|---|---|---|
| callLink | Link between the caller C# method and the DotNet SOAP Operation Call object | <ul><li>System.Web.Services.Protocols.SoapHttpClientProtocol.Invoke<li>System.Web.Services.Protocols.SoapHttpClientProtocol.InvokeAsync</li><li>System.Web.Services.Protocols.SoapHttpClientProtocol.BeginInvoke</li></ul> |
| callLink | Link between the DotNet SOAP Operation object and C# method | <ul><li>System.Web.Services.WebMethodAttribute</li><li>SoapCore.SoapEndpointExtensions.UseSoapEndpoint</li></ul> |

## Code Examples

### Server Side

#### SOAP Operation  


```c#
// Methods with [WebMethod] are Web services
[WebMethod]
public bool InsertAssessment(Assessment a)
{
    return DAL.InsertAssessment(a);
}//close InsertAssessment

```
![](../images/soap_server.png)

### Client Side

#### SOAP Operation Call
wsdl file
```yaml
<wsdl:definitions xmlns:tns="http://tempuri.org/"  xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" targetNamespace="http://tempuri.org/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:portType name="WebService1Soap">
    <wsdl:operation name="InsertAssessment">
      <wsdl:input message="tns:InsertAssessmentSoapIn" />
      <wsdl:output message="tns:InsertAssessmentSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
</wsdl:definitions>
```
- wsdl file contains "portType"
```c#
public bool InsertAssessment(Assessment a) {
    object[] results = this.Invoke("InsertAssessment", new object[] {
                a});
    return ((bool)(results[0]));
}
```

![](../images/soap_client_1.png)

#### SOAP Unknown Operation Call

```c#
public System.Data.DataTable GetTable(string table, string col) {
    String method = getMethodName();
    object[] results = this.Invoke(method, new object[] {
                table,
                col});
    return ((System.Data.DataTable)(results[0]));
}
```
![](../images/soap_unknown_client.png)

### SoapCore Server Side
```c#
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SoapCore;
using System.ServiceModel;

namespace SoapServer
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ICalculatorService, CalculatorService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                // All Methods inside CalculatorService class are Web services
                endpoints.UseSoapEndpoint<CalculatorService>("/CalculatorService.asmx", new SoapEncoderOptions(), SoapSerializer.DataContractSerializer);
            });
        }
    }
}
```
```c#
namespace SoapServer
{
    public class CalculatorService : ICalculatorService
    {
        public int Add(int a, int b) => a + b;

        public int Subtract(int a, int b) => a - b;

        public int Multiply(int a, int b) => a * b;

        public int Divide(int a, int b) => b != 0 ? a / b : throw new System.DivideByZeroException();
    }
}
```
![](../images/soapcore_server.png)

## Limitations 

- For VB.NET, only server side support is provided.
- This extension will not currently delete all duplicated SOAP objects generated by the **com.castsoftware.dotnet** extension for the following reasons:
    - To make sure there are no regressions in connectivity.
    - To materialize objects originiating from client-side (VB.NET) APIs that are not yet supported.

The table below provides information about how duplicate objects generated by the **com.castsoftware.dotnet** are handeled:

- The **Before** column refers to results from **com.castsoftware.dotnet** without the addition of the **com.castsoftware.dotnet.soap** extension.
- The **After** column refers to results produced with the addition of the **com.castsoftware.dotnet.soap** extension. 

This table is applicable for **com.castsoftware.dotnet** version 1.0.0 to latest:

| Example | Before | After | Explanation |
|---|---|---|---|
| C# -> C# | ![](../images/cs_cs_dotnet.png) | ![](../images/cs_cs_soap_green.png) | For CSharp projects full support is provided So both server and client from .Net Analyzer are deleted and Soap Extension objects are created |
| C# -> VB.NET | ![](../images/cs_vb_dotnet.png) | ![](../images/cs_vb_soap_green.png) | For VB.NET server side support is provided So both server and client from .Net Analyzer are deleted and Soap Extension objects are created |
| VB.NET -> C# | ![](../images/vb_cs_dotnet.png) | ![](../images/vb_cs_soap_green.png) | Client side support is not provided for VB.NET, .NET SOAP Operation object object is not deleted to maintain connectivity |
| VB.NET -> VB.NET | ![](../images/VB_dotnet.png) | ![](../images/vb_vb_soap_green.png) | Client side support is not provided for VB.NET, When we have VB.Net client side we dont delete any .NET SOAP objects |
| VB.NET  (no link from client to server ) | ![](../images/vb_vb_nolink.png) | ![](../images/vb_vb_nolink_soap_green.png) | When we have VB.Net client side we dont delete any .NET SOAP objects even though if they are not connected  |
| VB.NET (only server-side) | ![](../images/VB_dotnet_server.png) | ![](../images/vb_soap_server_green.png) | Server side support is provided for VB.NET so .NET SOAP Operation object gets deleted and DotNet SOAP Operation object is created |
| VB.NET (only client-side) | ![](../images/VB_dotnet_client.png)  | ![](../images/VB_soap_client.png) | Client side support is not provided for VB.NET so .NET SOAP ressource service object is not deleted  |
| C# (client-side) | ![](../images/cs_dotnet_client.png)  | ![](../images/cs_soap_client_green.png) | For CSharp projects full support is provided So .NET SOAP ressource service object is deleted and DotNet Soap OperationCall object is created |
| C# (server-side) | ![](../images/cs_dotnet_server.png)  | ![](../images/cs_soap_server_green.png) | For CSharp projects full support is provided So .NET SOAP Operation object is deleted and DotNet Soap Operation object is created |
