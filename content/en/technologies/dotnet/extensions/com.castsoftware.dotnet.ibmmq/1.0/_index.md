---
title: "IBM MQ for .NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.ibmmq

## What's new?

See [IBM MQ for .NET - 1.0 - Release Notes](rn) for more information.

## Description

This extension provides support for IBM MQ .NET APIs (see [Links](#links)) which are responsible for sending and receiving operations on the systems.

## In what situation should you install this extension?

If your C# application utilizes IBM MQ for messaging-related operations and the producer and consumer are handled using the C# language, and you want to modelize the producer and consumer links with appropriate objects and links, then you should install this extension.

## Technology Libraries

| Library | Version | Supported |
|---|---|:-:|
| [IBMMQDotnetClient](https://www.nuget.org/packages/IBMMQDotnetClient) | up to: 9.4.1  | :white_check_mark: |
| [IBMXMSDotnetClient](https://www.nuget.org/packages/IBMXMSDotnetClient) | up to: 9.4.1  | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in order to function correctly. The IBMMQ .NET extension requires that the following other CAST extensions are also installed (this will be managed automatically by CAST):

- [Internal platform](../../../../multi/com.castsoftware.wbslinker/) (internal technical extension)
- [Universal Linker](../../../../multi/com.castsoftware.wbslinker/)

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, you should manually install it.

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/640417881.png) | DotNet IBMMQ Queue Producer |
| ![](../images/640417880.png) | DotNet IBMMQ Queue Consumer |
| ![](../images/640417879.png) | DotNet IBMMQ Unknown Queue Producer |
| ![](../images/640417878.png) | DotNet IBMMQ Unknown Queue Consumer |
| ![](../images/640417881.png) | DotNet IBMMQ Topic Producer |
| ![](../images/640417880.png) | DotNet IBMMQ Topic Subscriber |
| ![](../images/640417879.png) | DotNet IBMMQ Unknown Topic Producer |
| ![](../images/640417878.png) | DotNet IBMMQ Unknown Topic Subscriber |

### Links

| Link Type | Source and Destination of link | Supported APIs |
|---|---|---|
| callLink | callLink between the caller C# method and the DotNet IBMMQ Queue Producer or DotNet IBMMQ Topic Producer objects | Producer API's support by IBMMQ :<br><br>IBM.WMQ.MQQueue.Put<br>IBM.WMQ.MQDestination.Put<br>IBM.WMQ.MQQueue.PutForwardMessage<br>IBM.XMS.IMessageConsumer.Receive<br>IBM.XMS.IMessageConsumer.ReceiveNoWait |
| callLink | callLink between the DotNet IBMMQ Queue Consumer or DotNet IBMMQ Topic Subscriber objects and the caller  C# method  | Consumer API's support by IBMMQ :<br><br>IBM.WMQ.MQQueue.Get<br>IBM.WMQ.MQDestination.Get<br>IBM.WMQ.MQQueue.PutReplyMessage<br>IBM.XMS.IMessageProducer.Send |

## Example code scenarios

### Producer APIs

##### .NET Queue Producer

``` c#
using System;
using System.Collections;
using IBM.WMQ;

class Program
{	
   String queueName = "SingheMessageQueue1";
   String queueManagerName = "Local.Network.Q01";
   
   void PutMessages()
        {		
				
                properties = new Hashtable();
                properties.Add(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_MANAGED);

                queueManager = new MQQueueManager(queueManagerName, properties);

                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                
                message = new MQMessage();
                message.WriteString(messageString);

                for (int i = 1; i <= numberOfMsgs; i++)
                {
                    queue.Put(message);
                }
                queue.Close();
                queueManager.Disconnect();
        }
    
}
```

![](../images/IBMMQProducer.png)

##### .NET Topic Producer

``` c#
using System;
using System.Collections;
using IBM.WMQ;

class Program
{  
   String queueManagerName = "LOCAL.SYSTEM.QM02";
   String topicName = "SYSTEM.DEF.TOPIC1";
   
   void PutMessages()
        {
                properties = new Hashtable();
                properties.Add(MQC.HOST_NAME_PROPERTY, hostName);
                properties.Add(MQC.PORT_PROPERTY, port);
                properties.Add(MQC.CHANNEL_PROPERTY, channelName);

                queueManager = new MQQueueManager(queueManagerName, properties);
                topic = queueManager.AccessTopic(topicName, "", MQC.MQTOPIC_OPEN_AS_PUBLICATION, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                message = new MQMessage();
                message.WriteString(messageString);

                for (int i = 1; i <= numberOfMsgs; i++)
                {
                    topic.Put(message);
                }

                topic.Close();
                queueManager.Disconnect();
        }
    
}
```

![](../images/IBMMQTopicProducer.png)

### Consumer APIs

##### .NET Queue Consumer

``` c#
using System;
using System.Threading;
using IBM.WMQ;

class SimpleGet
{		
		String queueName = "SingheMessageQueue1";
		String queueManagerName = "Local.Network.Q01";
        
		void GetMessages()
        {
				properties = new Hashtable();
                properties.Add(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_MANAGED);
                properties.Add(MQC.HOST_NAME_PROPERTY, hostName);
                properties.Add(MQC.PORT_PROPERTY, port);
                properties.Add(MQC.CHANNEL_PROPERTY, channelName);

                queueManager = new MQQueueManager(queueManagerName, properties);
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                
				for (int i = 1; i <= numberOfMsgs; i++)
                {
                    message = new MQMessage();
					queue.Get(message);
                    message.ClearMessage();
                }
				queue.Close();
                queueManager.Disconnect();
		
        }   
}
```

![](../images/IBMMQConsumer.png)

##### .NET Topic Subscriber

``` c#
using System;
using System.Threading;
using IBM.WMQ;

class SimpleSubscribe
{		
		String queueManagerName = "LOCAL.SYSTEM.QM02";
		String topicName = "SYSTEM.DEF.TOPIC1";
		
        void GetMessages()
        {
                properties = new Hashtable();
                properties.Add(MQC.HOST_NAME_PROPERTY, hostName);
                properties.Add(MQC.PORT_PROPERTY, port);
                properties.Add(MQC.CHANNEL_PROPERTY, channelName);

                queueManager = new MQQueueManager(queueManagerName, properties);
				
				if (durability == "nondurable")
                    topic = queueManager.AccessTopic(topicName, null, MQC.MQTOPIC_OPEN_AS_SUBSCRIPTION, MQC.MQSO_CREATE | MQC.MQSO_FAIL_IF_QUIESCING);
                else if (durability == "durable")
                    topic = queueManager.AccessTopic(topicName, null, MQC.MQSO_CREATE | MQC.MQSO_FAIL_IF_QUIESCING | MQC.MQSO_DURABLE | MQC.MQSO_RESUME, null, "DurableSubscriptionName");
                
				message = new MQMessage();
                message.WriteString(messageString);

                int time = 1;
                for (int i = 1; i <= numberOfMsgs; i++)
                {
                    message = new MQMessage();
                    topic.Get(message);
                    message.ClearMessage();
                }
				topic.Close();
                queueManager.Disconnect();
        }
}
```

![](../images/IBMMQTopicSubscriber.png)

### Cross Technology Java/.NET

##### .NET Producer

``` c#
using System;
using System.Threading;
using IBM.WMQ;

class nmqsput
{
	static int Main(String[] args)
		{
			MQQueueManager mqQMgr;  
			MQQueue mqQueue;       
			MQMessage mqMsg;     
			MQPutMessageOptions mqPutMsgOpts;  
			int msgLen;      
			String message;        
			
			String queueName = "SharedQueueV1";
			String queueManagerName = "QM2";
			
			mqQMgr = new MQQueueManager(queueManagerName);
			mqQueue = mqQMgr.AccessQueue( queueName,MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING );
			
			bool isContinue = true;
			while (isContinue)
			{
				message = System.Console.ReadLine();
				msgLen = message.Length;

				if (msgLen > 0)
				{
					mqMsg = new MQMessage();
					mqMsg.WriteString( message );
					mqMsg.Format = MQC.MQFMT_STRING;
					mqPutMsgOpts = new MQPutMessageOptions();

					mqQueue.Put( mqMsg, mqPutMsgOpts );
				}
				else
				{
					isContinue = false;
				}
			}
			return (0);
			}
}
```

##### Java Consumer

``` java

public class SampleIBM_mq_client{
	 public static final String  HOST_NAME           = "localhost";
	    public static final int     PORT_NUMBER         = 1313;
	    public static final String  QUEUE_MANAGER_NAME  = "QM2";
	    public static final String  QUEUE_NAME          = "SharedQueueV1";
	    public static final String  LOGIN_USERNAME      = "theanh";
	    public static final String  LOGIN_PASSWORD      = "GMAIL";
	    
	    
	    public static void main(String[] args) {
	            MQQueueConnectionFactory cf = new MQQueueConnectionFactory();
	            cf.setHostName(HOST_NAME);
	            cf.setPort(PORT_NUMBER);
	            cf.setTransportType(JMSC.MQJMS_TP_CLIENT_MQ_TCPIP);
	            cf.setQueueManager(QUEUE_MANAGER_NAME);
	            cf.setChannel("SYSTEM.DEF.SVRCONN");

	            MQQueueConnection connection = (MQQueueConnection) cf.createQueueConnection(LOGIN_USERNAME, LOGIN_PASSWORD);
	            MQQueueSession session = (MQQueueSession) connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
	            MQQueue queue = (MQQueue) session.createQueue(QUEUE_NAME);
	            MQQueueReceiver receiver = (MQQueueReceiver) session.createReceiver((Queue) queue);

	            long uniqueNumber = System.currentTimeMillis() % 1000;
	            JMSTextMessage message = (JMSTextMessage) session.createTextMessage("random number: " + uniqueNumber);

	            connection.start();

	            JMSTextMessage receivedMessage = (JMSTextMessage) receiver.receive();
	            
	            receiver.close();
	            session.close();
	            connection.close();
	    }
}
```

![](../images/IBMMQCrossTechnology.png)

### Reading from configuration file

##### appsettings.json

``` json
{
  "XMSConfig": {
    "Host": "127.0.0.1",
    "Port": "1414",
    "Channel": "DEV.APP.SVRCONN",
    "QueueManager": "QM1",
    "QueueName": "SMS.LOCAL.TESTQUEUE",
    "User": "admin",
    "Password": "password"
  }
}
```

##### .Net Producer

``` c#
using System;
using IBM.WMQ;

class MQCLient
    { 
        public void sendMessage(string textMessage){

            MQCLient client = new MQCLient();
             var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();
            var xmsConfig = config.GetSection("XMSConfig");
                // XMS Connection Configuration
            IConnectionFactory factory = XMSFactoryFactory.GetInstance(XMSC.CT_WMQ).CreateConnectionFactory();
            factory.SetStringProperty(XMSC.WMQ_HOST_NAME, xmsConfig["Host"]);
            factory.SetIntProperty(XMSC.WMQ_PORT, int.Parse(xmsConfig["Port"]));
            factory.SetStringProperty(XMSC.WMQ_CHANNEL, xmsConfig["Channel"]);
            factory.SetStringProperty(XMSC.WMQ_QUEUE_MANAGER, xmsConfig["QueueManager"]);
            factory.SetIntProperty(XMSC.WMQ_CONNECTION_MODE, XMSC.WMQ_CM_CLIENT);

            string queueName = xmsConfig["QueueName"];

            client.send(queueName,textMessage);

        }
 
        public void send(string queueName,string textMessage)
        {

                using (IConnection connection = factory.CreateConnection())
                {
                    connection.Start();

                    using (ISession session = connection.CreateSession(false, AcknowledgeMode.AutoAcknowledge))
                    {
                        IDestination destination = session.CreateQueue(queueName);

                        using (IMessageProducer producer = session.CreateProducer(destination))
                        {
                            ITextMessage message = session.CreateTextMessage("Hello, XMS with JSON Configuration!");
                            producer.Send(message);
                            Console.WriteLine("Message sent: " + message.Text);
                        }
                    }
                }
        }
    }
```

![](../images/IBMMQReadFromConfig.png)

## Limitations

-  Unknown objects will be created if evaluation fails to resolve the necessary parameter.
-  Analyzer might not be able to fetch Consumer/Producer properties defined via environment variable.