---
title: "ADODB.NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---
## Extension ID

com.castsoftware.dotnet.adodb

## What's new?

See [Release Notes](rn) for more information.

## Description

This extension provides support for Microsoft ADODB Library APIs which are responsible for typical CRUD operations with the database.

## Technology support

| Item                        | Version         |                                          Supported                                          | Supported Technology | Notes                                                                                                                                              |
|:-----------|:--------|:----:|:--------:|:------------------------------------|
| Microsoft Data Access Components (MDAC) | 1.0 to 2.8 SP2 | :white_check_mark:  |          C#          | See https://en.wikipedia.org/wiki/Microsoft_Data_Access_Components#Version_checking
| Windows DAC 6.0       | 6.0 | :white_check_mark:  |          C#          | DAC 6.0 is a variant of MDAC for use with Vista
| Microsoft ActiveX Data Objects      | 2.0 to 2.8 and 6.0 to 6.1 | :white_check_mark:  |          C#          | See https://learn.microsoft.com/en-us/sql/ado/guide/referencing-the-ado-libraries?view=sql-server-ver16&source=recommendations 
| ADOR       | 2.8 & 6.0 | :white_check_mark:  |          C#          | APIs are similar to that of ADODB; the namespace is ADOR
| ADODB Custom Nuget Package      | 7.10.3077 | :white_check_mark:  |          C#          | See [https://www.nuget.org/packages/ADODB](https://www.nuget.org/packages/ADODB) NOTE: This is not a Microsoft package. It's no longer available to download. However, the APIs are supported.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For applications using ADODB Library APIs, the extension needs to be installed manually in CAST Imaging Console.

Any ADODB ActiveX , assembly or Nuget package used to develop the application must be provided along with the source code for analysis.

## What results can you expect?

### Objects


| Icon  | Description  |
|---------|---------|
| ![](../images/670630295.png)  | DotNet ADODB SQL Query |
| ![](../images/unknown_sql_query_icon.png)  | DotNet ADODB Unknown SQL Query |

An object is created for each SQL query found and resolved in a ADODB CRUD method call

### Links created by this extension and other related extensions

| Link Type | Caller type | Callee type | Methods Supported |
|---|---|---|---|
| callLink | C# Method | DotNet ADODB SQL Query <br>DotNet ADODB Unknown SQL Query | <details><summary>Supported APIs</summary>ADODB.ConnectionClass.Execute<br>ADODB.Connection15.Execute<br>ADODB.Connection.Execute<br>ADODB._Connection.Execute<br>ADODB.CommandClass.Execute<br>ADODB.Command.Execute<br>ADODB.Command15.Execute<br>ADODB.Command25.Execute<br>ADODB._Command.Execute<br>ADODB.RecordsetClass.AddNew<br>ADODB.RecordsetClass.Delete<br>ADODB.RecordsetClass.Find<br>ADODB.RecordsetClass.GetRows<br>ADODB.RecordsetClass.Update<br>ADODB.RecordsetClass.UpdateBatch<br>ADODB.Recordset15.AddNew<br>ADODB.Recordset15.Delete<br>ADODB.Recordset15.Find<br>ADODB.Recordset15.GetRows<br>ADODB.Recordset15.Update<br>ADODB.Recordset15.UpdateBatch<br>ADODB.Recordset20.AddNew<br>ADODB.Recordset20.Delete<br>ADODB.Recordset20.Find<br>ADODB.Recordset20.GetRows.<br>ADODB.Recordset20.Update<br>ADODB.Recordset20.UpdateBatch<br>ADODB.Recordset21.AddNew<br>ADODB.Recordset21.Delete<br>ADODB.Recordset21.Find<br>ADODB.Recordset21.GetRows<br>ADODB.Recordset21.Update<br>ADODB.Recordset21.UpdateBatch<br>ADODB._Recordset.AddNew<br>ADODB._Recordset.Delete<br>ADODB._Recordset.Find<br>ADODB._Recordset.GetRows<br>ADODB._Recordset.Update<br>ADODB._Recordset.UpdateBatch<br>ADODB.Fields.Append<br>ADODB.Fields.Delete<br>ADODB.Fields.Update<br>ADODB.Fields._Append<br>ADODB.Fields20._Append<br>ADODB.Fields20.Delete<br>ADOR._Recordset.AddNew<br>ADOR._Recordset.Delete<br>ADOR._Recordset.Find<br>ADOR._Recordset.GetRows<br>ADOR._Recordset.Update<br>ADOR._Recordset.UpdateBatch<br>ADOR.Fields.Delete<br>ADOR.Fields.Update</details> |
| useLink | DotNet ADODB SQL Query | Table, View | Created by SQL Analyzer when DDL source files are analyzed |
| callLink | DotNet ADODB SQL Query | Procedure | As above |
| useLink | DotNet ADODB SQL Query | Missing Table | Created by Missing tables and procedures for .Net extension when the object is not analyzed. |
| callLink | DotNet ADODB SQL Query | Missing Procedure | As above |

## Example code scenarios

### Execute
    
Sample code for Execute API 

``` c#
public static void CommandSample()
    {
        /// Create a Connection object
        Connection conn = new Connection();
        conn.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Northwind.mdb");
        // Create a Command object
        _Command cmd = new Command();
        cmd.ActiveConnection = conn;

        // Specify the SQL command to execute
        cmd.CommandText = "INSERT INTO Employees (FirstName, LastName, Title) VALUES (?, ?, ?)";

        // Add parameters to the command
        cmd.Parameters.Append(cmd.CreateParameter("FirstName", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 10, "John"));
        cmd.Parameters.Append(cmd.CreateParameter("LastName", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 20, "Smith"));
        cmd.Parameters.Append(cmd.CreateParameter("Title", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 30, "Manager"));

        // Execute the command
        object recordsAffected;
        object missing = Type.Missing;
        cmd.Execute(out recordsAffected, ref missing, (int)ExecuteOptionEnum.adExecuteNoRecords);

        Console.WriteLine("Record inserted successfully. Records affected: " + recordsAffected);
    }
```

![](../images/adodb_execute.png)

### Update 

Sample code for Update

``` c#
static void UpdateBatchSample()
    {
        string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Northwind.mdb;";
        Connection connection = new Connection();
        Recordset rsTitles = new Recordset();

        // Open connection
        connection.Open(connectionString);

        // Open titles table.
        rsTitles.Open("select * from shippers", connection, CursorTypeEnum.adOpenKeyset, LockTypeEnum.adLockBatchOptimistic, -1);

        // Move to the first record
        rsTitles.MoveFirst();

        // Loop through recordset
        while (!rsTitles.EOF)
        {
            string name = rsTitles.Fields["CompanyName"].Value.ToString();
            if (name == "Speedy Express")
            {
                
                char chKey = Console.ReadKey().KeyChar;
                if (char.ToUpper(chKey) == 'Y')
                {
                    // Change type to other value.
                    rsTitles.Fields["name"].Value = "SuperFast Express";
                }
            }
            rsTitles.MoveNext();
        }

        // UpdateBatch to commit changes
        rsTitles.UpdateBatch(AffectEnum.adAffectAll);
              
        // Close recordset and connection
        rsTitles.Close();
        connection.Close();
        
    
    }
```
![](../images/adodb_updatebatch.png)

### Select 

Sample code for Select

``` c#
static void Main()
    {
        string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Northwind.MDB;";

        // Create a Connection object
        Connection connection = new Connection();
        connection.Open(connectionString);

        // Create a new ADODB.Recordset
        Recordset recordset = new Recordset();
        recordset.ActiveConnection = connection;
        recordset.Source = "SELECT * FROM Products";
        recordset.Open();

        // Add a new field to the recordset
        Fields fields = recordset.Fields;
        fields.Append("NewField", DataTypeEnum.adVarChar, 50, FieldAttributeEnum.adFldUpdatable);

        // Update the recordset with the new field
        fields.Update();

        // Add a new record to the recordset with FieldList and Values parameters
        object fieldList = new object[] { "ProductName", "NewField" };
        object values = new object[] { "NewProduct", "NewValue" };

        recordset.AddNew(fieldList, values);
        recordset.Update();

        // Find a specific record by ProductName
        recordset.Find("ProductName = 'NewProduct'", 0, SearchDirectionEnum.adSearchForward, Type.Missing);

        // Close the connection
        connection.Close();

    }
```
![](../images/adodb_find.png)

### Delete
    
Sample code for Delete API 

``` c#
static void FieldDeletionSample()
{
    // Initialize ADODB objects
    Connection conn = new Connection();
    Recordset rs = new Recordset();

    conn.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Northwind.mdb");

    // Open a recordset for Region table
    rs.Open("SELECT * FROM Region", conn, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockOptimistic, -1);

    Fields fields = rs.Fields;

    // Check if the recordset contains the "RegionDescription" and "Desc" fields
    if (fields["RegionDescription"] != null)
    {
        // Delete the Desc field
        fields.Delete("RegionDescription");

        // Update the recordset
        rs.Update();

    }
                
    // Clean up
    rs.Close();
    conn.Close();
            
 }
```
![](../images/adodb_delete.png)

### Unknown

Sample code for Execute API 

``` c#
public static void ExecuteSample()
        {
            // Initialize ADODB objects
            Connection conn = new Connection();
            Recordset rs = new Recordset();

            try
            {
                // Execute SQL queries or commands
            string sqlQuery = "INSERT INTO Customers (ID, Name, Age) VALUES (1, 'Will', 35)";
            conn.Execute(sqlQuery);

            Console.WriteLine("Record inserted successfully.");

            // Update a record
            conn.Execute(sqlUpdates);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                // Clean up
                rs.Close();
                conn.Close();
            }
        }
```
![](../images/adodb_unknown.png)

## Limitations

-   The support works when the ADODB Library is used
    directly, that is, not through a custom wrapper delivered as an
    assembly.


