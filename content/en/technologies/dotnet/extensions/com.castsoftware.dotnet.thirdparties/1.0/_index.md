---
title: ".NET Third Parties - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.thirdparties

## What's new?

See [Release Notes](rn).

## Description

This extension is a dependency of the [com.castoftware.dotnet](../../com.castsoftware.dotnet/) extension (≥ 2.0). The extension provides specific .NET related frameworks and libraries and which are required by [com.castoftware.dotnet](../../com.castsoftware.dotnet/) during the analysis process.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :x: |

## Download and installation instructions

For .NET applications, the extension will be automatically installed by CAST Imaging Console as a dependency of the [com.castoftware.dotnet](../../com.castsoftware.dotnet/) extension (≥ 2.0).

## Frameworks and third-party packages provided in the extension

Currently this extension provides the following:

- .NET Framework 4.x