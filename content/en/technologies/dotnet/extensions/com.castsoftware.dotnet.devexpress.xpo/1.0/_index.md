---
title: "DevExpress XPO - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.devexpress.xpo

## What's new?

Please see [Release Notes](rn/) for more information.

## Description

This extension provides support for the DexExpress XPO persistence framework for .NET.

## In what situation should you install this extension?

This extension should be installed when analyzing a .NET project that uses DexExpress XPO persistence framework, and you want to
view CRUD transactions of the XPO persistent objects. Links to corresponding SQL database tables can also be resolved, provided that the SQL database has been extracted and DDL has been created.

## Technology support

The following libraries are supported by this extension:

| Language | Library name | Namespace |Version | Supported |
| -------- | -------- | ------------------------------------------------------------------------- | :--------: | :----------------: |
| .NET     | [DevExpress.Xpo](https://www.devexpress.com/products/net/orm/) |DevExpress.Xpo<br> DevExpress.Xpo.DB| 18.1 to 23.2| :heavy_check_mark: |
| .NET     | [DevExpress.ExpressApp](https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp) |DevExpress.ExpressApp| 24.1.4| :heavy_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For applications using any of the above mentionned libraries, this extension will be automatically installed by CAST Imaging Console.
For upgrade, if the Extension Strategy is not set to Auto update, you can manually upgrade the extension using the Application - Extensions interface.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the below objects and links created.

### Objects
| Icon | Description | Comment |
|------|-------------|---------|
| ![entity_icon](../images/entity_icon.png) | DevExpress XPO Entity   | An object is created for each XPO Persistent Object |
| ![entity_operation](../images/entity_operation.png) | DevExpress XPO Entity Operation   | An object is created for each XPO Persistent Object CRUD operation|
| ![unknown_entity_icon](../images/Unknown_Entity.png) | DevExpress XPO Unknown Entity   | An object is created for when XPO Persistent Object cannot be resolved |
| ![unknown_entity_operation](../images/UnknownEntity_Operation.png) | DevExpress XPO Unknown Entity Operation   | An object is created for each XPO Persistent Object CRUD operation and respective Entity cannot be resolved|
| ![sql_query_icon](../images/sql_query_icon.png) | DevExpress XPO SQL Query | An object is created for each direct SQL query operation found in XPO project and resolved in a method call|
| ![unknown_sql_query_icon](../images/unknown_sql_query_icon.png) | DevExpress XPO Unknown SQL Query | An object is created for each direct SQL query found in XPO project and the exact query cannot be resolved |


### Links
| Link Type | Caller | Callee | APIs Supported |
| --- | --- | --- | --- |
| callLink | C# Method | DevExpress XPO Entity Operation <br>DevExpress XPO Unknown Entity Operation| <details><summary>DevExpress.Xpo.Session APIs</summary>DevExpress.Xpo.Session.BulkLoad<br>DevExpress.Xpo.Session.BulkLoadAsync<br>DevExpress.Xpo.Session.Delete<br>DevExpress.Xpo.Session.FindObject<br>DevExpress.Xpo.Session.CollectReferencingObjects<br>DevExpress.Xpo.Session.DeleteAsync<br>DevExpress.Xpo.Session.DeleteCore<br>DevExpress.Xpo.Session.DeleteCoreAsync<br>DevExpress.Xpo.Session.DeleteObject<br>DevExpress.Xpo.Session.DeleteObjectAsync<br>DevExpress.Xpo.Session.DeleteObjectOrCollection<br>DevExpress.Xpo.Session.DeleteObjectOrCollectionAsync<br>DevExpress.Xpo.Session.FindObjectAsync<br>DevExpress.Xpo.Session.FindObjectAsyncResultProcess<br>DevExpress.Xpo.Session.GetLoadedObjectByKey<br>DevExpress.Xpo.Session.GetObjectByKey<br>DevExpress.Xpo.Session.GetObjectByKeyAsync<br>DevExpress.Xpo.Session.GetObjects<br>DevExpress.Xpo.Session.GetObjectsAsync<br>DevExpress.Xpo.Session.GetObjectsByKey<br>DevExpress.Xpo.Session.GetObjectsByKeyAsync<br>DevExpress.Xpo.Session.Reload<br>DevExpress.Xpo.Session.Save<br>DevExpress.Xpo.Session.ReloadAsync<br>DevExpress.Xpo.Session.SaveAsync<br>DevExpress.Xpo.Session.SelectData<br>DevExpress.Xpo.Session.SelectDataAsync<br></details>  <details><summary>DevExpress.ExpressApp APIs</summary> DevExpress.ExpressApp.IObjectSpace.CreateCollection<br>DevExpress.ExpressApp.IObjectSpace.CreateObject<br>DevExpress.ExpressApp.IObjectSpace.Delete<br>DevExpress.ExpressApp.IObjectSpace.FindObject<br>DevExpress.ExpressApp.IObjectSpace.FirstOrDefault<br>DevExpress.ExpressApp.IObjectSpace.GetObject<br>DevExpress.ExpressApp.IObjectSpace.GetObjectByKey<br>DevExpress.ExpressApp.IObjectSpace.GetObjects<br>DevExpress.ExpressApp.IObjectSpace.GetObjectsCount<br>DevExpress.ExpressApp.IObjectSpace.GetObjectsQuery<br>DevExpress.ExpressApp.IObjectSpace.ReloadObject<br>DevExpress.ExpressApp.IObjectSpaceAsync.FindObjectAsync<br>DevExpress.ExpressApp.IObjectSpaceAsync.GetObjectAsync<br>DevExpress.ExpressApp.IObjectSpaceAsync.GetObjectByKeyAsync<br>DevExpress.ExpressApp.IObjectSpaceAsync.LoadAsync<br>DevExpress.ExpressApp.IObjectSpaceAsync.ReloadObjectAsync</details>DevExpress.Xpo.XPQuery.XPQuery<br>DevExpress.Xpo.XPCollection.XPCollection<br>DevExpress.Xpo.XPQueryExtensions.Query<br>DevExpress.Xpo.XPBaseObject.Save<br>DevExpress.Xpo.DB.IDataStore.ModifyData<br>DevExpress.Xpo.IDataLayer.ModifyData<br>DevExpress.Xpo.DB.ConnectionProviderSql.ModifyData<br>DevExpress.Xpo.DB.DataStoreSerializedBase.ModifyData<br>DevExpress.Xpo.DB.IDataStore.SelectData<br>DevExpress.Xpo.IDataLayer.SelectData<br>DevExpress.Xpo.DB.ConnectionProviderSql.SelectData<br>DevExpress.Xpo.DB.DataStoreSerializedBase.SelectData<br>  |
| callLink | C# Method | DevExpress XPO SQL Query <br> DevExpress XPO Unknown SQL Query | <details><summary>DevExpress.Xpo.Session APIs</summary>DevExpress.Xpo.Session.GetObjectsByKeyFromQuery<br>DevExpress.Xpo.Session.GetObjectsByKeyFromSproc<br>DevExpress.Xpo.Session.GetObjectsFromQuery<br>DevExpress.Xpo.Session.GetObjectsFromSproc<br>DevExpress.Xpo.Session.GetObjectsFromSprocAsync<br>DevExpress.Xpo.Session.GetObjectsFromSprocParametrized<br>DevExpress.Xpo.Session.GetObjectsFromSprocParametrizedAsync<br>DevExpress.Xpo.Session.ExecuteNonQuery<br>DevExpress.Xpo.Session.ExecuteNonQueryAsync<br>DevExpress.Xpo.Session.ExecuteQuery<br>DevExpress.Xpo.Session.ExecuteQueryAsync<br>DevExpress.Xpo.Session.ExecuteQueryWithMetadata<br>DevExpress.Xpo.Session.ExecuteQueryWithMetadataAsync<br>DevExpress.Xpo.Session.ExecuteScalar<br>DevExpress.Xpo.Session.ExecuteScalarAsync<br>DevExpress.Xpo.Session.ExecuteSproc<br>DevExpress.Xpo.Session.ExecuteSprocAsync<br>DevExpress.Xpo.Session.ExecuteSprocParametrized<br>DevExpress.Xpo.Session.ExecuteSprocParametrizedAsync<br></details> |
| useLink | DevExpress XPO Entity Operation | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | DevExpress XPO Entity Operation | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|
| useLink | DevExpress XPO SQL Query | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | DevExpress XPO SQL Query | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|

### Examples

#### XPO Persistent Objects CRUD

``` c#
using DevExpress.Xpo;

namespace DevExpress.Xpo.ConsoleCoreDemo {
    class StatisticInfo : XPLiteObject {
        public StatisticInfo(Session session)
            : base(session) {
        }
        Guid key;
        [Key(true)]
        public Guid Key {
            get { return key; }
            set { SetPropertyValue(nameof(Key), ref key, value); }
        }
        string info;
        [Size(255)]
        public string Info {
            get { return info; }
            set { SetPropertyValue(nameof(Info), ref info, value); }
        }
        DateTime date;
        public DateTime Date {
            get { return date; }
            set { SetPropertyValue(nameof(Date), ref date, value); }
        }
    }
}
```
##### DevExpress.Xpo.XPBaseObject.Save
``` c#
using DevExpress.Xpo;
using DevExpress.Xpo.DB;

namespace DevExpress.Xpo.ConsoleCoreDemo {
    class Program {
        static void Main(string[] args) {
                    
        using(UnitOfWork uow = new UnitOfWork()) {
                StatisticInfo newInfo = new StatisticInfo(uow);
                newInfo.Info = result;
                newInfo.Date = DateTime.Now;
                newInfo.Save();
                Console.WriteLine("Saved.");
            }
        }
    }
}
```

![](../images/XPBaseObject_Save.png)

##### DevExpress.Xpo.XPQueryExtensions.Query
``` c#
namespace DevExpress.Xpo.ConsoleCoreDemo {
    class Program {
        static void Main(string[] args) {

        using(UnitOfWork uow = new UnitOfWork()) {
                var query = uow.Query<StatisticInfo>()
                    .OrderBy(info => info.Date)
                    .Select(info => $"[{info.Date}] {info.Info}");
                foreach(var line in query) {
                    Console.WriteLine(line);
            }
        }     
    }
}
```
![](../images/XPQueryExtensions_Query.png)

#### DevExpress.ExpressApp APIs

##### DevExpress.ExpressApp.IObjectSpace.GetObject

```
using DevExpress.Xpo;

public partial class rec_DocSrv_Order_Detail : XPLiteObject
{
    int fdocsrv_order_detail;
    [Key(true)]
    [DevExpress.Xpo.DisplayName(@"ID")]
    public int docsrv_order_detail
    {
        get { return fdocsrv_order_detail; }
        set { SetPropertyValue<int>("docsrv_order_detail", ref fdocsrv_order_detail, value); }
    }
    rec_DocSrv_Order fdocsrv_order_id;
}

[DefaultClassOptions]
[NavigationItem("Document Service")]
[DevExpress.ExpressApp.DC.XafDisplayName("Document Service Order Detail")]
[DevExpress.Persistent.Base.ImageName("rec_DocSrv_Order_Detail")]
[DevExpress.ExpressApp.DC.XafDefaultProperty("rec_doc_info_id")]
public partial class rec_DocSrv_Order_Detail
{
    public rec_DocSrv_Order_Detail(Session session) : base(session){ }
}
```
```
using DevExpress.ExpressApp;

private void act_Delivered_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(rec_DocSrv_Order_Detail));
            DateTime serviceDateTime = Core.SqlOp.GetServerDateTime(objectSpace);
            foreach (rec_DocSrv_Order_Detail item in e.SelectedObjects)
            {
                rec_DocSrv_Order_Detail detail = objectSpace.GetObject(item);
                detail.delivered = true;
                detail.delivered_date = serviceDateTime;
            }
            objectSpace.CommitChanges();
            ObjectSpace.Refresh();
        }
```
![](../images/ObjectSpace_getobject.png)

##### DevExpress.ExpressApp.IObjectSpace.CreateObject

```
using DevExpress.Xpo;

public partial class rec_Applicant : XPLiteObject
    {
        string fapplicant_address;

        [Size(255)]
        DateTime fapplicant_birth_date;
    }

public partial class rec_Applicant
    {
        public rec_Applicant(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
```
```
using DevExpress.ExpressApp;

private void actionTransferApplicant_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var selectedItems = e.SelectedObjects;
            if (selectedItems.Count == 0)
                return;

            IObjectSpace objectSpace = Application.CreateObjectSpace();

            foreach (rec_job_post_apply apply in selectedItems)
            {
                rec_Applicant applicant = objectSpace.CreateObject<rec_Applicant>();
                applicant.applicant_name = apply.apply_name;
            }
            objectSpace.CommitChanges();
            View.ObjectSpace.Refresh();
        }
```

![](../images/XPO_ObjectSpace_CreateObj.png)

#### XPCollection

``` c#
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;

namespace dxTestSolutionXPO {
    class Program {
        static void Main(string[] args) {
            ConnectionHelper.Connect(DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema);
            MakeInitialData();
            var session = new Session();
            var lst = new XPCollection<OrderItem>(session);
            var cnt = lst.Count;
            var c = new XPCollection<Order>(session)[0].OrderItems.ToList();
        }

        static T CreateObject<T>(string propertyName, string value, Session session) {

            T theObject = session.FindObject<T>(new OperandProperty(propertyName) == value);
            if (theObject == null) {
                theObject = (T)Activator.CreateInstance(typeof(T), new object[] { session });
                ((XPCustomObject)(object)theObject).SetMemberValue(propertyName, value);
            }

            return theObject;

        }
    }
}
```
##### XPCollection\<Order\>
![](../images/XPC_Order.png)

##### XPCollection\<OrderItem\>
![](../images/XPC_OrderItem.png)

##### Unknown XPO Persistent Object

![](../images/XPO_Unknown_Entity.png)

#### Direct SQL Query 
##### DevExpress.Xpo.Session.ExecuteQueryWithMetadata
```c#
namespace WpfApplication1
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Windows;

  using DevExpress.Xpo;
  using DevExpress.Xpo.DB;


  public class DataSource
  {
    public DataSource()
    {
      Setup();
    }

    public XPDataView Data { get; private set; }

    private void Setup()
    {
      var connectionString = MSSqlConnectionProvider.GetConnectionString("sever", "user", "passwd", "database");
      var dataLayer        = XpoDefault.GetDataLayer(connectionString, AutoCreateOption.SchemaAlreadyExists);

      using (var uow = new UnitOfWork(dataLayer))
      {
        var sql    = "SELECT Id, Name FROM customer WHERE Name = @Name";
        var dbData = uow.ExecuteQueryWithMetadata(sql, new[] { "Name" }, new[] { "Computer" });

        Data = new XPDataView().FillAndLoad(dbData.ResultSet);
      }
    }
  }
}
```
![](../images/XPO_DirectQuery.png)

#### Unknown Direct SQL Query 


```c#
namespace WpfApplication1
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Windows;

  using DevExpress.Xpo;
  using DevExpress.Xpo.DB;

  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
      DataContext = new DataSource();
    }
  }

  public class DataSource
  {
    public DataSource()
    {
      Setup();
    }

    public static XPDataView FillAndLoad(this XPDataView self, Session sess, string sql)
    {
      return self.FillAndLoad(sess.ExecuteQueryWithMetadata(sql).ResultSet);
    }
  }
}
```

![](../images/XPO_Unknown_Direct.png)
