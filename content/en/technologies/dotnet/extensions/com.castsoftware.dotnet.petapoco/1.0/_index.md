---
title: "PetaPoco - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.petapoco

## What's new?

Please see [Release Notes](rn/) for more information.

## Description

This extension provides support for the [PetaPoco](https://github.com/CollaboratingPlatypus/PetaPoco),[NPoco](https://github.com/schotime/NPoco) and [DotNetNuke](https://github.com/dnnsoftware/Dnn.Platform/tree/develop/DNN%20Platform/Library/Data/PetaPoco)  .NET frameworks.

## In what situation should you install this extension?

This extension should be installed when analyzing a .NET project that uses the Petapoco/NPoco framework, and you want to
view CRUD transactions of the NPoco objects. Links to corresponding SQL database tables can also be resolved, provided that the SQL database has been extracted and DDL has been created.

## Technology support

The following libraries are supported by this extension:

| Language | Library name | Namespace |Version | Supported |
|---|---|---|:-:|:-:|
| C# | [PetaPoco](https://github.com/CollaboratingPlatypus/PetaPoco) | PetaPoco | Up to 6.x | :white_check_mark: |
| C# | [NPoco](https://github.com/schotime/NPoco) | NPoco | Up to 5.x | :white_check_mark: |
| C# | [DotNetNuke](https://github.com/dnnsoftware/Dnn.Platform/tree/develop/DNN%20Platform/Library/Data/PetaPoco) | DotNetNuke | Up to 9.x | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For applications using any of the above mentionned libraries, this extension will be automatically installed. For upgrade, if the Extension Strategy is not set to Auto Update, you can manually upgrade the extension.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the below objects and links created.

### Objects

| Icon | Description | Comment |
|:-:|---|---|
| ![entity_icon](../images/entity_icon.png) | NPoco Entity   | An object is created for each PetaPoco/NPoco Persistent Object |
| ![entity_operation](../images/entity_operation.png) | NPoco Entity Operation   | An object is created for each PetaPoco/NPoco Persistent Object CRUD operation|
| ![unknown_entity_icon](../images/Unknown_Entity.png) | NPoco Unknown Entity   | An object is created for when PetaPoco/NPoco Persistent Object cannot be resolved |
| ![unknown_entity_operation](../images/UnknownEntity_Operation.png) | NPoco Unknown Entity Operation   | An object is created for each PetaPoco/NPoco Persistent Object CRUD operation and respective Entity cannot be resolved|
| ![sql_query_icon](../images/sql_query_icon.png) | NPoco SQL Query | An object is created for each direct SQL query operation found in PetaPoco/NPoco project and resolved in a method call|
| ![unknown_sql_query_icon](../images/unknown_sql_query_icon.png) | NPoco Unknown SQL Query | An object is created for each direct SQL query found in PetaPoco/NPoco project and the exact query cannot be resolved |

### Links

| Link Type | Caller | Callee | APIs Supported |
|:-:|---|---|---|
| callLink | C# Method | NPoco Entity Operation <br>NPoco Unknown Entity Operation| PetaPoco.Database.Insert<br>PetaPoco.Database.Update<br>NPoco.Database.UpdateAsync<br>NPoco.Database.UpdateMany<br>NPoco.Database.UpdateManyAsync<br>PetaPoco.Database.Delete<br>PetaPoco.Database.Exists<br>PetaPoco.Database.Save<br>NPoco.Database.Insert<br>NPoco.Database.InsertAsync<br>NPoco.Database.InsertBulk<br>NPoco.Database.InsertBulkAsync<br>NPoco.Database.InsertBatch<br>NPoco.Database.InsertBatchAsync<br>NPoco.Database.SingleById<br>NPoco.Database.SingleOrDefaultById<br>NPoco.Database.Update<br>NPoco.Database.Delete<br>NPoco.Database.DeleteAsync<br>NPoco.Database.DeleteMany<br>NPoco.Database.DeleteManyAsync<br>NPoco.Database.Exists<br>NPoco.Database.IsNew<br>NPoco.Database.Save<br>NPoco.Database.SaveAsync<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.Delete<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.DeleteInternal<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.Find<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.InsertInternal<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.Update<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.UpdateInternal<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.GetByIdInternal<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.GetByScopeInternal<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.GetInternal<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.GetPageByScopeInternal<br>DotNetNuke.Data.PetaPoco.PetaPocoRepository.GetPageInternal  |
| callLink | C# Method | NPoco SQL Query <br> NPoco Unknown SQL Query | PetaPoco.Database.SingleOrDefault<br>PetaPoco.Database.Single<br>PetaPoco.Database.FirstOrDefault<br>PetaPoco.Database.First<br>PetaPoco.Database.Fetch<br>PetaPoco.Database.Query<br>PetaPoco.Database.Page<br>PetaPoco.Database.Delete<br>PetaPoco.Database.Execute<br>NPoco.Database.Single<br>NPoco.Database.SingleAsync<br>NPoco.Database.SingleOrDefault<br>NPoco.Database.First<br>NPoco.Database.FirstAsync<br>NPoco.Database.FirstOrDefault<br>NPoco.Database.Fetch<br>NPoco.Database.FetchAsync<br>NPoco.Database.Query<br>NPoco.Database.Page<br>NPoco.Database.PageAsync<br>NPoco.Database.Delete<br>NPoco.Database.Execute<br>NPoco.Database.ExecuteAsync<br>DotNetNuke.Data.PetaPoco.PetaPocoHelper.ExecuteNonQuery<br>DotNetNuke.Data.PetaPoco.PetaPocoHelper.ExecuteReader<br>DotNetNuke.Data.PetaPoco.PetaPocoHelper.ExecuteScalar<br>DotNetNuke.Data.PetaPoco.PetaPocoHelper.ExecuteSQL<br>
| useLink | NPoco Entity Operation | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | NPoco Entity Operation | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|
| useLink | NPoco SQL Query | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | NPoco SQL Query | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|

### Examples

#### Persistent Objects CRUD

##### Entity Object

``` c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PetaPocoWebTest.Poco
{
	[TableName( "tag")]
	public class Tag
	{
		public int Id {get; set;}

		[Column( "tagName")]
		public string TagName {get; set;}

		public Tag() : this( string.Empty)
		{
		}

		[ResultColumn]
		public List<Article> Articles {get; set;}

		public override string ToString()
		{
			return this.TagName;
		}

		public Tag( string tagName)
		{
			this.TagName = tagName;
			this.Id = int.MinValue;
			this.Articles = new List<Article>();
		}
	}
}
```

![](../images/entity_obj.png)

##### Insert Operation

``` c#
using SubSonic; 
public bool Insert( Tag tag)
		{
			_database.Insert( "tag", "id", true, tag);

			return true;
		}
```

![](../images/insert_entity.png)

##### Update Operation
``` c#
public bool Update( Tag tag)
		{
			_database.Update( tag);

			return true;
		}
```

![](../images/update_entity.png)

##### Select Operation
``` c#
public bool TagExists(int tagId)
		{
			return _database.Exists<Tag>(tagId);
		}
		
```

![](../images/select_entity.png)

###### Delete Operation
``` c#
public bool Delete_entity(Tag tag)
		{
			using (var scope = _database.GetTransaction())
			{
				int rowsAffected = _database.Delete(tag);

				if (rowsAffected > 0)
				{
					scope.Complete();
					return true;
				}
				else
				{
					return false; // Deletion failed or the entity did not exist
				}
			}
		}
```

![](../images/delete_entity.png)


##### Query Operation

``` c#
public List<Tag> RetrieveAll()
		{
			return _database.Fetch<Tag, Article, Author, Tag>(
				new TagRelator().Map,
				"select * from tag " +
				"left outer join articleTag on articleTag.tagId = tag.id " +
				"left outer join article on article.id = articleTag.articleId " +
				"join author on author.id = article.author_id order by tag.tagName asc").ToList();
		}
```

![](../images/query.png)
