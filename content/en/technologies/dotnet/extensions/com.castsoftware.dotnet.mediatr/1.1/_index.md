---
title: "MediatR - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.mediatr

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for libraries such as MediatR, Mediator.Abstractions, FluentMediator, which implement the Mediator design pattern on .NET

## In what situation should you install this extension?

If your .NET application uses Mediator design pattern for messaging and you want
to view a model that shows the transaction between the message publisher and handler object
with their corresponding links

## Technology support

The following libraries are supported by this extension:

| Language | Library name | Namespace |Version | Supported |
| -------- | -------- | ------------------------------------------------------------------------- | :--------: | :----------------: |
| .NET     | [MediatR](https://github.com/jbogard/MediatR/wiki)  | MediatR | 0.1.0 to 12.3.0| :white_check_mark: |
| .NET     | [Mediator.Abstractions](https://www.nuget.org/packages/Mediator.Abstractions/)  | Mediator | 1.0.5 to 2.1.7 | :white_check_mark: |
| .NET     | [FluentMediator](https://www.nuget.org/packages/FluentMediator)  | FluentMediator | 0.0.1 to 0.4.7 | :white_check_mark: |
| .NET     | [Mediator.Net](https://www.nuget.org/packages/Mediator.Net)  | Mediator.Net | 1.0.0 to 4.8.0 | :white_check_mark: |
| .NET     | [Elsa.Mediator](https://www.nuget.org/packages/Elsa.Mediator)  | Elsa.Mediator | 3.0.0 to 3.2.3 | :white_check_mark: |
| .NET     | [Handyman.Mediator](https://www.nuget.org/packages/Handyman.Mediator)  | Handyman.Mediator | 1.0.0 to 12.0.0 | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies

The MediatR extension is dependent on following extensions:

- Internal extension (com.castsoftware.internal.platform)

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, you should manually install it.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the below objects and links created.

### Objects

| Icon | Description | Comment |
|---|---|---|
| ![](../images/670630258.png) | MediatR DotNet Publisher | an object is created for each message published and message type is resolved |
| ![](../images/670630256.png) | MediatR DotNet Handler | an object is created for each resolved Message Handler |
| ![](../images/670630255.png) | MediatR Unknown DotNet Publisher | an object is created for each message published and message type is not resolved in a method call |

### Link Types

| Link Type | Source and Destination Link | Supported APIs |
|---|---|---|
| callLink | Link between the caller C# method and the DotNet MediatR Publisher object | <details><summary>MediatR Publisher APIs</summary>MediatR.IMediator.Publish<br>MediatR.IMediator.PublishAsync<br>MediatR.IMediator.Send<br>MediatR.IMediator.SendAsync<br>MediatR.IMediator.Stream<br>MediatR.IMediator.CreateStream<br>MediatR.ISender.Send<br>MediatR.ISender.CreateStream<br>MediatR.IPublisher.Publish<br>MediatR.Mediator.Publish<br>MediatR.Mediator.Send</details><details><summary>Mediator Abstractions Publisher APIs</summary>Mediator.IPublisher.Publish<br>Mediator.ISender.Send<br>Mediator.ISender.CreateStream<br></details><details><summary>FluentMediator Publisher APIs</summary>FluentMediator.Pipelines.PipelineAsync.IAsyncMediator.PublishAsync<br>FluentMediator.Pipelines.CancellablePipelineAsync.ICancellablePipelineAsync.PublishAsync<br>FluentMediator.Pipelines.PipelineAsync.IPipelineAsync.PublishAsync<br>FluentMediator.Pipelines.CancellablePipelineAsync.ICancellableMediator.PublishAsync<br>FluentMediator.Pipelines.Pipeline.ISyncMediator.Publish<br>FluentMediator.Pipelines.PipelineAsync.IPipelineAsync.SendAsync<br>FluentMediator.Pipelines.Pipeline.IPipeline.Publish<br>FluentMediator.Pipelines.PipelineAsync.IAsyncMediator.SendAsync<br>FluentMediator.Pipelines.CancellablePipelineAsync.ICancellablePipelineAsync.SendAsync<br>FluentMediator.Pipelines.CancellablePipelineAsync.ICancellableMediator.SendAsync<br>FluentMediator.Pipelines.Pipeline.ISyncMediator.Send<br>FluentMediator.Pipelines.Pipeline.IPipeline.Send<br></details><details><summary>Mediator.Net Publisher APIs</summary>Mediator.Net.IMediator.SendAsync<br>Mediator.Net.IMediator.PublishAsync<br>Mediator.Net.IMediator.RequestAsync<br>Mediator.Net.IMediator.CreateStream</details><details><summary>Elsa Mediator Publisher APIs</summary>Elsa.Mediator.Contracts.ICommandSender.SendAsync<br>Elsa.Mediator.Contracts.INotificationSender.SendAsync<br>Elsa.Mediator.Contracts.IRequestSender.SendAsync</details><details><summary>Handyman.Mediator Publisher APIs</summary>Handyman.Mediator.IPublisher.Publish<br>Handyman.Mediator.ISender.Send<br>Handyman.Mediator.IMediator.Send<br>Handyman.Mediator.IMediator.Publish<br>Handyman.Mediator.IDynamicMediator.Publish<br>Handyman.Mediator.IDynamicMediator.Send<br>Handyman.Mediator.IDynamicPublisher.Publish<br>Handyman.Mediator.IDynamicSender.Send</details>|
| callLink | Link between the DotNet MediatR Handler object and the caller C# method  | <details><summary>MediatR Handler APIs</summary>MediatR.IRequestHandler<br>MediatR.INotificationHandler<br>MediatR.IStreamRequestHandler<br>MediatR.IAsyncNotificationHandler<br>MediatR.IAsyncRequestHandler<br>MediatR.IAsyncRequestStreamHandler</details><details><summary>Mediator Abstractions Handler APIs</summary>Mediator.ICommandHandler<br>Mediator.INotificationHandler<br>Mediator.IQueryHandler<br>Mediator.IRequestHandler<br>Mediator.IStreamCommandHandler<br>Mediator.IStreamQueryHandler<br>Mediator.IStreamRequestHandler</details><details><summary>FluentMediator Handler APIs</summary>FluentMediator.Pipelines.PipelineAsync.IPipelineAsyncBuilder.Call<br>FluentMediator.Pipelines.PipelineAsync.IPipelineAsyncBuilder.Return<br>FluentMediator.Pipelines.CancellablePipelineAsync.ICancellablePipelineAsyncBuilder.Call<br>FluentMediator.Pipelines.CancellablePipelineAsync.ICancellablePipelineAsyncBuilder.Return<br>FluentMediator.Pipelines.Pipeline.IPipelineBuilder.Call<br>FluentMediator.Pipelines.Pipeline.IPipelineBuilder.Return</details><details><summary>Mediator.Net Handler APIs</summary>Mediator.Net.Contracts.ICommandHandler<br>Mediator.Net.Contracts.IStreamRequestHandler<br>Mediator.Net.Contracts.IEventHandler<br>Mediator.Net.Contracts.IRequestHandler<br>Mediator.Net.Contracts.IStreamCommandHandler</details><details><summary>Elsa Mediator Handler APIs</summary>Elsa.Mediator.Contracts.ICommandHandler<br>Elsa.Mediator.Contracts.INotificationHandler<br>Elsa.Mediator.Contracts.IRequestHandler</details><details><summary>Handyman.Mediator Handler APIs</summary>Handyman.Mediator.IEventHandler<br>Handyman.Mediator.IRequestHandler</details>|

## Code Examples

### Publisher

#### MediatR Publish

```c#
using MediatR;
using Microsoft.Extensions.DependencyInjection;

public class Benchmarks
{
    private IMediator _mediator;
    private readonly Pinged _notification = new Pinged();

    public void GlobalSetup()
    {
        var services = new ServiceCollection();
        services.AddMediatR(cfg =>
        {
            cfg.RegisterServicesFromAssemblyContaining(typeof(Ping));
        });
        var provider = services.BuildServiceProvider();
        _mediator = provider.GetRequiredService<IMediator>();
    }

    public Task PublishingNotifications()
    {
        return _mediator.Publish(_notification);
    }
}
```
![](../images/mediatr_publihser.png)

#### MediatR Send

```c#

private static async Task<bool> IsHandlerForLessSpecificExceptionWorks(IMediator mediator, WrappingWriter writer)
    {
        var isHandledCorrectly = false;

        await writer.WriteLineAsync("Checking base handler to catch any exception...");
        try
        {
            await mediator.Send(new PingResourceTimeout { Message = "Ping to ISS resource" });
            isHandledCorrectly = IsExceptionHandledBy<TaskCanceledException, CommonExceptionHandler> (writer);
        }
        catch (Exception e)
        {
            await writer.WriteLineAsync(e.Message);
        }
        await writer.WriteLineAsync();

        return isHandledCorrectly;
    }

```
![](../images/mediatr_send.png)

#### MediatR CreateStream

```c#
public async Task Should_register_and_wrap_with_behavior()
    {
        var output = new Logger();
        IServiceCollection services = new ServiceCollection();
        services.AddSingleton(output);
        services.AddMediatR(cfg =>
        {
            cfg.RegisterServicesFromAssembly(typeof(Ping).Assembly);
            cfg.
        });
        var provider = services.BuildServiceProvider();

        var mediator = provider.GetRequiredService<IMediator>();

        var stream = mediator.CreateStream(new StreamPing { Message = "Ping" });

        await foreach (var response in stream)
        {
            response.Message.ShouldBe("Ping Pang");
        }
    }
```
![](../images/mediatr_create_stream.png)

#### Unknown Publisher

MediatR Unknown DotNet Publihser object is created where the message type that is being sent cannot be resolved 

```c#
public Task Should_raise_execption_on_null_request() => Should.ThrowAsync<ArgumentNullException>(async () => await _mediator.Send(default!));

```
![](../images/mediatr_unknown_publisher.png)

#### FluentMediator

```
using FluentMediator;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class TaskService : ITaskService
{
    private readonly ITaskRepository _taskRepository;
    private readonly ITaskFactory _taskFactory;
    private readonly TaskViewModelMapper _taskViewModelMapper;
    private readonly ITracer _tracer;
    private readonly IMediator _mediator;

    public TaskService(ITaskRepository taskRepository, TaskViewModelMapper taskViewModelMapper, ITracer tracer, ITaskFactory taskFactory, IMediator mediator)
    {
        _taskRepository = taskRepository;
        _taskViewModelMapper = taskViewModelMapper;
        _tracer = tracer;
        _taskFactory = taskFactory;
        _mediator = mediator;
    }

    public async System.Threading.Tasks.Task Delete(Guid id)
    {
        using (var scope = _tracer.BuildSpan("Delete_TaskService").StartActive(true))
        {
            var deleteTaskCommand = _taskViewModelMapper.ConvertToDeleteTaskCommand(id);
            await _mediator.PublishAsync(deleteTaskCommand);
        }
    }
}
```
![](../images/fluent_pub.png)

#### Mediator.Net

```
using System;
using System.Threading.Tasks;
using Mediator.Net
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
public class ValuesController : Controller
{
    private readonly IMediator _mediator;

    public ValuesController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    [Route(("event"))]
    public async Task PostEvent(int id, [FromBody]EventData value)
    {
        await _mediator.PublishAsync(new ResultCalculatedEvent(value.Result));
    }
    
}

```

![](../images/med_net_pub.png)

#### Handyman.Mediator

```
using Handyman.Mediator;
using Microsoft.Extensions.DependencyInjection;

public class PublisherSample : Sample
{
   public override async Task RunAsync(CancellationToken cancellationToken)
   {
      var mediator = ServiceProvider.GetRequiredService<IPublisher<Event>>();

      var @event = new Event();

      await mediator.Publish(@event, cancellationToken);
   }
}
```
![](../images/handy_pub.png)

### Handlers

#### MediatR NotificationHandler

```c#
using MediatR;

public class PingedHandler : INotificationHandler<Pinged>
{
    public Task Handle(Pinged notification, CancellationToken cancellationToken)
    {
        return Task.CompletedTask;
    }
}

```
![](../images/mediatr_noti_handler.png)

#### MediatR RequestHandler

```c#
using MediatR;

public class PingResourceTimeoutHandler : IRequestHandler<PingResourceTimeout, Pong>
{
    private readonly TextWriter _writer;

    public PingResourceTimeoutHandler(TextWriter writer) => _writer = writer;

    public Task<Pong> Handle(PingResourceTimeout request, CancellationToken cancellationToken)
    {
        throw new TaskCanceledException();
    }
}

```
![](../images/mediatr_request_handler.png)

#### MediatR StreamRequestHandler

```c#
using MediatR;

public class PingStreamHandler : IStreamRequestHandler<StreamPing, Pong>
    {
        private readonly Logger _output;

        public PingStreamHandler(Logger output)
        {
            _output = output;
        }
        public async IAsyncEnumerable<Pong> Handle(StreamPing request, [EnumeratorCancellation] CancellationToken cancellationToken)
        {
            _output.Messages.Add("Handler");
            yield return await Task.Run(() => new Pong { Message = request.Message + " Pang" }, cancellationToken);
        }
    }

```
![](../images/mediatr_stream_handler.png)

#### FluentMediator Handler

```
using System;
using System.Collections.Generic;

using FluentMediator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();

        services.AddScoped<TaskCommandHandler>();
        services.AddScoped<TaskEventHandler>();

        services.AddFluentMediator(builder =>
        {
            builder.On<CreateNewTaskCommand>().PipelineAsync().Return<Domain.Tasks.Task, TaskCommandHandler>((handler, request) => handler.HandleNewTask(request));

            builder.On<TaskCreatedEvent>().PipelineAsync().Call<TaskEventHandler>((handler, request) => handler.HandleTaskCreatedEvent(request));

            builder.On<DeleteTaskCommand>().PipelineAsync().Call<TaskCommandHandler>((handler, request) => handler.HandleDeleteTask(request));

            builder.On<TaskDeletedEvent>().PipelineAsync().Call<TaskEventHandler>((handler, request) => handler.HandleTaskDeletedEvent(request));
        });

    }
    }
```
![](../images/fluent_handler.png)

#### Mediator.Net.Contracts Handler
```
using Mediator.Net.Context;
using Mediator.Net.Contracts;
using Mediator.Net.WebApiSample.Handlers.CommandHandler;

namespace Mediator.Net.WebApiSample.Handlers.EventHandler
{
    public class ResultCalculatedEventHandler: IEventHandler<ResultCalculatedEvent>
    {
        private readonly IBoardcastService _boardcastService;

        public ResultCalculatedEventHandler(IBoardcastService boardcastService)
        {
            _boardcastService = boardcastService;
        }
        public Task Handle(IReceiveContext<ResultCalculatedEvent> context, CancellationToken cancellationToken)
        {
            _boardcastService.Boardcast(context.Message.Result);
            return Task.FromResult(0);
        }
    }
}

```
![](../images/med_cont_handler.png)

#### Handyman.Mediator Handler
```
using Handyman.Mediator;
using Microsoft.Extensions.DependencyInjection;

public class Handler : IEventHandler<Event>
{
   public async Task Handle(Event @event, CancellationToken cancellationToken)
   {
      await Task.Yield();

      Console.WriteLine(GetType().Name);
   }
}
```

![](../images/handy_handler.png)

### Publisher and Handler Linking

```
public class SendVoid
{
    private readonly IServiceProvider _serviceProvider;
    private Dependency _dependency;
    private readonly IMediator _mediator;

    public async Task Should_resolve_main_void_handler()
        {
            await _mediator.Send(new VoidPing());

            _dependency.Called.ShouldBeTrue();
        }
}

public class VoidPingHandler : IRequestHandler<VoidPing>
    {
        private readonly Dependency _dependency;

        public VoidPingHandler(Dependency dependency) => _dependency = dependency;

        public Task Handle(VoidPing request, CancellationToken cancellationToken)
        {
            _dependency.Called = true;

            return Task.CompletedTask;
        }
    }

```
![](../images/mediatr_pub_link.jpg)

## Limitations

- Unknown Publisher Objects will be created when the message type of message being published cannot be resolved.
