---
title: ".NET NMS - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.nms

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Apache NMS .NET APIs which are
responsible for consumer and publisher operations in .NET applications.
If your C# application utilizes Apache NMS for messaging-related
operations and the producer and consumer are handled using the C#
language, and you want to modelize the producer and consumer links with
appropriate objects and links, then you should install this extension.

## Supported libraries

| Library | Version | Supported |
|---|---|:---:|
| [Apache NMS](https://www.nuget.org/packages/Apache.NMS) | up to: 2.1.0 | :white_check_mark:  |
| [Apache NMS ActiveMQ](https://www.nuget.org/packages/Apache.NMS.ActiveMQ) | up to 2.1.0 | :white_check_mark: |
| [Apache NMS AMQP](https://activemq.apache.org/components/nms/providers/amqp/) | up to 2.1.0 | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The .NET NMS extension requires that
the following other CAST extensions are also installed (this will be
managed automatically by CAST Imaging Console):

-   CAST AIP Internal extension (internal technical extension)
-   Web Services Linker

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Imaging Console. If you need to use it, you should manually install the
extension using the Application - Extensions interface.

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/669253775.png) | Apache NMS ActiveMQ publisher |
| ![](../images/669253773.png) | Apache NMS ActiveMQ Receiver |
| ![](../images/669253774.png) | Apache NMS ActiveMQ unknown publisher |
| ![](../images/669253772.png) | Apache NMS ActiveMQ unknown Receiver |
| ![](../images/669253775.png) | Apache NMS AMQP publisher |
| ![](../images/669253773.png) | Apache NMS AMQP Receiver |
| ![](../images/669253774.png) | Apache NMS AMQP unknown publisher |
| ![](../images/669253772.png) | Apache NMS AMQP unknown Receiver |

### Links

| Link Type | Source and Destination of link | Supported APIs |
|---|---|---|
| callLink | callLink between the caller C# method and the  Publisher object | Apache.NMS.IMessageProducer.Send<br>Apache.NMS.IMessageProducer.SendAsync<br>Apache.NMS.ActiveMQ.IMessageProducer.Send<br>Apache.NMS.ActiveMQ.IMessageProducer.SendAsync<br>Apache.NMS.AMQP.NmsProducer.Send<br>Apache.NMS.AMQP.NmsProducer.SendAsync  |
| callLink | callLink between the  Receiver object and the caller  C# method  | Apache.NMS.IMessageConsumer.Receive<br> Apache.NMS.IMessageConsumer.ReceiveAsync<br>Apache.NMS.ActiveMQ.IMessageReceiver.Send<br>Apache.NMS.ActiveMQ.IMessageReceiver.SendAsync<br>Apache.NMS.AMQP.NmsConsumer.Receive<br>Apache.NMS.AMQP.NmsConsumer.ReceiveAsync  |

### Example code scenarios

#### ActiveMQ

#### Publisher APIs

``` c#
 static void Main()
    {
        string brokerUri = "tcp://localhost:61616"; 
        string queueName = "dest_queue"; 

        using (IConnection connection = new ConnectionFactory(new Uri(brokerUri)).CreateConnection())
        {
            connection.Start();

            using (ISession session = connection.CreateSession())
            {
                IDestination destination = session.GetQueue(queueName);

                // Produce message
                ProduceMessage(session, destination);

                // Consume message
                ConsumeMessage(session, destination);

                connection.Stop();
            }
        }
    }

    static void ProduceMessage(ISession session, IDestination destination)
    {
        using (IMessageProducer producer = session.CreateProducer(destination))
        using (ITextMessage message = session.CreateTextMessage("Hello, ActiveMQ!"))
        {
            producer.Send(message);
            Console.WriteLine("Message sent: " + message.Text);
        }
    }
```

![](../images/669253778.png)

#### Receiver APIs

``` c#
 static void ConsumeMessage(ISession session, IDestination destination)
    {
        using (IMessageConsumer consumer = session.CreateConsumer(destination))
        {
            ITextMessage receivedMessage = consumer.Receive() as ITextMessage;
            if (receivedMessage != null)
            {
                Console.WriteLine("Received message: " + receivedMessage.Text);
            }
            else
            {
                Console.WriteLine("No messages in the queue.");
            }
        }
```

![](../images/669253777.png)

#### Dotnet Publisher and Receiver

``` c#
using Apache.NMS;
using Apache.NMS.ActiveMQ;
using System;

class Program
{
    static void Main()
    {
        string brokerUri = "tcp://localhost:61616"; 
        string queueName = "dest_queue"; 

        using (IConnection connection = new ConnectionFactory(new Uri(brokerUri)).CreateConnection())
        {
            connection.Start();

            using (ISession session = connection.CreateSession())
            {
                IDestination destination = session.GetQueue(queueName);

                // Produce message
                ProduceMessage(session, destination);

                // Consume message
                ConsumeMessage(session, destination);

                connection.Stop();
            }
        }
    }

    static void ProduceMessage(ISession session, IDestination destination)
    {
        using (IMessageProducer producer = session.CreateProducer(destination))
        using (ITextMessage message = session.CreateTextMessage("Hello, ActiveMQ!"))
        {
            producer.Send(message);
            Console.WriteLine("Message sent: " + message.Text);
        }
    }

    static void ConsumeMessage(ISession session, IDestination destination)
    {
        using (IMessageConsumer consumer = session.CreateConsumer(destination))
        {
            ITextMessage receivedMessage = consumer.Receive() as ITextMessage;
            if (receivedMessage != null)
            {
                Console.WriteLine("Received message: " + receivedMessage.Text);
            }
            else
            {
                Console.WriteLine("No messages in the queue.");
            }
        }
    }
}
```

![](../images/669253776.png)

#### AMQP

#### Publisher APIs

``` c#
private void SendMessage(string message)
        {
            var response = _session.CreateTextMessage();
            response.Text = message;
            _producer.Send(response);
        }
```

![](../images/amqp_publisher.png)

#### Receiver APIs

``` c#
private ITextMessage ReceiveMessage()
        {
            return _consumer.Receive() as ITextMessage;
        }
```

![](../images/amqp_receiver.png)

#### Linking code

``` c#
using System;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;

namespace Tutorial.ProfanityFilter.Svc
{
    class Program
    {
        private IMessageProducer _producer;
        private IMessageConsumer _consumer;
        private ISession _session;
        private const string Queue = "queue://App.Message.Processing.Queue";

        static void Main(string[] args)
        {
            new Program().Start();
        }

        private void Start()
        {
            while (true)
            {
                var request = ReceiveMessage();
                var requestText = request?.Text;
                if (string.IsNullOrWhiteSpace(requestText)) continue;

                Task.Factory.StartNew(async () =>
                {
                    var messageToPublish = await ProfanityFilterService.Filter(requestText);
                    SendMessage(messageToPublish);
                });
            }
        }

        private ITextMessage ReceiveMessage()
        {
            return _consumer.Receive() as ITextMessage;
        }

        private void SendMessage(string message)
        {
            var response = _session.CreateTextMessage();
            response.Text = message;
            _producer.Send(response);
        }

        private Program()
        {
            InitializeCommunication();
        }

        private void InitializeCommunication()
        {
            const string userName = "admin";
            const string password = "admin";
            const string uri = "amqp:tcp://localhost:61616";
            var connecturi = new Uri(uri);
            var factory = new NMSConnectionFactory(connecturi);
            var connection = factory.CreateConnection(userName, password);
            connection.Start();
            _session = connection.CreateSession();
            var queueDestination = SessionUtil.GetDestination(_session, Queue);
            _consumer = _session.CreateConsumer(queueDestination);
            _producer = _session.CreateProducer(queueDestination);
        }
    }
}

```

![](../images/amqp_linking.png)

## Known limitations

-   Unknown Publisher/Receiver Objects will be created if the evaluation fails to resolve the necessary parameter.
