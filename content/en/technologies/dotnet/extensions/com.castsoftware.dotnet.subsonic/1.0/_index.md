---
title: "Sub Sonic - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.subsonic

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for the SubSonic framework for .NET.

## In what situation should you install this extension?

This extension should be installed when analyzing a .NET project that uses SubSonic framework, and you want to
view CRUD transactions of the SubSonic objects. Links to corresponding SQL database tables can also be resolved, provided that the SQL database has been extracted and DDL has been created.

## Technology support

The following libraries are supported by this extension:

| Language | Library name | Namespace | Version | Supported |
|---|---|---|---|:---:|
| .NET | [SubSonic](https://github.com/subsonic/SubSonic-2.0) | SubSonic | 2.0.0.0 to 2.2.0.0 | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For applications using any of the above mentionned libraries, this extension will be automatically installed. For upgrade, if the Extension Strategy is not set to Auto Update, you can manually upgrade the extension.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the below objects and links created.

### Objects
| Icon | Description | Comment |
|------|-------------|---------|
| ![entity_icon](../images/entity_icon.png) | SubSonic Entity   | An object is created for each Subsonic Persistent Object |
| ![entity_operation](../images/entity_operation.png) | SubSonic Entity Operation   | An object is created for each Subsonic Persistent Object CRUD operation|
| ![unknown_entity_icon](../images/Unknown_Entity.png) | SubSonic Unknown Entity   | An object is created for when Subsonic Persistent Object cannot be resolved |
| ![unknown_entity_operation](../images/UnknownEntity_Operation.png) | SubSonic Unknown Entity Operation   | An object is created for each Subsonic Persistent Object CRUD operation and respective Entity cannot be resolved|
| ![sql_query_icon](../images/sql_query_icon.png) | SubSonic SQL Query | An object is created for each direct SQL query operation found in Subsonic project and resolved in a method call|
| ![unknown_sql_query_icon](../images/unknown_sql_query_icon.png) | SubSonic Unknown SQL Query | An object is created for each direct SQL query found in Subsonic project and the exact query cannot be resolved |


### Links
| Link Type | Caller | Callee | APIs Supported |
| --- | --- | --- | --- |
| callLink | C# Method | SubSonic Entity Operation <br>SubSonic Unknown Entity Operation| SubSonic.ActiveList.Find<br>SubSonic.ActiveList.SaveAll<br>SubSonic.ActiveList.BatchSave<br>SubSonic.ActiveRecord.Save<br>SubSonic.ActiveRecord.FetchByID<br>SubSonic.ActiveRecord.GetInsertCommand<br>SubSonic.ActiveRecord.GetUpdateCommand<br>SubSonic.ActiveRecord.GetDeleteCommand<br>SubSonic.ActiveRecord.GetSaveCommand<br>SubSonic.ActiveRecord.Delete<br>SubSonic.ActiveRecord.DeleteByParameter<br>SubSonic.ActiveRecord.Destroy<br>SubSonic.ActiveRecord.DestroyByParameter<br>  |
| callLink | C# Method | SubSonic SQL Query <br> SubSonic Unknown SQL Query | SubSonic.AbstractList.Load<br>SubSonic.AbstractList.Sort<br>SubSonic.AbstractList.OrderByAsc<br>SubSonic.AbstractList.OrderByDesc<br>SubSonic.AbstractList.GetListName<br>SubSonic.AbstractList.LoadAndCloseReader<br>SubSonic.Query.Inspect<br>SubSonic.Query.GetRecordCount<br>SubSonic.Query.ExecuteReader<br>SubSonic.Query.ExecuteDataSet<br>SubSonic.Query.ExecuteJoinedDataSet<br>SubSonic.Query.ExecuteScalar<br>SubSonic.Query.Execute<br>SubSonic.DataService.ExecuteTransaction<br>SubSonic.DataService.GetRecordCount<br>SubSonic.DataService.GetReader.ExecuteScalar<br>SubSonic.DataService.GetDataSet.ExecuteQuery<br>|
| useLink | SubSonic Entity Operation | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | SubSonic Entity Operation | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|
| useLink | SubSonic SQL Query | Table, View | Created by SQLAnalyzer when DDL source files are analyzed|
| callLink | SubSonic SQL Query | Procedure | Created by SQLAnalyzer when DDL source files are analyzed|

### Examples

#### Persistent Objects


##### Entity Object

``` c#
using SubSonic; 
using SubSonic.Utilities;

namespace MettleSystems.dashCommerce.Content
{
	public partial class Credit : ActiveRecord<Credit>
	{		
		public Credit(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
		
		  
		[XmlAttribute("CreditID")]
		public int CreditID 
		{
			get { return GetColumnValue<int>("CreditID"); }
			set { SetColumnValue("CreditID", value); }

		}
		  
		[XmlAttribute("AccountID")]
		public string AccountID 
		{
			get { return GetColumnValue<string>("AccountID"); }
			set { SetColumnValue("AccountID", value); }

		}		  
		[XmlAttribute("Amount")]
		public decimal Amount 
		{
			get { return GetColumnValue<decimal>("Amount"); }
			set { SetColumnValue("Amount", value); }

		}
		public struct Columns
		{
			 public static string CreditID = @"CreditID";
			 public static string AccountID = @"AccountID";
			 public static string Amount = @"Amount";
						
		}
	}
}
```

![](../images/SubSonicEntityObject.png)

#### CRUD Operations


##### Insert Operation

``` c#
using SubSonic; 
using SubSonic.Utilities;

namespace Subsonic.Crud {
    class Program {
        static void Main(string[] args) {
                    
        public static void Insert(Guid varPageGuid,int varParentId,string varTitle,string varMenuTitle,string varKeywords,string varDescription,int varSortOrder,int varTemplateId,string varCreatedBy,DateTime varCreatedOn,string varModifiedBy,DateTime varModifiedOn)
		{
			Page item = new Page();
			
			item.PageGuid = varPageGuid;			
			item.ParentId = varParentId;	
			item.Title = varTitle;			
			item.MenuTitle = varMenuTitle;			
			item.Keywords = varKeywords;			
			item.Description = varDescription;			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
        }
    }
}
```

![](../images/SubSonicObject_Save.png)


##### Select Operation

``` c#
using SubSonic; 
using SubSonic.Utilities;

namespace Subsonic.Crud {
    class Program {
        static void Main(string[] args) {
                    
        	public MettleSystems.dashCommerce.Content.Page Page
		{
			get { return MettleSystems.dashCommerce.Content.Page.FetchByID(this.PageId); }

			set { SetColumnValue("PageId", value.PageId); }

	    }
    }
}
```

![](../images/SunSonicObject_Select.png)


##### Delete Operation

``` c#
using SubSonic; 
using SubSonic.Utilities;

namespace Subsonic.Crud {
    class Program {
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object PageId)
        {
            return (Page.Delete(PageId) == 1);
        }
    }
}
```

![](../images/SubSonicObject_Delete.png)


#### Query Based CRUD Operations


##### Raw Query Operation 

``` c#
namespace Subsonic.Crud{
    class Program {
        		public static void SaveRegionMap(int varPageId, RegionCollection items)
		{
			QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
			//delete out the existing
			QueryCommand cmdDel = new QueryCommand("DELETE FROM dashCommerce_Content_Page_Region_Map WHERE PageId=@PageId", Page.Schema.Provider.Name);
			cmdDel.AddParameter("@PageId", varPageId);
			coll.Add(cmdDel);
			DataService.ExecuteTransaction(coll);
			foreach (Region item in items)
			{
				PageRegionMap varPageRegionMap = new PageRegionMap();
				varPageRegionMap.SetColumnValue("PageId", varPageId);
				varPageRegionMap.SetColumnValue("RegionId", item.GetPrimaryKeyValue());
				varPageRegionMap.Save();
			}

	} 
}
```
![](../images/SubSonicExtensions_Query.png)

##### Custom Query Operation

``` c#
namespace Subsonic.Crud{
    class Program {
	
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public PageCollection FetchAll()
        {
            PageCollection coll = new PageCollection();
            Query qry = new Query(Page.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
	}
}
```
![](../images/SubSonicExtensions_Custom_Query.png)

