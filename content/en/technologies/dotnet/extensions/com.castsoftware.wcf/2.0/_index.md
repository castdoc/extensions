---
title: "WCF (Windows Communication Foundation) - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.wcf

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for WCF (Windows Communication
Foundation). The calculation of Automated Function Points for your .NET
analyses will be supplemented through the creation of new objects and
links specific to the WCF framework that will link back to objects/links
produced by the base .NET Analyzer.

## In what situation should you install this extension?

If your .NET application contains WCF (Windows Communication Foundation)
source code and you want to view these object types and their links,
then you should install this extension.

## WCF framework support

The following WCF frameworks are supported by this extension:

| Version                | Supported          |
|------------------------|:------------------:|
| All versions up to 4.5 | :white_check_mark: |

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-------------------------------:|:------------------:|
| :white_check_mark:              | :white_check_mark: |

A Transaction definition is automatically imported when the extension is
installed. This definition will be available in the CAST Transaction
Configuration Center.

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST Imaging
Console. You can manage the extension using the **Application -
Extensions** interface:

![](../images/670630431.jpg)

## What results can you expect?

### Objects

| Icon                         | Description                 |
|:----------------------------:|-----------------------------|
| ![](../images/670630428.png) | WCF Configuration           |
| ![](../images/670630427.png) | WCF Client Endpoint         |
| ![](../images/670630426.png) | WCF Service Endpoint        |
| ![](../images/670630425.png) | WCF Operation Contract      |
| ![](../images/670630424.png) | WCF Service Contract        |
| ![](../images/670630428.png) | WCF Service                 |
| ![](../images/670630423.png) | WCF Operation Method Delete |
| ![](../images/670630422.png) | WCF Operation Method Get    |
| ![](../images/670630421.png) | WCF Operation Method Post   |
| ![](../images/670630420.png) | WCF Operation Method Put    |
| ![](../images/670630421.png) | WCF Post Resource Service   |
| ![](../images/670630419.png) | WCF Data Contract           |

#### Configuration objects

Configuration objects are mapped on the Web.config or App.config file
available in the csharp project:

![](../images/670630418.png)

``` java
<configuration>
  <system.serviceModel>
    <services>
        <service name="TrailService.Service2">
             <endpoint address="http://localhost:59364/Service2.svc/Service2WebConfig" binding="basicHttpBinding" bindingConfiguration="HttpBinding" contract="TrailService.IService2"></endpoint>
            <host>
                <baseAddresses>
                    <add baseAddress="http://localhost:59364/Service2.svc/Service2WebConfig" />
                </baseAddresses>
            </host>

        </service>
      <service name="TrailService.CalculatorService">
        <endpoint address="CalculatorService" binding="basicHttpBinding" bindingConfiguration="HttpBinding" contract="TrailService.ICalculator"></endpoint>
        <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
        <host>
          <baseAddresses>
            <add baseAddress="http://localhost:59365/Service1.svc/" />
                <add baseAddress="http://localhost:59365/Service1.svc/dummy" />
          </baseAddresses>
        </host>
      </service>
    </services>
      <client>
          <endpoint address="http://localhost:59363/ClientService/ClientService"
                binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_IClientService"
                contract="IClientService" name="BasicHttpBinding_IClientService" />
      </client>
  </system.serviceModel>
</configuration>
```

#### Operation objects

![](../images/670630417.png)

The following .NET code is supported:

``` c#
[ServiceContract(Namespace = "http://Microsoft.ServiceModel.Samples")]
public interface ICalculator
{
    [OperationContract]
    double Add(double arg1, double arg2);
    [OperationContract]
    double Sub(double arg1, double arg2);
    [OperationContract]
    double Mul(double arg1, double arg2);
    [OperationContract]
    double Div(double arg1, double arg2);
}
```

Four "WCF Operation Contract" operations are created with urls
constructed from interface namespace, interface name and method names.

-   [http://Microsoft.ServiceModel.Samples/ICalculator/Add](http://Microsoft.ServiceModel.Samples/ICalculator/Add)
-   [http://Microsoft.ServiceModel.Samples/ICalculator/Sub](http://Microsoft.ServiceModel.Samples/ICalculator/Sub)
    (note here that the OperationContract "Name" parameters is used
    instaed of the method name.
-   [http://Microsoft.ServiceModel.Samples/ICalculator/Mul](http://Microsoft.ServiceModel.Samples/ICalculator/Mul)
-   [http://Microsoft.ServiceModel.Samples/ICalculator/Div](http://Microsoft.ServiceModel.Samples/ICalculator/Div)

``` java
    [ServiceContract]
    public interface IRestServiceImpl
    {
        [OperationContract]
        [WebInvoke(Method = "PUT", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "todos/{id}")]
        void UpdateTodos(string id);
    }
```

A "WCF Operation PUT" operation is created from UriTemplate parameter:

-   todos/{id}

``` java
    [ServiceContract]
    public interface IRestServiceImpl
    {
        [WebGet(UriTemplate = "/todos")]
        void GetTodos();
    }
```

A "WCF Operation GET" operation is created from UriTemplate parameter:

-   todos

#### Resource service objects

![](../images/670630416.png)

The following .NET code is supported:

``` java
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://Microsoft.ServiceModel.Samples", ConfigurationName="ServiceReferenceTrailService.ICalculator")]
    public interface ICalculator {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.Samples/ICalculator/Add", ReplyAction="http://Microsoft.ServiceModel.Samples/ICalculator/AddResponse")]
        double Add(double arg1, double arg2);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.Samples/ICalculator/Sub", ReplyAction="http://Microsoft.ServiceModel.Samples/ICalculator/SubResponse")]
        double Sub(double arg1, double arg2);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.Samples/ICalculator/Mul", ReplyAction="http://Microsoft.ServiceModel.Samples/ICalculator/MulResponse")]
        double Mul(double arg1, double arg2);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://Microsoft.ServiceModel.Samples/ICalculator/Div", ReplyAction="http://Microsoft.ServiceModel.Samples/ICalculator/DivResponse")]
        double Div(double arg1, double arg2);
    }
```

Four "WCF POST resource service" resource services are created with urls
constructed from "Action" OperationContractAttribute parameter.

-   http://Microsoft.ServiceModel.Samples/ICalculator/Add
-   http://Microsoft.ServiceModel.Samples/ICalculator/Sub
-   http://Microsoft.ServiceModel.Samples/ICalculator/Mul
-   http://Microsoft.ServiceModel.Samples/ICalculator/Div

#### Data contract object

![](../images/670630415.png)

``` java
    [Serializable]
    [DataContract(Namespace = "")]
    public class AddressDetails
    {
        [DataMember]
        public string Street1 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

    }
```

### Links

#### Links from WCF and .NET objects to the configuration objects where they are referenced

| Source                  | Link type | Target               | Example                      |
|-------------------------|-----------|----------------------|------------------------------|
| .NET Interface or class | referLink | WCF Service Endpoint | ![](../images/670630414.png) |
| WCF Operation           | referLink | WCF Service Endpoint | ![](../images/670630413.png) |
| .NET Interface or class | referLink | WCF Client Endpoint  | ![](../images/670630412.png) |

#### Links around WCF operations and resources

![](../images/670630411.png)

All links are "callLink" type.

-   from the .NET method to the WCF resource service corresponding to
    the method.
-   from the WCF operation to the .NET method corresponding to the
    operation.
-   from the WCF resource service to the WCF operation. This last link
    is created by the "web service linker" extension comparing the urls
    which are matching.

#### Links around WCF Data Contract

![](../images/670630410.png)

Links are:

-   accessWriteLink from the .NET class constructor to the WCF Data
    contract object.
-   accessReadLink from each .NET getter of each property of the class
    to the WCF Data contract object.
-   accessWriteLink from each .NET setter of each property of the class
    to the WCF Data contract object.

If a property has no getter or setter:

-   accessReadLink from the property to the WCF Data contract object.

### Structural rules

The following structural rules are provided:

| Release | Link |
|---|---|
| 2.0.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.12-funcrel) |
| 2.0.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.11-funcrel) |
| 2.0.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.10-funcrel) |
| 2.0.9-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.9-funcrel)   |
| 2.0.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.8-funcrel)   |
| 2.0.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.7-funcrel)   |
| 2.0.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.6-funcrel)   |
| 2.0.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.5-funcrel)   |
| 2.0.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.4-funcrel)   |
| 2.0.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.3-funcrel)   |
| 2.0.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.2-funcrel)   |
| 2.0.1-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.1-funcrel)   |
| 2.0.0-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.0-funcrel)   |
| 2.0.0-beta1    | [https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=\|\|2.0.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_wcf&ref=%7C%7C2.0.0-beta1)       |

## Limitations

In this section we list the most significant functional limitations that
may affect the analysis of applications using WCF:

-   If the configuration of WCF service is done through the code
    (endpoint configuration, service creation), this is not supported.
-   Concerning rules about configuration, violations are reported only
    if violations are in configuration files or in CSharp code, but not
    in VB code.
