---
title: "ASP.NET Web API Framework and Security Rules - 1.4"
linkTitle: "1.4"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnetweb

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for ASP.NET Web API. This extension will create links between server side APIs and client calls for `HttpGet`, `httpPut`, `HttpPost`, and `HttpDelete` methods.

### In what situation should you install this extension?

CAST recommends that this extension is installed whenever you are analyzing a .NET application. The extension
is automatically installed whenever a .NET application is delivered.

## ASP.NET Web API support

The following frameworks are supported by this extension:

| Version | Supported |
|---|:-:|
| Web API 2 | :white_check_mark: |
| ASP.NET Core Web API | :white_check_mark: |
| ASHX / ASMX | :white_check_mark: |
| OData server side | :white_check_mark: |

## Files analyzed

| Icons | File | Extension | Note |
|---|---|---|---|
| - | C# | `*.cs` |   |
| ![](../images/670629948.png) | .NET Razor | `*.cshtml` |   |
| ![](../images/670629947.png) | VB.NET | `*.vb` |   |
| - | JSON | `*.json`, `*.jsonld` |   |
| ![](../images/670629946.png) | ASPX | `*.aspx` |   |
| - | XML | `*.xml` |   |
| - | Configuration | `web.config`, `appsettings.json` | This extension broadcasts an XML parser for others extensions to analyze `web.config` files. |

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Language | Supported |
|---|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | - | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | C# | :white_check_mark: |

## Download and installation instructions

A specific version of the ASP.NET Web API Framework extension is shipped with CAST Core. However, this release may not be the release you want to use, therefore you should check before beginning the analysis that the correct extension release is being used.

If you need to change the release use the Included interface:

![](../images/670629945.jpg)

### CAST Transaction Configuration Center (TCC) configuration

A set of ASP.NET WebAPI specific items are now automatically
imported when the extension is installed:

![](../images/670629944.jpg)

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/670629943.png) | DotNet Get Operation |
| ![](../images/670629942.png) | DotNet Delete Operation |
| ![](../images/670629941.png) | DotNet Post Operation |
| ![](../images/670629940.png) | DotNet Put Operation |
| ![](../images/670629941.png) | DotNet Patch Operation |
| ![](../images/670629941.png) | DotNet Any Operation |
| ![](../images/670629943.png) | DotNet Controller Action |

### Links

A DotNet Controller Action is created for each controller method, and a
call link is created from this action to the method:

![](../images/670629939.png)

These controller actions may be directly called from clients through
HTML5 Razor method calls present in cshtml files:

![](../images/670629938.png)

``` java
<td>
     @Html.ActionLink("Details", "Details", new { id = item.DepartmentID })
</td>
```

One or more DotNet operations are created for one DotNet Controller
Action, because the DotNet Server may be called by other clients than
Razor clients. From HTML files or sections of HTML in .cshtml files:

![](../images/670629937.png)

``` java
<div href="Department/Details">
```

Controller actions are therefore always present in transactions, but
operations are present only for purely HTMLclients (not clients using
razor). As the same controller action may be called for several types of
HTML5 resource services, and even several types of  URLs (e.g:
department/details, department/details/{}, ...), operations which are
not called from a client are deleted at the end of analysis. In many
cases clients are written in razor, as such it would not be a ideal to
keep all operations as it would produce false transactions.

### ASHX/ASMX support

#### WebHandle/ProcessRequest

In ashx/asmx file:

``` java
<%@ WebHandler Language="C#" class="PREFIX.TaxServerInfo" %>
```

In IISHandler1.vb:

``` java
Imports System.Web
Public Class IISHandler1
    Implements IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        ' Write your handler implementation here.

    End Sub
End Class
```

Will create an operation:

![](../images/670629936.png)  

#### WebService/WebMethod

In asmx file:

``` java
<%@ WebService Language="vb" CodeBehind="WebService1.asmx.vb" class="WebApplication1.WebService1" %>
```

In vb file:

``` java
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class WebService1
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function HelloWorld() As String
       Return "Hello World"
    End Function

End Class
```

Will create an operation for each WebMethod annotated methods:

![](../images/670629935.png)  

### OData server side support

Controller actions and DotNet operations will be created from each ODataController method following OData naming conventions. The naming of each Controller action will keep the same standard. The naming of the DotNet operations will concatenate the OData service route prefix and the OData path. The odata route prefix is set during the registration of the OData service.The OData path is defined by convention or by attributes in the ODataController.

#### OData v8 routing prefix in ASP.NET Core Web API

The OData services is added by calling the `AddOData` method. The `AddRouteComponents` method is used to register a route, passing along the Edm model to associate it with.

```
// Program.cs
using Lab01.Models;
using Microsoft.AspNetCore.OData;
using Microsoft.OData.ModelBuilder;
 
var builder = WebApplication.CreateBuilder(args);
 
var modelBuilder = new ODataConventionModelBuilder();
modelBuilder.EntityType<Order>();
modelBuilder.EntitySet<Customer>("Customers");
 
builder.Services.AddControllers().AddOData(
    options => options.Select().Filter().OrderBy().Expand().Count().SetMaxTop(null).AddRouteComponents(
        "odata2",
        modelBuilder.GetEdmModel()));
 
var app = builder.Build();
 
app.UseRouting();
 
app.UseEndpoints(endpoints => endpoints.MapControllers());
 
app.Run();
```
In this example the odata route prefix is `odata2`.

#### OData v7 routing prefix in ASP.NET Core
The OData services is added by calling the `UseMvc` method. The `MapODataServiceRoutemethod` is used to register a route, passing along the Edm model to associate it with.

```
public void ConfigureServices(IServiceCollection services)
{
    services.AddMvc();
    services.AddOData();
}
 
public void Configure(IApplicationBuilder app)
{
    var builder = new ODataConventionModelBuilder(app.ApplicationServices);
 
    builder.EntitySet<Product>("Products");
 
    app.UseMvc(routeBuilder =>
    {
        // and this line to enable OData query option, for example $filter
        routeBuilder.Select().Expand().Filter().OrderBy().MaxTop(100).Count();
 
        routeBuilder.MapODataServiceRoute("ODataRoute", "odata3", builder.GetEdmModel());
 
        // uncomment the following line to Work-around for #1175 in beta1
        // routeBuilder.EnableDependencyInjection();
    });
}
```
In this example the odata route prefix is `odata3`.

#### OData routing prefix in ASP.NET Web API

The OData services is added by calling the `MapODataServiceRoute method` to register a route, passing along the Edm model to associate it with.

```
public static class WebApiConfig
{
    public static void Register(HttpConfiguration config)
    {
        var builder = new ODataConventionModelBuilder();
 
        builder.EntitySet<Product>("Products");
 
        config.MapODataServiceRoute("ODataRoute", "odata1", model);
    }
}
```
In this example the odata route prefix is `odata1`.

#### OData paths

##### CRUD operations routing  

|Request	|Example URI	|Action Name	|Example Action	|Cast Operation name|	Cast Operation type|
|:-----|:-----|:-----|:-----|:-----|:-----|
|GET /entityset	|odata/Products|	GetEntitySet or Get	GetProducts	|odata/products|	CAST_DotNet_GetOperation|
|GET /entityset(key)	|odata/Products(1)	|GetEntitySet or Get	|GetProduct(int key)	|odata/products/{}	|CAST_DotNet_GetOperation|
|GET /entityset/cast	|odata/Products/Models.Book	|GetControllerNameFromEntityType or GetFromEntityType	|GetProductsFromBook() or GetFromBook()|odata/products/models.book	|CAST_DotNet_GetOperation|
|GET /entityset(key)/cast	|odata/Products(1)/Models.Book	|GetEntityType	|GetBook(int key)	|odata/products/{}/models.book	|CAST_DotNet_GetOperation|
|POST /entityset	|odata/Products	|PostEntitySet or Post	|PostProduct(Product prod)	|odata/products	|CAST_DotNet_PostOperation|
|POST /entityset/cast	|odata/Products/Models.Book	|PostFromEntityType or PostEntitySetFromEntityType	|PostFromBook(Book book) or PostProductFromBook(Book book)|odata/products/models.book	|CAST_DotNet_PostOperation|
|PUT /entityset(key)	|odata/Products(1)	|PutEntityType or Put	|PutProduct(int key, Product prod)	|odata/products/{}	|CAST_DotNet_PutOperation|
|PUT /entityset(key)/cast	|odata/Products(1)/Models.Book	|PutEntityType or Put	|PutBook(int key, Book book)	|odata/products/{}/models.book	|CAST_DotNet_PutOperation|
|PATCH /entityset(key)	|/Products(1)	|PatchEntitySet or Patch	|PatchProduct(int key, Product prod)	|odata/products/{}	|CAST_DotNet_PatchOperation|
|PATCH /entityset(key)/cast	|/Products(1)/Models.Book	 |PatchEntityType or Patch	 |PatchBook(int key, Book book)	 |odata/products/{}/models.book	|CAST_DotNet_PatchOperation|
|DELETE /entityset(key)	|/Products(1)	|DeleteEntitySet or Delete	|DeleteProduct(int key)	|odata/products/{}	|CAST_DotNet_DeleteOperation|
|DELETE /entityset(key)/cast	|/Products(1)/Models.Book	|DeleteEntityType or Delete	|DeleteBook(int key)	|odata/products/{}/models.book	|CAST_DotNet_DeleteOperation|


##### OData navigation property routing

|Request	|Example URI	|Action Name	|Example Action	|Cast Operation expected name|	Cast Operation type|
|:-----|:-----|:-----|:-----|:-----|:-----|
|GET /entityset(key)/navigation	|odata/Products(1)/Supplier	|GetProperty or GetPropertyFromEntitySet	|GetSupplier(int key) or GetSupplierFromProduct(int key) |odata/products/{}/supplier	|CAST_DotNet_GetOperation|
|GET /entityset(key)/cast/navigation	|odata/Products(1)/Models.Book/Author	|GetProperty or GetPropertyFromEntityType	|GetAuthor(int key) or GetAuthorFromBook(int key)|odata/products/{}/models.book/author	|CAST_DotNet_GetOperation|
|POST /entityset(key)/navigation	|odata/Customers(1)/Orders	|PostToProperty or PostToPropertyFromEntitySet	|PostToOrdersFromCustomer(int key) or PostToOrders(int key)	|odata/customers/{}/orders	|CAST_DotNet_PostOperation|
|POST /entityset(key)/cast/navigation	|odata/Customers(1)/Models.Vip/Orders	|PostToPropertyFromEntityType	|PostToOrdersFromVip(int key)	|odata/customers/{}/models.vip/orders	|CAST_DotNet_PostOperation|
|PUT /entityset(key)/navigation	|odata/Customers(1)/Friend	|PutToProperty or PutToPropertyFromEntitySet	|PutToFriend(int key) or PutToFriendFromCustomer(int key)	|odata/customers/{}/friend	|CAST_DotNet_PutOperation|
|PUT /entityset(key)/cast/navigation	|odata/Customers(1)/Models.Vip/Friend	|PutToPropertyFromEntityType	|PutToFriendFromVip(int key)	|odata/customers/{}/models.vip/friend	|CAST_DotNet_PutOperation|
|PATCH /entityset(key)/navigation	|/Customers(1)/Friend	|PatchToProperty or PatchToPropertyFromEntitySet	|PatchToFriend(int key) or PatchToFriendFromCustomer(int key)	|odata/customers/{}/friend	|CAST_DotNet_PatchOperation|
|PATCH /entityset(key)/cast/navigation	|/Customers(1)/Models.Vip/Friend	|PatchToProperty or PatchToPropertyFromEntitySet	|PatchToFriendFromVip(int key)	|odata/customers/{}/models.vip/friend	|CAST_DotNet_PatchOperation|


##### Attribute routing
Attribute routing is enabled in OData but several syntaxes exist depending on the version exist:  

* Use ODataRouteAttribute("routepath") for OData   version 7.x or 6.x;
* Use Http[verb]Attribute("routepath") for OData version 8.x;
* Use RouteAttribute("routepath") with Http[verb]Attribute

#### Links created
We create links between controller action and DotNet operations.
When used with the extension com.castsoftware.dotnet.odata we also create links towards web services.

![](../images/odata_get.png)

#### Limitations
These functionalities are not supported:
* OData functions 
* OData actions
* Query options

### Rules

| Release | Link |
|---|---|
| 1.4.15-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.15-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.15-funcrel) |
| 1.4.14-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.14-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.14-funcrel) |
| 1.4.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.13-funcrel) |
| 1.4.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.12-funcrel) |
| 1.4.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.11-funcrel) |
| 1.4.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.10-funcrel) |
| 1.4.9-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.9-funcrel)   |
| 1.4.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.8-funcrel)   |
| 1.4.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.7-funcrel)   |
| 1.4.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.6-funcrel)   |
| 1.4.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.5-funcrel)   |
| 1.4.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.4-funcrel)   |
| 1.4.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.3-funcrel)   |
| 1.4.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.2-funcrel)   |
| 1.4.1-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.1-funcrel)   |
| 1.4.0-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.0-funcrel)   |
| 1.4.0-beta1    | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.0-beta1)       |
| 1.4.0-alpha2   | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.0-alpha2](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.0-alpha2)     |
| 1.4.0-alpha1   | [https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=\|\|1.4.0-alpha1](https://technologies.castsoftware.com/rules?sec=srs_dotnetweb&ref=%7C%7C1.4.0-alpha1)     |


## Limitations

- URLs present in annotations, which are in a variable, are not supported.
