---
title: "REST Service Calls for .NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.service

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for some web services calls from C#.

## Supported REST client libraries

| Type | Library |
|---|---|
| Native | Windows.Web.Http.HttpClient<br>System.Net.Http.HttpClient <br>System.Net.HttpWebRequest  |
| Third-party | Refit<br>RestSharp.RestClient <br>- RestSharp.RestClient.Execute<br>- RestSharp.RestClient.GetAsync<br>- RestSharp.RestClient.PostAsync<br>- ... <br>Flurl.Http  |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

The REST Service Calls for .NET extension requires
that ≥ 1.3.4-funcrel or ≥ 1.4.1-funcrel of the [.NET Analyzer](../../com.castsoftware.dotnet/)  is also installed and used in order to ensure the most complete set of results. This dependency is not automatically handled when downloading the REST Service Calls for .NET extension, therefore you must ensure that the [.NET Analyzer](../../com.castsoftware.dotnet/) is downloaded and installed before starting an analysis.

## What results can you expect?

### Features

This extension analyzes web service calls made from C# code through
classic fluent APIs. For example with System.Net.Http.HttpClient:

``` c#
using System;
using System.Net.Http;

class Main
{
    void main()
    {
        var httpClient = new HttpClient();
        
        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get,"hello/world");
        
        httpClient.SendAsync(request);
    }
}
```

![](../images/523895131.png)

#### Refit

[Refit](https://github.com/reactiveui/refit) provides
the means to call web services through interfaces with attributes.
Therefore, analyzing:

``` java
using System.Threading.Tasks;
using Refit;

public interface IGitHubApi
{
    [Get("/users/{user}")]
    Task<string> GetUser(string user);
}
```

will create:

![](../images/557482439.png)

### Objects

| Icon | Description |
|---|---|
| ![](../images/523895135.png) | Dotnet Delete Resource Service |
| ![](../images/523895134.png) | Dotnet Get Resource Service |
| ![](../images/523895133.png) | Dotnet Post Resource Service |
| ![](../images/523895132.png) | Dotnet Put Resource Service |
| ![](../images/523895132.png) | Dotnet Patch Resource Service |

### Rules

None.

### Limitations

- When the code uses a custom URL builder, we cannot evaluate the URL.
- Configuration files are not  taken into account
