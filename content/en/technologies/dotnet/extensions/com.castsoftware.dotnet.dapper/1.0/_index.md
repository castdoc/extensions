---
title: "Dapper Framework - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension Id

com.castsoftware.dotnet.dapper

## What's new?

See [Dapper Framework - 1.0 - Release Notes](rn) for more information.

## Description

This extension provides support for the following:

| Framework                                                                                                                         | Description                                                                                                                                                                                                                                                                                                                                                                   |
| --------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [Dapper Framework](https://dapper-tutorial.net/)                                                                                  | Dapper is a simple Object Relational Mapper (ORM) for .NET, which is responsible for mapping between database and programming language. Dapper also extends the `IDbConnection` by providing useful extension methods to query your database. This extension supports the framework CRUD operations and SQL queries.                                                          |
| [Dapper Plus](https://dapper-plus.net/) (i.e. the Nuget repository [Z.Dapper.Plus](https://www.nuget.org/packages/Z.Dapper.Plus)) | The Dapper Plus (Z.Dapper.Plus) mapper - see https://dapper-plus.net/ and https://www.nuget.org/packages/Z.Dapper.Plus/ allows mapping of the class model (Entity) with the storage model (database) and provides options to perform Bulk Actions. It extends the IDbConnection with high-performance bulk operations like BulkInsert, BulkUpdate, BulkDelete, BulkMerge etc. |
| [Dapper.SimpleCRUD](https://dapper-tutorial.net/dapper-simplecrud)                                                                | Dapper.SimpleCRUD is a small library that provides simple crud APIs for Dapper. It takes advantage of dynamic programming to make it simple to work with data.                                                                                                                                                                                                                |
| [Dapper.Contrib](https://github.com/DapperLib/Dapper.Contrib)                                                                     | Dapper.Contrib is a NuGet library that extends the `IDbConnection` interface and adds some generic utility methods to Dapper.  It provides methods that allow less code to be written to perform basic CRUD operations.                                                                                                                                                       |
| [Dapper.SqlBuilder](https://github.com/DapperLib/Dapper/tree/main/Dapper.SqlBuilder)                                              | Dapper.SqlBuilder library provides various methods to build our SQL queries dynamically. It provides various methods like .Select, .Where, .InnerJoin etc.                                                                                                                                                                                                                    |
| [Dapper.FastCrud](https://github.com/MoonStorm/FastCrud/tree/master/Dapper.FastCrud)                                              | Dapper.FastCrud is a fast orm  that is built around essential features of the C# 6 / VB 14 which helps in improving the simplicity and maintenance levels of SQL operations.                                                                                                                                                                                                  |
| [Dapper.FluentMap.Dommel](https://github.com/henkmollema/Dapper-FluentMap/tree/master/src/Dapper.FluentMap.Dommel)                | Dapper.FluentMap.Dommel provide API's for resolving table and column names. Dapper.FluentMap.Dommel implements certain interfaces of Dommel and uses the configured mapping.                                                                                                                                                                                                  | 

## In what situation should you install this extension?

If your .NET application contains any source code from the frameworks
listed above, and you if want to view these object types and their
links, then you should install this extension. More specifically the
extension will identify:

-   "callLink" from C# methods to Dapper Entity Operation objects.
-   "callLink" from C# methods to Dapper Sql Query objects. 

## Technology support

| Item                        | Version               |     Supported      | Supported Technology | Notes                                                                                                                |
| --------------------------- | --------------------- | :----------------: | :------------------: | -------------------------------------------------------------------------------------------------------------------- |
| Dapper                      | 1.40.0 to 2.1.35      | :white_check_mark: |          C#          | see [https://www.nuget.org/packages/Dapper](https://www.nuget.org/packages/Dapper)                                   |
| Dapper Plus (Z.Dapper.Plus) | up to version 7.5.6  | :white_check_mark: |          C#          | see [https://www.nuget.org/packages/Z.Dapper.Plus](https://www.nuget.org/packages/Z.Dapper.Plus).                    |
| Dapper.SimpleCRUD           | up to version 2.3.0   | :white_check_mark: |          C#          | see [https://www.nuget.org/packages/Dapper.SimpleCRUD/](https://www.nuget.org/packages/Dapper.SimpleCRUD/)           |
| Dapper.Contrib              | up to version 2.0.78  | :white_check_mark: |          C#          | see [https://www.nuget.org/packages/Dapper.Contrib/](https://www.nuget.org/packages/Dapper.Contrib/)                 |
| Dapper.SqlBuilder           | up to version 2.0.78  | :white_check_mark: |          C#          | see [https://www.nuget.org/packages/Dapper.SqlBuilder/](https://www.nuget.org/packages/Dapper.SqlBuilder/)           |
| Dapper.FastCrud             | up to version 3.3.2  | :white_check_mark: |          C#          | see [https://www.nuget.org/packages/Dapper.FastCrud](https://www.nuget.org/packages/Dapper.FastCrud)                 |
| Dapper.FluentMap.Dommel     | up to version 2.0.0   | :white_check_mark: |          C#          | see [https://www.nuget.org/packages/Dapper.FluentMap.Dommel](https://www.nuget.org/packages/Dapper.FluentMap.Dommel) |

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For .NET applications using Dapper, the extension will be automatically installed. This is in place since October 2023. For upgrade, if the Extension Strategy is not set to Auto Update, you will need to manually install the extension.

## What results can you expect?

### Objects

| Icon                            | Type Description                | When is this object created?                                                                 |
|:-------------------------------:|---------------------------------|----------------------------------------------------------------------------------------------|
| ![](../images/670630004.png)    | Dapper SQL Query          | Created for each SQL query found and resolved in Dapper or Dapper SqlMapper CRUD method call |
| ![](../images/670630003.png)    | Dapper Entity                   | Created and used when CRUD operation is found in the method call                             |
| ![](../images/670630002.png)    | Dapper Entity Operation         | Created and used when CRUD operation is performed on Dapper Entity                           |
| ![](../images/UnknownQuery.png) | Dapper Unknown SQL Query | Used when the query could not be resolved
| ![](../images/unknown_entity.png) | Dapper Unknown Entity | Created and used when the entity could not be resolved
| ![](../images/unknown_entity_operation.png) | Dapper Unknown Entity Operation | Used when CRUD operation is performed and Entity could not be resolved                                                  |

### Links

| Link Type | Caller type | Callee type | Methods Supported |
|---|---|---|---|
| callLink | C# Method | Dapper Sql Query | <details><summary>Dapper.SqlMapper APIs</summary><br>Dapper.SqlMapper.Query<br>Dapper.SqlMapper.QueryAsync<br>Dapper.SqlMapper.QueryUnbufferedAsync<br>Dapper.SqlMapper.QueryFirst<br>Dapper.SqlMapper.QueryFirstAsync<br>Dapper.SqlMapper.QueryFirstOrDefault<br>Dapper.SqlMapper.QueryFirstOrDefaultAsync<br>Dapper.SqlMapper.QueryMultiple<br>Dapper.SqlMapper.QueryMultipleAsync<br>Dapper.SqlMapper.QuerySingle<br>Dapper.SqlMapper.QuerySingleAsync<br>Dapper.SqlMapper.QuerySingleOrDefault<br>Dapper.SqlMapper.QuerySingleOrDefaultAsync<br>Dapper.SqlMapper.Execute<br>Dapper.SqlMapper.ExecuteAsync<br>Dapper.SqlMapper.ExecuteScalar<br>Dapper.SqlMapper.ExecuteReader<br>Dapper.SqlMapper.ExecuteScalarAsync<br>Dapper.SqlMapper.ExecuteReaderAsync<br></details><br><details><summary>Dapper.SqlBuilder APIs</summary><br>Dapper.SqlBuilder.AddTemplate<br>Dapper.SqlBuilder.GroupBy<br>Dapper.SqlBuilder.Having<br>Dapper.SqlBuilder.InnerJoin<br>Dapper.SqlBuilder.Intersect<br>Dapper.SqlBuilder.Join<br>Dapper.SqlBuilder.LeftJoin<br>Dapper.SqlBuilder.OrderBy<br>Dapper.SqlBuilder.OrWhere<br>Dapper.SqlBuilder.RightJoin<br>Dapper.SqlBuilder.Select<br>Dapper.SqlBuilder.Set<br>Dapper.SqlBuilder.Where</details> |
| callLink | C# Method | Dapper Entity Operation | <details><summary>Z.Dapper.Plus.DapperPlusExtensions APIs</summary><br>Z.Dapper.Plus.DapperPlusExtensions.BulkInsert<br>Z.Dapper.Plus.DapperPlusExtensions.BulkInsertAsync<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkInsert<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkInsertAsync<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkInsert<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkInsertAsync<br>Z.Dapper.Plus.DapperPlusExtensions.SingleInsert<br>Z.Dapper.Plus.DapperPlusExtensions.SingleInsertAsync<br>Z.Dapper.Plus.DapperPlusExtensions.BulkMerge<br>Z.Dapper.Plus.DapperPlusExtensions.BulkMergeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkMerge<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkMergeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkMerge<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkMergeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.SingleMerge<br>Z.Dapper.Plus.DapperPlusExtensions.SingleMergeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.BulkSynchronize<br>Z.Dapper.Plus.DapperPlusExtensions.BulkSynchronizeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkSynchronize<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkSynchronizeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkSynchronize<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkSynchronizeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.SingleSynchronize<br>Z.Dapper.Plus.DapperPlusExtensions.SingleSynchronizeAsync<br>Z.Dapper.Plus.DapperPlusExtensions.BulkUpdate<br>Z.Dapper.Plus.DapperPlusExtensions.BulkUpdateAsync<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkUpdate<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkUpdateAsync<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkUpdate<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkUpdateAsync<br>Z.Dapper.Plus.DapperPlusExtensions.SingleUpdate<br>Z.Dapper.Plus.DapperPlusExtensions.SingleUpdateAsync<br>Z.Dapper.Plus.DapperPlusExtensions.BulkDelete<br>Z.Dapper.Plus.DapperPlusExtensions.BulkDeleteAsync<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkDelete<br>Z.Dapper.Plus.DapperPlusExtensions.AlsoBulkDeleteAsync<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkDelete<br>Z.Dapper.Plus.DapperPlusExtensions.ThenBulkDeleteAsync<br>Z.Dapper.Plus.DapperPlusExtensions.SingleDelete<br>Z.Dapper.Plus.DapperPlusExtensions.SingleDeleteAsync<br></details><br><details><summary>Z.Dapper.Plus.DapperPlusContext APIs</summary><br>Z.Dapper.Plus.DapperPlusContext.BulkInsert<br>Z.Dapper.Plus.DapperPlusContext.BulkInsertAsync<br>Z.Dapper.Plus.DapperPlusContext.SingleInsert<br>Z.Dapper.Plus.DapperPlusContext.SingleInsertAsync<br>Z.Dapper.Plus.DapperPlusContext.BulkMerge<br>Z.Dapper.Plus.DapperPlusContext.BulkMergeAsync<br>Z.Dapper.Plus.DapperPlusContext.SingleMerge<br>Z.Dapper.Plus.DapperPlusContext.SingleMergeAsync<br>Z.Dapper.Plus.DapperPlusContext.BulkSynchronize<br>Z.Dapper.Plus.DapperPlusContext.BulkSynchronizeAsync<br>Z.Dapper.Plus.DapperPlusContext.SingleSynchronize<br>Z.Dapper.Plus.DapperPlusContext.SingleSynchronizeAsync<br>Z.Dapper.Plus.DapperPlusContext.BulkUpdate<br>Z.Dapper.Plus.DapperPlusContext.BulkUpdateAsync<br>Z.Dapper.Plus.DapperPlusContext.SingleUpdate<br>Z.Dapper.Plus.DapperPlusContext.SingleUpdateAsync<br>Z.Dapper.Plus.DapperPlusContext.BulkDelete<br>Z.Dapper.Plus.DapperPlusContext.BulkDeleteAsync<br>Z.Dapper.Plus.DapperPlusContext.SingleDelete<br>Z.Dapper.Plus.DapperPlusContext.SingleDeleteAsync</details><br><details><summary>Dapper.SimpleCRUD APIs</summary><br>Dapper.SimpleCRUD.DeleteAsync<br>Dapper.SimpleCRUD.DeleteList<br>Dapper.SimpleCRUD.DeleteListAsync<br>Dapper.SimpleCRUD.Get<br>Dapper.SimpleCRUD.GetAsync<br>Dapper.SimpleCRUD.GetList<br>Dapper.SimpleCRUD.GetListAsync<br>Dapper.SimpleCRUD.GetListPaged<br>Dapper.SimpleCRUD.GetListPagedAsync<br>Dapper.SimpleCRUD.RecordCount<br>Dapper.SimpleCRUD.RecordCountAsync<br>Dapper.SimpleCRUD.Insert<br>Dapper.SimpleCRUD.InsertAsync<br>Dapper.SimpleCRUD.Update<br>Dapper.SimpleCRUD.UpdateAsync<br></details><br><details><summary>Dapper.Contrib APIs</summary><br>Dapper.Contrib.Extensions.SqlMapperExtensions.Delete<br>Dapper.Contrib.Extensions.SqlMapperExtensions.DeleteAll<br>Dapper.Contrib.Extensions.SqlMapperExtensions.DeleteAllAsync<br>Dapper.Contrib.Extensions.SqlMapperExtensions.DeleteAsync<br>Dapper.Contrib.Extensions.SqlMapperExtensions.Get<br>Dapper.Contrib.Extensions.SqlMapperExtensions.GetAll<br>Dapper.Contrib.Extensions.SqlMapperExtensions.GetAllAsync<br>Dapper.Contrib.Extensions.SqlMapperExtensions.GetAsync<br>Dapper.Contrib.Extensions.SqlMapperExtensions.Insert<br>Dapper.Contrib.Extensions.SqlMapperExtensions.InsertAsync<br>Dapper.Contrib.Extensions.SqlMapperExtensions.Update<br>Dapper.Contrib.Extensions.SqlMapperExtensions.UpdateAsync<br></details><br><details><summary>Dapper.FastCrud APIs</summary><br>Dapper.FastCrud.DapperExtensions.BulkDelete<br>Dapper.FastCrud.DapperExtensions.BulkDeleteAsync<br>Dapper.FastCrud.DapperExtensions.BulkUpdate<br>Dapper.FastCrud.DapperExtensions.BulkUpdateAsync<br>Dapper.FastCrud.DapperExtensions.CountAsync<br>Dapper.FastCrud.DapperExtensions.Delete<br>Dapper.FastCrud.DapperExtensions.DeleteAsync<br>Dapper.FastCrud.DapperExtensions.Find<br>Dapper.FastCrud.DapperExtensions.FindAsync<br>Dapper.FastCrud.DapperExtensions.Get<br>Dapper.FastCrud.DapperExtensions.GetAsync<br>Dapper.FastCrud.DapperExtensions.Insert<br>Dapper.FastCrud.DapperExtensions.InsertAsync<br>Dapper.FastCrud.DapperExtensions.Update<br>Dapper.FastCrud.DapperExtensions.UpdateAsync</details> |
| useLink | Dapper Sql Query | Table, View | Created by SQL Analyzer when DDL source files are analyzed |
| callLink | Dapper Sql Query | Procedure | As above |
| useLink | Dapper Entity Operation | Table, View | Created by WBS when DDL source files are analyzed by SQL Analyzer |
| callLink | Dapper Entity Operation | Procedure | As above |
| useLink | Dapper Sql Query | Missing Table | Created by Missing tables and procedures for .Net extension when the object is not analyzed. |
| callLink | Dapper Sql Query | Missing Procedure | As above |
| relyonLink | Dapper Entity | C# Class |

## Code Examples 

### Dapper

#### Insert Operation1

Insert Operation

``` c#
  private static void AddRecord(var builder)
            {
           
             var conn = new SqlConnection(builder.ConnectionString);
             int insertedUser= conn.Execute("INSERT INTO dbo.[Users] VALUES (1140481,'Andrew','Tate')");
             Console.WriteLine("'Insert' Inserted Rows: {0}", insertedUser);
             int insertedInfo= conn.Execute("INSERT INTO dbo.[Info] VALUES (1140481,'SSE','UK',6)");
             Console.WriteLine("'Insert' Inserted Rows: {0}", insertedInfo);
           
            }
```

![](../images/670630001.png)

#### Insert Operation2

Insert Operation2

Real caller example

``` c#
 private static void method_calls()
   {
            var insertQuery= "INSERT INTO dbo.[Users] VALUES (1140481,'Andrew','Tate')";
            var updateQuery= "UPDATE dbo.[Users] SET [FirstName] = 'Andrew' WHERE [Id] = 2";
            var deleteQuery= "DELETE FROM dbo.[Users] WHERE [ID] = 1140491";
            AddRecord(builder,insertQuery);
            UpdateRecord(builder,updateQuery);
            DeleteRecord(builder,deleteQuery);

    }

 private static void AddRecord(var builder,var insertQuery)
     {
           
         var conn = new SqlConnection(builder.ConnectionString);
         int insertedUser= conn.Execute(insertQuery);
         Console.WriteLine("'Insert' Inserted Rows: {0}", insertedUser);
         int insertedInfo= conn.Execute("INSERT INTO dbo.[Info] VALUES (1140481,'SSE','UK',6)");
         var insert_query="INSERT INTO dbo.[Info] VALUES (1140500,'SSE','USA',7)";
         int insertedInfo1=conn.Execute(insert_query);
           
       }
```

![](../images/670630000.png)

#### Update Operation

Update Operation

``` c#
private static void UpdateRecord(var builder)
            {
              var conn = new SqlConnection(builder.ConnectionString);
              int affectedRows = conn.Execute("UPDATE dbo.[Users] SET [FirstName] = 'John' WHERE [Id] = 3");
              Console.WriteLine("'UPDATE' Affected Rows: {0}", affectedRows);
            } 
```

![](../images/670629999.png)

#### Delete Operation

Delete Operation

``` c#
private static void DeleteRecord(var builder)
{
var conn = new SqlConnection(builder.ConnectionString);
int deletedRows= conn.Execute("DELETE FROM dbo.[Users] WHERE [ID]=1140481");
Console.WriteLine("'DELETE' Deleted Rows: {0}", DeletedRows);
}
```

![](../images/670629998.png)

### Z.DapperPlus Operations

#### BulkInsert Operation

BulkInsert Operation

BulkInsert

``` c#
public static void StepBulkInsert() 
    {
        suppliers.Add(  new Supplier() { SupplierName = "BulkInsert", ContactName = "BulkInsert", Products = new List<Product> 
        { new Product() {ProductName = "BulkInsert", Unit = "BulkInsert"},new Product() {ProductName = "BulkInsert", Unit = "BulkInsert"} ,new Product() {ProductName = "BulkInsert", Unit = "BulkInsert"}  }});
        
        products.Add(  new Product() {ProductID=321,ProductName = "BulkInsert", Unit = "BulkInsert",SupplierID=123});

        
        // STEP BULKINSERT
        var builder = new SqlConnectionStringBuilder();
        using (var connection = new SqlConnection(builder.ConnectionString))
        {
            connection.BulkInsert(suppliers).ThenForEach(x => x.Products.ForEach(y => y.SupplierID =  x.SupplierID)).ThenBulkInsert(x => x.Products);
            connection.BulkInsert(products);
        }
    }
```

![](../images/670629997.png)

#### BulkMerge Operation

BulkMerge

BulkMerge

``` c#
public static void StepBulkMerge() 
    {
        suppliers.Add(new Supplier() { SupplierName = "BulkMerge", ContactName = "BulkMerge", Products = new List<Product>()
        { new Product() { ProductName = "BulkMerge", Unit = "BulkMerge"},  new Product() { ProductName = "BulkMerge" , Unit = "BulkMerge"} ,  new Product() { ProductName = "BulkMerge", Unit = "BulkMerge" }     
        }});
         
        suppliers.ForEach(x => 
        {
            x.SupplierName = "BULKMERGE";
            x.Products.ForEach(y => y.ProductName = "BULKMERGE");
        }); 
        
        // STEP BULKMERGE
        

            var builder = new SqlConnectionStringBuilder();
            using (var connection = new SqlConnection(builder.ConnectionString))
            {
            connection.BulkMerge(suppliers).ThenForEach(x => x.Products.ForEach(y => y.SupplierID =  x.SupplierID)).ThenBulkMerge(x => x.Products);
            }
        
        
    }
```

![](../images/thenbulkalso.png)

#### BulkUpdate Operation

BulkUpdate

BulkUpdate

``` c#
public static void StepBulkUpdate() 
    { 
        suppliers.ForEach(x => 
        {
            x.SupplierName = "BULKUPDATE";
            x.Products.ForEach(y => y.ProductName = "BULKUPDATE");
        }); 
        
        
        // STEP BULKUPDATE
        var builder = new SqlConnectionStringBuilder();
        using (var connection = new SqlConnection(builder.ConnectionString))
        {
            connection.BulkUpdate(suppliers, x => x.Products);
        }
    }
```

![](../images/670629995.png)

#### BulkDelete Operation

BulkDelete Operation

BulkDelete

``` c#
public static void SteBulkDelete() 
    {
        // STEP BULKDELETE
        var builder = new SqlConnectionStringBuilder();
        using (var connection = new SqlConnection(builder.ConnectionString))
        {
            connection.BulkDelete(suppliers.SelectMany(x => x.Products)).BulkDelete(suppliers);
        }
    }
```

![](../images/670629994.png)

### Dapper.SimpleCRUD

#### Select Operation 

Select Operation

Select operation

``` c#
static void Main(string[] args)
    {
        DeleteSingleAuthor();
        List<Author> authors = GetAllAuthors();
        List<Book> books = GetAllBooks();
        InsertSingleAuthor();
        UpdateSingleBook();
        DeleteSingleAuthor();
        DeleteMultipleBooks();

    }

private static List<Book> GetAllBooks(string category)
    {
        var builder = new SqlConnectionStringBuilder();
        using (var conn= new SqlConnection(builder.ConnectionString))
        {
            List<Book> books = conn.GetList<Book>(new { Category = category }).ToList();
            return books;
        }
    }
```

![](../images/670629993.png)

#### Insert Operation

Insert Operation

Insert Operation

``` c#
static void Main(string[] args)
    {
        DeleteSingleAuthor();
        List<Author> authors = GetAllAuthors();
        List<Book> books = GetAllBooks();
        InsertSingleAuthor();
        UpdateSingleBook();
        DeleteSingleAuthor();
        DeleteMultipleBooks();

    }

    
private static void InsertSingleAuthor()
    {
        var builder = new SqlConnectionStringBuilder();
        using (var conn= new SqlConnection(builder.ConnectionString))
        {
            Author author = new Author()
            {
                FirstName = "Cokie",
                LastName = "Roberts"
            };
            
            conn.Insert<Author>(author);
        }
      }
```

![](../images/670629992.png)

#### Update Operation

Update operation

Update operation

``` c#
    static void Main(string[] args)
    {
        DeleteSingleAuthor();
        List<Author> authors = GetAllAuthors();
        List<Book> books = GetAllBooks();
        InsertSingleAuthor();
        UpdateSingleBook();
        DeleteSingleAuthor();
        DeleteMultipleBooks();

    }

    private static void UpdateSingleBook()
    {
        var builder = new SqlConnectionStringBuilder();
        using (var conn= new SqlConnection(builder.ConnectionString))
        {
            Book book = new Book { Id = 1, Title = "Introduction to AI", Category = "Software", AuthorId = 1 };
            conn.Update<Book>(book);
        }
    }
    
```

![](../images/670629991.png)

#### Delete Operation

Delete Operation

Delete Operation

``` c#
static void Main(string[] args)
    {
        DeleteSingleAuthor();
        List<Author> authors = GetAllAuthors();
        List<Book> books = GetAllBooks();
        InsertSingleAuthor();
        UpdateSingleBook();
        DeleteSingleAuthor();
        DeleteMultipleBooks();

    }

private static void DeleteMultipleBooks()
     {
        var builder = new SqlConnectionStringBuilder();
        using (var conn= new SqlConnection(builder.ConnectionString))
        {
            conn.DeleteList<Book>("where Id > 3");
        }
     }
```

![](../images/670629990.png)

### Dapper.Contrib

#### Insert Operation

Insert Operation

``` c#
 private static void InsertSingleAuthor()
        {
            var builder = new SqlConnectionStringBuilder();
            using (var conn= new SqlConnection(builder.ConnectionString))
            {
                Author author = new Author()
                {
                    FirstName = "William",
                    LastName = "Shakespeare"
                };
        
                conn.Insert<Author>(author);
            }
        }
```

![](../images/670629989.png)

#### Update Operation

Update Operation

``` c#
 private static void UpdateSingleBook()
        {
            var builder = new SqlConnectionStringBuilder();
            using (var conn= new SqlConnection(builder.ConnectionString))
            {
                Book book = new Book { Id = 1, Title = "Introduction to AI", Category = "Software", AuthorId = 1 };
                conn.Update<Book>(book);
            }
        }
```

![](../images/670629988.png)

#### Select Operation

Select Operation

``` c#
 private static List<Author> GetAllAuthors()
        {
            var builder = new SqlConnectionStringBuilder();
            using ( var conn= new SqlConnection(builder.ConnectionString))
            {
                List<Author> authors = conn.GetList<Author>().ToList();
                return authors;
            }
        }
```

![](../images/670629987.png)

### Dapper.SqlBuilder

#### Select Query

Select Query

``` c#
private static List<Author> GetAuthors()
            
            {
                var builder1 = new SqlConnectionStringBuilder();
                using (var connection = new SqlConnection(builder1.ConnectionString))
                {
                    var builder = new SqlBuilder();
                    builder.Select("Id");
                    builder.Select("FirstName");
                    builder.Select("LastName");
            
                    var builderTemplate = builder.AddTemplate("Select /select/ from Authors");
            
                    var authors = connection.Query<Author>(builderTemplate.RawSql).ToList();
            
                    return authors;
                }
            }
```

![](../images/670629986.png)

#### InnerJoin Query

InnerJoin Query

``` c#
private static List<Author> GetAuthorWithBooks()
            {
                var builder = new SqlBuilder();
                builder.Select("*");
            
                builder.InnerJoin("Books on Books.AuthorId=Authors.Id");
            
                var builderTemplate = builder.AddTemplate("Select /select/ from Authors /innerjoin/ ");
            
                var authorDictionary = new Dictionary<int, Author>();
                var builder1 = new SqlConnectionStringBuilder();
                using (var connection = new SqlConnection(builder1.ConnectionString))
                {
                    var authors = connection.Query<Author, Book, Author>(
                        builderTemplate.RawSql,
                        (author, book) =>
                        {
                            Author authorEntry;
            
                            if (!authorDictionary.TryGetValue(author.Id, out authorEntry))
                            {
                                authorEntry = author;
                                authorEntry.Books = new List<Book>();
                                authorDictionary.Add(authorEntry.Id, authorEntry);
                            }
            
                            authorEntry.Books.Add(book);
                            return authorEntry;
                        },
                        splitOn: "Id")
                    .Distinct()
                    .ToList();
            
                    return authors;
                }
            }
    }   
```

![](../images/670629985.png)

#### Where Query

Where Query

``` c#
private static Author GetAuthor(int id)
            {
                var builder1 = new SqlConnectionStringBuilder();
                using (var connection = new SqlConnection(builder1.ConnectionString))
                    {
                        var builder = new SqlBuilder();
                        builder.Select("Id");
                        builder.Select("FirstName");
                        builder.Select("LastName");
                
                        DynamicParameters parameters = new DynamicParameters();
                        parameters.Add("@MyParam", id, DbType.Int32, ParameterDirection.Input);
                
                        builder.Where("Id = @MyParam", parameters);
                
                        var builderTemplate = builder.AddTemplate("Select /select/ from Authors /where/ ");
                
                        var author = connection.Query<Author>(builderTemplate.RawSql, builderTemplate.Parameters).FirstOrDefault();
                
                        return author;
                    }
            }
```

![](../images/670629984.png)

### Dapper.FastCrud

#### Insert Operation

Insert Operation

 Expand source

``` c#
  private static void InsertBook()
    {
        var builder = new SqlConnectionStringBuilder();
        var dbConnection = new SqlConnection(builder.ConnectionString);
        dbConnection.Insert(new BookDbEntity() {BookID=324,BookName = "Data Structure",new StudentDbEntity(){FirstName="David", LastName="Roy"} });

    }
```

![](../images/670629983.png)

#### Update Operation

Update Operation

``` c#
 private static void UpdateStudents()
    {
        var builder = new SqlConnectionStringBuilder();
        var dbConnection = new SqlConnection(builder.ConnectionString);

        students.Add(new StudentDbEntity(){StudentId=1140481,FirstName="Ishanya", LastName="Mittal",
        new TeacherDbEntity(){OnlineTeacher="Vijay Chabraa", OnlineTeacherId=32 ,ClassroomTeacher="Priyanka Alahawat " ,ClassroomTeacherId=53 }},

        new StudentDbEntity(){StudentId=1140491,FirstName="Rachit", LastName="Bansal",
        new TeacherDbEntity(){OnlineTeacher="Vijay Chabraa", OnlineTeacherId=32 ,ClassroomTeacher="Priyanka Alahawat " ,ClassroomTeacherId=53 }}
        );

        dbConnection.BulkUpdate(students);
    }
```

![](../images/670629982.jpg)

#### Delete Operation

Delete Operation

``` c#
private static void DeleteBooks()
    {
        var builder = new SqlConnectionStringBuilder();
        var dbConnection = new SqlConnection(builder.ConnectionString);
        dbConnection.Delete(new BookDbEntity() { BookID=328, BookName = "Head First Java",new StudentDbEntity(){FirstName="Ishanya", LastName="Mittal"}});
        dbConnection.BulkDelete<BookDbEntity>();

    }
```

![](../images/670629981.png)

#### Select Operation

Select operation

``` c#
private static void GetWorkstationInventoryIndex()
        {
            var queryParams = new {
                                      EmployeeFirstName = "Jane",
                                      EmplopyeeLastName = "Doe",
                                      WorkstationMinInventoryIndex = 5
                                    }
            var builder = new SqlConnectionStringBuilder();
            var dbConnection = new SqlConnection(builder.ConnectionString);
            dbConnection.Find<EmployeeDbEntity>(options => options
                    .WithAlias("em")
                    .Include<WorkstationDbEntity>(join => join
                    .WithAlias("ws")                        
                    .On($"{nameof(EmployeeDbEntity.WorkstationId):of em} = {nameof(WorkstationDbEntity.WorkstationId):of ws}")
                    .MapResults(true)                                                                                                          
                    .Referencing<EmployeeDbEntity>(relationship => relationship
                        .FromAlias("em")
                        .FromProperty(employee => employee.Workstation)
                        .ToProperty(workstation => workstation.Employees))));
        }
```

![](../images/670629980.png)

### Dommel

#### Overriding of table name

Table Name

This class derives from `EntityMap<TEntity>` and allows you to map an
entity to a database table using the `ToTable()` method:

Overriding Table Name

``` c#
public class AuthorMap : DommelEntityMap<TEntity>
{
    public AuthorMap()
    {
        ToTable("tbl_Authors");

        // ...
    }
}
```

![](../images/670629979.png)

## Limitations

-   Objects will not be created if evaluation fails to resolve the
    necessary parameter.

-   The set of supported methods is limited to what is documented.
