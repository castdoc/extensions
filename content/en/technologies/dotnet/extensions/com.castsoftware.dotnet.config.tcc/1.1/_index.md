---
title: ".NET Function Point Configuration - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.config.tcc

## What's new?

Introducing a brand-new extension dedicated to entry/exit definitions. In order to get accurate transactions with specific definition of entry/exit points and to avoid duplication across extensions.

This simplifies some transactions' call graph, by removing:

*   Entry/end points property on objects
*   classes, methods, functions, interfaces objects that are not actual entry/end points

### Example of simplification in a transaction

(Examples from Java, but it's similar for .Net)

**Entry point**: on the right, the newly calculated transaction, with 2 nodes removed. The entry point is identified more precisely.
![deleted-entry-points-before-after](../../../../jee/extensions/com.castsoftware.java.config.tcc/images/deleted-entry-points-before-after.png)

**End point**: on the right, the newly calculated transaction, with 2 nodes removed, and 3 methods whose end points have been deleted. Only the actual End point remains.
![deleted-end-points-before-after](../../../../jee/extensions/com.castsoftware.java.config.tcc/images/deleted-end-points-before-after.png)

### Names of deleted packages and objects

* Entry points

  | Deleted definition     | Extension with new modelisation |
  |------------------------|---------------------------------|
  | System.Net (C#)        | REST Service Calls for .NET     |
  | Xamarin.Forms (VB, C#) | .NET XAML                       |

* End points

  | Deleted definition                         | Extension with new modelisation |
  |--------------------------------------------|---------------------------------|
  | Microsoft.Practices.EnterpriseLibrary (C#) | MS Enterprise Library Framework |
  | SQLite (C#)                                | ADO.NET                         |
  | System.Data (C#)                           | ADO.NET                         |
  | System.Data.Common (C#)                    | ADO.NET                         |
  | System.Data.Entity.DbContext (C#)          | .NET Entity Framework           |
  | System.Net (C#)                            | REST Service Calls for .NET     |

## Description

This extension defines entry/end points to compute Automated Function Points (ISO/IEC 19515). This is also used to create CAST Imaging Blueprint.

It will be installed automatically as a dependency of .NET Analyzer version ≥ 1.5.6-funcrel.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

| .NET Analyzer | Supported |
|---|:-:|
| ≥ 1.5.6-funcrel | :white_check_mark: |

## Download and installation instructions

This extension will be downloaded and installed automatically when .NET Analyzer version ≥ 1.5.6-funcrel is installed.
