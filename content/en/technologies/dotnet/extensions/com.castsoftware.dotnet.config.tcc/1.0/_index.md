---
title: ".NET Function Point Configuration - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.config.tcc

## What's new?

See [Release Notes](rn/).

## Description

This extension defines entry/end points to compute Automated Function Points (ISO/IEC 19515). This is also used to create CAST Imaging Blueprint.

It will be installed automatically as a dependency of .NET Analyzer version ≥ 1.5.6-funcrel. 

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

| .NET Analyzer | Supported |
|---|:-:|
| ≥ 1.5.6-funcrel | :white_check_mark: |

## Download and installation instructions

This extension will be downloaded and installed automatically when .NET Analyzer version ≥ 1.5.6-funcrel is installed.
