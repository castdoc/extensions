---
title: "NHibernate Framework - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension Id

com.castsoftware.dotnet.nhibernate

## What's new?

See [Release Notes](rn/) for more information.

## Description

This extension provides support for NHibernate Framework and Fluent NHibernate Framework. 

## In what situation should you install this extension?

If your C# application uses the NHibernate/Fluent NHibernate Framework and you want to view these object types and their links, then you should install this extension.

## Technology support

| Component | Version | Supported | Supported Technology |
|---|---|:-:|:-:|
| NHibernate Framework | 2.0 to 5.5 | :white_check_mark: | C# |
| Fluent NHibernate Framework | 2.0 to 3.0 | :white_check_mark: | C# |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Download and installation instructions

For .NET applications using NHibernate, the extension will be automatically installed. This is in place since
October 2023. For upgrades, if the Extension Strategy is not set to Auto Update, you can manually install the extension.

## What results can you expect?

### Objects

| Icon | Type Description | When is this object created? |
|---|---|---|
| ![](../images/670630340.png) | NHibernate Entity | Created when an entity is found in hbm xml file or ClassMap is being inherited in the Map class  |
| ![](../images/670630338.png) | NHibernate Entity Operation | Created and used when CRUD operation is performed on NHibernate Entity |
| ![](../images/670630336.png) | NHibernate SQL Query | Created for each SQL query found and resolved in an NHibernate method call |
| ![](../images/670630336.png) | NHibernate HQL Query | Created for each HQL query found and resolved in an NHibernate method call |
| ![](../images/UnknownQuery.png) | NHibernate Unknown SQL Query | Created when the query could not be resolved in an NHibernate method call |
| ![](../images/unknown_entity.png) | NHibernate Unknown Entity | Created and used when the entity could not be resolved |
| ![](../images/unknown_entity_operation.png) | NHibernate Unknown Entity Operation | Used when CRUD operation is performed and Entity could not be resolved |

### Links

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh">Caller type</th>
<th class="confluenceTh">Callee type</th>
<th class="confluenceTh">NHibernate API's Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>callLink </p>
<p><br />
</p></td>
<td class="confluenceTd">C# Method</td>
<td class="confluenceTd"><p>NHibernate Entity Operation</p>
<p><br />
</p></td>
<td class="confluenceTd">
<ul>
<details><summary>ISession APIs</summary>
<li>NHibernate.ISession.Delete</li>
<li>NHibernate.ISession.DeleteAsync</li>
<li>NHibernate.ISession.Get</li>
<li>NHibernate.ISession.GetAsync</li>
<li>NHibernate.ISession.Load</li>
<li>NHibernate.ISession.LoadAsync</li>
<li>NHibernate.ISession.Update</li>
<li>NHibernate.ISession.UpdateAsync</li>
<li>NHibernate.ISession.Query</li>
<li>NHibernate.ISession.QueryOver</li>
<li>NHibernate.ISession.Merge</li>
<li>NHibernate.ISession.MergeAsync</li>
<li>NHibernate.ISession.Persist</li>
<li>NHibernate.ISession.PersistAsync</li>
<li>NHibernate.ISession.Refresh</li>
<li>NHibernate.ISession.RefreshAsync</li>
<li>NHibernate.ISession.Replicate</li>
<li>NHibernate.ISession.ReplicateAsync</li>
<li>NHibernate.ISession.Save</li>
<li>NHibernate.ISession.SaveAsync</li>
<li>NHibernate.ISession.SaveOrUpdate</li>
<li>NHibernate.ISession.SaveOrUpdateAsync</li>
</details>
<details><summary>IStatelessSession APIs</summary>
<li>NHibernate.IStatelessSession.Delete</li>
<li>NHibernate.IStatelessSession.DeleteAsync</li>
<li>NHibernate.IStatelessSession.Get</li>
<li>NHibernate.IStatelessSession.GetAsync</li>
<li>NHibernate.IStatelessSession.Insert</li>
<li>NHibernate.IStatelessSession.InsertAsync</li>
<li>NHibernate.IStatelessSession.Query</li>
<li>NHibernate.IStatelessSession.QueryOver</li>
<li>NHibernate.IStatelessSession.Refresh</li>
<li>NHibernate.IStatelessSession.RefreshAsync</li>
<li>NHibernate.IStatelessSession.Update</li>
<li>NHibernate.IStatelessSession.UpdateAsync</li>
</details>
<details><summary>SessionImpl APIs</summary>
<li>NHibernate.Impl.SessionImpl.Delete</li>
<li>NHibernate.Impl.SessionImpl.DeleteAsync</li>
<li>NHibernate.Impl.SessionImpl.Get</li>
<li>NHibernate.Impl.SessionImpl.GetAsync</li>
<li>NHibernate.Impl.SessionImpl.ImmediateLoad</li>
<li>NHibernate.Impl.SessionImpl.ImmediateLoadAsync</li>
<li>NHibernate.Impl.SessionImpl.InternalLoad</li>
<li>NHibernate.Impl.SessionImpl.InternalLoadAsync</li>
<li>NHibernate.Impl.SessionImpl.Load</li>
<li>NHibernate.Impl.SessionImpl.LoadAsync</li>
<li>NHibernate.Impl.SessionImpl.Merge</li>
<li>NHibernate.Impl.SessionImpl.MergeAsync</li>
<li>NHibernate.Impl.SessionImpl.Persist</li>
<li>NHibernate.Impl.SessionImpl.PersistAsync</li>
<li>NHibernate.Impl.SessionImpl.PersistOnFlush</li>
<li>NHibernate.Impl.SessionImpl.PersistOnFlushAsync</li>
<li>NHibernate.Impl.SessionImpl.QueryOver</li>
<li>NHibernate.Impl.SessionImpl.Refresh</li>
<li>NHibernate.Impl.SessionImpl.RefreshAsync</li>
<li>NHibernate.Impl.SessionImpl.Replicate</li>
<li>NHibernate.Impl.SessionImpl.ReplicateAsync</li>
<li>NHibernate.Impl.SessionImpl.Save</li>
<li>NHibernate.Impl.SessionImpl.SaveAsync</li>
<li>NHibernate.Impl.SessionImpl.SaveOrUpdate</li>
<li>NHibernate.Impl.SessionImpl.SaveOrUpdateAsync</li>
<li>NHibernate.Impl.SessionImpl.Update</li>
<li>NHibernate.Impl.SessionImpl.UpdateAsync</li>
</details>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">C# Method</td>
<td class="confluenceTd"><p>NHibernate SQL Query</p>
<p>/ NHibernate HQL Query</p></td>
<td class="confluenceTd"><ul>
<details><summary>ICriteria APIs</summary>
<li>NHibernate.ICriteria.List</li>
<li>NHibernate.ICriteria.ListAsync</li>
<li>NHibernate.ICriteria.Future</li>
<li>NHibernate.ICriteria.UniqueResult</li>
<li>NHibernate.ICriteria.UniqueResultAsync</li>
</details>
<details><summary>ISession APIs</summary>
<li>NHibernate.ISession.GetNamedQuery</li>
</details>
<details><summary>IStatelessSession APIs</summary>
<li>NHibernate.IStatelessSession.GetNamedQuery</li>
</details>
<details><summary>SessionImpl APIs</summary>
<li>NHibernate.Impl.SessionImpl.ExecuteNativeUpdate</li>
<li>NHibernate.Impl.SessionImpl.ExecuteNativeUpdateAsync</li>
</details>
<details><summary>IQuery APIs</summary>
<li>NHibernate.IQuery.ExecuteUpdate</li>
<li>NHibernate.IQuery.ExecuteUpdateAsync</li>
<li>NHibernate.IQuery.List</li>
<li>NHibernate.IQuery.ListAsync</li>
<li>NHibernate.IQuery.Future</li>
<li>NHibernate.IQuery.UniqueResult</li>
<li>NHibernate.IQuery.UniqueResultAsync</li>

</details>
<details><summary>AdoNet APIs</summary>
<li>NHibernate.AdoNet.MySqlClientSqlCommandSet.ExecuteNonQuery</li>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteReader</li>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteReaderAsync</li>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteNonQuery</li>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteNonQueryAsync</li>
</details>
<details><summary>Engine APIs</summary>
<li>NHibernate.Engine.IBatcher.ExecuteNonQuery</li>
<li>NHibernate.Engine.IBatcher.ExecuteNonQueryAsync</li>
<li>NHibernate.Engine.IBatcher.ExecuteReader</li>
<li>NHibernate.Engine.IBatcher.ExecuteReaderAsync</li>
</details>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">NHibernate HQL Query</td>
<td class="confluenceTd">NHibernate Entity operation</td>
<td class="confluenceTd"><ul>
<details><summary>ISession APIs</summary>
<li>NHibernate.ISession.GetNamedQuery</li>
</details>
<details><summary>IStatelessSession APIs</summary>
<li>NHibernate.IStatelessSession.GetNamedQuery</li>
</details>
<details><summary>SessionImpl APIs</summary>
<li>NHibernate.Impl.SessionImpl.ExecuteNativeUpdate</li>
<li>NHibernate.Impl.SessionImpl.ExecuteNativeUpdateAsync</li>
</details>
<details><summary>IQuery APIs</summary>
<li>NHibernate.IQuery.ExecuteUpdate</li>
<li>NHibernate.IQuery.ExecuteUpdateAsync</li>
<li>NHibernate.IQuery.List</li>
<li>NHibernate.IQuery.ListAsync</li>
<li>NHibernate.IQuery.Future</li>
<li>NHibernate.IQuery.UniqueResult</li>
<li>NHibernate.IQuery.UniqueResultAsync</li>
</details>
<details><summary>AdoNet APIs</summary>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteReader</li>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteReaderAsync</li>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteNonQuery</li>
<li>NHibernate.AdoNet.AbstractBatcher.ExecuteNonQueryAsync</li>
<li>NHibernate.AdoNet.MySqlClientSqlCommandSet.ExecuteNonQuery</li>
</details>
<details><summary>Engine APIs</summary>
<li>NHibernate.Engine.IBatcher.ExecuteNonQuery</li>
<li>NHibernate.Engine.IBatcher.ExecuteNonQueryAsync</li>
<li>NHibernate.Engine.IBatcher.ExecuteReader</li>
<li>NHibernate.Engine.IBatcher.ExecuteReaderAsync</li>
</details>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd"><p>NHibernate SQL Query</p>
<p>/ NHibernate HQL Query</p></td>
<td class="confluenceTd">Table, View</td>
<td rowspan="2" class="confluenceTd">Created by <strong>SQL
Analyzer</strong> when DDL source files are analyzed</td>
</tr>
<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>NHibernate SQL Query</p>
<p>/ NHibernate HQL Query</p></td>
<td class="confluenceTd">Procedure</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd">NHibernate Entity Operation</td>
<td class="confluenceTd">Table, View</td>
<td rowspan="2" class="confluenceTd">Created
by <strong>WBS</strong> when DDL source files are analyzed by SQL
Analyzer</td>
</tr>
<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd">NHibernate Entity Operation</td>
<td class="confluenceTd">Procedure</td>
</tr>
<tr class="even">
<td class="confluenceTd">useLink</td>
<td class="confluenceTd"><p>NHibernate SQL Query</p>
<p>/ NHibernate HQL Query</p></td>
<td class="confluenceTd">Missing Table</td>
<td rowspan="2" class="confluenceTd">Created by <strong>Missing tables
and procedures for .Net </strong>extension when the object is not
analyzed.</td>
</tr>
<tr class="odd">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><p>NHibernate SQL Query</p>
<p>/ NHibernate HQL Query</p></td>
<td class="confluenceTd">Missing Procedure</td>
</tr>
<tr class="even">
<td class="confluenceTd">relyonLink</td>
<td class="confluenceTd"><p>NHibernate Entity</p>
</td>
<td class="confluenceTd">C# Class</td>
<td rowspan="2" class="confluenceTd"></td>
</tr>
</tbody>
</table>

### Code Examples 

#### Select Operation

Using hbm.xml file

``` c#
  public static void studentGet(NHibernate.ISession session)
        {
            var stdnt = session.Get<Student>(1);
            Console.WriteLine("Retrieved by ID");
            Console.WriteLine("{0} \t{1} \t{2}", stdnt.ID,
               stdnt.FirstMidName, stdnt.LastName);
        }
```

![](../images/670630335.png)

Using Fluent Nhibernate mapping

``` c#
using FluentNHibernate.Mapping;

namespace NHibernateExample
{
    class ParentMap : ClassMap<Parent>
    {
        public ParentMap()
        {
            Id(x => x.ID);
            Map(x => x.FirstMidName);
            Map(x => x.LastName);
            Table("ParentFluent");
        }
    }
}  

public static void parentUpdate(NHibernate.ISession session)
        {
            var parentGet = session.Get<Parent>(1);
            Console.WriteLine("Retrieved by ID");
            Console.WriteLine("{0} \t{1} \t{2}", parentGet.ID, parentGet.FirstMidName, parentGet.LastName);

            Console.WriteLine("Update the last name of ID = {0}", parentGet.ID);
            parentGet.LastName = "Donald";
            session.Update(parentGet);
            Console.WriteLine("\nFetch the complete list again\n");

        }
```

![](../images/670630334.png)

#### Update Operation

Update

``` c#
public static void studentUpdate(NHibernate.ISession session, Student stdntGet)
        {
           
            session.Update(stdntGet);
            Console.WriteLine("\nFetch the complete list again\n");

        }
```

![](../images/670630333.png)

#### Delete Operation

Delete

``` c#
public static void studentDelete(NHibernate.ISession session, Student stdntDel)
        {
            
            session.Delete(stdntDel);
            Console.WriteLine("\nFetch the complete list again\n");

        }
```

![](../images/670630332.png)

#### Query String

Execute Update

``` c#
public static void deleteDataUsingQuery(ISession session)
        {
            IQuery query = session.CreateQuery("DELETE FROM Customer WHERE Id = '12345'");
            query.ExecuteUpdate();
        }
```

![](../images/670630331.png)

ExecuteNonQuery Using ADOSQLCommandSet

``` c#
public static void updateDataUsingADOSQLCommandSetNonQuery(ISession session, CancellationToken cancellationToken)
        {
            MySqlClientSqlCommandSet mySqlClientSqlCommandSet = new MySqlClientSqlCommandSet(5);
            DbCommand dbCommand = session.Connection.CreateCommand();
            for (int i = 0; i < 5; i++)
            {
                dbCommand.CommandText = "INSERT INTO Customer (CustomerID, Name) VALUES (" +i.ToString()+ ", 'John Doe')";
                mySqlClientSqlCommandSet.Append(dbCommand);
            }
            
            mySqlClientSqlCommandSet.ExecuteNonQuery();

        }
```

![](../images/670630330.png)

ExecuteNonQuery Using EngineNonQuery

``` c#
public static void updateDataUsingEngineNonQuery(ISession session, CancellationToken cancellationToken)
        {
            IBatcher batcher = (IBatcher)session.CreateQueryBatch();
            batcher.BatchSize = 5;
            DbCommand dbCommand = session.Connection.CreateCommand();
            dbCommand.CommandText = "INSERT INTO Customer (CustomerID, Name) VALUES (1, 'John Doe')";

            batcher.ExecuteNonQuery(dbCommand);
        }
```

![](../images/670630329.png)

ExecuteNonQuery Using ADO

``` c#
public static void updateDataUsingADONonQuery(ISession session, CancellationToken cancellationToken)
        {
            DbCommand dbCommand = session.Connection.CreateCommand();
            dbCommand.CommandText = "INSERT INTO Customer (CustomerID, Name) VALUES (1, 'John Doe')";

            AbstractBatcher abstractBatcher = (AbstractBatcher)session.CreateQueryBatch();
            abstractBatcher.ExecuteNonQuery(dbCommand);
        }
```

![](../images/670630328.png)

ExecuteReader Using ADO

``` c#
public static void updateDataUsingADOReader(ISession session, CancellationToken cancellationToken)
        {
            DbCommand dbCommand = session.Connection.CreateCommand();
            dbCommand.CommandText = "SELECT * FROM Parent p WHERE p.ID IN( SELECT ID FROM Teacher WHERE ID = '12345')";

            AbstractBatcher abstractBatcher = (AbstractBatcher)session.CreateQueryBatch();
            DbDataReader reader = abstractBatcher.ExecuteReader(dbCommand);
        }
```

![](../images/670630327.png)

ExecuteReader Using Engine

``` c#
public static void updateDataUsingEngineReader(ISession session, CancellationToken cancellationToken)
        {
            IBatcher batcher = (IBatcher)session.CreateQueryBatch();
            batcher.BatchSize = 5;
            DbCommand dbCommand = session.Connection.CreateCommand();
            dbCommand.CommandText = "UPDATE Employee SET FirstMidName = 'Robert' WHERE ID = '12345'";

            DbDataReader reader = batcher.ExecuteReader(dbCommand);
        }
```

![](../images/670630326.png)

Criteria

``` c#
public static void studentSelect(NHibernate.ISession session)
        {
            var cc = session.CreateCriteria<Student>();
            var students = cc.List<Student>();

            foreach (var student in students)
            {
                Console.WriteLine("{0} \t{1} \t{2}", student.ID,
                   student.FirstMidName, student.LastName);
            }
        }
```

![](../images/670630325.png)

Query and QueryOver

``` c#
public static void fetchTouristUsingQuery(NHibernate.ISession session)
        {
            var tourists = session.Query<Tourist>().Where(c => c.FirstMidName.StartsWith("H"));

            foreach (var tourist in tourists.ToList())
            {
                Console.WriteLine(tourist);
            }
        }
```

![](../images/670630324.png)

``` c#
public static void fetchVendorUsingQueryOver(NHibernate.ISession session)
        {
            var vendors = session.QueryOver<Vendor>().Where(x => x.LastName == "Laverne");

            foreach (var vendor in vendors.List())
            {
                Console.WriteLine(vendor);
            }
        }
```

![](../images/670630323.png)

Named Queries

Named Queries are generated with the help of hbm.xml files relating to
the entity. Below is an example of one such hbm.xml file :

``` xml
<hibernate-mapping xmlns="urn:nhibernate-mapping-2.2">
    <class name="Employee" table="employees">
        <id name="Id" column="id">
            <generator class="native" />
        </id>
        <property name="Name" column="name" />
        <property name="Age" column="age" />
        <property name="Salary" column="salary" />

        <sql-query name="GetEmployeesWithSalaryGreaterThan">
            SELECT *
            FROM employees
            where Salary > :minSalary
        </sql-query>

        <query name="GetEmployeesWithSalaryLessThan">
            SELECT *
            FROM Employee
            where :minSalary > Salary
        </query>
    </class>
</hibernate-mapping>
```

Named SQL Queries

Named SQL Queries

``` c#
public static void fetchEmployeeUsingNamedSQLQuery(NHibernate.ISession session)
        {
            var query = session.GetNamedQuery("GetEmployeesWithSalaryGreaterThan");
            query.SetParameter("minSalary", 50000m);

            var results = query.List<Employee>();

            foreach (var employee in results)
            {
                Console.WriteLine("{0}: {1}", employee.Name, employee.Salary);
            }
        }
```

![](../images/670630322.png)

Named HQL Queries

Named HQL Queries

``` c#
public static void fetchEmployeeUsingNamedQuery(NHibernate.ISession session)
        {
            var query = session.GetNamedQuery("GetEmployeesWithSalaryLessThan");
            query.SetParameter("minSalary", 50000m);

            var results = query.List<Employee>();

            foreach (var employee in results)
            {
                Console.WriteLine("{0}: {1}", employee.Name, employee.Salary);
            }
        }
```

![](../images/670630321.png)
