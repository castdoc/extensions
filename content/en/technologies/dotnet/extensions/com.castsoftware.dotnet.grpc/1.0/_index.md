---
title: "gRPC-.NET - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet.grpc

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for gRPC when used inside .NET
source code. 

## In what situation should you install this extension?

If your .NET application source code uses the gRPC framework you
should install this extension (see [gRPC](../../../../multi/grpc/) for more information about this framework).

## Features

### Object structure and links

-   Creates gRPC service method objects which represent entry-points
    to the server.
-   Creates calls to gRPC service method objects which represent
    exit-points from the client.
-   The dependent [Web Services Linker](../../../../multi/com.castsoftware.wbslinker) is
    responsible for detecting and creating links between
    matching calls to gRPC Method objects and the gRPC
    Method objects.

### Support

This extension supports:

1.  The original C# implementation of gRPC (i.e. Grpc.Core nuget
    package)
2.  The grpc-dotnet implementation of gRPC (i.e. Grpc.Net.Client and
    Grpc.AspNetCore.Server nuget packages)

#### Server side

When a class inherits from *FooService*Grpc.FooServiceBase, for
any overiden method `FooMethod` the analyzer creates a gRPC Method
object named `FooService.FooMethod`. For instance when analyzing the
following source code, the extension creates a Greeter.sayHello gRPC
Method and a callLink to the CSharp Method `sayHello`.

``` c#
using Grpc.Core;
using firstgRPCService;

namespace firstgRPCService.Services
{
    public class GreeterService: Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }
        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            return Task.FromResult(new HelloReply
            {
                Message = "Hello! " + request.Name
            });
        }
    }
}
```

![](../images/632455198.png)

#### Client side

When analyzing the following source code, this extension will generate a
call to gRPC Method object name` Greeter.sayHello`. 

``` c#
using System.Threading.Tasks;
using Grpc.Net.Client;
using firstgRPCServiceClient;

class grpc_handler
{
    
    static async Task Main(string[] args)
    {
        // The port number must match the port of the gRPC server.
        var channel = GrpcChannel.ForAddress("https://localhost:7287");
        var client = new Greeter.GreeterClient(channel);
        var reply = await client.SayHelloAsync(
                        new HelloRequest { Name = "GreeterClient" });
        Console.WriteLine("Greetings: " + reply.Message);
        Console.WriteLine("Press any key to exit...");
        Console.ReadKey();
    }
}   
```

![](../images/632455197.png)

 [Web Services Linker](Web_Services_Linker) is then responsible for
linking that call to gRPC Method to any matching gRPC service
method.

![](../images/632455196.png)

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## What source code is needed by our analyzer?

In order to develop an application which uses gRPC one needs to go
through the following steps: 

-   Write at least one \*.proto file. This file is used to define
    the name of the services and their methods which will be available
    for remote procedure calls. 
-   Use protoc to generate source code (in a chosen language)
    containing the client and server. This code is generated based on
    the \*.proto files. 
-   Define the services and methods available through gRPC by extending
    the generated server.
-   Calls to these methods are carried out using clients created from
    Grpc.Net.Client.GrpcChannel.

CAST grpc.NET analyzer depends on the naming convention used by gRPC for
generating the source code based on the \*.proto
files. CAST grpc.NET analyzer need the \*.proto files, although the
generated source code is not required.

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The gRPC-.NET extension requires the
following CAST extensions to be installed:

-   [Web Services Linker](Web_Services_Linker)
-   [com.castsoftware.internal.platform](Internal_Extension)
    (Internal Extension)

-   Any dependent extensions are automatically downloaded and
    installed. You do not need to do anything.
-   The [.NET Analyzer](.NET_Analyzer) is not a dependency, but this
    extension will be automatically installed when .NET source code is
    delivered for analysis.

## Download and installation instructions

The extension will not be automatically downloaded and installed in CAST
Console. If you need to use it, you should manually install the
extension using the Application -
Extensions interface:

![](../images/632455195.jpg)

## What results can you expect?

### Objects

The following objects are displayed in CAST Enlighten:


| Icon  | Description  |
|---------|---------|
|![](../images/632455199.png) | DotNet gRPC service method |
|![](../images/632455199.png) | DotNet call to gRPC service method |

## Limitations

-   Only proto based applications are supported.
-   In some rare scenarios, undesirable 'DotNet call to gRPC service
    method' objects could get created in case of client-streaming rpc
    methods.
