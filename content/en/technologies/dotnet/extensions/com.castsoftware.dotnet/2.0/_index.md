---
title: ".NET Analyzer - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for analyzing applications built with .NET, .NET Standard and .NET Core technologies: objects and links between these objects are identified and Automated Function Point values are calculated. A set of .NET specific structural rules are also available with the extension.

## Extension compatibility and equivalence

This release of the extension is based on the previous 1.5.x releases. Please check the [release notes](rn) for more information. Despite the equivalence, there are some key differences in functionality between 2.x and 1.x releases and as a result results may differ:

- **Technology support**: 2.x no longer includes the Silverlight and the Xamarin support that was present in 1.x releases of the extension. All Xamarin-related functionality, including link resolution for Android, iOS, TvOS, WatchOS and UWP platforms, has been removed.
- **Improvements**
    - Long Path Support: 2.x introduces support for paths exceeding 250 characters, significantly improving file system handling capabilities.


## Technology support

Please also see [.NET - Technical notes and limitations](../../../notes/) for additional information.

### Visual Studio / .NET Framework support

| Visual Studio version | .NET Framework version | .NET Core / .NET | .NET Standard | C# version | VB.NET version | Supported | Notes |
|---|---|---|---|---|---|:--:|---|
| 2003 | 1.1 |  |  | 1.2 | 7.1 | :white_check_mark: |  |
| 2005 | All of the above and 2.0, 3.0 |  |  | 2.0 | 8.0 | :white_check_mark: |  |
| 2008 | All of the above and 3.5 |  |  | 3.0 | 9.0 | :white_check_mark: |  |
| 2010 | All of the above and 4.0 |  |  | 4.0 | 10.0 | :white_check_mark: |  |
| 2012/2013 | All of the above and 4.5, 4.5.1, 4.5.2 |  |  | 5.0 | 12.0 | :white_check_mark: |  |
| 2015 | All of the above and 4.6 |  |  | 6.0 | 14.0 | :white_check_mark: |  |
| 2017 | All of the above and 4.6.1, 4.6.2, 4.7, 4.7.1, 4.7.2 | 1.0, 1.1, 2.0, 2.1, 2.2, 3.0, 3.1 | 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 2.0, 2.1 | 7.2 | 15.5 | :white_check_mark: |  |
| 2019 | All of the above and 4.8, 4.8.1 | All of the above and 5.0, 6.0 | Same as above | 8.0, 9.0,  10.0 (.NET 6.0 only) | 15.8 | :white_check_mark: | C# 10 is only compatible with .NET 6.0. |
| 2022 | Same as above | All of the above and 7.0 | Same as above | Same as above and 11.0 (.NET 7.0 only) | 15.8 | :white_check_mark: | C# 11 is only compatible with .NET 7.0. |

The following are implicitly supported:

-   ADO.NET
-   ASP.NET
-   ASP.NET Core
-   iFrame
-   Interop
-   WinForms
-   WPF 
-   LINQ
    -   LINQ to Objects
    -   LINQ to DataSets
    -   LINQ to SQL

## Function Point, Quality and Sizing support

| Function Points  (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :x: |

## Prerequisites

### Required third-party software on node

No third-party software is required. Any requirements are provided by the [com.castsoftware.dotnet.thirdparties](../../com.castsoftware.dotnet.thirdparties/) dependent extension.

### Source code delivery

-   Complete source code must be delivered for analysis after a DEBUG  type build.
-   The source code must include all the generated files which are essential for a successful analysis, i.e. all files with the extensions `.g.cs` (C#) or `.g.vb` (VB.Net).
-   Note that it is not necessary to deliver `.cs` or `.vb` files from the `obj` folder (files like `obj\Debug\net6.0\GlobalUsingDirectiveInCSharp.GlobalUsings.g.cs`).

### Location of the source code deployment folder

It is possible (and usually recommended) to configure the source code Deployment folder on a network share, however, in the case of .NET analyses, CAST recommends that the Deployment folder is located on the same server as the Node. If the Deployment folder is located on a remote network file share, there is a risk that assemblies may not be loaded correctly during the analysis, causing external objects to not be saved.

### Location of extensions folder on Node

If you have altered the location where extensions are stored from `%PROGRAMDATA%\CAST\CAST\Extensions` to a network location remote to the node (for example changing the `CAST_PLUGINS_ROOT_PATH` in the `CastGlobalSettings.ini` file) then you will need to add the following lines to the .NET Framework machine.config file (located on the node). This is to ensure that the .NET Analyzer DLLs located in the remote network folder can be successfully run from a network location. The analysis will fail without this configuration.

Replace the following tag:

``` xml
<runtime />
```

with:

``` xml
<runtime>
    <loadFromRemoteSources enabled="true"/>
</runtime>
```

## Dependent frameworks and third-party packages provided in the extension

Some dependent frameworks and third-party packages are provided in the extension and in [com.castsoftware.dotnet.thirdparties](../../com.castsoftware.dotnet.thirdparties/). There is therefore no need to specifically package these items if your source code relies on them - note however, that CAST Imaging Console will generate missing library/assembly alerts for these items - these alerts can safely be ignored.

### Frameworks

-   .NET Framework
-   .NET Core
-   .NET Standard

### Third-party packages

-   Microsoft.AspNetCore.All (ASP.NET)
-   Microsoft.AspNetCore.App (ASP.NET)
-   log4net
-   NHibernate
-   NPersistence
-   NLog
-   Newtonsoft.Json
-   Newtonsoft.Json.Bson
-   MySql.Data
-   Iesi.Collections
-   EntityFramework
-   Microsoft.Practices.EnterpriseLibrary.2008
-   elmah.corelibrary
-   System.Data.SqlClient
-   Serilog
-   Remotion.Linq

## Dependencies with other extensions

Some extensions require the presence of other extensions in order to function correctly. The .NET Analyzer extension requires that the following other extensions are also installed:

-   [com.castsoftware.wbslinker](../../../../multi/com.castsoftware.wbslinke/)
-   [com.castsoftware.dotnet.thirdparties](../../com.castsoftware.dotnet.thirdparties/) (only in Linux environment)

>Note that when using CAST Imaging Console to install the extension, any dependent extensions are automatically downloaded and installed for you. You do not need to do anything.

## Download and installation instructions

For .NET applications, the extension will be automatically installed by CAST Imaging:

![](../images/662143549.jpg)

## What results can you expect?

### Objects

| Icon                                                      | Name                         | Parent                                                  |
| --------------------------------------------------------- | ---------------------------- | ------------------------------------------------------- |
| ![](../images/662569233.png) ![](../images/662569224.png) | Project                      | -                                                       |
| ![](../images/662569234.png)                              | Application                  | Project                                                 |
| ![](../images/662569202.png)                              | Directory                    | Application                                             |
| ![](../images/662569199.png)                              | C![](../images/) Source File | Application, Directory                                  |
| ![](../images/662569231.png)                              | VB.NET Source File           | Application, Directory                                  |
| ![](../images/662569237.png)![](../images/662569236.png)  | ASA / Asax Source File       | Application, Directory                                  |
| ![](../images/662569194.png)                              | ASP Source File              | Application, Directory                                  |
| ![](../images/662569195.png)                              | Aspx Source File             | Application, Directory                                  |
| ![](../images/662569192.png)                              | Ascx Source File             | Application, Directory                                  |
| ![](../images/662569193.png)                              | Asmx Source File             | Application, Directory                                  |
| ![](../images/662569197.png)                              | Configuration Source File    | Application, Directory                                  |
| ![](../images/662569241.png)                              | Data Set Source File         | Application, Directory                                  |
| ![](../images/662569217.png)![](../images/662569216.png)  | Htc / Htm / Html Source File | Application, Directory                                  |
| ![](../images/662569220.png)                              | Java Script Source File      | Application, Directory                                  |
| ![](../images/662569242.png)                              | Master Source File           | Application, Directory                                  |
| ![](../images/662569243.png)                              | Settings Source File         | Application, Directory                                  |
| ![](../images/662569244.png)                              | Skin Source File             | Application, Directory                                  |
| ![](../images/662569246.png)                              | Xaml Source File             | Application, Directory                                  |
| ![](../images/662569245.png)                              | Visual Basic Script File     | Application, Directory                                  |
| ![](../images/662569211.png)                              | Class                        | Namespace, Interface, Class, Generic Class              |
| ![](../images/662569198.png)                              | Constant (field)             | Class, Generic Class                                    |
| ![](../images/662569201.png)                              | Delegate                     | Class, Generic Class                                    |
| ![](../images/662569204.png)                              | Enumeration                  | Class, Generic Class                                    |
| ![](../images/662569203.png)                              | Enumeration Item             | Enumeration                                             |
| ![](../images/662569206.png)                              | Event                        | Class, Generic Class                                    |
| ![](../images/662569205.png)                              | Event Add On                 | Event                                                   |
| ![](../images/662569205.png)                              | Event Remove On              | Event                                                   |
| ![](../images/662569212.png)                              | Interface                    | Namespace, Interface, Class, Generic Class              |
| ![](../images/662569208.png)                              | Field (non constant)         | Interface, Class, Generic Class                         |
| -                                                         | Finalizer                    | Class, Generic Class                                    |
| ![](../images/662569212.png)                              | Generic Class                | Namespace, Interface, Class, Generic Class              |
| ![](../images/662569213.png)                              | Generic Method               | Generic Class                                           |
| ![](../images/662569225.png)                              | Generic Property             | Generic Class                                           |
| ![](../images/662569218.png)                              | Indexer                      | Method, Generic Method                                  |
| ![](../images/662569218.png)                              | Indexer Getter               | Interface, Class, Generic Class                         |
| ![](../images/662569218.png)                              | Indexer Setter               | Interface, Class, Generic Class                         |
| ![](../images/662569213.png)                              | Method                       | Interface, Class, Generic Class                         |
| ![](../images/662569222.png)                              | Module (VB.NET only)         | -                                                       |
| ![](../images/662569223.png)                              | Namespace                    | Application, Namespace                                  |
| ![](../images/662569213.png)                              | Operator                     | Interface, Class, Generic Class                         |
| ![](../images/662569225.png)                              | Property                     | Interface, Class, Generic Class                         |
| ![](../images/662569226.png)                              | Property Getter              | Interface, Class, Generic Class                         |
| ![](../images/662569227.png)                              | Property Setter              | Interface, Class, Generic Class                         |
| ![](../images/662569215.png)                              | Structure                    | Interface, Class, Generic Class, Method, Generic Method |
| ![](../images/662569210.png)                              | Windows Forms                | Class                                                   |
| ![](../images/662569209.png)                              | Windows Forms User Control   | Class                                                   |

### Structural rules

The following rules are provided in the extension:

| Release | Link |
| ------- | ---- |
| 2.0.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|2.0.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C2.0.2-funcrel) |
| 2.0.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|2.0.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C2.0.1-funcrel) |
| 2.0.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|2.0.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C2.0.0-funcrel) |
| 2.0.0-beta4 | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|2.0.0-beta4](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C2.0.0-beta4) |
| 2.0.0-beta3 | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|2.0.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C2.0.0-beta3) |
| 2.0.0-beta2 | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|2.0.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C2.0.0-beta2) |
| 2.0.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|2.0.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C2.0.0-beta1) |

See also the following rules provided in the default CAST Assessment Model shipped with CAST Imaging Core:

| Technology | URL |
| ---------- | --- |
| C#         | [https://technologies.castsoftware.com/rules?sec=t_138383&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138383&ref=%7C%7C) |
| VB.NET     | [https://technologies.castsoftware.com/rules?sec=t_138635&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138635&ref=%7C%7C) |
| ASP.NET    | [https://technologies.castsoftware.com/rules?sec=t_138636&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138636&ref=%7C%7C) |

The following extensions also provide .NET specific rules:

-   [com.castsoftware.nosqldotnet](../../../../nosql/extensions/com.castsoftware.nosqldotnet/)
-   [com.castsoftware.dotnetweb](../../com.castsoftware.dotnetweb/)
-   [com.castsoftware.securityanalyzer](../../../../multi/com.castsoftware.securityanalyzer/)
-   [com.castsoftware.wcf](../../com.castsoftware.wcf/)
-   [com.castsoftware.securityanalyzer](../../../../multi/com.castsoftware.securityanalyzer/)
-   [com.castsoftware.sqlanalyzer](../../../../sql/extensions/com.castsoftware.sqlanalyzer/)
