---
title: "Blazor support"
linkTitle: "Blazor support"
type: "docs"
---

***

## Overview

[Blazor](https://learn.microsoft.com/en-us/aspnet/core/blazor/?view=aspnetcore-9.0) is partially supported via the **com.castsoftware.dotnet** extension. This section explains how this support is provided.

## Features

- Creates objects specific to Razor embedded in .razor files.
- Creates links between objects created in .razor files.
   
## Files analyzed

| Icon | File | Extension |
|---|---|---|
| ![](images/662569446.png) | .NET Razor | `*.razor` |
  
## Objects
The following objects are created:

| Icon | Description |
|---|---|
| ![](images/662569446.png)| Razor source file |
| ![](images/662569428.png)| Generated C# class |

The Blazor component class full name is created using the default convention, for example: `TestProject.Pages.Counter`:

- the project name
- a dot
- the folder path of the component
- the component name (component name must begin with an upper case character)

Except where the syntax `@namespace my.component.namespace` is used inside the razor file and in this case the full name will be `my.component.namespace.ComponentName`.

### Objects created inside @code{} block

Objects created inside `@code{}` block syntax, such as fields, properties and methods are created  for all C# classes in `.cs` files.

## Links

### Links from code inside @code{} block

Links from code inside` @code{}` block syntax such as method/getter/setter calls or field accesses are created for all C# class in `.cs` files.

### Injecting component field/property value in a tag content

The syntax `@member` will include the value of the underlying class member in the html content:

```html
<p role="status">Current count: @currentCount</p>
```

The analyzer will create an access read link from the Razor source file  `Counter.razor` to the field:

![](images/095254.png)

### \<Component> tag

You can include a Blazor component inside another component just by using a tag with the component name. For example a component named `Counter`:

```html
<Counter IncrementAmount="10" />
```

The analyzer will create an `accessExecLink` link from the razor file to both the constructor of the C# class `Counter` and to the setter of the `IncrementAmout` property. It will also create a `relyonLink` to the C# class `Counter`:

![](images/142549.png)

### _Imports.razor

A Blazor component will use all the namespaces referenced in the `_Imports.razor` files in its hierarchical tree. The analyzer will create an `includeLink` from the component file to the import file:

![](images/100405.png)

### @using namespace syntax

The syntax `@using My.Namespace` in a razor file will generate the code `using My.Namespace` in the C# file and include the namespace, for example:

```html
@using BlazorApp.Components
```

The analyzer will create a link from the razor file to the namespace object and add the bookmark on the razor file:

![](images/100927.png)

### Data binding using @bind syntax

The syntax `@bind="field"` will bind a field/property of the component to an input/outout of the page, for example:

```html
<input @bind="text" />
```

The analyzer will create `accessRead` and `accessWrite` links from the razor file to the field/property object and add the bookmark on the razor file:

![](images/141712.png)

### Events with @on{event} syntax

The syntax `@on{event}="MethodName"` or `@on{event}="() => {}"` will call a method or the lambda when the event is triggered (e.g.: `onclick`):

```html
<button @onclick="() => text = string.Empty">Clear</button>
```

```html
<input @onchange="InputChanged" />
```

The analyzer will create an `accessExecLink` from the razor file to the method object and add the bookmark on the razor file:

![](images/141559.png)

### Component inheritance with @inherits syntax

The syntax `@inherits ...` will add inheritance to a component:

```html
@inherits LayoutComponentBase
```

The analyzer will create a `InheritExtendLink` from the razor file to the class inherited (in the example `LayoutComponentBase`) and add the bookmark on the razor file:

![](images/142818.png)

### Interface implementation with @implements syntax

The syntax `@implements ...` will add an interface to a component:

```html
@implements IDisposable
```

The analyzer will create a `InheritImplementLink` from the razor file to the interface implemented (in the example `IDisposable`) and add the bookmark on the razor file:

![](images/143215.png)

### Component using attribute with @attribute \[NameAttribute] syntax

The syntax `@attribute […]` will add an attribute to a component:

```html
@attribute [StreamRendering]
```

The analyzer will create an `accessExecLink` from the razor file to the constructor of the attribute (in the example `StreamRendering`) and add the bookmark on the razor file:

![](images/143433.png)

### Routing with @page "/route/syntax/{parameter}" syntax

The syntax `@page "/route"` in a razor file will generate the code to add the `RouteAttribute` to the component in the C# file:

```html
@page "/counter"
```

The analyzer will create an `accessExecLink` from the razor file to the constructor of the attribute (in the example `StreamRendering`) and add the bookmark on the razor file:

![](images/144531.png)

## Not currently supported

- Templated components
- `EditForm` component
- `EditContext` component
- NavigationManager.NavigateTo (method to navigate to the specified URI) 
  
## Quality rules

None specific to Blazor.
