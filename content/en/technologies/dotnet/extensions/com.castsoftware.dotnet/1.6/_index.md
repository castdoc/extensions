---
title: ".NET Analyzer - 1.6"
linkTitle: "1.6"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet

## What's new?

See [Release Notes](rn).

## Description

This extension provides support for analyzing applications built with
.NET, .NET Standard and .NET Core technologies: objects and links
between these objects are identified and Automated Function Point values
are calculated. A set of .NET specific structural rules are also
available with the extension.

## Technology support

Please also see [.NET - Technical notes and limitations](../../../notes) for additional information.

## Visual Studio / .NET Framework support

| Visual Studio version | .NET Framework version | .NET Core / .NET | .NET Standard | C# version | VB.NET version | Supported | Notes |
|---|---|---|---|---|---|:---:|---|
| 2003 | 1.1 |  |  | 1.2 | 7.1 | :white_check_mark: |  |
| 2005 | All of the above and 2.0, 3.0 |  |  | 2.0 | 8.0 | :white_check_mark: |  |
| 2008 | All of the above and 3.5 |  |  | 3.0 | 9.0 | :white_check_mark: |  |
| 2010 | All of the above and 4.0 |  |  | 4.0 | 10.0 | :white_check_mark: |  |
| 2012/2013 | All of the above and 4.5, 4.5.1, 4.5.2 |  |  | 5.0 | 12.0 | :white_check_mark: |  |
| 2015 | All of the above and 4.6 |  |  | 6.0 | 14.0 | :white_check_mark: |  |
| 2017 | All of the above and 4.6.1, 4.6.2, 4.7, 4.7.1, 4.7.2 | 1.0, 1.1, 2.0, 2.1, 2.2, 3.0, 3.1 | 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 2.0, 2.1 | 7.2 | 15.5 | :white_check_mark: |  |
| 2019 | All of the above and 4.8, 4.8.1 | All of the above and 5.0, 6.0 | Same as above | 8.0, 9.0, 10.0 (.NET 6.0 only) | 15.8 | :white_check_mark: | C# 10 is only compatible with .NET 6.0. |
| 2022 | Same as above | All of the above an 7.0, 8.0 | Same as above | All of the above and 11.0, 12.0 (.NET 8.0 only) | 15.8 | :white_check_mark: | C# 12 is only compatible with .NET 8.0. |

The following are implicitly supported:

- ADO.NET
- ASP.NET
- ASP.NET Core
- iFrame
- Interop
- WinForms
- WPF 
- LINQ:
    - LINQ to Objects
    - LINQ to DataSets
    - LINQ to SQL
- First level of support for Xamarin (links to Xamarin API objects will be resolved) for:
    - Android
    - iOS
    - TvOS
    - WatchOS
    - UWP
- [Blazor](results/blazor/)

## Function Point, Quality and Sizing support

- Function Points: A green tick indicates that OMG Function Point
counting and Transaction Risk Index are supported.

- Quality and Sizing: A green tick indicates that CAST can measure
size and that a minimum set of Quality Rules exist.

| Function Points<br> (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Prerequisites

### Required third-party software on CAST Node

.NET Framework ≥ 4.7.2 must be installed on the CAST Node server in
order for the analysis to function. A check will be done when the
analysis starts and a message will produced if the minimum .NET
Framework cannot be found.

Note that a .NET Framework is installed via the CAST Core installer,
however this may not be the required version. Please check.

### Location of the source code deployment folder

It is possible (and usually recommended) to configure the source
code Deployment folder on a network share, however, in the case
of .NET analyses, CAST recommends that the Deployment folder is
located on the same server as the CAST Node. If the Deployment
folder is located on a remote network file share, there is a risk that
assemblies may not be loaded correctly during the analysis, causing
external objects to not be saved.

### Location of extensions folder on CAST Node

If you have altered the location where extensions are stored from
%PROGRAMDATA%\CAST\CAST\Extensions to a network location remote to the
CAST Node (for example changing the CAST_PLUGINS_ROOT_PATH in the
CastGlobalSettings.ini file) then you will need to add the following
lines to the .NET Framework machine.config file (located on CAST
Node). This is to ensure that the .NET Analyzer DLLs located in the
remote network folder can be successfully run from a network location.
The analysis will fail without this configuration.

Replace the following tag:

``` xml
<runtime />
```

with:

``` xml
<runtime>
    <loadFromRemoteSources enabled="true"/>
</runtime>
```

## Dependent frameworks and third-party packages provided in the extension

Some dependent frameworks and third-party packages are provided in the
extension itself. There is therefore no need to specifically package
these items if your source code relies on them - note however, that the
AIP COnsole will generate missing library/assembly alerts for these
items - these alerts can safely be ignored.

### Frameworks

-   .NET framework
-   .NET Core
-   .NET standard
-   Silverlight
-   Universal Windows Platform
-   Mono Android
-   Xamarin iOS
-   Xamarin TvOS
-   Xamarin WatchOS

### Third-party packages

-   Microsoft.AspNetCore.All (ASP.NET)
-   Microsoft.AspNetCore.App (ASP.NET)
-   log4net
-   NHibernate
-   NPersistence
-   NLog
-   Newtonsoft.Json
-   Newtonsoft.Json.Bson
-   MySql.Data
-   Iesi.Collections
-   EntityFramework
-   Microsoft.Practices.EnterpriseLibrary.2008
-   elmah.corelibrary
-   System.Data.SqlClient
-   Serilog
-   Remotion.Linq
-   Xamarin.Android.Support.v7.AppCompat
-   Xamarin.Android.Support.v7.CardView
-   Xamarin.Android.Support.v7.MediaRouter
-   Xamarin.Android.Support.v4
-   Xamarin.Android.Support.Media.Compat
-   Xamarin.Android.Support.Fragment
-   Xamarin.Android.Support.Design
-   Xamarin.Android.Support.Core.Utils
-   Xamarin.Android.Support.Core.UI
-   Xamarin.Android.Support.Wear
-   Xamarin.Android.Wear
-   Xamarin.Forms

## Dependencies with other extensions

Some extensions require the presence of other extensions in order to
function correctly. The.NET Analyzer extension requires that the
following other extensions are also installed:

-  [ com.castsoftware.wbslinker](../../../../multi/com.castsoftware.wbslinker/)

Note that when using CAST Console to install the extension, any
dependent extensions are automatically downloaded and installed for you.
You do not need to do anything.

## Download and installation instructions

For .NET applications, the extension will be automatically installed
by CAST Console:

![](../images/662569451.jpg)

## What results can you expect?

### Objects

| Icon | Name | Parent |
|---|---|---|
| ![](../images/662569450.png) ![](../images/662569449.png) | Project | - |
| ![](../images/662569448.png) | Application | Project |
| ![](../images/662569447.png) | Directory | Application |
| ![](../images/662569446.png) | C# Source File | Application, Directory |
| ![](../images/662569445.png) | VB.NET Source File | Application, Directory |
| ![](../images/662569444.png) ![](../images/662569443.png) | ASA / Asax Source File | Application, Directory |
| ![](../images/662569442.png) | ASP Source File | Application, Directory |
| ![](../images/662569441.png) | Aspx Source File | Application, Directory |
| ![](../images/662569440.png) | Ascx Source File | Application, Directory |
| ![](../images/662569439.png) | Asmx Source File | Application, Directory |
| ![](../images/662569438.png) | Configuration Source File | Application, Directory |
| ![](../images/662569437.png) | Data Set Source File | Application, Directory |
| ![](../images/662569436.png) ![](../images/662569435.png) |  Htc / Htm / Html Source File | Application, Directory |
| ![](../images/662569434.png) | Java Script Source File | Application, Directory |
| ![](../images/662569433.png) | Master Source File | Application, Directory |
| ![](../images/662569446.png) | Razor Source File | Application, Directory |
| ![](../images/662569432.png) | Settings Source File | Application, Directory |
| ![](../images/662569431.png) | Skin Source File | Application, Directory |
| ![](../images/662569430.png) | Xaml Source File | Application, Directory |
| ![](../images/662569429.png) | Visual Basic Script File | Application, Directory |
| ![](../images/662569428.png) | Class | Namespace, Interface, Class, Generic Class |
| ![](../images/662569427.png) | Constant (field) | Class, Generic Class |
| ![](../images/662569426.png) | Delegate | Class, Generic Class |
| ![](../images/662569425.png) | Enumeration | Class, Generic Class |
| ![](../images/662569424.png) | Enumeration Item | Enumeration |
| ![](../images/662569423.png) | Event | Class, Generic Class |
| ![](../images/662569422.png) | Event Add On | Event |
| ![](../images/662569422.png) | Event Remove On | Event |
| ![](../images/662569421.png) | Interface | Namespace, Interface, Class, Generic Class |
| ![](../images/662569420.png) | Field (non constant) | Interface, Class, Generic Class |
| - | Finalizer | Class, Generic Class |
| ![](../images/662569421.png) | Generic Class | Namespace, Interface, Class, Generic Class |
| ![](../images/662569419.png) | Generic Method | Generic Class |
| ![](../images/662569418.png) | Generic Property | Generic Class |
| ![](../images/662569417.png) | Indexer | Method, Generic Method |
| ![](../images/662569417.png) | Indexer Getter | Interface, Class, Generic Class |
| ![](../images/662569417.png) | Indexer Setter | Interface, Class, Generic Class |
| ![](../images/662569419.png) | Method | Interface, Class, Generic Class |
| ![](../images/662569416.png) | Module (VB.NET only) | - |
| ![](../images/662569415.png) | Namespace | Application, Namespace |
| ![](../images/662569419.png) | Operator | Interface, Class, Generic Class |
| ![](../images/662569418.png) | Property | Interface, Class, Generic Class |
| ![](../images/662569414.png) | Property Getter | Interface, Class, Generic Class |
| ![](../images/662569413.png) | Property Setter | Interface, Class, Generic Class |
| ![](../images/662569412.png) | Structure | Interface, Class, Generic Class, Method, Generic Method |
| ![](../images/662569411.png) | Windows Forms | Class |
| ![](../images/662569410.png) | Windows Forms User Control | Class |

## Structural rules

| Release | URL |
|---|---|
| 1.6.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.6.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.6.3-funcrel) |
| 1.6.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.6.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.6.2-funcrel) |
| 1.6.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.6.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.6.1-funcrel) |
| 1.6.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.6.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.6.0-funcrel) |

See also the following rules provided in the default CAST Assessment Model shipped with CAST AIP Core:

| Technology | URL |
|---|---|
| C# | [https://technologies.castsoftware.com/rules?sec=t_138383&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138383&ref=%7C%7C) |
| VB.NET | [https://technologies.castsoftware.com/rules?sec=t_138635&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138635&ref=%7C%7C) |
| ASP.NET | [https://technologies.castsoftware.com/rules?sec=t_138636&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138636&ref=%7C%7C) |

The following extensions also provide .NET specific rules:

-   [com.castsoftware.nosqldotnet](../../com.castsoftware.nosqldotnet/)
-   [com.castsoftware.dotnetweb](../../com.castsoftware.dotnetweb/)
-   [com.castsoftware.wcf](../../com.castsoftware.wcf/)
-   [com.castsoftware.securityanalyzer](../../../../multi/com.castsoftware.securityanalyzer/)
-   [com.castsoftware.sqlanalyzer](../../../../sql/extensions/com.castsoftware.sqlanalyzer/)
