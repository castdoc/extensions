---
title: ".NET Analyzer - 1.5"
linkTitle: "1.5"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dotnet

## What's new?

See [.NET Analyzer - 1.5 - Release
Notes](rn).

## Description

This extension provides support for analyzing applications built with
.NET, .NET Standard and .NET Core technologies: objects and links
between these objects are identified and Automated Function Point values
are calculated. A set of .NET specific structural rules are also
available with the extension.

## Technology support

Please also see [.NET - Technical notes and
limitations](../../../notes) for additional
information.

## Visual Studio / .NET Framework support

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Visual Studio version</th>
<th class="confluenceTh">.NET Framework version</th>
<th class="confluenceTh">.NET Core / .NET</th>
<th class="confluenceTh">.NET Standard</th>
<th class="confluenceTh">C# version</th>
<th class="confluenceTh">VB.NET version</th>
<th class="confluenceTh">Supported</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>2003</p></td>
<td class="confluenceTd"><p>1.1</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: center;">1.2</td>
<td class="confluenceTd" style="text-align: center;">7.1</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>2005</p></td>
<td class="confluenceTd"><p>2.0, 3.0</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: center;">2.0</td>
<td class="confluenceTd" style="text-align: center;">8.0</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>2008</p></td>
<td class="confluenceTd"><p>2.0, 3.0, 3.5</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: center;">3.0</td>
<td class="confluenceTd" style="text-align: center;">9.0</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>2010</p></td>
<td class="confluenceTd"><p>2.0, 3.0, 3.5, 4.0</p></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: center;">4.0</td>
<td class="confluenceTd" style="text-align: center;">10.0</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">2012/2013</td>
<td class="confluenceTd">2.0, 3.0, 3.5, 4.0, 4.5, 4.5.1, 4.5.2</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: center;">5.0</td>
<td class="confluenceTd" style="text-align: center;">12.0</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd">2015</td>
<td class="confluenceTd">2.0, 3.0, 3.5, 4.0, 4.5, 4.5.1, 4.5.2, 4.6</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd" style="text-align: center;">6.0</td>
<td class="confluenceTd" style="text-align: center;">14.0</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">2017</td>
<td class="confluenceTd">2.0, 3.0, 3.5, 4.0, 4.5, 4.5.1, 4.5.2, 4.6,
4.6.1, 4.6.2, 4.7, 4.7.1, 4.7.2</td>
<td class="confluenceTd"><p>1.0, 1.1, 2.0, 2.1, 2.2, 3.0, 3.1</p></td>
<td class="confluenceTd">1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 2.0,
2.1</td>
<td class="confluenceTd" style="text-align: center;">7.2</td>
<td class="confluenceTd" style="text-align: center;">15.5</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd">2019</td>
<td class="confluenceTd">2.0, 3.0, 3.5, 4.0, 4.5, 4.5.1, 4.5.2, 4.6,
4.6.1, 4.6.2, 4.7, 4.7.1, 4.7.2</td>
<td class="confluenceTd">1.0, 1.1, 2.0, 2.1, 2.2, 3.0, 3.1, 5.0,
6.0</td>
<td class="confluenceTd">1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 2.0,
2.1</td>
<td class="confluenceTd" style="text-align: center;">8.0 / 9.0 /
10.0</td>
<td class="confluenceTd" style="text-align: center;">15.8</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">2022</td>
<td class="confluenceTd">2.0, 3.0, 3.5, 4.0, 4.5, 4.5.1, 4.5.2, 4.6,
4.6.1, 4.6.2, 4.7, 4.7.1, 4.7.2</td>
<td class="confluenceTd">1.0, 1.1, 2.0, 2.1, 2.2, 3.0, 3.1, 5.0, 6.0,
7.0</td>
<td class="confluenceTd">1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 2.0,
2.1</td>
<td class="confluenceTd" style="text-align: center;">8.0 / 9.0 / 10.0 /
11.0</td>
<td class="confluenceTd" style="text-align: center;">15.8</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
</tbody>
</table>

The following are implicitly supported:

- ADO.NET
- ASP.NET
- ASP.NET Core
- iFrame
- Interop
- WinForms
- WPF 
- LINQ:
    - LINQ to Objects
    - LINQ to DataSets
    - LINQ to SQL
- First level of support for Xamarin (links to Xamarin API objects will be resolved) for:
    - Android
    - iOS
    - TvOS
    - WatchOS
    - UWP

## Function Point, Quality and Sizing support

- Function Points: A green tick indicates that OMG Function Point
counting and Transaction Risk Index are supported.

- Quality and Sizing: A green tick indicates that CAST can measure
size and that a minimum set of Quality Rules exist.

| Function Points<br> (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Prerequisites

### Required third-party software on CAST Node

.NET Framework ≥ 4.7.2 must be installed on the CAST Node server in
order for the analysis to function. A check will be done when the
analysis starts and a message will produced if the minimum .NET
Framework cannot be found.

Note that a .NET Framework is installed via the CAST Core installer,
however this may not be the required version. Please check.

### Location of the source code deployment folder

It is possible (and usually recommended) to configure the source
code Deployment folder on a network share, however, in the case
of .NET analyses, CAST recommends that the Deployment folder is
located on the same server as the CAST Node. If the Deployment
folder is located on a remote network file share, there is a risk that
assemblies may not be loaded correctly during the analysis, causing
external objects to not be saved.

### Location of extensions folder on CAST Node

If you have altered the location where extensions are stored from
%PROGRAMDATA%\CAST\CAST\Extensions to a network location remote to the
CAST Node (for example changing the CAST_PLUGINS_ROOT_PATH in the
CastGlobalSettings.ini file) then you will need to add the following
lines to the .NET Framework machine.config file (located on CAST
Node). This is to ensure that the .NET Analyzer DLLs located in the
remote network folder can be successfully run from a network location.
The analysis will fail without this configuration.

Replace the following tag:

``` xml
<runtime />
```

with:

``` xml
<runtime>
    <loadFromRemoteSources enabled="true"/>
</runtime>
```

## Dependent frameworks and third-party packages provided in the extension

Some dependent frameworks and third-party packages are provided in the
extension itself. There is therefore no need to specifically package
these items if your source code relies on them - note however, that the
AIP COnsole will generate missing library/assembly alerts for these
items - these alerts can safely be ignored.

### Frameworks

-   .NET framework
-   .NET Core
-   .NET standard
-   Silverlight
-   Universal Windows Platform
-   Mono Android
-   Xamarin iOS
-   Xamarin TvOS
-   Xamarin WatchOS

### Third-party packages

-   Microsoft.AspNetCore.All (ASP.NET)
-   Microsoft.AspNetCore.App (ASP.NET)
-   log4net
-   NHibernate
-   NPersistence
-   NLog
-   Newtonsoft.Json
-   Newtonsoft.Json.Bson
-   MySql.Data
-   Iesi.Collections
-   EntityFramework
-   Microsoft.Practices.EnterpriseLibrary.2008
-   elmah.corelibrary
-   System.Data.SqlClient
-   Serilog
-   Remotion.Linq
-   Xamarin.Android.Support.v7.AppCompat
-   Xamarin.Android.Support.v7.CardView
-   Xamarin.Android.Support.v7.MediaRouter
-   Xamarin.Android.Support.v4
-   Xamarin.Android.Support.Media.Compat
-   Xamarin.Android.Support.Fragment
-   Xamarin.Android.Support.Design
-   Xamarin.Android.Support.Core.Utils
-   Xamarin.Android.Support.Core.UI
-   Xamarin.Android.Support.Wear
-   Xamarin.Android.Wear
-   Xamarin.Forms

## Dependencies with other extensions

Some extensions require the presence of other extensions in order to
function correctly. The.NET Analyzer extension requires that the
following other extensions are also installed:

-  [ com.castsoftware.wbslinker](../../../../multi/com.castsoftware.wbslinker/)

Note that when using CAST Console to install the extension, any
dependent extensions are automatically downloaded and installed for you.
You do not need to do anything.

## Download and installation instructions

For .NET applications, the extension will be automatically installed
by CAST Console:

![](../images/662569451.jpg)

## What results can you expect?

### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: center;">Icon</th>
<th class="confluenceTh">Name</th>
<th class="confluenceTh">Parent</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569450.png" draggable="false"
data-image-src="../images/662569450.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569450"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_ApplicationVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /><img
src="../images/662569449.png" draggable="false"
data-image-src="../images/662569449.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569449"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Project32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Project</p></td>
<td class="confluenceTd"><p>-</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569448.png" draggable="false"
data-image-src="../images/662569448.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569448"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_ApplicationVBScriptClientSide32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Application</p></td>
<td class="confluenceTd"><p>Project</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569447.png" draggable="false"
data-image-src="../images/662569447.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569447"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Directory32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Directory</p></td>
<td class="confluenceTd"><p>Application</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569446.png" draggable="false"
data-image-src="../images/662569446.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569446"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_CSharpFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">C# Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569445.png" draggable="false"
data-image-src="../images/662569445.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569445"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_VBScriptFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">VB.NET Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569444.png" draggable="false"
data-image-src="../images/662569444.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569444"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_AsaxFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /><img
src="../images/662569443.png" draggable="false"
data-image-src="../images/662569443.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569443"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_AsaFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ASA  / Asax Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569442.png" draggable="false"
data-image-src="../images/662569442.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569442"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_ASP_Subset32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">ASP Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569441.png" draggable="false"
data-image-src="../images/662569441.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569441"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_AspxFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Aspx Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569440.png" draggable="false"
data-image-src="../images/662569440.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569440"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_AscxFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Ascx Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569439.png" draggable="false"
data-image-src="../images/662569439.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569439"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_AsmxFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Asmx Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569438.png" draggable="false"
data-image-src="../images/662569438.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569438"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_ConfigurationFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Configuration Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569437.png" draggable="false"
data-image-src="../images/662569437.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569437"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_DataSetFile.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Data Set Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569436.png" draggable="false"
data-image-src="../images/662569436.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569436"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_HtmFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /><img
src="../images/662569435.png" draggable="false"
data-image-src="../images/662569435.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569435"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_HtcFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Htc / Htm / Html Source File</p>
</div></td>
<td class="confluenceTd"><p>Application, Directory</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569434.png" draggable="false"
data-image-src="../images/662569434.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569434"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_JavaScriptFile32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Java Script Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569433.png" draggable="false"
data-image-src="../images/662569433.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569433"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_MasterFile.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Master Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569432.png" draggable="false"
data-image-src="../images/662569432.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569432"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_SettingsFile.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Settings Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569431.png" draggable="false"
data-image-src="../images/662569431.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569431"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_SkinFile.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Skin Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569430.png" draggable="false"
data-image-src="../images/662569430.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569430"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_XamlFile.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Xaml Source File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569429.png" draggable="false"
data-image-src="../images/662569429.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569429"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_VBScriptFile.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Visual Basic Script File</td>
<td class="confluenceTd">Application, Directory</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569428.png" draggable="false"
data-image-src="../images/662569428.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569428"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_GenericClassCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Class</p></td>
<td class="confluenceTd"><p>Namespace, Interface, Class, Generic
Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569427.png" draggable="false"
data-image-src="../images/662569427.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569427"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_ConstantCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Constant (field)</p></td>
<td class="confluenceTd"><p>Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569426.png" draggable="false"
data-image-src="../images/662569426.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569426"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_DelegateVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Delegate</p></td>
<td class="confluenceTd"><p>Class, Generic Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569425.png" draggable="false"
data-image-src="../images/662569425.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569425"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_EnumerationVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Enumeration</p></td>
<td class="confluenceTd"><p>Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569424.png" draggable="false"
data-image-src="../images/662569424.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569424"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_EnumerationItemVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Enumeration Item</p></td>
<td class="confluenceTd"><p>Enumeration</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569423.png" draggable="false"
data-image-src="../images/662569423.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569423"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_EventVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Event</p></td>
<td class="confluenceTd"><p>Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569422.png" draggable="false"
data-image-src="../images/662569422.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569422"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_EventAddonCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Event Add On</p></td>
<td class="confluenceTd"><p>Event</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569422.png" draggable="false"
data-image-src="../images/662569422.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569422"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_EventAddonCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Event Remove On</p></td>
<td class="confluenceTd"><p>Event</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569421.png" draggable="false"
data-image-src="../images/662569421.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569421"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_GenericInterfaceCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Interface</p></td>
<td class="confluenceTd"><p>Namespace, Interface, Class, Generic
Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569420.png" draggable="false"
data-image-src="../images/662569420.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569420"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_FieldVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Field (non constant)</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;">-</td>
<td class="confluenceTd"><p>Finalizer</p></td>
<td class="confluenceTd"><p>Class, Generic Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569421.png" draggable="false"
data-image-src="../images/662569421.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569421"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_GenericInterfaceCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Generic Class</p></td>
<td class="confluenceTd"><p>Namespace, Interface, Class, Generic
Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569419.png" draggable="false"
data-image-src="../images/662569419.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569419"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_GenericMethodCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Generic Method</p></td>
<td class="confluenceTd"><p>Generic Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569418.png" draggable="false"
data-image-src="../images/662569418.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569418"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_PropertyCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Generic Property</p></td>
<td class="confluenceTd"><p>Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569417.png" draggable="false"
data-image-src="../images/662569417.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569417"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_IndexerGetterVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" width="16" /></p>
</div></td>
<td class="confluenceTd"><p>Indexer</p></td>
<td class="confluenceTd"><p>Method, Generic Method</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569417.png" draggable="false"
data-image-src="../images/662569417.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569417"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_IndexerGetterVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" width="16" /></p>
</div></td>
<td class="confluenceTd"><p>Indexer Getter</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569417.png" draggable="false"
data-image-src="../images/662569417.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569417"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_IndexerGetterVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" width="16" /></p>
</div></td>
<td class="confluenceTd"><p>Indexer Setter</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569419.png" draggable="false"
data-image-src="../images/662569419.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569419"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_GenericMethodCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Method</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569416.png" draggable="false"
data-image-src="../images/662569416.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569416"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_ModuleVB32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Module (VB.NET only)</td>
<td class="confluenceTd">-</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569415.png" draggable="false"
data-image-src="../images/662569415.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569415"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_NamespaceDotNet32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Namespace</p></td>
<td class="confluenceTd"><p>Application, Namespace</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569419.png" draggable="false"
data-image-src="../images/662569419.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569419"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_GenericMethodCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Operator</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569418.png" draggable="false"
data-image-src="../images/662569418.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569418"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_PropertyCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Property</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569414.png" draggable="false"
data-image-src="../images/662569414.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569414"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_PropertyGetterCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Property Getter</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569413.png" draggable="false"
data-image-src="../images/662569413.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569413"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_PropertySetterCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Property Setter</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569412.png" draggable="false"
data-image-src="../images/662569412.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569412"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_GenericStructureCSharp32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd"><p>Structure</p></td>
<td class="confluenceTd"><p>Interface, Class, Generic Class, Method,
Generic Method</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569411.png" draggable="false"
data-image-src="../images/662569411.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569411"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_Form32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Windows Forms</td>
<td class="confluenceTd">Class</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/662569410.png" draggable="false"
data-image-src="../images/662569410.png"
data-unresolved-comment-count="0" data-linked-resource-id="662569410"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_DotNet_FieldVBCustomControl32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="612860021"
data-linked-resource-container-version="1" height="32" /></p>
</div></td>
<td class="confluenceTd">Windows Forms User Control</td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Class</p>
</div></td>
</tr>
</tbody>
</table>

## Structural rules

| Release       | URL                                                                                                                                                                    |
| ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1.5.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.5-funcrel) |
| 1.5.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.4-funcrel) |
| 1.5.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.3-funcrel) |
| 1.5.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.2-funcrel) |
| 1.5.1-funcrel | Extension withdrawn.                                                                                                                                                   |
| 1.5.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.0-funcrel) |
| 1.5.0-beta4   | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.0-beta4](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.0-beta4)     |
| 1.5.0-beta3   | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.0-beta3)     |
| 1.5.0-beta2   | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.0-beta2)     |
| 1.5.0-beta1   | [https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=\|\|1.5.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_dotnet&ref=%7C%7C1.5.0-beta1)     |


See also the following rules provided in the default CAST Assessment
Model shipped with CAST Imaging Core:

| Technology | URL                                                                                                                                      |
| ---------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| C#         | [https://technologies.castsoftware.com/rules?sec=t_138383&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138383&ref=%7C%7C) |
| VB.NET     | [https://technologies.castsoftware.com/rules?sec=t_138635&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138635&ref=%7C%7C) |
| ASP.NET    | [https://technologies.castsoftware.com/rules?sec=t_138636&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_138636&ref=%7C%7C) |

The following extensions also provide .NET specific rules:

-   [com.castsoftware.nosqldotnet](../../com.castsoftware.nosqldotnet/)
-   [com.castsoftware.dotnetweb](../../com.castsoftware.dotnetweb/)
-   [com.castsoftware.wcf](../../com.castsoftware.wcf/)
-   [com.castsoftware.securityanalyzer](../../../../multi/com.castsoftware.securityanalyzer/)
-   [com.castsoftware.sqlanalyzer](../../../../sql/extensions/com.castsoftware.sqlanalyzer/)
