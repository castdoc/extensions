---
title: "Prepare and deliver the source code"
linkTitle: "Prepare and deliver the source code"
type: "docs"
weight: 4
---

## Information about discovery

Discovery is a process that is actioned during the packaging process
whereby CAST will attempt to automatically identify projects within your
application using a set of predefined rules. This discovery process also
allows CAST AIP to set the initial analysis configuration settings
explained in .NET - Analysis configuration. Discoverers are
currently embedded in CAST AIP:

-   [Microsoft Visual Studio .NET
    Discoverer](../extensions/com.castsoftware.dmtdotnetdiscoverer/)

You should read the relevant documentation for each discoverer (provided
in the link above) to understand how the source code will be handled.

## Source code delivery using CAST Console


### Prepare the application source code

AIP Console expects the application source code to be delivered either
via a ZIP file or via a source code folder configured
in AIP Console. Whichever option you chose, you should include in the
ZIP/source code folder all of your .NET application source code. CAST
highly recommends placing all the relevant files in a folder and using
sub-folders where necessary. You can deliver other technologies at the
same time (for example, database DDL). If you are using a ZIP/archive
file, zip the folders in the "temp" folder as shown in the image below -
but do not zip the "temp" folder itself, nor create any intermediary
folders:

``` java
D:\temp
    |-----DotNET
    |-----OtherTechno1
    |-----OtherTechno2
```

Any additional framework specific source code (such as Entity
Framework, Silverlight Framework, WCF, WPF,
NoSQL etc.) should also be provided in the ZIP/archive file or the source
code folder.

### What about delivering framework/external/custom assemblies?

The .NET Analyzer needs to know the location of any assemblies such
as the .NET Framework assemblies, external assemblies (third-party
DLLs) and other custom assemblies that are used by your
application source code. There are various ways to declare the location
of these assemblies when using AIP Console, but the action you choose
also depends on the release of the .NET Analyzer you are using. This is
explained in more detail below:

#### .NET Framework assemblies

.NET Framework assemblies are used by all .NET applications and
therefore the .NET Analyzer needs to have access to these assemblies in
order to resolve references correctly. Here there is a choice of
options:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Using .NET Analyzer ≥ 1.1</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>When using .NET Analyzer <strong>≥ 1.1</strong>, the .NET Framework
assemblies are provided in the extension itself (as listed in the
section <strong>Dependent frameworks and third-party packages provided
in the extension</strong> in the <strong><a
href="../extensions/com.castsoftware.dotnet/">extension documentation</a></strong>) and they will
be used to resolve references to specific assemblies that have been used
by the Application source code. Therefore it is not necessary to declare
the location of these .NET Framework assemblies.</p>
<p><strong>.NET Analyzer ≥ 1.4.3</strong></p>
<p>In these releases, a file called <strong>ressources.json</strong> is
provided in the root of the extension which lists all .NET Framework
assemblies shipped with the extension. This file is taken into account
during the source code delivery process and any  <strong>"missing
references" type</strong> alerts related to .NET Framework assemblies
shipped with the extension will no longer be displayed in AIP
Console.</p>
<p><strong>.NET Analyzer 1.1.0 - 1.4.2</strong></p>
<p>In these releases, <strong>"missing references" type alerts</strong>
will be generated during the source code delivery process for any .NET
Framework assemblies used by your application that are delivered in the
extension, for example as shown below. These alerts can be
<strong>safely ignored</strong>. The resulting analysis will use the
.NET Framework assemblies delivered in the .NET Analyzer extension.</p>
<p><em>Click to enlarge</em></p>
<p><img src="670630527.jpg" draggable="false"
data-image-src="670630527.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="670630527"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="alerts.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="670630522"
data-linked-resource-container-version="1" width="400"
height="277" /></p>
<p>If you prefer not to have missing references type alerts, then you
can, optionally, declare the location of the .NET Framework assemblies
using the aip-node-app.properties file on the AIP Node responsible for
analyzing your application, ensuring that the AIP Node can access the
declared location (i.e. a folder on the AIP Node or a shared network
location). These assemblies will be used in priority over the .NET
Framework assemblies provided in the extension itself. </p>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">Using .NET Analyzer 1.0</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>When using .NET Analyzer<strong> 1.0</strong>, the .NET Framework
assemblies are NOT provided in the extension, therefore you MUST
declare the location of these .NET Framework assemblies using
the aip-node-app.properties file, ensuring that the AIP Node responsible
for analyzing your application can access the declared location (i.e. a
folder on the AIP Node or a shared network location). If you do not
declare them, then the analysis will fail with a missing mandatory
reference error:</p>
</div></td>
</tr>
</tbody>
</table>

#### External assemblies (third-party DLLs)/custom assemblies

When using .NET Analyzer ≥ 1.1

CAST provides some specific frameworks and third party packages in the
extension itself which will automatically be used. Therefore if your
source code uses these specific frameworks and third party packages, it
is not necessary to deliver these items. However, missing
library/assembly alerts for these items will be generated during the
delivery. The alerts can be safely ignored if the alert references an
item that CAST provides in the extension. See the section Dependent
frameworks and third-party packages provided in the extension
[documentation](../extensions/com.castsoftware.dotnet/) for more information.

If your application uses external assemblies provided by third parties
or your own custom assemblies, the .NET Analyzer also needs to know the
location of them:

-   if these assemblies are stored in the correct location as
    specified in the project definition file and are delivered with
    the application source code (in the ZIP file or via a source code folder),
    then the .NET Analyzer will find them during the analysis without
    you needing to do anything.

If there are projects that use "HintPath" to reference assemblies
compiled from other projects within the application, then it is advised
to also deliver them in the correct location (i.e. as specified in the
project definition file). They will be used where circular references of
assemblies exist (e.g: project A depends on B.dll and project B depends
on A.dll).

-   if these assemblies are not stored in the correct location, then
    there are two options available to you to ensure that the .NET
    Analyzer is aware of their location:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Declare the location to AIP Console</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This option involves:</p>
<ul>
<li>placing the assemblies in a folder that the AIP Node responsible for
analyzing your application can access - a shared network folder is
preferred.</li>
<li>then <strong>declaring</strong> this folder using either:</li>
</ul>
<p>- the <strong>AIP Console UI</strong> (available only in <strong>AIP
Console ≥ 1.26</strong>, see <strong><a
href="https://doc.castsoftware.com/display/AIPCONSOLE/Administration+Center+-+Settings+-+.NET+Assemblies">Administration
Center - Settings - .NET Assemblies</a></strong>) - this is valid for
all AIP Nodes:</p>
<p><img src="670630525.jpg" draggable="false"
data-image-src="670630525.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="670630525"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="assembly_locations.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="670630522"
data-linked-resource-container-version="1" width="478"
height="250" /></p>
<p>- or using the  aip-node-app.properties file (located in
%PROGRAMDATA%\CAST\AipConsole\AipNode\aip-node-app.properties) on the
AIP Node (path must use single forward slashes or double back slashes -
the single back slash is not valid and multiple paths must be separated
by semi-colons):</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a>#scanner<span class="op">.</span><span class="fu">dotnet</span><span class="op">.</span><span class="fu">assembly</span><span class="op">.</span><span class="fu">locations</span><span class="op">=</span>C<span class="op">:/</span>dotNet<span class="op">/</span>v4<span class="fl">.0</span><span class="op">;</span>C<span class="op">:/</span>dotNet<span class="op">/</span>v4<span class="fl">.5</span><span class="op">;</span>C<span class="op">:/</span>dotNet<span class="op">/</span>v3<span class="fl">.5</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a>scanner<span class="op">.</span><span class="fu">dotnet</span><span class="op">.</span><span class="fu">assembly</span><span class="op">.</span><span class="fu">locations</span><span class="op">=</span></span></code></pre></div>
</div>
</div>
<p>When the analysis is run, the .NET Analyzer will search the folders
you declared and find the assemblies required by your application source
code.</p>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">Deliver with the application source code</th>
<td class="confluenceTd"><div class="content-wrapper">
<div>
<div>
<p>This option has been removed in <strong>AIP
Console ≥ 1.26</strong>.</p>
</div>
</div>
<p>This option involves:</p>
<ul>
<li>delivering the assemblies in a dedicated folder (typically this is a
sub folder or the "bin" folder) together with the application source
code (i.e. in the <strong>ZIP file</strong> or via a <strong><a
href="https://doc.castsoftware.com/display/AIPCONSOLE/Administration+Center+-+Settings+-+Source+Folder+Location">source
code located in a folder</a></strong>).</li>
<li>ensuring that
the <code>scanner.detect.dotnet.assemblies </code>option is set to
<strong>true</strong> the aip-node-app.properties file on the AIP
Node.</li>
</ul>
<p>When the analysis is run, the .NET Analyzer will attempt to find
the assemblies required by your application source code.</p>
<div>
<div>
This option is <strong>limited in scope</strong> however and if in
doubt, you should use the alternative option described above
(<strong>Declare the location in aip-node-app.properties</strong>): only
one folder in the delivered source code will be detected by the .NET
Analyzer - the folder containing the largest number of assemblies (dll
files). Therefore if your assemblies are distributed in multiple folders
throughout the source code, this method is not recommended.
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

##### Default .NET Assemblies package created on source code root in AIP Console ≥ 1.26

In AIP Console ≥ 1.26 a change has been made: by default, a .NET
Assemblies package will always be automatically created on the
application source code root folder. This is to ensure that any
assemblies delivered with the application source code (in the ZIP
file or via a [source code located in a
folder](https://doc.castsoftware.com/display/AIPCONSOLE/Administration+Center+-+Settings+-+Source+Folder+Location))
but not stored in the correct location as specified in the project
definition file, are always taken into account - a kind of fail safe
mechanism. This mechanism is governed by the following option in
the the  aip-node-app.properties file (located in
%PROGRAMDATA%\CAST\AipConsole\AipNode\aip-node-app.properties):

``` java
## Option is defaulted to true
scanner.dotnet.assembly.create.from.root=true
```

If you do not want the default .NET Assemblies package to be created,
you can change the option to false (restart the AIP Node so that the
changes are taken into account).

### What about Nuget package dependencies?

An extension ([NuGet Resources
Extractor](../extensions/com.castsoftware.dmtdotnetnugetresourcesextractor/)) has been published that provides the means to
configure an automatic extraction of NuGet package dependencies from
a NuGet repository specifically for .NET application source code. In
other words, NuGet package based source code that resides in a simple
local or [nuget.org](http://nuget.org/) location. For example, when your .NET application
contains .csproj files which have package references defined, you
can use this extractor to extract those NuGet packages from the NuGet
repository. 

Out of the box, if a .csproj file is detected in the delivered
source code, the extension will be downloaded and installed as part of
the analysis process. If the .csproj file contains package dependency
references, these references will automatically be accessed and
included in the analysis. Example package references shown below:

``` xml
<ItemGroup>
    <!-- ... -->
    <PackageReference Include="Contoso.Utility.UsefulStuff" Version="3.6.0" />
    <!-- ... -->
</ItemGroup>
```

The extractor will extract all NuGet package dependencies and place them
inside a folder called "nugetPck" folder located in the Deploy
folder. The extractor is driven by
the `%PROGRAMDATA%\CAST\AipConsole\AipNode\aip-node-app.properties` file
attribute `scanner.nuget.repository`:

``` text
## HTTP V3 Nuget repository to download package dependencies https://api.nuget.org/v3/index.json or file system like file://C:/Users/johndoe/.nuget/packages
scanner.nuget.repository=https://api.nuget.org/v3/index.json
```

## Technical information

### Project exclusion rules

The .NET Analyzer extension includes several .NET related Project
Exclusion Rules. These rules are used to determine what CAST should do
(during the source code delivery process) when certain conditions are
met. For .NET, the following rules exist:

| Rule name                                                                     | Default Position                             |
|-------------------------------------------------------------------------------|----------------------------------------------|
| Exclude ASP .NET web projects when a Visual C#/basic .NET project also exists | True                                         |
| Exclude ASP projects when a .NET web project also exists                      | True                                         |
| Exclude duplicate .NET projects located inside the same source folder         | False for AIP Core ≥ 8.3.47, otherwise True. |

The position of these rules (either true or false) is set by default and
can impact the way in which projects are detected for analysis.
See Managing Project Exclusion Rules for more information.

### Information extracted during delivery

##### C# and VB.NET

-   Source files and their associated BuildAction (e.g. Compile,
    Content, EmbeddedResource, None, etc)
-   Assembly name (can be different from the project name)
-   .NET framework version
-   Compilation constants, including their values for VB.NET

##### C# specific

-   Default namespace (the default namespace to be used when creating
    new files; will be used when processing .xsd files)
-   Option "Allow unsafe code"

##### VB.NET specific

-   Root namespace (the default namespace to be used instead of the
    global namespace)
-   Option explicit (On / Off)
-   Option strict (On / Off)
-   Option infer (On / Off)
-   Imported namespaces (namespaces that are automatically imported by
    each file in the project)

### Unreferenced .cs files (Only for .Net Framework and .Net Standard projects)

Any .cs files that are not referenced in a parent .csproj file will be
ignored by the analyzer since they are not used in the application
source code.
