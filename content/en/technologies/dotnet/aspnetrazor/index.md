---
title: "ASP.NET MVC Razor support"
linkTitle: "ASP.NET MVC Razor support"
type: "docs"
weight: 3
---

## Introduction

ASP.NET MVC Razor is supported via the [HTML5 and
JavaScript](../../web/html5-js/com.castsoftware.html5/) extension, therefore you should ensure
that a HTML5 and JavaScript Analysis Unit is configured to cover the
relevant source code.

## Features

-   Creates objects specific to Razor corresponding to requests to
    the .NET server, as services, embedded in .cshtml files.
-   Creates objects specific to ASP.NET corresponding to operations
    which are methods of controllers in the .NET server, embedded in
    .cs files. This part of the extension is launched by the .NET
    analyzer.
-   Creates call links between these two kinds of objects through
    the [Universal Linker](../../multi/com.castsoftware.wbslinker/). These two kinds of objects
    are identified by a URL, in order to be compatible with the
    [Universal Linker]()../../multi/com.castsoftware.wbslinker/).

## Server part (without specific routing)

The following objects are created:

|Icon|Description|
|--- |--- |
|![Alt text](CAST_HTML5_WebSocketService32.png)|ASP.NET Any Operation|
|![Alt text](CAST_HTML5_GetXMLHttpRequestService32.png)|ASP.NET Get Operation / HTML5 Razor Get service|
|![Alt text](CAST_HTML5_UpdateXMLHttpRequestService32.png)|ASP.NET Put Operation|
|![Alt text](CAST_HTML5_PostXMLHttpRequestService32.png)|ASP.NET Post Operation / HTML5 Razor Post service|
|![Alt text](CAST_HTML5_DeleteXMLHttpRequestService32.png)|ASP.NET Delete Operation|

### URL creation

URLs are created as follows:

"Course/Edit" for the Edit method of this CourseController class.
This is an operation which may correspond to any type of request from
the client, then a CAST_AspDotNet_AnyOperation is created:

``` c#
public class CourseController : Controller
{ 
    public ActionResult Edit(int? id)
    {
    }
}
```

"Course/Edit"  for the Edit method of this CourseController class,
first Edit corresponds to a POST operation, then
a CAST_AspDotNet_PostOperation is created.  The second Edit corresponds
to any of the other operation types,and depending on this a
CAST_AspDotNet_GetOperation object, CAST_AspDotNet_PutOperation object
or CAST_AspDotNet_DeleteOperation are created:

``` c#
public class CourseController : Controller
{ 
    [HttpPost]
    public ActionResult Edit(int? id)
    {
    }

    public ActionResult Edit(other params)
    {
    }
}
```

"Course/Edit"  for the Edit method of this CourseController class,
this is a POST operation:

``` c#
public class CourseController : Controller
{ 
    [HttpPost, ActionName("Edit")]
    public ActionResult EditPost(int? id)
    {
    }
}
```

In this case, the url is "CourseEdit", and not "CourseEditPost" because
of the attribute "ActionName" presence, which contains an alias (click
to enlarge):

![Alt text](image2017-9-22_15_52_23.jpg)

## Server part (with specific routing)

This section is the same as the previous one (Server part (without
specific routing)), except that URLs are modified depending on a
specific config routing. Specific routings are present in the following
files:

App_Start/RouteConfig.cs files:

``` c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ContosoUniversity
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
```

or some files named \<name\>AreaRegistration.cs files:

``` c#
using System.Web.Mvc;

namespace XL.XLI.ScoringWebAppTool.WebUI.Areas.DesignProfessional
{
    public class DesignProfessionalAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get { return "DesignProfessional"; }
        }
        
        /* Order is important, as the engine uses fall-through matching logic. */
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute
            (
                "DesignProfessional_Default2",
                "DesignProfessional/{controller}/{action}/{id}",
                new
                {
                    controller = "PreRenewal",
                    action     = "Index",
                    id         = UrlParameter.Optional
                }
            );
            context.MapRoute
            (
                "DesignProfessional_Default",
                "DesignProfessional/PreRenewal",
                new
                {
                    controller = "PreRenewal",
                    action     = "Index",
                    id         = UrlParameter.Optional
                }
            );
        }
    }
}
```

## Razor Client part

Creates object specific to Razor corresponding to requests to the NET
server, as services, embedded in .cshtml files. The following
objects are created:

|Icon|Description|
|--- |--- |
|![Alt text](CAST_HTML5_GetXMLHttpRequestService32.png)|HTML5 Razor Get service|
|![Alt text](CAST_HTML5_PostXMLHttpRequestService32.png)|HTML5 Razor Post service|


### Code examples

#### Html.ActionLink call

This code in a .cshtml file located in a directory named
"Course" will create an object of type
CAST_HTML5_Razor_GetService whose URL is "Course/Delete/{}". The
presence of "{}" is due to the presence of a parameter of type
"new {}" containing a word equals to "id" or ending with
"id":

``` c#
@Html.ActionLink("Delete", "Delete", new { id = item.CourseID })
```

![Alt text](image2017-9-22_16_11_32.jpg)

#### Url.Action call

This code in a .cshtml file located in a directory named
"Student" will create an object of type
CAST_HTML5_Razor_GetService whose URL is "Student/Index":

``` c#
@Html.PagedListPager(Model, page => Url.Action("Index",
    new { page, sortOrder = ViewBag.CurrentSort, currentFilter = ViewBag.CurrentFilter }))
```

![Alt text](image2017-9-22_16_14_44.jpg)

#### Html.ActionLinkWithAccess call

This code in a .cshtml file located in a directory named
"StaffIncreaseProfilePeriod" will create an object of type
CAST_HTML5_Razor_GetService whose URL is
"StaffIncreaseProfilePeriod/Create":

``` c#
<li>@Html.ActionLinkWithAccess(__("Add increase profile"), "Create", null, new { @class = "btn popupLink", data_popupsize = "big" })</li>
```

#### Html.Action call

This code in a .cshtml file located in a directory named
"Account" will create an object of type
CAST_HTML5_Razor_GetService whose URL is
"Account/ExternalLoginsList".

``` c#
@Html.Action("ExternalLoginsList", new { ReturnUrl = ViewBag.ReturnUrl })
```

#### Ajax.ActionLink call

This code in a .cshtml file located in a directory named
"NotificationMessenger" will create an object of type
CAST_HTML5_Razor_PostService whose URL is
"NotificationMessenger/Delete/{}".

``` c#
@Ajax.ActionLink("x", "Delete", "NotificationMessenger", new { notific.Id }, new AjaxOptions() { HttpMethod = "POST", OnComplete = "DismissNotification" }, new { @class = "notification-close" })
```

#### Html.RenderAction call

This code in a .cshtml file located in a directory named
"Holidays" will create an object of type
CAST_HTML5_Razor_PostService whose url is
"Holidays/ColorsLegend".

``` c#
@{ Html.RenderAction("ColorsLegend"); }
```

#### Html.BeginForm call

This code in a .cshtml file will create an object of type
CAST_HTML5_Razor_PostService whose url is "Student/Index".

``` c#
   @using (Html.BeginForm("Index", "Student", FormMethod.Get))
{
    <p>
        Find by name: @Html.TextBox("SearchString", ViewBag.CurrentFilter as string)
        <input type="submit" value="Search" />
    </p>
}
```

#### Ajax.BeginForm call

This code in a .cshtml file will create an object of type
CAST_HTML5_Razor_PostService whose url is
"Missions/SynthesisAjaxHandler/{}".

``` c#
@using (Ajax.BeginForm(
"SynthesisAjaxHandler", //actionName
"Missions", //controllerName
new { //routeValues
missionId = Model.Mission.Id,
dayId = Model.MissionDayId,
method="GET"
},
new AjaxOptions { //Ajax setup
UpdateTargetId = "synthesisLinesDiv",
HttpMethod = "GET",
InsertionMode = InsertionMode.Replace,
},
new { //htmlAttributes
Id = "form-selectSynthesis",
//method="get"
}
))
```
