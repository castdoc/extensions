---
title: "Requirements"
linkTitle: "Requirements"
type: "docs"
weight: 2
---

## Required third-party software

To successfully deliver and analyze .NET code, the following third-party
software is required:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh"><p>Install on workstation running the AIP
Console / DMT (for delivery)</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>Nothing required.</p>
<div>
<div>
<p>Some frameworks/third-party packages are shipped with the extension.</p>
<p>When using the legacy DMT, if your source code references specific
frameworks or third-party packages, then these will be required on
the workstation running the DMT for packaging purposes.</p>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh"><div class="content-wrapper">
<p>Install on workstation running AIP Node / CMS (for analysis)</p>
</div></th>
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>.NET Framework ≥ 4.7.2 must be installed</strong> in order
for the analysis to function. A check will be done when the analysis
starts and a message will produced if the minimum .NET Framework cannot
be found.</p>
<div>
<div>
<p>Note that a .NET Framework is installed with the AIP Core setup, however this may not be the required version.
Please check.</p>
</div>
</div>
<p><strong><br />
</strong></p>
</div></td>
</tr>
</tbody>
</table>

## Location of the source code deployment folder

It is possible to configure the source code Deployment folder on a
network share (as discussed in [Configure AIP Node storage folder
locations - optional - v.
1.x](https://doc.castsoftware.com/display/AIPCONSOLE/Configure+AIP+Node+storage+folder+locations+-+optional+-+v.+1.x).
However, in the case of .NET analyses CAST recommends that the
Deployment folder is located on the same server as the AIP Node. If
the Deployment folder is located on a remote network file share, there
is a risk that assemblies may not be loaded correctly during the
analysis, causing external objects to not be saved.

## Location of extensions folder

If you have altered the location where extensions are stored to a
network location remote to the AIP Node (for example changing
the CAST_PLUGINS_ROOT_PATH in the
[CastGlobalSettings.ini](https://doc.castsoftware.com/display/AIPCONSOLE/Configure+AIP+Node+storage+folder+locations+-+optional+-+v.+1.x)
file) then you will need to add the following lines to the .NET
Framework machine.config file (located on the machine where the
analysis is being run). This is to ensure that the .NET Analyzer DLLs
located in the remote network folder can be successfully run from a
network location. The analysis will fail without this configuration.

Replace the following tag:

``` xml
<runtime />
```

with:

``` xml
<runtime>
    <loadFromRemoteSources enabled="true"/>
</runtime>
```
