---
title: "Analysis configuration"
linkTitle: "Analysis configuration"
type: "docs"
weight: 7
---

## Using Console

Console exposes the Technology configuration options once a version has
been accepted/imported, or an analysis has been run.
Click C/C++ Technology to display the available options:

![](498926300.jpg)

Technology settings are organized as follows:

-   Settings specific to the technology for the entire
    Application (i.e. all Analysis Units)
-   List of Analysis Units (a set of source code files to analyze)
    created for the Application
    -   Settings specific to each Analysis Unit (typically the
        settings are the same or similar as at Application level) that
        allow you to make fine-grained configuration changes.

Settings are initially set according to the information discovered
during the source code discovery process when creating a version.
You should check that these auto-determined settings are as required and
that at least one Analysis Unit exists for the specific technology:

*Click to enlarge*

![](498926299.jpg)

Technology and Analysis Unit settings

Notes on Technology and Analysis Unit settings:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"
style="text-align: left;"><strong>Technology</strong></td>
<td class="confluenceTd" style="text-align: left;"><ul>
<li>These are the default options that will be used to populate the same
fields at <strong>Analysis Unit </strong>level when the source code
discovery is not able to find a particular setting in the project files.
If you need to define specific settings at Technology level then you can
override them. These settings will be <strong>replicated</strong> at
Analysis Unit level (except as described in the point below).</li>
<li>If you make a change to a specific option at <strong>Analysis Unit
level</strong>, and then subsequently change the same option
at <strong>Technology level</strong>, this setting will NOT be mirrored
back to the Analysis Unit - this is because specific settings at
Analysis Unit level have precedence if they have been changed from the
default setting available at Technology level.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;"><strong>Analysis
Unit</strong></td>
<td class="confluenceTd" style="text-align: left;"><div
class="content-wrapper">
<ul>
<li>Options available at this level are valid only for the specific
Analysis Unit.</li>
<li>When the Analysis Unit has been created <strong>automatically during
the source code delivery process</strong>, options will "inherit" their
initial configuration settings from the discovery process (i.e.
"project" settings). Where an option could not be defined automatically,
it will "inherit" its initial configuration settings from those defined
at <strong>Technology level</strong>.</li>
<li>Analysis Units that are<strong> manually</strong> defined will
"inherit" their initial configuration settings from the settings defined
at <strong>Technology level</strong></li>
<li>Modifying an identical option at <strong>Technology
level </strong>will automatically update the <strong>same
option</strong> in the <strong>Analysis Unit editor</strong> unless that
specific option has already been modified independently in the Analysis
Unit editor</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

### Technology level settings

![](498926298.jpg)

#### Settings

<table class="relative-table wrapped confluenceTable"
style="width: 99.9451%;">
<colgroup>
<col style="width: 7%" />
<col style="width: 45%" />
<col />
<col style="width: 46%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh" style="width: 7.07625%">Option</th>
<th colspan="3" class="confluenceTh"
style="width: 92.8689%">Description</th>
</tr>
&#10;<tr class="odd">
<th class="confluenceTh" style="width: 7.07625%">Development
Environment</th>
<td colspan="3" class="confluenceTd" style="width: 92.8689%"><div
class="content-wrapper">
<p>Use this option to choose the development environment of your source
code files - this option is detected automatically during source code
delivery. <strong>AIP Console</strong> then applies a default
<strong>Environment Profile</strong> (these can be viewed here on the
AIP Node: &lt;install_folder&gt;\EnvProf\CPP) that matches the selected
<strong>Development Environment</strong>.</p>
<div>
<div>
<ul>
<li>If the source code is detected as a variant of Visual C++, then the
appropriate option will be set at Analysis Unit level.</li>
<li>If you do not have the correct installation of Visual Studio on the
AIP Node, you can choose the Not Specified option - this will impact
your analysis results however and should only be used if your analyses
fail.</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh" style="width: 7.07625%">STL Support</th>
<td colspan="3" class="confluenceTd" style="width: 92.8689%"><div
class="content-wrapper">
<p>This option allows you to choose how you want CAST to handle
<strong>STL header files</strong> that are referenced in your source
code. Not all other Environment Profiles provided by CAST contain STL
header files (for copyright reasons) and this can lead to an incomplete
analysis, and thus to incomplete results.  As a direct result of this,
CAST has introduced this option that allows you to tell AIP Console
whether the STL header files are available or not:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<th class="confluenceTh">CAST emulation</th>
<td class="confluenceTd"><div class="content-wrapper">
If STL header files <strong>are not available</strong> in your source
code, select this option. This will then force the CAST Management
Studio to activate a basic (i.e. non exhaustive) emulation of the most
common STL headers (i.e. &lt;string&gt;, &lt;vector&gt;,
&lt;algorithm&gt; etc.), thus avoiding an incomplete analysis or
ambiguous/erroneous analysis results in most circumstances.
<p>This option is provided via a pre-defined Environment Profile, and
you can list the headers that are handled by viewing the contents of the
following folder:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>&lt;install_folder&gt;\EnvProf\CPP\STLforUnix</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">STL already in your Source Code Repository</th>
<td class="confluenceTd">If STL header files <strong>are
available</strong> in your source code, select this option. The STL
header files will be analyzed along with the rest of your source code as
normal.
<p>This is the default option.</p></td>
</tr>
</tbody>
</table>
</div>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh">Accept trigraphs</th>
<td colspan="3" class="confluenceTd">Select this option only if your
code contains trigraphs. The analyzer will then interpret normalized
character sequences as a single character - for example
<strong>??&lt;</strong> will be interpreted as <strong>{</strong>. Note
also that this option will also force the analyzer to accept
digraphs.</td>
</tr>
</tbody>
</table>

#### Files to Analyze

This section displays a list of the file extensions for the various
parts of your C/C++ application. These file extensions are the default
included in the current file filter and only files that match these file
extensions will be taken into account during the analysis. You can
manually update these file extensions if necessary.

|                     |                                                                                                                                                                          |     |
|---------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----|
| C File Extensions   | Use this option to specify the file extensions that will be considered as C files for analysis purposes. By default, a list of extensions will already be present.   |     |
| C++ File Extensions | Use this option to specify the file extensions that will be considered as C++ files for analysis purposes. By default, a list of extensions will already be present. |     |

#### Resources

The options Force Include File and Includes enable you to define
additional paths for files that have been specified in the #include
part of your project files. This is to ensure the correct resolution of
links to files located in other directories. The analyzer will take into
account the two "include" styles:

-   #include "fileA.h" \> in this case, the analyzer will
    automatically search the path of the file that contains the
    #include, THEN the additional paths you have added and in the order
    specified.
-   #include \<fileB.h\> \> in this case, the analyzer will search
    the additional paths first then, THEN the path of the the file that
    contains the #include.

The include paths are locations from where the included headers are
found. These parameters should be identified automatically from the
compilation directives. The files found in these include paths will be
saved in the Analysis schema. Thus, the only header paths you should
mention here are the header path of the customer applications.

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Force Include File</th>
<td class="confluenceTd"><p>This option is used to provide an extra
header file which will be appended at the beginning of the preprocessed
source, as if each source file were beginning with
a <strong>#include</strong> directive on this file. It is useful in the
following cases:</p>
<ul>
<li>When you have a very large number of macros</li>
<li>When you have macros that need to be undefined (the GUI here can
only define them)</li>
<li>When the code is using embedded 'C' on Sybase: contrary to Oracle,
the Sybase embedded 'C' preprocessor adds some code at the beginning of
the generated files. To compensate for this behaviour, you should add
the following code in the header used for this option: <br />
&#10;<ul>
<li><strong>#include &lt;sybhesql.h&gt;</strong><br />
<strong>SQLCA sqlca;</strong></li>
</ul></li>
</ul>
<p>You should indicate here the "ExternFunctions.h". If you have several
files to "force include", all you need to do is to create an additional
file that will include them (using the #include directive) and set this
file as being "force included".</p>
<ul>
<li>Use the <strong>Add</strong> button to browse for the file you want
to include. This file will contain the macros that you want the analyzer
to take into account and must have the same structure as any C/C++ file
(in other words, this file can contain "#define" orders to define
macros, "#include" orders to include files, but also declare
functions).</li>
<li>It is possible to enter a simple filename (for example test.h)
without an absolute path. In this case, the analyzer will search for the
file as follows:
<ul>
<li>in the associated Source Repository</li>
<li>then in any defined <strong>Include Path</strong></li>
</ul></li>
</ul></td>
</tr>
<tr class="even">
<th class="confluenceTh">Includes</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This option enables you to add your include paths:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Adding a path</strong></td>
<td class="confluenceTd"><p>Click the ADD button. Select the specific
path you want to include from the available source code.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Removing a path</strong></td>
<td class="confluenceTd"><p>To remove a path that you do not want to
include in the analysis:</p>
<ul>
<li>Select the path that you want to remove from the list by
left-clicking with the mouse.</li>
<li>Click the <strong>Remove</strong> button to clear the selected
item(s). The <strong>Delete</strong> key and the right-click short cut
menu can also be used. You can remove multiple items by selecting the
items in the list and then clicking the <strong>Remove</strong>
button.</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Editing an existing path</strong></td>
<td class="confluenceTd"><p>If you want to edit an existing path:</p>
<ul>
<li>Select the item in the list and use the fields directly below the
list to make your changes.</li>
</ul></td>
</tr>
</tbody>
</table>
</div>
<div>
<div>
Note that include paths configured in the current <strong>Includes
section</strong> will be processed BEFORE any definitions made in the
Environment Profiles associated to the <strong>Development
Environment </strong>option, and to any <strong>Custom Environment
Profiles</strong> you have added.
</div>
</div>
<p><em>Defining include paths for Microsoft compilers</em></p>
<p>Usually, if the Microsoft C++ compiler is installed on the analysis
machine, you just need to select the default "Microsoft VCxxxx"
Environment Profile (via the <strong>Development
Environment</strong> option ). In some cases, you may also need to
provide your include list yourself. The following rules should be
followed:</p>
<ol>
<li>Do not add more directories then required</li>
<li>Do not select the "recursive" option for these paths</li>
<li>Do not select a path such as <strong>C:\Program Files\Microsoft
Visual Studio 10.0\VC\crt\src</strong> (this typically contains files
that are reserved for Microsoft internal use and debugger)</li>
</ol>
<p>In any of the three cases above, you will get an error message during
the analysis such as:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>#error directive encountered &#39;Use of C runtime library internal header file
... (file &#39;C:\PROGRAM FILES\MICROSOFT[...]\VC\CRT\SRC\CRTDEFS.H&quot;)&#39;</code></pre>
</div>
</div>
<p>Include paths for Microsoft compilers should resemble the
following:</p>
<p>For Microsoft VC 9.0 (2008) and higher:</p>
<ul>
<li>C:\Program Files\Microsoft Visual Studio 10.0\VC\include</li>
<li>C:\Program Files\Microsoft Visual Studio 10.0\VC\atlmfc\include</li>
<li>C:\Program Files\Microsoft SDKs\Windows\vxx.xx\include</li>
</ul>
<p>(the third one is for &lt;windows.h&gt; etc., the exact value for
xx.xx depends on your exact installation)</p>
<p>For older compilers:</p>
<ul>
<li>C:\Program Files\Microsoft Visual Studio 8\VC\include</li>
<li>C:\Program Files\Microsoft Visual Studio 8\VC\atlmfc\include</li>
<li>C:\Program Files\Microsoft Visual Studio
8\VC\PlatformSDKinclude</li>
</ul>
<p>For even older MS Dev 6 (VC98):</p>
<ul>
<li>C:\Program Files\Microsoft Visual Studio\VC98\Include</li>
<li>C:\Program Files\Microsoft Visual Studio\VC98\MFC\Include</li>
<li>C:\Program Files\Microsoft Visual Studio\VC98\atl\Include</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh">Macros</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>Use this option to define any macros that are present in your source
code and that need to be taken into account during the analysis.</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<th class="confluenceTh"><strong>Adding a macro</strong></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>Click the ADD button and define the macro's name and value:</p>
<p><img src="498926297.jpg" draggable="false"
data-image-src="498926297.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="498926297"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="add_macro.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="235477464"
data-linked-resource-container-version="3" height="250" /></p>
<p><br />
</p>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">Removing a macro</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>To remove a macro that you do not want to include in the analysis,
click the <strong>Remove</strong> button to clear the selected
item(s):</p>
<p><img src="498926296.jpg" draggable="false"
data-image-src="498926296.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="498926296"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="macro_remove.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="235477464"
data-linked-resource-container-version="3" width="530"
height="191" /></p>
</div></td>
</tr>
<tr class="odd">
<th class="confluenceTh"><p>Editing a macro</p></th>
<td class="confluenceTd"><div class="content-wrapper">
<p>Use the <strong>Edit</strong> button to edit the macro:</p>
<p><img src="498926295.jpg" draggable="false"
data-image-src="498926295.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="498926295"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="macro_edit.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="235477464"
data-linked-resource-container-version="3" width="530"
height="191" /></p>
</div></td>
</tr>
</tbody>
</table>
</div>
<div>
<div>
<ul>
<li>Note that it is also possible to define <strong>persistent
Macros</strong> via a custom Environment Profile in CAST Management
Studio.</li>
<li>Note that macro definitions configured here will be processed AFTER
any definitions made in the Environment Profiles associated to the
<strong>Development Environment </strong> option, and to any
<strong>custom Environment Profiles</strong> you have added.</li>
<li>If you include a file containing a list of macros, this file is
included in each main file that is analyzed. In other words, and for
illustrative purposes only, it would be as if each file selected for
analysis started with "#include "autoinclude.h" " (where autoinclude.h
is the name of the file selected in the <strong>Includes</strong>
section below). These macros are processed AFTER the macro definitions
made in this section.</li>
<li>Most, if not all applications developed with Microsoft Visual C++
rely on a set of (standard or not) header files provided with the
compiler (stdlib.h, windows.h...). These headers themselves heavily rely
on various macros that are internally defined by the compiler. These
macros are predefined via Environment Profiles that are applied
automatically to the source code following the selections made in the
<strong>Development Environment</strong> option (see
<strong>IDE</strong> section above), as such there is no need to
explicitly add them.</li>
</ul>
</div>
</div>
</div></td>
</tr>
</tbody>
</table>

#### Dependencies

![](498926293.jpg)

Dependencies are configured at Application level for each
technology and are configured between individual Analysis
Units/groups of Analysis Units (dependencies between technologies are
not available as is the case in the CAST Management Studio). You can
find out more detailed information about how to ensure dependencies are
set up correctly, in Validate dependency configuration.

### Analysis Units

This section lists all Analysis Units created either automatically based
on the source code discovery process or manually. You should check that
at least one Analysis Unit exists for the specific technology.
Settings at Analysis Unit level can be modified independently of the
parent Technology settings and any other Analysis Units if required -
click the Analysis Unit to view its technology settings:

![](498926292.jpg)

See [Application - Config - Analysis - Working with Execution
Groups](https://doc.castsoftware.com/display/AIPCONSOLEDRAFT/Application+-+Config+-+Analysis+-+Working+with+Execution+Groups)
for more information about the options available here. Clicking an
Analysis Unit will take you directly to the available Analysis Unit
level settings:

![](498926291.jpg)

#### Technology settings at Analysis Unit level



Technology settings at Analysis Unit level are for the most part
identical to those explained above. Some differences exist:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<th class="confluenceTh">Settings</th>
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>Use Microsoft Managed C++ extensions (CLR)</strong></p>
<p>Select this option if your Source Code contains Microsoft Managed C++
extensions targeted at the Common Language Runtime (CLR). A
corresponding Environment Profile will then be applied. See the
following location on the AIP Node:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>&lt;install_folder&gt;\EnvProf\CPP\Microsoft Managed C++</code></pre>
</div>
</div>
<p>Note that this option is automatically ticked if the project is
configured to execute code under the Common Language Runtime virtual
machine.</p>
</div></td>
</tr>
<tr class="even">
<th class="confluenceTh">Main Files</th>
<td class="confluenceTd"><div class="content-wrapper">
<p>This section lists the location of the files for analysis. This is
usually a location in the designated Deployment folder. You can
<strong>include</strong> additional folder or <strong>exclude</strong>
existing folders if required using the <strong>Add</strong> button. All
three options will open the web interface on the following root folder
location, unless the location has been changed during the <strong><a
href="https://doc.castsoftware.com/display/AIPCONSOLE/Configure+AIP+Node+storage+folder+locations+-+optional+-+v.+1.x">AIP
Console setup and installation</a></strong>:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>%PROGRAMDATA%/CAST/AipConsole/AipNode/upload/</code></pre>
</div>
</div>
<p><img src="498926290.jpg" draggable="false"
data-image-src="498926290.jpg"
data-unresolved-comment-count="0" data-linked-resource-id="498926290"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="mainfiles.jpg"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/jpeg"
data-linked-resource-container-id="235477464"
data-linked-resource-container-version="3" width="600" /></p>
</div></td>
</tr>
</tbody>
</table>

## Using legacy CAST Management Studio



### Introduction to analysis configuration options

The CAST Management Studio has three levels at which analysis
configuration options can be set:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Technology</strong></td>
<td class="confluenceTd"><ul>
<li>The options available at this level are valid for <strong>all
Applications</strong> managed in the CAST Management Studio.</li>
<li>These are the default options that will be used to populate the same
fields at <strong>Application </strong>and <strong>Analysis
Unit </strong>level. If you need to define specific options for a
specific Application or Analysis Unit, then you can override them.</li>
<li>If you make a change to a specific option
at <strong>Application</strong> or <strong>Analysis Unit level</strong>,
and then subsequently change the same option at <strong>Technology
level</strong>, this setting will NOT be mirrored back to the
Application or Analysis Unit - this is because specific settings at
Application and Analysis Unit level have precedence if they have been
changed from the default setting available at Technology level.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Application</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>The options available at this level set are valid for
all <strong>corresponding Analysis Units</strong> defined in
the <strong>current Applicatio</strong>n (so making changes to a
specific option will mean all Analysis Units in that specific Technology
will "inherit" the same setting). If you need to define specific options
for a specific Analysis Unit in a specific Technology, then you can do
so at Analysis Unit level.</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Analysis Unit</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<ul>
<li>Options available at this level are valid only for the specific
Analysis Unit.</li>
<li>An Analysis Unit can best be described as a set of configuration
settings that govern how a perimeter of source code is consistently
analyzed.</li>
<li>Analysis Units are automatically created when you use
the <strong>Set as current version</strong> option to deploy the
delivered source code - as such they correspond
to <strong>Projects</strong> discovered by the CAST Delivery Manager
Tool. However, they can also be created manually for situations where no
Analysis Unit has been automatically created for a given project.
<ul>
<li>When the Analysis Unit has been
created <strong>automatically</strong>, options will "inherit" their
initial configuration settings from the discovery process in the CAST
Delivery Manager Tool (i.e. "project" settings). Where an option could
not be defined automatically via the CAST Delivery Manager Tool, it will
"inherit" its initial configuration settings from those defined
at <strong>Technology level </strong>and at <strong>Application
level</strong>.</li>
<li>Analysis Units that are<strong> manually</strong> defined will
"inherit" their initial configuration settings from the settings defined
at <strong>Technology level</strong> and at <strong>Application
level</strong>.</li>
</ul></li>
<li>Modifying an identical option at <strong>Technology
level</strong> or at <strong>Application level </strong>will
automatically update the <strong>same option</strong> in
the <strong>Analysis Unit editor</strong> unless that specific option
has already been modified independently in the Analysis Unit
editor.</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

Some settings at Application and Analysis Unit level have a "Reset"
option - using this will reset the option to the value set at the parent
level:

![](attachments/219061153/219061152.jpg)

### Auto-configuration validation

#### Technology / Application level

Using the Technology level or Application level options,
validate the settings for C / C++ packages. Make any update as
required. These settings apply to the Technology or Application as a
whole (i.e. all Analysis Units):

![](235477476.jpg)

![](235477475.jpg)

#### Analysis Unit level

To inspect the auto-generated analysis configuration, you should
review the settings in each Analysis Unit - they can be accessed through
the Application editor:

![](235477477.jpg)

### Technology options

Normally, the source code discoverer will auto-configure the Analysis
Unit and analysis settings as described above. However, in some
circumstances, these settings may be incorrect and must be adjusted
manually. The main parameters to work on are as follows:

-   Source Settings
-   Analysis
    -   IDE used for the Analysis Unit
    -   STL Support
    -   Include paths
    -   Macro definitions
    -   Precompiled header
    -   Environment Profile

The available options for configuring an analysis are described below.
Note that some options are not available at specific levels
(Technology/Application/Analysis Unit).

#### Source Settings

This tab shows the location of each type of source code in the C / C++
Analysis Unit - this is determined automatically by the CAST Delivery
Manager Tool. You should, however, review the configuration and make any
changes you need:



![](235477478.jpg)

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Project path</strong></td>
<td colspan="2" class="confluenceTd">The file based location of the
corresponding project. This field is read-only. When the field contains
<strong>User defined</strong>, this indicates that the Analysis Unit has
been defined manually instead of automatically following the use of the
CAST Delivery Manager Tool.</td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd"><strong>Consider Files
As</strong></td>
<td class="confluenceTd"><strong>C only</strong></td>
<td class="confluenceTd">If this option is selected, C++ specifics are
ignored (templates, namespaces, classes, inheritance and polymorphism)
and are not taken into account when the source files are analyzed.
<p>This option allows optimized handling of C files.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>C++ only</strong></td>
<td class="confluenceTd">If this option is selected, C++ specifics
(templates, namespaces, classes, inheritance and polymorphism) are taken
into account when the source files are analyzed.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Both C and C++</strong></td>
<td class="confluenceTd">If this option is selected only one distinct
job is generated and executed: one <strong>C++ technology</strong> job
using the <strong>Consider Files As &gt; C++</strong> option. However
the results stored in the CAST Analysis Service will contain both
<strong>C and C++ object types</strong>.
<p>In other words, both file types are analyzed as <strong>C++
files</strong> and <strong>Line Of Code Count</strong> (LoC) for C files
will be included in the C++ technology rather than the C
technology.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>C File Extensions</strong></td>
<td colspan="2" class="confluenceTd">Use this option to specify the file
extensions that will be considered as <strong>C</strong> files for
analysis purposes.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>C++ File Extensions</strong></td>
<td colspan="2" class="confluenceTd">Use this option to specify the file
extensions that will be considered as <strong>C++</strong> files for
analysis purposes.</td>
</tr>
</tbody>
</table>

Impacts of choosing one parsing option over the other.

Selecting one of these options will cause the source code to be handled
differently by the analyzer:

-   A function or a method contains any number of declarations and at
    least one definition. The grouping of these blocks in one unique
    object is done differently by the analyzer according to whether the
    C++ or C option has been selected:
    -   With option C, declaration or definition blocks with the same
        name are grouped together in the same function or in the same
        method without taking into account any parameters.
    -   With option C++, declaration or definition blocks with the same
        name and the same parameters are grouped together in the same
        function or in the same method.

For example, using the option C++ here would create two functions called
"foo" in the Analysis Service schema, whereas using the C option would
only create one "foo" function.

``` cpp
 void foo();
 void foo(int a)
 {
 }
```

-   The analyzer detects calling links differently depending on whether
    the C or C++ option is selected:
    -   With option C, the analyzer believes that the function or the
        method is called as soon as an object with the same name is
        called.
    -   With option C++, the analyzer believes that the function or the
        method is called if an object with the same name and comparable
        parameters is also called.

For example (and using the previous example), because using the C++
option with the previous example would create two functions "foo" in the
Analysis Service - one with a parameter, the other without - the
analyzer will only create a Call type link to the function WITHOUT the
parameter. Using the C option, the analyzer will create a link to the
one function even if it has parameters.

``` cpp
void main ()
 {
 foo();
 }
```

Notes

-   A file written in class C can be analyzed with the option C++
    only with no risk. The only consequences are that the job may take
    slightly longer to run and some groupings of functions/methods with
    the same name but different parameters will not be carried out.

-   A file written in C++ must NOT be analyzed with the option C
    File.

-   When C/C++ Analysis Units are auto created in the CAST Delivery
    Manager Tool from Visual Studio project files (.vcproj files) and
    these project files have the Visual Studio option Compile As NOT
    set in bold (i.e. this reflects a default Visual Studio value
    which is not written in the .vcproj file), the CAST Management
    Studio will not be able to detect the Compile As value. As a
    result the Compile As value cannot be reflected in the Analysis
    Unit under the option Consider File As. In this instance, the
    source file extension will be used to determine the Analysis Unit
    Consider File As value.  
      
    For example, you may have a project which has the value Compile
    As set to Compile as C++ Code in the Visual Studio environment
    (NOT set in bold). In this instance, when the Analysis Units are
    auto created, the CAST Management Studio cannot detect the Compile
    As value and will set the Analysis Unit Consider File As
    option to C only (using the file extension).

-   If you have an application where .c files must be analyzed as
    C++ files, you should set the .c extension as a C++ extension -
    i.e. add it to the C++ File Extensions field.

#### Analysis

The settings in this tab govern how the source code is handled by the
analyzer:



![](235477466.jpg)

##### Global settings

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td rowspan="2" class="confluenceTd"><strong>Analyzer to
invoke</strong></td>
<td rowspan="2" class="confluenceTd">Use this option to choose between
the analysis engine that will be used to run the analysis:</td>
<td class="confluenceTd"><div class="content-wrapper">
<strong>Legacy analyzer</strong>
<p>This option invokes the legacy C/C++ analyzer as provided in previous
releases of CAST AIP and in the <strong>C and Cpp Analyzer
extension</strong>. By default this option <strong>will always be
active</strong> at Technology/Application/Analysis Unit level.</p>
<div>
<div>
<p>Please do not modify this option when using the <strong>C and Cpp
Analyzer extension</strong>.</p>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<strong>Clang analyzer</strong>
<p>This option is only relevant for those that are using the
<strong>CAST AIP Objective C Extension</strong> in order to analyze
<strong>Objective C</strong> source code. This option must be active in
order for the Objective C source code to be analyzed by correctly.</p>
<p>If this option is <strong>invoked</strong> and:</p>
<ul>
<li>the <strong>CAST AIP Objective C Extension</strong> is
<strong>not</strong> installed, then an error will be displayed when the
analysis is run:</li>
</ul>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>The C-Family analyzer is not installed. Please use the Extension Downloader to get it (com.castsoftware.cfamily)</code></pre>
</div>
</div>
<ul>
<li>the <strong>CAST AIP Objective C Extension</strong> is installed but
your source code does <strong>not contain Objective C</strong> (i.e. it
is a traditional Visual C/C++ application) then results will not be
consistent with the output of the "legacy analyzer". In this situation
where only traditional Visual C/C++ source code is being analayzed, CAST
recommends that the <strong>Legacy analyzer</strong> option is always
used.</li>
</ul>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>IDE used to develop the
Application/Analysis Unit</strong></td>
<td colspan="2" class="confluenceTd">Use this option to choose the
development environment of your source code files. The <strong>CAST
Management Studio</strong> then applies a default <strong>Environment
Profile</strong> (these can be viewed here:
&lt;install_folder&gt;\EnvProf\CPP) that matches the selected
<strong>Development Environment</strong>.
<p>Note that if the CAST Delivery Manager Tool detects the source code
as a variant of Visual C++, then the appropriate option will be set at
Analysis Unit level.</p></td>
</tr>
<tr class="even">
<td rowspan="3" class="confluenceTd"><strong>STL Support</strong></td>
<td colspan="2" class="confluenceTd">This option allows you to choose
how you want CAST to handle <strong>STL header files</strong> that are
referenced in your source code. Not all other Environment Profiles
provided by CAST contain STL header files (for copyright reasons) and
this can lead to an incomplete analysis, and thus to incomplete
results.  As a direct result of this, CAST has introduced this option
that allows you to tell the CAST Management Studio whether the STL
header files are available or not:</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>CAST emulation</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
If STL header files <strong>are not available</strong> in your source
code, select this option. This will then force the CAST Management
Studio to activate a basic (i.e. non exhaustive) emulation of the most
common STL headers (i.e. &lt;string&gt;, &lt;vector&gt;,
&lt;algorithm&gt; etc.), thus avoiding an incomplete analysis or
ambiguous/erroneous analysis results in most circumstances.
<p>This option is provided via a pre-defined Environment Profile, and
you can list the headers that are handled by viewing the contents of the
following folder:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>&lt;install_folder&gt;\EnvProf\CPP\STLforUnix</code></pre>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>STL already in your Source Code
Repository</strong></td>
<td class="confluenceTd">If STL header files <strong>are
available</strong> in your source code, select this option. The STL
header files will be analyzed along with the rest of your source code as
normal.
<p>This is the default option.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Use Microsoft Managed C++ extensions
(CLR)</strong></td>
<td colspan="2" class="confluenceTd"><div class="content-wrapper">
<p>Select this option if your Source Code contains Microsoft Managed C++
extensions targeted at the Common Language Runtime (CLR). A
corresponding Environment Profile will then be applied. See:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>&lt;install_folder&gt;\EnvProf\CPP\Microsoft Managed C++</code></pre>
</div>
</div>
<p>Note that this option is automatically ticked if the project is
configured to execute code under the Common Language Runtime virtual
machine.</p>
</div></td>
</tr>
</tbody>
</table>

##### Includes

The options Force Include File and Define the Include Paths
enable you to define additional paths for files that have been
specified in the #include part of your project files. This is to ensure
the correct resolution of links to files located in other directories.
The analyzer will take into account the two "include" styles:

-   #include "fileA.h" \> in this case, the analyzer will
    automatically search the path of the file that contains the
    #include, THEN the additional paths you have added and in the order
    specified.
-   #include \<fileB.h\> \> in this case, the analyzer will search
    the additional paths first then, THEN the path of the the file that
    contains the #include.

The include paths are locations from where the included headers are
found. These parameters should be identified automatically from the
compilation directives. The files found in these include paths will be
saved in the Analysis Service. Thus, the only header paths you should
mention here are the header path of the customer applications

The C++ Analyzer provides a feature that will scan the source code in
order to detect missing header paths and macro definitions. This feature
is available through the action Test analysis on the current Analysis
Unit in the Execution tab of the Analysis Unit editor. The
tools delivered in the CAST Implementation Framework are therefore no
longer necessary.

You should not reference here any third party software header paths (see
previous section). Also files that are common to several jobs should be
defined in an Environment Profile. Note that include paths defined
directly in the job are searched first. Include paths defined in
Environment Profiles have a lesser priority. As for the compiler,
inclusions defined within "" are searched within the path of the file
being analyzed, while inclusions defined within \<\> aren't.

You should also avoid adding more include paths than required: the
search for an included file in the include path list occurs regularly
and too many unnecessary paths will affect performance.

Note that the Include paths are not recursive unless you choose them
to be: as such the default behavior is that sub-folders of the folders
mentioned in the lists are not considered as potential include paths if
not explicitly added to the list (nor are they in the compiler options
of the output from the Macro Analyzer, so you should report them as is).
However, if you have an include like #include "dir1/file1.h", you should
select the directory containing *dir1*, not the one containing *file1*.

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Force Include File</strong></td>
<td class="confluenceTd"><p>This option is used to provide an extra
header file which will be appended at the beginning of the preprocessed
source, as if each source file were beginning with
a <strong>#include</strong> directive on this file. It is useful in the
following cases:</p>
<ul>
<li>When you have a very large number of macros</li>
<li>When you have macros that need to be undefined (the GUI here can
only define them)</li>
<li>When the code is using embedded 'C' on Sybase: contrary to Oracle,
the Sybase embedded 'C' preprocessor adds some code at the beginning of
the generated files. To compensate for this behaviour, you should add
the following code in the header used for this option: <br />
&#10;<ul>
<li><strong>#include &lt;sybhesql.h&gt;</strong><br />
<strong>SQLCA sqlca;</strong></li>
</ul></li>
</ul>
<p>You should indicate here the "ExternFunctions.h". If you have several
files to "force include", all you need to do is to create an additional
file that will include them (using the #include directive) and set this
file as being "force included".</p>
<ul>
<li>Use the <strong>Browse</strong> button to browse for the file you
want to include. This file will contain the macros that you want the
analyzer to take into account and must have the same structure as any
C/C++ file (in other words, this file can contain "#define" orders to
define macros, "#include" orders to include files, but also declare
functions).</li>
<li>It is possible to enter a simple filename (for example test.h)
without an absolute path. In this case, the analyzer will search for the
file as follows:
<ul>
<li>in the associated Source Repository</li>
<li>then in any defined <strong>Include Path</strong> (defined in
<strong>this section</strong> or in an <strong>Environment
Profile</strong> defined via the <strong>IDE used to develop the
application</strong> option above or via a <strong>Custom Environment
Profile</strong> defined in the <strong>Custom Environment
Profiles</strong> section below).</li>
</ul></li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Define the Include Paths used by the
Analysis Unit</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>This option enables you to add your include paths:</p>
<div class="table-wrap">
<table class="wrapped confluenceTable">
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Adding a path</strong></td>
<td class="confluenceTd"><ol>
<li>Click the button. A new path called "Folder" will be displayed in
the list.</li>
<li>To define the path (using the Browse option) and whether it is
<strong>recursive</strong> or not, select the "Folder" item and use the
relevant fields directly below the list.</li>
<li><p>The <strong>Recursive</strong> option indicates whether any
sub-folders and the files they contain that are located in the chosen
path will be included for resolution purposes (TRUE or FALSE). By
default all paths that you add to the list will be flagged as FALSE -
i.e. their sub-folders and any paths they contain will not be included.
Choose whether the path is recursive as appropriate.</p></li>
</ol>
<p><strong>Notes</strong></p>
<ul>
<li>Please note that CAST only recommends selecting the
<strong>Recursive</strong> option if you know exactly what you want to
achieve. In the vast majority of situations, you should instead define
each include path exactly as it is defined in the C++ project
itself.<br />
<br />
For example, if your C++ project uses the "comp/include" include path,
and your sources reference the file "stat.h" through:<br />
<br />
<em>#include &lt;sys/stat.h&gt; // comp/include/sys/stat.h
intended</em><br />
<br />
just add the path "comp/include" here, like in your C++ project, without
selecting the <strong>Recursive</strong> option.<br />
<br />
<strong>Recursive</strong> is only useful if you really want to list all
subdirectories of a directory, and spare some typing. This is generally
not the case.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Removing a path</strong></td>
<td class="confluenceTd"><p>To remove a path that you do not want to
include in the analysis:</p>
<ol>
<li>Select the path that you want to remove from the list by
left-clicking with the mouse.</li>
<li>Click the <strong>Remove</strong> button to clear the selected
item(s). The <strong>Delete</strong> key and the right-click short cut
menu can also be used. You can remove multiple items by selecting the
items in the list and then clicking the <strong>Remove</strong>
button.</li>
</ol></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Editing an existing path</strong></td>
<td class="confluenceTd"><p>If you want to edit an existing path:</p>
<ul>
<li>Select the item in the list and use the fields directly below the
list to make your changes.</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Up and Down buttons</strong></td>
<td class="confluenceTd">If you have two or more paths, then you can use
the <strong>Up</strong> and <strong>Down</strong> arrow buttons to
<strong>re-order</strong> the paths. This is <strong>important</strong>
as the order they appear in the list is the order in which the paths are
dealt with during the analysis process. To move a path down one level,
select the path, then click <strong>Down</strong>; to move it up one
level, click <strong>Up</strong>.</td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>Notes</strong>
<ul>
<li>Please note that you can also use the CAST environment variable
<strong>$(CastCommonDir)</strong> when adding an include path. This
variable corresponds to the CAST installation folder
"<strong>%CommonProgramFiles%\CAST\CAST</strong>". When you are using a
non-English version of Windows, this variable will be localized.</li>
<li>Note that it is also possible to define <strong>persistent Include
Paths</strong> via a custom Environment Profile (see <strong>Custom
Environment Profiles</strong> section below).</li>
<li>Note that include paths configured in the current <strong>Includes
section</strong> will be processed BEFORE any definitions made in the
Environment Profiles associated to the <strong>IDE used to develop the
Analysis Unit</strong> option, and to any <strong>Custom Environment
Profiles</strong> (see <strong>Custom Environment Profiles</strong>
section) you have added.</li>
</ul></td>
</tr>
</tbody>
</table>
</div>
<p><em>Defining include paths for Microsoft compilers</em></p>
<p>Usually, if the Microsoft C++ compiler is installed on the analysis
machine, you just need to select the default "Microsoft VCxxxx"
Environment Profile (via the <strong>IDE used to develop the Analysis
Unit</strong> option above). In some cases, you may also need to provide
your include list yourself. The following rules should be followed:</p>
<ol>
<li>Do not add more directories then required</li>
<li>Do not select the "recursive" option for these paths</li>
<li>Do not select a path such as <strong>C:\Program Files\Microsoft
Visual Studio 10.0\VC\crt\src</strong> (this typically contains files
that are reserved for Microsoft internal use and debugger)</li>
</ol>
<p>In any of the three cases above, you will get an error message during
the analysis such as:</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<pre class="text"
data-syntaxhighlighter-params="brush: text; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: text; gutter: false; theme: Confluence"><code>#error directive encountered &#39;Use of C runtime library internal header file
... (file &#39;C:\PROGRAM FILES\MICROSOFT[...]\VC\CRT\SRC\CRTDEFS.H&quot;)&#39;</code></pre>
</div>
</div>
<p>Include paths for Microsoft compilers should resemble the
following:</p>
<p>For Microsoft VC 9.0 (2008) and higher:</p>
<ul>
<li>C:\Program Files\Microsoft Visual Studio 10.0\VC\include</li>
<li>C:\Program Files\Microsoft Visual Studio 10.0\VC\atlmfc\include</li>
<li>C:\Program Files\Microsoft SDKs\Windows\vxx.xx\include</li>
</ul>
<p>(the third one is for &lt;windows.h&gt; etc., the exact value for
xx.xx depends on your exact installation)</p>
<p>For older compilers:</p>
<ul>
<li>C:\Program Files\Microsoft Visual Studio 8\VC\include</li>
<li>C:\Program Files\Microsoft Visual Studio 8\VC\atlmfc\include</li>
<li>C:\Program Files\Microsoft Visual Studio
8\VC\PlatformSDKinclude</li>
</ul>
<p>For even older MS Dev 6 (VC98):</p>
<ul>
<li>C:\Program Files\Microsoft Visual Studio\VC98\Include</li>
<li>C:\Program Files\Microsoft Visual Studio\VC98\MFC\Include</li>
<li>C:\Program Files\Microsoft Visual Studio\VC98\atl\Include</li>
</ul>
</div></td>
</tr>
</tbody>
</table>

##### Macros

Define the macros used by the application/analysis unit

Use this option to define any macros that are present in your source
code and that need to be taken into account during the analysis.

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Adding a macro</strong></td>
<td class="confluenceTd"><ol>
<li>Click the button. A new macro called "MY_MACRO" will be displayed in
the list.</li>
<li>Define the macro's name and value in the fields that are displayed
below the macro list.</li>
</ol></td>
</tr>
<tr class="even">
<td class="confluenceTd">Removing a macro</td>
<td class="confluenceTd"><p>To remove a macro that you do not want to
include in the analysis:</p>
<ol>
<li>Select the macro that you want to remove from the list by
left-clicking with the mouse.</li>
<li>Click the <strong>Remove</strong> button to clear the selected
item(s). The <strong>Delete</strong> key and the right-click short cut
menu can also be used. You can remove multiple items by selecting the
items in the list and then clicking the <strong>Remove</strong>
button.</li>
</ol></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Editing a macro</p></td>
<td class="confluenceTd"><p>If you want to edit an existing macro:</p>
<ul>
<li>Select the item and edit the name and value as necessary in the
fields that are displayed below the macro list</li>
<li>You can also double click the macro to right click it and select
<strong>Edit</strong> from the shortcut menu to edit the name and values
in a separate editor window.</li>
</ul></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>Notes</strong>
<ul>
<li>Note that it is also possible to define <strong>persistent
Macros</strong> via a custom Environment Profile (see <strong>Custom
Environment Profiles</strong> - only in the Application editor (Analysis
tab) or the Analysis Unit editor)</li>
<li>Note that macro definitions configured here will be processed AFTER
any definitions made in the Environment Profiles associated to the
<strong>IDE used to develop the Application/Analysis Unit</strong>
option (see <strong>IDE</strong> section above), and to any
<strong>custom Environment Profiles</strong> (see <strong>Custom
Environment Profiles</strong> - only in the Application editor (Analysis
tab) or the Analysis Unit editor) you have added.</li>
<li>If you include a file (see below) containing a list of macros, this
file is included in each main file that is analyzed. In other words, and
for illustrative purposes only, it would be as if each file selected for
analysis started with "#include "autoinclude.h" " (where autoinclude.h
is the name of the file selected in the <strong>Includes</strong>
section below). These macros are processed AFTER the macro definitions
made in this section.</li>
<li>Most, if not all applications developed with Microsoft Visual C++
rely on a set of (standard or not) header files provided with the
compiler (stdlib.h, windows.h...). These headers themselves heavily rely
on various macros that are internally defined by the compiler. These
macros are predefined via Environment Profiles that are applied
automatically to the source code following the selections made in the
<strong>IDE used to develop the Application/Analysis Unit</strong>
option (see <strong>IDE</strong> section above), as such there is no
need to explicitly add them.</li>
</ul></td>
</tr>
</tbody>
</table>

##### Precompiled Headers (PCH)

|                        |                                                                                                                                                                                                             |
|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Precompiled header | Enter the name of a header file, that the majority of the source code files in the project include first, and that self includes many other library files. Typically "stdafx.h" on most Microsoft projects. |
| PCH generator      | A source file that includes the Precompiled header file (as referenced above), and that should be analyzed first. Typically "stdafx.cpp" on most Microsoft projects.                                        |
| Disable use of PCH | Selecting this option will cause Precompiled headers to be ignored. This is generally not recommended, as PCHs improve analysis speed.                                                                      |

##### Custom Environment Profiles

This option enables you to select C/C++ based Environment Profiles that
you have created yourself (as oppose to the Environment Profiles that
are pre-defined by CAST and applied when the IDE used to develop the
Application/Analysis Unit option is set)

Environment Profiles are a set of predefined configuration settings that
can be included in an analysis. An Environment Profile can be
particularly useful where you have several applications that rely on the
same specific settings. By creating an Environment Profile that defines
these settings, you then simply include the Environment Profile in the
analysis. When the analysis is run, the settings in the Environment
Profile are taken into account.

For C/C++ technologies, you can define:

-   Include Paths
-   Macros

See [C and Cpp - Environment
Profiles](C_and_Cpp_-_Environment_Profiles).

##### Parsing

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>&gt;&gt; allowed to close nested
template list</strong></td>
<td class="confluenceTd">Many compilers allow to close two nested
template lists with &gt;&gt; (instead of "&gt;(blank)&gt;" ). There is
no reason to deactivate this option, unless "x&gt;&gt;y" is used in any
template expression.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Accept Trigraphs</strong></td>
<td class="confluenceTd">Select this option only if your code contains
trigraphs. The analyzer will then interpret normalized character
sequences as a single character - for example <strong>??&lt;</strong>
will be interpreted as <strong>{</strong>.
<p>Note also that this option will also force the analyzer to accept
digraphs.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Standard scope of "for"
loops</strong></td>
<td class="confluenceTd">Variables declared inside "for" loops are not
visible outside the loop.
<p>Activating this option could generate messages about unknown
variables on some pre-ISO compilers, like Microsoft VC6.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>Standard processing of
"typename"</strong></td>
<td class="confluenceTd">According to the standard, "T::X", not preceded
by "typename" (where T is a template parameter) should always be
interpreted as an expression.
<p>Activating this option is recommended on most compilers, except
Microsoft compilers.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>Detect unknown compilation
directives</strong></td>
<td class="confluenceTd">When this option is activated, the analyzer
tries to detect compilation directives that have not been defined as
macros (e.g. WINAPI and the like). Since this option is not 100%
reliable, it is recommended to disable it once the analysis has been
properly configured.</td>
</tr>
</tbody>
</table>

#### Production

*Click to enlarge*

*![](235477465.jpg)*

##### Process Settings

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><strong>Number of Instances</strong></td>
<td class="confluenceTd">This option is only relevant when the
"<strong>Legacy analyzer</strong>" option is active. It allows you to
<strong>limit the number of objects held in memory</strong> before they
are committed to disk during the save process of an analysis.
<p>Please contact CAST Support before modifying this option.</p></td>
</tr>
</tbody>
</table>

##### Dynamic Links Rules

See the CAST Management Studio help for more information about this
global option.