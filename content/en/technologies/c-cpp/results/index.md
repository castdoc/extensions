---
title: "Analysis results"
linkTitle: "Analysis results"
type: "docs"
weight: 8
---

## Objects

The following specific objects are detected by the analyzer and can be
displayed in CAST Enlighten:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477481.png" draggable="false"
data-image-src="235477481.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477481"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_CLASS.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Class</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477482.png" draggable="false"
data-image-src="235477482.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477482"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_CLASSFOLDER.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Folder for Classes, Unions and
Structures<br />
Dependencies Folder<br />
Globals Folder<br />
File Scope Folder</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477483.png" draggable="false"
data-image-src="235477483.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477483"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_CONSTRUCTOR.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Constructor</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477485.png" draggable="false"
data-image-src="235477485.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477485"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_DESTRUCTOR.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Destructor</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477484.png" draggable="false"
data-image-src="235477484.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477484"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_ENUM.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Enum</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477486.png" draggable="false"
data-image-src="235477486.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477486"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_ENUMITEM.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Enum Item</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477487.png" draggable="false"
data-image-src="235477487.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477487"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_FILE.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>File</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477488.png" draggable="false"
data-image-src="235477488.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477488"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_FUNCTION.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Function</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477489.png" draggable="false"
data-image-src="235477489.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477489"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_GLOBALVARIABLE.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Global Variable</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477490.png" draggable="false"
data-image-src="235477490.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477490"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_MACRO.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Macro</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477493.png" draggable="false"
data-image-src="235477493.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477493"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_MEMBER.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Member</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477491.png" draggable="false"
data-image-src="235477491.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477491"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_METHOD.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Method</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477492.png" draggable="false"
data-image-src="235477492.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477492"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_CLASSPROJECT.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Project</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477494.png" draggable="false"
data-image-src="235477494.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477494"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_STRUCT.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Struct</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<img src="235477496.png" draggable="false"
data-image-src="235477496.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477496"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_CLASS.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" />
</div></td>
<td class="confluenceTd"><p>Class Template</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477496.png" draggable="false"
data-image-src="235477496.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477496"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_CLASS.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Class Template Specialization</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477495.png" draggable="false"
data-image-src="235477495.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477495"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_CTOR.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Constructor Template</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477497.png" draggable="false"
data-image-src="235477497.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477497"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_CTOR_INSTANCE.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Constructor Template Specialization</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477499.png" draggable="false"
data-image-src="235477499.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477499"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_FUNCTION.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Function Template</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477500.png" draggable="false"
data-image-src="235477500.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477500"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_FUNCTION_INSTANCE.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Function Template Specialization</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477498.png" draggable="false"
data-image-src="235477498.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477498"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_METHOD.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Method Template</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477501.png" draggable="false"
data-image-src="235477501.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477501"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_METHOD_INSTANCE.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Method Template Specialization</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477503.png" draggable="false"
data-image-src="235477503.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477503"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_SUBSET.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Subset</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477502.png" draggable="false"
data-image-src="235477502.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477502"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TEMPLATE_PARAM.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Template Parameter</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477504.png" draggable="false"
data-image-src="235477504.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477504"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_TYPEDEF.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Typedef</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477506.png" draggable="false"
data-image-src="235477506.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477506"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_UNION.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>Union</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477505.png" draggable="false"
data-image-src="235477505.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477505"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_C_CLR_Event.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>CLR Event (Managed C++)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477507.png" draggable="false"
data-image-src="235477507.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477507"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_C_CLR_EventAddon.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>CLR EventAddon (Managed C++)</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477508.png" draggable="false"
data-image-src="235477508.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477508"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_C_CLR_EventRaise.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>CLR EventRaise (Managed C++)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477509.png" draggable="false"
data-image-src="235477509.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477509"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_C_CLR_EventRemoveon.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>CLR EventRemoveOn (Managed C++)</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477510.png" draggable="false"
data-image-src="235477510.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477510"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_C_CLR_Property.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>CLR Property (Managed C++)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477511.png" draggable="false"
data-image-src="235477511.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477511"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_C_CLR_PropertyGetter.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>CLR PropertyGetter (Managed C++)</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="235477512.png" draggable="false"
data-image-src="235477512.png"
data-unresolved-comment-count="0" data-linked-resource-id="235477512"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_C_CLR_PropertySetter.PNG"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" /></p>
</div></td>
<td class="confluenceTd"><p>CLR PropertySetter (Managed C++)</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="300646407.png" draggable="false"
data-image-src="300646407.png"
data-unresolved-comment-count="0" data-linked-resource-id="300646407"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="SQLQuery32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="16" /></p>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>SQL Query</p>
</div></td>
</tr>
</tbody>
</table>

## Links

The following is a non-exhaustive list of links that may be identified
between objects saved in the CAST Analysis Service:

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td colspan="2" class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>Link Type</strong></td>
<td class="highlight-grey confluenceTd"
data-highlight-colour="grey"><strong>When is this type of link
used?</strong></td>
</tr>
<tr class="even">
<td rowspan="5" class="confluenceTd"><strong>ACCESS</strong></td>
<td class="confluenceTd"><strong>READ</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Specifies that an object reads a value of another object. For example
if a function reads a global variable:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>int g_nVar1;
void f()
{
int nVar1 = g_nVar1;
}</code></pre>
</div>
</div>
<p>C++ Analyzer will generate this Access (Read) link:</p>
<p><img src="235477513.gif" draggable="false"
data-image-src="235477513.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477513"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="accessread.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="171"
height="176" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>WRITE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Specifies that an object modifies another one. For example if a
function modifies a global variable:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>int g_nVar1;
void f()
{
g_nVar1 = 1;
}</code></pre>
</div>
</div>
<p>C++ Analyzer will generate this Access (Write) link:</p>
<p><img src="235477514.gif" draggable="false"
data-image-src="235477514.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477514"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="accesswrite.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="153"
height="161" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>EXEC</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Specifies that an object executes another object (method or
function). For example if a function calls another function:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>void g()
{
f();
}</code></pre>
</div>
</div>
<p>C++ Analyzer will generate this Access (Exec) link:</p>
<p><img src="235477515.gif" draggable="false"
data-image-src="235477515.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477515"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="accessexec.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="125"
height="152" /></p>
<div>
<div>
Please note that, as a convention, an EXEC link is also used when the
definition or declaration of an object invokes a macro. The links is
created from the object to the macro.
</div>
</div>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>MEMBER</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Specifies that an object accesses a member of another object. For
example if a function "g()" accesses a global var "g_pVar" and to one of
its members:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>struct { int m_n1; }* g_Var;
void g()
{
g_pVar-&gt;m_n1 = 1;
}</code></pre>
</div>
</div>
<p>C++ Analyzer will generate this Access (Member) link:</p>
<p><img src="235477516.gif" draggable="false"
data-image-src="235477516.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477516"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="accessmember.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="187"
height="162" /></p>
</div></td>
</tr>
<tr class="even">
<td class="confluenceTd"><strong>ARRAY</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Specifies that an object accesses another using square brackets ([
]). For example if a function "g()" accesses a character's array at its
first position:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>char g_lpBuffer[256];
void g()
{
g_lpBuffer[0] = &#39;\0&#39;;
}</code></pre>
</div>
</div>
<p>C++ Analyzer will generate this Access(Array) link (with the
<em>write</em> property also):</p>
<p><img src="235477517.gif" draggable="false"
data-image-src="235477517.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477517"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="accessarray.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="180"
height="160" /></p>
</div></td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>BELONG</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Represents a parent link between two objects. If "Sample.h" contains
the following statement:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>class C1
{
public: int foo() {}
};</code></pre>
</div>
</div>
<p>C++ Analyzer will generate these Belong links:</p>
<p><img src="235477518.gif" draggable="false"
data-image-src="235477518.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477518"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="belong.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="305"
height="141" /></p>
<div>
<div>
<ul>
<li>An object always has a "logical" Belong link to its parent
(function, class, global variable folder etc.). Furthermore, when the
object is defined at the main file level (global variable or function,
non-nested class, class method defined at file level), it has a second
"physical" link to this implementation file.</li>
<li>An object never has more than two parents.</li>
</ul>
</div>
</div>
</div></td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"><strong>CONTAIN</strong></td>
<td class="confluenceTd"><strong>DECLARE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
This link type occurs between a file and a Global object when there is
also a declaration (forward declaration or external declaration) in
addition to the definition itself. For example, between a file and a:
<ul>
<li>Class</li>
<li>Structure</li>
<li>Enum</li>
<li>Union</li>
<li>Function or method</li>
<li>Variable or class member</li>
</ul>
<p>This link can be seen in the example below (Zc):</p>
<p><img src="235477519.gif" draggable="false"
data-image-src="235477519.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477519"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="containdec.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="292"
height="226" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>DEFINE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
This link type occurs when a method is defined outside the file
containing the class definition. The link is created from the file
containing the method definition to the class. For example, using the
code below:<br />
&#10;<p><strong>glob.h</strong></p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>extern int theGlobalVariable;
class theClass
{
void method();
};</code></pre>
</div>
</div>
<p><strong>glob.cpp</strong></p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>/* header comment */
##include &quot;glob.h&quot;
int theGlobalVariable = 3;
void theClass::method()
{ if(2+2==4);
}</code></pre>
</div>
</div>
<p>C++ Analyzer will generate the following link (Zf):</p>
<p><img src="235477520.gif" draggable="false"
data-image-src="235477520.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477520"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="containdef.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="398"
height="266" /></p>
</div></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>FRIEND</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
Specifies that a class or a function is a friend of a class.
<p>For example if the class "C2" is declared as friend of a new "C3"
class:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>{
//...
friend class C2;
};</code></pre>
</div>
</div>
<p>C++ Analyzer will generate this Friend link:</p>
<p><img src="235477521.gif" draggable="false"
data-image-src="235477521.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477521"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="friend.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="120"
height="158" /></p>
</div></td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>INCLUDE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Represents an inclusion link between two files. If the "Sample.cpp"
file contains the following statement:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>#include &quot;Sample.h&quot;</code></pre>
</div>
</div>
<p>C++ Analyzer will generate this Include link:</p>
<p><img src="235477522.gif" draggable="false"
data-image-src="235477522.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477522"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="include.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="113"
height="139" /></p>
</div></td>
</tr>
<tr class="even">
<td rowspan="2" class="confluenceTd"><strong>INHERIT</strong></td>
<td class="confluenceTd"><strong>-</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Represents an inheritance between two classes or structures. If we
define a new class "C2" inheriting from C1 as in following
statement:</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>class C2 : public C1
{ int foo() {}
};</code></pre>
</div>
</div>
<p>C++ Analyzer will generate following Inherit link:</p>
<p><img src="235477523.gif" draggable="false"
data-image-src="235477523.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477523"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="inherit.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="120"
height="143" /></p>
</div></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><strong>OVERRIDE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
Specifies that a method redefines another method from an inherited
class. For example using the previous sample, "C2::foo()" redefines
"C1::foo()". So C++ Analyzer will generate this link:<br />
&#10;<p><img src="235477525.gif" draggable="false"
data-image-src="235477525.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477525"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="override.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="197"
height="129" /></p>
</div></td>
</tr>
<tr class="even">
<td colspan="2" class="confluenceTd"><strong>RELY ON</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Specifies that an object uses an existing type. If we define a new
variable of type "C1" in "Sample.cpp":</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>C1* g_pVar1;</code></pre>
</div>
</div>
<p>C++ Analyzer will generate following Rely On link :</p>
<p><img src="235477524.gif" draggable="false"
data-image-src="235477524.gif"
data-unresolved-comment-count="0" data-linked-resource-id="235477524"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="relyon.gif"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/gif"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" width="155"
height="152" /></p>
</div></td>
</tr>
<tr class="odd">
<td colspan="2" class="confluenceTd"><strong>USE</strong></td>
<td class="confluenceTd"><div class="content-wrapper">
<p>Represents a link between a C/C++ object and a database object. For
example, if we define a function "f_ExecSql" containing a ProC statement
referencing the "Authors" table :</p>
<div class="preformatted panel" style="border-width: 1px;">
<div class="preformattedContent panelContent">
<pre><code>void f_ExecSql()
{
EXEC SQL EXECUTE select * from authors where au_id = 0;
}</code></pre>
</div>
</div>
<p>C++ Analyzer will generate a SQL Query object plus links:</p>
<p><img src="300646409.png" draggable="false"
data-image-src="300646409.png"
data-unresolved-comment-count="0" data-linked-resource-id="300646409"
data-linked-resource-version="2" data-linked-resource-type="attachment"
data-linked-resource-default-alias="C_PP_SQL.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="235477480"
data-linked-resource-container-version="5" height="109" /></p>
<p><br />
</p>
<p>The following ESQL tags are also supported (non exhaustive list):</p>
<ul>
<li>EXEC SQL ... INSERT</li>
<li>EXEC SQL ... UPDATE</li>
<li>EXEC SQL ... DELETE</li>
<li>EXEC SQL ... SELECT</li>
<li>EXEC SQL ... CALL</li>
<li>EXEC SQL ... DECLARE ... CURSOR FOR</li>
<li>EXEC SQL ... EXECUTE ... END-EXEC</li>
</ul>
<p>In ESQL tags, links to the following objects will be found (non
exhaustive list):</p>
<ul>
<li>Tables</li>
<li>Views</li>
<li>Synonyms</li>
<li>Stored Procedures and Functions</li>
<li>Package Procedures and Functions (Oracle)</li>
</ul>
</div></td>
</tr>
</tbody>
</table>
