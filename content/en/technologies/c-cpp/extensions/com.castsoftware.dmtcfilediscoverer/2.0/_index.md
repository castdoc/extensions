---
title: "C and Cpp File Discoverer - 2.0"
linkTitle: "2.0"
type: "docs"
no_list: true
---

***

{{% alert color="info"%}}CAST recommends using the [CPP Compilation Database Discoverer](../../com.castsoftware.dmtcppcompilationdatabasediscoverer/) extension instead of the C and C++ File Discoverer where possible.
The [CPP Compilation Database Discoverer](../../com.castsoftware.dmtcppcompilationdatabasediscoverer/) extension detects
projects based on the presence of specific build compilation output
files and their content. It can be used in conjunction with [iOS - Objective-C](../../../../mobile/ios/com.castsoftware.cfamily/)
and the C/C++ Analyzer embedded in **CAST Imaging Core**.{{% /alert %}}

## Extension ID

com.castsoftware.dmtcfilediscoverer

## What's new?

See [Release Notes](rn).

## Extension description

This discoverer detects projects based on the presence of C or C++ files according to a list of extensions declared for these languages.

### In what situation should you install this extension?

This extension should be used when you don't use Microsoft C/C++
projects to build your C/C++ application. Project files such
as `makefile` are not supported by the discoverers provided "out of
the box" in CAST. Therefore, this extension acts as a kind of "catch-all" to ensure that all C and C++ files will be packaged for analysis.

### Technical information

What does the extension identify - i.e. what does it consider as a
"project" and therefore an Analysis Unit?

Only one project is created per package when C or C++ files are detected (i.e. one project is equal to one Analysis Unit) - the following are
supported:

- `*.c`, `*.pc`, `*.ppc`
- `*.cpp`, `*.cc`, `*.cxx`

This project will use the name of the top-folder that contains C or C++ files. As such, given the following folder structure (for example):

```text
root
-folder1
--folder2
---src
----folder3
-----file1.c
----folder4
-----file2.c
----folder5
-----file3.c
-----file4.cpp
---include
----file1.h
----file2.h
```

Using this extension with the above folder structure will yield a
project called `src` with a root path `folder1/folder2/src`. This
project references the folder corresponding to the root in order to
include all C/C++ files. If header files are found outside the
top-folder, they are not added in the references of the project.

{{% alert color="info"%}}Note that even if the extension is activated, NO project will be created when at least one Microsoft C/C++ `.vcproj` or `.vcxproj` project definition file is identified in any location under the root path (including recursive sub-folders).{{% /alert %}}

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |
