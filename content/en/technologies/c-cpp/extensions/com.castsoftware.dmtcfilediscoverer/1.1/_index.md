---
title: "C and Cpp File Discoverer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

>CAST recommends using the [CPP Compilation Database Discoverer](../../com.castsoftware.dmtcppcompilationdatabasediscoverer/) extension instead of the C and C++ File Discoverer where possible.
The [CPP Compilation Database Discoverer](../../com.castsoftware.dmtcppcompilationdatabasediscoverer/) extension detects
projects based on the presence of specific build compilation output
files and their content. It can be used in conjunction with:
>-   [iOS - Objective-C](../../../../mobile/ios/com.castsoftware.cfamily/)
>-   C/C++ Analyzer embedded in CAST Imaging Core

## Extension ID

com.castsoftware.dmtcfilediscoverer

## What's new?

Please see: [C and Cpp File Discoverer - 1.1 - Release Notes](rn) for more
information.

## Extension description

This discoverer detects projects based on the presence of C or C++ files
according to a list of extensions declared for these languages.

### In what situation should you install this extension?

This extension should be used when you don't use Microsoft C/C++
projects to build your C/C++ application. Project files such
as makefile are not supported by the discoverers provided "out of
the box" in the CAST Delivery Manager Tool. Therefore, this extension
acts as a kind of "catch-all" to ensure that all C and C++ files will be
packaged for analysis.

When the extension is installed, you can choose to activate it (or not)
in each package you create in the CAST Delivery Manager Tool. To do so,
click the Package Configuration tab and define the type of projects
to discover in the package (refer to the Packaging your source code
with the CAST Delivery Manager Tool paragraph below).

### Technical information

What does the extension identify - i.e. what does it consider as a
"project" and therefore an Analysis Unit in the CAST Management Studio?

Only one project is created per package when C or C++ files are detected
(i.e. one project is equal to one Analysis Unit) - the following are
supported:

-   \*.c, \*.pc, \*.ppc
-   \*.cpp, \*.cc, \*.cxx

This project will use the name of the top-folder that contains C or C++
files. As such, given the following folder structure (for example):

    root
    -folder1
    --folder2
    ---src
    ----folder3
    -----file1.c
    ----folder4
    -----file2.c
    ----folder5
    -----file3.c
    -----file4.cpp
    ---include
    ----file1.h
    ----file2.h

Using this extension with the above folder structure will yield a
project called "src" with a root path folder1/folder2/src. This
project references the folder corresponding to the root in order to
include all C/C++ files. If header files are found outside the
top-folder, they are not added in the references of the project.

Note that even if the extension is activated, NO project will be created
when at least one Microsoft C/C++ .vcproj or .vcxproj project definition
file is identified in any location under the root path (including
recursive sub-folders).

## New in this release

The C and C++ File Discoverer 1.1.x brings the following update over
previous releases:

-   File extensions that are taken into account by the C and C++ File
    Discoverer are now taken directly from the CAST AIP language
    definition, rather than being hard coded in the discoverer itself.
-   The following file extensions are now taken into account:
    -   C files: \*.c, \*.pc, \*.ppc
    -   C++ files: \*.cpp, \*.cc, \*.cxx
-   NO project will be created when at least one Microsoft C/C++ .vcproj
    or .vcxproj project definition file definition is identified in any
    location under the root path (including recursive sub-folders).

## AIP Core compatibility

This extension is compatible with:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><div>
AIP Core release
</div></th>
<th class="confluenceTh"><div>
Supported
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">8.3.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
</tbody>
</table>

## Extension interface

The following screen shots show the differences in the product when the
extension is installed:

-   in the CAST Delivery Manager Tool, in the Package configuration
    tab, the interface contains a new item in the list of "Project
    types discovered":

![](../images/235477547.jpg)

-   in the CAST Delivery Manager Tool, in the Package content tab,
    when the package contains some C/C++ files, the interface will
    display a new item in the list of "Project types discovered":

![](../images/235477548.png)

-   in the CAST Delivery Manager Tool, in the Package configuration
    tab, you can define the various exclusion of projects with the
    "Filtering rules" and "Project exclusion" lists. However,
    since only one project is created, it's better to unselect the
    project type discovered rather than create exclusion lists.

Note: please ensure that you exclude projects that correspond to the
same source code. If you have some Visual C++ discovered projects inside
the same package, CAST recommends deactivating this discoverer.

-   in the CAST Management Studio, when the delivery is accepted and set
    as current version, the package will contain Analysis Units
    corresponding to these projects:

![](../images/235477549.png)

-    In the CAST Management Studio, the Analysis Units will contain:
    -    the Source Settings discovered by the extension:  
        -   Sources: path corresponding to the top-folder

 ![](../images/235477550.png)

-   -   -   the Analysis Settings are not discovered by the
            extension - you must define them at application level or at
            analysis unit level

 ![](../images/235477550.png)

## Packaging your source code with the CAST Delivery Manager Tool

When you use the CAST Delivery Manager Tool to package the source code,
please ensure that you tick the following option in the CAST Delivery
Manager Tool to activate the extension:

Note that:

-   this option will be disabled (unticked) in all pre-existing
    packages - you need to check it before starting the packaging
    process otherwise it will be ignored.
-   for new packages created after the extension's installation, the
    option will be enabled by default

![](../images/235477547.jpg)
