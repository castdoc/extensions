---
title: "Release Notes - 1.1"
linkTitle: "Release Notes"
type: "docs"
---

## 1.1.2-funcrel

### Other Updates

| Details |
| ------- |
| An improvement has been implemented to ensure that when C and C++ files are both present in the delivered source code that source code references are created on both language types. This information is used by the Version report which lists the number of files per language. |
