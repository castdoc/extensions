---
title: "C and Cpp Analyzer - 2.3"
linkTitle: "2.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.cpp

## What's new

See [Release Notes](rn/) for more information.

## Description

This extension provides support for C/C++/Pro\*C.

### In what situation should you install this extension?

If your application contains C/C++/Pro\*C source code and you want
to view these object types and their links with other objects, then you
should install this extension.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :x: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Detailed technology support

Language support

-   C++ language is supported up
    to [C++17](https://en.wikipedia.org/wiki/C%2B%2B17)
-   C language up to C99, C11 and C17

Language extensions supported

-   Oracle Pro\*C and IBM DB2 SQC extensions (EXEC SQL commands
    embedded in C code)
-   some Microsoft C++ Component Extensions:
    -   interface
    -   property
    -   for each

Available project discoverers for the CAST Delivery Manager Tool

-   Visual C++ 2003
-   Visual C++ 2005
-   Visual C++ 2008
-   Visual C++ 2010
-   Visual C++ 2012
-   Visual C++ for versions \> 2012  The Analysis Unit generated for
    this project is mapped to a Visual C++ 2012 environment profile

Visual C++ 2013, 2015, 2017 and 2019 projects, will be discovered as
a Visual C++ 2012 project (i.e. the Analysis Unit generated for this
project is mapped to a Visual C++ 2012 environment profile). You can
therefore:

-   either change the analysis options in the CAST Management Studio so
    that:
    -   "IDE used for this Analysis Unit" is set to "Not
        Specified"
    -   "STL Support" is set to "Cast emulation"
-   or have Visual C++ 2012 installed on the analysis machine and
    analyse the code as a Visual C++ 2012 project.

-   See [Technical notes](../../../notes/) for a list of additional tips and information.
-   See [Project discovery](../../../discovery/) for more information about how discovery functions.

## Required third-party software

To successfully deliver and analyze C / C++ code, the following
third-party software is required:

| Install on workstation running CAST Console                                                                                          | Install on workstation running Node services                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|--------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   Nothing required<br><br>Note that no project discoverer exists for Visual Studio 6.0, however, source code can still be packaged.    | Software required:  If you intend to use the CAST - VC++ XXX - Mandatory Part Environment Profile (usage is recommended), then you MUST install:<br><br>- All include files of third party libraries that are used.<br>- The appropriate IDE depending on source code to be analyzed:<br><br>- Visual Studio 6.0<br> - Visual Studio .NET 2003<br> - Visual Studio 2005<br> - Visual Studio 2008<br> - Visual Studio 2010<br> - Visual Studio 2012<br><br> Note that<br> - if you do not install the appropriate IDE for your source code, then the analysis will fail.<br> - the above information does not apply to other source code (C/C++ compiler from vendors other than Microsoft).    |

## Download and installation instructions

CAST Console will automatically download this extension when C/C++/Pro\*C source code is delivered.

## Packaging, delivering and analyzing your source code

Please see: [C and Cpp - Prepare and deliver the source
code](C_and_Cpp_-_Prepare_and_deliver_the_source_code) and all child
pages:

-   [Project discovery](../../../discovery/)
-   [C and Cpp - Analysis configuration](../../../analysis-config/)
-   [C and Cpp - Analysis messages](../../../logs/)

## What analysis results can you expect?

Please see: [C and Cpp - Analysis results](../../../results/).

## Rules

Please see: [C and Cpp - Structural rules](../../../rules/).
