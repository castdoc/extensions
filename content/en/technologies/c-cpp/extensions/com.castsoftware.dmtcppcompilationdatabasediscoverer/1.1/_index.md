---
title: "CPP Compilation Database Discoverer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

## Extension ID

com.castsoftware.dmtcppcompilationdatabasediscoverer

## Extension description

This discoverer detects projects based on the presence of specific build
compilation output files and their content. It can be used in
conjunction with:

-   [iOS Objective-C](../../../../mobile/ios/com.castsoftware.cfamily/)
-   C/C++ Analyzer embedded in CAST Imaging Core / [C and Cpp Analyzer extension](../../com.castsoftware.cpp/)

### In what situation should you install this extension?

If your C/C++ source code is designed to compile on Linux, you
can use a compilation logging tool called [Scan Build](https://github.com/CAST-projects/scan-build/) (freely
available from CAST - instructions included) while compiling the source
code to output two
files compile_commands.json and compile_config.json that will contain
all the information necessary to compile your source code. These two
Scan Build files should be delivered as input with your source code in
the CAST Delivery Manager Tool: as long as you have also installed
the CPP Compilation Database Discoverer, then the Scan Build output
files will be taken into account when you package the source code and
projects will be created based on what is found in the Scan Build files
(compile_commands.json and compile_config.json).

## What's new?

### 1.1.0 - funcrel

Macros without a value (e.g.: -DMACRO_NAME instead
of DMACRO_NAME=MACRO_VALUE) are now supported.

### 1.1.0 - beta 3

-   The following specific include paths are now supported:
    -   -isystem
    -   -iquote
    -   -idirafter

## CAST AIP compatibility

This extension is compatible with:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><div>
CAST AIP release
</div></th>
<th class="confluenceTh"><div>
Supported
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">8.3.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd">8.2.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/check.svg" class="emoticon emoticon-tick"
data-emoticon-name="tick" alt="(tick)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">8.1.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/error.svg" class="emoticon emoticon-cross"
data-emoticon-name="cross" alt="(error)" /></td>
</tr>
<tr class="even">
<td class="confluenceTd">8.0.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/error.svg" class="emoticon emoticon-cross"
data-emoticon-name="cross" alt="(error)" /></td>
</tr>
<tr class="odd">
<td class="confluenceTd">7.3.x</td>
<td class="confluenceTd" style="text-align: center;"><img
src="/images/icons/emoticons/error.svg" class="emoticon emoticon-cross"
data-emoticon-name="cross" alt="(error)" /></td>
</tr>
</tbody>
</table>


## Packaging your source code with the CAST Delivery Manager Tool

When you use the CAST Delivery Manager Tool to package the source code,
please ensure that you tick the following option in the CAST Delivery
Manager Tool to activate the extension:

![](../images/235477540.jpg)

Note that:

-   this option will be disabled (unticked) in all pre-existing
    packages - you need to check it before starting the packaging
    process otherwise it will be ignored.

-   for new packages created after the extension's installation, the
    option will be enabled by default

### Packaging results

The use of the discoverer will not result in a specific CPP
Compilation Database Discoverer "project" - instead, based on what has
been delivered in the Scan Build tool output files, projects may be
identified (such as Visual Studio projects), or may not, in which case
you will need to manually create an Analysis Unit for your analysis
requirements.

Note also that:

-   if your source code includes vcproj files and the Visual
    Studio C++ discoverers (embedded in the CAST Delivery Manager
    Tool) are active (ticked) as well as the CPP Compilation Database
    Discoverer, then you may need to carefully check the results of
    the packaging.
-   you may need to create additional source code packages in the
    CAST Delivery Manager Tool to deliver the header folders

### Errors during packaging

#### The directory 'xxx' used in commands inside the compile_config.json file is not matching the selected root 'yyy'

The following error may be visible following a packaging action when
the CPP Compilation Database Discoverer is installed:

``` text
The directory 'xxx' used in commands inside the compile_config.json file is not matching the selected root 'yyy'.
The source code must be packaged in its development location or you should define a value for the Development root folder.
```

##### Explanation

This error occurs if you are packaging the code (i.e. running the CAST
Delivery Manager Tool) on a machine different from the one where the
compilation took place and indicates that the commands found by the CAST
Delivery Manager Tool in the compile_config.json file (output of
the Scan Build tool) do not match the path defined in the Root Folder
field. 

##### Solution

The solution to this error is as follows:

-   Either run the CAST Delivery Manager Tool on the machine on which
    the compilation took place
-   Or, use the Development Root Folder option in the CAST Delivery
    Manager Tool to remap the paths:

![](../images/235477539.jpg)

### Other warnings and errors

#### ERROR invalidRoot

``` java
invalidRoot %DIR%="some/path" %ROOT%="some/path"
```

Typically this error occurs when CAST identifies the path specified by
%DIR% in the source code (i.e. the compilation processes use this path),
whereas the root path (specified by %ROOT%) is the path defined in the
CAST Delivery Manager Tool itself under Root folder: the error is
displayed because the two paths do not match. To resolve the issue,
input the path specified in %DIR% into the Development root folder
field. This will ensure that CAST can resolve the dependencies specified
in the source code.

This error can also occur if you input the wrong path into the
Development root folder.

#### WARNING invalidCommand

``` java
invalidCommand %LINE%="line_number" %FILE%="some/path"
```

Typically this warning occurs when CAST is processing the Scan Build
files (compile_commands.json and compile_config.json) and
indicates that an inconsistency has been discovered in one of these
files. These warnings will not stop the process and can usually be
ignored but deserve some investigation of the specific line numbers
noted in the warning.

#### WARNING noFile

``` java
noFile %PATH%="some_file"
```

Typically this warning occurs when CAST is processing the Scan Build
files (compile_commands.json and compile_config.json) and
indicates that a path to file specified in %PATH has been discovered but
the file cannot be found. These warnings will not stop the process and
can usually be ignored but deserve some investigation.