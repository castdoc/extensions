---
title: "Visual Cpp 2010-2012 Project Discoverer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

## Extension ID

com.castsoftware.dmtdevvisualcplusplus2010discoverer

## What's new ?

See [Visual Cpp 2010-2012 Project Discoverer - 1.1 - Release Notes](rn)
for more information.

## Extension description

This discoverer detects projects based on the presence of Visual Studio
C++ \*.vcxproj files.

### In what situation should you install this extension?

This extension should be used when you are analyzing Visual Studio based
C++ applications. The discoverer will automatically create one
project for every \*.vcxproj file that is delivered for analysis
and one corresponding Analysis Unit will then be created for analysis
purposes.

### Project Exclusion Rules

The discoverer contains a Project Exclusion Rule which is enabled by
default: where an equivalent \*.vcproj file (Visual C++ 2002 - 2008)
is also discovered, the \*.vcxproj file will always be used in
preference. Following the first delivery of an application where the
discoverer was automatically downloaded and installed (Console ≥
2.4), the rule will then be available in Console UI for subsequent
version deliveries:

![](../images/589856909.jpg)

### Technical information

The discoverer is already embedded in AIP Core with the release number
1.0.0 (LTS). This embedded version of the extension will not undergo any
further updates and instead all functional changes/customer bug fixes
will be actioned in the extension.

## AIP Core compatibility

This extension is compatible with:

| AIP Core release |                                         Supported                                          |
|:-----------------|:------------------------------------------------------------------------------------------:|
| 8.3.x            | ![(tick)](/images/icons/emoticons/check.svg) |

## Download and installation instructions

This extension will be downloaded and installed automatically when using
Console ≥ 2.4 and .NET \*.vcxproj files are delivered. In all other
circumstances, the extension will not be automatically installed:
Instead if it is required, the extension should be downloaded and
installed "manually" using the Console interface:

![](../images/636452952.jpg)
