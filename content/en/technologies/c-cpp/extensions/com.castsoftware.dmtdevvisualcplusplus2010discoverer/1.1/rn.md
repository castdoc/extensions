---
title: "Release Notes - 1.1"
linkTitle: "Release Notes"
type: "docs"
---

## 1.1.1

### Resolved Issues

| Customer Ticket Id | Details |
| ------------------ | ------- |
| 41952 | Fixes an issue where an Analysis Unit was correctly created from a .vcxproj file but it did not contain any source code (despite references existing) causing a "missing source code" errors during the analysis. |

### Other Updates

| Details |
| ------- |
| An existing warning message ("Discovered Visual Studio version not recognized: v142") which is displayed when an unsupported Visual Studio version is encountered has been improved: "Discovered Visual Studio version not recognized: %VERSION%, continuing with default version v141". |

## 1.1.0

### Resolved Issues

| Customer Ticket Id | Details |
| ------------------ | ------- |
| 36370 | Fixes an issue where CPP projects are discovered for both .vcproj and .vcxproj files, creating duplicate Analysis Units. |

### Other Updates

| Details |
| ------- |
| As a result of the fix provided for the customer bug 36370, the discoverer has been modified to add an exclusion rule called "Exclude Visual C++ 2002-2008 projects when a Visual C++ 2010-2012 project also exists". This rule is enabled by default and this means that the .vcxproj files will always be used to generate the projects and Analysis Units instead of the .vcproj files when an equivalent .vcproj file exists. |
