---
title: "Log messages"
linkTitle: "Log messages"
type: "docs"
weight: 9
---

This page presents the different messages that are produced by the C and
Cpp Analyzer during application source code analysis. Messages are
presented with their identifier (referenced in the analysis log files),
a description, and a possible remediation. 

## Issue #1: Analysis stops prematurely while executing a C/C++ analysis

During an analysis errors can occur that stop the analysis process. In
the table below you will find the list of error messages that are
related to this and the steps to follow in order to resolve the problem:

|                                                                                                |                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |               |                                             |                                                                                                                      |
|------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|---------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| Message                                                                                        | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                             | Message Level | Impact                                      | Resolution                                                                                                           |
| Analysis canceled on user request.                                                             | The analysis has been canceled by the user.                                                                                                                                                                                                                                                                                                                                                                                                                             | Error         | The analysis ended.                         | Restart the analysis.                                                                                                |
| Run-time exception occurred, analysis canceled: \<Reasons\>                                    | The analysis crashed.                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Error         | The analysis ended without any result.      | Contact Support.                                                                                                     |
| Please take a copy of the file 'PATH' and send it to CAST, to help analyze the issue.          | This message is associated with the analysis crash above. Please send support the preprocessed file mentioned in the error message.                                                                                                                                                                                                                                                                                                                                     | Error         | The analysis ended without any result.      | Contact Support and send them the preprocessed file.                                                                 |
| Unknown exception occurred, analysis canceled.                                                 | The analysis crashed.                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Error         | The analysis ended without any result.      | Contact Support.                                                                                                     |
| Fatal error: \<Reason\>                                                                        | The analysis crashed.                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Error         | The analysis ended without any result.      | Contact Support.                                                                                                     |
| Analysis canceled.                                                                             | The analysis has been canceled.                                                                                                                                                                                                                                                                                                                                                                                                                                         | Error         | The analysis ended without any result.      | Restart the analysis. If you face the problem again, then contact Support.                                           |
| Invalid positions (start, end) found for object NAME when computing CRC: \<list of positions\> | Internal error.                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Error         | The result of the analysis will be corrupt. | Contact Support and send them the preprocessed file.                                                                 |
| Parser error \<Details\>                                                                       | Parsing error.                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Error         | The result of the analysis can be corrupt.  | Contact Support and send them the preprocessed file.                                                                 |
| Some unexisting paths where found in the configuration:                                        | This error will occur when you have manually added Source code, Includes or Macros to the analysis configuration and these items point to a code location that either no longer exists or cannot be accessed when the analysis is run. Typically this occurs when analyzing Vn+1 of the Application and the Deployment folder changes location between versions: if the Source code, Includes or Macros point to the previous Deployment folder, then the error occurs. | Error         | The analysis ended without any result.      | Update and correct the location of the manually added Source code, Includes and Macros and then re-run the analysis. |

## Issue #2: Missing source code

Situation is clear: either you don't have the complete source code or
there is a mismatch between the Operating System you have configured the
Analysis Unit for and the system headers you are using.

Please find below the error messages concerning these issues:

|                                                                           |                                                                                              |               |                                              |                                                                  |
|---------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|---------------|----------------------------------------------|------------------------------------------------------------------|
| Message                                                                   | Description                                                                                  | Message Level | Impact                                       | Resolution                                                       |
| "Invalid include path \<path\>, declared in environment profile 'PROFILE' | The path of the included file is not correct. Please correct it.                             | Error         | The results of the analysis may be corrupt.  | Correct the definition of the profile or select another profile. |
| Configuration \<Message\> (during the phase of parsing)                   | A macro or an include file it is not correctly defined. Probably an include file is missing. | Error         | The results of the analysis will be corrupt. | Correct the configuration or define the missing macro.           |

## Issue #3: Configuration problems

Sometimes the whole source code is available but issues related to the
configuration can occur.  Fortunately these issues are highlighted by
error messages (see the table below) giving you the possibility to
correct them and improve the quality of the results.

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Message</p></th>
<th class="confluenceTh"><p>Description</p></th>
<th class="confluenceTh"><p>Message Level</p></th>
<th class="confluenceTh"><p>Impact</p></th>
<th class="confluenceTh"><p>Resolution</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>Unable to open file &lt;name of the
file&gt;.</p></td>
<td class="confluenceTd"><p>Unable to open the file.</p></td>
<td class="confluenceTd"><p>Error</p></td>
<td class="confluenceTd"><p>The result of the analysis will be
corrupt.</p></td>
<td class="confluenceTd"><p>Check that file is accessible (network,
permissions etc.) or call Support.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>"Unable to open file 'PATH' and to compute
CRC. Reason: &lt;reason&gt;</p></td>
<td class="confluenceTd"><p>Unable to open the file.</p></td>
<td class="confluenceTd"><p>Error</p></td>
<td class="confluenceTd"><p>The result of the analysis will be
corrupt.</p></td>
<td class="confluenceTd"><p>Check that file is accessible (network,
permissions etc.) or call Support.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Preprocessor cannot find file &lt;name of
the file&gt;</p></td>
<td class="confluenceTd"><p>An included file is missing.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The result of the analysis may be
corrupt.</p></td>
<td class="confluenceTd"><p>Add the file or correct the include
path.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Following file(s) is/are selected for
analysis, while being also included by other analyzed file(s). This
could lead so some inconsistencies in the analysis, if they are parsed
with different preprocessing contexts (e.g. different macro
definitions).<br />
Maybe you want to consider removing that/these file(s) from the
selection: POSLIST</p></td>
<td class="confluenceTd"><p>Following file(s) is/are selected for
analysis, while also being included by other analyzed file(s).</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The result of the analysis may be
corrupt.</p></td>
<td class="confluenceTd"><p>Correct the analysis configuration.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd">Trying to include a precompiled header from
another place than the start of the compiled file (file '&lt;path&gt;',
line &lt;line&gt;). This can cause strange behavior. PCH usage
deactivated</td>
<td class="confluenceTd">This usually occurs when a file that is
included, itself includes a .pch. This usually would happen in files
such as .hpp or .c files.<br />
What customer can do: Not include pch from file that is included.</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">No impact.</td>
<td class="confluenceTd">Avoid including .pch in the source code where
possible.</td>
</tr>
</tbody>
</table>

## Issue #4: There are some ambiguities with regard to object names

Issues can occur at analysis time with regard to object names. This
probably means that:

-   Either you have issues in the macro on portable code (i.e code meant
    for multiple Operating Systems): some functions are defined several
    times (one for each Operating System) and the macros prevent the
    analyzer from only considering one of them. To see this, you should
    use the Test Analysis action - have a look at the locations
    where the functions are defined and see under which preprocessor
    conditions they should be compiled.
-   Or (less probable) you have some code that relies on some specific
    properties of the compiler (which is indeed bad practice).

Below are the main messages related to these issues with the impact and
the resolution steps:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Message</p></th>
<th class="confluenceTh"><p>Description</p></th>
<th class="confluenceTh"><p>Message Level</p></th>
<th class="confluenceTh"><p>Impact</p></th>
<th class="confluenceTh"><p>Resolution</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>Empty variable name encountered (is that a
C++ file?)</p></td>
<td class="confluenceTd"><p>Error concerning the empty variable names,
most probably due to an earlier parsing error .</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The result of the analysis will be
corrupt.</p></td>
<td class="confluenceTd"><p>Correct the analysis configuration.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Multiple definitions found for 'NAME'. A
link will be traced towards each of these definitions: &lt;list of
positions&gt;</p></td>
<td class="confluenceTd"><p>Resolution error.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>Unwanted links can be created.</p></td>
<td class="confluenceTd"><p>Contact Support.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Several definitions found for symbol
&lt;symbol mangling&gt;:&lt;list of positions&gt;<br />
References to that symbol will be duplicated in some cases.</p></td>
<td class="confluenceTd"><p>Multiple definitions found for the same
object inside different source code.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The analysis will contain unwanted
links.</p></td>
<td class="confluenceTd"><p>Split the Analysis Unit into several
parts.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Several definitions found for &lt;name of
object&gt;. Two objects will be created: &lt;list of
positions&gt;</p></td>
<td class="confluenceTd"><p>Multiple definitions found for the same
object inside the same source code.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The analysis will contain unwanted
links.</p></td>
<td class="confluenceTd"><p>Split the Analysis Unit into several
parts.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>Unresolved identifier &lt;name of
identifier&gt;</p></td>
<td class="confluenceTd"><p>Resolution error. Some source code may be
missing.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>Some missing links.</p></td>
<td class="confluenceTd"><p>Correct the analysis configuration or
contact Support.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Function &lt;name of the function&gt;() is
called without having been declared.</p></td>
<td class="confluenceTd"><p>This message concerns only the C language.
An include file is missing or the file uses C feature
unrecommended.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>There is no impact.</p></td>
<td class="confluenceTd"><p>Check the analysis configuration.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>No parent class nor namespace found for
method/function &lt;name of the method&gt;. Object will be saved under
its parent file only.</p></td>
<td class="confluenceTd"><p>The source code is not complete. Most of the
time headers with class definitions are missing.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The result of the analysis may be
corrupt.</p></td>
<td class="confluenceTd"><p>Correct the analysis configuration (the
include paths).</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Unknown identifier &lt;name of the
identifier&gt; is now considered as a type.</p></td>
<td class="confluenceTd"><p>The parser has corrected a missing macro
definition or a missing include file. Please complete the macro
definition list in order to remove the warning.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>No impact.</p></td>
<td class="confluenceTd"><p>Correct the analysis configuration.</p></td>
</tr>
</tbody>
</table>

## Issue #5: Memory problems

If you are experiencing memory issues during an analysis, Cast
recommends using the Windows perfmon tool while running the analysis
to check if the process is using more than 1.9 GB of memory.

If this is the case, it means that the Analysis Unit is too big in terms
of file size or file number - you will need to split the Analysis Unit
into multiple smaller Analysis Units.

Note that the CAST Management Studio automatically manages dependencies
between Analysis Units provided that the Analysis Units are all part of
the same parent application.  
Please also check that all include files are available if needed in all
the new Analysis Unit configurations.

## Issue #6: Other problems

If you are still experiencing problems and none of them concern the
topics above, then please check the messages below:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Message</p></th>
<th class="confluenceTh"><p>Description</p></th>
<th class="confluenceTh"><p>Message Level</p></th>
<th class="confluenceTh"><p>Impact</p></th>
<th class="confluenceTh"><p>Resolution</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><p>Unable to access list of system functions
implied in diags (strcpy, gets...). Some of these diags could be
inconsistent.</p></td>
<td class="confluenceTd"><p>The analyzer cannot access the list of
system functions.</p></td>
<td class="confluenceTd"><p>Error</p></td>
<td class="confluenceTd"><p>The result of the analysis will be
corrupt.</p></td>
<td class="confluenceTd"><p>Contact Support.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd"><p>Flag 'recursive' has been set for include
path 'PATH'. This flag is generally useless and this is generally an
error to set it.</p></td>
<td class="confluenceTd"><p>The order defined for the included files is
not correct.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The result of the analysis will be
corrupt.</p></td>
<td class="confluenceTd"><p>Remove the "recursive" flag.</p></td>
</tr>
<tr class="odd">
<td class="confluenceTd"><p>File &lt;name of the file&gt; has no
executable code.</p></td>
<td class="confluenceTd"><p>The files only contain function definitions,
macros or comments.</p></td>
<td class="confluenceTd"><p>Warning</p></td>
<td class="confluenceTd"><p>The result of the analysis may be
corrupt.</p></td>
<td class="confluenceTd"><p>Check the analysis configuration.</p></td>
</tr>
<tr class="even">
<td class="confluenceTd">"[Parser] XXX: Single operand or parenthesized
expression expected."<br />
<br />
"[Parser] XXX:  Nonstandard untyped declaration, "int" assumed."<br />
<br />
"[Parser] XXX: Ignored token 'XXX'"<br />
<br />
"[Parser] XXX: Variable has already a name"</td>
<td class="confluenceTd">Generally these type of messages are not
blocking and usually mean that a defined macro or a header file is
missing.</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Potentially missing links.</td>
<td class="confluenceTd">Check the analysis configuration and the
delivered source code.</td>
</tr>
</tbody>
</table>

