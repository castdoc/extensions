---
title: "Structural rules"
linkTitle: "Structural rules"
type: "docs"
weight: 10
---

See [https://technologies.castsoftware.com/rules?sec=t_1050571&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1050571&ref=%7C%7C).

>Note that the following extensions also provide .NET specific rules:
>- [C and Cpp Analyzer](../extensions/com.castsoftware.cpp/).