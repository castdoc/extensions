---
title: "Covered Technologies"
linkTitle: "Covered Technologies"
type: "docs"
weight: 2
---

## C and Cpp - Covered technologies

#### Language

| Icon                                                                                                                                                            | Name   | Versions      | Quality                                                                                                            | Extension                                                       |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|:-------|:--------------|:-------------------------------------------------------------------------------------------------------------------|:----------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/c/c-original.svg" alt="icon" style="width: 32px; height: 32px;">                 | C      | C99, C11, C17 | ISO 5055 Security, ISO 5055 Efficiency, ISO 5055 Reliability, ISO 5055 Maintainability, OWASP, CWE, Autosar, MISRA | [com.castsoftware.cpp](../cpp/extensions/com.castsoftware.cpp/) |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="icon" style="width: 32px; height: 32px;"> | C++    | Up to C++17   | ISO 5055 Security, ISO 5055 Efficiency, ISO 5055 Reliability, ISO 5055 Maintainability, OWASP, CWE, Autosar, MISRA | [com.castsoftware.cpp](../cpp/extensions/com.castsoftware.cpp/) |

#### Data Access

| Icon                                                                                                                                                                                | Name           | Versions   | Quality   | Extension                                                       |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------|:-----------|:----------|:----------------------------------------------------------------|
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/defaultcppframework/defaultcppframework-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Oracle Pro*C   |            |           | [com.castsoftware.cpp](../cpp/extensions/com.castsoftware.cpp/) |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/defaultcppframework/defaultcppframework-original.svg" alt="icon" style="width: 32px; height: 32px;"> | Oracle Pro*C++ |            |           | [com.castsoftware.cpp](../cpp/extensions/com.castsoftware.cpp/) |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/ibmdb2/ibmdb2-original.svg" alt="icon" style="width: 32px; height: 32px;">                           | IBM Db2 SQC    |            |           | [com.castsoftware.cpp](../cpp/extensions/com.castsoftware.cpp/) |
| <img src="https://raw.githubusercontent.com/CAST-projects/devicon/master/icons/ibmdb2/ibmdb2-original.svg" alt="icon" style="width: 32px; height: 32px;">                           | IBM Db2 SQC++  |            |           | [com.castsoftware.cpp](../cpp/extensions/com.castsoftware.cpp/) |

