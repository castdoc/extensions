---
title: "Requirements"
linkTitle: "Requirements"
type: "docs"
weight: 3
---

To successfully deliver and analyze C and Cpp technologies, the
following third-party software is required:

<table class="confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><p>Install on workstation running the DMT (for
extraction)</p></th>
<th class="confluenceTh"><p>Install on workstation running CMS (for
analysis)</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>Nothing required</strong></p>
<div>
<div>
Note that no project discoverer exists for Visual Studio 6.0, however,
source code can still be packaged.
</div>
</div>
</div></td>
<td class="confluenceTd"><div class="content-wrapper">
<p><strong>Software required</strong></p>
<p>If you intend to use the <strong>CAST - VC++ XXX - Mandatory
Part</strong> Environment Profile (usage is recommended), then you MUST
install:</p>
<ul>
<li>All include files of third party libraries that are used.</li>
<li>The appropriate IDE depending on source code to be analyzed:
<ul>
<li>Visual Studio 6.0</li>
<li>Visual Studio .NET 2003</li>
<li>Visual Studio 2005</li>
<li>Visual Studio 2008</li>
<li>Visual Studio 2010</li>
<li>Visual Studio 2012</li>
</ul></li>
</ul>
<div>
<div>
Note that if you do not install the appropriate IDE for your source
code, then the analysis will fail.
</div>
</div>
<div>
<div>
Note that the above information does not apply to other source code
(C/C++ compiler from vendors other than Microsoft).
</div>
</div>
</div></td>
</tr>
</tbody>
</table>
