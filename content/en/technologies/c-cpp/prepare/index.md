---
title: "Prepare and deliver the source code"
linkTitle: "Prepare and deliver the source code"
type: "docs"
weight: 5
---

## Introduction

Discovery is a process that is actioned during the packaging process
whereby CAST will attempt to automatically identify projects within your
application using a set of predefined rules. This discovery process also
allows CAST to set the initial analysis configuration settings explained
in C and Cpp - Analysis configuration.

Discovery for C/C++ is explained in more detail in [C and Cpp -
Project discovery](../discovery) - you should read
this to understand how the source code will be handled and the choices
that are available to you.

## Prepare the application source code

AIP Console expects the application source code to be delivered either
via a ZIP file or via a source code folder configured
in AIP Console. Whichever option you chose, you should include in the
ZIP/source code folder all of your C/C++ application source code. CAST
highly recommends placing all the relevant files in a folder and using
sub-folders where necessary. You can deliver other technologies at the
same time (for example, database DDL). If you are using a ZIP/archive
file, zip the folders in the "temp" folder as shown in the image below -
but do not zip the "temp" folder itself, nor create any intermediary
folders:

``` java
D:\temp
    |-----C-Cpp
    |-----OtherTechno1
    |-----OtherTechno2
```