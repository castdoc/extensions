---
title: "Node.js - 2.10"
linkTitle: "2.10"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.nodejs

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Node.js. Node.js is a JavaScript
runtime built on [Chrome's V8 JavaScript
engine](https://developers.google.com/v8/). Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. 

CAST recommends using this extension with [HTML5 and JavaScript](../../../html5-js/com.castsoftware.html5/) ≥ 2.0.0 for the best results.

## In what situation should you install this extension?

Regarding Front-End to Back-End connections, we do support the following
cross-technology stacks:

| ![](../images/628851140.png)                             | ![](../images/628851137.png)                        | ![](../images/628851133.png)                                 |
| -------------------------------------------------------- | --------------------------------------------------- | ------------------------------------------------------------ |
| *iOS Front-End connected to Node.js/PostgreSQL Back-end* | *iOS Front-End connected to Node.js/MSSQL Back-end* | *AngularJS Front-End connected to Node.js/MongoDB_ Back-end* |

If your Web application contains Node.js source code and you want to
view these object types and their links with other objects, then you
should install this extension:

-   creates a Node.js application object when an instance has been found
-   creates Node.js operations which represent entry-points of web services
-   Node.js operations are called from client applications, using jQuery Ajax for example. Supported client frameworks are:
    -   [jQuery](../../../jquery/com.castsoftware.jquery/)
    -   [AngularJS](../../../angularjs/com.castsoftware.angularjs/)

## Supported Node.js versions

| Version |      Support       | Comment                 |
| ------- | :----------------: | ----------------------- |
| v0.x    |        :x:         | No longer supported     |
| v4.x    | :white_check_mark: | LTS                     |
| v5.x    | :white_check_mark: | Based on Javascript ES6 |
| v6.x    | :white_check_mark: | Based on Javascript ES6 |
| v7.x    | :white_check_mark: | Based on Javascript ES6 |
| v8.x    | :white_check_mark: |                         |
| v9.x    | :white_check_mark: |                         |
| v10.x   | :white_check_mark: |                         |
| v11.x   | :white_check_mark: |                         |
| v12.x   | :white_check_mark: |                         |
| v13.x   | :white_check_mark: |                         |
| v14.x   | :white_check_mark: |                         |
| v15.x   | :white_check_mark: |                         |
| v16.x   | :white_check_mark: |                         |
| v17.x   | :white_check_mark: |                         |
| v18.x   | :white_check_mark: |                         |
| v19.x   | :white_check_mark: |                         |
| v20.x   | :white_check_mark: |                         |

## Node.js Ecosystem

Node.js comes with numerous libraries and frameworks bringing data access, web services calls, microservices architectures. This list contains all supported libraries:

| Library                       | Comment                                    |    Data Access     |    Web Service     |     Messaging      | Cloud code execution | Remote Process Call |
| ----------------------------- | ------------------------------------------ |:------------------:| :----------------: | :----------------: | :------------------: | :-----------------: |
| AWS.DynamoDB                  | Amazon database access                     | :white_check_mark: |                    |                    |                      |                     |
| AWS.S3                        | Amazon storage service                     | :white_check_mark: |                    |                    |                      |                     |
| AWS.SQS                       | Amazon messaging service                   |                    |                    | :white_check_mark: |                      |                     |
| AWS Lambda                    | Cloud code execution                       |                    |                    |                    |  :white_check_mark:  |                     |
| Azure blobs                   | Azure storage service                      | :white_check_mark: |                    |                    |                      |                     |
| Azure Service Bus             | Azure Queue Service                        |                    |                    | :white_check_mark: |                      |                     |
| Azure Event Hubs              | Azure Queue Service                        |                    |                    | :white_check_mark: |                      |                     |
| Azure Function                | Cloud code execution                       |                    |                    |                    |  :white_check_mark:  |                     |
| Cosmos DB                     | Microsoft Azure NoSQL Database solution    | :white_check_mark: |                    |                    |                      |                     |
| Couchdb                       | Couchdb access                             | :white_check_mark: |                    |                    |                      |                     |
| Couchdb-nano                  | Couchdb access                             | :white_check_mark: |                    |                    |                      |                     |
| elasticsearch                 | Open-source search engine                  | :white_check_mark: |                    |                    |                      |                     |
| Express                       | Node.js application framework              |                    | :white_check_mark: |                    |                      |                     |
| GCP Bigtable                  | GCP database access                        | :white_check_mark: |                    |                    |                      |                     |
| GCP Cloud Storage             | GCP storage service                        | :white_check_mark: |                    |                    |                      |                     |
| GCP Pub/Sub                   | GCP messaging service                      |                    |                    | :white_check_mark: |                      |                     |
| g-RPC                         | Remote Process Call service                |                    |                    |                    |                      | :white_check_mark:  |
| Hapi                          | Node.js application framework              |                    | :white_check_mark: |                    |                      |                     |
| Knex                          | Node.js SQL query builder                  | :white_check_mark: |                    |                    |                      |                     |
| Koa                           | Node.js application framework              |                    | :white_check_mark: |                    |                      |                     |
| Loopback                      | Node.js application framework              | :white_check_mark: | :white_check_mark: |                    |                      |                     |
| Marklogic                     | Marklogic access                           | :white_check_mark: |                    |                    |                      |                     |
| Memcached                     | Storage framework                          | :white_check_mark: |                    |                    |                      |                     |
| Mongodb (node-mongodb-native) | MongoDB access                             | :white_check_mark: |                    |                    |                      |                     |
| Mongo-client                  | MongoDB access                             | :white_check_mark: |                    |                    |                      |                     |
| Mongoose                      | MongoDB access                             | :white_check_mark: |                    |                    |                      |                     |
| MQTT                          | Messaging library                          |                    |                    | :white_check_mark: |                      |                     |
| mssql                         | SQL Server                                 | :white_check_mark: |                    |                    |                      |                     |
| my_connection                 | MySQL access                               | :white_check_mark: |                    |                    |                      |                     |
| myssql                        | MySQL access                               | :white_check_mark: |                    |                    |                      |                     |
| Node-couchdb                  | Couchdb access                             | :white_check_mark: |                    |                    |                      |                     |
| oracledb                      | Oracle Database access                     | :white_check_mark: |                    |                    |                      |                     |
| pg                            | PostgreSQL access                          | :white_check_mark: |                    |                    |                      |                     |
| redis                         | Redis access                               | :white_check_mark: |                    |                    |                      |                     |
| request                       | HTTP request client                        |                    | :white_check_mark: |                    |                      |                     |
| request-promise               | HTTP request client                        |                    | :white_check_mark: |                    |                      |                     |
| request-promise-native        | HTTP request client                        |                    | :white_check_mark: |                    |                      |                     |
| request-promise-any           | HTTP request client                        |                    | :white_check_mark: |                    |                      |                     |
| Sails                         | Node.js application framework              | :white_check_mark: | :white_check_mark: |                    |                      |                     |
| Seneca                        | Microservice toolkit                       |                    | :white_check_mark: |                    |                      |                     |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Comparison with existing support for JavaScript

CAST AIP has provided support for analyzing JavaScript via its JEE and
.NET analyzers (provided out of box in CAST AIP) for some time now.
The HTML5/JavaScript extension (on which the Node.js extension
depends) also provides support for JavaScript but with a focus on web
applications. CAST highly recommends that you use this extension if your
Application contains JavaScript and more specifically if you want to
analyze a web application, however you should take note of the
following:

-   You should ensure that you configure the extension to NOT analyze
    the back end web client part of a .NET or JEE application.
-   You should ensure that you configure the extension to ONLY analyze
    the front end web application built with the HTML5/JavaScript that
    communicates with the back end web client part of a .NET or JEE
    application.
-   If the back end web client part of a .NET or JEE application is
    analyzed with the Node.js extension and with the native .NET/JEE
    analyzers, then your results will reflect this - there will be
    duplicate objects and links (i.e. from the analyzer and from the
    extension) therefore impacting results and creating erroneous
    Function Point data.

>In CAST AIP ≥ 8.3.x support for analyzing JavaScript has been withdrawn
from the JEE and .NET analyzers.

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The Node.js extension requires that the
following other CAST extensions are also installed:

-   [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)
-   Web services linker service (internal technical extension)

Note that when using the CAST Extension Downloader to download the
extension and the Manage Extensions interface in CAST Server
Manager to install the extension, any dependent extensions are
automatically downloaded and installed for you. You do not need to
do anything.

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST
Console. You can manage the extension using the Application -
Extensions interface:

### Analysis with CAST Imaging Console

CAST Imaging Console exposes the technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Universal Technology (3) in the
Config (1) \> Analysis (2) tab to display the available options for your Node.js source
code:

![](../images/530317462.jpg)

Then choose the relevant Analysis Unit (1) to view the
configuration:

![](../images/530317461.jpg)

![](../images/530317460.jpg)


### Analysis warning and error messages

|  Message ID  |  Message Type  |   Logged during  |  Impact                                                                                            |  Remediation  |  Action                        |
|--------------|----------------|------------------|----------------------------------------------------------------------------------------------------|---------------|--------------------------------|
| NODEJS-001   | Warning        | Analysis         | An internal issue occured when parsing a statement in a file. A part of a file was badly analyzed. |               | [Contact](https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support) CAST Technical Support |

## What results can you expect?

Node.js source code will be categorised in CAST Dashboards as
"HTML5/JavaScript".

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner (for example via CAST Enlighten):

![](../images/628851126.png)

*Node.js application with MongoDB data storage exposing web services*

### Detailed analysis results per framework

See below for more details about how the extension handles each
supported framework: [Results](results/).

### Objects

The following specific objects are displayed in CAST Enlighten:

| Icon | Description |
|:---:|-----|
| ![](../images/628851127.png) | NodeJS Application |
| ![](../images/628851138.png) | NodeJS Port |
| ![](../images/628851134.png) | NodeJS Delete Operation Service |
| ![](../images/628851141.png) | NodeJS Get Operation Service |
| ![](../images/628851130.png) | NodeJS Post Operation Service |
| ![](../images/628851136.png) | NodeJS Put Operation Service |
| ![](../images/628851131.png) | NodeJS Service<br>NodeJS Call to gRPC service method<br>NodeJS gRPC service method |
| ![](../images/628851142.jpg) | NodeJS Express Controller |
| ![](../images/628851141.png) | NodeJS Get Http Request Service |
| ![](../images/628851130.png) | NodeJS Post Http Request Service |
| ![](../images/628851136.png) | NodeJS Put Http Request Service |
| ![](../images/628851134.png) | NodeJS Delete Http Request Service |
| ![](../images/628851132.png) | NodeJS Unknown Database |
| ![](../images/628851139.png) | NodeJS Collection |
| ![](../images/628851151.png) | NodeJS Memcached Connection |
| ![](../images/628851152.png) | NodeJS Memcached Value |
| ![](../images/628851155.png) | NodeJS Call to Java Program |
| ![](../images/628851156.png) | NodeJS Call to Generic Program |
| ![](../images/628851141.png) | NodeJS Restify Get Operation |
| ![](../images/628851130.png) | NodeJS Restify Post Operation |
| ![](../images/628851136.png) | NodeJS Restify Put Operation |
| ![](../images/628851134.png) | NodeJS Restify Delete Operation |
| ![](../images/628851176.png) | NodeJS AWS SQS Publisher<br>NodeJS AWS SNS Publisher<br>NodeJS Azure Service Bus Publisher<br>NodeJS Azure Event Hub Publisher<br>NodeJS GCP Pub/Sub Publisher |
| ![](../images/628851179.png) | NodeJS AWS SQS Receiver<br>NodeJS AWS SNS Subscriber<br>NodeJS Azure Service Bus Receiver<br>NodeJS Azure Event Hub Receiver<br>NodeJS GCP Pub/Sub Receiver |
| ![](../images/628851177.png) | NodeJS AWS SQS Unknown Publisher<br>NodeJS AWS SNS Unknown Publisher<br>NodeJS Azure Unknown Service Bus Publisher<br>NodeJS Azure Unknown Event Hub Publisher<br>NodeJS GCP Unknown Pub/Sub Publisher |
| ![](../images/628851180.png) | NodeJS AWS SQS Unknown Receiver<br>NodeJS AWS SNS Unknown Subscriber<br>NodeJS Azure Unknown Service Bus Receiver<br>NodeJS Azure Unknown Event Hub Receiver<br>NodeJS GCP Unknown Pub/Sub Receiver |
| ![](../images/628851178.png) | NodeJS GCP Pub/Sub Subscription  |
| ![](../images/628851165.png) | NodeJS Azure Function |
| ![](../images/628851166.png) | NodeJS Call to Azure Function |
| ![](../images/628851167.png) | NodeJS Call to Unknown Azure Function |
| ![](../images/628851163.png) | NodeJS AWS call to Lambda Function |
| ![](../images/628851164.png) | NodeJS AWS call to unknown Lambda Function |
| ![](../images/628851175.png) | NodeJS SignalR Hub Method |
| ![](../images/628851173.png) | NodeJS SignalR Call to Hub Method |
| ![](../images/628851174.png) | NodeJS SignalR Call to Unknown Hub Method |
| ![](../images/628851153.png) | NodeJS S3 Bucket<br>NodeJS Azure Blob Container<br>NodeJS GCP Cloud Storage Bucket |
| ![](../images/628851162.png) | NodeJS S3 Unknown Bucket<br>NodeJS Azure Unknown Blob Container<br>NodeJS GCP Unknown Cloud Storage Bucket |
| ![](../images/628851170.png) | NodeJS CosmosDB Collection<br>NodeJS AWS DynamoDB Table<br>NodeJS Elasticsearch Index<br>NodeJS GCP Bigtable table<br>NodeJS Marklogic collection<br>NodeJS Redis Collection |
| ![](../images/628851171.png) | NodeJS CosmosDB Unknown Collection<br>NodeJS Elasticsearch Unknown Index<br>NodeJS GCP Unknown Bigtable table<br>NodeJS Unknown Database Table |
| ![](../images/628851181.png) | NodeJS SQL Query |
| ![](../images/628851139.png) | NodeJS MongoDB collection |
| ![](../images/628851151.png) | NodeJS MongoDB connection |
| ![](../images/CAST_NodeJS_Unknown_MongoDB_Collection.png) | NodeJS Unknown MongoDB collection |
| ![](../images/CAST_NodeJS_Unknown_MongoDB_Connection.png) | NodeJS Unknown MongoDB connection |

#### External link behavior

Behaviour is different depending on the version of CAST AIP you are
using the extension with:

-   From 7.3.6, SQL queries are sent to the external links exactly
    like standard CAST AIP analyzers.
-   From 7.3.4 and before 7.3.6, a degraded mode takes place:
    The Node.js extension analyzes the FROM clause to retrieve table
    names, then sends the table names only to external links.
-   For all other versions, if no links are found via external links and the com.castsoftware.nodejs.missingtable extension is installed,  
    unresolved objects are created (with type
    CAST_NodeJS_Unknown_Database_Table).

### Data sensitivity

This extension is capable of setting a property on NoSQL collection and Cloud File storage objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

See [Data Sensitivity](../../../../multi/data-sensitivity/) for more information.

### Structural Rules

The following structural rules are provided:

|  Release | Link  |
|----------|-------|
| 2.10.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=\|\|2.10.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=%7C%7C2.10.3-funcrel) |
| 2.10.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=\|\|2.10.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=%7C%7C2.10.2-funcrel) |
| 2.10.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=\|\|2.10.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=%7C%7C2.10.1-funcrel) |
| 2.10.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=\|\|2.10.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=%7C%7C2.10.0-funcrel) |
| 2.10.0-beta1   | [https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=\|\|2.10.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_nodejs&ref=%7C%7C2.10.0-beta1)     |

## Known Limitations

In this section we list the most significant functional limitations that
may affect the analysis of applications using Node.js:

-   With regard to external links degraded mode, only statements with a
    FROM clause are correctly handled.
-   NodeJS objects are only supported for ES5 standard.
-   Analysis of AWS Lambda function needs have access to the
    serverless.yml file mapping routes and handlers together
-   Technology specific known limitations are listed in the dedicated
    framework page:  
    -   [Node.js - 2.10 - Analysis Results - AWS S3](results/aws-s3/)
    -   [Node.js - 2.10 - Analysis Results - AWS SQS](results/aws-sqs/)
    -   [Node.js - 2.10 - Analysis Results - GCP Bigtable](results/gcp-bigtable/)
    -   [Node.js - 2.10 - Analysis Results - GCP Cloud Storage](results/gcp-cloudstorage/)
