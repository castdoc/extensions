---
title: "SQL Named Query support for Node.js"
linkTitle: "SQL Named Query"
type: "docs"
---

When executing an sql query directly, the Node.js extension will create a `CAST SQL NamedQuery object`. For example:

``` js
var oracledb = require('oracledb');

connection = oracledb.getConnection(
  {
    user          : "hr",
    password      : "welcome",
    connectString : "localhost/XE"
  }
);

oracledb.getConnection(
  {
    user          : "hr",
    password      : "welcome",
    connectString : "localhost/XE"
  },
  function(err, connection)
  {
    if (err) { console.error(err); return; }
    connection.execute(
      "SELECT department_id, department_name "
    + "FROM titles "
    + "WHERE department_id < 70 "
    + "ORDER BY department_id",
      function(err, result)
      {
        if (err) { console.error(err); return; }
        console.log(result.rows);
      });
  });
```

Will give the following results:

![](../../../images-results/628851293.png)