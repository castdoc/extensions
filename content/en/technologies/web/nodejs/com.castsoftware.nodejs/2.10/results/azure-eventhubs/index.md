---
title: "Azure Event Hubs support for Node.js"
linkTitle: "Azure Event Hubs"
type: "docs"
---

This extension supports version 5.x of the @azure/event-hubs
package. Whenever a call to one of the following APIs is found in the
source code:

-   EventHubBufferedProducerClient.enqueueEvent
-   EventHubBufferedProducerClient.flush
-   EventHubProducerClient.sendBatch

...this extension evaluates the name of the Event Hub in which the
operation is made and creates an Event Hub Publisher object with a
callLink from the caller.  Whenever a call
to EventHubConsumerClient.subscribe is found in the source code, this
extension evaluates the name of the Event Hub in which the operation is
made and creates an Event Hub Receiver object with a callLink to the
handler method*.*

If the evaluation of the Event Hub name fails (either due to missing
information in the source code or to limitations in the evaluation) an
Unknown Event Hub Publisher/Receiver object is created.

When analyzing the following source code:

publisher.js

``` js
const { EventHubProducerClient } = require("@azure/event-hubs");

const eventHubName = "tests-hub";

function my_publish(){
  // Create a producer client to send messages to the event hub.
  const producer = new EventHubProducerClient(connectionString, eventHubName, {retryOptions: {maxRetries: 3, maxRetryDelayInMs: 3000}});

  const batch = await producer.createBatch({ partitionKey: i });
  batch.tryAdd({ body: "Message" });
  // Send the batch to the event hub.
  await producer.sendBatch(batch);

}
```

consumer.js

``` js
const { EventHubConsumerClient } = require("@azure/event-hubs");

const eventHubName = "tests-hub";

async function main() {

  // Create a consumer client for the event hub by specifying the checkpoint store.
  const consumerClient = new EventHubConsumerClient(consumerGroup, connectionString, eventHubName, checkpointStore);

  // Subscribe to the events, and specify handlers for processing the events and errors.
  const subscription = consumerClient.subscribe({
      processEvents: async (events, context) => {
        console.log(events.length);
      }
    }
  );
}
```

the following result is produced:

![](../../../images-results/628851210.png)
