---
title: "Azure SignalR support for Node.js"
linkTitle: "Azure SignalR"
type: "docs"
---

## ASP.NET Core

This extension supports calls to server for
the @microsoft/signalr and @aspnet/signalr packages. Whenever a
call to one of the following API is found in the source code:

-   HubConnection.send
-   HubConnection.invoke
-   HubConnection.stream

...this extension evaluates the name of the invoked method as well as
the hub name and creates a NodeJS Azure SignalR Call to Hub
Method object named with the name of the invoked method and with a
property storing the hub name.  If the evaluation of the method name
fails (either due to missing information in the source code or to
limitations in the evaluation) a NodeJS Azure SignalR Call to Unknown
Hub Method object is created.

## ASP.NET

This extension supports call to server using the signalr package.
This extension supports both calls with or without the generated proxy:

### Without generated proxy

Whenever an invoke call is found, if this call comes from a hub proxy
created from a createHubProxy() call, this extension creates a NodeJS
Azure SignalR Call to Hub Method. Its name is evaluated from the first
argument of the invoke and it has a hub name property whose value is
evaluated from the first argument of the createHubProxy Call.

If the evaluation of the method name fails (either due to missing
information in the source code or to limitations in the evaluation)
a NodeJS Azure SignalR Call to Unknown Hub Method object is created.

### With generated proxy

When a method call $.connection.hubname.server.methodname() is found,
this extension creates a NodeJS Azure SignalR Call to Hub
Method. Its name is methodname and it has a hub name property whose
value is hubname.

## Example

When analyzing one of the following pieces of code:

module_for_aspnet_core.js

``` js
import * as signalr from '@microsoft/signalr'

function my_invoke(){
    const connection = new signalR.HubConnectionBuilder()
    .withUrl("/myhub")
    .configureLogging(signalR.LogLevel.Information)
    .build();
    
    await connection.start()
    await connection.invoke("MyMethod", user, message);
    
}
```

module_for_aspnet_without_generated_proxy.js

``` js
function my_invoke(){
  var connection = $.hubConnection();
  var hubProxy= connection.createHubProxy('myhub');
  hubProxy.invoke('MyMethod', { UserName: userName, Message: message})
}
```

module_for_aspnet_with_generated_proxy.js

``` js
function my_invoke(){
   var hubProxy = $.connection.myhub;
   hubProxy.server.MyMethod({user:user, message:message})
}
```

the following result will be produced:

![](../../../images-results/628851226.png)

The MyMethod object has a property hub name whose value is myhub*.*
If a DotNet Azure SignalR Method object exists with matching method
name and hub name exits
the [com.castsoftware.wbslinker](../../../../../../multi/com.castsoftware.wbslinker/) extension will
create a link from the NodeJS Azure SignalR Call to Hub Method to
that object.
