---
title: "Express Framework support for Node.js"
linkTitle: "Express Framework"
type: "docs"
---

The Node.js extension will create a Node.js Get Operation object for
the following Express declaration:

``` js
app.get('/login', function (req, res) {
    "use strict";
    console.log('login ' + req.url);
    console.log('login ' + req.query.pseudo);
    var currentSession = getSessionId(req, res);
    datab.userExists(currentSession, req.query.pseudo, res, cbLogin);
});
```

and will create a NodeJS Service Operation object for the following
Express declaration:

``` js
var admin = express();

app.use('/admin', admin);
```
