---
title: "Call to Program support for Node.js"
linkTitle: "Call to Program"
type: "docs"
---

## Introduction

The Node.js extension supports calls to external programs using the
child-proces*s* module.

The *fork()* function is not handled as its only purpose is to fork
node.js programs.

## Calls to Java programs/JAR files

The following declarations create a call to a java program/JAR file. For
example:

``` js
const exec = require('child_process').exec;

exec('java -cp com.castsoftware.Archive -jar jarFile.jar', (e, stdout, stderr) => {
    if (e instanceof Error) {
        console.error(e);
        throw e;
    }

    console.log('stdout ', stdout);
    console.log('stderr ', stderr);
});
```

Giving the following result:

![](../../../images-results/628851229.png)

For example:

``` js
const cp = require('child_process');
const class_name = 'com.castsoftware.Foo'

function call_foo(req, resp) {
    const args = [
        '-cp',
        '/bin',
        class_name
    ];
    const proc = cp.spawn('java', args);
}
```

Giving the following result:

![](../../../images-results/628851230.png)

## Calls to Python programs

The following declaration creates a call to a Python program:

``` js
const execFile = require('child_process').execFile;
const python_file = 'app.py'

const child = execFile('python', [python_file], (error, stdout, stderr) => {

    if (error) {
        console.error('stderr', stderr);
        throw error;
    }
    console.log('stdout', stdout);
});
```

Giving the following result:

![](../../../images-results/628851231.png)
