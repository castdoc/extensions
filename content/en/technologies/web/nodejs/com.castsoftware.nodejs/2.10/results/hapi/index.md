---
title: "Hapi support for Node.js"
linkTitle: "Hapi"
type: "docs"
---

## Introduction

This page explains the support of the [Hapi](https://hapi.dev/) framework.

## Server

For example, create a server - index.js:

``` js
const Hapi = require('hapi');

// Create Server
const server = new Hapi.Server();
```

This will give the following result:

![](../../../images-results/628851265.png)

## Routes

For example, create a route for a server:

``` js
server.route([     
    {
        method: 'GET',
        path: '/api/directors/{id}',
        handler: api.directors.get,
        config: {
            tags: ['api'],
            description: 'Get one director by id',
            notes: 'Get one director by id',
            validate: {
                params: {
                    id: Joi.number().required()
                }
            },
            cors: {
                origin: ['*']
            }
        }
    }
];
```

This will give the following result:

![](../../../images-results/628851266.png)
