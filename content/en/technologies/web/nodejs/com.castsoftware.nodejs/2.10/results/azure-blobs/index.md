---
title: "Azure Blobs support for Node.js"
linkTitle: "Azure Blobs"
type: "docs"
---

Whenever a call to a method carrying a CRUD operation on an
Azure Blob is found in the source code, this extension evaluates the
name of the container in which the operation is made and a link is
created from the caller of the method to that blob container. If
the evaluation of the container name fails (either due to missing
information in the source code or to limitations in the evaluation) a
link is created to an Unknown container. For methods copying a
blob from one container to another, a useSelectLink is created to
the source container and both a useInsertLink and a
useUpdateLink are created to the destination container.

The list of supported methods and the type of links created are listed
in the following table:

<table class="relative-table confluenceTable" style="width:100%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 28%" />
<col style="width: 28%" />
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">LinkType</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from containerClient</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> {BlobServiceClient} <span class="op">=</span> <span class="pp">require</span>(<span class="st">&quot;@azure/storage-blob&quot;</span>)<span class="op">;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blobServiceClient <span class="op">=</span> <span class="kw">new</span> <span class="fu">BlobServiceClient</span>(<span class="op">...</span>)<span class="op">;</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> containerClient <span class="op">=</span> blobServiceClient<span class="op">.</span><span class="fu">getContainerClient</span>(<span class="op">...</span>)</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from BlobClients (blockBlobClient,<br />
PageBlobClient, AppendBlobClient or BlobBatchCLient)</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> {BlobServiceClient} <span class="op">=</span> <span class="pp">require</span>(<span class="st">&quot;@azure/storage-blob&quot;</span>)</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blobServiceClient <span class="op">=</span> <span class="kw">new</span> <span class="fu">BlobServiceClient</span>(<span class="op">...</span>)</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> containerClient <span class="op">=</span> blobServiceClient<span class="op">.</span><span class="fu">getContainerClient</span>(<span class="op">...</span>)</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blockBlobClient <span class="op">=</span> containerClient<span class="op">.</span><span class="fu">getBlockBlobClient</span>(<span class="op">...</span>)</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from BlobServices</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> azure <span class="op">=</span> <span class="pp">require</span>(<span class="st">&#39;azure-storage&#39;</span>)</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blobService <span class="op">=</span> azure<span class="op">.</span><span class="fu">createBlobService</span>(<span class="op">...</span>)</span></code></pre></div>
</div>
</div>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useInsert</td>
<td class="confluenceTd">uploadBlockBlob</td>
<td class="confluenceTd">syncUploadFromURL, upload', uploadPages,
uploadPagesFromURL, beginCopyFromURL</td>
<td class="confluenceTd">createAppendBlobFromBrowserFile,
createAppendBlobFromLocalFile, createAppendBlobFromStream,
createAppendBlobFromText, createBlobSnapshot, createBlobSnapshot,
createBlockBlobFromLocalFile, createBlockBlobFromStream,
createBlockBlobFromText, createBlockFromStream, createBlockFromText,
createBlockFromURL, createOrReplaceAppendBlob, createPageBlob,
createPageBlob, createPageBlobFromLocalFile, createPageBlobFromStream,
createPagesFromStream, createWriteStreamToBlockBlob,
createWriteStreamToBlockBlob, createWriteStreamToNewAppendBlob,
createWriteStreamToNewPageBlob, startCopyBlob</td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdate</td>
<td class="confluenceTd">uploadBlockBlob</td>
<td class="confluenceTd">commitBlockList, stageBlock, stageBlockFromURL,
syncUploadFromURL, upload, uploadBrowserData<br />
'ploadData, uploadFile, uploadStream, uploadPages, uploadPagesFromURL,
appendBlock, appendBlockFromURL, beginCopyFromURL,
startCopyIncremental</td>
<td class="confluenceTd">appendBlockFromStream, appendBlockFromText,
appendFromBrowserFile, appendFromLocalFile, appendFromStream,
appendFromText, commitBlocks, createAppendBlobFromBrowserFile,
createAppendBlobFromLocalFile, createAppendBlobFromStream,
createAppendBlobFromText, createBlobSnapshot,
createBlockBlobFromLocalFile, createBlockBlobFromStream,
createBlockBlobFromText, createBlockFromStream, createBlockFromText,
createBlockFromURL, createOrReplaceAppendBlob, createPageBlob,
createPageBlob, createPageBlobFromLocalFile, createPageBlobFromStream,
createPagesFromStream, createWriteStreamToBlockBlob,
createWriteStreamToBlockBlob, createWriteStreamToExistingAppendBlob,
createWriteStreamToExistingAppendBlob,
createWriteStreamToExistingPageBlob, startCopyBlob</td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDelete</td>
<td class="confluenceTd">deleteBlob, deleteIfExists</td>
<td class="confluenceTd">clearPages, deleteBlobs, delete,
deleteIfExists</td>
<td class="confluenceTd">deleteBlob, deleteBlobIfExists,
deleteContainer, deleteContainerIfExists</td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelect</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">getBlockList, query, createSnapshot, download,
downloadToBuffer,
downloadToFile, beginCopyFromURL, startCopyIncremental</td>
<td class="confluenceTd">createBlobSnapshot, createReadStream,
getBlobToLocalFile, getBlobToStream, getBlobToText, startCopyBlob,
createBlockFromURL</td>
</tr>
</tbody>
</table>

When analyzing the following code, a blob container named
my_container is created as well as useInsert and useUpdate
links from the main function to that container:

``` js
const { BlobServiceClient } = require("@azure/storage-blob");

const blobServiceClient = new BlobServiceClient(account_url, defaultAzureCredential);

async function main() {
  const containerClient = blobServiceClient.getContainerClient("my_container");
  const content = "Hello world!";
  const blockBlobClient = containerClient.getBlockBlobClient("blobName");
  const uploadBlobResponse = await blockBlobClient.upload(content, content.length);
}
```

![](../../../images-results/628851205.png)
