---
title: "SQL Connectors support for Node.js"
linkTitle: "SQL Connectors"
type: "docs"
---

## Introduction

The following SQL Connecter APIs are supported by the Node.js extension:

## RDBMS vendors

### Oracle "oracledb" connector

Connector "oracledb"

``` js
var oracledb = require('oracledb');
connection = oracledb.getConnection(
  {
    user          : "hr",
    password      : "welcome",
    connectString : "localhost/XE"
  }
);
connection.execute(
      "SELECT department_id, department_name FROM departments WHERE department_id < 70",
      function(err, result)
      {
        if (err) { console.error(err); return; }
        console.log(result.rows);
      }
  );
```

### Microsoft SQL Server "node-sqlserver" and "mssql" connectors

Connector "node-sqlserver"

``` js
var sql = require('node-sqlserver');
//
var connStr = "Driver={SQL Server Native Client 11.0};Server=myySqlDb,1433;Database=DB;UID=Henry;PWD=cat;";
var query = "SELECT * FROM GAData WHERE TestID = 17";
sql.open(connStr, function(err,conn){
    if(err){
        return console.error("Could not connect to sql: ", err);
    }
    conn.queryRaw("SELECT TOP 10 FirstName, LastName FROM authors", function (err, results) {
        if (err) {
            console.log("Error running query!");
            return;
        }
        for (var i = 0; i < results.rows.length; i++) {
            console.log("FirstName: " + results.rows[i][0] + " LastName: " + results.rows[i][1]);
        }
    });
});
var match = "%crombie%";
sql.query(conn_str, "SELECT FirstName, LastName FROM titles WHERE LastName LIKE ?", [match], function (err, results) { 
    for (var i = 0; i < results.length; i++) {
        console.log("FirstName: " + results[i].FirstName + " LastName: " + results[i].LastName);
    }
});
```

Connector "mssql"

``` js
var sql = require('mssql');
var config = {
    user: '...',
    password: '...',
    server: 'localhost', // You can use 'localhost\\instance' to connect to named instance 
    database: '...',
     
    options: {
        encrypt: true // Use this if you're on Windows Azure 
    }
}
  
var connection = new sql.Connection(config, function(err) {
    // ... error checks 
     
    // Query 
     
    var request = new sql.Request(connection); // or: var request = connection.request(); 
    request.query('select * from authors', function(err, recordset) {
        // ... error checks 
         
        console.dir(recordset);
    });
     
    // Stored Procedure 
     
    var request = new sql.Request(connection);
    request.input('input_parameter', sql.Int, 10);
    request.output('output_parameter', sql.VarChar(50));
    request.execute('procedure_name', function(err, recordsets, returnValue) {
        // ... error checks 
         
        console.dir(recordsets);
    });
     
});
```

### PostgreSQL "pg" connector

Connector "pg"

``` js
var pg = require("pg");
var conString = "pg://operator:CastAIP@localhost:2280/postgres";
var client = new pg.Client(conString);
client.connect();
var querySchemas = client.query("select nspname from pg_catalog.pg_namespace");
querySchemas.on("row", function (row, result) {
    "use strict";
    result.addRow(row);
});
querySchemas.on("end", function (result) {
    "use strict";
    console.log(result.rows);
    client.end();
});
```

### MySQL "my_connection" connector

Connector "my_connection"

``` js
var connection = require("my_connection");
connection.query('my_url', 
            function result_getCatLogDetails(getCatLogDetails_err, getCatLogDetails_rows, 
            getCatLogDetails_fields) {
        
                if (getCatLogDetails_err) {
                    logContent += '|ERROR'+";";
                    logContent += getCatLogDetails_err.message+";";
                    utils.logAppDetails(logContent);
                    deferred.reject(new Error(getCatLogDetails_err));
                } else {
                    deferred.resolve(getCatLogDetails_rows);
                }
            });
```

## NoSQL vendors

The Node.js extension does not directly create NoSQL server side
representation in the analysis results, however, it will create a client
side representation based on the API access and will create links from
JavaScript functions to NoSQL "Database" or "Table" equivalents as
follows:

| Vendor             | Link                                                                                               |
|--------------------|----------------------------------------------------------------------------------------------------|
| Azure Cosmos DB    | See [Azure Cosmos DB support for Node.js source code](../../../../../../nosql/overview/azure-cosmos-db/nodejs/) |
| CouchDB            | See [CouchDB support for Node.js source code](../../../../../../nosql/overview/couchdb/nodejs/)     |
| DynamoDB           | See [DynamoDB support for Node.js source code](../../../../../../nosql/overview/dynamodb/nodejs-js) |
| Elasticsearch      | See [Elasticsearch support for Node.js source codeTitle](../../../../../../nosql/overview/elasticsearch/nodejs) |
| MarkLogic          | See [MarkLogic support for Node.js source code](../../../../../../nosql/overview/marklogic/nodejs) |
| Memcached          | See [Memcached support for Node.js source code](../../../../../../nosql/overview/memcached/) |
| MongoDB "mongoose" | See [MongoDB support for Node.js source code](../../../../../../nosql/overview/mongodb/nodejs) |
| Redis              | See [Redis support for Node.js source code](../../../../../../nosql/overview/redis/nodejs) |
