---
title: "Amazon Web Services SNS support for Node.js"
linkTitle: "AWS SNS"
type: "docs"
---

## Supported APIs

The following APIs are supported:

### For SDK V2

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a>AWS<span class="op">.</span><span class="fu">SNS</span><span class="op">({</span>apiVersion<span class="op">:</span> <span class="er">&#39;</span><span class="dv">2010</span><span class="op">-</span><span class="bn">03</span><span class="op">-</span><span class="dv">31</span><span class="er">&#39;</span><span class="op">}).</span><span class="fu">publish</span><span class="op">(</span>params<span class="op">)</span></span></code></pre></div>
</div>
</div>
</div></td>
<td class="confluenceTd">A <strong>NodeJS AWS SNS Publisher</strong>
object is created. Its name is that of the topic.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a>AWS<span class="op">.</span><span class="fu">SNS</span><span class="op">({</span>apiVersion<span class="op">:</span> <span class="er">&#39;</span><span class="dv">2010</span><span class="op">-</span><span class="bn">03</span><span class="op">-</span><span class="dv">31</span><span class="er">&#39;</span><span class="op">}).</span><span class="fu">subscribe</span><span class="op">(</span>params<span class="op">)</span></span></code></pre></div>
</div>
</div>
</div></td>
<td class="confluenceTd">A <strong>NodeJS</strong> <strong>AWS SNS
Subscriber </strong>object is created. Its name is that of the topic.
Then for each supported protocol, an object is created with a callLink
from the subscriber to that object.</td>
</tr>
</tbody>
</table>

### For SDK V3

<table class="wrapped confluenceTable">
<tbody>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="kw">new</span> <span class="fu">SNSClient</span><span class="op">({</span>region<span class="op">:</span> REGION<span class="op">}).</span><span class="fu">send</span><span class="op">(</span><span class="kw">new</span> <span class="fu">PublishCommand</span><span class="op">(</span>params<span class="op">));</span></span></code></pre></div>
</div>
</div>
</div></td>
<td class="confluenceTd">A <strong>NodeJS AWS SNS Publisher</strong>
object is created. Its name is that of the topic.</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: java; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: java; gutter: false; theme: Confluence"><pre
class="sourceCode java"><code class="sourceCode java"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="kw">new</span> <span class="fu">SNSClient</span><span class="op">({</span>region<span class="op">:</span> REGION<span class="op">}).</span><span class="fu">send</span><span class="op">(</span><span class="kw">new</span> <span class="fu">SubscribeCommand</span><span class="op">(</span>params<span class="op">));</span></span></code></pre></div>
</div>
</div>
</div></td>
<td class="confluenceTd">A <strong>NodeJS</strong> <strong>AWS SNS
Subscriber </strong>object is created. Its name is that of the topic.
Then for each supported protocol, an object is created with a callLink
from the subscriber to that object. </td>
</tr>
</tbody>
</table>

## Supported protocols

The [com.castsoftware.wbslinker](Web_Services_Linker) will create a
callLink between the SNS Publishers and SNS Subscribers which have the
same name.

|            |                                           |                                                                |
|------------|-------------------------------------------|----------------------------------------------------------------|
| Protocol   | Object created                            | Object name                                                    |
| email      | NodeJS Email                              | an Email (the email addresses are not evaluated).              |
| sms        | NodeJS SMS                                | an SMS (the SMS numbers are not evaluated).                    |
| http/https | NodeJS AWS Post HttpRequers service       | the URL (evaluated from the endpoint).                         |
| sqs        | NodeJS AWS Simple Queue Service Publisher | the name of the queue (evaluated from the endpoint).           |
| lambda     | NodeJS Call to AWS Lambda Function        | the name of the Lambda function (evaluated from the endpoint). |

## Examples

### API v2

When analyzing the following source code:

``` js
var AWS = require('aws-sdk');
// Set region
AWS.config.update({region: 'REGION'});
// Create promise and SNS service object

var sns = new AWS.SNS({apiVersion: '2010-03-31'})

function my_subscribe(params) {
    sns.subscribe(params, function (err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else console.log(data);           // successful response
    });
}

function my_publish(params) {
    sns.publish(params);
}

function foo() {
    let topicArn = "arn:aws:sns:eu-west-3:757025016730:testTopic";
    my_subscribe({Protocol: "EMAIL", TopicArn: topicArn, Endpoint: "EMAIL_ADDRESS"})
    my_subscribe({Protocol: "SMS", TopicArn: topicArn, Endpoint: "911"})
    my_subscribe({Protocol: "LAMBDA", TopicArn: topicArn, Endpoint: "arn:aws:lambda:eu-west-3:757025016730:testLambda"})
    my_subscribe({Protocol: "HTTP", TopicArn: topicArn, Endpoint: "http:/myapi.api.test.com"})
 }
function bar() {
    const params2 = {
        TopicArn: "arn:aws:sns:eu-west-3:757025016730:testTopic",
        Message: "MESSAGE_TEXT"
    };
    my_publish(params2)
}
```

The following results will be produced:

![](../../../images-results/628851194.png)

### API v3

When analyzing the following source code:

``` js
const {SNSClient} = require("@aws-sdk/client-sns");
// Set the AWS Region.
const REGION = "us-east-1";
// Create SNS service object.
const snsClient = new SNSClient({region: REGION});
export {snsClient};

// ====== Import required AWS SDK clients and commands for Node.js
const {PublishCommand} = require("@aws-sdk/client-sns");
const {SubscribeCommand} = require("@aws-sdk/client-sns");

const subscription = async () => {
    try {
        let topicArn = "arn:aws:sns:eu-west-3:757025016730:testTopic";
        const data1 = await snsClient.send(new SubscribeCommand({Protocol: "EMAIL", TopicArn: topicArn, Endpoint: "test@mail.com"}));
        const data2 = await snsClient.send(new SubscribeCommand({Protocol: "SMS", TopicArn: topicArn, Endpoint: "911"}));
        const data3 = await snsClient.send(new SubscribeCommand({Protocol: "LAMBDA", TopicArn: topicArn, Endpoint: "arn:aws:lambda:eu-west-3:757025016730:testLambda"}));
        const data4 = await snsClient.send(new SubscribeCommand({Protocol: "HTTP", TopicArn: topicArn, Endpoint: "http:/myapi.api.test.com"}));
        console.log("Success.", data1);
        return data1;
    } catch (err) {
        console.log("Error", err.stack);
    }
};
subscription();

const run = async () => {
    // Set the parameters
    const params = {
        Message: "MESSAGE_TEXT", // MESSAGE_TEXT
        TopicArn: "arn:aws:sns:eu-west-3:757025016730:testTopic", //TOPIC_ARN
    };
    try {
        const data = await snsClient.send(new PublishCommand(params));
        console.log("Success.", data);
        return data; // For unit tests.
    } catch (err) {
        console.log("Error", err.stack);
    }
};
run();
```

The following results will be produced:

![](../../../images-results/628851195.png)
