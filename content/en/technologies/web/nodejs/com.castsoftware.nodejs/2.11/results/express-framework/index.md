---
title: "Express Framework support for Node.js"
linkTitle: "Express Framework"
type: "docs"
---

## Introduction

[Express](https://expressjs.com/)
is a back-end web application framework for building RESTful APIs with Node.js.


## Objects

This extension, when ExpressJS library is found in the source code, may create the following objects:

| Icon | Object |
|---|---|
| ![](../../../images/628851141.png) | NodeJS Get Operation
| ![](../../../images/628851130.png) | NodeJS Post Operation
| ![](../../../images/628851136.png) | NodeJS Put Operation
| ![](../../../images/628851134.png) | NodeJS Delete Operation
| ![](../../../images/628851131.png) | NodeJS Use Operation
| ![](../../../images/628851131.png) | NodeJS Any Operation

## API methods

The supported Express API [methods](https://expressjs.com/en/4x/api.html#app.METHOD) for handling routes and middleware are:

| API method                 | Object created             | Link type |
|----------------------------|----------------------------|-----------|
| app.get(path, callback)    | NodeJS Get Operation       | callLink  |
| app.post(path, callback)   | NodeJS Post Operation      | callLink  |
| app.put(path, callback)    | NodeJS Put Operation       | callLink  |
| app.delete(path, callback) | NodeJS Delete Operation    | callLink  |
| app.all(path, callback)    | NodeJS Any Operation       | callLink  |
| app.use(path, callback)    | NodeJS Use Operation       | callLink  |

When one of the supported API methods is found with a resolved handler function (callback), a `callLink` is created from the operation object to the handler.

### Router object

The methods provided by Express for handling routes and middleware can also be used with a [Router](https://expressjs.com/en/4x/api.html#router) object. The same methods mentioned above are also supported in this context.

### Example

``` js
var express = require('express')
const app = express()
const router = express.Router()

router.get('/getPath', function (req, res) {
  res.send('Response for GET requests with path toto')
})


router.post('/postPath', function (req, res) {
  res.send('Response for POST requests')
})

router.put('/putPath', function (req, res) {
  res.send('Response for PUT requests')
})

router.delete('/deletePath', function (req, res) {
  res.send('Response for DELETE requests')
})

router.all('/allPath', function (req, res) {
  res.send('Response for ANY requests')
})

app.use('/home', router)

```

The code above will create the links and objects below:

![](../../../images-results/express_example.png)