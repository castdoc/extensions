---
title: "Amazon Web Services SQS support for Node.js" 
linkTitle: "AWS SQS"
type: "docs"
---

>Support for [AWS SQS](https://aws.amazon.com/sqs/).

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh"><br />
</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from SDK V2 sqs client</p>
</div></th>
<th class="confluenceTh"><p>Commands from SDK V3</p>
<p>imported from '@aws-sdk/client-sqs'</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Publish</td>
<td class="confluenceTd"><ul>
<li><p>sendMessage</p></li>
<li>sendMessageBatch</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>SendMessageCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">Receive</td>
<td class="confluenceTd"><ul>
<li>receiveMessage</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>ReceiveMessageCommand</li>
</ul></td>
</tr>
</tbody>
</table>

## Examples

### SDK v2

This code will publish a message into the "SQS_QUEUE_URL" queue:

``` js
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'REGION'});

// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var params = {
   // Remove DelaySeconds parameter and value for FIFO queues
  DelaySeconds: 10,
  MessageAttributes: {
    "Title": {
      DataType: "String",
      StringValue: "The Whistler"
    },
    "Author": {
      DataType: "String",
      StringValue: "John Grisham"
    },
    "WeeksOn": {
      DataType: "Number",
      StringValue: "6"
    }
  },
  MessageBody: "Information about current NY Times fiction bestseller for week of 12/11/2016.",
  // MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
  // MessageGroupId: "Group1",  // Required for FIFO queues
  QueueUrl: "SQS_QUEUE_URL"
};

sqs.sendMessage(params, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data.MessageId);
  }
});
```

This code will receive a message from the queue "SQS_QUEUE_URL":

``` js
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'REGION'});

// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var params = {
   // Remove DelaySeconds parameter and value for FIFO queues
  DelaySeconds: 10,
  MessageAttributes: {
    "Title": {
      DataType: "String",
      StringValue: "The Whistler"
    },
    "Author": {
      DataType: "String",
      StringValue: "John Grisham"
    },
    "WeeksOn": {
      DataType: "Number",
      StringValue: "6"
    }
  },
  MessageBody: "Information about current NY Times fiction bestseller for week of 12/11/2016.",
  // MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
  // MessageGroupId: "Group1",  // Required for FIFO queues
  QueueUrl: "SQS_QUEUE_URL"
};

sqs.receiveMessage(params, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data.MessageId);
  }
});
```

The code listed above will produce the following results:

*Click to enlarge*

![](../../../images-results/628851198.png)

When the evaluation of the queue name fails, a Node.js AWS SQS Unknown
Publisher (or Receiver) object will be created.

### SDK v3

This code will publish a message into the "SQS_QUEUE_URL" queue:

``` js
const { SQSClient } = require("@aws-sdk/client-sqs");
const REGION = "us-east-1";
const sqsClient = new SQSClient({ region: REGION });
export  { sqsClient };

const { SendMessageCommand } = require("@aws-sdk/client-sqs");

const params = {
  DelaySeconds: 10,
  MessageAttributes: {
    Title: {
      DataType: "String",
      StringValue: "The Whistler",
    },
    Author: {
      DataType: "String",
      StringValue: "John Grisham",
    },
    WeeksOn: {
      DataType: "Number",
      StringValue: "6",
    },
  },
  MessageBody: "Information about current NY Times fiction bestseller for week of 12/11/2016."
  QueueUrl: "SQS_QUEUE_URL"

};

const run = async () => {
  try {
    const data = await sqsClient.send(new SendMessageCommand(params));
    console.log("Success, message sent. MessageID:", data.MessageId);
    return data; // For unit tests.
  } catch (err) {
    console.log("Error", err);
  }
};
run();
```

This code will receive a message from the queue "SQS_QUEUE_URL":

``` js
const { SQSClient } = require("@aws-sdk/client-sqs");
const REGION = "us-east-1";
const sqsClient = new SQSClient({ region: REGION });
export  { sqsClient };

const { ReceiveMessageCommand } = require("@aws-sdk/client-sqs");

const queueURL = 'SQS_QUEUE_URL'; 
const params = {
  AttributeNames: ["SentTimestamp"],
  MaxNumberOfMessages: 10,
  MessageAttributeNames: ["All"],
  QueueUrl: queueURL,
  VisibilityTimeout: 20,
  WaitTimeSeconds: 0,
};

const receive = async () => {
  try {
    const data = await sqsClient.send(new ReceiveMessageCommand(params));
    if (data.Messages) {
      console.log("messages obtained");
    } else {
      console.log("No messages");
    }
    return data; // For unit tests.
  } catch (err) {
    console.log("Receive Error", err);
  }
};
receive();
```

The code listed above will produce the following results:

![](../../../images-results/628851199.png)

When the evaluation of the queue name fails, a Node.js AWS SQS Unknown
Publisher (or Receiver) object will be created.

## Limitations

-   The use of AWS.SQS with promises is not supported. For instance no
    link would be created between the receiver and the handler function
    defined in .then() call in the following source code: 

``` xml
sqs.receiveMessage(params).promise().then( () => {});
```

-   The use of AWS.SQS with send() is not supported. For instance no
    link would be created between the receiver and the handler function
    defined in .send() call in the following source code: 

``` xml
var request = sqs.receiveMessage(params);
request.send(() => {});
```
