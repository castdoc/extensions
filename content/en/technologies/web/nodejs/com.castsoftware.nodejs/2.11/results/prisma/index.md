---
title: "Prisma support for Node.js"
linkTitle: "Prisma"
type: "docs"
---

## Introduction

Prisma is an open source next-generation framework - see https://www.prisma.io/docs for more information.

## Objects

Prisma framework particularly supports access to both SQL and MongoDB databases. For the SQL analysis, the entities are created from the prisma configuration file, and the entity operations linked to these entities are also created then. For the MongoDB analysis, a connection and the collections are created from the prisma configuration file:
- 'Prisma Configuration' object when .prisma file is found in project.
- 'NodeJS Entity' object when SQL database is used and a model keyword
is found inside .prisma file.
- 'NodeJS Entity Operation' object when SQL database is used and
a Prisma method is defined within a .js file.
- 'NodeJS MongoDB connection' or 'NodeJS Unknown MongoDB connection' object
when MongoDB framework is used.
- 'NodeJS MongoDB collection' object when MongoDB framework is used and
a model keyword is found inside .prisma file.

| Icon                                                            | Description                        |
|-----------------------------------------------------------------|------------------------------------|
| ![](../../../images/nodejs_prisma_configuration.png)            | Prisma Configuration       |
| ![](../../../images/nodejs_entity.png)                          | NodeJS Entity                     |
| ![](../../../images/nodejs_entity_operation.png)                | NodeJS Entity Operation           |
| ![](../../../images/628851135.png)                              | NodeJS MongoDB connection         |
| ![](../../../images/CAST_NodeJS_Unknown_MongoDB_Connection.png) | NodeJS Unknown MongoDB connection |
| ![](../../../images/628851139.png)                              | NodeJS MongoDB collection         |

### Analysis of prisma configuration file

If the .prisma configuration file is missing from the analyzed source code, this extension will not be able to create the objects above.

## Supported persistence SQL databases

### Supported operations

| Entity Operation | Supported APIs |
|---|---|
| Add | <ul><li>prisma.entity.create</li><li>prisma.entity.createMany</li></ul> |
| Update | <ul><li>prisma.entity.update</li><li>prisma.entity.updateMany</li><li>prisma.entity.upsert</li></ul> |
| Remove | <ul><li>prisma.entity.delete</li><li>prisma.entity.deleteMany</li></ul> |
| Select | <ul><li>prisma.entity.findMany</li><li>prisma.entity.findFirst</li><li>prisma.entity.findFirstOrThrow</li><li>prisma.entity.findUnique</li><li>prisma.entity.findUniqueOrThrow</li><li>prisma.entity.count</li></ul> |

The 'entity' above is 'NodeJS Entity' defined in .prisma file.

### Supported links

| Link Type | Caller type | Callee type |
|---|---|---|
| callLink | <ul><li>JavaScript function</li><li>JavaScript initialisation</li></ul> | <ul><li>NodeJS Entity Operation: Add</li><li>NodeJS Entity Operation: Update</li><li>NodeJS Entity Operation: Remove</li><li>NodeJS Entity Operation: Select</li></ul> |
| useInsertLink | NodeJS Entity Operation: Add | SQL table |
| useUpdateLink | NodeJS Entity Operation: Update | SQL table |
| useDeleteLink | NodeJS Entity Operation: Remove | SQL table |
| useSelectLink | NodeJS Entity Operation: Select | SQL table |

### Example

Taking the following codes of prisma and javascript files:

``` js
datasource db {
  provider = "postgresql"
  url      = "postgresql://doe:password@localhost:5432/mydb?schema=sample"
}

model User {
  id    Int     @id @default(autoincrement())
  email String  @unique
  name  String?
  posts Post[]
}

model Post {
  id        Int     @id @default(autoincrement())
  title     String
  content   String?
  published Boolean @default(false)
  author    User    @relation(fields: [authorId], references: [id])
  authorId  Int
}
```
``` js
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

async function main1() {
  const user1 = await prisma.user.create({
  })

  const user2 = await prisma.user.create({
  })

  const allUsers = await prisma.user.findMany({
  })
}
```

In this example, two 'NodeJS Entity' and two 'NodeJS Entity Operation' objects are created
and two 'call' links between the function 'main1' and these entity operations are added.
The [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer)
or [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable)
can then link these entity operations with the corresponding table. In the present case, this
extension creates a 'useInsert' and a 'useSelect' links to the missing table 'user':

![](../../../images-results/sql_query_prisma.png)

## Supported persistence MongoDB database

### Supported links

| Link Type | Source and destination of link | Supported APIs |
|---|---|---|
| useInsertLink | Between JavaScript function (JavaScript initialisation also) and NodeJS MongoDB collection | <ul><li>prisma.entity.create</li><li>prisma.entity.createMany</li></ul> |
| useUpdateLink | Between JavaScript function (JavaScript initialisation also) and NodeJS MongoDB collection | <ul><li>prisma.entity.update</li><li>prisma.entity.updateMany</li><li>prisma.entity.upsert</li></ul> |
| useDeleteLink | Between JavaScript function (JavaScript initialisation also) and NodeJS MongoDB collection | <ul><li>prisma.entity.delete</li><li>prisma.entity.deleteMany</li></ul> |
| useSelectLink | Between JavaScript function (JavaScript initialisation also) and NodeJS MongoDB collection | <ul><li>prisma.entity.findMany</li><li>prisma.entity.findFirst</li><li>prisma.entity.findFirstOrThrow</li><li>prisma.entity.findUnique</li><li>prisma.entity.findUniqueOrThrow</li><li>prisma.entity.count</li></ul> |

The 'entity' above is 'NodeJS Entity' defined in .prisma file.

### Example

Taking the following codes of prisma and javascript files:

``` js
datasource db {
  provider = "mongodb"
  url      = "mongodb+srv://test:test@cluster0.mongodb.net/database2"
}

model User {
  id    Int     @id @default(autoincrement())
  email String  @unique
  name  String?
  posts Post[]
}

model Post {
  id        Int     @id @default(autoincrement())
  title     String
  content   String?
  published Boolean @default(false)
  author    User    @relation(fields: [authorId], references: [id])
  authorId  Int
}
```
``` js
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

async function main2() {
  const post1 = await prisma.post.create({
  })

  const post2 = await prisma.post.create({
  })

  const allPosts = await prisma.post.findMany({
  })
}
```

In this example, a 'NodeJS MongoDB connection' and two 'NodeJS MongoDB collection' objects are created.
This extension creates a 'useInsert' and a 'useSelect' links from the
function 'main2' to the collection 'Post':

![](../../../images-results/nosql_query_prisma.png)
