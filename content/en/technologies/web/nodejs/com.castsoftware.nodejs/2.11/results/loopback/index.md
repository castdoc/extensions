---
title: "Loopback support for Node.js"
linkTitle: "Loopback"
type: "docs"
---

## Introduction

This page explains the support of the [Loopback](https://loopback.io/) framework.

## Create webservice from Express API

The App extends and supports Express Middleware. Webservice can be
supported as API Express framework:

``` js
var loopback = require('loopback');
var app = loopback();

// Create get method
app.get('/', function(req,res){
res.send('hello wor;ld')
});

app.listen(3000);
```

## Create webservice from model

Model todo.js:

``` js
module.exports = function(Todo) {
  Todo.stats = function(filter, cb) {
  ...
  };
  ...
  Todo.remoteMethod('stats', {
    accepts: {arg: 'filter', type: 'object'},
    returns: {arg: 'stats', type: 'object'},
    http: { path: '/stats' }
  }, Todo.stats);
  ...
}
```

This will give the following result:

![](../../../images-results/628851274.png)

## Exposing models over REST

Loopback models automatically have a standard set of HTTP endpoints that provide REST APIs. See [https://loopback.io/doc/en/lb3/Exposing-models-over-REST.html](https://loopback.io/doc/en/lb3/Exposing-models-over-REST.html) for more information.

Example: todo.json:

``` js
{
  "name": "Todo",
  "base": "PersistedModel",
  "strict": "throw",
  "persisteUndefinedAsNull": true,
  "trackChanges": true,
  "properties": {
    "id": {
      "id": true,
      "type": "string",
      "defaultFn": "guid"
    },
    "title": "string",
    "completed": {
      "type": "boolean",
      "default": false
    },
    "created": {
      "type": "number"
    }
  }
}
```

This will give the following result:

![](../../../images-results/628851275.png)
