---
title: "Azure Service Bus support for Node.js"
linkTitle: "Azure Service Bus"
type: "docs"
---

>NodeJS Azure Service Bus Publisher and NodeJS Azure Service Bus Receiver
objects are created when the following methods are used:

| Object created | Methods from ServiceBusClient from @azure/service-bus package | Methods from ServiceBusService from azure-sb package |
|---|---|---|
| NodeJS Azure Service Bus Publisher | sendMessages, scheduleMessages | createSender, createReceiver |
| NodeJS Azure Service Bus Receiver | receiveMessages, peekMessages, subscribe, getMessageIterator, receiveDeferredMessages | receiveQueueMessage, receiveSubscriptionMessage |

-   The name of the object created is that of the Queue (or the Topic).
    Whenever the evaluation of the Queue (or Topic) name fails, an
    Unknown object is created.
-   For publishers, a callLink  from the callable object (usually
    Function or Method) containing the call (to the supported method) to
    the publisher is added (see example below).
-   For receivers, some APIs (such as the subscribe API) require
    providing a handler function, in such case a callLink is created
    from the receiver to that handler. In other cases, a callLink is
    added from the receiver to the callable object (usually a Function
    or a Method) containing the call to the supported method (see
    example below).
-   Whenever an Azure Service Bus Publisher has the same name as
    an Azure Service Bus Receiver,
    the [com.castsoftware.wbslinker](../../../../../../multi/com.castsoftware.wbslinker/) extension
    will create a call link from the Publisher to the Receiver.

When analyzing the following source code:

``` java
import { ServiceBusClient, ServiceBusMessage, ServiceBusMessageBatch } from "@azure/service-bus";

function my_publish(Messages){
    const sbClient = new ServiceBusClient(connectionString);
    const sender = sbClient.createSender("MyQueue");
    await sender.sendMessages(Messages);
}

function my_receive(){
    const sbClient = new ServiceBusClient(connectionString);
    let receiver = sbClient.createReceiver("MyQueue", {
        receiveMode: "receiveAndDelete"
    });
    let messages = await receiver.receiveMessages(1);
}
```

the following results are produced:

![](../../../images-results/628851223.png)
