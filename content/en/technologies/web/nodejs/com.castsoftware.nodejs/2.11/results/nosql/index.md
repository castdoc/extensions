---
title: "NoSQL support for Node.js"
linkTitle: "NoSQL"
type: "docs"
---

## NoSQL

The Node.js extension does not directly create NoSQL server side
representation in the analysis results, however, it will create a client
side representation based on the API access and will create links from
JavaScript functions to NoSQL "Database" or "Table" equivalents as
follows:

| Vendor                                    | Link                                                                                               |
|-------------------------------------------|----------------------------------------------------------------------------------------------------|
| Azure Cosmos DB                           | See [Azure Cosmos DB support for Node.js](../../../../../../nosql/overview/azure-cosmos-db/nodejs) |
| CouchDB                                   | See [CouchDB support for Node.js](../../../../../../nosql/overview/couchdb/nodejs)                 |
| DynamoDB                                  | See [DynamoDB support for Node.js](../../../../../../nosql/overview/dynamodb/nodejs-js)            |
| Elasticsearch                             | See [Elasticsearch support for Node.js](../../../../../../nosql/overview/elasticsearch/nodejs)     |
| MarkLogic                                 | See [MarkLogic support for Node.js](../../../../../../nosql/overview/marklogic/nodejs)             |
| Memcached                                 | See [Memcached support for Node.js](../../../../../../nosql/overview/memcached/nodejs)             |
| MongoDB ("mongodb", "mongoose", "prisma") | See [MongoDB support for Node.js](../../../../../../nosql/overview/mongodb/nodejs)                 |
| Redis                                     | See [Redis support for Node.js](../../../../../../nosql/overview/redis/nodejs)                     |
