---
title: "Azure Cosmos DB support for Node.js"
linkTitle: "Azure Cosmos DB"
type: "docs"
---

See [Azure Cosmos DB support for Node.js source code - JavaScript](../../../../../../nosql/overview/azure-cosmos-db/nodejs/) for more information.