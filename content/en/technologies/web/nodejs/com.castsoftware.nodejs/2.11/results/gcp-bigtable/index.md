---
title: "GCP Bigtable support for Node.js"
linkTitle: "GCP Bigtable"
type: "docs"
---

## Introduction

[Google Bigtable](https://cloud.google.com/bigtable)
is a NoSQL database service in the cloud. This is the equivalent service
to Amazon DynamoDB and Azure Cosmos DB offered by Google Cloud Platform
(GCP).

## Objects

This extension, when Bigtable API NodeJS library is found in the source
code, may create the following objects representing Bigtable tables:

| Icon | Object | Name |
|---|---|---|
| ![](../../../images-results/635568163.png) | NodeJS GCP Bigtable table | Name of the table (table id) |
| ![](../../../images-results/635568162.png) | NodeJS GCP Unknown Bigtable table | Unknown |

## API methods

A Bigtable table object will be created when the table ID (i.e. the
table name) is resolved in calls to the method Table.table(). When
the name of a given referred table in the code is not resolved, an
unknown table object will be created with name Unknown. There can be a
maximum of one unknown table per project:

| API method        | Object creation                     |
|-------------------|-------------------------------------|
| Table.table() | NodeJS GCP (Unknown) Bigtable table |

In addition to tables, links representing CRUD operations on Bigtable
tables are created as well. The following methods are supported:

| API methods (Table)         | Link type                    |
|-----------------------------|------------------------------|
| Table.createReadStream()    | useSelectLink                |
| Table.deleteRows()          | useDeleteLink                |
| Table.getRows()             | useSelectLink                |
| Table.insert()              | useInsertLink, useUpdateLink |
| Table.mutate()              | useUpdateLink, useInsertLink |
| Table.sampleRowKeys()       | useSelectLink                |
| Table.sampleRowKeysStream() | useSelectLink                |
| Table.truncate()            | useDeleteLink                |

| API methods (Row) | Link type                    | Remaks                                                            |
|-------------------|------------------------------|-------------------------------------------------------------------|
| Row.create()      | useInsertLink                | Link created only when entries are passed in as options argument. |
| Row.delete()      | useDeleteLink                | -                                                                |
| Row.deleteCells() | useUpdateLink                | -                                                                |
| Row.filter()      | useInsertLink, useUpdateLink | Link type presence will depend on input arguments.                |
| Row.get()         | useSelectLink                | -                                                                |
| Row.increment()   | useInsertLink, useUpdateLink | -                                                                |
| Row.save()        | useInsertLink, useUpdateLink | -                                                                |

## Example

The following code shows an example of javascript file querying data
from a GCP Bigtable table via the Table.getRows() API method for NodeJS:

script01.js

``` js
const {Bigtable} = require('@google-cloud/bigtable');
const bigtable = new Bigtable();

const instanceId = 'instanceName';
const tableId = 'Books';
const instance = bigtable.instance(instanceId);
const table = instance.table(tableId);

const options = {
  keys: ['title1', 'title2'],
};
table
  .getRows(options)
  .then(result => {
    const rows = result[0];
  })
  .catch(err => {
    // Handle the error.
  });
```

When the code listed above is analyzed, a Bigtable table object and the
useSelectLink (Us) link will be created:

![](../../../images-results/628851237.png)

## Limitations

-   Only CRUD-like operations are modelized. Actions changing table
    structure (column/family/table creation/deletion) are not
    represented.
-   In the same line, administration clients (BigtableTableAdminClient,
    BigtableInstanceAdminClient) are not supported.
-   Backup operations are not represented.
