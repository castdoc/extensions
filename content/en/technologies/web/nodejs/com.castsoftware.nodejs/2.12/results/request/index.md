---
title: "Request support for Node.js"
linkTitle: "request"
type: "docs"
---

## Introduction

This page explains the support of the [request](https://www.npmjs.com/package/request) library; and its extensions: [request-promise](https://www.npmjs.com/package//request-promise), [request-promise-native](https://www.npmjs.com/package/request-promise-native), [request-promise-any](https://www.npmjs.com/package/request-promise-any).


### Objects

The following specific objects are displayed in CAST Enlighten:

| Icon | Description |
|:---:|-----|
| ![](../../../images/628851141.png) | NodeJS Get Http Request Service |
| ![](../../../images/628851130.png) | NodeJS Post Http Request Service |
| ![](../../../images/628851136.png) | NodeJS Put Http Request Service |
| ![](../../../images/628851134.png) | NodeJS Delete Http Request Service |


### Creating HTTP requests on the front-end

``` js
const request = require('request');

// GET request to fetch users
function getUsers(){
	request.get('api/users')
		.then(user => {
			console.log('GET User:', user);
		})
		.catch(error => {
			console.error('Error fetching user:', error);
    });
}

// POST request to add a new user
function addUser(){
	request.post({
		uri: 'api/users',
		body: { name: 'John' },
		json: true
	})
		.then(newUser => {
			console.log('POST New User:', newUser);
		})
		.catch(error => {
			console.error('Error adding new user:', error);
    });
}

// PUT request to update an existing user
function updateUser(){
	request.put({
		uri: '/api/users/1',
		body: { id: 1, name: 'Eve' },
		json: true
	})
		.then(updatedUser => {
			console.log('PUT Updated User:', updatedUser);
		})
		.catch(error => {
			console.error('Error updating user:', error);
    });
}

// DELETE request to delete a user
function deleteUser(){
	request.del('/api/users/2')
    .then(() => {
        console.log('DELETE User');
    })
    .catch(error => {
        console.error('Error deleting user:', error);
    });
}
```
The code above (_alongside with a corresponding back-end side_) will create the links below:

![](../../../images-results/request_results.png)
*The results for the extensions of `request` mentioned earlier remain consistent.*

