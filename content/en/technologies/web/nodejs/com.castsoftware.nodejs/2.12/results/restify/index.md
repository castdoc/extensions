---
title: "Restify support for Node.js"
linkTitle: "Restify"
type: "docs"
---

The Node.js extension supports routing using the restify module. The following is an example of application using restify to handle URIs:

``` js
var restify = require('restify');

function send(req, res, next) {

  res.send('hello ' + req.params.name);
  next();
}

var server = restify.createServer();

server.post('/hello', function create(req, res, next) {

  res.send(201, Math.random().toString(36).substr(3, 8));
  return next();

});

server.put('/hello', send);
server.get('/hello/:name', function create(req, res, next) {

  res.send(201, Math.random().toString(36).substr(3, 8));
  return next();

},send);
server.head('/hello/:name', send);

server.del('hello/:name', function rm(req, res, next) {

  res.send(204);
  return next();

});

server.listen(8080, function() {

  console.log('%s listening at %s', server.name, server.url);

});
```

Giving the following result:

![](../../../images-results/628851281.png)