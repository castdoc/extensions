---
title: "Amazon Web Services S3 support for Node.js"
linkTitle: AWS S3"
type: "docs"
---

>Support for [AWS S3](https://aws.amazon.com/s3/).

## Links

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from SDK V2 s3client</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> {AWS} <span class="im">from</span> <span class="st">&#39;aws-sdk&#39;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> s3client <span class="op">=</span> <span class="kw">new</span> AWS<span class="op">.</span><span class="fu">S3</span>()</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from SDK V3 s3client</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> {S3} <span class="im">from</span> <span class="st">&#39;@aws-sdk/client-s3&#39;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> s3client <span class="op">=</span> <span class="kw">new</span> <span class="fu">S3</span>()</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><p>Commands from SDK V3</p>
<p>imported from '@aws-sdk/client-s3'</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">No Link</td>
<td class="confluenceTd"><ul>
<li>createBucket</li>
</ul></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><ul>
<li>CreateBucketCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><ul>
<li><p>createMultipartUpload</p></li>
<li><p>createPresignedPost</p></li>
<li><p>abortMultipartUpload</p></li>
<li><p>completeMultipartUpload</p></li>
<li><p>deleteBucketAnalyticsConfiguration</p></li>
<li><p>deleteBucketCors</p></li>
<li><p>deleteBucketEncryption</p></li>
<li><p>deleteBucketInventoryConfiguration</p></li>
<li><p>deleteBucketLifecycle</p></li>
<li><p>deleteBucketMetricsConfiguration</p></li>
<li><p>deleteBucketPolicy</p></li>
<li><p>deleteBucketReplication</p></li>
<li><p>deleteBucketTagging</p></li>
<li><p>deleteBucketWebsite</p></li>
<li><p>deleteObjectTagging</p></li>
<li><p>deletePublicAccessBlock</p></li>
<li><p>getBucketAccelerateConfiguration</p></li>
<li><p>getBucketAcl</p></li>
<li><p>getBucketAnalyticsConfiguration</p></li>
<li><p>getBucketCors</p></li>
<li><p>getBucketEncryption</p></li>
<li><p>getBucketInventoryConfiguration</p></li>
<li><p>getBucketLifecycle</p></li>
<li><p>getBucketLifecycleConfiguration</p></li>
<li><p>getBucketLocation</p></li>
<li><p>getBucketLogging</p></li>
<li><p>getBucketMetricsConfiguration</p></li>
<li><p>getBucketNotification</p></li>
<li><p>getBucketNotificationConfiguration</p></li>
<li><p>getBucketPolicy</p></li>
<li><p>getBucketPolicyStatus</p></li>
<li><p>getBucketReplication</p></li>
<li><p>getBucketTagging</p></li>
<li><p>getBucketVersioning</p></li>
<li><p>getBucketWebsite</p></li>
<li><p>getObjectAcl</p></li>
<li><p>getObjectLegalHold</p></li>
<li><p>getObjectLockConfiguration</p></li>
<li><p>getObjectRetention</p></li>
<li><p>getObjectTagging</p></li>
<li><p>getPublicAccessBlock</p></li>
<li><p>getSignedUrl</p></li>
<li>listBuckets</li>
<li><p>listBucketAnalyticsConfigurations</p></li>
<li><p>listBucketInventoryConfigurations</p></li>
<li><p>listBucketMetricsConfigurations</p></li>
<li><p>listMultipartUploads</p></li>
<li><p>listObjectVersions</p></li>
<li><p>listParts</p></li>
<li>putBucketLogging</li>
<li>putBucketAnalyticsConfiguration</li>
<li><p>putBucketLifecycleConfiguration</p></li>
<li><p>putBucketMetricsConfiguration</p></li>
<li><p>putBucketNotification</p></li>
<li><p>putBucketNotificationConfiguration</p></li>
<li><p>putBucketPolicy</p></li>
<li><p>putBucketReplication</p></li>
<li><p>putBucketRequestPayment</p></li>
<li><p>putBucketTagging</p></li>
<li><p>putBucketVersioning</p></li>
<li><p>putObjectAcl</p></li>
<li><p>putObjectLegalHold</p></li>
<li><p>putObjectLockConfiguration</p></li>
<li><p>putObjectRetention</p></li>
<li><p>putObjectTagging</p></li>
<li><p>putPublicAccessBlock</p></li>
<li><p>putBucketAccelerateConfiguration</p></li>
<li><p>putBucketAcl</p></li>
<li><p>putBucketCors</p></li>
<li><p>putBucketEncryption</p></li>
<li><p>putBucketInventoryConfiguration</p></li>
<li><p>putBucketLifecycle</p></li>
<li>putBucketLogging</li>
<li><p>upload</p></li>
<li><p>uploadPart</p></li>
<li><p>uploadPartCopy</p></li>
</ul></td>
<td class="confluenceTd"><ul>
<li>abortMultipartUpload</li>
<li>completeMultipartUpload</li>
<li>copyObject</li>
<li>createBucket</li>
<li>createMultipartUpload</li>
<li>deleteBucket</li>
<li>deleteBucketAnalyticsConfiguration</li>
<li>deleteBucketCors</li>
<li>deleteBucketEncryption</li>
<li>deleteBucketIntelligentTieringConfiguration</li>
<li>deleteBucketInventoryConfiguration</li>
<li>deleteBucketLifecycle</li>
<li>deleteBucketMetricsConfiguration</li>
<li>deleteBucketOwnershipControls</li>
<li>deleteBucketPolicy</li>
<li>deleteBucketReplication</li>
<li>deleteBucketTagging</li>
<li>deleteBucketWebsite</li>
<li>deleteObjectTagging</li>
<li>deletePublicAccessBlock</li>
<li>destroy</li>
<li>getBucketAccelerateConfiguration</li>
<li>getBucketAcl</li>
<li>getBucketAnalyticsConfiguration</li>
<li>getBucketCors</li>
<li>getBucketEncryption</li>
<li>getBucketIntelligentTieringConfiguration</li>
<li>getBucketInventoryConfiguration</li>
<li>getBucketLifecycleConfiguration</li>
<li>getBucketLocation</li>
<li>getBucketLogging</li>
<li>getBucketMetricsConfiguration</li>
<li>getBucketNotificationConfiguration</li>
<li>getBucketOwnershipControls</li>
<li>getBucketPolicy</li>
<li>getBucketPolicyStatus</li>
<li>getBucketReplication</li>
<li>getBucketRequestPayment</li>
<li>getBucketTagging</li>
<li>getBucketVersioning</li>
<li>getBucketWebsite</li>
<li>getObjectAcl</li>
<li>getObjectLegalHold</li>
<li>getObjectLockConfiguration</li>
<li>getObjectRetention</li>
<li>getObjectTagging</li>
<li>getPublicAccessBlock</li>
<li>headBucket</li>
<li>headObject</li>
<li>listBucketAnalyticsConfigurations</li>
<li>listBucketIntelligentTieringConfigurations</li>
<li>listBucketInventoryConfigurations</li>
<li>listBucketMetricsConfigurations</li>
<li>listBuckets</li>
<li>listMultipartUploads</li>
<li>listObjectVersions</li>
<li>listParts</li>
<li>putBucketAccelerateConfiguration</li>
<li>putBucketAcl</li>
<li>putBucketCors</li>
<li>putBucketEncryption</li>
<li>putBucketIntelligentTieringConfiguration</li>
<li>putBucketInventoryConfiguration</li>
<li>putBucketLifecycleConfiguration</li>
<li>putBucketLogging</li>
<li>putBucketMetricsConfiguration</li>
<li>putBucketNotificationConfiguration</li>
<li>putBucketOwnershipControls</li>
<li>putBucketPolicy</li>
<li>putBucketReplication</li>
<li>putBucketRequestPayment</li>
<li>putBucketTagging</li>
<li>putBucketVersioning</li>
<li>putBucketWebsite</li>
<li>putObjectAcl</li>
<li>putObjectLegalHold</li>
<li>putObjectLockConfiguration</li>
<li>putObjectRetention</li>
<li>putObjectTagging</li>
<li>putPublicAccessBlock</li>
<li>restoreObject</li>
<li>selectObjectContent</li>
<li>send</li>
<li>uploadPart</li>
<li>uploadPartCopy</li>
<li>writeGetObjectResponse</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>AbortMultipartUploadCommand</li>
<li>CompleteMultipartUploadCommand</li>
<li>CreateMultipartUploadCommand</li>
<li>DeleteBucketAnalyticsConfigurationCommand</li>
<li>DeleteBucketCorsCommand</li>
<li>DeleteBucketEncryptionCommand</li>
<li>DeleteBucketIntelligentTieringConfigurationCommand</li>
<li>DeleteBucketInventoryConfigurationCommand</li>
<li>DeleteBucketLifecycleCommand</li>
<li>DeleteBucketMetricsConfigurationCommand</li>
<li>DeleteBucketOwnershipControlsCommand</li>
<li>DeleteBucketPolicyCommand</li>
<li>DeleteBucketReplicationCommand</li>
<li>DeleteBucketTaggingCommand</li>
<li>GetBucketAccelerateConfigurationCommand</li>
<li>GetBucketAclCommand</li>
<li>DeleteBucketWebsiteCommand</li>
<li>DeleteObjectTaggingCommand</li>
<li>DeletePublicAccessBlockCommand</li>
<li>GetBucketAnalyticsConfigurationCommand</li>
<li>GetBucketCorsCommand</li>
<li>GetBucketEncryptionCommand</li>
<li>GetBucketIntelligentTieringConfigurationCommand</li>
<li>GetBucketInventoryConfigurationCommand</li>
<li>GetBucketLifecycleConfigurationCommand</li>
<li>GetBucketLocationCommand</li>
<li>GetBucketLoggingCommand</li>
<li>GetBucketMetricsConfigurationCommand</li>
<li>GetBucketNotificationConfigurationCommand</li>
<li>GetBucketOwnershipControlsCommand</li>
<li>GetBucketPolicyCommand</li>
<li>GetBucketPolicyStatusCommand</li>
<li>GetBucketReplicationCommand</li>
<li>GetBucketRequestPaymentCommand</li>
<li>GetBucketTaggingCommand</li>
<li>GetBucketVersioningCommand</li>
<li>GetBucketWebsiteCommand</li>
<li>GetObjectAclCommand</li>
<li>GetObjectLegalHoldCommand</li>
<li>GetObjectLockConfigurationCommand</li>
<li>GetObjectRetentionCommand</li>
<li>GetObjectTaggingCommand</li>
<li>GetPublicAccessBlockCommand</li>
<li>HeadBucketCommand</li>
<li>HeadObjectCommand</li>
<li>ListBucketAnalyticsConfigurationsCommand</li>
<li>ListBucketIntelligentTieringConfigurationsCommand</li>
<li>ListBucketInventoryConfigurationsCommand</li>
<li>ListBucketMetricsConfigurationsCommand</li>
<li>ListMultipartUploadsCommand</li>
<li>ListObjectVersionsCommand</li>
<li>ListPartsCommand</li>
<li>PutBucketAccelerateConfigurationCommand</li>
<li>PutBucketAclCommand</li>
<li>PutBucketAnalyticsConfigurationCommand</li>
<li>PutBucketCorsCommand</li>
<li>PutBucketEncryptionCommand</li>
<li>PutBucketIntelligentTieringConfigurationCommand</li>
<li>PutBucketInventoryConfigurationCommand</li>
<li>PutBucketLifecycleConfigurationCommand</li>
<li>PutBucketLoggingCommand</li>
<li>PutBucketMetricsConfigurationCommand</li>
<li>PutBucketNotificationConfigurationCommand</li>
<li>PutBucketOwnershipControlsCommand</li>
<li>PutBucketPolicyCommand</li>
<li>PutBucketReplicationCommand</li>
<li>PutBucketRequestPaymentCommand</li>
<li>PutBucketTaggingCommand</li>
<li>PutBucketVersioningCommand</li>
<li>PutBucketWebsiteCommand</li>
<li>PutObjectAclCommand</li>
<li>PutObjectLegalHoldCommand</li>
<li>PutObjectLockConfigurationCommand</li>
<li>PutObjectRetentionCommand</li>
<li>PutObjectTaggingCommand</li>
<li>PutPublicAccessBlockCommand</li>
<li>UploadPartCommand</li>
<li>UploadPartCopyCommand</li>
<li>WriteGetObjectResponseCommand</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><ul>
<li>putObject</li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>putObject</li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>RestoreObjectCommand</li>
<li>PutObjectCommand</li>
<li>CopyObjectCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><ul>
<li>deleteBucket</li>
<li><p>deleteObject</p></li>
<li><p>deleteObjects</p></li>
</ul></td>
<td class="confluenceTd"><ul>
<li>deleteBucket</li>
<li><p>deleteObject</p></li>
<li><p>deleteObjects</p></li>
</ul></td>
<td class="confluenceTd"><ul>
<li>DeleteBucketCommand</li>
<li>DeleteObjectCommand</li>
<li>DeleteObjectsCommand</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><ul>
<li>getObject</li>
<li>getObjectTorrent</li>
<li><p>listObjects</p></li>
<li><p>listObjectsV2</p></li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>getObject</li>
<li>getObjectTorrent</li>
<li>listObjects</li>
<li>listObjectsV2</li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>GetObjectCommand</li>
<li>ListObjectsCommand</li>
<li>ListObjectsV2Command</li>
<li>SelectObjectContentCommand</li>
<li>GetObjectTorrentCommand</li>
<li>CopyObjectCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><ul>
<li>putBucketAnalyticsConfiguration</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>putBucketAnalyticsConfiguration</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>RestoreObjectCommand</li>
<li>PutObjectCommand</li>
<li>CopyObjectCommand</li>
</ul></td>
</tr>
</tbody>
</table>

## Examples

This code will create a S3 Bucket named "BucketTest1" on an AWS server:

``` js
//import  { SNSClient } from "@aws-sdk/client-sns";
// ES5 example
const {S3Client} = require("@aws-sdk/client-s3");
// Set the AWS Region.
const REGION = "us-east-1";
// Create an Amazon S3 service client object.
const s3Client = new S3Client({ region: REGION });
export { s3Client };

const {CreateBucketCommand} = require("@aws-sdk/client-s3");

const {PutObjectCommand} = require("@aws-sdk/client-s3");

const {DeleteBucketCommand} = require("@aws-sdk/client-s3");

import {path} from "path";
import {fs} from "fs";

const file = "OBJECT_PATH_AND_NAME"; // Path to and name of object. For example '../myFiles/index.js'.
const fileStream = fs.createReadStream(file);

export const bucket = {
  Bucket: "BucketTest1",
  ACL : "public-read'"
};

// Create the Amazon S3 bucket.
export const runTest = async () => {
  try {
    const data = await s3Client.send(new CreateBucketCommand(bucket));
    console.log("Success", data);
    return data; // For unit tests.
  } catch (err) {
    console.log("Error", err);
  }
};
runTest();

export const uploadParams = {
  Bucket: "BucketTest1",
  // Add the required 'Key' parameter using the 'path' module.
  Key: path.basename(file),
  // Add the required 'Body' parameter
  Body: fileStream,
};

// Upload file to specified bucket.
export const runTestPut = async () => {
  try {
    const data = await s3Client.send(new PutObjectCommand(uploadParams));
    console.log("Success", data);
    return data; // For unit tests.
  } catch (err) {
    console.log("Error", err);
  }
};
runTestPut();

// Upload file to specified bucket.
export const runTestDelete = async () => {
  try {
    const data = await s3Client.send(new DeleteBucketCommand(bucket));
    console.log("Success", data);
    return data; // For unit tests.
  } catch (err) {
    console.log("Error", err);
  }
};
runTestDelete();
```

The code listed above will produce the following results:

![](../../../images-results/628851191.png)

## Limitations

-   Use of access points is not supported.
