---
title: "Hapi support for Node.js"
linkTitle: "Hapi"
type: "docs"
---

## Introduction

Hapi is an open-source framework for web applications.
The most common use of Hapi is to build web services - see https://hapi.dev/ for more information.

## Objects

This extension creates the application and operation objects:
- a 'NodeJS Application' object when the 'Hapi.server' API or a 'Hapi.Server' instance is found;
- a 'NodeJS Delete Operation' object when the 'route' API of a server is found, and the method key is 'DELETE';
- a 'NodeJS Get Operation' object when the 'route' API of a server is found, and the method key is 'GET';
- a 'NodeJS Post Operation' object when the 'route' API of a server is found, and the method key is 'POST';
- a 'NodeJS Put Operation' object when the 'route' API of a server is found, and the method key is 'PUT'.

| Icon                               | Description             |
|------------------------------------|-------------------------|
| ![](../../../images/628851127.png) | NodeJS Application      |
| ![](../../../images/628851134.png) | NodeJS Delete Operation |
| ![](../../../images/628851141.png) | NodeJS Get Operation    |
| ![](../../../images/628851130.png) | NodeJS Post Operation   |
| ![](../../../images/628851136.png) | NodeJS Put Operation    |

## Supported links

| Link Type     | Caller type                                                                                                                       | Callee type                              |
|---------------|-----------------------------------------------------------------------------------------------------------------------------------|------------------------------------------|
| callLink      | <ul><li>NodeJS Delete Operation</li><li>NodeJS Get Operation</li><li>NodeJS Post Operation</li><li>NodeJS Put Operation</li></ul> | <ul><li>JavaScript function</li></ul>    |
| relyonLink    | <ul><li>NodeJS Application</li></ul>                                                                                              | <ul><li>JavaScript Source Code</li></ul> |

## Example

Take the following codes:

``` js
const Hapi = require('@hapi/hapi');

const init = async () => {
    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route({
        method: 'GET',
        path: '/api',
        handler: (request, h) => {

            return 'Hello World!';
        }
    });

    await server.start();
};
```

In this example, a 'NodeJS Application' and a 'NodeJS Get Operation' objects are created.
A 'relyOn' link is added between 'index' application and 'index.js' source code 
when 'server' API is found in 'index.js' file. A 'call' link is also created between 'api/'
get operation and 'handler' function.

![](../../../images-results/nodejs_hapi.png)
