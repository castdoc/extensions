---
title: "Seneca Microservice support for Node.js"
linkTitle: "Seneca Microservice"
type: "docs"
---

This page explains the support of the [Seneca Microservice](https://senecajs.org/) framework.

Create a service via web-app.js:

``` js
var seneca = require('seneca')()

seneca
  .use('user')
  .use('auth')
  .use('../lib/api.js')
  .client({port:10202,pin:{role:'offer',cmd:'*'}})
  .client({port:10201,pin:{role:'user',cmd:'*'}})


var app = express()

app.use( bodyParser.json() )
app.use( seneca.export('web') )
app.use( express.static('./public') )

app.listen(3000)
```

offer-service:

``` js
require('seneca')()
  .use('../lib/offer')
  .listen(10202)
  .ready(function(){
    this.act({role:'offer',cmd:'provide'},console.log)
  })
```

Define the api.js:

``` js
module.exports = function( options ) {
var seneca = this
var plugin = 'api'

seneca.add( {role:plugin, end:'offer'}, end_offer) 

function end_offer( args, done ) {
var user = args.req$.seneca.user || {}

this.act('role:offer,cmd:provide',{nick:user.nick},done)
}

seneca.act({role:'web', use:{
prefix:'/api/',
pin:{role:plugin,end:'*'},
map:{
'offer': { GET:true },
}
}})

return {name:plugin};
}
```

offer.js:

``` js
module.exports = function( options ) {
  var seneca = this
  var plugin = 'offer'


  seneca.add( {role:plugin, cmd:'provide'}, cmd_provide)
  

  function cmd_provide( args, done ) {
    if( args.nick ) return done(null,{product:'Apple'});

    return done(null,{product:'Orange'});
  }


  return {name:plugin};
}
```

When a service sends an action (seneca.act()):

-   It may be to create a webservice with role:web -
    see: [https://github.com/senecajs/seneca-web/blob/master/docs/providing-routes.md](https://github.com/senecajs/seneca-web/blob/master/docs/providing-routes.md).
-   An action to call the seneca.add

![](../../../images-results/628851288.png)

Webservice RestAPI:

``` js
...

    seneca.act('role:web',{use:{
      prefix:'/product',
      pin:'role:api,product:*',
      startware: verify_token,
      map:{
        star: { 
          GET:true,
          alias:'/:id/star' 
        },
        handle_star:{
          PUT:true,
          DELETE:true,
          POST:true,
          alias:'/:id/star'
        }
      }
...
```

![](../../../images-results/628851288.png)
