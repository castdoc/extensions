---
title: "Amazon Web Services DynamoDB support for Node.js"
linkTitle: "AWS DynamoDB"
type: "docs"
---

See [DynamoDB support for Node.js source code - JavaScript](../../../../../../nosql/overview/dynamodb/nodejs-js/) for more information.