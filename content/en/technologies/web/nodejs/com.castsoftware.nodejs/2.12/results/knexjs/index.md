---
title: "Knex.js support for Node.js"
linkTitle: "Knex.js"
type: "docs"
---

## Introduction

Knex.js is a "batteries included" SQL query builder for PostgreSQL, MSSQL, MySQL, SQLite3,
Oracle - see https://knexjs.org/ for more information.

## Objects

This extension creates the 'NodeJS SQL Query' object when one of the supported Knex APIs is used:
- For the API 'raw', the first argument is evaluated and if it corresponds to a 'NodeJS SQL Query',
a 'NodeJS SQL Query' object is created.
- For the others APIs, a 'NodeJS SQL Query' object is created. The 'NodeJS SQL Query' is partially evaluated
(only the CRUD operations and table names are evaluated).

| Icon                                             | Description             |
|--------------------------------------------------|-------------------------|
| ![](../../../images/628851181.png)               | NodeJS SQL Query        |

## Supported persistence SQL databases

### Supported links

| Link Type     | Supported APIs                                                                                                                              | Caller type                                                                                            | Callee type                                   | Comment                                                                                                                                                                                                                                                                                              |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------|-----------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| callLink      | <ul><li>insert</li><li>update</li><li>save</li><li>fetch</li><li>destroy</li><li>del</li><li>select</li><li>raw</li><li>join APIs</li></ul> | <ul><li>JavaScript function</li><li>JavaScript initialisation</li><li>JavaScript source code</li></ul> | <ul><li>NodeJS SQL Query</li></ul>            |                                                                                                                                                                                                                                                                                                      |
| useInsertLink | <ul><li>insert</li><li>raw</li><li>join APIs</li></ul>                                                                                      | <ul><li>NodeJS SQL Query</li></ul>                                                                     | <ul><li>Table</li><li>Missing Table</li></ul> | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |
| useUpdateLink | <ul><li>update</li><li>save</li><li>fetch</li><li>raw</li><li>join APIs</li></ul>                                                           | <ul><li>NodeJS SQL Query</li></ul>                                                                     | <ul><li>Table</li><li>Missing Table</li></ul> | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |
| useDeleteLink | <ul><li>destroy</li><li>del</li><li>raw</li><li>join APIs</li></ul>                                                                         | <ul><li>NodeJS SQL Query</li></ul>                                                                     | <ul><li>Table</li><li>Missing Table</li></ul> | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |
| useSelectLink | <ul><li>select</li><li>raw</li><li>join APIs</li></ul>                                                                                      | <ul><li>NodeJS SQL Query</li></ul>                                                                     | <ul><li>Table</li><li>Missing Table</li></ul> | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |

For the API 'raw', possibly all four link types can be created depending on query value.

The APIs 'join' ('join', 'leftJoin', 'leftOuterJoin', 'rightJoin', 'joinRaw',
'rightOuterJoin', 'outerJoin', 'fullOuterJoin', 'crossJoin')
can be used to specify joins between tables.
For each API 'join' used with one of the supported APIs listed above,
a 'NodeJS SQL Query' object is created.

## Example

### Take the following codes of javascript file:

``` js
const knex = require('knex')({
    client: 'sqlite3',
    connection: {
    }
});

function createBook(book) {
    knex.raw('INSERT INTO books_1 VALUES ?');
    knex('books_2').insert(book);
    knex.table('books_3').insert(book);
}

function selectBook() {
    knex.select('title', 'author', 'year').from('books_2');
}

function updateBook(id, newDetails) {
    knex.table('books_3').where('id', id).update(newDetails);
}
```
In this example, five 'NodeJS SQL Query' objects are created.
The [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer)
or [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable)
links these queries with the corresponding Missing Tables.

![](../../../images-results/nodejs_knex_1.png)

### Take another example with API 'join':

``` js
const knex = require('knex')({
    client: 'sqlite3',
    connection: {
    }
});

const join_tables = knex('users as u')
    .join('contacts AS c', 'u.id', '=', 'c.id')
    .select('c.id', 'u.username', 'c.contents')
```
In this example, the API 'join' is used to join tables 'users' and 'contacts'.
This extension creates two 'NodeJS SQL Query' objects.
The [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer)
or [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable)
then links these queries with the corresponding tables.
In the present case, this extension creates two 'useSelect' links from each query
to its corresponding missing tables 'contacts' or 'users'.

![](../../../images-results/nodejs_knex_2.png)
