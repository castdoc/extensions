---
title: "Sails.js support for Node.js"
linkTitle: "Sails.js"
type: "docs"
---

## Introduction

This page explains the support of the [Sails.js](https://sailsjs.com/) framework.

## Server

For example, create a server - app.js:

``` js
...
  // Start server
  sails.lift(rc('sails'));
...
```

This will give the following result:

![](../../../images-results/628851284.png)

## Routes

Routes control at config/routes.js:

``` js
...
'GET /site/:idSite' : {controller: "Site", action: "getSite", rel: RelServices.REL_ENUM.GET_VIEWED_SITE},
...
'PUT /alert' : {controller: "Alert", action: "putAlert", rel: RelServices.REL_ENUM.PUT_ALERT, profile: ProfileServices.PROFILE_ENUM.OPERER},
...
```

Controller actions:

SiteController.js

``` js
...
self.getSite = function (req, res) {
  ...
  var promise = Site.findOne({
    idSite: idSite
  });
  ...
};
```

AlertController.js

``` js
...
self.putAlert = function (req, res) {
  ...
  var promise = Alert.findOne({
    alertId: alertId
  });
  ...
};
```

Model definition:

Site.js:

``` js
...
self.connection = 'postgresqlServer';

self.tableName = 'T_SITE';


self.attributes = {
...
}
...
```

Alert.js

``` js
...
self.connection = 'postgresqlServer';

self.tableName = 'T_ALERT';


self.attributes = {
...
}
...
```

Transaction from get operation method to database when using the [SQL
Analyzer](SQL_Analyzer):

![](../../../images-results/628851285.png)
