---
title: "GCP Cloud Storage support for Node.js"
linkTitle: "GCP Cloud Storage"
type: "docs"
---

## Introduction

[Google Cloud Storage](https://cloud.google.com/storage) is a file
storage service in the cloud. This is the equivalent service to Amazon
S3 provided by Google Cloud Platform (GCP). CAST supports the three
relevant classes [@google-cloud/storage](https://www.npmjs.com/package/@google-cloud/storage)
for CRUD-like link creation:

-   Storage
-   Bucket
-   File

## Objects

This extension, when the NodeJS client library for Google Storage is
found in the source code, may create the following objects representing
Storage buckets. When the name of a given referred bucket in the code is
not resolved (either because of unavailable data or required technical
complexity), an unknown bucket object will be created with name Unknown.
There can be a maximum of one unknown table per project.

| Icon | Object | Name |
|---|---|---|
| ![](../../../images-results/628851239.png) | NodeJS GCP Cloud Storage Bucket | Name of the bucket |
| ![](../../../images-results/628851240.png) | NodeJS GCP Unknown Cloud Storage Bucket | Unknown |

## API methods

A Storage Bucket object will be created when its name is resolved in
calls to the method Storage.bucket() for example*. *The two methods
creating objects:

| API method             | Object creation                           |
|------------------------|-------------------------------------------|
| Storage.bucket()       | NodeJS GCP (Unknown) Cloud Storage Bucket |
| Storage.createBucket() | NodeJS GCP (Unknown) Cloud Storage Bucket |

Method calls involving CRUD-like operations are represented with links
from the respective caller objects to Storage Buckets. The supported
methods are listed in the table below:

| CRUD-like API methods    | Link type                                   |
|--------------------------|---------------------------------------------|
| Bucket.combine()         | useInsertLink, useUpdateLink                |
| Bucket.deleteFiles()     | useDeleteLink                               |
| Bucket.upload()          | useInsertLink, useUpdateLink                |
| File.copy()              | useSelectLink, useInsertLink, useUpdateLink |
| File.createReadStream()  | useSelectLink                               |
| File.createWriteStream() | useInsertLink, useUpdateLink                |
| File.delete()            | useDeleteLink                               |
| File.download()          | useSelectLink                               |
| File.save()              | useInsertLink, useUpdateLink                |

## Example

The following code illustrates a simple utilization of the NodeJS SDK
for downloading a file from Google Storage:

downloadFile.js

``` js
const bucketName = 'myBucketName';
const {Storage} = require('@google-cloud/storage');

// Client
const storage = new Storage();

async function downloadFile() {
    
    const options = {
      destination: 'destFileName',
    };

    // Downloads the file
    await storage.bucket(bucketName).file(fileName).download(options);
}
```

When the code listed above is analyzed, a corresponding bucket object
and a useSelectLink link is created:

![](../../../images-results/628851241.png)

## Limitations

-   Files-list parameters from callbacks of Bucket.getFiles() and
    Bucket.getFilesStream() are not interpreted.
