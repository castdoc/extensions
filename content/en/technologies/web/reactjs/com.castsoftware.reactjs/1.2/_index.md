---
title: "ReactJS - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.reactjs

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for the ReactJS and React Native
framework. 

When the source code is written in TypeScript, the support for React is
provided by the [TypeScript extension](../../../typescript/com.castsoftware.typescript/).

### In what situation should you install this extension?

If your Web application contains ReactJS or React Native source
code and you want to view these object types and their links with other
objects, then you should install this extension:

- creates HTML fragments, ReactJS applications, ReactJS components and ReactJS Forms.
- creates links between these objects.

![](../images/418185648.png)

*ReactJS Javascript Front-end connected to Node.js/Express/MongoDB
Back-end*

## Supported versions

The following table displays the list of ReactJS versions that this
extension supports:

| Version   | Supported   |
|---|:--:|
| 15.x | :white_check_mark: |
| 16.x | :white_check_mark: |
| 17.x | :white_check_mark: |

## Files analyzed

| Icon(s) | File | Extension | Notes |
|---|---|---|---|
| ![](../images/418185678.png) |  HTML  | *.html, *.htm, *.xhtml |  Creates one "HTML5 Source Code" object that is the caller of html to js links and a transaction entry point<br><br>Broadcasts tags and attributes/values to other CAST extensions such as com.castsoftware.angularjs. Other extensions will not need to analyze the files themselves.  |
| ![](../images/418185674.png) | Javascript | *.js | Creates Functions, Classes and Constructors<br><br>Local call links between function calls and functions inside each JavaScript file.  |
| ![](../images/418185673.png) | Cascading Style Sheet | *.css | No specifc objects are created  |
| ![](../images/418185669.png) | Java Server Page | *.jsp | - |
| ![](../images/418185683.png) | JSX | *.jsx |   |
| ![](../images/418185672.png)![](../images/418185671.png) | Active Server Page | *.asp, *.aspx | - |
| ![](../images/418185670.png) | HTML Components | *.htc | HTC files contain html, javascript fragments that will be parsed. Created objects will be linked to the HTC file. |
| ![](../images/418185668.png) | .NET Razor | *.cshtml | - |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The ReactJS extension requires that the
following other CAST extensions are also installed (this is handled automatically):

-   [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)
-   [Web Services Linker](../../../../multi/com.castsoftware.wbslinker/)

## Download and installation instructions

The extension will be automatically downloaded and installed when you deliver React code.

## What results can you expect?

### Objects

| Icon | Metamodel name |
|---|---|
| ![](../images/CAST_ReactJS_Application.png) | ReactJS Application |
| ![](../images/CAST_ReactJS_Component.png) | ReactJS Component |
| ![](../images/CAST_ReactJS_Function_Component.png) | ReactJS Function Component |
| ![](../images/418185654.png) | HTML5 HTML fragment |
| ![](../images/CAST_ReduxJS_ReducerHandler.png) | ReduxJS Handler |

### Examples

#### ReactJS Application

The Application name is the file name or the parent directory name if
the file name is "index". The following declarations will create
a ReactJS application:

html fragment in ReactDOM.render() call:

``` xml
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';

ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <App version={appVersion} />
    </Provider>
  </AppContainer>,
  appElement
);
```

html fragment containing "Provider" or "AppContainer" tags, in or out
of ReactDOM.render() call:

``` xml
import { AppContainer } from 'react-hot-loader';

<AppContainer>
      <App version={appVersion} />
</AppContainer>
```

In the code examples shown above, a relyon link is created from the
application to the HTML fragment of the application:

![](../images/613777423.png)

#### ReactJS Component

This declaration will create a ReactJS component named
UserContainer because the class inherits from Component or
PureComponent. A component corresponds to a class which contains a
render method. The render method is automatically called when the
component is referred to in an HTML fragment.

In the function "mapDispatchToProps", there are mapping of functions
which are used to find resolutions from HTML fragments to functions. The
example below shows a "deleteUsers" function:

``` xml
import React, { Component } from 'react';
 
 class UserContainer extends Component {
 
  componentDidUpdate(prevProps) {
...
  }
  componentDidMount = () { ... };

  render() {
    return (
      <g>
        {this.props.sequence.map((nucleotide, index) =>
          <Nucleotide
            type={nucleotide}
            position={this.props.positionFrom + index}
            key={index}
            index={index}
            onClick={this.props.onNucleotideClick}
            {...this.props}
          />,
        )}
      </g>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    createUsers: bindActionCreators(createUsers, dispatch),
    searchUsers: bindActionCreators(searchUsers, dispatch),
    getUsers: bindActionCreators(getUsers, dispatch),
    triggerNotification: bindActionCreators(triggerNotification, dispatch),
    resetUsersActionStatus: bindActionCreators(resetUsersActionStatus, dispatch),
    deleteUsers: bindActionCreators(deleteUsers, dispatch),
    resetSearchUserApiStatus: bindActionCreators(resetSearchUserApiStatus, dispatch),
    getRoles: bindActionCreators(getRoles, dispatch),
    setConfigDetails: bindActionCreators(setConfigDetails, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    loginUsersList: state.admin.loginUsersList,
    imagingUsersList: state.admin.imagingUsersList,
    usersActionStatus: state.admin.usersActionStatus,
    searchUserApiStatus: state.admin.searchUserApiStatus,
    securityMode: state.login.securityMode,
    deleteUserStatus: state.admin.deleteUserStatus,
    rolesList: state.admin.rolesList,
  };
}

export default compose(withStyles(tableStyles), connect(mapStateToProps, mapDispatchToProps))(UserContainer);
```

The code example above will give the following results:

![](../images/613777419.png)

![](../images/613777420.png)

-   call links are created from the component to its following
    methods when they exist:
    -   'render'
    -   'shouldComponentUpdate'
    -   'componentWillReceiveProps'
    -   'componentWillUpdate'
    -   'componentWillMount'
    -   'componentWillUnmount'
    -   'componentDidUpdate'
    -   'componentDidMount'
    -   'componentDidUnmount'
-   these components may be called from html fragments anywhere in code,
    for example:

``` java
 <Route
        exact={true}
        path={`${basePath}/apps`}
        component={UserContainer}
      />
```

or

``` java
 <UserContainer ... />
```

-   Functions referred to in the "mapDispatchToProps" function may
    be referenced anywhere in the code for example:

``` java
          <AlertDialog
            onClick={this.deleteUsers}
          />
```

#### ReactJS Function Component

This declaration will create a ReactJS function component named
ReportContainer because the function is exported in one of the
following statements:

-   export default withRouter(connect(mapStateToProps,
    mapDispatchToProps)(ReportContainer));

-   export default connect(null, mapDispatchToProps)(ReportContainer);

-   export default connect(mapStateToProps,
    mapDispatchToProps)(withStyles(stylesTheme)(ReportContainer));

-   export default connect(mapStateToProps,
    mapDispatchToProps)(RedirectIfNotGranted(ReportContainer,
    'STORE_ORDER_READ', 'store'));

-   export default withStyles(styles)(connect(mapStateToProps,
    mapDispatchToProps, null, {withRef: true})(ReportContainer));

-   export default compose(withStyles(styles), connect(mapStateToProps,
    mapDispatchToProps))(ReportContainer);

-   export default withStyles(styles)(ReportContainer);

-   export default compose(withReducer, withSaga,
    withConnect)(injectIntl(ReportContainer));

-   export default memo(ReportContainer);

When none of these exports are present, but there is one simple function
export and this function contains a "return" statement returning a jsx
expression, a reactjs function component is also created.

For example, the file report/img-report-container.jsx:

``` xml
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
 
const ReportContainer = (props) => {
  const { isReportDialog, reportList, appName, reportDownloadQueue, reportQueue,
    reportActions, triggerDownload: { nextDownload, activeReportId } } = props;
...
}

function mapStateToProps(state) {
  return {
    isReportDialog: state.report.isReportDialog,
    reportList: state.report.reportList,
    appName: state.dataSource.selectedApp.value,
    reportDownloadQueue: state.report.reportDownloadQueue,
    reportQueue: state.report.reportQueue,
    triggerDownload: state.report.triggerDownload,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reportActions: bindActionCreators(Object.assign({}, Actions), dispatch),
    triggerNotification: bindActionCreators(triggerNotification, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ReportContainer);
```

Will give the following results:

![](../images/613777421.png)

-   A call link is created from the function component to its
    function.
-   These components may be called from html fragments anywhere in code,
    for example:

``` java
import ReportDialog from '../report/img-report-container';
...
 <ReportDialog />
```

-   The same is true for the "mapDispatchToProps" function as for
    ReactJS components.

#### HTML fragment

This declaration will create an HTML fragment named
render_fragment_1 starting with "\<g\>" and ending with
"\</g\>". There can be several fragments in any function/method:

``` xml
render() {
  return (
    <g>
      {this.props.sequence.map((nucleotide, index) =>
        <Nucleotide
          type={nucleotide}
          position={this.props.positionFrom + index}
          key={index}
          index={index}
          onClick={this.props.onNucleotideClick}
          {...this.props}
        />,
      )}
    </g>
  );
}
```

When you have purely JavaScript code inside an html fragment, the
JavaScript code is analyzed and you will obtain links exactly as you
would have in conventional JavaScript code. You can also have JavaScript
functions inside HTML fragments, in this case, functions are children of
the HTML fragment.

In the code example for the ReactJS application above, we can see that
it contains also a HTML fragment.

#### Redux support

The React Redux library, often used in combination with ReactJS, is
supported. A specific object "ReduxJS handler" is created.

##### Imports

the following imports are supported:

-   redux

-   redux-saga

-   redux-saga/effects

-   redux-thunk

-   @reduxjs/toolkit

-   redux-define

Examples:

``` java
import {createStore, applyMiddleware} from 'redux'
import {combineReducers} from 'redux'
import { takeLatest } from 'redux-saga';
import thunkMiddleware from 'redux-thunk';
import { createSlice } from '@reduxjs/toolkit';
import { createAction } from '@reduxjs/toolkit';
import { bindActionCreators } from 'redux';
import { call, fork, put, takeLatest, select } from 'redux-saga/effects';
import { defineAction } from 'redux-define';
```

##### Calls

The following calls are supported:

-   createSlice
-   createAction
-   defineAction
-   useDispatch
-   combineReducers
-   injectReducer
-   dispatch
-   takeLatest, takeEvery, take, takeMaybe, all, race, put, call, fork,
    select (all imports from redux-saga/effects)

Examples

![](../images/650739715.png)

``` java
import { call, fork, put, takeLatest, select } from 'redux-saga/effects';

yield put({ type: reportConstants.ADD_NEW_REPORT, ...});
```

``` java
export default function reportReducer(state = initialState, action) {
  switch (action.type) {
    case constants.ADD_NEW_REPORT: return { ...state, reportDownloadQueue: action.payload };
    case constants.TRIGGER_REPORT_DOWNLOAD: return { ...state, triggerDownload: { ...action.payload } };
    default: return state;
  }
}
```

ADD_NEW_REPORT handler is on "return { ...state, reportDownloadQueue:
action.payload };" code (the "case" corresponding to ADD_NEW_REPORT).

The "reportReducer" function is seen as the parent of handlers by the
presence of following code:

``` java
import { applyMiddleware, createStore, combineReducers } from 'redux';

const appReducer = combineReducers({
  report: report.ReportReducer,
  smartSearch: smartSearch.SmartSearchReducer,
  overview: overview.OverviewReducer,
  tileExpand: tileExpand.TileExpandReducer,
  configCenter: configCenter.ConfigReduces,
});
```

![Alt text](../images/650739714.png)

``` java
import { call, fork, put, takeLatest, select } from 'redux-saga/effects';

yield put({ type: constants.GET_USERS_ACTION });
```

``` java
export function* getUsers({ payload }) {
}
```

GET_USERS_ACTION  is on "getUsers" function.

Correspondence between GET_USERS_ACTION and the function is found in
following code (then, the handler is created):

``` java
import { call, fork, put, takeLatest, all } from 'redux-saga/effects';

yield takeLatest(constants.GET_USERS_ACTION, getUsers);
```

### Structural Rules

The following structural rules are provided:

| Release   | Link  |
|----------------|--------------|
| 1.2.18-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.18-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.18-funcrel) |
| 1.2.17-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.17-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.17-funcrel) |
| 1.2.16-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.16-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.16-funcrel) |
| 1.2.15-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.15-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.15-funcrel) |
| 1.2.14-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.14-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.14-funcrel) |
| 1.2.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.13-funcrel) |
| 1.2.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.12-funcrel) |
| 1.2.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.11-funcrel) |
| 1.2.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.10-funcrel) |
| 1.2.9-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.9-funcrel)   |
| 1.2.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.8-funcrel)   |
| 1.2.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.7-funcrel)   |
| 1.2.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.6-funcrel)   |
| 1.2.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.5-funcrel)   |
| 1.2.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.4-funcrel)   |
| 1.2.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.3-funcrel)   |
| 1.2.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.2-funcrel)   |
| 1.2.1-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.1-funcrel)   |
| 1.2.0-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=\|\|1.2.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_reactjs&ref=%7C%7C1.2.0-funcrel)   |

## Known Limitations

-   [React Without
    JSX](https://reactjs.org/docs/react-without-jsx.html)
    is not supported.
