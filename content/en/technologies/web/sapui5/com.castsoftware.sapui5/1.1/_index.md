---
title: "SAPUI5 - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.sapui5

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for SAPUI5, the SAP JavaScript UI
library.

Note that the extension supports SAP Fiori applications using the
SAPUI5 framework for front-end development.

-   SAP Fiori is a new user experience for SAP software. To overcome the
    complexity of traditional SAP GUIs, SAP Fiori has been developed.
-   SAP Fiori focuses mainly on mobility and uses the SAPUI5 Framework
    for front-end development.

### In what situation should you install this extension?

The main purpose of this extension is to modelize the front-end part
(SAPUI5 source code) of your SAP Application and to
identify links between HTTP request services and ABAP operations
/JavaScript functions, to enable linking from the
Web App front-end right through to the back-end. Therefore if your SAP
application contains SAPUI5  source code and you want to view these
object types and their links with other objects, then you should install
this extension.

![](../images/490897451.png)

#### Object structure

-   creates SAPUI5 Applications
-   creates SAPUI5 Views (XML and JS Views Only)
-   creates SAPUI5 Fragments  
-   creates SAPUI5 Libraries
-   creates SAPUI5 Controllers  
-   creates SAPUI5 Model Functions (controller functions accessible from views)
-   creates SAPUI5 Components  
-   creates SAPUI5 OData Models
-   creates SAPUI5 JSON Models  
-   creates SAPUI5 Objects (SAPUI5 objects except controllers, components and models)
-   creates HTML5 Get/Post/Put/delete http requests services
-   creates SAPUI5 ABAP Get/Post/Put/delete operations  
-   creates SAPUI5 ABAP CDS View operations

#### Link structure

-   Creates links from views to controllers
-   Creates links between controllers and components
-   Creates links between javascript functions and Http request services
-   Creates links between ABAP operations and ABAP functions
-   Creates links between Http request services and ABAP operations (through web service linker)

#### Transactions

-   Default entry point is the html file content.
-   Applications and views are Transaction entry-points.
-   SAPUI5 ABAP operations are entry-points when they are not called by another object.
-   JSON Models are Transaction end-points.
-   Http request services are end-points when they do not call another object.

## Technical information

### SAPUI5 Applications

SAPUI5 Applications can be defined in two ways:

#### HTML files

They can be found in an html file (mostly index.html) files where we
can find the following code:

``` xml
 <title>Shopping Cart</title>

    <script
        id='sap-ui-bootstrap'
        type='text/javascript'
        src='../../../../../resources/sap-ui-core.js'
        data-sap-ui-theme='sap_bluecrystal'
        data-sap-ui-libs='sap.m'
        data-sap-ui-compatVersion="edge"
        data-sap-ui-resourceroots='{
            "sap.ui.demo.cart" : "./"
        }' >
    </script>
```

-   This code contains "sap-ui-bootstrap", and "Shopping Cart"
    is the application name.
-   The value of "data-sap-ui-resourceroots" is also used to create
    fullnames of objects.
-   The "Component.js" file present at the same place as
    "index.html" is the main component associated to the
    application.

#### manifest.json

They can be found in a manifest.json file where we can find the
following code:

-   The code contains "sap.app", and the application name is found
    in the title parameter - note that if the title starts with
    "{{", then the application name is founc in id (in the
    example below, the name is "masterdetail".
-   The "Component.js" file present at the same place as
    "manifest.json" is the main component associated to the
    application.
-   The metadata part can be used if not present in the component code
    (see later in the component description).



``` js
{
    "_version": "1.2.0",

    "sap.app": {
        "_version": "1.2.0",
        "id": "sap.ui.demo.masterdetail",
        "type": "application",
        "i18n": "i18n/i18n.properties",
        "title": "{{appTitle}}",
        "description": "{{appDescription}}",

        "applicationVersion": {
            "version": "1.0.0"
        },
        "dataSources": {
            "mainService": {
                "uri": "/here/goes/your/serviceUrl/",
                "type": "OData",
                "settings": {
                    "odataVersion": "2.0",
                    "localUri" : "localService/metadata.xml"
                }
            }
        }
    },
...

    "sap.ui5": {
        "_version": "1.1.0",
        "rootView": {
            "viewName": "sap.ui.demo.masterdetail.view.App",
            "type": "XML",
            "id": "app"
        },
...

        "models": {
            "i18n": {
                "type": "sap.ui.model.resource.ResourceModel",
                "settings": {
                    "bundleName": "sap.ui.demo.masterdetail.i18n.i18n"
                }
            },
            "" : {
                "dataSource": "mainService",
                "settings": {
                    "metadataUrlParams": {
                        "sap-documentation": "heading"
                    }
                }
            }
        },

        "routing": {
            "config": {
                "routerClass": "sap.m.routing.Router",
                "viewType": "XML",
                "viewPath": "sap.ui.demo.masterdetail.view",
                "controlId": "idAppControl",
                "controlAggregation": "detailPages",
                "bypassed": {
                    "target": ["master", "notFound"]
                },
                "async": "true"
            },

            "routes": [
                {
                    "pattern": "",
                    "name": "master",
                    "target": ["object", "master"]
                },
                {
                    "pattern": "Objects/{objectId}",
                    "name": "object",
                    "target": ["master", "object"]
                }
            ],

            "targets": {
                "master": {
                    "viewName": "Master",
                    "viewLevel": 1,
                    "viewId": "master",
                    "controlAggregation": "masterPages"
                },
                "object": {
                    "viewName": "Detail",
                    "viewId": "detail",
                    "viewLevel": 2
                },
                "detailObjectNotFound": {
                    "viewName": "DetailObjectNotFound",
                    "viewId": "detailObjectNotFound"
                },
                "detailNoObjectsAvailable": {
                    "viewName": "DetailNoObjectsAvailable",
                    "viewId": "detailNoObjectsAvailable"
                },
                "notFound": {
                    "viewName": "NotFound",
                    "viewId": "notFound"
                }
            }
        }
    }
}
```

### SAPUI5 Components

SAPUI5 Components are defined by the following code. What is important
is the top part of the file where we can find "sap.ui.define",
"'sap/ui/core/UIComponent'", and "return
UIComponent.extend("sap.ui.demo.cart.Component"".

-   The component name is found in the first parameter of the
    "extend" call.
-   With regard to the main component associated to the application, we
    use the "metadata" part to find views and root.
-   If the metadata part is not present here, it can be present in
    the "manifest.json" file representing the application.
-   A call link is created from the application to the "init"
    function of the main component.
-   A call link is created from the "init" function of a
    component to the "createContent" function of the same component.



``` js
sap.ui.define([
    'sap/ui/core/UIComponent',
    'sap/m/routing/Router',
    'sap/ui/model/resource/ResourceModel',
    'sap/ui/model/odata/ODataModel',
    'sap/ui/model/json/JSONModel'
], function (UIComponent,
            Router,
            ResourceModel,
            ODataModel,
            JSONModel) {

    return UIComponent.extend("sap.ui.demo.cart.Component", {

        metadata: {
            includes : ["css/style.css"],
            routing: {
                config: {
                    routerClass: Router,
                    viewType: "XML",
                    viewPath: "sap.ui.demo.cart.view",
                    controlId: "splitApp",
                    transition: "slide",
                    bypassed: {
                        target: ["home" , "notFound"]
                    }
                },
                routes: [
                    {
                        pattern: "category/{id}",
                        name: "category",
                        target: "categoryView"
                    },
                    {
                        pattern: "category/{id}/product/{productId}",
                        name: "product",
                        target: ["categoryView", "productView"]
                    }
                ],
                targets: {
                    productView: {
                        viewName: "Product",
                        viewLevel: 3,
                        controlAggregation: "detailPages"
                    },
                    categoryView: {
                        viewName: "Category",
                        viewLevel: 2,
                        controlAggregation: "masterPages"
                    }
                }
            }
        },

        init: function () {
            // call overwritten init (calls createContent)
        },

        createContent: function () {
            // create root view
            return sap.ui.view({
                viewName: "sap.ui.demo.cart.view.App",
                type: "XML"
            });
        },
....
    });

});
```

### SAPUI5 XML view

SAPUI5 XML files are found in XML files whose name ends with
".view.xml" where we can find the following code.

-   For XML view detection, we use "mvc:View" where mvc is defined
    as "sap.ui.core.mvc" or "sap.ui.core" or "sap.m".
-   The value of "controllerName" is also used to find the used
    controller.
-   A use link is created from the view to the used controller
-   A call link is created from the view to the "onInit" model
    function of the used controller
-   Some call links are created from the view to the various model
    functions of the controller when they are called from the view (in
    the example below "handleCartButtonPress"
    and "handleAddButtonPress" are called)

``` xml
<mvc:View
    controllerName="sap.ui.demo.cart.view.Product"
    xmlns="sap.m"
    xmlns:core="sap.ui.core"
    xmlns:mvc="sap.ui.core.mvc"
    xmlns:control="control"
    xmlns:h="http://www.w3.org/1999/xhtml">
    <Page
        id="page"
        title="{Name}"
        showNavButton="{device>/isPhone}"
        navButtonPress="handleNavButtonPress" >
        <headerContent>
            <Button
                icon="sap-icon://cart"
                visible="{device>/isPhone}"
                tooltip="{i18n>TO_CART_BUTTON_TOOLTIP}"
                press="handleCartButtonPress" />
        </headerContent>
        <footer>
            <Toolbar>
                <ToolbarSpacer/>
                <Button
                    icon="sap-icon://add"
                    text="{i18n>PRODUCT_ADD_BUTTON_TEXT}"
                    tooltip="{i18n>PRODUCT_ADD_BUTTON_TOOLTIP}"
                    press="handleAddButtonPress" />
            </Toolbar>
        </footer>
    </Page>
</mvc:View>
```

### SAPUI5 JS Views

JS Views are .js files whose name ends with ".view.js". For example:

``` js
sap.ui.jsview("shoppingcart.Category", {
 
    / Specifies the Controller belonging to this View.
    * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
    * @memberOf shoppingcart.Category
    */
    getControllerName : function() {
        return "shoppingcart.Category";
    },
 
    / Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
    * Since the Controller is given to this method, its event handlers can be attached right away.
    * @memberOf shoppingcart.Category
    */
    createContent : function(oController) {
         
        var oList = new sap.m.List({
            id: "listId"
        })
         
        oList.bindItems({
            path : "products>/collection",
            template : new sap.m.StandardListItem({
                title: "{products>category}",
                type: sap.m.ListType.Navigation,
                press:function(evt){
                    oController.categoryListItemPress(evt);
                }
            })
        });
         
        return new sap.m.Page({
            title: "Category",
            content: [oList]
        });
    }
 
});
```

### SAPUI5 Controllers

SAPUI5 Controllers are defined by the following code. What is important
is the top part of the file where we can find "sap.ui.define",
"'sap/ui/core/mvc/Controller'", and "return
Controller.extend("sap.ui.demo.cart.view.Product"":

``` js
sap.ui.define([
    'sap/ui/core/mvc/Controller',
    'sap/ui/demo/cart/model/formatter',
    'sap/m/MessageToast',
    'sap/m/MessageBox'
], function (Controller, formatter, MessageToast, MessageBox) {
    return Controller.extend("sap.ui.demo.cart.view.Product", {
        formatter : formatter,

        onInit : function () {
            var oComponent = this.getOwnerComponent();

            this._router = oComponent.getRouter();
            this._router.getRoute("product").attachPatternMatched(this._routePatternMatched, this);
            this._router.getRoute("cartProduct").attachPatternMatched(this._routePatternMatched, this);

            // register for events
            var oBus = sap.ui.getCore().getEventBus();
            oBus.subscribe("shoppingCart", "updateProduct", this.fnUpdateProduct, this);
        },

        handleAddButtonPress : function () {
        }
    });
});
```

### SAPUI5 Model functions

SAPUI5 Model functions are controller functions which can be called from
a view. They are defined in the same code as used for the Controller
above. For example, "handleAddButtonPress" is a model function.

### SAPUI5 Libraries

SAPUI5 Libraries are defined by the following code. What is important is
the top part of the file where we can find "sap.ui.define", and
where the only statement is a "return" statement which returns a set of
functions:

``` js
sap.ui.define([
    "sap/m/MessageBox",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "sap/m/MessageToast"
], function (MessageBox, JSONModel, Fragment, MessageToast) {
    return {
        _oComponent: null,
        _oResourceBundle: null,
        _oCache: {},

        init: function (oComponent, oResourceBundle) {
            this._oComponent = oComponent;
            this._oResourceBundle = oResourceBundle;
        },
        getODataModel: function () {
            return this._oComponent.getModel();
        },
        ...
   };
});
```

### SAPUI5 XML fragments

SAPUI5 XML fragments are parts of views. They are found in XML files
whose name ends with ".fragment.xml" where we can find the code
example below:

``` xml
<core:FragmentDefinition xmlns="sap.m" xmlns:core="sap.ui.core" xmlns:f="sap.ui.layout.form" xmlns:commons="sap.ui.commons">
    <Carousel>
        <pages>
            <f:SimpleForm title="Server" editable="false" class="sapUiSmallMarginTopBottom">
                <f:content></f:content>
            </f:SimpleForm>
            <f:SimpleForm title="Kitchen" editable="false" class="sapUiSmallMarginTopBottom">
                <f:content></f:content>
            </f:SimpleForm>
            <f:SimpleForm title="Bedroom" editable="false" class="sapUiSmallMarginTopBottom">
                <f:content></f:content>
            </f:SimpleForm>
        </pages>
    </Carousel>
</core:FragmentDefinition>
```

The item "core:FragmentDefinition" is used to the detect the XML
fragment, where core is defined as "sap.ui.core.mvc" or
"sap.ui.core" or "sap.m".

When the following code is found in a controller, this means that the
fragment "smartHome.view.Chart" is added to the view of the
controller in the id "idChartPage" of the view.

``` js
oChartPage = this.getView().byId("idChartPage");
oChartContent = sap.ui.xmlfragment(this.getView().getId(), "smartHome.view.Chart", this);
oChartPage.addContent(oChartContent);
```

Call links from the view to the fragments (when preceding js code is
found in controller) are created:

![](../images/490897454.jpg)

Fragments can also be defined with some ids inside:

``` xml
<core:FragmentDefinition xmlns="sap.m" xmlns:core="sap.ui.core">
    <BusyDialog id="idServerBusyDialog" class="sapUiSizeCompact" title="{i18n>serverBusyDialogWindowTitle}" text="{i18n>serverBusyDialogWindowText}" showCancelButton="true"
        cancelButtonText="{i18n>serverBusyDialogWindowCancelButtonText}" close="onServerBusyDialogWindowClosed"/>
</core:FragmentDefinition>
```

When the following code is found in a controller:

``` js
oServerBusyDialog = this.getView().byId("idServerBusyDialog").open();
oServerBusyDialog = sap.ui.xmlfragment(this.getView().getId(), "smartHome.view.ServerBusyDialog", this);
this.getView().addDependent(oServerBusyDialog);
```

This means that the part of the fragment 
"smartHome.view.ServerBusyDialog" referenced by id "idChartPage"
is opened by the controller (as a modal window) and added to the view.

-   A call link from the view to the fragment is created
-   A call link from the controller function where this code is
    called to the fragment is created

### SAPUI5 JSON Models

SAPUI5 JSON Models are end-points for transactions. They occur where
data is accessed in read or write mode. They are defined when the
following code is found in controllers or components. The model name is
here "cartProducts":

``` js
            var oCartModel = new JSONModel({
                entries: [],
                totalPrice: "0",
                showEditAndProceedButton: false
            });
            this.setModel(oCartModel, "cartProducts");
```

-   An access read link is created from the code to the model when
    the following code is present:

``` js
var oCartModel = this.getView().getModel("cartProducts");
var oCartData = oCartModel.getData();
```

-   An access write link is created from the code to the model when
    the following code is present:

### SAPUI5 OData models

SAPUI5 OData models are end-points for transactions. They occur where
data is accessed in read or write mode. They are defined when the
following code is found in controllers or components. The model name is
here "/sap/opu/odata/IWBEP/EPM_DEVELOPER_SCENARIO_SRV/".

``` js
var oModel = new ODataModel("/sap/opu/odata/IWBEP/EPM_DEVELOPER_SCENARIO_SRV/", true);
oModel.setDefaultCountMode("None");

this.setModel(oModel);
```

### Http resource services

SAP has developed its own functions in order to call ABAP functions
through services. The following statements are supported:

Http Delete resource service (uri
=sap/opu/odata/sap/YMYSERVICE_EFORM_SRV/myUrl):

``` js
var url = "/sap/opu/odata/sap/YMYSERVICE_EFORM_SRV";
var oModel = new sap.ui.model.odata.ODataModel(url, true);
oModel.remove("myUrl(PARAM=...)", {
    success: ...,
    error: ...
    });
```

Http Post resource service (uri
=sap/opu/odata/sap/YMYSERVICE_EFORM_SRV/myUrl):

``` js
var url = "/sap/opu/odata/sap/YMYSERVICE_EFORM_SRV";
var oModel = new sap.ui.model.odata.ODataModel(url, true);
oModel.create("myUrl", c, null, function(oData, response) {
    });
```

Http Put resource service (uri
=sap/opu/odata/sap/YMYSERVICE_EFORM_SRV/myUrl):

``` js
var url = "/sap/opu/odata/sap/YMYSERVICE_EFORM_SRV";
var oModel = new sap.ui.model.odata.ODataModel(url, true);
oModel.udate("myUrl", ...);
```

Http Get resource service (uri
=sap/opu/odata/sap/YMYSERVICE_EFORM_SRV/myUrl?...):

``` js
var url = "/sap/opu/odata/sap/YMYSERVICE_EFORM_SRV";
var oModelData = new sap.ui.model.odata.ODataModel(url, true);
var relPath = "myUrl?...";
oModelData.read(relPath, null, [], false, function(oData, response) {
    });
```

### SAPUI5 ABAP operations

SAP has developed services in ABAP so ABAP methods can be called from
javascript client side. The following statements are supported:

service is identified as a parameter in "set_schema_namespace" method
called in the "DEFINE" method of the class inheriting from
"/IWBEP/CL_MGW_PUSH_ABS_MODEL".

This service name is the prefix of all urls of methods of a same class.

``` java
CLASSPOOL_NAME YCL_MY_CLASSPOOL_MPC .
  method DEFINE.
*&---------------------------------------------------------------------*
*&           Generated code for the MODEL PROVIDER BASE CLASS         &*
*&                                                                     &*
*&  !!!NEVER MODIFY THIS CLASS. IN CASE YOU WANT TO CHANGE THE MODEL  &*
*&        DO THIS IN THE MODEL PROVIDER SUBCLASS!!!                   &*
*&                                                                     &*
*&---------------------------------------------------------------------*
super->define( ).

model->set_schema_namespace( 'YMYSERVICE_EFORM_SRV' ).
```

#### SAPUI5 Delete operation (named /YMYSERVICE_EFORM_SRV/myUrl)

``` java
CLASSPOOL_NAME YCL_MY_CLASSPOOL_MPC_EXT .
  
method /IWBEP/IF_MGW_APPL_SRV_RUNTIME~DELETE_ENTITY.


 DATA lv_entityset_name TYPE string.
lv_entityset_name = io_tech_request_context->get_entity_set_name( ).

CASE lv_entityset_name.


   when 'myUrl'.
*     Call the entity set generated method
     myurl_delete_entity(...).
...
   when others.
     super->/iwbep/if_mgw_appl_srv_runtime~delete_entity(...).
 ENDCASE.
endmethod.

```

#### SAPUI5 Post operation (named /YMYSERVICE_EFORM_SRV/myUrl)

``` java
CLASSPOOL_NAME YCL_MY_CLASSPOOL_MPC_EXT .
  
method /IWBEP/IF_MGW_APPL_SRV_RUNTIME~CREATE_ENTITY.


 DATA lv_entityset_name TYPE string.
lv_entityset_name = io_tech_request_context->get_entity_set_name( ).

CASE lv_entityset_name.


   when 'myUrl'.
*     Call the entity set generated method
     myurl_create_entity(...).
...
   when others.
     super->/iwbep/if_mgw_appl_srv_runtime~create_entity(...).
 ENDCASE.
endmethod.

```

#### SAPUI5 Put operation (named /YMYSERVICE_EFORM_SRV/myUrl)

``` java
CLASSPOOL_NAME YCL_MY_CLASSPOOL_MPC_EXT .
  
method /IWBEP/IF_MGW_APPL_SRV_RUNTIME~UPDATE_ENTITY.


 DATA lv_entityset_name TYPE string.
lv_entityset_name = io_tech_request_context->get_entity_set_name( ).

CASE lv_entityset_name.


   when 'myUrl'.
*     Call the entity set generated method
     myurl_update_entity(...).
...
   when others.
     super->/iwbep/if_mgw_appl_srv_runtime~update_entity(...).
 ENDCASE.
endmethod.

```

#### SAPUI5 Get operation (named /YMYSERVICE_EFORM_SRV/myUrl)

``` java
CLASSPOOL_NAME YCL_MY_CLASSPOOL_MPC_EXT .
  
method /IWBEP/IF_MGW_APPL_SRV_RUNTIME~GET_ENTITYSET.


 DATA lv_entityset_name TYPE string.
lv_entityset_name = io_tech_request_context->get_entity_set_name( ).

CASE lv_entityset_name.


   when 'myUrl'.
*     Call the entity set generated method
     myurl_get_entityset(...).
...
   when others.
     super->/iwbep/if_mgw_appl_srv_runtime~get_entityset(...).
 ENDCASE.
endmethod.

```

Here is a snapshot showing a full transaction from a SAPUI5 XML view to
a SAP table with a link between client and ABAP server (Http request
service and SAPUI5 ABAP operation):

![](..images/490897452.png)

#### SAPUI5 CDS View operation (named /ZCDSAN_PROJ_LIST)

Such an operation is created for one CDS view with annotation

``` java
@OData.publish: true
```

At the end of analysis, operations witch are not called from a http
request service are deleted.

![](../images/529563733.png)

### JQuery resources services

SAP has developed its own functions using the JQuery framework in order
to call web servers. These calls are managed by the [JQuery
extension](jQuery). The following statements are supported:

``` js
jQuery.sap.sjax({
 url: "http://localhost/qmacro/test",
 dataType: "json"
 });


jQuery.sap.syncGet("syncGet_url", { dataType: "json" });


jQuery.sap.syncGetJSON("syncGetJSON_url", { dataType: "json" });


jQuery.sap.syncGetText("syncGetText_url", { dataType: "json" });


jQuery.sap.syncPost("syncPost_url", { dataType: "json" });
```

### Miscellaneous links

#### sap.ui.core.routing.Router.navTo() support

``` java
this.router.navTo("SearchDeliveries");
```

An access link is created from the javascript function to the SAPUI5
view corresponding to the first parameter.

The manifest.json file found at the application root, describing the
SAPUI5 application is used to make the connection between the parameter
and the view (routes section):

``` java
{
    "sap.ui5": {
        "rootView": {
            "viewName": "main.app.TrackingApp.view.TrackingApp",
            "type": "XML"
        },
        "routing": {
            "config": {
                "routerClass": "sap.m.routing.Router",
                "viewType": "XML",
                "async": true,
                "viewPath": "main.app.TrackingApp.view",
                "controlAggregation": "pages",
                "controlId": "idAppControl",
                "clearControlAggregation": false
            },
            "routes": [
                {
                    "name": "SearchDeliveries",
                    "pattern": "SearchDeliveries",
                    "target": [
                        "SearchDeliveries"
                    ]
                },
            ]
        }
    }
}
```

## Supported SAPUI5 versions

| Version        | Supported                                                                                     |
|----------------|:------------------:|
| 1.28 and above | :white_check_mark: |
| 1.26           | :x:                |
| 1.24           | :x:                |
| 1.22           | :x:                |
| 1.20           | :x:                |

## Function Point, Quality and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Function Points(transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |   

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: When using CAST Imaging Core 8.3.41 or later, you must use at least v. 1.1.6-funcrel of the SAPUI5 extension.|

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The SAPUI5 extension requires that the
following other CAST extensions are also installed:

- [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)

Note that when using the CAST Extension Downloader to download the
extension and the Manage Extensions interface in CAST Server
Manager to install the extension, any dependent extensions
are automatically downloaded and installed for you. You do not need
to do anything.

## Download and installation instructions

The extension will be automatically downloaded and installed in AIP
Console when you deliver SAPUI5 source code. You can also manually
install the extension using the Application -
Extensions interface.

## Packaging, delivering and analyzing your source code

Once the extension is installed, no further configuration changes are
required before you can package your source code and run an analysis.
The process of packaging, delivering and analyzing your source code is
as follows:

## Analysis using CAST Imaging Console

CAST Imaging Console exposes the technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Universal Technology (3) in the
[Config](https://doc.castsoftware.com/display/AIPCONSOLE/Application+-+Config) (1)
\>
[Analysis](https://doc.castsoftware.com/display/AIPCONSOLE/Application+-+Config+-+Analysis)
(2) tab to display the available options for your SAPUI5 source
code:

![](../images/530317515.jpg)

Then choose the relevant Analysis Unit (1) to view the
configuration:

![](../images/530317514.jpg)

![](../images/530317513.jpg)

## What results can you expect?

![](../images/490897453.jpg)  

### Objects

| Icon | Description |
|---|---|
| ![](../images/490897457.png) | SAPUI5 Application |
| ![](../images/490897461.png) | SAPUI5 Component |
| ![](../images/490897462.png) | SAPUI5 Controller |
| ![](../images/490897462.png) | SAPUI5 Library |
| ![](../images/490897458.png) | SAPUI5 OData Model |
| ![](../images/490897463.png) | SAPUI5 HTML View |
| ![](../images/490897465.png) | SAPUI5 JS View |
| ![](../images/490897456.png) | SAPUI5 JSON Model |
| ![](../images/490897464.png) | SAPUI5 JSON View |
| ![](../images/490897466.png) | SAPUI5 Model Function |
| ![](../images/490897467.png) | SAPUI5 Object |
| ![](../images/490897455.png) | SAPUI5 XML Fragment |
| ![](../images/490897459.png) | SAPUI5 XML View |
| ![](../images/529563737.png) | SAPUI5 ABAP Get Operation |
| ![](../images/529563736.png) | SAPUI5 ABAP Post Operation |
| ![](../images/529563735.png) | SAPUI5 ABAP Put Operation |
| ![](../images/529563734.png) | SAPUI5 ABAP Delete Operation |
| ![](../images/529563737.png) | SAPUI5 CDS View Operation |

### Structural Rules

The following structural rules are provided:

| Release  | Link  |
|----------|-------|
| 1.1.15-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.15-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.15-funcrel) |
| 1.1.14-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.14-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.14-funcrel) |
| 1.1.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.13-funcrel) |
| 1.1.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.12-funcrel) |
| 1.1.11-funcrel | Extension withdrawn. |
| 1.1.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.10-funcrel) |
| 1.1.9-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.9-funcrel)   |
| 1.1.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.8-funcrel)   |
| 1.1.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.7-funcrel)   |
| 1.1.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.6-funcrel)   |
| 1.1.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.5-funcrel)   |
| 1.1.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.4-funcrel)   |
| 1.1.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.3-funcrel)   |
| 1.1.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=\|\|1.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_sapui5&ref=%7C%7C1.1.2-funcrel)   |
