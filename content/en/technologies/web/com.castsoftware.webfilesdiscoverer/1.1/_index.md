---
title: "Web Files Discoverer - 1.1"
linkTitle: "1.1"
type: "docs"
no_list: true
---

***

### Extension ID

com.castsoftware.webfilesdiscoverer

## What's new?

See [Release Notes](rn/).

## Description

This discoverer detects projects based on the presence of web technology
files (for example .HTML, .JS etc.).

### In what situation should you install this extension?

The [HTML5 and JavaScript](../../html5-js/com.castsoftware.html5/) extension, or any
extension that reuses it ([AngularJS](../../angularjs/com.castsoftware.angularjs/),
[Node.js](../../nodejs/com.castsoftware.nodejs/), [jQuery](../../jquery/com.castsoftware.jquery/) etc.) does not contain a
file "discoverer": this means that no Analysis Units (essential for
a successful analysis) will be created by CAST Console. The Web Files
Discoverer therefore exists purely to create Analysis Units for use
with the [HTML5 and JavaScript](../../html5-js/com.castsoftware.html5/) extension.

### Technically, how does the extension work?

The Web Files Discoverer will detect a project (and therefore
will create an Analysis Unit) when any of the following files are
detected in any of the scanned folders:

-   .js
-   .ts
-   .jsx
-   .tsx
-   .html
-   .htm
-   .xhtml
-   .jsp
-   .jsf
-   .jsff
-   .jspx
-   .xml
-   .json
-   .css
-   .asp
-   .aspx
-   .htc
-   .cshtml
-   .jade
-   .yml
-   .yaml
-   .vue

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :x: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST
Console. You can manage the extension using the Application -
Extensions interface.

The file `code-scanner-config.xml` (available in your Node
installation folder) governs when this extension is installed, i.e. it
is based on the file extensions in your delivered source code:

``` java
<discoverer extensionId="com.castsoftware.webfilesdiscoverer" dmtId="dmtdiscovererhtml5"
    fileExtensions=".js;.ts;.jsx;.tsx;.jsp;.aspx;.cshtml;.xhtml;.htc" label="HTML5 project"/>
```