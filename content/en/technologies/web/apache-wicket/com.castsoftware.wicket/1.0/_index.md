---
title: "Apache Wicket - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.wicket

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for [Apache Wicket](https://wicket.apache.org/) - an open source, component oriented, server-side, Java web application framework.

### In what situation should you install this extension?

The main purpose of this extension is to improve the transaction path between HTML files that contain Apache Wicket resources (tags within the HTML file) and the Java server-side web framework. The analysis of Apache Wicket therefore also requires that the [HTML5 and JavaScript](../../../html5-js/com.castsoftware.html5/) and the [JEE Analyzer](../../../../jee/extensions/com.castsoftware.jee/) are installed.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, should manually install the extension.

## What results can you expect?

### Page initialization: basic support for HTML to Java Constructor

Assuming that the Apache Wicket application is declared correctly and
given the following HelloPeople.html file:

html_wicket Expand source

``` xml
<html>
<body>
    <span wicket:id="message">Message goes here</span>
</body>
</html>
```

and given the corresponding HelloPeople.java file located in the
same folder:

java_wicket Expand source

``` java
package com.example;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class HelloPeople extends WebPage {
    public HelloPeople(PageParameters params) {
        String username = params.get("user").toString();
        add(new Label("message", "Bonjour " + username + "!!!!"));
    }
}
```

Results in Enlighten are :

![](../images/596148375.png)

Java classes can contain multiple Java Constructors. In this
situation, the extension will produce a callLink from the HTML5
source code object to each of the explicitly defined Java
Constructors objects as follows:

![](../images/596410879.png)

CallLinks between HTML5 source code and Java Constructors are
triggered when the path of the HTML object matches the package name of
the Java Constructor.

For this result the current extension adds the following Objects, Links
and Properties:

#### Objects

None

#### Links

|      Caller       |   Caller name    | Type of Link |         Callee          | Callee name |
|:-----------------:|:----------------:|:------------:|:-----------------------:|:-----------:|
| HTML5 source code | HelloPeople.html |   callLink   | Public Java Constructor | HomePeople  |

#### Properties

| Object                  | Object name      | Property name                 | Property value     |
|-------------------------|------------------|-------------------------------|--------------------|
| HTML5 source code       | HelloPeople.html | Use Apache Wicket (1 if Yes)  | 1                  |
| HTML5 source code       | HelloPeople.html | Wicket Ids with Positions     | message#(3,1,3,48) |
| Public Java Constructor | HelloPeople      | Use Apache Wicket (1 if Yes)  | 1                  |

### Page initialization and Detach: Basic support for HTML to onInitialize and onDetach methods

Initialization of the page can also be done through the API java
methods onInitialize and onDetach. For the following java code
file named SignInPage.java and knowing that the
BootstrapBasePage class inherits from
the org.apache.wicket.markup.html.WebPage class: 

java_wicket Expand source

``` java
public class SignInPage extends BootstrapBasePage {
    private String username;
    private String password;
    
    @Override
    protected void onInitialize() {
        super.onInitialize();
        
        StatelessForm form = new StatelessForm("form"){
            @Override
            protected void onSubmit() {
                if(Strings.isEmpty(username) || Strings.isEmpty(password))
                    return;
                
                boolean authResult = AuthenticatedWebSession.get().signIn(username, password);
                
                if(authResult){
                    continueToOriginalDestination();
                    setResponsePage(Application.get().getHomePage());
                }else{
                    error("Username and password are not equal!");
                }
            }
        };
        
        form.setDefaultModel(new CompoundPropertyModel(this));
        
        form.add(new TextField("username"));
        form.add(new PasswordTextField("password"));
        
        form.add(new FeedbackPanel("feedbackPanel"));
        add(form);
    }
}
```

and the associated html file SignIn.html file: 

html_wicket Expand source

``` xml
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
    <wicket:extend>
        <form action="" wicket:id="form">
            <legend>
                Type the same value for username and password to
                authenticate. Use 'superuser' to sign in as ADMIN.
            </legend>
            <br />
            <div style="display: table;border: solid 1px;padding:5px">
                <div style="display: table-row;">
                    <div style="display: table-cell;">Username:</div>
                    <div style="display: table-cell;">
                        <input type="text" wicket:id="username" />
                    </div>
                </div>
                <div style="display: table-row;">
                    <div style="display: table-cell;">Password:</div>
                    <div style="display: table-cell;">
                        <input type="password" wicket:id="password" />
                    </div>
                </div>
            </div>
            <input type="submit" value="Submit" />
            <div wicket:id="feedbackPanel"></div>
        </form>
    </wicket:extend>
</body>
</html>
```

Results in Enlighten are : 

![](../images/603193355.png)

And in CAST Imaging:

![](../images/603193354.jpg)

For this result the current extension adds the following Objects, Links
and Properties:

#### Objects

None

#### Links

| Caller | Caller name | Type of Link | Callee | Callee name |
|---|---|---|---|---|
| HTML5 source code | SignInPage.html | callLink | Protected Java Method | onInitialize |

#### Property

| Object | Object name | Property name | Property value |
|---|---|---|---|
| HTML5 source code | HomePage.html | Use Apache Wicket (1 if Yes)  | 1 |
| HTML5 source code | HomePage.html | Wicket Ids with Positions | form#(27,29,27,35)<br>username#(37,36,37,46)<br>password#(43,40,43,50)<br>feedbackPanel#(48,19,48,34) |
| Private Java Method | onInitialize | Use Apache Wicket (1 if Yes)  | 1 |

Note: `onDetach` methods are treated exactly as onInitialize
methods.

### Event: creation of Cast_Wicket_Event object

For the following Java code snippet and knowing that `BootstrapBasePage` inherits from the `org.apache.wicket.markup.html.WebPage` class: 

java_wicket

``` java
public class HomePage extends BootstrapBasePage {
    private static final long serialVersionUID = 1L;

    public HomePage(final PageParameters parameters) {
        super(parameters);      
        add(new Link<Void>("logOut") {

            @Override
            public void onClick() {
                AuthenticatedWebSession.get().invalidate();
                setResponsePage(getApplication().getHomePage());
            }
        });
    }
}
```

And the associated html file:

html_wicket

``` xml
<!DOCTYPE html>
<html xmlns:wicket="http://wicket.apache.org">
    <head>
        <meta charset="utf-8" />
        <title></title>
    </head>
    <body>
        <wicket:extend>
            <h2>Select one of the following links</h2>
            <wicket:enclosure>
                <span>You are signed in as:</span>
                <span wicket:id="username"></span>
                <br />
                <br />
            </wicket:enclosure>

            <wicket:link>
                <a href="SignInPage.html">Link to sign in page</a>
                <br />
                <a href="admin/AdminOnlyPage.html">
                    Link to admin-only page
                </a>
                <br />
            </wicket:link>
            <br />
            <a wicket:id="logOut">Log out.</a>
        </wicket:extend>
    </body>
</html>
```

Results in Enlighten are: 

![](../images/613777600.png)

And in CAST Imaging:

![](../images/613777599.jpg)

For this result the current extension adds the following Objects, Links and Properties:

#### Objects

| Icon | Type of Object | Name of Object |
|---|---|---|
| ![](../images/606830700.png) | Apache Wicket Event Handler | logOut |

#### Links

| Caller | Caller name | Type of Link | Callee | Callee name |
|---|---|---|---|---|
| HTML5 source code | HomePage.html | callLink | Public Java Constructor | HomePage |
| HTML5 source code | HomePage.html | callLink | Apache Wicket Event Handler | logOut |
| Apache Wicket Event Handler | logOut | callLink | JV_METHOD | get |
| Apache Wicket Event Handler | logOut | callLink | JV_METHOD | getApplication |
| Apache Wicket Event Handler | logOut | callLink | JV_METHOD | getHomePage |
| Apache Wicket Event Handler | logOut | callLink | JV_METHOD | invalidate |

#### Properties

| Object | Object name | Property name | Property value |
|---|---|---|---|
| HTML5 source code | HomePage.html | Use Apache Wicket (1 if Yes)  | 1 |
| HTML5 source code | HomePage.html | Wicket Ids with Positions | username#(30,21,30,31)<br>logOut#(44,17,44,25) |
| Public Java Constructor | HomePage | Use Apache Wicket (1 if Yes)  | 1 |
| Apache Wicket Event Handler | logOut | Type of Action | Link |

### Apache Wicket Event Handler modelization

Whenever a call to a method adding elements to an Apache Wicket Java
class (Java class that inherits any class that contains
"org.apache.wicket")  is found in the source code, this extension
evaluates the name of the wicket_id in which the operation is made and a
corresponding object is created. 

If the evaluation of the wicket_id name fails (either due to missing
information in the source code or to limitations in the evaluation) no
object Apache Wicket Event Handler will be created.

#### Methods triggering an evaluation of wicket_id are:

| Classes | Methods |
|---|---|
| org.apache.wicket.Component | add |
| org.apache.wicket.markup.html.form.Form | add |
| org.apache.wicket.markup.html.form.FormComponent | add |
| org.apache.wicket.page.PartialPageUpdate | add |
| org.apache.wicket.MarkupContainer | add<br>addOrReplace |
| org.apache.wicket.ajax.markup.html.AjaxLink | add |
| org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink | add |
| org.apache.wicket.markup.html.form.RadioChoice | add |
| org.apache.wicket.ajax.AjaxRequestHandler | add |
| org.apache.wicket.markup.html.WebMarkupContainer | add |
| org.apache.wicket.core.request.handler.IPartialPageRequestHandler | add |

#### Supported Handler methods

Following methods of the Apache Wicket API are the
supported *Handler* methods:

| Handler method | Fully Supported |
|---|:-:|
| onClick | :white_check_mark: |
| onSubmit | :white_check_mark: |
| onUpdate | :x: |
| onEvent | :x: |

{{% alert color="info" %}}<ul><li>An Apache Wicket Event Handler object will be created only if a Supported Handler is declared explicitly and is an override of the Apache Wicket API.</li><li>The onUpdate and onEvent Handlers are partially supported: Apache Wicket Event Handler Objects will be created, however links from HTML5 source code to Apache Wicket Event Handler might be missing.</li></ul>{{% /alert %}}
