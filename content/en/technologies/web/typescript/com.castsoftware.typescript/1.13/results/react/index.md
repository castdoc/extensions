---
title: "React and React Native support"
linkTitle: "React and React Native"
type: "docs"
---

## ReactJS Application

This declaration will create a ReactJS application named App:

``` xml
ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <App version={appVersion} />
    </Provider>
  </AppContainer>,
  appElement
);
```

## ReactJS Component

This declaration will create a ReactJS component
named Sequence because the class inherits from Component (a
class that inherits from PureComponent would also induce the creation of
a ReactJS component). A component must contain a render method. The
render method is automatically called when the component is referred to
in an HTML fragment:

``` xml
import React, { Component } from 'react';
 
 class Sequence extends Component {
 
  shouldComponentUpdate(nextProps) {
    return this.props.positionFrom !== nextProps.positionFrom ||
      this.props.sequence !== nextProps.sequence ||
      this.props.nucleotidesPerRow !== nextProps.nucleotidesPerRow ||
      this.props.rowHeight !== nextProps.rowHeight;
  }
 
  render() {
    return (
      <g>
        {this.props.sequence.map((nucleotide, index) =>
          <Nucleotide
            type={nucleotide}
            position={this.props.positionFrom + index}
            key={index}
            index={index}
            onClick={this.props.onNucleotideClick}
            {...this.props}
          />,
        )}
      </g>
    );
  }
}
```

## ReactJS Form

This declaration will create a ReactJS form named contact:

``` xml
import React from 'react'
import { Field, reduxForm } from 'redux-form'

let ContactForm = props => {
  const { handleSubmit } = props
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="firstName">First Name</label>
        <Field name="firstName" component="input" type="text" />
      </div>
      <div>
        <label htmlFor="lastName">Last Name</label>
        <Field name="lastName" component="input" type="text" />
      </div>
      <div>
        <label htmlFor="email">Email</label>
        <Field name="email" component="input" type="email" />
      </div>
      <button type="submit">Submit</button>
    </form>
  )
}

ContactForm = reduxForm({
  // a unique name for the form
  form: 'contact'
})(ContactForm)
```

## HTML fragment

This declaration will create a HTML fragment named render_fragment_1
starting with "\<g\>" and ending with "\</g\>". There can be several
fragments in any function/method:

``` xml
render() {
  return (
    <g>
      {this.props.sequence.map((nucleotide, index) =>
        <Nucleotide
          type={nucleotide}
          position={this.props.positionFrom + index}
          key={index}
          index={index}
          onClick={this.props.onNucleotideClick}
          {...this.props}
        />,
      )}
    </g>
  );
}
```

## Links

### Links from the ReactJS application

-   A relyon link is created from the application to the HTML
    fragment of the application.
-   A call link is created from the fragment to the ReactJS
    component referred to in the fragment
-   A call link is created from the component to the render method
    of the class representing the component (as the render method is
    automatically called).
-   A call link is created from the render method to the fragment
    contained inside the render method

Please remember that components are called mainly from html fragments,
and render methods are implicitly called from ReactJS components.

![](../../../images-results/653984202.png)

### Links from a HTML fragment

HTML tags present in HTML fragments are linked to ReactJS components if
they exist:

``` java
<g> <ChildrenLoading dispatch={props.dispatch} {...this.props} /> </g>
```

![](../../../images-results/653984201.png)

HTML tags present in HTML fragments can also be linked to a Typescript
function. HTML fragments are linked to functions or methods when they
are represented between { ... }:

``` java
<UserSearchSelect dispatch={props.dispatch} label={formatMessage({ defaultMessage: 'Transfer to...' })} />
```

![](../../../images-results/653984202.png)
