---
title: "Node.js - Sequelize support"
linkTitle: "Node.js - Sequelize"
type: "docs"
---

This section describes support for the Node.js Sequilize framework.

Sequelize is a promise-based Node.js ORM for Postgres, MySQL, MariaDB,
SQLite and Microsoft SQL Server. In sequelize, the user defines Models
for data storage. The models are modelized as tables. The following
links are added for these API calls:

| Link type     | API                                                                              |
|---------------|----------------------------------------------------------------------------------|
| useInsertLink | create, bulkCreate                                                               |
| useSelectLink | findAll, findByPk, findOne, findOrCreate,  findAndCountAll, count, max, min, sum |
| useUpdateLink | update, restore, increment, decrement                                            |
| useDeleteLink | destroy                                                                          |

The query API is also supported and a Query object is created as a
result.

Example

In the following code:

``` java
import * as Sequelize from 'sequelize';

const Model = Sequelize.Model;
class User extends Model {}
User.init({
  // attributes
  firstName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  lastName: {
    type: Sequelize.STRING
    // allowNull defaults to true
  }
}, {
  sequelize,
  modelName: 'user'
  tableName: 'users'
  // options
});
function myfind(){
    User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
    });
}
```

...the User class defines a model which is linked with the table
named 'users' (through the User.init() call). The name of the table
is defined by the tableName value which if not defined is set to the
pluralized (if freezeTableName is not set to true) value of
modelName which is itself set to the class name when it is not
explicitly defined. The User.findAll() call then selects elements from
that table 'users'.

In this example, this extension creates a useSelect link to the table
'users':

![](../../../images-results/653984160.png)

Note that a model can also be defined using method
sequelize.define().

The Sequilize framework query method is not supported: if there any
calls to database procedures using the Sequilize API query method, then
missing links are expected.

