---
title: "Node.js - Loopback support"
linkTitle: "Node.js - Loopback"
type: "docs"
---

This section describes support for the Node.js Loopback framework.

Supported APIs from @loopback/rest:

-   get, post, put and del decorators
-   RestApplication.route

For instance the analysis of the following source code will lead to the
creation of  NodeJS Get Operation named ping/ with a callLink to the
ping Method:

``` js
import {get} from '@loopback/rest';

class FooClass{
   @get('/ping', {
    responses: {
      '200': PING_RESPONSE,
    },
  })
  ping(){}
}
```

![](../../../images-results/653984148.png)
