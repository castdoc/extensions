---
title: "Node.js - Microsoft Azure support"
linkTitle: "Node.js - Azure"
type: "docs"
---

## Azure functions, triggers and bindings

Azure functions allow the execution of source code in the cloud. The
execution can be set to be triggered by some Azure events. A source code
implementing a TypeScript azure function contains a function.json file
and an index.ts file. These files are placed in the same directory and
the name of the directory gives the name of the azure function. The
index.ts file contains a default export function with the source code
that will be executed when the azure function is triggered.

The com.castsoftware.nodejs extension is responsible for creating Azure
function objects and supporting the triggers and bindings. It also links
these objects to the TypeScript handler function. The level of support
may depend upon the version of the com.castsoftware.nodejs version you
are using. Please refer to its documentation page to understand the
level of support you have.

## Durable functions

Durable functions allow calling an azure function from another azure
function. There are three kinds of durable functions: the orchestrator
function, the client function and the activity function.

The startNew method from the "durable-function" client allow the client
function to call an orchestrator function.  The callActivity method from
the "durable-function" package allows calling an activity
function.Whenever one of these method calls is found in a JavaScript
source code, a call to Azure function object is created. The name of the
object is evaluated from the first argument of the call. If an azure
function object with the same name exists a call link from the call to
Azure function to that matching azure function is created.

For example when analyzing the following source code:

index.ts

``` js
import * as df from "durable-functions"

const orchestrator = df.orchestrator(function* (context) {
    const outputs = [];
    outputs.push(yield context.df.callActivity("WorkActivity1", "Tokyo"));
    return outputs;
});
```

you will get the following result:

![](../../../images-results/653984191.png)

### Durable function client with HTTP trigger

It is common that a durable function client is triggered by an HTTP call
and that with a proper URL any orchestrator function can be triggered.
In this case, for each existing orchestrator azure function, an
Operation with the corresponding url is created with a callLink to a
Call to Azure Function object to the orchestrator function. For more
detail see the [com.castsoftware.nodejs](Node.js) extension
documentation.

## Azure Service Bus

NodeJS Azure Service Bus Publisher and NodeJS Azure Service Bus Receiver
objects are created when the following methods are used:

<table class="relative-table wrapped confluenceTable"
style="width: 60.6692%;">
<colgroup>
<col style="width: 22%" />
<col style="width: 41%" />
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Object created</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from ServiceBusClient from @azure/service-bus package</p>
<p><br />
</p>
</div></th>
<th class="confluenceTh">Methods from ServiceBusService from azure-sb
package</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">NodeJS Azure Service Bus Publisher</td>
<td class="confluenceTd"><p>sendMessages,<br />
scheduleMessages</p></td>
<td class="confluenceTd">createSender, createReceiver</td>
</tr>
<tr class="even">
<td class="confluenceTd">NodeJS Azure Service Bus Receiver</td>
<td class="confluenceTd">receiveMessages, peekMessages, subscribe,
getMessageIterator, receiveDeferredMessages</td>
<td class="confluenceTd">receiveQueueMessage,
receiveSubscriptionMessage</td>
</tr>
</tbody>
</table>

The name of the object created is that of the Queue (or the Topic).
Whenever the evaluation of the Queue (or Topic) name fails, an Unknown
object is created.

-   For publishers, a callLink  from the callable object (usually
    Function or Method) containing the call (to the supported method) to
    the publisher is added (see example below).
-   For receivers, some APIs (such as the subscribe API) require
    providing a handler function, in such case a callLink is created
    from the receiver to that handler. In other cases, a callLink is
    added from the receiver to the callable object (usually a Function
    or a Method) containing the call to the supported method (see
    example below).

Whenever an Azure Service Bus Publisher has the same name as
an Azure Service Bus Receiver, the web service linker extension will
create a call link from the Publisher to the Receiver.

Example

When analyzing the following source code:

``` java
import { ServiceBusClient, ServiceBusMessage, ServiceBusMessageBatch } from "@azure/service-bus";

function my_publish(Messages){
    const sbClient = new ServiceBusClient(connectionString);
    const sender = sbClient.createSender("MyQueue");
    await sender.sendMessages(Messages);
}

function my_receive(){
    const sbClient = new ServiceBusClient(connectionString);
    let receiver = sbClient.createReceiver("MyQueue", {
        receiveMode: "receiveAndDelete"
    });
    let messages = await receiver.receiveMessages(1);
}
```

you will get the following result:

*Click to enlarge*

![](../../../images-results/653984190.png)

## Azure Blobs

When a source code contains calls to the APIs listed in the following
table, this extension will create a link to Azure Blob objects:

<table class="relative-table wrapped confluenceTable"
style="width:100%;">
<colgroup>
<col style="width: 6%" />
<col style="width: 28%" />
<col style="width: 28%" />
<col style="width: 35%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">LinkType</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from containerClient</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> {BlobServiceClient} <span class="im">from</span> <span class="st">&quot;@azure/storage-blob&quot;</span><span class="op">;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blobServiceClient <span class="op">=</span> <span class="kw">new</span> <span class="fu">BlobServiceClient</span>(<span class="op">...</span>)<span class="op">;</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> containerClient <span class="op">=</span> blobServiceClient<span class="op">.</span><span class="fu">getContainerClient</span>(<span class="op">...</span>)</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from BlobClients (blockBlobClient,<br />
PageBlobClient, AppendBlobClient or BlobBatchCLient)</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> {BlobServiceClient} <span class="im">from</span> <span class="st">&quot;@azure/storage-blob&quot;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blobServiceClient <span class="op">=</span> <span class="kw">new</span> <span class="fu">BlobServiceClient</span>(<span class="op">...</span>)</span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> containerClient <span class="op">=</span> blobServiceClient<span class="op">.</span><span class="fu">getContainerClient</span>(<span class="op">...</span>)</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blockBlobClient <span class="op">=</span> containerClient<span class="op">.</span><span class="fu">getBlockBlobClient</span>(<span class="op">...</span>)</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from BlobServices</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb3"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> <span class="op">*</span> <span class="im">as</span> azure <span class="im">from</span> <span class="st">&#39;azure-storage&#39;</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> blobService <span class="op">=</span> azure<span class="op">.</span><span class="fu">createBlobService</span>(<span class="op">...</span>)</span></code></pre></div>
</div>
</div>
</div></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">useInsert</td>
<td class="confluenceTd">uploadBlockBlob</td>
<td class="confluenceTd">syncUploadFromURL, upload', uploadPages,
uploadPagesFromURL, beginCopyFromURL</td>
<td class="confluenceTd">createAppendBlobFromBrowserFile,
createAppendBlobFromLocalFile, createAppendBlobFromStream,
createAppendBlobFromText, createBlobSnapshot, createBlobSnapshot,
createBlockBlobFromLocalFile, createBlockBlobFromStream,
createBlockBlobFromText, createBlockFromStream, createBlockFromText,
createBlockFromURL, createOrReplaceAppendBlob, createPageBlob,
createPageBlob, createPageBlobFromLocalFile, createPageBlobFromStream,
createPagesFromStream, createWriteStreamToBlockBlob,
createWriteStreamToBlockBlob, createWriteStreamToNewAppendBlob,
createWriteStreamToNewPageBlob, startCopyBlob</td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdate</td>
<td class="confluenceTd">uploadBlockBlob</td>
<td class="confluenceTd">commitBlockList, stageBlock, stageBlockFromURL,
syncUploadFromURL, upload, uploadBrowserData<br />
'ploadData, uploadFile, uploadStream, uploadPages, uploadPagesFromURL,
appendBlock, appendBlockFromURL, beginCopyFromURL,
startCopyIncremental</td>
<td class="confluenceTd">appendBlockFromStream, appendBlockFromText,
appendFromBrowserFile, appendFromLocalFile, appendFromStream,
appendFromText, commitBlocks, createAppendBlobFromBrowserFile,
createAppendBlobFromLocalFile, createAppendBlobFromStream,
createAppendBlobFromText, createBlobSnapshot,
createBlockBlobFromLocalFile, createBlockBlobFromStream,
createBlockBlobFromText, createBlockFromStream, createBlockFromText,
createBlockFromURL, createOrReplaceAppendBlob, createPageBlob,
createPageBlob, createPageBlobFromLocalFile, createPageBlobFromStream,
createPagesFromStream, createWriteStreamToBlockBlob,
createWriteStreamToBlockBlob, createWriteStreamToExistingAppendBlob,
createWriteStreamToExistingAppendBlob,
createWriteStreamToExistingPageBlob, startCopyBlob</td>
</tr>
<tr class="odd">
<td class="confluenceTd">useDelete</td>
<td class="confluenceTd">deleteBlob, deleteIfExists</td>
<td class="confluenceTd">clearPages, deleteBlobs, delete,
deleteIfExists</td>
<td class="confluenceTd">deleteBlob, deleteBlobIfExists,
deleteContainer, deleteContainerIfExists</td>
</tr>
<tr class="even">
<td class="confluenceTd">useSelect</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">getBlockList, query, createSnapshot, download,
downloadToBuffer,
downloadToFile, beginCopyFromURL, startCopyIncremental</td>
<td class="confluenceTd">createBlobSnapshot, createReadStream,
getBlobToLocalFile, getBlobToStream, getBlobToText, startCopyBlob,
createBlockFromURL</td>
</tr>
</tbody>
</table>

Example

When analyzing the following code, a blob container named
*my_container* is created as well as a useInsert and a useUpdate links
from the main function to that container:

``` js
import { BlobServiceClient } from "@azure/storage-blob";

const blobServiceClient = new BlobServiceClient(account_url, defaultAzureCredential);

async function main() {
  const containerClient = blobServiceClient.getContainerClient("my_container");
  const content = "Hello world!";
  const blockBlobClient = containerClient.getBlockBlobClient("blobName");
  const uploadBlobResponse = await blockBlobClient.upload(content, content.length);
}
```

This will produce the following result:

![](../../../images-results/653984189.png)

## Azure CosmosDB

The @azure/cosmos package is supported. Whenever a container client
use one of the method listed in the following table, a link is created
to the corresponding collection.

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: left;"><p>LinkType</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Methods from
Container</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Methods from
Item</p></th>
<th class="confluenceTh" style="text-align: left;"><p>Methods from
Items</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useDelete</td>
<td class="confluenceTd" style="text-align: left;">delete</td>
<td class="confluenceTd" style="text-align: left;">delete</td>
<td class="confluenceTd" style="text-align: left;">batch, bulk</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useSelect</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
<td class="confluenceTd" style="text-align: left;">read, query</td>
<td class="confluenceTd" style="text-align: left;">batch, bulk, query,
readAll</td>
</tr>
<tr class="odd">
<td class="confluenceTd" style="text-align: left;">useUpdate</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
<td class="confluenceTd" style="text-align: left;">replace, patch</td>
<td class="confluenceTd" style="text-align: left;">batch,
bulk, upsert</td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: left;">useInsert</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
<td class="confluenceTd" style="text-align: left;"><br />
</td>
<td class="confluenceTd" style="text-align: left;">batch,
bulk, create</td>
</tr>
</tbody>
</table>

If the evaluation of the container name fails (either due to missing
information in the source code or to limitations in the evaluation) a
link is created to an Unknown collection.

Example

Analyzing the following code:

``` js
import { Container, FeedOptions, SqlQuerySpec, CosmosClient } from "@azure/cosmos";


function my_query(){
  const client = new CosmosClient({
    endpoint,
    key
  });

  const query1 = "Select * from c order by c._ts";
  const container = client.database('My database').container('My collection');
  const queryIterator = container.items.query(query1, options);
}
```

will produce the following result:

![](../../../images-results/653984188.png)

## Azure Event Hubs

This extension supports the version 5.x of @azure/event-hubs package as
follows:

-   Whenever a call to one of the following API is found in the source
    code this extension evaluates the name of the Event Hub in which the
    operation is made and creates an Event Hub Publisher with a callLink
    from the caller. 
    -   EventHubBufferedProducerClient.enqueueEvent
    -   EventHubBufferedProducerClient.flush
    -   EventHubProducerClient.sendBatch
-   Whenever a call to EventHubConsumerClient.subscribe is found in the
    source code, this extension evaluates the name of the Event Hub in
    which the operation is made and creates an Event Hub Receiver with a
    callLink to the handler method*.*

If the evaluation of the Event Hub name fails (either due to missing
information in the source code or to limitations in the evaluation) an
Unknown Event Hub Publisher/Receiver is created.

Example

Analyzing the following code:

publisher.ts

``` js
const { EventHubProducerClient } = require("@azure/event-hubs");

const eventHubName = "tests-hub";

function my_publish(){
  // Create a producer client to send messages to the event hub.
  const producer = new EventHubProducerClient(connectionString, eventHubName, {retryOptions: {maxRetries: 3, maxRetryDelayInMs: 3000}});

  const batch = await producer.createBatch({ partitionKey: i });
  batch.tryAdd({ body: "Message" });
  // Send the batch to the event hub.
  await producer.sendBatch(batch);

}
```

consumer.ts

``` js
const { EventHubConsumerClient } = require("@azure/event-hubs");

const eventHubName = "tests-hub";

async function main() {

  // Create a consumer client for the event hub by specifying the checkpoint store.
  const consumerClient = new EventHubConsumerClient(consumerGroup, connectionString, eventHubName, checkpointStore);

  // Subscribe to the events, and specify handlers for processing the events and errors.
  const subscription = consumerClient.subscribe({
      processEvents: async (events, context) => {
        console.log(events.length);
      }
    }
  );
}
```

will produce the following result:

![](../../../images-results/653984187.png)

## Azure SignalR

### ASP.NET Core

This extension supports the calls to server for
the @microsoft/signalr and @aspnet/signalr packages.

Whenever a call to one of the following API is found in the source
code this extension evaluates the name of the invoked method as well as
the hub name and creates a NodeJS Azure SignalR Call to Hub
Method object named with the name of the invoked method and with a
property storing the hub name:

-   HubConnection.send
-   HubConnection.invoke
-   HubConnection.stream

If the evaluation of the method name fails (either due to missing
information in the source code or to limitations in the evaluation)
a NodeJS Azure SignalR Call to Unknown Hub Method object is created.

### ASP.NET

This extension supports:

-   calls to server using signalr package.
-   both calls with or without the generated proxy.

#### Without generated proxy

Whenever an invoke call is found, if this call comes from a hub proxy
created from a createHubProxy() call, this extension creates a NodeJS
Azure SignalR Call to Hub Method. Its name is evaluated from the
first argument of the invoke and it has a hub name property whose value
is evaluated from the first argument of the createHubProxy Call.

If the evaluation of the method name fails (either due to missing
information in the source code or to limitations in the evaluation)
a NodeJS Azure SignalR Call to Unknown Hub Method object is created.

#### With generated proxy

When a method call $.connection.hubname.server.methodname() is found,
this extension creates a NodeJS Azure SignalR Call to Hub
Method. Its name is methodname and it has a hub name property whose
value is hubname.

### Example

Analyzing one of the following pieces of code:

module_for_aspnet_core.ts

``` js
import * as signalr from '@microsoft/signalr'

function my_invoke(){
    const connection = new signalR.HubConnectionBuilder()
    .withUrl("/myhub")
    .configureLogging(signalR.LogLevel.Information)
    .build();
    
    await connection.start()
    await connection.invoke("MyMethod", user, message);
    
}
```

module_for_aspnet_without_generated_proxy.ts

``` js
function my_invoke(){
  var connection = $.hubConnection();
  var hubProxy= connection.createHubProxy('myhub');
  hubProxy.invoke('MyMethod', { UserName: userName, Message: message})
}
```

module_for_aspnet_with_generated_proxy.ts

``` js
function my_invoke(){
   var hubProxy = $.connection.myhub;
   hubProxy.server.MyMethod({user:user, message:message})
}
```

will produce the following result:

![](../../../images-results/653984186.png)

>If a *DotNet Azure SignalR Method* object exists with matching method
name and hub name exits the *com.castsoftware.wbslinker* extension will
create a link from the *NodeJS Azure SignalR Call to Hub Method * to
that object.
