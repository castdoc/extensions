---
title: "Node.js - Axios support"
linkTitle: "Node.js - Axios"
type: "docs"
---

This section describes support for the Node.js Axios framework.

The analysis of the following code will create a Node.js Put
HttpRequest named foo/put/ with a call link from my_func to
that request:

``` java
import * as axios from 'axios';

function my_func(){
  axios('foo/put', {method:'put'})

     .then(function (response) {
       // handle success
       console.log(response);
     })
}
```

![](../../../images-results/653984157.png)