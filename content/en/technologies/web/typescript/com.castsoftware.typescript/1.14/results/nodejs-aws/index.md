---
title: "Node.js - Amazon Web Services support for TypeScript"
linkTitle: "Node.js - AWS"
type: "docs"
---

## AWS Lambda

Lambda services allow executing some source code on the cloud. The
execution can be set to be triggered by some AWS events. Lambda
functions can be deployed using several deployment frameworks. The
supported deployment frameworks are listed on
this [page](../../../../../../cloud-services/aws/overview/lambda). When a lambda function is created and its
runtime is Node.js, the TypeScript and Frameworks extension is
responsible for linking the lambda objects and their triggers with the
TypeScript handler functions.

### Basic example

Let us consider a source code defining a lambda function that has two
triggers: an SQS queue and an API Gateway. The lambda function has a
nodejs runtime and the handler function is given by the handler function
fullname. If the lambda function is deployed using a supported
deployment framework (such as CloudFormation), the analysis will create
a lambda function, an SQS receiver, and an API Gateway objects. Each of
these objects has a runtime property (nodejs) and a handler property
with the function fullname.  If the current extension finds a TypeScript
function matching the handler fullname a link to that function will be
added from the lambda function, the SQS queue, and the API Gateway
objects.

![](../../../images-results/653984199.png)

### Lambda invocation using SDK

The SDK provides APIs to execute (i.e. invoke) lambda function. The
following APIs are supported:

-   invoke method of the Lambda "aws-sdk" client of the SDK V2
-   InvokeCommand of the SDK V3

Example

When analyzing the following source code:

``` js
import * as AWS from 'aws-sdk'
// Set region

function my_invoke(){
  AWS.config.update({region: 'REGION'});
  var lambda = new AWS.Lambda();
  var params = {
    FunctionName: 'mylambda', /* required */
    //...
  };

  lambda.invoke(params, function(err, data) {
    //...
  });
}
```

a *call to AWS lambda function* object named mylambda is created.
com.castsoftware.wbslinker links that call to lambda object to AWS
Lambda Function objects having the same name: 

![](../../../images-results/653984198.png)

## AWS SNS

The following APIs are supported:

### SDK V2

The publish and subscribe methods of the SNS client are supported:

``` js
import AWS from 'aws-sdk'
const snsClient = new AWS.SNS({...})
snsClient.publish(...)
snsClient.subscribe(...)
```

### SDK V3

The PublishCommand and SubscribeCommand (which are used with the
snsClient.send method) are supported:

``` js
import { SNSClient, PublishCommand, SubscribeCOmmand } from "@aws-sdk/client-sns";
const snsClient = new SNSClient({ region: REGION });
snsClient.send(new PublishCommand(params))
snsClient.send(new SubscribeCommand(params))
```

### Detailed support

A Nodejs SNS Publisher object is created whenever a publication to a
topic is found by the analyzer. Its name is that of the topic. For
subscriptions, a Nodejs SNS Subscriber object is created. Its name
is that of the topic. Then for each supported protocol, an object is
created with a callLink from the subscriber to that object. The
following protocols are supported:

| Protocol   | Object created                            | Object name                                                   |
|:-----------|:------------------------------------------|:--------------------------------------------------------------|
| email      | Nodejs Email                              | *an Email* (the email addresses are not evaluated)            |
| sms        | Nodejs SMS                                | *an SMS* (the SMS numbers are not evaluated)                  |
| http/https | Nodejs Post Service                       | the url (evaluated from the endpoint)                         |
| sqs        | Nodejs AWS Simple Queue Service Publisher | the name of the queue (evaluated from the endpoint)           |
| lambda     | Nodejs Call to AWS Lambda Function        | the name of the lambda function (evaluated from the endpoint) |

Example

When analyzing the following source code:

``` js
import {CreateTopicCommand, SNSClient, SubscribeCommand, PublishCommand } from "@aws-sdk/client-sns";

const REGION = "REGION"; //e.g. "us-east-1"
// Create SNS service object.
const snsClient = new SNSClient({ region: REGION });

function my_subscribe(protocol, topic_arn, endpoint){
  const params = {
    Protocol: protocol,
    TopicArn: topic_arn, //TOPIC_ARN
    Endpoint: endpoint, 
  };
  const data = await snsClient.send(new SubscribeCommand(params));
};

function subcribe_to_foo_topic(){
   const topic_arn = 'arn:aws:sns:us-east-2:123456789012:foo_topic';
   my_subscribe("email", topic_arn, "EMAIL_ADDRESS") 
   my_subscribe("sms", topic_arn, "007")
   my_subscribe("lambda", topic_arn, "fooarn:function:lambda_name:v2")
   my_subscribe("sqs", topic_arn, "https://sqs.us-east-2.amazonaws.com/123456789012/foo_queue")

   my_subscribe("http", topic_arn, "http://foourl")
}


function publish_to_foo_topic(){
  const params = {
      Message: "MESSAGE_TEXT", // MESSAGE_TEXT
      TopicArn: 'arn:aws:sns:us-east-2:123456789012:foo_topic'
  };
  const data = await snsClient.send(new PublishCommand(params));
}
```

This code will produce the following result:

![](../../../images-results/653984197.png)

## AWS S3

The following links are created:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Link Type</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from SDK V2 s3client</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb1"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> {AWS} <span class="im">from</span> <span class="st">&#39;aws-sdk&#39;</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> s3client <span class="op">=</span> <span class="kw">new</span> AWS<span class="op">.</span><span class="fu">S3</span>()</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from SDK V3 s3client</p>
<div class="code panel pdl" style="border-width: 1px;">
<div class="codeContent panelContent pdl">
<div class="sourceCode" id="cb2"
data-syntaxhighlighter-params="brush: js; gutter: false; theme: Confluence"
data-theme="Confluence"
style="brush: js; gutter: false; theme: Confluence"><pre
class="sourceCode js"><code class="sourceCode javascript"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="im">import</span> {S3} <span class="im">from</span> <span class="st">&#39;@aws-sdk/client-s3&#39;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="kw">const</span> s3client <span class="op">=</span> <span class="kw">new</span> <span class="fu">S3</span>()</span></code></pre></div>
</div>
</div>
</div></th>
<th class="confluenceTh"><p>Commands from SDK V3</p>
<p>imported from '@aws-sdk/client-s3'</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">No Link</td>
<td class="confluenceTd"><ul>
<li>createBucket</li>
</ul></td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd"><ul>
<li>CreateBucketCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">callLink</td>
<td class="confluenceTd"><ul>
<li><p>createMultipartUpload</p></li>
<li><p>createPresignedPost</p></li>
<li><p>abortMultipartUpload</p></li>
<li><p>completeMultipartUpload</p></li>
<li><p>deleteBucketAnalyticsConfiguration</p></li>
<li><p>deleteBucketCors</p></li>
<li><p>deleteBucketEncryption</p></li>
<li><p>deleteBucketInventoryConfiguration</p></li>
<li><p>deleteBucketLifecycle</p></li>
<li><p>deleteBucketMetricsConfiguration</p></li>
<li><p>deleteBucketPolicy</p></li>
<li><p>deleteBucketReplication</p></li>
<li><p>deleteBucketTagging</p></li>
<li><p>deleteBucketWebsite</p></li>
<li><p>deleteObjectTagging</p></li>
<li><p>deletePublicAccessBlock</p></li>
<li><p>getBucketAccelerateConfiguration</p></li>
<li><p>getBucketAcl</p></li>
<li><p>getBucketAnalyticsConfiguration</p></li>
<li><p>getBucketCors</p></li>
<li><p>getBucketEncryption</p></li>
<li><p>getBucketInventoryConfiguration</p></li>
<li><p>getBucketLifecycle</p></li>
<li><p>getBucketLifecycleConfiguration</p></li>
<li><p>getBucketLocation</p></li>
<li><p>getBucketLogging</p></li>
<li><p>getBucketMetricsConfiguration</p></li>
<li><p>getBucketNotification</p></li>
<li><p>getBucketNotificationConfiguration</p></li>
<li><p>getBucketPolicy</p></li>
<li><p>getBucketPolicyStatus</p></li>
<li><p>getBucketReplication</p></li>
<li><p>getBucketTagging</p></li>
<li><p>getBucketVersioning</p></li>
<li><p>getBucketWebsite</p></li>
<li><p>getObjectAcl</p></li>
<li><p>getObjectLegalHold</p></li>
<li><p>getObjectLockConfiguration</p></li>
<li><p>getObjectRetention</p></li>
<li><p>getObjectTagging</p></li>
<li><p>getPublicAccessBlock</p></li>
<li><p>getSignedUrl</p></li>
<li>listBuckets</li>
<li><p>listBucketAnalyticsConfigurations</p></li>
<li><p>listBucketInventoryConfigurations</p></li>
<li><p>listBucketMetricsConfigurations</p></li>
<li><p>listMultipartUploads</p></li>
<li><p>listObjectVersions</p></li>
<li><p>listParts</p></li>
<li>putBucketLogging</li>
<li>putBucketAnalyticsConfiguration</li>
<li><p>putBucketLifecycleConfiguration</p></li>
<li><p>putBucketMetricsConfiguration</p></li>
<li><p>putBucketNotification</p></li>
<li><p>putBucketNotificationConfiguration</p></li>
<li><p>putBucketPolicy</p></li>
<li><p>putBucketReplication</p></li>
<li><p>putBucketRequestPayment</p></li>
<li><p>putBucketTagging</p></li>
<li><p>putBucketVersioning</p></li>
<li><p>putObjectAcl</p></li>
<li><p>putObjectLegalHold</p></li>
<li><p>putObjectLockConfiguration</p></li>
<li><p>putObjectRetention</p></li>
<li><p>putObjectTagging</p></li>
<li><p>putPublicAccessBlock</p></li>
<li><p>putBucketAccelerateConfiguration</p></li>
<li><p>putBucketAcl</p></li>
<li><p>putBucketCors</p></li>
<li><p>putBucketEncryption</p></li>
<li><p>putBucketInventoryConfiguration</p></li>
<li><p>putBucketLifecycle</p></li>
<li>putBucketLogging</li>
<li><p>upload</p></li>
<li><p>uploadPart</p></li>
<li><p>uploadPartCopy</p></li>
</ul></td>
<td class="confluenceTd"><ul>
<li>abortMultipartUpload</li>
<li>completeMultipartUpload</li>
<li>copyObject</li>
<li>createBucket</li>
<li>createMultipartUpload</li>
<li>deleteBucket</li>
<li>deleteBucketAnalyticsConfiguration</li>
<li>deleteBucketCors</li>
<li>deleteBucketEncryption</li>
<li>deleteBucketIntelligentTieringConfiguration</li>
<li>deleteBucketInventoryConfiguration</li>
<li>deleteBucketLifecycle</li>
<li>deleteBucketMetricsConfiguration</li>
<li>deleteBucketOwnershipControls</li>
<li>deleteBucketPolicy</li>
<li>deleteBucketReplication</li>
<li>deleteBucketTagging</li>
<li>deleteBucketWebsite</li>
<li>deleteObjectTagging</li>
<li>deletePublicAccessBlock</li>
<li>destroy</li>
<li>getBucketAccelerateConfiguration</li>
<li>getBucketAcl</li>
<li>getBucketAnalyticsConfiguration</li>
<li>getBucketCors</li>
<li>getBucketEncryption</li>
<li>getBucketIntelligentTieringConfiguration</li>
<li>getBucketInventoryConfiguration</li>
<li>getBucketLifecycleConfiguration</li>
<li>getBucketLocation</li>
<li>getBucketLogging</li>
<li>getBucketMetricsConfiguration</li>
<li>getBucketNotificationConfiguration</li>
<li>getBucketOwnershipControls</li>
<li>getBucketPolicy</li>
<li>getBucketPolicyStatus</li>
<li>getBucketReplication</li>
<li>getBucketRequestPayment</li>
<li>getBucketTagging</li>
<li>getBucketVersioning</li>
<li>getBucketWebsite</li>
<li>getObjectAcl</li>
<li>getObjectLegalHold</li>
<li>getObjectLockConfiguration</li>
<li>getObjectRetention</li>
<li>getObjectTagging</li>
<li>getPublicAccessBlock</li>
<li>headBucket</li>
<li>headObject</li>
<li>listBucketAnalyticsConfigurations</li>
<li>listBucketIntelligentTieringConfigurations</li>
<li>listBucketInventoryConfigurations</li>
<li>listBucketMetricsConfigurations</li>
<li>listBuckets</li>
<li>listMultipartUploads</li>
<li>listObjectVersions</li>
<li>listParts</li>
<li>putBucketAccelerateConfiguration</li>
<li>putBucketAcl</li>
<li>putBucketCors</li>
<li>putBucketEncryption</li>
<li>putBucketIntelligentTieringConfiguration</li>
<li>putBucketInventoryConfiguration</li>
<li>putBucketLifecycleConfiguration</li>
<li>putBucketLogging</li>
<li>putBucketMetricsConfiguration</li>
<li>putBucketNotificationConfiguration</li>
<li>putBucketOwnershipControls</li>
<li>putBucketPolicy</li>
<li>putBucketReplication</li>
<li>putBucketRequestPayment</li>
<li>putBucketTagging</li>
<li>putBucketVersioning</li>
<li>putBucketWebsite</li>
<li>putObjectAcl</li>
<li>putObjectLegalHold</li>
<li>putObjectLockConfiguration</li>
<li>putObjectRetention</li>
<li>putObjectTagging</li>
<li>putPublicAccessBlock</li>
<li>restoreObject</li>
<li>send</li>
<li>uploadPart</li>
<li>uploadPartCopy</li>
<li>writeGetObjectResponse</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>AbortMultipartUploadCommand</li>
<li>CompleteMultipartUploadCommand</li>
<li>CreateMultipartUploadCommand</li>
<li>DeleteBucketAnalyticsConfigurationCommand</li>
<li>DeleteBucketCorsCommand</li>
<li>DeleteBucketEncryptionCommand</li>
<li>DeleteBucketIntelligentTieringConfigurationCommand</li>
<li>DeleteBucketInventoryConfigurationCommand</li>
<li>DeleteBucketLifecycleCommand</li>
<li>DeleteBucketMetricsConfigurationCommand</li>
<li>DeleteBucketOwnershipControlsCommand</li>
<li>DeleteBucketPolicyCommand</li>
<li>DeleteBucketReplicationCommand</li>
<li>DeleteBucketTaggingCommand</li>
<li>GetBucketAccelerateConfigurationCommand</li>
<li>GetBucketAclCommand</li>
<li>DeleteBucketWebsiteCommand</li>
<li>DeleteObjectTaggingCommand</li>
<li>DeletePublicAccessBlockCommand</li>
<li>GetBucketAnalyticsConfigurationCommand</li>
<li>GetBucketCorsCommand</li>
<li>GetBucketEncryptionCommand</li>
<li>GetBucketIntelligentTieringConfigurationCommand</li>
<li>GetBucketInventoryConfigurationCommand</li>
<li>GetBucketLifecycleConfigurationCommand</li>
<li>GetBucketLocationCommand</li>
<li>GetBucketLoggingCommand</li>
<li>GetBucketMetricsConfigurationCommand</li>
<li>GetBucketNotificationConfigurationCommand</li>
<li>GetBucketOwnershipControlsCommand</li>
<li>GetBucketPolicyCommand</li>
<li>GetBucketPolicyStatusCommand</li>
<li>GetBucketReplicationCommand</li>
<li>GetBucketRequestPaymentCommand</li>
<li>GetBucketTaggingCommand</li>
<li>GetBucketVersioningCommand</li>
<li>GetBucketWebsiteCommand</li>
<li>GetObjectAclCommand</li>
<li>GetObjectLegalHoldCommand</li>
<li>GetObjectLockConfigurationCommand</li>
<li>GetObjectRetentionCommand</li>
<li>GetObjectTaggingCommand</li>
<li>GetPublicAccessBlockCommand</li>
<li>HeadBucketCommand</li>
<li>HeadObjectCommand</li>
<li>ListBucketAnalyticsConfigurationsCommand</li>
<li>ListBucketIntelligentTieringConfigurationsCommand</li>
<li>ListBucketInventoryConfigurationsCommand</li>
<li>ListBucketMetricsConfigurationsCommand</li>
<li>ListMultipartUploadsCommand</li>
<li>ListObjectVersionsCommand</li>
<li>ListPartsCommand</li>
<li>PutBucketAccelerateConfigurationCommand</li>
<li>PutBucketAclCommand</li>
<li>PutBucketAnalyticsConfigurationCommand</li>
<li>PutBucketCorsCommand</li>
<li>PutBucketEncryptionCommand</li>
<li>PutBucketIntelligentTieringConfigurationCommand</li>
<li>PutBucketInventoryConfigurationCommand</li>
<li>PutBucketLifecycleConfigurationCommand</li>
<li>PutBucketLoggingCommand</li>
<li>PutBucketMetricsConfigurationCommand</li>
<li>PutBucketNotificationConfigurationCommand</li>
<li>PutBucketOwnershipControlsCommand</li>
<li>PutBucketPolicyCommand</li>
<li>PutBucketReplicationCommand</li>
<li>PutBucketRequestPaymentCommand</li>
<li>PutBucketTaggingCommand</li>
<li>PutBucketVersioningCommand</li>
<li>PutBucketWebsiteCommand</li>
<li>PutObjectAclCommand</li>
<li>PutObjectLegalHoldCommand</li>
<li>PutObjectLockConfigurationCommand</li>
<li>PutObjectRetentionCommand</li>
<li>PutObjectTaggingCommand</li>
<li>PutPublicAccessBlockCommand</li>
<li>UploadPartCommand</li>
<li>UploadPartCopyCommand</li>
<li>WriteGetObjectResponseCommand</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useInsertLink</td>
<td class="confluenceTd"><ul>
<li>putObject</li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>putObject</li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>RestoreObjectCommand</li>
<li>PutObjectCommand</li>
<li>CopyObjectCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">useDeleteLink</td>
<td class="confluenceTd"><ul>
<li>deleteBucket</li>
<li><p>deleteObject</p></li>
<li><p>deleteObjects</p></li>
</ul></td>
<td class="confluenceTd"><ul>
<li>deleteBucket</li>
<li><p>deleteObject</p></li>
<li><p>deleteObjects</p></li>
</ul></td>
<td class="confluenceTd"><ul>
<li>DeleteBucketCommand</li>
<li>DeleteObjectCommand</li>
<li>DeleteObjectsCommand</li>
</ul></td>
</tr>
<tr class="odd">
<td class="confluenceTd">useSelectLink</td>
<td class="confluenceTd"><ul>
<li>getObject</li>
<li>getObjectTorrent</li>
<li><p>listObjects</p></li>
<li><p>listObjectsV2</p></li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>getObject</li>
<li>getObjectTorrent</li>
<li>listObjects</li>
<li>listObjectsV2</li>
<li>copyObject</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>GetObjectCommand</li>
<li>ListObjectsCommand</li>
<li>ListObjectsV2Command</li>
<li>SelectObjectContentCommand</li>
<li>GetObjectTorrentCommand</li>
<li>CopyObjectCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">useUpdateLink</td>
<td class="confluenceTd"><ul>
<li>putBucketAnalyticsConfiguration</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>putBucketAnalyticsConfiguration</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>RestoreObjectCommand</li>
<li>PutObjectCommand</li>
<li>CopyObjectCommand</li>
</ul></td>
</tr>
</tbody>
</table>

Example

This code will create an S3 Bucket named "MyBucket" and a useInsert link
to that bucket:

foo.ts

``` js
// Load the AWS SDK for Node.js
import * as AWS from 'aws-sdk'
// Set the region 
AWS.config.update({region: 'REGION'});

// Create S3 service object
s3 = new AWS.S3({apiVersion: '2006-03-01'});

// Create the parameters for calling createBucket
var bucketParams = {
  Bucket : "MyBucket",
  ACL : 'public-read'
};

// call S3 to create the bucket
s3.createBucket(bucketParams, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data.Location);
  }
});

params = {
    // ...
    Bucket: "MyBucket"
};
s3.putObject(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else     console.log(data);           // successful response
});
```

This code will produce the following result:

![](../../../images-results/653984196.png)

## AWS SQS

Supported APIs

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">API</th>
<th class="confluenceTh"><div class="content-wrapper">
<p>Methods from SDK V2 sqs client</p>
</div></th>
<th class="confluenceTh"><p>Commands from SDK V3</p>
<p>imported from '@aws-sdk/client-sqs'</p></th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">Publish</td>
<td class="confluenceTd"><ul>
<li><p>sendMessage</p></li>
<li>sendMessageBatch</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>SendMessageCommand</li>
</ul></td>
</tr>
<tr class="even">
<td class="confluenceTd">Receive</td>
<td class="confluenceTd"><ul>
<li>receiveMessage</li>
</ul></td>
<td class="confluenceTd"><ul>
<li>ReceiveMessageCommand</li>
</ul></td>
</tr>
</tbody>
</table>

Example

This code will publish a message into the "SQS_QUEUE_URL" queue:

``` js
import * as AWS from "aws-sdk";
AWS.config.update({ region: 'REGION' });

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const queueUrl = "SQS_QUEUE_URL"

const params = {
    MessageBody: "This is a message",
    QueueUrl: queueUrl,
    MaxNumberOfMessages: 1,
    VisibilityTimeout: 0,
};
class Foo {
  sendMessage(){
    sqs.sendMessage(params, function (err, data) {
        if (err) {
            console.log("Error", err);
        } else {
         console.log("Success", data.MessageId);
        }
    });
 }
}
```

This code will receive a message from the queue "SQS_QUEUE_URL":

``` js
import * as AWS from "aws-sdk";
AWS.config.update({ region: 'REGION' });

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

const queueUrl = "SQS_QUEUE_URL"

const params = {
    QueueUrl: queueUrl,
    MaxNumberOfMessages: 1,
    VisibilityTimeout: 0,
};

export class SqsReciver {
    constructor() {
        this.reciver();
    }
    private reciver(): void {
        sqs.receiveMessage(params, (err, data) => {
    // do something
        });
    }
}
```

This code will produce the following result:

![](../../../images-results/653984195.png)

When the evaluation of the queue name fails, a Node.js AWS SQS Unknown
Publisher (or Receiver) will be created.

## AWS DynamoDB

See DynamoDB support for Node.js source code - TypeScript.

## AWS X-Ray

AWS X-Ray encapsulates AWS method calls in order to provide status and
load status.

Example

This code will encapsulate AWS SDK then create a dynamoDB instance, and
a Table instance:

``` js
import AWSXRay from 'aws-xray-sdk-core'
import AWS from 'aws-sdk'
const AWS = AWSXRay.captureAWS(AWS) // Encapsulate AWS SDK
const DDB = new AWS.DynamoDB({ apiVersion: "2012-10-08" }) // use AWS as usual
const { v1: uuidv1 } = require('uuid');
 
// environment variables
const { TABLE_NAME, ENDPOINT_OVERRIDE, REGION } = process.env
const options = { region: REGION }
AWS.config.update({ region: REGION })
 
if (ENDPOINT_OVERRIDE !== "") {
    options.endpoint = ENDPOINT_OVERRIDE
}
 
const docClient = new AWS.DynamoDB.DocumentClient(options)
// response helper
const response = (statusCode, body, additionalHeaders) => ({
    statusCode,
    body: JSON.stringify(body),
    headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', ...additionalHeaders },
})
 

 
function addRecord(event) {
 
    let usernameField = {
        "cognito-username": getCognitoUsername(event)
    }
 
    // auto generated date fields
    let d = new Date()
    let dISO = d.toISOString()
    let auto_fields = {
        "id": uuidv1(),
        "creation_date": dISO,
        "lastupdate_date": dISO
    }
 
    //merge the json objects
    let item_body = {...usernameField, ...auto_fields, ...JSON.parse(event.body) }
 
    console.log(item_body);
     
    //final params to DynamoDB
    const params = {
        TableName: TABLE_NAME,
        Item: item_body
    }
 
    return docClient.put(params)
}
```

This code will produce the following result:

![](../../../images-results/653984194.png)

## Known limitations

-   The use of AWS.SQS with promises is not supported. For instance, no
    link would be created between the receiver and the handler function
    defined in .then() call in the following source code: 

``` xml
this.sqs.receiveMessage(params).promise().then( () => {})
```

-   If the queueName is set using the createQueue API, the evaluation of
    the queue name will fail.
-   Use of access points is not supported
