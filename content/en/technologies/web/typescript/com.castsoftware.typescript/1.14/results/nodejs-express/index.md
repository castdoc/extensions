---
title: "Node.js - Express support"
linkTitle: "Node.js - Express"
type: "docs"
---

This section describes support for the Node.js Express framework.

The analysis of the following code will create a Node.js Get
Operation named /login with a call link from the operation to the
handler function f:

``` java
var app = express()
function f(req, res) {
    console.log('login ' + req.url);
    var currentSession = getSessionId(req, res);
}
app.get('/login', f);
```
![](../../../images-results/653984145.png)

Routers are also supported and the analysis of the following code will
create a Node.js Get Operation named /foo1/foo2:

``` java
import express from 'express';

var router = express.Router()

// define the about route
router.get('/foo2', function (req, res) {
  res.send('About birds')
})

var app = express()
app.use('/foo1', router)
```