---
title: "gRPC support for TypeScript"
linkTitle: "gRPC"
type: "docs"
---

## Introduction

gRPC is an open source Remote Procedure Call (RPC) framework -
see https://grpc.io/docs/.

## Objects

This extension creates:

-   'NodeJS Call to gRPC service method' object when a call to a gRPC
    method is found inside a .ts file and 
-   'NodeJS gRPC service method' when a gRPC method is defined within a
    .ts file.

These objects are named *{ServiceName}.{MethodName}.*
When a 'NodeJS Call to gRPC service method' object has the same name as a 
'NodeJS gRPC service method' object, these objects are linked.

| Icon | Description                        |
|------|------------------------------------|
| ![](../../../images-results/653984115.png) | NodeJS Call to gRPC service method |
| ![](../../../images-results/653984115.png) | NodeJS gRPC service method |

## Supported frameworks and APIs

Both the @grpc/grpc-js and grpc packages are supported.

### Analysis of proto files

This extension extracts possible grpc ServiceName and MethodName from
.proto files.  If the .proto files are missing from the analyzed source
code, this extension will not be able to create most gRPC objects.

### Server side

Calls to the addService or addProtoService of a grpc Server are
analyzed. From the first argument of the call we extract the
*ServiceName*. From the second argument, we extract pairs of
*MethodName* and handler function. For each pair, we create a
'NodeJS gRPC service method' object named *{ServiceName}.{MethodName}* with a
*callLink* to the corresponding handler.

#### Example

When analyzing the following Source Code:

``` js
import * as grpc from '@grpc/grpc-js';
function greet(){
   //...
}
function greetManyTimes(){
   //...
}

const server = new grpc.Server();
server.addService(service.GreetServiceService, {
    greet: greet,
    greetManyTimes: greetManyTimes
});
```

you will get the following result:

![](../../../images-results/653984118.png)

### Client side

When there is a call to a method named as a gRPC method defined in a
proto file, the analyzer evaluates a possible service name from the
instantiation of the client. If both the service name and the method
names match a gRPC method definition in a proto file, a
'NodeJS Call to gRPC service method' object is created.
Both dynamic (using the loadPackageDefinition API) and static
(using protocol buffer compiler protoc) generation of the client are supported.

#### Example

Here is an example with a dynamically generated client. In the following
source code, we see that there is a client.greet() call. The client
comes from a service GreetService. 

``` js
import * as grpc from '@grpc/grpc-js';
import { GreetServiceClient } from '../proto/greetservice_grpc_pb';
new SongsClient(

const client = new GreetServiceClient ("localhost:50051",
        grpc.credentials.createInsecure()
)

function my_greet(){
    client.greet(
                 //...
                )
}

function my_greet_many_times(){
    client.greetManyTimes(
                          //...
                         )

}
```

If there is a .proto file defining a *GreetService* service with methods
*greet* and *greetManyTimes,* you will get the following results
(assuming that the analyzed source code also contains the definition of
the corresponding gRPC methods):

![](../../../images-results/653984117.png)
