---
title: "TypeScript and Frameworks - 1.14"
linkTitle: "1.14"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.typescript

## What's new

See [Release Notes](rn/).

## Description

This extension provides support for the TypeScript language. It also
includes support for Angular, React, React-Native and some
of the main frameworks for Node.js Web applications such
as Express (when they are used within typescript source code).

-   TypeScript is an open-source programming  language. It is a
    superset of JavaScript that adds optional typing and that compiles
    to plain JavaScript.
-   Angular is a front-end web framework used to create modern web
    platform capabilities.
-   React and React-Native are frameworks for building user
    interfaces
-   Redux is a state container for JavaScript and TypeScript
    application
-   Node.js is a run-time environment that executes JavaScript code
    outside of a browser thus allowing to develop the server-side of an
    application using TypeScript.
    -   Express is a server web framework.
    -   Fastify is a server web framework.
    -   Axios is a promise based HTTP client.
    -   Mongoose is a MongoDB object modeling tool designed to work
        in an asynchronous environment.
    -   AWS is a cloud service by Amazon.
    -   Azure is a cloud service by Microsoft
    -   Google Cloud Platform is a cloud service by Google
    -   TypeORM is an Object-Relational Mapping framework.
    -   Sequelize is a framework for database management.

Note that:

-   in order to analyze a JavaScript source code which
    uses Express or Fastify frameworks, you should use the
    [Node.js](../../../nodejs/com.castsoftware.nodejs/) extension.
-   similarly, in order to analyze a JavaScript source code which uses
    the Angular framework, you should use the
    [AngularJS ](../../../angularjs/com.castsoftware.angularjs/) extension.
-   finally, in order to analyze a JavaScript source code which uses
    the React or React-Native framework, you should use the
    [ReactJS ](../../../reactjs/com.castsoftware.reactjs/)extension.

### In what situation should you install this extension?

The typical use case would be a full-stack web application developed
entirely in TypeScript using Angular framework and Node.js.
However, this extension should be used whenever any sub-set of the
application is implemented using TypeScript (provided that you want to
view the call-graph of that part of the application).

![](../images/653984107.png)

*Angular/TypeScript Front-end connected to Node.js/Express/MongoDB back-end*

## Supported versions

The following tables display the list of versions that this extension
supports. It also gives a list of the supported frameworks for Node.js.

### TypeScript

| Version | Supported |
|---------|:---------:|
| 1.x     | :white_check_mark: |
| 2.x     | :white_check_mark: |
| 3.x     | :white_check_mark: |
| 4.x     | :white_check_mark: |

### Angular

| Version | Supported |
|---------|:---------:|
| 2       | :white_check_mark: |
| 4       | :white_check_mark: |
| 5       | :white_check_mark: |
| 6       | :white_check_mark: |
| 7       | :white_check_mark: |
| 8       | :white_check_mark: |
| 9       | :white_check_mark: |
| 10      | :white_check_mark: |
| 11      | :white_check_mark: |
| 12      | :white_check_mark: |
| 13      | :white_check_mark: |
| 14      | :white_check_mark: |
| 15      | :white_check_mark: |
| 16      | :white_check_mark: |
| 17      | :white_check_mark: |

### React

| Version | Supported |
|---------|:---------:|
| 15.x    | :white_check_mark: |
| 16.x    | :white_check_mark: |

### React-Native

| Version | Supported |
|---------|:---------:|
| 0.x     | :white_check_mark: |

### Supported Node.js versions

| Version | Support  | Comment                 |
|---------|:--------:|-------------------------|
| v0.x    | :x:                | No longer supported     |
| v4.x    | :white_check_mark: | LTS                     |
| v5.x    | :white_check_mark: | Based on Javascript ES6 |
| v6.x    | :white_check_mark: | Based on Javascript ES6 |
| v7.x    | :white_check_mark: | Based on Javascript ES6 |
| v8.x    | :white_check_mark: |                         |
| v9.x    | :white_check_mark: |                         |
| v10.x   | :white_check_mark: |                         |
| v11.x   | :white_check_mark: |                         |
| v12.x   | :white_check_mark: |                         |
| v13.x   | :white_check_mark: |                         |
| v14.x   | :white_check_mark: |                         |
| v15.x   | :white_check_mark: |                         |
| v16.x   | :white_check_mark: |                         |
| v17.x   | :white_check_mark: |                         |
| v18.x   | :white_check_mark: |                         |
| v19.x   | :white_check_mark: |                         |
| v20.x   | :white_check_mark: |                         |


### Supported frameworks for Node.js

- :white_check_mark: indicates that the framework is currently supported.
- :x: indicates that the framework is not yet supported.

| Library                       | Comment                            |    Data Access     | Web Service or communication | Supported versions                                                                |
|-------------------------------| ---------------------------------- | :----------------: | :--------------------------: | --------------------------------------------------------------------------------- |
| AWS.DynamoDB                  | Amazon database access             | :white_check_mark: |                              | SDK 2.x; SDK 3.x                                                                  |
| AWS.S3                        | Amazon storage service             | :white_check_mark: |                              | SDK 2.x; SDK 3.x                                                                  |
| AWS.Lambda                    | Amazon routing solution            |                    |      :white_check_mark:      | Cloudformation, Serverless framework, SAM (requires com.castsoftware.cloudconfig) |
| AWS.SNS                       | Amazon Simple Notification Service |                    |      :white_check_mark:      | SDK 2.x; SDK 3.x                                                                  |
| AWS.SQS                       | Amazon Simple Queue Service        |                    |      :white_check_mark:      | SDK 2.x; SDK 3.x                                                                  |
| Axios                         | Promise based HTTP client          |                    |      :white_check_mark:      | 0.x                                                                               |
| Azure Blobs                   | Azure storage service              | :white_check_mark: |                              | @azure/storage-blob; azure-storage                                                |
| Azure Service Bus             | Azure Queue Service                |                    |      :white_check_mark:      | @azure/service-bus; azure-sb                                                      |
| Couchdb                       | Couchdb access                     |        :x:         |                              |                                                                                   |
| Couchdb-nano                  | Couchdb access                     |        :x:         |                              |                                                                                   |
| Express                       | Node.js application framework      |                    |      :white_check_mark:      | 4.x                                                                               |
| Fastify                       | Node.js server                     |                    |      :white_check_mark:      | 3.x                                                                               |
| fetch                         | JavaScript builtin web service     |                    |      :white_check_mark:      |                                                                                   |
| GCP BigTable                  | GCP database access                | :white_check_mark: |                              | @google-cloud/bigtable                                                            |
| GCP Cloud Storage             | GCP storage service                | :white_check_mark: |                              | @google-cloud/storage                                                             |
| Hapi                          | Node.js application framework      |        :x:         |                              |                                                                                   |
| https                         | Node.js web service                |                    |      :white_check_mark:      |                                                                                   |
| Koa                           | Node.js application framework      |        :x:         |                              |                                                                                   |
| Knex                          | Node.js SQL query builder          |        :x:         |                              |                                                                                   |
| Loopback                      | Node.js application framework      |        :x:         |      :white_check_mark:      |                                                                                   |
| Marklogic                     | Marklogic access                   |        :x:         |                              |                                                                                   |
| Memcached                     | Storage framework                  |        :x:         |                              |                                                                                   |
| Mongodb (node-mongodb-native) | MongoDB access                     | :white_check_mark: |                              | 3.x                                                                               |
| Mongo-client                  | MongoDB access                     |        :x:         |                              |                                                                                   |
| Mongoose                      | MongoDB access                     | :white_check_mark: |                              | 5.x                                                                               |
| mssql                         | MsSQL access                       | :white_check_mark: |                              | 5.x; 6.x                                                                          |
| my_connection                 | MySQL access                       | :white_check_mark: |                              | 0.x                                                                               |
| mysql                         | MySQL access                       | :white_check_mark: |                              | 0.x                                                                               |
| mysql2                        | MySQL2 access                      | :white_check_mark: |                              | from 0.8.2 to 3.x                                                                 |
| nestjs                        | Node.js application framework      |                    |      :white_check_mark:      | 6.x; 7.x                                                                          |
| Node-couchdb                  | Couchdb access                     |        :x:         |                              |                                                                                   |
| oracledb                      | Oracle Database access             | :white_check_mark: |                              | 4.x; 5.x                                                                          |
| pg                            | PostgreSQL access                  | :white_check_mark: |                              | 7.x; 8.x                                                                          |
| request                       | HTTP request client 'request'      |                    |      :white_check_mark:      | 2.x                                                                               |
| request-promise               | HTTP request client 'request'      |                    |      :white_check_mark:      | 4.x                                                                               |
| request-promise-native        | HTTP request client 'request'      |                    |      :white_check_mark:      | 1.x                                                                               |
| request-promise-any           | HTTP request client 'request'      |                    |      :white_check_mark:      | 1.x                                                                               |
| Sails                         | Node.js application framework      |        :x:         |      :white_check_mark:      | 4.x                                                                               |
| Sequelize                     | Node.js application framework      | :white_check_mark: |                              | 5.x; 6.x                                                                          |
| sqlite3                       | SQLite3 access                     | :white_check_mark: |                              | from 2.x to 5.x                                                                   |
| tedious                       | Tedious access                     | :white_check_mark: |                              | from 0.1.3 to 18.x                                                                |
| TypeORM                       | ORM                                | :white_check_mark: |                              | 0.2.x                                                                             |

>Supported Emailing frameworks: @sendgrid/mail, nodemailer

## Files analyzed

| Icon(s) | File       | Extension             | Notes                                                          |
|:-------:|------------|-----------------------|----------------------------------------------------------------|
| ![](../images/653984106.jpg)<br>![](../images/653984105.png) | TypeScript | .ts, .tsx, .mts, .cts | .mts and .cts files are supported for version of CAIP ≥ 8.3.54 |

>Skipped files
>
>The TypeScript analyzer will automatically skip files inside folders (or sub-folders) that by convention pertain to either external libraries or unit-testing. Currently, the following are skipped:
>
>-   Folders named as node_modules, e2e, e2e-bdd, e2e-app
>-   Files with following name endings: .spec.ts, -spec.ts, \_spec.ts, .d.ts
>
>As mentioned in the introduction, TypeScript is a language that will be compiled to generate JavaScript files. Sometimes, the delivery will include these files, this is why the analyzer will skip the generated JavaScript files if we find their original TypeScript file.

## Function Point, Quality, and Sizing support

This extension provides the following support:

-   Function Points (transactions): a green tick indicates that OMG
    Function Point counting and Transaction Risk Index are supported
-   Quality and Sizing: a green tick indicates that CAST can measure
    size and that a minimum set of Quality Rules exist

| Technology | Function Points (transactions) | Quality and Sizing |
| ---------- | :----------------------------: | :----------------: |
| TypeScript |       :white_check_mark:       | :white_check_mark: |
| Angular    |       :white_check_mark:       | :white_check_mark: |
| React      |       :white_check_mark:       | :white_check_mark: |
| Express    |       :white_check_mark:       |        :x:         |
| Mongoose   |       :white_check_mark:       |        :x:         |
| Sequelize  |       :white_check_mark:       |        :x:         |
| TypeORM    |       :white_check_mark:       |        :x:         |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The TypeScript and frameworks extension
requires that the following other CAST extensions are also installed:

-   [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)
-   [ReactJS ](../../../reactjs/com.castsoftware.reactjs/)
-   [Node.js](../../../nodejs/com.castsoftware.nodejs/)

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner:

![](../images/653984104.png)

In this full-stack example, the front-end is implemented in TypeScript
and uses Angular and the back-end is implemented in JavaScript and uses
Express and MongoDB. If analyzing a web-application with the back-end
implemented in TypeScript, the data-base access would be missing since
this extension does not yet support any database access framework.

### Detailed analysis results per framework

See below for more details about how the extension handles each
supported framework - see [Results](results).

### Objects

The following objects are identified:

| Icon | Metamodel name | Code Reference (example) |
|:---:|---|---|
| ![](../images/653984103.png) | Typescript Module |  |
| ![](../images/653984102.jpg) | Typescript Namespace | <pre>namespace A {<br>   // not exported<br>   function a() {<br>   }<br>}</pre> |
| ![](../images/653984101.jpg) | Typescript Class | <pre>export class TodoStorage implements ... {<br>   ...<br>}</pre> |
| ![](../images/653984100.png) | Class Initializer | <pre>export class TodoStorage implements ... {<br>  field = new Todo();<br>  ...<br>}</pre> |
| ![](../images/653984099.jpg) | Typescript Method | <pre>export class TodoStorage implements ... {<br>   // Standard method<br>   getTodos(): Observable<Todo[]> {<br>   }<br>   // Arrow method     m1 = () => {alert("m1 is called")}<br>}</pre> |
| ![](../images/653984098.jpg) | Typescript Interface | <pre>export interface ITodoScope extends... {<br>   ...<br>}</pre> |
| ![](../images/653984097.jpg) | Typescript Function | <pre>// Named function<br>function add(x, y) {<br>   return x + y;<br>}<br><br>// Anonymous function<br>function(x, y) { return x + y; };<br><br>// Arrow function<br>var f = (x,y) => { return x + y };<br><br>// Anonymous arrow function<br>(x,y) => x+y;<br><br>// Generator function<br>function* infiniteSequence() {<br>   var i = 0;<br>   while(true) {<br>      yield i++;<br>   }<br>}</pre> |
| ![](../images/653984103.png) | Typescript Built-in Object | <pre>function a() : void {<br>    var strValue = new String("text");<br>    let numValue = new Number(123);<br>}<br></pre> |
| ![](../images/TS_Field.png) | Typescript Field | <pre>class A {<br>    someField: number = 5;<br>}<br></pre> |
| ![](../images/653984096.jpg) | Angular Component |  |
| ![](../images/653984095.jpg) | Angular Directive |  |
| ![](../images/653984094.jpg) | Angular GET http service<br>TypeScript GET http service |  |
| ![](../images/653984093.jpg) | Angular POST http service<br>TypeScript POST http service |  |
| ![](../images/653984092.jpg) | Angular PUT http service<br>TypeScript PUT http service |  |
| ![](../images/653984091.jpg) | Angular DELETE http service<br>TypeScript DELETE http service |  |
| ![](../images/653984090.png) | HTML5 HTML fragment |  |
| ![](../images/653984089.jpg) | ReactJS Application |  |
| ![](../images/653984088.png) | ReactJS Component |  |
| ![](../images/653984087.jpg) | ReactJS Form |  |
| ![](../images/653984086.png) | Node.js Delete Operation |  |
| ![](../images/653984085.png) | Node.js Get Operation |  |
| ![](../images/653984084.png) | Node.js Post Operation |  |
| ![](../images/653984083.png) | Node.js Put Operation |  |
| ![](../images/653984082.png) | NodeJS MongoDB connection |  |
| ![](../images/653984081.png) | NodeJS MongoDB collection |  |
| ![](../images/653984080.png) | TypeScript SQL Query |  |
| ![](../images/653984079.png) | Node.js AWS SQS Publisher<br>Node.js AWS SNS Publisher<br>Node.js Azure Service Bus Publisher |  |
| ![](../images/653984078.png) | Node.js AWS SQS Receiver<br>Node.js AWS SNS Subscriber<br>Node.js Azure Service Bus Receiver |  |
| ![](../images/653984077.png) | Node.js AWS Unknown SQS Publisher<br>Node.js AWS Unknown SNS Publisher<br>Node.js Azure Unknown Service Bus Publisher |  |
| ![](../images/653984076.png) | Node.js AWS Unknown SQS Receiver<br>Node.js AWS Unknown SNS Subscriber<br>Node.js Azure Unknown Service Bus Receiver |  |
| ![](../images/653984075.png) | Node.js Call to Lambda |  |
| ![](../images/653984074.png) | Node.js Call to unknown Lambda |  |
| ![](../images/653984057.png) | NodeJS SignalR Hub Method |  |
| ![](../images/653984072.png) | NodeJS Azure SignalR Call to Hub Method |  |
| ![](../images/653984071.png) | NodeJS Azure SignalR Call to Unknown Hub Method |  |
| ![](../images/653984070.png) | Node.js S3 Bucket<br>Node.js Azure Blob Container |  |
| ![](../images/653984069.png) | Node.js S3 Unknown Bucket<br>Node.js Azure Unknown Blob Container |  |
| ![](../images/653984064.png) | NodeJS Azure Function |  |
| ![](../images/653984063.png) | NodeJS Call to Azure Function |  |
| ![](../images/653984062.png) | NodeJS Call to Unknown Azure Function |  |
| ![](../images/653984061.png) | NodeJS CosmosDB Collection<br>NodeJS AWS DynamoDB Table<br>NodeJS GCP Bigtable table |  |
| ![](../images/653984060.png) | NodeJS CosmosDB Unknown Collection<br>NodeJS GCP Unknown Bigtable table |  |
| ![](../images/CAST_NodeJS_Unknown_MongoDB_Collection.png) | NodeJS Unknown MongoDB collection |  |
| ![](../images/CAST_NodeJS_Unknown_MongoDB_Connection.png) | NodeJS Unknown MongoDB connection |  |
| ![](../images/653984056.png) | TypeScript Redux Action Handler |  |

### Links

Analysis of the TypeScript application will result in the following
links:

-   callLink: Created when a method or a function is called. These links connect TypeScript Method and TypeScript Function elements between them.
-   inheritExtendLink: Represents inheritance between Typescript Class and Typescript Class objects.
-   inheritImplementLink: Represents inheritance between:
    -   Typescript Class and Typescript Interface objects,
    -   Typescript Method and Typescript Interface Method objects,
    -   Typescript Method and Typescript Abstract Method objects.
-   inheritOverrideLink: Represents inheritance between Typescript Method and Typescript Method objects.
-   relyonLink: Created when a class or an interface is called. The caller is a callable. The callee can be:
    -   Typescript Class,
    -   Typescript Interface,
-   accessWriteLink: Created when a value is assigned to a field. These links connect:
    -   Typescript Function and Typescript Field objects,
    -   Typescript Method and Typescript Field objects,
    -   Typescript ClassInitializer and Typescript Field objects.
-   accessReadLink: Created when a field is called. These links connect:
    -   Typescript Function and Typescript Field objects,
    -   Typescript Method and Typescript Field objects.

<details>

<summary><h4>Example analysis results</h4></summary>

##### callLink

```typescript
class A {
  method(){};
}

function main(param: A)
{
    param.method();
}

```

![](../images/example_callLink.png)

##### inheritExtendLink

```typescript
abstract class Animal {};

class Dog extends Animal {
}
```


![](../images/example_extends.png)


##### inheritImplementLink

```typescript
interface Animal {};

class Dog implements Animal {
}
```

![](../images/example_implements.png)

##### inheritOverrideLink

```typescript
class BarClass
{
    barMethod(): void {};
}

class FooClass extends BarClass
{
    barMethod(): void{};
}

```

![](../images/example_override.png)

##### relyOnLink

Example 1 (parameter type):
```typescript
class FooType{};

function fooFunc(param: FooType){

}

```

![](../images/example_relyOn_1.png)

Example 2 (return type):
```typescript
class FooClass{};

function fooFunc() : FooClass{
    var a = new Object();
    return a;
}

```

![](../images/example_relyOn_2.png)

Example 3 (variable declaration):
```typescript
class FooClass {}

function fooFunc() : void {
    let someVariable: FooClass;
}
```

![](../images/example_relyOn_3.png)

##### accessWriteLink

```typescript
class Foo {
    someField: number = 5;

	fooMethod(){
        return this.someField;
    }
}

```

![](../images/example_accessWrite.png)

##### accessReadLink

```typescript
class Foo {
    someField = 5;

    getSomeField(){
        return this.someField;
    }
}
```

![](../images/example_accessRead.png)

</details>

### External libraries

When an import is made to an external library (with the library defined
in a package.json file or provided within the node_module folders), an
IncludeLink is created between the TypeScript file and the external
library which is of the type "HTML5 JavaScript Source Code". This
feature requires using a version of com.castsoftware.html5 ≥
2.1.24-funcrel. For more details about external libraries, check the
com.castsoftware.html5 documentation.

### Rules

The following rules are shipped with this extension:

| Release | Link |
|---------|------|
| 1.14.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=\|\|1.14.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=%7C%7C1.14.0-funcrel) |
| 1.14.0-beta3 | [https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=\|\|1.14.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=%7C%7C1.14.0-beta3) |
| 1.14.0-beta2 | [https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=\|\|1.14.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=%7C%7C1.14.0-beta2) |
| 1.14.0-beta1 | [https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=\|\|1.14.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_typescript&ref=%7C%7C1.14.0-beta1) |

The rule "Avoid too many copy-pasted artifacts" depends
on com.castsoftware.html5 extension. It will be activated
automatically for TypeScript source code when using a version
of com.castsoftware.html5 \>= 2.0.15-funcrel.

### Data sensitivity

This extension is capable of setting a property on NoSQL collection and Cloud File storage objects for the following:

-   custom sensitivity
-   GDPR
-   PCI-DSS

See [Data Sensitivity](../../../../multi/data-sensitivity/) for more information.

## Limitations

-   Limitations for support of the following frameworks are given in
    their own section:
    -   [Angular](results/angular/)
    -   [NestJS](results/nodejs-nestjs/)
    -   [TypeORM](results/nodejs-typeorm/)
    -   [Fastify](results/nodejs-fastify/)
    -   [AWS](results/nodejs-aws/)
-   Calls between JavaScript and TypeScript source codes are supported
    only in some simple cases.
-   The use of setters and getters is not supported.
-   Passing the URL strings directly (or string variables referring to
    URLs) as arguments to web-service calls is supported for many use
    cases. However, passing them through http.RequestOptions (containing
    metadata) is work in progress.
-   String concatenations using the operator '+' inside loops do not
    raise violations currently.
-   The cyclomatic complexity number might appear underestimated in
    callables containing loops with complex conditional expressions.
-   A single production environment file is supported (see corresponding
    section above).
-   The use of bind method is not supported and would lead to missing
    callLinks.
-   The use of Object.freeze method is not supported.
-   [React Without
    JSX](https://reactjs.org/docs/react-without-jsx.html)
    is not supported.
-   The spread operator "..." is not supported.
