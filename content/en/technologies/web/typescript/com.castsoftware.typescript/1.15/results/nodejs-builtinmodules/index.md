---
title: "Node.js - Built-in modules support"
linkTitle: "Node.js - Built-in modules"
type: "docs"
---

## Introduction

This section describes support for Node.js [built-in
modules](https://nodejs.org/api/documentation.html).

## Http, Http2, Https modules

This extension currently supports the use of these modules for http
requests from the client-side only (i.e. the use of these modules as
server is not yet supported). The analysis of the previous code will
generate Node.js Get HttpRequest service object which is called
by my_request function. A link between my_request function and
the anonymous handler function is also added.

``` java
import * as https from "https"

function my_request(){
  https.get('https://encrypted.google.com/', (res) => {
    console.log('statusCode:', res.statusCode);
    console.log('headers:', res.headers);
}
```

![](../../../images-results/653984137.png)