---
title: "Node.js - NestJS support"
linkTitle: "Node.js - NestJS"
type: "docs"
---

## Introduction

NestJS is a framework for building Node.js server-side
applications. NestJS provides a level of abstraction above many common
Node.js frameworks. The following features provided by NestJS are
supported:

## Controllers

A controller's purpose is to receive specific requests for the
application. A controller is defined using a classe@ and decorators.

The analysis of the following code will create a Node.js Get
Operation named cats/all/ with a call link from the operation to
the handler method findAll. The URL corresponds to the concatenation
of the argument of the @Controller decorator with that of the @Get
decorator of the method. The decorator of each method defines the type
of the operation.

``` java
import { Controller, Get } from '@nestjs/common';

@Controller('cats')
export class CatsController {
  @Get('/all')
  findAll(): string {
    return 'This action returns all cats';
  }}
```

![](../../../images-results/653984142.png)

## Middleware

Middleware is a function that is called before the route handler.
For instance, in the following source code, the logger function will be
called before each handler of the CatsController. Our analyzer will
create a callLink between the operations defined in the CatsController
and the logger.

``` java
import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
export function logger(req: Request, res: Response, next: Function) {
  console.log(`Request...`);
  next();
};

@Module({
  imports: [CatsModule],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(logger)
      .forRoutes(CatsController);
  }
}
```

![](../../../images-results/653984141.png)

## HTTP module

NestJS allows performing http requests using the HTTP module. The
analysis of the following source code will create a Node.js Get
HttpRequest named foo/path/ with a call link from the findAll
method to that request:

``` java
import { HttpService, Injectable, Logger } from '@nestjs/common';

@Injectable()
export class CatsService {
  constructor(private httpService: HttpService) {}

  findAll(): Observable<AxiosResponse<Cat[]>> {
    return this.httpService.get('foo/path');
  }
}
```

![](../../../images-results/653984140.png)

## Known limitations

All NestJS features which are not documented here are not
supported. 
