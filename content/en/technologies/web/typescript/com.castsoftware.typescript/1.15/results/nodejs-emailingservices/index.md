---
title: "Node.js - Emailing Services support"
linkTitle: "Node.js - Emailing Services"
type: "docs"
---

This section describes support for Node.js emailing
services nodemailer and sendgrid/mail.

When analyzing the following source code, an email object is created
with a callLink from the my_send_mail function:

``` js
import * as nodemailer from 'nodemailer';

async function my_send_mail() {
  let transporter = nodemailer.createTransport({
    //...
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Fred Foo" <foo@example.com>', // sender address
    to: "bar@example.com, baz@example.com", // list of receivers
    //...
  });
}
```

This will produce the following result:

![](../../../images-results/653984134.png)

Similarly when analyzing the following source code, an email object is
created with a callLink from the my_send_mail function:

``` js
const sgMail = require('@sendgrid/mail');

function my_send_mail(msg){
  sgMail
   .send(msg)
}
```

![](../../../images-results/653984134.png)