---
title: "Node.js - TypeORM support"
linkTitle: "Node.js - TypeORM"
type: "docs"
---

## Introduction

This section describes support for the Node.js TypeORM framework.

## Connection

TypeORM can be used both with SQL and NoSQL databases. The database type
is set through the connection option type:

``` java
import {createConnection, Connection} from "typeorm";

const connection = await createConnection({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "test",
    password: "test",
    database: "test"
});
```

The only NoSQL database which is supported by TypeORM is MongoDB. A
connection object is created by our analyzer only for MongoDB databases.
The name of the connection is the URL mongodb://localhost/test which
is constructed using the host and database values. An Entity is
a class that maps to a database table (or collection when using
MongoDB):

``` java
import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    isActive: boolean;

}
```

When a MongoDB database is used, for each entity, the extension
creates a MongoDB collection object. A parentLink between that
collection and the corresponding connection is added. For an SQL
connection, the entity will be associated with the SQL table having the
same name (if that table exists).

### Link to table/collection

TypeORM provide several ways to access and/or update a table or
collection. One can use an entity manager, a repository, or a query
builder. The use of InjectRepository from @nestjs/typeorm is also
supported. Here is an example with a repository:

``` java
import {getRepository} from "typeorm";
import {User} from "./user";

function update_user(){
    const userRepository = getRepository(User); 
    const user = await userRepository.findOne(1);
    user.name = "fooname";
    await userRepository.save(user);
}
```

-   The userRepository.findOne(1) method call generates a
    "useSelectLink" to the User table/entity.
-   The userRepository.save(user) method call  generates a
    "useUpdateLink" to the User table/entity.

Example for a mongodb database: both useSelect and useUpdate links
are created between the update_user function and the User entity
which belongs to the \<Default\> connection:

![](../../../images-results/653984165.png)

Example for a SQL database: both useSelect and useUpdate links are
created between the update_user function and the user table. 

![](../../../images-results/653984164.png)

## Cascades

In the following example, the User entity has its column profile
with a one-to-one relation with the Profile entity and cascade set
to true:

``` java
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn} from "typeorm";
import {Profile} from "./profile";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToOne(type => Profile, {
        cascade: true
    })
    @JoinColumn()
    profile: Profile;

}
```

When the profile column of a user instance is saved, if the
profile column of that instance was updated with a profile instance,
that profile instance is also saved.

``` java
import {getRepository} from "typeorm";
import {User} from "../entity_dir/user";
import {Profile} from "../entity_dir/profile";


function update_user(){
    const userRepository = getRepository(User); 
    const user = await userRepository.findOne(1);
    const profile = new Profile()
    user.name = "fooname";
    user.profile = profile
    await userRepository.save(user);
}
```

In the previous example, a useUpdate link is created between the
update_user function and both the user and profile tables/entities:

![](../../../images-results/653984163.png)

## SQL queries

Plain SQL queries can also be carried out using TypeORM such as with the
following code:

``` java
import {getConnection} from "typeorm";

export class A {
    public foo() {
        const manager = getConnection().manager;
        const rawData = await manager.query(`SELECT * FROM USER`);
    }
} 
```

In this example, a 'TypeScript SQL Query' object is created and a callLink
between the foo method and that query is added.
The [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer)
or [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable)
can then link that query with the corresponding table:

![](../../../images-results/653984162.png)

## Known limitations

The following features are not supported

-   use of ormconfigs.yml, ormconfigs.env and ormconfigs.xm ORM configuration files
-   custom repositories
-   view entities
-   entity listeners and subscribers
-   connectionManager