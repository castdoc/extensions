---
title: "Node.js - Fastify support"
linkTitle: "Node.js - Fastify support"
type: "docs"
---

This section describes support for the Node.js Fastify framework.

Supported fastify methods are: get, put, post, delete,
route. The analysis of the following code will create a Node.js
Get Operation named /login/{} with a call link from the operation
to the handler function f:

``` java
import * as fastify from 'fastify'

function f(request, reply) {
   reply.send({ hello: 'world' })
}

const server: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify({})
// Declare a route
server.get('/login/:name', f)
```

![](../../../images-results/653984151.png)

## Known limitations

-   prefixes are not supported
-   fastify-mongodb is not supported
