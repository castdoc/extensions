---
title: "Node.js - Request support"
linkTitle: "Node.js - Request"
type: "docs"
---

This section describes support for the Node.js request,
request-promise, request-promise-native, and
request-promise-any framework.

The analysis of the following code will create a Node.js Post
HttpRequest named foo/post/ with a call link from my_func to
that request:

``` java
import * as rp from 'request-promise-native'

function my_post(){
  var options = {
    method: 'POST',
    uri: 'foo/post',
    body: {
        some: 'payload'
    },
    json: true // Automatically stringifies the body to JSON
  };
 
  rp(options)
    .then(function (parsedBody) {
        // POST succeeded...
    })
    .catch(function (err) {
        // POST failed...
    });
}
```
![](../../../images-results/653984154.png)