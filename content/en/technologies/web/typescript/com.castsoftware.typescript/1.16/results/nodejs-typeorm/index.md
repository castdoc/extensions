---
title: "Node.js - TypeORM support"
linkTitle: "Node.js - TypeORM"
type: "docs"
---

## Introduction

TypeORM is an ORM that can run in NodeJS, Browser, Cordova, PhoneGap, Ionic, React Native, NativeScript,
Expo, and Electron platforms and can be used with TypeScript - see https://typeorm.io/ for more information.

## Objects

TypeORM can be used both with SQL and NoSQL databases.

- For the SQL:
  - a `NodeJS Entity` object when the decorator `Entity` attached to a class declaration or an `EntitySchema` instance is found;
  - a `NodeJS Entity Operation` object when one of the supported TypeORM APIs is used and linked to one of the entities;
  - a `TypeScript SQL Query` object when the APIs `query` or `createQueryBuilder` is found.

- For the NoSQL, the only NoSQL database supported is MongoDB.
  - a `NodeJS MongoDB connection` object when the APIs `createConnection`, `createConnections`,
or a `DataSource` instance is found. The name of connection is constructed from the host and database values;
  - a `NodeJS MongoDB collection` object when the decorator `Entity` attached to a class declaration
or an `EntitySchema` instance is found, and linked to one of the connections.

| Icon | Description |
|---|---|
| ![](../../../images/nodejs_entity.png) | NodeJS Entity |
| ![](../../../images/nodejs_entity_operation.png) | NodeJS Entity Operation |
| ![](../../../images/653984080.png) | TypeScript SQL Query |
| ![](../../../images/653984082.png) | NodeJS MongoDB connection |
| ![](../../../images/653984081.png) | NodeJS MongoDB collection |

## Supported persistence SQL databases

### Supported operations

| Entity Operation | Supported APIs                                                                                                                                                                                                                                                                                                                                                                                                        |
|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Add              | <ul><li>insert</li></ul>                                                                                                                                                                                                                                                                                                                                                                                              |
| Update           | <ul><li>preload</li><li>increment</li><li>decrement</li><li>update</li><li>save</li><li>merge</li><li>upsert</li></ul>                                                                                                                                                                                                                                                                                                |
| Remove           | <ul><li>remove</li><li>delete</li><li>clear</li><li>softDelete</li><li>softRemove</ul>                                                                                                                                                                                                                                                                                                                                |
| Select           | <ul><li>find</li><li>findAndCount</li><li>findByIds</li><li>findOne</li><li>findOneOrFail</li><li>count</li><li>hasId</li><li>getId</li><li>findBy</li><li>findAndCountBy</li><li>findOneBy</li><li>findOneByOrFail</li><li>findTrees</li><li>findRoots</li><li>findDescendants</li><li>findDescendantsTree</li><li>countDescendants</li><li>findAncestors</li><li>findAncestorsTree</li><li>countAncestors</li></ul> |

TypeORM provide several ways to access and/or update a table. One can use:
- an entity manager: getManager, getConnection().manager, DataSource().manager;
- a repository: getRepository, getManager().getRepository, DataSource().getRepository;
- a query builder: getConnection().createQueryBuilder, getManager().createQueryBuilder, getRepository().createQueryBuilder, DataSource().createQueryBuilder, DataSource().manager.createQueryBuilder, DataSource().getRepository().createQueryBuilder.

The use of InjectRepository from @nestjs/typeorm is also supported.

### Supported links

| Link Type     | Caller type                                                                               | Callee type                                                            | Comment                                                                                                                                                                                                                                                                                              |
|---------------|-------------------------------------------------------------------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| callLink      | <ul><li>TypeScript Function</li><li>TypeScript Method</li><li>TypeScript Module</li></ul> | <ul><li>NodeJS Entity Operation</li><li>TypeScript SQL Query</li></ul> |                                                                                                                                                                                                                                                                                                      |
| relyonLink    | <ul><li>NodeJS Entity</li></ul>                                                           | <ul><li>TypeScript Class</li></ul>                                     | When the entity is defined using the decorator 'Entity' attached to a class declaration.                                                                                                                                                                                                             |
| useInsertLink | <ul><li>NodeJS Entity Operation: Add</li><li>TypeScript SQL Query</li></ul>               | <ul><li>Table</li><li>Missing Table</li></ul>                          | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |
| useUpdateLink | <ul><li>NodeJS Entity Operation: Update</li><li>TypeScript SQL Query</li></ul>            | <ul><li>Table</li><li>Missing Table</li></ul>                          | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |
| useDeleteLink | <ul><li>NodeJS Entity Operation: Remove</li><li>TypeScript SQL Query</li></ul>            | <ul><li>Table</li><li>Missing Table</li></ul>                          | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |
| useSelectLink | <ul><li>NodeJS Entity Operation: Select</li><li>TypeScript SQL Query</li></ul>            | <ul><li>Table</li><li>Missing Table</li></ul>                          | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed. |

### Cascades

In the following example, the 'User' entity has its column profile with a one-to-one relation
with the 'Profile' entity and cascade set to true:

``` js
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Profile {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    gender: string;
    
    @Column()
    photo: string;
}
```

``` js
import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn } from "typeorm";
import { Profile } from "./profile";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    name: string;
    
    @OneToOne(type => Profile, {
        cascade: true
    })
    
    @JoinColumn()
    profile: Profile;
}
```

When the profile column of a user instance is saved, if the profile column of that instance was updated
with a profile instance, that profile instance is also saved.

``` js
import { getRepository } from "typeorm";
import { User } from "../entity_dir/user";
import { Profile } from "../entity_dir/profile";

function update_user(){
    const userRepository = getRepository(User); 
    const user = await userRepository.findOne(1);
    const profile = new Profile()
    user.name = "fooname";
    user.profile = profile
    await userRepository.save(user);
}
```

In this example, two 'NodeJS Entity' and three 'NodeJS Entity Operation' objects are created.
Two 'relyOn' links are added when 'User' and 'Profile' entities are created from their classes.
Three 'call' links are added from 'update_user' function to three 'NodeJS Entity Operation' objects.
Two 'useUpdate' and one 'useSelect' links are added between the 'NodeJS Entity Operation' objects
and both the 'user' and 'profile' tables:

![](../../../images-results/ts_typeorm_sql1.png)

### SQL Queries

SQL queries can also be carried out using TypeORM such as with the following code:

``` js
import { getManager } from "typeorm";
import { User } from "../entity_dir/user";
import { getConnection } from "typeorm";

export class AuthService {
  private userRepository = getRepository(User);

  signUp = async (userData: CreateUserDTO) => {
    const { username, email, password } = userData;
    const existingUser = await this.userRepository
      .createQueryBuilder()
      .where('username = :username OR email = :email', { username, email })
      .getMany();
  };

  public foo() {
    const manager = getConnection().manager;
    const rawData = await manager.query(`SELECT * FROM USER`);
  }
}
```

In this example, two 'TypeScript SQL Query' objects are created, and two callLink between the two methods and
these queries are added.
The [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer)
or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable)
can then link these queries to the corresponding table:

![](../../../images-results/ts_typeorm_sql2.png)

## Supported persistence MongoDB database

### Supported links

| Link Type     | Caller type                                                                               | Callee type                                 | Supported APIs                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|---------------|-------------------------------------------------------------------------------------------|---------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| useInsertLink | <ul><li>TypeScript Function</li><li>TypeScript Method</li><li>TypeScript Module</li></ul> | <ul><li>NodeJS MongoDB collection</li></ul> | <ul><li>insertMany</li><li>insertOne</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| useUpdateLink | <ul><li>TypeScript Function</li><li>TypeScript Method</li><li>TypeScript Module</li></ul> | <ul><li>NodeJS MongoDB collection</li></ul> | <ul><li>findOneAndUpdate</li><li>replaceOne</li><li>updateMany</li><li>updateOne</li><li>rename</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                  |
| useDeleteLink | <ul><li>TypeScript Function</li><li>TypeScript Method</li><li>TypeScript Module</li></ul> | <ul><li>NodeJS MongoDB collection</li></ul> | <ul><li>deleteMany</li><li>deleteOne</li><li>findOneAndDelete</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| useSelectLink | <ul><li>TypeScript Function</li><li>TypeScript Method</li><li>TypeScript Module</li></ul> | <ul><li>NodeJS MongoDB collection</li></ul> | <ul><li>aggregate</li><li>count</li><li>group</li><li>collectionIndexInformation</li><li>initializeOrderedBulkOp</li><li>initializeUnorderedBulkOp</li><li>isCapped</li><li>listCollectionIndexes</li><li>mapReduce</li><li>parallelCollectionScan</li><li>stats</li><li>createCollectionIndex</li><li>createCollectionIndexes</li><li>dropCollectionIndex</li><li>dropCollectionIndexes</li><li>collectionIndexes</li><li>collectionIndexExists</li><li>reIndex</li><li>exists</li><li>existsBy</li></ul> |

TypeORM provide several ways to access and/or update a collection. One can use:
- an entity manager: getMongoManager, getConnection().mongoManager, DataSource().manager;
- a repository: getMongoRepository, getConnection().getMongoRepository, DataSource().getRepository.

The use of InjectRepository from @nestjs/typeorm is also supported.

### Link to collection

In the following example, a 'NodeJS MongoDB connection' object is created.

``` js
import { DataSource } from "typeorm";

export const myDataSource = new DataSource({
    type: "mongodb",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "admin",
    database: "test",
    entities: [
        "../entity_dir/*.ts"
    ],
    synchronize: true,
    logging: false
})
```

When a MongoDB database is used, for each entity, the extension creates a 'NodeJS MongoDB collection' object.
A parentLink between that collection and the corresponding connection is added. 

``` js
import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    firstName: string;
    
    @Column()
    lastName: string;
    
    @Column()
    isActive: boolean;
}
```

TypeORM provides several ways to access and/or update a collection.
Here is an example with a repository:

``` js
import { myDataSource } from "./create_connection_datasource";
import { User } from "../entity_dir/user";

function update_user(){
    const userRepository = myDataSource.getRepository(User);
    const user = await userRepository.findOne({
      id: 1,
    })
    user.name = "fooname";
    await userRepository.save(user);
}
```

Both 'useSelect' and 'useUpdate' links are created between the 'update_user' function and the 'User' collection
which belongs to the 'mongodb://localhost/test' connection:

![](../../../images-results/653984165.png)

## Known limitations

The following features are not supported
-   use of ormconfigs.yml, ormconfigs.env and ormconfigs.xm ORM configuration files
-   custom repositories
-   view entities
-   entity listeners and subscribers
-   connectionManager
