---
title: "Node.js - Sequelize support"
linkTitle: "Node.js - Sequelize"
type: "docs"
---

## Introduction

Sequelize is a promise-based Node.js ORM tool for Postgres, MySQL, SQLite, Microsoft SQL Server, 
Oracle Database - see https://sequelize.org/docs/ for more information.

## Objects

This extension creates the entity, entity operation and query objects:

- a 'NodeJS Entity' object when the APIs 'define' or 'init' of a Sequelize.Model instance is found.
- a 'NodeJS Entity Operation' object when one of the supported Sequelize APIs is used and linked to one of the entities.
- a 'TypeScript SQL Query' object when the API 'query' is found.

| Icon                                             | Description              |
|--------------------------------------------------|--------------------------|
| ![](../../../images/nodejs_entity.png)           | NodeJS Entity           |
| ![](../../../images/nodejs_entity_operation.png) | NodeJS Entity Operation |
| ![](../../../images/653984080.png)               | TypeScript SQL Query     |

## Supported persistence SQL databases

### Supported operations

| Entity Operation | Supported APIs                                                                                                                                                                                                  |
|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Add              | <ul><li>Model.create</li><li>Model.bulkCreate</li></ul>                                                                                                                                                         |
| Update           | <ul><li>Model.update</li><li>Model.restore</li><li>Model.increment</li><li>Model.decrement</li></ul>                                                                                                            |
| Remove           | <ul><li>Model.destroy</li></ul>                                                                                                                                                                                 |
| Select           | <ul><li>Model.findAll</li><li>Model.findByPk</li><li>Model.findOne</li><li>Model.findOrCreate</li><li>Model.findAndCountAll</li><li>Model.count</li><li>Model.max</li><li>Model.min</li><li>Model.sum</li></ul> |

'Model' above can be defined by 'Sequelize.define' or using a class extending 'Sequelize.Model'.

### Supported links

| Link Type     | Caller type                                                                               | Callee type                                                             | Comment                                                                                                                                                                                                                                                                                               |
|---------------|-------------------------------------------------------------------------------------------|-------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| callLink      | <ul><li>TypeScript Function</li><li>TypeScript Method</li><li>TypeScript Module</li></ul> | <ul><li>NodeJS Entity Operation</li><li>TypeScript SQL Query</li></ul> |                                                                                                                                                                                                                                                                                                       |
| relyonLink    | <ul><li>NodeJS Entity</li></ul>                                                          | <ul><li>TypeScript Class</li></ul>                                      | When the entity is defined using a class extending 'Sequelize.Model'.                                                                                                                                                                                                                                 |
| useInsertLink | <ul><li>NodeJS Entity Operation: Add</li><li>TypeScript SQL Query</li></ul>              | <ul><li>Table</li><li>Missing Table</li></ul>                           | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed.  |
| useUpdateLink | <ul><li>NodeJS Entity Operation: Update</li><li>TypeScript SQL Query</li></ul>           | <ul><li>Table</li><li>Missing Table</li></ul>                           | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed.  |
| useDeleteLink | <ul><li>NodeJS Entity Operation: Remove</li><li>TypeScript SQL Query</li></ul>           | <ul><li>Table</li><li>Missing Table</li></ul>                           | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed.  |
| useSelectLink | <ul><li>NodeJS Entity Operation: Select</li><li>TypeScript SQL Query</li></ul>           | <ul><li>Table</li><li>Missing Table</li></ul>                           | Created by [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer) when DDL source files are analyzed or by [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable) when the object is not analyzed . |

## Example

Take the following codes:

``` js
import Sequelize from 'sequelize';
const sequelize = new Sequelize('sqlite::memory:');

class User extends Sequelize.Model { }
User.init(
  {
    // ... (attributes)
  },
  {
    sequelize,
    modelName: 'user',
  }
);

await User.findAll().then(t_user=> { });

const Post = sequelize.define(
  'post',
  {
    // ... (attributes)
  },
  {
    freezeTableName: true,
  },
);

await Post.update(
  { lastName: 'Doe' },
);

function userSearch (req) {
    var query = "SELECT name,id FROM Users WHERE login='" + req.body.login + "'";
    db.sequelize.query(query, {
        model: db.User
    })
      .then(user => {
    })
};
```

In this example, two 'Node.js Entity' objects, two 'Node.js Entity Operation' objects, and
a 'SQL Query' object are created.
A 'relyOn' link is added when entity 'user' is defined using a class extending 'Sequelize.Model'.
The [SQL Analyzer](../../../../../../sql/extensions/com.castsoftware.sqlanalyzer)
or [Missing tables and procedures for Node.js](../../../../../../sql/extensions/missing-tables/com.castsoftware.nodejs.missingtable)
links these entity operations and this query with the corresponding tables.
In the present case, this extension creates two 'useSelect' and a 'useUpdate' links
to the missing tables 'Users' and 'post':

![](../../../images-results/ts-sequelize.png)
