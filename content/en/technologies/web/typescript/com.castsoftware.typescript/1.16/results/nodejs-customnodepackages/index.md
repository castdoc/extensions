---
title: "Node.js - Custom Node packages support"
linkTitle: "Node.js - Custom Node packages"
type: "docs"
---

This section describes support for Node.js custom node packages.

It is possible to develop custom node packages. A package is a file
or a directory that is described by a package.json file.

An application can combine mutliple packages. The packages can be built
together using npm. npm adds these packages to a node_mudules directory.
Note that the analyzer will skip everything which is inside a
node_modules directory. Users should provide their custom packages at
the root of the analyzed source code. Let's consider a package named
my_proj. The root dir contains a package.json file which defines
the project name:

my_proj/package.json

``` yml
{ "name": "@myprojname"
}
```

Everything which is exported (or re-exported) from the
my_proj/src/index.ts can be imported from any project which uses this
package. 

my_proj/src/index.ts

``` java
export function my_func(){
}
```

For instance when an other package uses this package, it can import the
my_func function using the name of the package for the import:

otherproj/src/foo.ts

``` js
import {my_func} from '@myprojname'


function other_func(){
  my_func() 
}
```

When analyzing these source code, a callLink will be created between
other_func and my_func:

![](../../../images-results/653984131.png)