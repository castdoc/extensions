---
title: "Node.js - Axios support"
linkTitle: "Node.js - Axios"
type: "docs"
---

This section describes support for the Node.js Axios related frameworks.

The following packages are supported:

| Library                       | Supported versions                       |
|-------------------------------|------------------------------------------|
| axios                         | 0.x; 1.x                                 |
| @nestjs/axios                 | 1.x; 2.x; 3.x                            |      
| vue-axios                     | 1.x; 2.x; 3.x                            |

### Support example for axios
The analysis of the following code will create a Node.js Put
HttpRequest named foo/put/ with a call link from my_func to
that request:

``` javascript
import * as axios from 'axios';

function my_func(){
  axios('foo/put', {method:'put'})

     .then(function (response) {
       // handle success
       console.log(response);
     })
}
```
![](../../../images-results/653984157.png)

### Support example for @nestjs/axios
Whenever an API call to one of a HTTP method of an HttpService (from @nestjs/axios) instance is found a Request service object is created with a _callLink_ from the caller. 
For instance when analyzing the following source code,  
``` javascript
import { HttpService } from '@nestjs/axios'

export class AppService {
  constructor(
    private cacheManager: Cache,
    private readonly httpService: HttpService
  ) {}

  async foocaller(params) {
      this.httpService.post('foo/post', params)
  }
}
```
you will get the following result:
![](../../../images/nestjsaxios.png)

### Support example for vue-axios
Whenever an axios instance is added to an application as follow
``` javascript 
import * as Vue from 'vue' // in Vue 3
import axios from 'axios'
import VueAxios from 'vue-axios'

const app = Vue.createApp(...)
app.use(VueAxios, axios)
```

and this axios instance is used within the vue application such as: 
``` javascript 
    export default {
      name: 'App',
      methods: {
        getList() {
          this.axios.get("foo/get").then((response) => {
            console.log(response.data)
          })
        }
      }
    }
```
a request service is created by this extension with a _callLink_ from the caller:
![](../../../images/vueaxios.png)
