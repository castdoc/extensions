---
title: "AngularJS - 2.1"
linkTitle: "2.1"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.angularjs

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for AngularJS.

### In what situation should you install this extension?

If your Web application contains AngularJS source code and you want
to view these object types and their links with other objects, then you
should install this extension:

-   Creates AngularJS object structure (see below)
-   Creates links between these objects and links between HTML files and controller scope elements
-   Directives are partly handled:
    -   links between HTML tags and directives are handled
    -   links between HTML attributes in directive sections and
        directive scope items are handled

Regarding Front-End to Back-End connections, we support the following cross-technology stacks:

| AngularJS Front-End connected JEE Back-End | AngularJS Front-End connected Node.js Back-End |
|---|---|
| ![](../images/418185747.png) | ![](../images/418185726.png) |

## Supported AngularJS versions

The following table displays the list of AngularJS versions that this
extension supports:

| AngularJS Version | Transaction Support | Rules Support | Type of Support |
|---|:-:|:-:|---|
| 1.0 | :white_check_mark: | :white_check_mark: | Full-stack use case support |
| 1.2 | :white_check_mark: | :white_check_mark: | Full-stack use case support |
| 1.3 | :white_check_mark: | :white_check_mark: | Full-stack use case support |
| 1.4 | :white_check_mark: | :white_check_mark: | Full-stack use case support |
| 1.5 | :white_check_mark: | :white_check_mark: | Full-stack use case support |
| 1.6 | :white_check_mark: | :white_check_mark: | Full-stack use case support |
| 1.7 | :white_check_mark: | :white_check_mark: | Full-stack use case support |
| 1.8 | :white_check_mark: | :white_check_mark: | Full-stack use case support |

#### Comparison with existing support for JavaScript

CAST AIP has provided support for analyzing JavaScript via its JEE and
.NET analyzers (provided out of box in CAST AIP) for some time now.
The HTML5/JavaScript extension (on which the AngularJS extension
depends) also provides support for JavaScript but with a focus on web
applications. CAST highly recommends that you use this extension if your
Application contains JavaScript and more specifically if you want to
analyze a web application, however you should take note of the
following:

-   You should ensure that you configure the extension to NOT analyze
    the back end web client part of a .NET or JEE application.
-   You should ensure that you configure the extension to ONLY analyze
    the front end web application built with the HTML5/JavaScript that
    communicates with the back end web client part of a .NET or JEE
    application.
-   If the back end web client part of a .NET or JEE application is
    analyzed with the AngularJS extension and with the native .NET/JEE
    analyzers, then your results will reflect this - there will be
    duplicate objects and links (i.e. from the analyzer and from the
    extension) therefore impacting results and creating erroneous
    Function Point data.

In CAST AIP ≥ 8.3.x support for analyzing JavaScript has been withdrawn
from the JEE and .NET analyzers.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The AngularJS extension requires that
the following other CAST extensions are also installed:

-   [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)
-   [Universal Linker](../../../../multi/com.castsoftware.wbslinker/)

## Download and installation instructions

The extension will be automatically downloaded and installed in CAST
Console.

## Packaging, delivering and analyzing your source code

Once the extension is downloaded and installed, you can now package your
source code and run an analysis. The process of packaging, delivering
and analyzing your source code is described below:

### Packaging and delivery

Note that the AngularJS extension does not contain any CAST Delivery
Manager Tool discoverers or extractors, therefore, no "AngularJS"
projects will be detected. However, the [Web Files
Discoverer](../../../com.castsoftware.webfilesdiscoverer/) extension
will be automatically installed (it is a "shipped" extension which means
it is delivered with AIP Core) and will automatically detect projects as
HTML5 if specific files are delivered, therefore ensuring that
Analysis Units are created for your source code.

### Analyzing

AIP Console exposes the technology configuration options once a version
has been accepted/imported, or an analysis has been run. Click
Universal Technology (3) in the
[Config](https://doc.castsoftware.com/display/AIPCONSOLE/Application+-+Config) (1)
\>
[Analysis](https://doc.castsoftware.com/display/AIPCONSOLE/Application+-+Config+-+Analysis)
(2) tab to display the available options for your AngularJS source
code:

![](../images/530317404.jpg)

Then choose the relevant Analysis Unit (1) to view the
configuration:

![](../images/530317401.jpg)

![](../images/530317400.jpg)

### Analysis warning and error messages

The following table lists the errors and warnings that may occur
during when analyzing source code with the AngularJS extension:

<table class="relative-table confluenceTable" style="width: 82.5%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 7%" />
<col style="width: 7%" />
<col style="width: 35%" />
<col style="width: 28%" />
<col style="width: 11%" />
</colgroup>
<thead>
<tr class="header">
<th class="confluenceTh"><div>
Message ID
</div></th>
<th class="confluenceTh"><div>
Message Type
</div></th>
<th class="confluenceTh"><div>
<p>Logged during</p>
</div></th>
<th class="confluenceTh"><div>
Impact
</div></th>
<th class="confluenceTh"><div>
Remediation
</div></th>
<th class="confluenceTh"><div>
Action
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd">ANGULARJS-001</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">A link could not be created because of an
internal error.</td>
<td class="confluenceTd"><p> </p></td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">ANGULARJS-002</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">Some directories seem identical and perhaps
source code is duplicated. Results will be more accurate without
duplication of source code</td>
<td class="confluenceTd">Remove duplicated source code to improve
accuracy of results.</td>
<td class="confluenceTd"> </td>
</tr>
<tr class="odd">
<td class="confluenceTd">ANGULARJS-003</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">An html file could not be analyzed. If the html
file is empty, has a strange format, or is not related to AngularJS, you
can ignore this warning.</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">ANGULARJS-004</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">A file was not full analyzed because of an
internal error.</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">ANGULARJS-005</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">A file was not fully analyzed because of an
internal error. The message will list the source code line numbers that
are causing the problem. If nothing important is between these lines you
can ignore the message.</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">ANGULARJS-006</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">An html file referenced in a templateUrl
property in an AngularJS file was not found. If the referenced html file
does not exist, you can ignore the warning.</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">ANGULARJS-007</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">Internal issue with a SQL query that prevents
the correct parent ibject being assigned to AngularJS applications.</td>
<td class="confluenceTd"> </td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
</tbody>
</table>

  

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the
results in the normal manner (for example via CAST Enlighten):

![](../images/418185743.png)

*E.g. : AngularJS Front-end connected to Node.js Back-end*

![](../images/418185745.png)

*E.g. : AngularJS Front-end connected to JEE/Spring MVC Back-end*

### Web Service calls

The following libraries are supported for Web Service HTTP calls:

-   $http
-   $resource
-   Restangular

Once the AngularJS extension analysis is finished, the analyzer will
output the final number of web service calls created.

These web services are resolved to web service operations on the server
side for the following supported
frameworks: [Node.js](../../../nodejs/com.castsoftware.nodejs/), [JAX-RS](../../../../jee/extensions/com.castsoftware.jaxrs/), [Spring
MVC](../../../../jee/extensions/com.castsoftware.springmvc):

#### $http

``` js
$http.get('/api')
    .then(function () {
        return $injector.get('api');
    }, function () {
        return $injector.get('localStorage');
    });
```

``` js
$http.delete('/api/todos')
    .then(function success() {
        return store.todos;
    }, function error() {
        angular.copy(originalTodos, store.todos);
        return originalTodos;
    });
```

#### $resource

``` js
return $resource('resources/scenarios/:id/mapping', {'id':'@id'}, {
    'query': { method: 'GET', isArray: false },
    'update': {method:'PUT', isArray: false }
});  
```

#### Restangular

``` js
angular.module('Patient', [])
    .service('Patient', ['$rootScope','Restangular', function($rootScope, Restangular){
    var Patient = Restangular.service('patients');
    Restangular.extendModel('patients', function(model){
        model.fullname = function(){
            return this.fname+' '+this.sname;
        };
        model.over18 = function(){
            if(this.age >= 18){
                return 'yes';
            }else{
                return 'no';
            }
            
        };
        return model;
    })

    return Patient;
}]);

angular.module('PatientsService', []).service('PatientsService', ['$rootScope','Restangular', 'Patient', function($rootScope, Restangular, Patient){
    var service = {
        currentPatient: [],
        setCurrentPatient: function(patient){
            this.currentPatient = patient;
            $rootScope.$broadcast('currentPatient.change', patient);
        },
        getPatients: function(){
                Patient.getList().then(function(patients){
                $rootScope.$broadcast('patientList.change', patients);
            })
        },
        removePatient: function(patient){
            patient.remove().then(function(){
                //refresh list
                this.getList();
            });
        },
        createPatient: function(newPatient){
            console.log(newPatient);
        }
    };

    return service;
}]);
  
angular.module('PatientsAppointmentsCtrl', ['ngGrid']).controller('PatientsAppointmentsCtrl', ['$scope', 'PatientsService', 'Patient', function($scope, PatientsService, Patient){

    $scope.appointments = [];

    $scope.$on('currentPatient.change', function(event, currentPatient){
        //get appointments
        if(currentPatient != null){
            currentPatient.getList('appointments').then(function(appointments){
                console.log(appointments);
                $scope.appointments = appointments;
            })
        }else{
            $scope.appointments = [];
        }
        
        
    })
```

### Objects

| Icon | Description |
|---|---|
| ![](../images/418185736.png) | AngularJS Application |
| ![](../images/418185718.png) | AngularJS Controller |
| ![](../images/418185737.png) | AngularJS Directive |
| ![](../images/418185735.png) | AngularJS Directive Controller |
| ![](../images/418185742.png) | AngularJS Factory |
| ![](../images/418185741.png) | AngularJS Model function |
| ![](../images/418185746.png) | AngularJS Service |
| ![](../images/418185744.png) | AngularJS Get Resource Service |
| ![](../images/418185739.png) | AngularJS Post Resource Service |
| ![](../images/418185729.png) | AngularJS Put Resource Service |
| ![](../images/418185732.png) | AngularJS Delete Resource Service |
| ![](../images/418185728.png) | AngularJS Get Http Service |
| ![](../images/418185727.png) | AngularJS Post Http Service |
| ![](../images/418185738.png) | AngularJS Put Http Service |
| ![](../images/418185731.png) | AngularJS Delete Http Service |
| ![](../images/418185737.png) | AngularJS State |

{{% alert color="info" %}}Tip: If your application is supposed to be communicating with services, ensure you have GET, POST, PUT, DELETE Service objects created after the analysis.{{% /alert %}}

### Structural Rules

The following structural rules are provided:

| Release | Link |
|---------|------|
| 2.1.18-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.18-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.18-funcrel) |
| 2.1.17-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.17-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.17-funcrel) |
| 2.1.16-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.16-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.16-funcrel) |
| 2.1.15-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.15-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.15-funcrel) |
| 2.1.14-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.14-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.14-funcrel) |
| 2.1.13-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.13-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.13-funcrel) |
| 2.1.12-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.12-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.12-funcrel) |
| 2.1.11-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.11-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.11-funcrel) |
| 2.1.10-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.10-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.10-funcrel) |
| 2.1.9-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.9-funcrel)   |
| 2.1.8-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.8-funcrel)   |
| 2.1.7-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.7-funcrel)   |
| 2.1.6-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.6-funcrel)   |
| 2.1.5-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.5-funcrel)   |
| 2.1.4-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.4-funcrel)   |
| 2.1.3-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.3-funcrel)   |
| 2.1.2-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.2-funcrel)   |
| 2.1.1-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.1-funcrel)   |
| 2.1.0-funcrel  | [https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=\|\|2.1.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_angularjs&ref=%7C%7C2.1.0-funcrel)   |

### Technical notes

#### AngularJS Service objects and Transactions

AngularJS Service objects are not considered by CAST AIP as being
part of any transaction (they are considered as technical function
points). Instead, the AngularJS Service's functions are considered
to be part of the transaction. Therefore when looking at the results of
an AngularJS analysis, links to the AngularJS Service's functions should
be checked to ensure that the expected transactions exist. Transactions
within AngularJS are ALWAYS from functions or methods to functions or
methods (objects which are executable).

#### Avoid using unsanitized AngularJS application (1020546)

The AngularJS Quality Rule Avoid using unsanitized AngularJS
application (1020546) is designed to list violations for missing
items. When this type of Quality Rule is violated, it is not possible to
place a bookmark at the location in the code where the violation occurs
since the violation is for something that is missing. In this situation,
for this specific Quality Rule, the following is done:

-   When the AngularJS application is defined in an HTML file (as is
    sometimes the case) the bookmark will be placed on the entire HTML
    file.
-   If the no HTML file associated to the AngularJS application can be
    found then no bookmark will be created at all. In this situation,
    the message "No associated html file" will be visible in the
    analysis log file and the following error message will be displayed
    in the legacy CAST Engineering Dashboard when an attempt is made to
    view the code of the violation: *The source file is absent from the
    Local Site. Page cannot be displayed.*

## Known Limitations

In this section we list the most significant functional limitations that
may affect the analysis of applications using AngularJS:

### AngularJS objects declared with prototypes

The extension does not detect AngularJS objects when they are declared
with prototype. For example:

``` java
xxx.service('ContentService', ['$rootScope', '$window', '$resource', '$q', '$log', 'ManageContentURIs', attContent.content]);

attContent.content.prototype.retrieveContent = function(criteria, useStubs) {
var useStub = useStubs?useStubs:false;
var deferred = this.q.defer(),
```
