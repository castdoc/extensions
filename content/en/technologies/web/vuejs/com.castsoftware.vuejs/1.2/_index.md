---
title: "Vue.js - 1.2"
linkTitle: "1.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.vuejs

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Vue.js. Vue.js is an open-source
JavaScript framework used to build user interface for web
application: vuejs.org

## In what situation should you install this extension?

If your Web application contains Vue.js source code used inside
JavaScript files and you want to view these object types and their links
with other objects, then you should install this extension.

This extension does note analyse any Vue.js source code within
TypeScript files.

## Supported Vue.js versions

| Version | Support                                                                                    |
|---------|--------------------------------------------------------------------------------------------|
| v2.x    | :white_check_mark: |

## Supported template definitions

Vue.js has different methods to define a component's template - these
methods are shown
here: [https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/](https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/).
The following table shows the methods that are currently supported by
Vue.js extension:

| Pattern                | Support                                                                                       |
|------------------------|-----------------------------------------------------------------------------------------------|
| String                 | :x: |
| Template literal       | :x: |
| X-Templates            | :white_check_mark:    |
| Inline                 | :white_check_mark:    |
| Render functions       | :white_check_mark:    |
| JSX                    | :x: |
| Single page components | :white_check_mark:    |

Unsupported template definition methods will not create links to any
called functions.

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly.  
The Vue.js extension requires that the following other CAST
extensions are also installed:

-   [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)
-   Web services linker service (internal technical extension)

## What results can you expect?

Once the analysis has completed, you can view the results in the normal
manner:

![](../images/503414848.png)

### Objects

The following specific objects are displayed:

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Description</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/503414855.png" draggable="false"
data-image-src="../images/503414855.png"
data-unresolved-comment-count="0" data-linked-resource-id="503414855"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_GetOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="503414845"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Vue.js Get Request Service</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/503414854.png" draggable="false"
data-image-src="../images/503414854.png"
data-unresolved-comment-count="0" data-linked-resource-id="503414854"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_PostOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="503414845"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Vue.js Post Request Service</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/503414853.png" draggable="false"
data-image-src="../images/503414853.png"
data-unresolved-comment-count="0" data-linked-resource-id="503414853"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_PutOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="503414845"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Vue.js Put Request Service</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/503414856.png" draggable="false"
data-image-src="../images/503414856.png"
data-unresolved-comment-count="0" data-linked-resource-id="503414856"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_NodeJS_DeleteOperation32.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="503414845"
data-linked-resource-container-version="2" /></p>
</div></td>
<td class="confluenceTd">Vue.js Delete Request Service</td>
</tr>
</tbody>
</table>

### Specific support

#### Store

The "Vuex.Store()" calls are searched for throughout the application,
and the first parameter is used to resolve different links.

``` java
const store = new Vuex.Store({
  state: {
    pageTitle: 'Home',
    menu: menu,
    user: {},
    token: null,
    message: {
      type: null,
      body: null
    },
    config: config

  },
  mutations: {

    setAuth (state, { user, token }) {
      state.user = user
      state.token = token
      global.helper.ls.set('user', user)
      global.helper.ls.set('token', token)
    },
    setMenu (state, data) {
      state.menu = data
    },
    setPageTitle (state, data) {
      state.pageTitle = data
    },
    showMessage (state, type, body) {
      state.message = { type, body }
    }
  },
  actions: {

    checkAuth ({ commit }) {
      let data = {
        user: global.helper.ls.get('user'),
        token: global.helper.ls.get('token')
      }
      commit('setAuth', data)
    },
    checkPageTitle ({commit, state}, path) {
      for (let k in state.menu) {
        if (state.menu[k].href === path) {
          commit('setPageTitle', state.menu[k].title)
          break
        }
      }
    }
  }
})

export default store
```

the "sync()" calls are also used:

``` java
import store from './store'
import router from './router'
sync(store, router)
```

##### Dispatch

This extension supports "Vuex store dispatch" methods which are used to
call shared functions. Here is an example of a classic architecture
using this method:

![](../images/503414849.png)

In actions.type.js

``` js
export const FETCH_DATA = "fetchData";
```

In data.module.js

``` js
import { FETCH_DATA } from "./actions.type";
import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";


Vue.use(VueAxios, axios);

const actions = {
  [FETCH_DATA]() {
    return return Vue.axios.get('uri/to/data')
  },
};

export default {
  actions
};
```

In index.js

``` js
import Vue from "vue";
import Vuex from "vuex";

import data from "./data.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    data
  }
});
```

  

In StoreExample.vue

``` js
<template>
  <div></div>
</template>

<script>
import { FETCH_DATA } from "@/store/actions.type";

export default {
  name: "StoreExample",
  mounted: function() {
    this.$store.dispatch(FETCH_DATA)
  }
};
</script>
```

This code gives the following results:

![](../images/503414848.png)

#### Webservices

##### Axios

The Vue.js extension supports webservices using Axios:

``` js
async onLogout() {
      await this.$axios.$post('/api/oauth/logout');
      this.setLogout(true);
      this.setCurrentUser(null);

      try {
        await this.$axios.$get('/api/oauth/me')
      } catch (e) {
        // middleware redirects to login page
      }
    }
```

Giving the following result:

![](../images/503414851.png)

##### Vue-resources

The Vue.js extension supports webservices using vue-resources.

``` js
async onLogout() {
      await this.$http.post('/api/oauth/logout');
      this.setLogout(true);
      this.setCurrentUser(null);

      try {
        await this.$http.$get('/api/oauth/me')
      } catch (e) {
        // middleware redirects to login page
      }
    }
```

Giving the following result:

![](../images/503414851.png)

##### Fetch

The Vue.js extension supports webservices using fetch:

``` js
async onLogout() {
      await fetch('/api/oauth/logout');
      this.setLogout(true);
      this.setCurrentUser(null);

      try {
        await fetch('/api/oauth/me')
      } catch (e) {
        // middleware redirects to login page
      }
    }
```

Giving the following result:

![](../images/503414851.png)

### Rules

The following structural rules are provided:

| Release | Link   |
|---------|--------|
| 1.2.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=\|\|1.2.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.2.4-funcrel) |
| 1.2.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=\|\|1.2.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.2.3-funcrel) |
| 1.2.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=\|\|1.2.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.2.2-funcrel) |
| 1.2.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=\|\|1.2.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.2.1-funcrel) |
| 1.2.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=\|\|1.2.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.2.0-funcrel) |

### Known limitations

#### Functions passed as properties

In Vue.js, components can have properties, which could be functions.
Links to those functions are currently unsupported.
