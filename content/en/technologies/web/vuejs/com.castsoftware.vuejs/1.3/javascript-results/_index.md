---
title: "JavaScript support for Vue.js - 1.3"
linkTitle: "JavaScript support"
type: "docs"
weight: 20
no_list: true
---

## Supported versions

| Component | Version | Support |
|---|---|:-:|
| Vue.js | v2.x | :white_check_mark: |

## What results can you expect?

Once the analysis has completed, you can view the results in the normal
manner:

![](../../images/503414848.png)

### Objects

The following specific objects are displayed:

| Icon | Description |
|:---:|---|
| ![](../../images/503414855.png) | Vue.js Get Request Service |
| ![](../../images/503414854.png) | Vue.js Post Request Service |
| ![](../../images/503414853.png) | Vue.js Put Request Service |
| ![](../../images/503414856.png) | Vue.js Delete Request Service |

### Specific support

#### Store

The "Vuex.Store()" calls are searched for throughout the application,
and the first parameter is used to resolve different links.

``` java
const store = new Vuex.Store({
  state: {
    pageTitle: 'Home',
    menu: menu,
    user: {},
    token: null,
    message: {
      type: null,
      body: null
    },
    config: config

  },
  mutations: {

    setAuth (state, { user, token }) {
      state.user = user
      state.token = token
      global.helper.ls.set('user', user)
      global.helper.ls.set('token', token)
    },
    setMenu (state, data) {
      state.menu = data
    },
    setPageTitle (state, data) {
      state.pageTitle = data
    },
    showMessage (state, type, body) {
      state.message = { type, body }
    }
  },
  actions: {

    checkAuth ({ commit }) {
      let data = {
        user: global.helper.ls.get('user'),
        token: global.helper.ls.get('token')
      }
      commit('setAuth', data)
    },
    checkPageTitle ({commit, state}, path) {
      for (let k in state.menu) {
        if (state.menu[k].href === path) {
          commit('setPageTitle', state.menu[k].title)
          break
        }
      }
    }
  }
})

export default store
```

the "sync()" calls are also used:

``` java
import store from './store'
import router from './router'
sync(store, router)
```

##### Dispatch

This extension supports "Vuex store dispatch" methods which are used to
call shared functions. Here is an example of a classic architecture
using this method:

![](../../images/503414849.png)

In actions.type.js

```js
export const FETCH_DATA = "fetchData";
```

In data.module.js

``` js
import { FETCH_DATA } from "./actions.type";
import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";


Vue.use(VueAxios, axios);

const actions = {
  [FETCH_DATA]() {
    return return Vue.axios.get('uri/to/data')
  },
};

export default {
  actions
};
```

In index.js

``` js
import Vue from "vue";
import Vuex from "vuex";

import data from "./data.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    data
  }
});
```

In StoreExample.vue

```js
<template>
  <div></div>
</template>

<script>
import { FETCH_DATA } from "@/store/actions.type";

export default {
  name: "StoreExample",
  mounted: function() {
    this.$store.dispatch(FETCH_DATA)
  }
};
</script>
```

This code gives the following results:

![](../../images/503414848.png)

#### Webservices

##### Axios

The Vue.js extension supports webservices using Axios:

```js
async onLogout() {
      await this.$axios.$post('/api/oauth/logout');
      this.setLogout(true);
      this.setCurrentUser(null);

      try {
        await this.$axios.$get('/api/oauth/me')
      } catch (e) {
        // middleware redirects to login page
      }
    }
```

Giving the following result:

![](../../images/503414851.png)

##### Vue-resources

The Vue.js extension supports webservices using vue-resources.

```js
async onLogout() {
      await this.$http.post('/api/oauth/logout');
      this.setLogout(true);
      this.setCurrentUser(null);

      try {
        await this.$http.$get('/api/oauth/me')
      } catch (e) {
        // middleware redirects to login page
      }
    }
```

Giving the following result:

![](../../images/503414851.png)

##### Fetch

The Vue.js extension supports webservices using fetch:

```js
async onLogout() {
      await fetch('/api/oauth/logout');
      this.setLogout(true);
      this.setCurrentUser(null);

      try {
        await fetch('/api/oauth/me')
      } catch (e) {
        // middleware redirects to login page
      }
    }
```

Giving the following result:

![](../../images/503414851.png)
