---
title: "Vue.js - 1.3"
linkTitle: "1.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.vuejs

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Vue.js. Vue.js is an open-source
JavaScript framework used to build user interfaces for web applications: see https://vuejs.org.

## In what situation should you install this extension?

If your Web application contains Vue.js source code used inside
JavaScript or TypeScript files, and you want to view these object types and their links with other objects, then you should install this extension.

## Support information

See the following pages for more detailed information:

- [JavaScript](javascript-results/)
- [TypeScript](typescript-results/)

### Supported template definitions

Vue.js has different methods to define a component's template - these
methods are shown here: [https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/](https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/). The following table shows the methods that are currently supported by
Vue.js extension:

| Pattern | Support |
|---|:-:|
| String | :x: |
| Template literal | :x: |
| X-Templates | :white_check_mark: |
| Inline | :white_check_mark: |
| Render functions | :white_check_mark: |
| JSX | :x: |
| Single page components | :white_check_mark: |

Unsupported template definition methods will not create links to any called functions.

{{% alert color="info"%}}Unsupported template definition methods will be ignored and the extension will not create links to any called functions.{{% /alert %}}

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Core release | Operating System | Supported |
|---|---|:-:|
| 8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| 8.3.x | Microsoft Windows | :white_check_mark: |

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in order to function correctly. The Vue.js extension requires that the following other CAST extensions are also installed:

- [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)
- [TypeScript](../../../typescript/com.castsoftware.typescript/)
- [Universal Linker](../../../../multi/com.castsoftware.wbslinker/)

## Structural rules
The following structural rules are provided:

- [1.3.0-beta4](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.3.0-beta4)
- [1.3.0-beta3](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.3.0-beta3)
- [1.3.0-beta2](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.3.0-beta2)
- [1.3.0-beta1](https://technologies.castsoftware.com/rules?sec=srs_vuejs&ref=%7C%7C1.3.0-beta1)

## Known limitations

### Functions passed as properties

In Vue.js, components can have properties, which could be functions.
Links to those functions are currently unsupported.
