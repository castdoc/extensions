---
title: "Direct Web Remoting - 1.0"
linkTitle: "1.0"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.dwr

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for Direct Web Remoting - an open
source, component oriented, Java library, that enables Java on the
server and JavaScript in a browser to interact and call each other as
simply as possible. DWR is Easy Ajax for Java.

### In what situation should you install this extension?

The main purpose of this extension is to improve the transaction path between JavaScript (`.js`, `.jsp` or `.html`) files that contain calls to methods declared on the Java server-side. The analysis of Direct Web Remoting therefore also requires that [com.castsoftware.html5](https://extend.castsoftware.com/#/extension?id=com.castsoftware.html5&version=latest) and the [com.castsoftware.jee](https://extend.castsoftware.com/#/extension?id=com.castsoftware.jee&version=latest) are installed.

### Support for User Input Security

Service entry points are created automatically for applications that have a presentation layer based on DWR with `@Path` (and associated
annotations) usage. This can be seen in the analysis log file as follows:

``` text
2018-07-04 20:27:29,227 INFO  SecurityAnalyzer.Processor LoadBlackboxesForApplication cast#spec cast#lib DWRServiceEntryPoints
```

This corresponds to the generation of a file in the following location which will be used by the [com.castsoftware.securityanalyzer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.securityanalyzer&version=latest) extension:

``` text
com.castsoftware.dwr: ≥ 1.0.2-funcrel
<BytecodeFolder>\com.castsoftware.dwr\ServiceEntryPoints.blackbox-v2.xml

com.castsoftware.dwr: all releases
<BytecodeFolder>\com.castsoftware.dwr\ServiceEntryPoints.blackbox.xml
```

- The ServiceEntryPoints.blackbox-v2.xml file will only be exploited by the [com.castsoftware.securityanalyzer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.securityanalyzer&version=latest) extension ≥ 1.0.10-funcrel.
- The ServiceEntryPoints.blackbox.xml file will only be exploited by the [com.castsoftware.securityanalyzer](https://extend.castsoftware.com/#/extension?id=com.castsoftware.securityanalyzer&version=latest) extension ≤ 1.0.9-funcrel, but is always generated and retained for backwards compatibility.

## Function Point, Quality and Sizing support

This extension provides the following support:

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :x: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

The extension will not be automatically downloaded and installed. If you need to use it, you should manually install the
extension.

## What results can you expect?

### Objects

| Icon | Description |
|---|---|
| ![](../images/622919886.png) | Call to a DWR service method |
| ![](../images/622919885.png) | A DWR service method |

### Support of interaction between frontend and backend via dwr.xml binding

Take the following example where a .JSP file contains the function
deleteIndex and the functionCall CMSMaintenanceAjax.deleteIndex:

view_cms_maintenance.jsp

```js
<script type="text/javascript" src="/dwr/engine.js"></script>
<script type="text/javascript" src="/dwr/util.js"></script>
<script type='text/javascript' src='/dwr/interface/CMSMaintenanceAjax.js'></script>
<script type="text/javascript" src="/dwr/interface/ThreadMonitorTool.js"></script>
<script type="text/javascript" src="/dwr/interface/UserSessionAjax.js"></script>

function deleteIndex(indexName, live){

    if(live && ! confirm("<%= UtilMethods.escapeDoubleQuotes(LanguageUtil.get(pageContext, "Delete-Live-Index")) %>")){
        return;
    }
    CMSMaintenanceAjax.deleteIndex(indexName,deleteIndexCallback);

}
```

with the associated dwr.XML file (only a snippet of it):

dwr.xml

```xml
   <create creator="new" javascript="CMSMaintenanceAjax" scope="request">
        <param name="class" value="com.dotmarketing.portlets.cmsmaintenance.ajax.CMSMaintenanceAjax"/>
    </create>
```

This allows the creation of a new object DWR CallTo Method as well
as the callLink on the client side between the JavaScript functionCall
and the DWR CallTo Method. In addition, below is a Java file
containing the
class com.dotmarketing.portlets.cmsmaintenance.ajax.CMSMaintenanceAjax and
the method deleteIndex:

com.dotmarketing.portlets.cmsmaintenance.ajax.CMSMaintenanceAjax Expand
source

``` java
    public boolean deleteIndex(String indexName){
        validateUser();
        return  APILocator.getContentletIndexAPI().delete(indexName);
    }
```

This allows the creation of the new object DWR Method as well as the
callLink on the server side between the DWR Method object and the
Java method declaration. Finally, the link between the DWR CallTo
Method and DWR Method is created via the extension
[com.castsoftware.wbslinker](../../../../multi/com.castsoftware.wbslinker/) using name
matching.

Here are some example results:

![](../images/627146793.png)

For this result,  the following Objects, Links and Properties are added:

Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: center;">Icon</th>
<th class="confluenceTh" style="text-align: center;">Type of Object</th>
<th class="confluenceTh" style="text-align: center;">Name of Object</th>
<th class="confluenceTh" style="text-align: center;">Primary
Bookmark</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/622919886.png" draggable="false"
data-image-src="../images/622919886.png"
data-unresolved-comment-count="0" data-linked-resource-id="622919886"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_HTML5_DWR_CallTo_Method.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="622919881"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">DWR CallTo
Method</td>
<td class="confluenceTd"
style="text-align: center;">CMSMaintenanceAjax.deleteIndex</td>
<td class="confluenceTd" style="text-align: center;">on function
call </td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/622919885.png" draggable="false"
data-image-src="../images/622919885.png"
data-unresolved-comment-count="0" data-linked-resource-id="622919885"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JEE_DWR_Method.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="622919881"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">DWR Method</td>
<td class="confluenceTd"
style="text-align: center;">CMSMaintenanceAjax.deleteIndex</td>
<td class="confluenceTd" style="text-align: center;"><p>On create block
in dwr.xml file</p>
<p>if method is "include" then bookmark on include block
instead </p></td>
</tr>
</tbody>
</table>

Links

|   Extension responsible    |          Caller           |          Caller name           | Type of Link |       Callee       |          Callee name           |
|:--------------------------:|:-------------------------:|:------------------------------:|:------------:|:------------------:|:------------------------------:|
|    com.castsoftware.dwr    | HTML5 JavaScript function |          deleteIndex           |   callLink   | DWR CallTo Method  | CMSMaintenanceAjax.deleteIndex |
|    com.castsoftware.dwr    |        DWR Method         | CMSMaintenanceAjax.deleteIndex |   callLink   |    JAVA Method     |          deleteIndex           |
| com.castsoftware.wbslinker |     DWR CallTo Method     | CMSMaintenanceAjax.deleteIndex |   callLink   |     DWR Method     | CMSMaintenanceAjax.deleteIndex |

#### Properties

None

### Support of interaction between frontend and backend via complex xml binding

Take the following example where the index.html file contains the
function getDataFromServer and the functionCall
dwrService.getAddress:

index.html

``` xml
<html>
    <head>
        <title>DWR Dev</title>
        <script type="text/javascript" src="/dwr3SampleAppSpringMVC/dwr/engine.js"></script>
        <script type="text/javascript" src="/dwr3SampleAppSpringMVC/dwr/util.js"></script>
        <script type="text/javascript" src="/dwr3SampleAppSpringMVC/dwr/interface/dwrService.js"></script>
        <script>
            function getDataFromServer() {
              dwrService.getAddress({
                callback: getDataFromServerCallBack
              });
            }
            
            function getDataFromServerCallBack(dataFromServer) {
              alert(dwr.util.toDescriptiveString(dataFromServer, 3));
            }
        </script>
    </head>
    <body>
        <h3>DWR/Spring and Spring MVC</h3>
        <a href="#" onclick="getDataFromServer(); return false;">Retrieve test data</a><br/>
    </body>
</html>
```

The following xml file which is mentioned within the web.xml: 

web.xml

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:dwr="http://www.directwebremoting.org/schema/spring-dwr"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
       http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-3.0.xsd
       http://www.directwebremoting.org/schema/spring-dwr http://www.directwebremoting.org/schema/spring-dwr-3.0.xsd">

  <dwr:controller id="dwrController" debug="true" />

  <bean class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping">
    <property name="alwaysUseFullPath" value="true"/>
    <property name="mappings">
    <props>
      <prop key="/dwr//*">dwrController</prop>
    </props>
    </property>
  </bean>
  
  <dwr:configuration>
    <dwr:convert type="bean" class="org.uk.ltd.dwr.dev.model.Address" />
  </dwr:configuration>

  <bean id="dwrService" class="org.uk.ltd.dwr.dev.service.DWRService">
    <dwr:remote javascript="dwrService">
      <dwr:include method="getAddress" />   
    </dwr:remote>
  </bean>  
</beans>
```

and the java file containing the dwrService.getAddress method
declaration:

java file

``` java
package org.uk.ltd.dwr.dev.service;

import org.uk.ltd.dwr.dev.model.Address;

public class DWRService {
    
    public DWRService() { }
    
    public Address getAddress() throws Exception {
        Address address = new Address();
        address.setStreet("2245 NW Overlook Drive");
        address.setCity("Portland");
        address.setState("Oregon");
        return address;
    }
}
```

Here are some example results:

![](../images/627146791.png)

For this result, the following Objects, Links and Properties are added:

#### Objects

<table class="wrapped confluenceTable">
<tbody>
<tr class="header">
<th class="confluenceTh">Icon</th>
<th class="confluenceTh">Type of Object</th>
<th class="confluenceTh">Name of Object</th>
<th class="confluenceTh">Primary Bookmark</th>
<th class="confluenceTh">Parent</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/622919886.png" draggable="false"
data-image-src="../images/622919886.png"
data-unresolved-comment-count="0" data-linked-resource-id="622919886"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_HTML5_DWR_CallTo_Method.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="622919881"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">DWR CallTo Method</td>
<td class="confluenceTd">dwrService.getAddress</td>
<td class="confluenceTd">on function call </td>
<td class="confluenceTd">Function containing the function call</td>
</tr>
<tr class="even">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/622919886.png" draggable="false"
data-image-src="../images/622919886.png"
data-unresolved-comment-count="0" data-linked-resource-id="622919886"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_HTML5_DWR_CallTo_Method.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="622919881"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">DWR CallTo Method</td>
<td class="confluenceTd">dwrService.getAddress</td>
<td class="confluenceTd">on function call </td>
<td class="confluenceTd">Function containing the function call</td>
</tr>
<tr class="odd">
<td class="confluenceTd"><div class="content-wrapper">
<p><img src="../images/622919885.png" draggable="false"
data-image-src="../images/622919885.png"
data-unresolved-comment-count="0" data-linked-resource-id="622919885"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JEE_DWR_Method.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="622919881"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd">DWR Method</td>
<td class="confluenceTd">dwrService.getAddress</td>
<td class="confluenceTd"><p>On create bean block in secondary xml
file.</p>
<p>if method is "include" then bookmark on include block
instead </p></td>
<td class="confluenceTd">the secondary xml file</td>
</tr>
</tbody>
</table>

Links

<table class="wrapped confluenceTable">
<thead>
<tr class="header">
<th class="confluenceTh" style="text-align: center;">Extension
responsible </th>
<th class="confluenceTh" style="text-align: center;"><p>Caller</p></th>
<th class="confluenceTh" style="text-align: center;"><p>Caller
name</p></th>
<th class="confluenceTh" style="text-align: center;"><p>Type of
Link</p></th>
<th class="confluenceTh" style="text-align: center;"><p>Callee</p></th>
<th class="confluenceTh" style="text-align: center;"><p>Callee
name</p></th>
<th class="confluenceTh">Remarks</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td class="confluenceTd"
style="text-align: center;">com.castsoftware.dwr</td>
<td class="confluenceTd" style="text-align: center;"><p>HTML5 JavaScript
function</p></td>
<td class="confluenceTd"
style="text-align: center;">getDataFromServer</td>
<td class="confluenceTd" style="text-align: center;">callLink</td>
<td class="confluenceTd" style="text-align: center;"><p>DWR CallTo
Method </p></td>
<td class="confluenceTd"
style="text-align: center;">dwrService.getAddress</td>
<td class="confluenceTd">Based on parent</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: center;">com.castsoftware.dwr</td>
<td class="confluenceTd" style="text-align: center;"><p>HTML5 JavaScript
function</p></td>
<td class="confluenceTd"
style="text-align: center;">getDataFromServer</td>
<td class="confluenceTd" style="text-align: center;">callLink</td>
<td class="confluenceTd" style="text-align: center;"><p>DWR CallTo
Method </p></td>
<td class="confluenceTd"
style="text-align: center;">dwrService.getAddress</td>
<td class="confluenceTd">Based on parent</td>
</tr>
<tr class="odd">
<td class="confluenceTd"
style="text-align: center;">com.castsoftware.dwr</td>
<td class="confluenceTd" style="text-align: center;">DWR Method</td>
<td class="confluenceTd"
style="text-align: center;">dwrService.getAddress</td>
<td class="confluenceTd" style="text-align: center;">callLink</td>
<td class="confluenceTd" style="text-align: center;">JAVA Method</td>
<td class="confluenceTd" style="text-align: center;">getAddress</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd"
style="text-align: center;">com.castsoftware.wbslinker</td>
<td class="confluenceTd" style="text-align: center;">DWR CallTo
Method</td>
<td class="confluenceTd"
style="text-align: center;">dwrService.getAddress</td>
<td class="confluenceTd" style="text-align: center;">callLink</td>
<td class="confluenceTd" style="text-align: center;">DWR Method</td>
<td class="confluenceTd"
style="text-align: center;">dwrService.getAddress</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>

#### Properties

None

### Support of interaction between frontend and backend via Annotations

Take the following Annotations on the backend java file named
: ArithmeticService.java.

ArithmeticService.java

``` java
package org.krams.tutorial.service;

import org.apache.log4j.Logger;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/
 * @Service enables the class to be used as a Spring service
 * @RemoteProxy enables the class to be used as a DWR service
 * @Transactional enables transaction support for this clas
 */
@Service("springService")
@RemoteProxy(name = "dwrService")
@Transactional
public class ArithmeticService {

    protected static Logger logger = Logger.getLogger("service");

    /
     * @RemoteMethod exposes this method to DWR.
     *               Your JSP pages can access this method as Javascript
     *               Your Spring beans can still access this method.
     */
    @RemoteMethod
    public Integer add(Integer operand1, Integer operand2) {
        logger.debug("Adding two numbers");
        // A simple arithmetic addition
        return operand1 + operand2;
    }

    public Integer add2(Integer operand1, Integer operand2) {
        logger.debug("Adding two numbers");
        // A simple arithmetic addition
        return operand1 + operand2;
    }
}
```

And the first associated file named ajax-add-page.jsp:

ajax-add-page.jsp

``` js
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <script type='text/javascript' src="/spring-mvc-dwr/krams/dwr/engine.js"></script>
    <script type='text/javascript' src="/spring-mvc-dwr/krams/dwr/util.js"></script>
    <script type="text/javascript" src="/spring-mvc-dwr/krams/dwr/interface/dwrService.js"></script>
    
    <title>Spring MVC - DWR Integration Tutorial</title>
</head>
<body>

<h3>Spring MVC - DWR Integration Tutorial</h3>
<h4>AJAX version</h4>

Demo 1
<div style="border: 1px solid #ccc; width: 250px;">
    Add Two Numbers: <br/>
    <input id="inputNumber1" type="text" size="5"> +
    <input id="inputNumber2" type="text" size="5">
    <input type="submit" value="Add" onclick="add()" /> <br/>
    Sum: <span id="sum">(Result will be shown here)</span>
</div>

<script type="text/javascript">
    // Retrieves the matching value
    // Delegates to the dwrService
    function add() {
        // Retrieve value of text inputs
        var operand1 = dwr.util.getValue("inputNumber1");
        var operand2 = dwr.util.getValue("inputNumber2");
        
        // Pass two numbers, a callback function, and error function
        dwrService.add(operand1, operand2, {
            callback : handleAddSuccess,
            errorHandler : handleAddError
        });
    }

    // data contains the returned value
    function handleAddSuccess(data) {
        // Assigns data to result id
        dwr.util.setValue("sum", data);
    }

    function handleAddError() {
        // Show a popup message
        alert("We can't add those values!");
    }
</script>

</body>
</html>
```

Here are some example results:

![](../images/627146792.png)

For this result,  the following Objects, Links and Properties are added:

#### Objects

<table class="wrapped relative-table confluenceTable"
style="width: 51.7661%;">
<colgroup>
<col style="width: 14%" />
<col style="width: 13%" />
<col style="width: 14%" />
<col style="width: 17%" />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh" style="text-align: center;">Icon</th>
<th class="confluenceTh" style="text-align: center;">Type of Object</th>
<th class="confluenceTh" style="text-align: center;">Name of Object</th>
<th class="confluenceTh" style="text-align: center;">Bookmark</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/622919886.png" draggable="false"
data-image-src="../images/622919886.png"
data-unresolved-comment-count="0" data-linked-resource-id="622919886"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_HTML5_DWR_CallTo_Method.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="622919881"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">DWR CallTo
Method</td>
<td class="confluenceTd" style="text-align: center;">dwrService.add</td>
<td class="confluenceTd" style="text-align: center;">On function
call </td>
</tr>
<tr class="even">
<td class="confluenceTd" style="text-align: center;"><div
class="content-wrapper">
<p><img src="../images/622919885.png" draggable="false"
data-image-src="../images/622919885.png"
data-unresolved-comment-count="0" data-linked-resource-id="622919885"
data-linked-resource-version="1" data-linked-resource-type="attachment"
data-linked-resource-default-alias="CAST_JEE_DWR_Method.png"
data-base-url="https://doc.castsoftware.com"
data-linked-resource-content-type="image/png"
data-linked-resource-container-id="622919881"
data-linked-resource-container-version="1" width="32" /></p>
</div></td>
<td class="confluenceTd" style="text-align: center;">DWR Method</td>
<td class="confluenceTd" style="text-align: center;">dwrService.add</td>
<td class="confluenceTd" style="text-align: center;"><p>On Annotated
Method </p></td>
</tr>
</tbody>
</table>

#### Links

|   Extension responsible    |          Caller           |  Caller name   | Type of Link |       Callee       |  Callee name   |
|:--------------------------:|:-------------------------:|:--------------:|:------------:|:------------------:|:--------------:|
|    com.castsoftware.dwr    | HTML5 JavaScript function |      add       |   callLink   | DWR CallTo Method  | dwrService.add |
|    com.castsoftware.dwr    |        DWR Method         | dwrService.add |   callLink   |     JAVA class     |      add       |
| com.castsoftware.wbslinker |     DWR CallTo Method     | dwrService.add |   callLink   |     DWR Method     | dwrService.add |

#### Properties

None

## Notes
Within DWR framework, when using Annotations:

-   For a method to be exposed the RemoteMethod and the RemoteProxy
    (on the class) must be present. 
-   For finding dwr function call in \*.js file in absence of xml
    configuration (i.e. from Annotation),  the javascript alias is
    looked upon nearby header script in other \*.jsp or \*.html.  

General Case on Frontend within DWR framework:

-   When two identical function call  within the same function occurs
    only one DWR CallTo Method object is created. However, there is
    multiple bookmark for each function call within the method.
-   DWR CallTo Method can be found in \*.js, \*.jsp and \*.html files.
