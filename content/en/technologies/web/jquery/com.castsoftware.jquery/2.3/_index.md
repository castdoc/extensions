---
title: "jQuery - 2.3"
linkTitle: "2.3"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.jquery

## What's new?

See [Release Notes](rn/).

## Description

This extension provides support for jQuery.

### In what situation should you install this extension?

If your Web application contains jQuery source code and you want to
view these object types and their links with other objects, then you
should install this extension:

-   Creates jQuery events and access points to services:  
    -   jQuery events: they represent a query in DOM tree
    -   jQuery access points to web
        services: CAST_JQuery_GetResourceService, CAST_JQuery_PutResourceService, CAST_JQuery_PostResourceService
-   Creates links (direct or indirect) from jQuery events to jQuery
    access points to services (these links are created by the jQuery
    extension and the HTML5/JavaScript extension).
-   Links from the jQuery access points to services to different servers
    written in other languages are then created by the WBSlinker
    extension (an internal extension).
-   The following declarations will create a jQuery event:

``` java
$('#advancedLoginOptionsLink').bind('click', dm.login.toggle_advanced);
$('#advancedLoginOptionsLink').click(dm.login.toggle_advanced);
$('#advancedLoginOptionsLink').on('click', this.destroy.bind(this));....
```

## Transaction configuration

Default entry point is the HTML file content.

## Supported jQuery versions

| Versions  | Supported? |
|---|:-:|
| 1.0 - 3.x | :white_check_mark: |

## Function Point, Quality and Sizing support

- Function Points (transactions): a green tick indicates that OMG Function Point counting and Transaction Risk Index are supported
- Quality and Sizing: a green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist

| Function Points  (transactions) | Quality and Sizing |
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

### Comparison with existing support for JavaScript

CAST AIP has provided support for analyzing JavaScript via its JEE and
.NET analyzers (provided out of box in CAST AIP) for some time now.
The HTML5/JavaScript extension (on which the jQuery extension
depends) also provides support for JavaScript but with a focus on web
applications. CAST highly recommends that you use this extension if your
Application contains JavaScript and more specifically if you want to
analyze a web application, however you should take note of the
following:

-   You should ensure that you configure the extension to NOT analyze
    the back end web client part of a .NET or JEE application.
-   You should ensure that you configure the extension to ONLY analyze
    the front end web application built with the HTML5/JavaScript that
    communicates with the back end web client part of a .NET or JEE
    application.
-   If the back end web client part of a .NET or JEE application is
    analyzed with the jQuery extension and with the native .NET/JEE
    analyzers, then your results will reflect this - there will be
    duplicate objects and links (i.e. from the analyzer and from the
    extension) therefore impacting results and creating erroneous
    Function Point data.

>Note that in CAST AIP ≥ 8.3.x support for analyzing JavaScript has been
withdrawn from the JEE and .NET analyzers.

## Dependencies with other extensions

Some CAST extensions require the presence of other CAST extensions in
order to function correctly. The jQuery extension requires that the
following other CAST extensions are also installed (this will be handled automatically):

-   [HTML5/JavaScript](../../../html5-js/com.castsoftware.html5/)
-   [Web Services Linker](../../../../multi/com.castsoftware.wbslinker/)

## Download and installation instructions

The extension will be automatically downloaded and installed.

## What results can you expect?

Once the analysis/snapshot generation has completed, you can view the results in the normal manner:

![](../images/418185785.png)

### Web Service calls

The following libraries are supported for Web Service HTTP calls:

-   $.ajax
-   $.get
-   $.post
-   jQuery.sap.syncGet
-   jQuery.sap.syncGetJSON
-   jQuery.sap.syncGetText
-   jQuery.sap.syncPost
-   Ajax.Request
-   Ext.Ajax.request
-   CQ.Ext.Ajax.request

Once the JQuery extension analysis is finished, the analyzer will give
the output, i.e., the final number of web service calls created.

These web services are resolved to web service operations on the server
side for the following supported frameworks: [Node.js](Node.js),
[JAX-RS](JAX-RS), [Spring MVC](Spring_MVC):

#### $.ajax

``` js
$.ajax({
    type: 'POST',
    url: val ? '_auth/login' : '_auth/logout',
    data: {assertion: assertion},
    success: function(res, status, xhr) { window.location.reload(); },
    error: function(xhr, status, err) {
        box.remove();
        navigator.id.logout();
        alert('Login failure: ' + err);
    }
});
```

#### $.get

``` js
$.get("ajax/test.html", function( data ) 
    { 
        $( ".result" ).html( data ); 
        alert( "Load was performed." ); 
    }
);
```

#### $.post

``` js
$.post( "ajax/test.html", function( data ) { $( ".result" ).html( data ); });
```

#### jQuery.sap.syncGet

``` js
jQuery.sap.syncGet("syncGet_url", { dataType: "json" });
```

#### jQuery.sap.syncGetJSON

``` js
jQuery.sap.syncGetJSON("syncGetJSON_url", { dataType: "json" });
```

#### jQuery.sap.syncGetText

``` js
jQuery.sap.syncGetText("syncGetText_url", { dataType: "json" });
```

#### jQuery.sap.syncPost

``` js
jQuery.sap.syncPost("syncPost_url", { dataType: "json" });
```

#### Ajax.request

``` js
new Ajax.Request('/your/url', {
  onSuccess: function(response) {
    // Handle the response content...
  }
});
```

#### Ext.Ajax.request

``` js
Ext.Ajax.request({
            url: '/api/students',
            method: 'GET',
            timeout: 60000,
            params:
            {
                id: 1 // loads student whose Id is 1
            },
            headers:
            {
                'Content-Type': 'application/json'
            },
            success: function (response) {
            
            },
            failure: function (response) {
                Ext.Msg.alert('Status', 'Request Failed.');

            }
        });
```

#### CQ.Ext.Ajax.request

``` js
CQ.Ext.Ajax.request({
   url: 'foo.php',
   success: someFn,
   failure: otherFn,
   headers: {
       'my-header': 'foo'
   },
   params: { foo: 'bar' }
});
```

### Objects

| Icon | Description |
|---|---|
| ![](../images/418185782.png) | jQuery Selector |
| ![](../images/418185784.png) | jQuery $.ajax, $.get, $.getJSON, $.getScript<br><br>jQuery SAP sjax, syncGet, syncGetJSON, syncGetText<br><br>Get Resource Service |
| ![](../images/418185772.png) | jQuery $.ajax, $.get, $.getJSON, $.getScript<br><br> jQuery SAP syncPost Post Resource Service |
| ![](../images/418185779.png) | jQuery $.ajax, $.get, $.getJSON, $.getScript Put Resource Service |
| ![](../images/418185781.png) | jQuery $.ajax, $.get, $.getJSON, $.getScript Delete Resource Service |

### Structural Rules

The following structural rules are provided:

|  Release | Link  |
|----------|-------|
| 2.3.9-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.9-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.9-funcrel) |
| 2.3.8-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.8-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.8-funcrel) |
| 2.3.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.7-funcrel) |
| 2.3.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.6-funcrel) |
| 2.3.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.5-funcrel) |
| 2.3.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.4-funcrel) |
| 2.3.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.3-funcrel) |
| 2.3.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.2-funcrel) |
| 2.3.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.1-funcrel) |
| 2.3.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_jquery&ref=\|\|2.3.0-funcrel) |
