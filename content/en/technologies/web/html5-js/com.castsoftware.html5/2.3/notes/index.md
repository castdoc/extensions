---
title: "Technical notes"
linkTitle: "Technical notes"
type: "docs"
---

## (Classic) ASP support

See [ASP - Technical notes and limitations](../../../../../microsoft/asp/notes/).

## ASP.NET MVC Razor support

See [.NET - ASP.NET MVC Razor support](../../../../../dotnet/aspnetrazor/).

## JavaScript support

### JavaScript (1 to 1.8.1)

-   JavaScript call(), function(), bind(), prototype and prototype
    inheritance are supported
-   Creates Functions, Classes and Constructors
-   Local call links between function calls and functions inside each
    JavaScript file are created

### JavaScript ECMA 6

Supported Object Oriented statements are as follows:

-   arrows [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)
-   classes
    [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)
-   const
    [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const)
-   let
    [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let)
-   try...catch
    [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch)
-   subclassable built-ins

No explicit support:

-   symbols
    [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol)
-   promises
    [https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise#Advanced_Example](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise#Advanced_Example)
-   reflect api
    [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect)
-   proxies
    [https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy)

## Support for Web Service calls in JavaScript code

The following libraries are supported for Web Service HTTP calls:

-   XMLHttpRequest
-   window.open
-   document.submit
-   location.href
-   Ext.Updater (Sencha)
-   EventSource
-   fetch
-   axios
-   superagent
-   aws-amplify
-   falcor
-   WLResourceRequest
-   WebSocket

Once the HTML5 extension analysis is finished, the analyzer will output
the final number of web service calls created. When urls cannot be
correctly evaluated, a HTTP call named "{}" will be created, except for:

-   window.open
-   window.location.href, 
-   document.location.href

These exceptions are due to very large number of statements, and they
are not explicit calls as "ajax", "get", "post", "delete"... functions.

### XMLHttpRequest

``` js
var xhttp = new XMLHttpRequest();
xhttp.open("GET", "ajax_info.txt", false);
xhttp.send();
```

### window.open

``` js
url = "EditStandaloneWeb.do?url_name_id="+urlNameId;
popUpWindow = window.open(url,'popUpWindow','height=900,width=900,left=300,top=50,resizable=no,directories=no,status=no');
```

### document.submit

``` js
addUpload.document.forms[1].action = "ciedit";
addUpload.document.forms[1].submit();
```

``` js
$("form[id='configurarClassificacaoProdutoForm']").action = '<s:url action="configurarClassificacaoProduto" method="excluir"/>';
$("form[id='configurarClassificacaoProdutoForm']").submit();
```

``` js
$form.prop('method', 'post');
$form.prop('action', '<c:out value="/toto" />');
$form.submit();
```

### location.href

``` js
url = "//localization.att.com/loc/controller";
window.location.href = url;
```

``` js
url = "//localization.att.com/loc/controller";
document.location.href=url;
```

### Ext.Updater (Sencha)

``` js
Ext.get(panelId).getUpdater().update({
    url:'actionUrl',
    params:params,
    method:'POST',
    failure: function(inResp) {}
});
```

### EventSource

``` js
const eventsUrl = '/api/dashboard/events?token=' + localStorage.getItem('dashboard_token');
const serverEvents = new EventSource(eventsUrl);
```

### fetch

``` js
fetch('/users', {
    method: 'POST',
    body: new FormData(form)
});
```

### got

``` js
import got from "got"
const {data} = await got.post('https://httpbin.org/anything', {
}).json();
```
``` js
import got from "got"
got.get(url, options?)
got.post(url, options?)
got.put(url, options?)
got.patch(url, options?)
got.head(url, options?)
got.delete(url, options?)
```
``` js
import got from "got"
const resp = await got('http://${gitserverUrl}/exec', {
                body: JSON.stringify({ repo: repository, args }),
            })
```

### unirest

``` js
import unirest from "unirest"
unirest.get(url)
unirest.post(url)
unirest.put(url)
unirest.patch(url)
unirest.head(url)
unirest.delete(url)
```

### wretch

``` js
import wretch from "wretch"
wretch("examples/example")
  .url("/ex1/1")
  .get("/ex1/1");
```
``` js
import wretch from "wretch"
const api = wretch("examples/example").url("/ex1/1");
api.get("/ex1/1");
```
``` js
import wretch from "wretch"
wretch("examples/example")
  .url("/ex1/1")
  .post();
```
``` js
import wretch from "wretch"
wretch("examples/example")
  .url("/ex1/1")
  .patch();
```
``` js
import wretch from "wretch"
wretch("examples/example")
  .url("/ex1/1")
  .put();
```
``` js
import wretch from "wretch"
wretch("examples/example")
  .url("/ex1/1")
  .delete();
```

### axios

``` js
axios.get('/user', {
    params: {
        ID: 12345
    }
})
.then(function (response) {
    console.log(response);
})
.catch(function (error) {
    console.log(error);
});
```

``` js
var http = require('axios');
http.get('/user', {
    params: {
    ID: 12345
    }
})
.then(function (response) {
    console.log(response);
})
.catch(function (error) {
    console.log(error);
});
```

``` js
axios.request({
    url : '/user',
    method : 'delete'
});
```

``` js
axios('/user/12345');
```

``` js
axios({
    method: 'post',
    url: '/user/12345',
    data: {
        firstName: 'Fred',
        lastName: 'Flintstone'
    }
});
```

``` js
var instance = axios.create({
    baseURL: 'https://api.example.com'
}); 
instance.get('/longRequest', {
    timeout: 5000
});
```

### superagent

``` js
request
    .post('/api/pet')
    .send({ name: 'Manny', species: 'cat' })
    .set('X-API-Key', 'foobar')
    .set('Accept', 'application/json')
    .end(function(err, res){
        // Calling the end function will send the request 
    });
```

``` js
var myrequest = require('superagent');
myrequest
    .get('/some-url')
    .use(prefix) // Prefixes *only* this request 
    .use(nocache) // Prevents caching of *only* this request 
    .end(function(err, res){
        // Do something 
    });
```

``` js
request
    .head('/users')
    .query({ email: 'joe@smith.com' })
    .end(function(err, res){

    });
```

``` js
request('GET', '/users')
    .end(function(err, res){
    });
```

``` js
request('/search', (err, res) => {
});
```

### aws-amplify

``` js
import {API} from 'aws-amplify';
API.get('ReactSample','/items/orders/' + sessionStorage.getItem('latestOrder')).then(response => {});
```

### falcor

``` js
var falcor = require('falcor');
var model = new falcor.Model({source: new falcor.HttpDataSource('/model.json')});
model
    .get["events", "byName", ["Midwest JS"], ['description']])
    .then(function(response) {
            document.getElementById('event-data').innerHTML = JSON.stringify(response, null, 2);
        }, 
        function(err) {
            console.log(err);
        });
```

### WLResourceRequest

``` js
var request = new WLResourceRequest('/adapters/sampleAdapter/multiplyNumbers', WLResourceRequest.GET);
request.setQueryParameter('params', [5, 6]);
request.send().then(
        function(response) {
            // success flow, the result can be found in response.responseJSON
        },
        function(error) { 
            // failure flow 
            // the error code and description can be found in error.errorCode and error.errorMsg fields respectively 
        }
);
```

``` js
  adapterPath = new URI("/adapters/sampleAdapter/multiplyNumbers");
  var request = new WLResourceRequest(adapterPath, WLResourceRequest.GET);
  request.setQueryParameter('params', [5, 6]);
  request.send().then(function(response) {}, function(error) {});
```

``` js
  var request = new WLResourceRequest(URI("/adapters/sampleAdapter/multiplyNumbers"), WLResourceRequest.GET);
  request.setQueryParameter('params', [5, 6]);
  request.send().then(function(response) {}, function(error) {});
```

### dojo

``` js
      var xhrArgs = {
        url: "dojo/LICENSE_NOT_THERE",
        handleAs: "text",
        preventCache: true,
        handle: function(error, ioargs){
        }
      }
      var deferred = dojo.xhr("POST", xhrArgs);
```

``` js
  var xhrArgs = {
    url: "dojo/LICENSE_NOT_THERE",
    handleAs: "text",
    preventCache: true,
    handle: function(error, ioargs){
    }
  }
  var deferred = dojo.xhrGet(xhrArgs); // or dojo.xhrPost, dojo.xhrPut, dojo.xhrDelete
```

``` js
  require["dojo/request"], function(request){
    request("my_url").then(
        function(text){
        },
        function(error){
        }
    );
});
```

``` js
require["dojo/request/node"], function(nodeRequest){
    nodeRequest("my_url").then(
        function(text){
            console.log("The file's content is: " + text);
        },
        function(error){
            console.log("An error occurred: " + error);
        }
    );
});
```

``` js
require["dojo/request"], function(request){
    request.get("my_url").then(
        function(text){
            console.log("The file's content is: " + text);
        },
        function(error){
            console.log("An error occurred: " + error);
        }
    );
}); // or request.post, request.put, request.del
```

### WebSocket

``` js
var socket = new WebSocket('ws://game.example.com:12010/updates2');
```

## Support for Web Service calls in html/jsp code

The following libraries are supported for Web Service HTTP calls:

-   href
-   iframe.src
-   form.action
-   input.formaction
-   datamap
-   jsp.forward
-   jsp.pager
-   struts-html
-   struts tags-html
-   struts-tags
-   struts-nested
-   struts jqgrid
-   euamtags
-   spring

Once the HTML5 extension analysis is finished, the analyzer will output
the final number of web service calls created.

### href

``` xml
<a href="@Url.Action("Edit", "MissionMandatoryDocuments", new
    {
        type = item.Key,
        nodeId = Model.NodeId
    })">
</a>
```

### iframe.src

``` xml
<iframe src="/greco/ValidationADP.do?hidden=doPerform" height="900" width="1000"></iframe>
```

### form.action

``` xml
<form action="@Url.Action("Export", "InterimBillsEvaluation")" method="post">
</form>
```

``` xml
<form action="<c:out value="${epayCustomer.absoluteBaseWithContextPath}"/>/dlife/payBill.perform" method="GET">
</form>
```

### input.formaction

``` xml
<input type="submit" value="Save" name="Edit" formaction="@Url.Action("edit")" class="btn btn-default" />
```

### datamap

``` xml
<div id="map" data-request-url="@Url.Action("GetStops", "Home")" data-request-url2="@Url.Action("Driving", "Home")">
<script>
    function getStops() {
        var url = $('#map').data('request-url');
        window.open(url) 
    }
</script>
```

### jsp.forward

``` xml
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<jsp:forward page="/dispatcher/mainMenu"/>
```

### jsp.pager

``` xml
<%@ taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager"%>
<html>
    <pg:pager url="RolePaginationAction.do" items="<%= total_records %>" index="<%= index %>" maxPageItems="<%= maxPageItems %>" maxIndexPages="<%= maxIndexPages %>" isOffset="<%= true %>" export="offset,currentPageNumber=pageNumber" scope="request">
    </pg:pager>
</html>
```

### struts-html

``` xml
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<html:html xhtml="true">
    <div align="center">
        <html:form action="/submitCustomerForm" method="GET" />
    </div>
</html:html>
```

``` xml
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<html:html xhtml="true">
    <div align="center">
        <html:link page="/submitCustomerForm" />
    </div>
</html:html>
```

### struts tags-html

``` xml
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<html:html xhtml="true">
    <div align="center">
        <html:link action="bookEdit.do?do=borrowBook" paramName="book" paramProperty="id" paramId="id">Borrow book</html:link>
    </div>
</html:html>
```

### struts-tags

``` xml
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
    <s:url action="banque" method="consulter" var="urlAcceuil"/>
    <s:a href="%{urlAcceuil}">Retour à l'acceuil</s:a>
</html>
```

``` xml
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
    <body>
        <s:a action="compte" />
    </body>
</html>
```

``` xml
<%@taglib prefix="s" uri="/struts-tags"%>
<html>
    <body>
        <s:form action="compte">
            <s:hidden name="contract_nbr" id="contract_nbr" value="%{mod.contract_nbr}"/>
        </s:form>
    </body>
</html>
```

### struts-nested

``` xml
<%@ taglib uri="/tags/struts-nested" prefix="nested" %>
<html:html xhtml="true">
    <div align="center">
        <nested:form action="modifyAccount.do" />
    </div>
</html:html>
```

### struts jqgrid

``` xml
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<html>
    <sjg:grid id="sjgrid" dataType="json" href="/url" caption="Grid Model" gridModel="gridModel" pager="true" navigator="true">
    </sjg:grid>
</html>
```

``` xml
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<html>
    <sjg:grid id="sjgrid" dataType="json" editurl="/url" caption="Grid Model" gridModel="gridModel" pager="true" navigator="true">
    </sjg:grid>
</html>
```

``` xml
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<html>
    <sjg:grid id="sjgrid" dataType="json" cellurl="/url" caption="Grid Model" gridModel="gridModel" pager="true" navigator="true">
    </sjg:grid>
</html>
```

``` xml
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<html>
    <sjg:gridColumn id="sjgrid" surl="/url">
    </sjg:gridColumn>
</html>
```

``` xml
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<html>
    <sjg:gridColumn id="sjgrid" editoptions="{dataUrl: '/url'}">
    </sjg:gridColumn>
</html>
```

### euamtags

``` xml
<%@ taglib uri="WEB-INF/euamtags.tld" prefix="Euam" %>
<div align="center">
    <Euam:form action="/submitCustomerForm" />
</div>
```

### spring

``` xml
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
    <div align="center">
        <form:form method="POST" action="/HelloWeb/addStudent" />
    </div>
</html>
```

``` xml
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
    <div align="center">
        <form:form method="POST" commandName="changePassword" action="" />
    </div>
</html>
```

## Support for Web Service calls in Razor code (cshtml)

-   Html.BeginForm
-   Ajax.BeginForm
-   Html.Action
-   Html.ActionLinkWithAccess
-   Html.ActionLink
-   Ajax.ActionLink
-   Html.RenderAction
-   Url.Action
-   Url.RouteUrl
-   Kendo

Once the HTML5 extension analysis is finished, the analyzer will output
the final number of web service calls created.

### Html.BeginForm

``` xml
@using (Html.BeginForm("Setting", "Peopulse", FormMethod.Post, new { autocomplete = "off", id = "form-peopulseFtpSettings", @class = "block" }))
{
    <input type="submit" name="Submit" />
}
```

### Ajax.BeginForm

``` xml
@using (Ajax.BeginForm("Setting", "Peopulse", new { //routeValues
        missionId = Model.Mission.Id,
        dayId = Model.MissionDayId,
        method="GET"
    },
    new AjaxOptions { //Ajax setup
        UpdateTargetId = "synthesisLinesDiv",
        HttpMethod = "GET",
        InsertionMode = InsertionMode.Replace,
    },
    new { //htmlAttributes
        Id = "form-selectSynthesis",
        //method="get"
    }))
{
}
```

### Html.Action

``` xml
@Html.Action("Delete", "Ctrl", new { id = item.ID })
```

### Html.ActionLinkWithAccess

``` xml
@Html.ActionLinkWithAccess(__("Add frequency interim summary"), "AddFrenquencyInterimSummary", null, new {@class = "btn popupLink"})
```

### Html.ActionLink

``` xml
@Html.ActionLink("Save", "SaveAction", "MainController", null, new { @class = "saveButton", onclick = "return false;" })
```

Ajax.ActionLink

``` xml
@Ajax.ActionLink("x", "Delete", "NotificationMessenger", new { notific.Id }, new AjaxOptions() { HttpMethod = "POST", OnComplete = "DismissNotification" }, new { @class = "notification-close" })
```

### Html.RenderAction

``` xml
@Html.RenderAction("Delete", "Ctrl", new { id = item.ID })
```

### Url.Action

``` xml
@{ Html.RenderPartial("_EmployeesTable", @Url.Action("GetFrenquencyInterimSummary")); }
```

``` xml
@using (Html.BeginForm("edit", "Employee", FormMethod.Post))
{
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Save" name="Edit" formaction="@Url.Action("edit")" formmethod="post" class="btn btn-default"/>
            </div>
        </div>
    </div>
}
```

``` xml
@Html.PagedListPager(Model, page => Url.Action("Index", new { page, sortOrder = ViewBag.CurrentSort, currentFilter = ViewBag.CurrentFilter}))
```

### Url.RouteUrl

``` xml
<a href="@(Url.RouteUrl(new{ action="DownloadFile", controller="DPScoreResults", id=Model.QuoteId, scoreRunId=Model.ScoreRunId}))" class="popupLink ico">
```

### Kendo

``` xml
<div>
    @(Html.Kendo().Grid<ElementControle>()
        .Name("gridElementsControle")
        .DataSource(datasource =>
            datasource.Ajax()
                .Read(r => r.Action("GetElementsControle", "GestionUnitesTraitement"))
        )
    )
</div>
```

## Support for Marionette with Backbone framework

The following objects are supported:

Marionette application:

``` java
var eduApp = Marionette.Application.extend({
        // called by marionette automatically when app is started
        onStart: function(options) {
        }
    });
```

Marionette view (the "template" item is important because it corresponds
to the link between the view and an html/hbs file):

``` java
    var overviewLayoutView = Marionette.LayoutView.extend({
        template: overviewTemplate,
        initialize: function () {
        },
        onShow: function () {
        }
    });
```

Backbone view:

``` java
    var view = BaseView.extend({

        initialize: function () {
        },
        render: function () {
        }
    });
```

The following links are created:

-   inheritance link between two views:

``` java
var ReviewBaseView = ValidatingStepBaseView.extend({ });
```

``` java
var ValidatingStepBaseView = Marionette.CompositeView.extend({});
```

-   call link between Application and its "onStart" method:

![](665813268.png)

-   call link between View and its "onShow" method:

![](665813267.png)

-   call link from an Application or View function to a View:

![](665813266.png)

``` java
var eduApp = Marionette.Application.extend({
        onStart: function(options) {
        },
       _showContentPage: function (model) {
           view = new OverviewLayoutView({model: model});
        }
     });
```

-   call link from an html/hbs file to the corresponding View, and to
    some view functions

In some cases (events), view functions are directly called from the
html/hbs file. In this case, call links are created from the html/hbs
file to the function:

![](665813265.png)

``` java
var viewTemplate = require('as-combined-complete-t');
var AccountEnrollmentCompleteView = ValidatingStepBaseView.extend({
    template: viewTemplate,
    events: {
        'submit'                 : 'submit',
        'click .acacomp-btn-prev': 'navigateToPreviousSlide',
        'click .com-exit'        : 'navigateToEnrollmentFlow'

    },
    submit: function () {
    },
    navigateToPreviousSlide: function () {
    },
    navigateToEnrollmentFlow: function () {
    },
});
```

-   call link from a view to another view functions (childEvents).

![](665813264.png)

``` java
var TraditionalInvestmentItemView = require('traditional-investment-item-v');
var TraditionalContributionElectionView = Marionette.CompositeView.extend({
    childEvents: {
        'inputCleared': 'onChildInputCleared'
    },
    childView: TraditionalInvestmentItemView,
    onChildInputCleared: function () {
    }
});
```

``` java
var TraditionalInvestmentItemView = Marionette.CompositeView.extend({
});
module.exports = TraditionalInvestmentItemView;
```

## Support for Apache Tiles framework

Apache Tiles are defined in xml files present mainly under the "WEB-INF"
directory, but not always. "Apache Tiles definition" objects are created
with links to jsp files. For example:

``` java
<?xml version="1.0" encoding="UTF-8" ?>

<!DOCTYPE tiles-definitions PUBLIC "-//Apache Software Foundation//DTD Tiles Configuration 3.0//EN"
  "http://tiles.apache.org/dtds/tiles-config_3_0.dtd">

<tiles-definitions>
  <definition name="commonLayout" template="/WEB-INF/views/template/common_layout.jsp">
    <put-attribute name="title" value="Access Care Planning"/>
    <put-attribute name="body"/>
  </definition>
  <definition name="service-documents-view" extends="commonLayout">
    <put-attribute name="title" value="Service Management"/>
    <put-attribute name="body">
      <definition template="/WEB-INF/views/configuration-menu/configuration-menu.jsp">
        <put-attribute name="content" value="/WEB-INF/views/service-documents.jsp"/>
      </definition>
    </put-attribute>
  </definition>
</tiles-definitions>
```

or

``` java
<?xml version="1.0" encoding="UTF-8" ?>

<!DOCTYPE tiles-definitions PUBLIC "-//Apache Software Foundation//DTD Tiles Configuration 1.1//EN" "http://jakarta.apache.org/struts/dtds/tiles-config_1_1.dtd">
<tiles-definitions>
  <definition name="commonLayout" page="/WEB-INF/views/template/common_layout.jsp">
    <put name="title" value="Access Care Planning"/>
    <put name="body"/>
  </definition>
  <definition name="service-documents-view" extends="commonLayout">
    <put name="title" value="Service Management"/>
    <put name="body">
      <definition page="/WEB-INF/views/configuration-menu/configuration-menu.jsp">
        <put name="content" value="/WEB-INF/views/service-documents.jsp"/>
      </definition>
    </put>
  </definition>
</tiles-definitions>
```

``` java
<?xml version="1.0" encoding="UTF-8" ?>

<!DOCTYPE tiles-definitions PUBLIC "-//Apache Software Foundation//DTD Tiles Configuration 3.0//EN"
  "http://tiles.apache.org/dtds/tiles-config_3_0.dtd">

<tiles-definitions>
    <definition name="landing.*" template="/pages/shop/templates/{1}/catalogLayout.jsp">
        <put-attribute name="header" value="/pages/shop/templates/{1}/sections/header.jsp" />
        <put-attribute name="navbar" value="/pages/shop/templates/{1}/sections/navbar.jsp" /> 
        <put-attribute name="body" value="/pages/shop/templates/{1}/pages/landing.jsp" />
        <put-attribute name="footer" value="/pages/shop/templates/{1}/sections/footer.jsp" />
    </definition>
</tiles-definitions>
```

In this case, several definitions are created, one for each folder
present under the "/pages/shop/templates" folder.

![](665813262.png)

"tiles:insertTemplate" in a jsp file:

``` java
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<tiles:insertTemplate template="/WEB-INF/jsp/acctresrs/importPlanSmallHeader.jsp" flush="false" />
```

A link is created from the jsp file to importPlanSmallHeader.jsp.

### Limitations

-   definitions created directly in jsp files through
    `<tiles:insertDefinition>` or other tags starting with `tiles:` or
    `tilesx:` are not supported. They seem to be very rarely used and
    are not recommended.
-   definitions created directly in java code are not supported.

## Support for Apache Tapestry framework

Java methods are called from .tml files.

TableauHistoriqueEffectif.tml
``` java
<t:container xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://tapestry.apache.org/schema/tapestry_5_1_0.xsd"
    <body>
        <t:if test="isAnnuel()">
        </t:if>
    </body>
</t:container>
```
or
``` java
<t:container xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://tapestry.apache.org/schema/tapestry_5_1_0.xsd"
            <b>
                ${isAnnuel()}
            </b>
</t:container>
```

TableauHistoriqueEffectif.java
``` java
public class TableauHistoriqueEffectif {
    public boolean isAnnuel() {
    }
}
```
![img_3.png](img_3.png)

Links between .tml pages are also created.

HistoriqueEffectif.tml
``` java
<t:container xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://tapestry.apache.org/schema/tapestry_5_1_0.xsd"
     <div id="ListeHistoriqueEffectif" class="content1">
         <t:historique.TableauHistoriqueEffectif  listeEffectifsHistoriques="listeEffectifsHistoriques" rechercheFormulaireEffectifs="rechercheFormulaireEffectifs"
                effectifChoisi="effectifChoisi"/>
     </div>
</t:container>
```
or
``` java
<t:container xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://tapestry.apache.org/schema/tapestry_5_1_0.xsd"
    <a href="#" t:type="pageLink" t:page="TableauHistoriqueEffectif">
    </a>
</t:container>
```

TableauHistoriqueEffectif.tml
``` java
<t:container xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://tapestry.apache.org/schema/tapestry_5_1_0.xsd"
</t:container>
```
![img_1.png](img_1.png)

BlocFormulaireRechercheDSN.tml
``` java
<t:container xmlns="http://www.w3.org/1999/xhtml" xmlns:t="http://tapestry.apache.org/schema/tapestry_5_1_0.xsd"
    <form t:type="form" t:id="formulaire" t:zone="^">
    </form>
</t:container>
```

BlocFormulaireRechercheDSN.java
``` java
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.AfterRender;

public class BlocFormulaireRechercheDSN {
    BlocFormulaireRechercheDSN() {
    }
    @OnEvent(value = EventConstants.VALIDATE, component = "formulaire")
    final void verificationFormulaire() {
    }
    @OnEvent(value = EventConstants.SUCCESS, component = "formulaire")
    final Object validationFormulaire() {
    }
    @OnEvent(value = EventConstants.FAILURE, component = "formulaire")
    final Object invalidationFormulaire() {
    }
    @AfterRender
    void afterRender() {
    }
}
```

![img_4.png](img_4.png)

## Support for handlebars file

In HTML5/Javascript ≥ 2.1.29, the analyzer supports handlebars files
[https://handlebarsjs.com/](https://handlebarsjs.com/).
This templating can be used in any kind of file type (javascript, HTML,
text, or any other type). A detection process is carried out by the
HTML5 extension to identify which kind of file represents the file. This
is why a specific metamodel type "Handlebars Source Code" is created,
and a HTML or Javascript fragment is created under this handlebars
source code:

 ![](665813261.png)

## Support for ADODB in asp/aspx files

In HTML5/Javascript ≥ 2.2.8-funcrel, the analyzer supports ADODB in vbscript code present in asp and aspx files. This enables to create links between vbscript code and tables present in analyzed databases.

 ![](image.png)

Following code snippets are supported:

``` java
Set intranet = Server.CreateObject("ADODB.Connection")
requete = " INSERT INTO TRAITEMENT (" 
requete = requete & " TYPE_TRAITEMENT, DATE_LANCEMENT, DATE_FIN) " 
requete = requete & " VALUES ( 'IMPORT FILTRE_EOTP'," 
requete = requete & " SYSDATE ," 
requete = requete & " NULL)" 
intranet.execute(requete)
```

``` java
set rs = server.CreateObject("ADODB.Recordset")
sql_select = "select ORDLI_Id,ORDLI_Status,ORDLI_Quantity,ORDLI_ND_Id from [orderline] where ordli_ordform_id = " & getSessionCurrentOrderFormId
rs.open sql_select, cn, 3, 3
```

``` java
Set cmd = server.createObject("ADODB.Command")
With cmd
	Set .ActiveConnection = dataconn
	.CommandText = "dbo.[v2_report_getReportExport_bk_articles3]"
	.CommandType = adCmdStoredProc
	.CommandTimeout = 44000
	Set params = .Parameters
End With
set rsTemp = cmd.execute
```

Here are the supported APIs:

-   ADODB.Command.Execute
-   ADODB.Connection.Execute
-   ADODB.Recordset.Open
-   ADODB.Recordset.Update
-   ADODB.Recordset.UpdateBatch
-   ADODB.Recordset.Delete

Same methods are supported on classes:

-   ADODB._Command
-   ADODB._RecordSet
-   ADODB.Command<nn>
-   ADODB.Connection<nn>
-   ADODB.Recordset<nn>
where <nn> is a version number.

## External libraries

In HTML5/Javascript ≥ 2.1.21, the analyzer is capable of finding
external libraries using following 4 files `npm-shrinkwrap.json`, `package-lock.json`, `yarn.lock`, `package.json` (dependencies and
devDependencies sections). For each library, the version is taken in the first occurrence in these files in this order (from `npm-shrinkwrap.json` file to `package.json`). It creates external libraries under a folder
named `HTML5 external libraries` directly under the project root. One
external library is created for each couple (library name, version), and
include links are created from the javascript sources to these
libraries. Only called libraries are created. Each library object
contains a property for the version. These objects are of the type
"HTML5 JavaScript Source Code".

External libraries are also created when code like `<script src="http://..../jquery.1.2.3.min.js">` is found in html files. The
version is searched in the url (here `1.2.3`) and the name is extracted (here `jquery`).

![](665813260.png)

## Support for contents of "node_modules" folder

In HTML5/Javascript ≥ 2.1.14, the analyzer is capable of scanning the
contents of the `node_modules` folder which contains third-party
external libraries. For every external item in the `node_modules` which
is called by analyzed source code, a corresponding object will be
created by the HTML5/JavaScript analyzer and therefore exposed in the
analysis results. The items in the `node_modules` folder are not
actually analyzed as such and are still ignored via an entry in
the [filters.json](../../filtersjson/) file.
This is one part of a project to expose (in analysis results) called
external libraries located in the `node_modules` folder.

By default, Console will ignore the `node_modules` folder (except in
releases 2.5.x and 2.6.x) due to analysis performance issues. Therefore
if you require this behaviour, you must manually remove the exclusion
pattern knowing that your analysis times may increase:

Fast Scan onboarding

![](665813259.jpg)

Legacy onboarding

![](665813258.jpg)

## End of comment and File skipped messages for .js files

You may find that the analysis log contains the following messages
for .js files:

``` text
Warning MODULMSG ; Job execution end of comment '\*\/' not found 0 ; 0 0 0 [Module name] 0 0 
Warning MODULMSG ; Job execution File skipped : K:\CAST\Ref\auto-refi2-staticresources\testing\inflow.js
```

These messages occur when the underlying Universal Analyzer raises a
warning whenever it encounters what it considers a syntax error. The
"End of comment" message is logged and then a following message is
logged stating that the file has been "skipped". These warnings should
be ignored as they have no impact: the HTML5/JavaScript extension will
analyze the file and raise the following message in the analysis log:

``` text
Information MODULMSG ; Job execution [com.castsoftware.html5] Light parsing of file K:\CAST\CASTMS\Deploy\Test_source\Ref\auto-refi2-staticresources\testing\inflow.js
```
