---
title: "Using the filters.json file"
linkTitle: "Using the filters.json file"
type: "docs"
---

## Introduction

The HTML5 and JavaScript extension is provided with a file called
filters.json at the root of the extension folder on your Node:

``` java
%PROGRAMDATA%\CAST\CAST\Extensions\com.castsoftware.html5.<version>\filters.json
```

This file lists all items that will be ignored during an analysis. The
file is pre-populated with items, but it is also possible to modify the
file manually and add your own custom filters.

## Filter syntax

One line in the filters.json file is equal to one filter. Lines are
separated by commas:

``` xml
{ "type" : "FilePath|FileContent", "value" : "<path>|<content>" },
{ "type" : "FilePath|FileContent", "value" : "<path>|<content>" }
```

## Ignoring files

To ignore a specific file, add the file name as follows. In the example
below, all files named "bootstrap.js" will be ignored wherever they are
found in the delivered source code:

``` xml
{ "type" : "FilePath", "value" : "/bootstrap.js" }
```

In the following example, the file named "module.js" will be ignored
when it is present in the path "/assets/js/":

``` xml
{ "type" : "FilePath", "value" : "/assets/js/module.js" }
```

## Ignoring folders

To ignore entire folders and their contents, specify the folder
path/name. In the examples below, all files contained in the
"node-modules" folder and in the "ui-bootstrap/assets" folder (wherever
they are found in the delivered source code) will be ignored:

``` xml
{ "type" : "FilePath", "value" : "/node_modules/" },
{ "type" : "FilePath", "value" : "/ui-bootstrap/assets/" }
```

## Ignoring file content

To ignore content in files, you can use the syntax shown below. In the
example below, all files containing "Copyright DHTMLX LTD" will be
ignored wherever they are found in the delivered source code:

``` xml
{ "type" : "FileContent", "value" : "Copyright DHTMLX LTD" }
```

## Wildcards

To ignore files named in a similar way you can use a \* wildcard. In the
example below, the following will be ignored wherever they are found in
the delivered source code:

-   all files named "blanket\*.js" 
-   all \*.js files located in the "test" folder
-   all files containing the content "cupQ.\*MIT License" (for example
    "Copyright 2016 Google Inc")

``` xml
{ "type" : "FilePath", "value" : "/blanket*.js" },
{ "type" : "FilePath", "value" : "/test/*.js" },
{ "type" : "FileContent", "value" : "Copyright.*Google Inc" },
```
