---
title: "Errors and warnings"
linkTitle: "Errors and warnings"
type: "docs"
---

<table>
<colgroup>
<col />
<col />
<col />
<col />
<col />
<col />
</colgroup>
<tbody>
<tr class="header">
<th class="confluenceTh">Message ID</th>
<th class="confluenceTh">Message Type</th>
<th class="confluenceTh"><p>Logged during</p></th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remediation</th>
<th class="confluenceTh">Action</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">HTML5-001</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">A jsp file has not been entirely analyzed
because an internal issue.</td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd">if the jsp file is important for you,
contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">HTML5-002</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">An asp/aspx/htc/cshtml file has not been
entirely analyzed because an internal issue.</td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd">if the file is important for you, contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">HTML5-003</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">A file has not been opened/analyzed.</td>
<td class="confluenceTd">See if you have access rights to this
file.</td>
<td class="confluenceTd"><br />
</td>
</tr>
<tr class="even">
<td class="confluenceTd">HTML5-004</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">An HTTP request has not been created, a
transaction link could be missing between a client and a server.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">if the file is important for you, contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">HTML5-005</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">One statement of a file has not been correctly
parsed.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">if the file is important for you and/or many
objects/links have not been created about this file, contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">HTML5-006</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">An important sql query has failed when removing
files skipped by analysis. Impact is that lines of code (LOC) will be
higher than expected.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">HTML5-007</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">An important sql query has failed when removing
bad links created by UA through grep (internal). Impact is that
transactions will be badly altered by these bad links.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">HTML5-008</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">Metrics of a file have not been reported (code
lines, comment lines). LOC could be wrong for this file.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">HTML5-009</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">A link useful for transactions has not been
created between a file created by JEE analyzer and an object
representing the same file created by HTML5/Javascript analyzer.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">HTML5-010</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">Objects corresponding to jsp files could be
counted twice because they were not set external in JEE project.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">HTML5-011</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">Objects corresponding to asp files could be
counted twice because they were not set external in DOTNET project.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">HTML5-012</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">Objects corresponding to aspx files could be
counted twice because they were not set external in DOTNET project.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="odd">
<td class="confluenceTd">HTML-013</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Post analyses</td>
<td class="confluenceTd">Objects corresponding to htc files could be
counted twice because they were not set external in DOTNET project.</td>
<td class="confluenceTd"><br />
</td>
<td class="confluenceTd">contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="header">
<th class="confluenceTh">Message ID</th>
<th class="confluenceTh">Message Type</th>
<th class="confluenceTh"><p>Logged during</p></th>
<th class="confluenceTh">Impact</th>
<th class="confluenceTh">Remediation</th>
<th class="confluenceTh">Action</th>
</tr>
&#10;<tr class="odd">
<td class="confluenceTd">EXTDOTNET-001</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">An ASP DOTNET operation has not been created
correctly.</td>
<td class="confluenceTd"><p><br />
</p></td>
<td class="confluenceTd">Contact <a
href="https://help.castsoftware.com/hc/en-us/articles/204189137-How-to-contact-CAST-Technical-Support"
rel="nofollow">CAST Technical Support</a></td>
</tr>
<tr class="even">
<td class="confluenceTd">EXTDOTNET-002</td>
<td class="confluenceTd">Warning</td>
<td class="confluenceTd">Analysis</td>
<td class="confluenceTd">A file has not been opened/analyzed.</td>
<td class="confluenceTd">See if you have access rights to this
file.</td>
<td class="confluenceTd"><br />
</td>
</tr>
</tbody>
</table>
