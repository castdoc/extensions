---
title: "HTML5 and JavaScript - 2.2"
linkTitle: "2.2"
type: "docs"
no_list: true
---

***

## Extension ID

com.castsoftware.html5

## What's new?

See [Release Notes](rn/).

## Description

The HTML5/JavaScript Analyzer can be used if your application is a Web Application, has HTML/JavaScript/CSS files and/or contains HTML/JavaScript fragments embedded into JEE and .NET files (for example). The analyzer provides the following features:

-   Checksum, number of code lines, number of comment lines, comments are present.
-   Local and global resolution is done when function is called directly through its name (inference engine resolution is not available).
-   For global resolution, caller is searched in all .js files. If only one callee is found, a link is created. If several callees are found, the analyzer watches inclusions in html files to see if it can filter the callee. If nothing is found in html files to filter, links are created to all possible callees.
-   Automated Function Point counting.

## Technology support

See [HTML5 and JavaScript - Technical Notes](notes) for information about:

-   Web Services support
-   Marionette with Backbone framework support
-   Apache Tiles framework support
-   Handlebars support
-   JSP/Classic ASP/JavaScript notes

## Files analyzed

<table>
<thead>
  <tr>
    <th>Icon(s)</th>
    <th>File</th>
    <th>Extension</th>
    <th>Notes</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td><img src="../images/665813309.png"></td>
    <td>HTML</td>
    <td>*.html, *.htm, *.xhtml, *.ftl; *.ftlh; *.vm, *.ngt, *.vt, *.vtl<br> <br>*.tml, *.mustache, *.njk, *.nunjucks, *.liquid, *.ejs, *.jst<br> <br>*.shtml, *.sshtml, *.ssi, *.shtm, *.stm</td>
    <td> <br>Supports HTML/XHTML versions 1 - 5.<br> <br>Creates one "HTML5 Source Code" object that is the caller of html to js links and a transaction entry point<br> <br>Broadcasts tags and attributes/values to other CAST extensions such as com.castsoftware.angularjs. Other extensions will not need to analyze the files themselves.<br> <br>*.ftl files are supported in releases ≥ 2.1.24-funcrel.<br> <br>*.ftlh, *.vm files are supported in releases ≥2.1.25-funcrel.<br> <br>*.ngt, *.vt, *.vtl files are supported in releases ≥ 2.1.28-funcrel and CAIP ≥ 8.3.54.<br> <br>*.tml, *.mustache, *.njk, *.nunjucks, *.liquid, *.ejs, *.jst, *.shtml, *.sshtml, *.ssi, *.shtm, *.stm files are supported in releases ≥ 2.2.1-funcrel and CAIP ≥ 8.3.54. </td>
  </tr>
  <tr>
    <td><img src="../images/665813300.png"><img src="../images/665813302.png"></td>
    <td>JavaScript</td>
    <td>*.js, *.jsx, *.mjs, *.jsm, *.cjs</td>
    <td> <br>Supports JavaScript 1 to 1.8.1.<br> <br>Supports JavaScript ECMA 6<br> <br>*.jsm files are supported in releases ≥ 2.1.23-funcrel.<br> <br>*.cjs files are supported in releases ≥2.1.27-funcrel. <br> <br>See also HTML5 and JavaScript - Technical Notes.</td>
  </tr>
  <tr>
    <td><img src="../images/665813305.png"></td>
    <td>Cascading Style Sheet</td>
    <td>*.css, *.scss<br> <br>*.less','*.sass', '*.styl', '*.stylus', '*.cssm', '*.pcss', '*.postcss</td>
    <td> <br>Supports CSS 1 - 3.<br> <br>*.less','*.sass', '*.styl', '*.stylus', '*.cssm', '*.pcss', '*.postcss files are supported in releases ≥ 2.1.29-funcrel and CAIP ≥ 8.3.54. </td>
  </tr>
  <tr>
    <td><img src="../images/665813301.png"></td>
    <td>Java Server Page</td>
    <td>*.jsp, *.jspx, *.jsf, *.jsff, *.jspf, *.tag, *.tags, *.tagf, *.tagx</td>
    <td> <br>Supports JSP 1.1 - 2.3. </td>
  </tr>
  <tr>
    <td><img src="../images/665813307.png"><img src="../images/665813308.png"></td>
    <td>Active Server Page</td>
    <td>*.asp, *.aspx</td>
    <td>See also HTML5 and JavaScript - Technical Notes.</td>
  </tr>
  <tr>
    <td><img src="../images/665813303.png"></td>
    <td>HTML Components</td>
    <td>*.htc</td>
    <td>HTC files contain html, JavaScript fragments that will be parsed. Created objects will be linked to the HTC file.</td>
  </tr>
  <tr>
    <td><img src="../images/665813306.png"></td>
    <td>ASP.NET MVC Razor</td>
    <td>*.cshtml</td>
    <td>See also <a href="notes">HTML5 and JavaScript - Technical Notes</a></td>
  </tr>
  <tr>
    <td><img src="../images/659914918.png"></td>
    <td>IBM EAD4J Jade</td>
    <td>*.jade</td>
    <td>Files related to the IBM EAD4J Jade framework.</td>
  </tr>
  <tr>
    <td><img src="../images/659914918.png"></td>
    <td>Pug</td>
    <td>*.pug</td>
    <td>Pug files.</td>
  </tr>
  <tr>
    <td><img src="../images/659914918.png"></td>
    <td>Haml</td>
    <td>*.haml</td>
    <td>Haml files are supported in releases ≥ 2.2.1-funcrel and CAIP ≥ 8.3.54.</td>
  </tr>
  <tr>
    <td>-</td>
    <td>YAML (YAML Ain't Markup Language)</td>
    <td>*.yml, *.yaml, </td>
    <td>Files related to the YAML language, handled by the com.castsoftware.nodejs extension.</td>
  </tr>
  <tr>
    <td>-</td>
    <td>Vue.JS</td>
    <td>*.vue</td>
    <td>Files related to the Vue.js JavaScript language, handled by the com.castsoftware.nodejs and com.castsoftware.vuejs extensions.</td>
  </tr>
  <tr>
    <td>-</td>
    <td>Properties</td>
    <td>*.properties</td>
    <td>-</td>
  </tr>
  <tr>
    <td>-</td>
    <td>Template</td>
    <td>*.template</td>
    <td>Files related to serverless.template, handled by the com.castsoftware.nodejs extension.</td>
  </tr>
  <tr>
    <td><img src="../images/659914918.png"></td>
    <td>Handlebars</td>
    <td>*.hbs, *.handlebars</td>
    <td>Files related to Handlebars.js. See Specific case of handlebars file below.</td>
  </tr>
</tbody>
</table>

You may find that the number of files delivered is more than then number of files reported after analysis. This is due to the following:

-   by default some files are automatically excluded from the analysis, typically third-party frameworks which are not required. Please see the [filters.json](../filtersjson/) file located at the root of the extension folder for a complete list of default exclusions.
-   some bundled javascript files are automatically excluded from the analysis, typically minified files (extension.min.js) or disguised minified files (extension.js), but for files containing one or a few very long lines, a specific calculation is actioned by the analyzer. 
-   some files that have been included in the analysis may not be saved in the CAST Analysis Service schema because they do not contain any useful information, i.e. they do not contain any technical sections such as functions which would lead to the creation of a specific object.

Since version 2.1.14-funcrel, files which are excluded from analysis are considered as external libraries when they are included in analyzed code. For each file, a "HTML5 JavaScript Source Code" is created with the external property set to "True". This means that these files are not included in rule checks or other computations.

## Function Point, Quality and Sizing support

- A green tick indicates that OMG Function Point counting and Transaction Risk Index are supported.
- A green tick indicates that CAST can measure size and that a minimum set of Quality Rules exist.

| Function Points<br> (transactions) | Quality and Sizing|
|:-:|:-:|
| :white_check_mark: | :white_check_mark: |

## Compatibility

| Release | Operating System | Supported |
|---|---|:-:|
| v3/8.4.x | Microsoft Windows / Linux | :white_check_mark: |
| v2/8.3.x | Microsoft Windows | :white_check_mark: |

## Download and installation instructions

For HTML5 web applications, the extension will be automatically installed by CAST Imaging Console:

![](../images/665813310.jpg)

## What results can you expect?

### Example application

![](../images/659914867.png)

Javascript ECMA6 Classes and Constructors example:

![](../images/659914866.png)  


### Objects

| Icon                         | Description                                                                                                 |
| :--------------------------: | ----------------------------------------------------------------------------------------------------------- |
| ![](../images/659914865.png) | Applet Class Reference                                                                                      |
| ![](../images/665813308.png) | ASP Content                                                                                                 |
| ![](../images/665813307.png) | ASPX Content                                                                                                |
| ![](../images/665813281.png) | Backbone View                                                                                               |
| ![](../images/665813276.png) | Bean Method Reference                                                                                       |
| ![](../images/665813306.png) | CSHTML Content / Razor Content                                                                              |
| ![](../images/665813305.png) | CSS Source Code                                                                                             |
| ![](../images/665813304.png) | CSS Source Code Fragment                                                                                    |
| ![](../images/665813278.png) | HTML5 External Libraries                                                                                    |
| ![](../images/665813275.png) | Haml Source Code                                                                                            |
| ![](../images/665813297.png) | Handlebars Source Code                                                                                      |
| ![](../images/665813303.png) | HTC Content                                                                                                 |
| ![](../images/665813309.png) | HTML Source Code / HTML Source Code Fragment (HTML fragment inside template files like .hbs files)          |
| ![](../images/665813309.png) | HTML Fragment (HTML fragment present inside JavaScript code)                                                |
| ![](../images/659914918.png) | Jade Source Code                                                                                            |
| ![](../images/665813292.png) | JavaScript Class                                                                                            |
| ![](../images/665813291.png) | JavaScript Constructor                                                                                      |
| ![](../images/665813294.png) | JavaScript Function                                                                                         |
| ![](../images/665813295.png) | JavaScript Initialisation                                                                                   |
| ![](../images/665813293.png) | JavaScript Method                                                                                           |
| ![](../images/665813302.png) | JavaScript Source Code                                                                                      |
| ![](../images/665813295.png) | JavaScript Source Code Fragment                                                                             |
| ![](../images/665813301.png) | Java Server Pages Content                                                                                   |
| ![](../images/665813300.png) | JSX Source Code                                                                                             |
| ![](../images/665813285.png) | Knockout View                                                                                               |
| ![](../images/665813283.png) | Marionette Application                                                                                      |
| ![](../images/665813284.png) | Marionette View                                                                                             |
| ![](../images/665813298.png) | Pug Source Code                                                                                             |
| ![](../images/665813279.png) | Tiles Definition                                                                                            |
| ![](../images/659914864.png) | JEE Applet                                                                                                  |
| ![](../images/665813290.png) | JavaScript WebSocket Service                                                                                |
| ![](../images/665813289.png) | XMLHttpRequest Get Service, JavaScript HttpRequest Get Service, EventSource Get Service, Razor Get Service  |
| ![](../images/665813288.png) | XMLHttpRequest Update Service, JavaScript HttpRequest Update Service                                        |
| ![](../images/665813287.png) | XMLHttpRequest Post Service, JavaScript HttpRequest Post/Put Service, Razor Method Call, Razor Post Service |
| ![](../images/665813286.png) | XMLHttpRequest Delete Service, JavaScript HttpRequest Delete Service                                        |


### Structural Rules

| Release | Link |
|---------|------|
| 2.2.7-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.7-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.7-funcrel) |
| 2.2.6-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.6-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.6-funcrel) |
| 2.2.5-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.5-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.5-funcrel) |
| 2.2.4-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.4-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.4-funcrel) |
| 2.2.3-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.3-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.3-funcrel) |
| 2.2.2-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.2-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.2-funcrel) |
| 2.2.1-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.1-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.1-funcrel) |
| 2.2.0-funcrel | [https://technologies.castsoftware.com/rules?sec=srs_html5&ref=\|\|2.2.0-funcrel](https://technologies.castsoftware.com/rules?sec=srs_html5&ref=%7C%7C2.2.0-funcrel) |

A global list is also available here: [https://technologies.castsoftware.com/rules?sec=t_1020000&ref=\|\|](https://technologies.castsoftware.com/rules?sec=t_1020000&ref=%7C%7C).

## Known Limitations

-   Creation and detection of object using "prototype" is not supported.
-   When HTML5/JavaScript source code is used as the "source" or "destination" in a Reference Pattern it will be ignored when the analysis is run - this is due to a limitation in the way the analyzer functions.
