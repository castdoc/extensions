# extensions

This repository is used to store the live documentation for CAST technology extensions in markdown format. The `main` branch is `protected` and serves as the `production` branch. All changes must be made in a feature branch and merged in via a merge request. Documentation in markdown format is located in the `content/en/technologies` folder.

A site called `extensions` is configured in Cloudflare Pages and uses this repository in a `Hugo` build process - the build process is not currently automatic. In turn, this repository is defined as a `Hugo module` and is referenced in the build configuration for the Cloudlare Pages `doc` site (which is used to publish `doc.castsoftware.com`). A gitlab.com webhook is used in the Cloudlfare Pages `doc` site so that merges/commits in `main` for this current repository trigger a Hugo build.

_COMING SOON (not yet implemented):_ When changes are merged into `main`, an automatic build process is triggered so that you can view the documentation in its HTML form before it is built into the production documentation website, here: [https://extensions-45e.pages.dev/technologies](https://extensions-45e.pages.dev/technologies) >>> this site reflects the current content of the `main` branch.
